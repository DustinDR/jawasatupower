# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import _, api, fields, models
from datetime import datetime

class StockQuantInherited(models.Model):
    _inherit = 'stock.quant'

    expire_date = fields.Datetime('Expiration_date', readonly=True, compute='_compute_lots_expire_date')


    def _compute_lots_expire_date(self):
        for record in self:
            if record.lot_id.id == False:
                record.expire_date = False
            else:
                record.expire_date = record.lot_id.expiration_date

    @api.model
    def create_scrap(self):
        now = datetime.now()
        stock_quants = self.env['stock.quant'].search([])
        scrap_list = []
        ICP = self.env['ir.config_parameter'].sudo()
        expired_lot_serial_no = ICP.get_param('expired_lot_serial_no', 'scrap_expire')
        is_auto_validate = ICP.get_param('is_auto_validate', False)
        quant_ids = []
        product_usage_quant = []
        context = dict(self.env.context) or {}
        for quant in stock_quants:
            if quant.location_id.usage == 'internal':
                if quant.expire_date == False:
                    continue
                if quant.lot_id.expiration_date < now :
                    if quant.quantity > 0:
                        quant_ids.append(quant)
                        if expired_lot_serial_no == 'scrap_expire':
                            product_usage_quant.append(quant)
        if expired_lot_serial_no == 'scrap_expire':
            pu_temp_data = []
            final_data = []
            for rec in product_usage_quant:
                if {'location_id': rec.location_id.id, 'warehouse_id': rec.location_id.warehouse_id.id} in pu_temp_data:
                    filter_line = list(filter(lambda r:r.get('location_id').id == rec.location_id.id and r.get('warehouse_id').id == rec.location_id.warehouse_id.id, final_data))
                    if filter_line:
                        filter_line[0]['quants'].append(rec)
                else:
                    pu_temp_data.append({'location_id': rec.location_id.id, 'warehouse_id': rec.location_id.warehouse_id.id})
                    final_data.append({
                        'location_id': rec.location_id,
                        'warehouse_id': rec.location_id.warehouse_id,
                        'quants': [rec],
                    })
            for data in final_data:
                product_usage_line = [(0, 0, {'location_id': line.location_id.id, 
                                              'product_id': line.product_id.id,
                                              'scrap_qty': line.available_quantity,
                                              'product_uom_id': line.product_id.uom_id.id,
                                              'lot_id': line.lot_id.id,
                                              'package_id': line.package_id.id,
                                              'owner_id': line.owner_id.id,
                    }) for line in data.get('quants')]
                product_usage = self.env['stock.scrap.request'].create({
                    'scrap_request_name': 'Product Usage %s' % data.get('warehouse_id').name,
                    'warehouse_id': data.get('warehouse_id').id,
                    'responsible_id': 1,
                    'create_uid': 1,
                    'scrap_ids': product_usage_line,
                    'is_product_usage': True,
                    'auto_scrap_notification': [(6, 0, data.get('warehouse_id').responsible_users.ids)]
                })
                product_usage.action_request_confirm()
                context.update({'auto_validate': True})
                product_usage.with_context(context).action_request_validated()
        if expired_lot_serial_no == 'posted':
            temp_data = []
            final_data = []
            for record in quant_ids:
                if record.location_id.id in temp_data:
                    filter_line = list(filter(lambda r:r.get('location_id').id == record.location_id.id, final_data))
                    if filter_line:
                        filter_line[0]['quants'].append(record)
                else:
                    temp_data.append(record.location_id.id)
                    final_data.append({
                        'location_id': record.location_id,
                        'quants': [record],
                        })
            for data in final_data:
                source_location_id = data.get('location_id')
                warehouse_id = source_location_id.warehouse_id
                destination_location_id = self.env['stock.location'].search([('warehouse_id', '=', warehouse_id.id), ('is_expired_stock_location', '=', True)], limit=1)
                operation_type_id = warehouse_id.int_type_id
                schedule_date = datetime.today()
                internal_warehouse_id = self.env['stock.picking'].create({
                    'warehouse_id': warehouse_id.id,
                    'location_id': source_location_id.id,
                    'location_dest_id': destination_location_id.id,
                    'company_id': warehouse_id.company_id.id,
                    'scheduled_date': schedule_date,
                    'is_expired_tranfer': True,
                    'picking_type_id': operation_type_id.id,
                })
                for line in data.get('quants'):
                    move_line_id = self.env['stock.move'].create({
                        'name': line.product_id.display_name,
                        'product_id': line.product_id.id,
                        'product_uom_qty': line.available_quantity or 1,
                        'product_uom': line.product_id.uom_id.id,
                        'location_id': source_location_id.id,
                        'location_dest_id': destination_location_id.id,
                        'date': schedule_date,
                        'picking_id': internal_warehouse_id.id
                    })
                internal_warehouse_id.action_confirm()
                for line in internal_warehouse_id.move_ids_without_package:
                    line.remaining = line.product_uom_qty
