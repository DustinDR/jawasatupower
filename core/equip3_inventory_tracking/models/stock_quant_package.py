from odoo import api, fields, models, _
import datetime
from datetime import date, time
from odoo.exceptions import ValidationError

class StockQuantPackage(models.Model):
    _inherit = "stock.quant.package"

    name = fields.Char(readonly=True, default='New')
    max_weight = fields.Float("Maximum Weight", related="packaging_id.max_weight", readonly=True)

    package_status = fields.Selection([
                    ('packed', 'Packed'),
                    ('partial', 'Partial'),
                    ('empty', 'Empty')
                    ], 'Status', compute="_compute_package_staus", store=True)

    @api.model
    def action_package_unpack(self, vals):
        vals = [int(i) for i in vals]
        pack_ids = self.browse(vals)
        if pack_ids.filtered(lambda x: x.package_status == 'empty'):
            raise ValidationError(_("Selected Records contain Empty Packages!"))
        else:
            pack_ids.unpack()
        return True

    @api.depends('quant_ids', 'weight', 'max_weight')
    def _compute_package_staus(self):
        for rec in self:
            if len(rec.quant_ids) == 0:
                rec.package_status = 'empty'
            elif len(rec.quant_ids) >= 1 and rec.weight != rec.max_weight:
                rec.package_status = 'partial'
            elif len(rec.quant_ids) >= 1 and rec.weight == rec.max_weight:
                rec.package_status = 'packed'

    @api.model
    def create(self, vals):
        stock_quant_package_seq = self.env.ref('equip3_inventory_tracking.stock_quant_package_seq')
        record_id = self.search([], limit=1, order='id desc')
        check_today = False
        if record_id and record_id.create_date.date() == date.today():
            check_today = True
        if not check_today:
            stock_quant_package_seq.number_next_actual = 1
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code(
                'stock.quant.package.seq') or 'New'
        result = super(StockQuantPackage, self).create(vals)
        return result
