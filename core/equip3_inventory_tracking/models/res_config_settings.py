
from odoo import _, api, fields, models

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    expired_lot_serial_no = fields.Selection([
                ('scrap_expire', 'Immediately Scrap Expired Products'),
                ('posted', 'Creates a Transfer Operation to Expired Location'),
                ], string='Auto-Scrap Expired Products', defalut="scrap_expire")
    is_auto_validate = fields.Boolean(string="Auto-Validate Transfer Operation")    

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        ICP = self.env['ir.config_parameter'].sudo()
        res.update(
            expired_lot_serial_no=ICP.get_param('expired_lot_serial_no', 'scrap_expire'),
            is_auto_validate=ICP.get_param('is_auto_validate', False)
        )
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        ICP = self.env['ir.config_parameter'].sudo()
        ICP.set_param('expired_lot_serial_no', self.expired_lot_serial_no)
        ICP.set_param('is_auto_validate', self.is_auto_validate)
    