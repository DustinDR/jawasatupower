# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import product_product
from . import stock_quant
from . import internal_transfer
from . import stock_picking
from . import res_config_settings
from . import stock_quant_package
from . import stock_production_lot
from . import stock_scrap_request