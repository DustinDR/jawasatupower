
from odoo import _, api, fields, models

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    is_expired_tranfer = fields.Boolean(string="Expired Transfer")
    