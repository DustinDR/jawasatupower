from odoo import _, api, fields, models
from datetime import datetime, date, timedelta

class StockProductionLot(models.Model):
    _inherit = 'stock.production.lot'

    @api.model
    def _alert_date_exceeded(self):
        """overriding and disabling the original function"""

    @api.model
    def _expire_date_cron(self):
        lot_ids = self.search([("expiration_date", ">", datetime.now().replace(hour=0, minute=0, second=0)), ("expiration_date", "<=", datetime.now().replace(hour=23, minute=59, second=59))])
        expire_lot_quant_ids = lot_ids.quant_ids.filtered(lambda l: l.location_id.usage == "internal" and l.location_id.get_warehouse().responsible_users)
        warehouse_wise_expired_quant = {}
        for quant in expire_lot_quant_ids:
            quant_warehouse = quant.location_id.get_warehouse()
            if quant_warehouse not in warehouse_wise_expired_quant:
                warehouse_wise_expired_quant[quant_warehouse] = quant
            else:
                warehouse_wise_expired_quant[quant_warehouse] = warehouse_wise_expired_quant[quant_warehouse] | quant

        mail_template_name = "equip3_inventory_tracking.email_template_expired_stock_production_lot"
        mail_subject = "Expired Products Today"
        for rec in warehouse_wise_expired_quant:
            self.send_email_notification(rec.responsible_users, rec, warehouse_wise_expired_quant[rec], mail_template_name, mail_subject)

    @api.model
    def _expire_alert_date_cron(self):
        lot_ids = self.search([("alert_date", ">", datetime.now().replace(hour=0, minute=0, second=0)), ("alert_date", "<=", datetime.now().replace(hour=23, minute=59, second=59))])
        expire_lot_quant_ids = lot_ids.quant_ids.filtered(lambda l: l.location_id.usage == "internal" and l.location_id.get_warehouse().responsible_users)
        warehouse_wise_expire_quant = {}
        for quant in expire_lot_quant_ids:
            quant_warehouse = quant.location_id.get_warehouse()
            if quant_warehouse not in warehouse_wise_expire_quant:
                warehouse_wise_expire_quant[quant_warehouse] = quant
            else:
                warehouse_wise_expire_quant[quant_warehouse] = warehouse_wise_expire_quant[quant_warehouse] | quant

        mail_template_name = "equip3_inventory_tracking.email_template_expired_alert_stock_production_lot"
        mail_subject = "Alert On Products That Are Going To Be Expire Soon"
        for rec in warehouse_wise_expire_quant:
            self.send_email_notification(rec.responsible_users, rec, warehouse_wise_expire_quant[rec], mail_template_name, mail_subject)

    def send_email_notification(self, responsible_users, warehouse, quant, mail_template_name, mail_subject):
        ctx = {
            "email_from": self.env.user.company_id.email,
            "quant": quant
        }
        template_id = self.env.ref(mail_template_name)

        for user in responsible_users:
            ctx.update({
                "email_to": user.email,
                "responsible_users": user.name,
            })

            template_id.with_context(ctx).send_mail(warehouse.id, True)
            body_html = self.env['mail.render.mixin'].with_context(ctx)._render_template(
                template_id.body_html, 'stock.production.lot', warehouse.ids, post_process=True)[warehouse.id]
            message_id = (
                self.env["mail.message"]
                    .sudo()
                    .create(
                    {
                        "subject": mail_subject,
                        "body": body_html,
                        "message_type": "notification",
                        "partner_ids": [
                            (
                                6,
                                0,
                                user.partner_id.ids,
                            )
                        ],
                    }
                )
            )
            notif_create_values = {
                "mail_message_id": message_id.id,
                "res_partner_id": user.partner_id.id,
                "notification_type": "inbox",
                "notification_status": "sent",
            }
            self.env["mail.notification"].sudo().create(notif_create_values)

    def get_lot_url(self):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        action_id = self.env.ref('stock.action_production_lot_form')
        return base_url + '/web#id=' + str(self.id) + '&action='+ str(action_id.id) +'&view_type=form&model=stock.production.lot'
