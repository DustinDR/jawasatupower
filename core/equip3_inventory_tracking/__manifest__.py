# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Equip3 Inventory Tracking',
    'author': 'Hashmicro / Prince',
    'version': '1.1.7',
    'summary': 'Manage your Inventory tracking.',
    'depends': ['aspl_product_expiry_alert', 'delivery', 'equip3_inventory_control'],
    'category': 'Inventory/Inventory',
    'data': [
        "views/assets.xml",
        'views/product_views.xml',
        'views/stock_quant.xml',
        'views/expiry_views.xml',
        'views/stock_picking_views.xml',
        'views/res_config_settings_views.xml',
        'views/stock_quant_package_views.xml',
        'views/stock_scrap_request_views.xml',
        'data/ir_sequence_data.xml',
        'data/cron_autoscrap.xml',
        'data/mail_template.xml',
        'data/stock_product_lot_cron.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
