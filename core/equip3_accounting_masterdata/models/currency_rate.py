import logging
import math
import re
import time
import traceback
from lxml import etree
from odoo import api, fields, models, tools, _
from odoo.addons.base.models.ir_ui_view import (
transfer_field_to_modifiers, transfer_node_to_modifiers, transfer_modifiers_to_node,
)


def setup_modifiers(node, field=None, context=None, in_tree_view=False):
    modifiers = {}
    if field is not None:
        transfer_field_to_modifiers(field, modifiers)
    transfer_node_to_modifiers(
        node, modifiers, context=context)
    transfer_modifiers_to_node(modifiers, node)

_logger = logging.getLogger(__name__)

try:
    from num2words import num2words
except ImportError:
    _logger.warning("The num2words python library is not installed, amount-to-text features won't be fully available.")
    num2words = None

CURRENCY_DISPLAY_PATTERN = re.compile(r'(\w+)\s*(?:\((.*)\))?')


class CurrencyRate(models.Model):
    _inherit = "res.currency.rate"

    @api.depends('conversion', 'mr_rate')
    def _get_rate(self):
        for record in self:
            if record.conversion and record.company_id.is_inverse_rate:
                val_rate = float(1) / record.conversion
                record.rate = val_rate
                record.mr_rate = val_rate
            elif record.mr_rate and not record.company_id.is_inverse_rate:
                record.rate = record.mr_rate
                record.conversion = float(1) / record.mr_rate
            else:
                record.rate = 1.0

    rate = fields.Float(
        digits=(12, 12), default=1.0,
        help='The rate of the currency to the currency of rate 1',
        compute='_get_rate', store=True)
    conversion = fields.Float('Inverse Rate')
    mr_rate = fields.Float(
        digits=(12, 12), default=1.0,
        help='The rate of the currency to the currency of rate 1')

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        result = super(CurrencyRate, self).fields_view_get(
            view_id, view_type, toolbar=toolbar, submenu=submenu)
        company_id = self.env.context.get("company_id") or self.env.company.id
        company_obj = self.env['res.company'].browse(company_id)
        doc = etree.XML(result['arch'])
        if not company_obj.is_inverse_rate:
            for node in doc.xpath("//field[@name='conversion']"):
                doc.remove(node)
            if view_type == 'tree':
                for node in doc.xpath("//field[@name='rate']"):
                    doc.remove(node)
        else:
            for node in doc.xpath("//field[@name='mr_rate']"):
                doc.remove(node)
        result['arch'] = etree.tostring(doc, encoding='unicode')
        return result


class Currency(models.Model):
    _name = "res.currency"
    _inherit = [
        'res.currency', 'mail.thread', 'mail.activity.mixin']

    name = fields.Char(
        string='Currency', size=3, required=True,
        tracking=True, help="Currency Code (ISO 4217)")
    symbol = fields.Char(
        help="Currency sign, to be used when printing amounts.",
        tracking=True, required=True)
    rate = fields.Float(
        compute='_compute_current_rate',
        tracking=True, string='Current Rate', digits=(12, 12),
        help='The rate of the currency to the currency of rate 1.')
    conversion = fields.Float(
        compute='_compute_current_conversion',
        tracking=True, string='Inverse Rate', digits=0)
    rate_ids = fields.One2many(
        'res.currency.rate', 'currency_id',
        tracking=True, string='Rates')
    rounding = fields.Float(
        string='Rounding Factor', digits=(12, 6),
        tracking=True, default=0.01)
    decimal_places = fields.Integer(
        compute='_compute_decimal_places',
        tracking=True, store=True)
    active = fields.Boolean(default=True)
    position = fields.Selection(
        [
            ('after', 'After Amount'), ('before', 'Before Amount')
        ], default='after',
        tracking=True, string='Symbol Position',
        help="Determines where the currency symbol should be placed after or before the amount.")
    date = fields.Date(
        tracking=True, compute='_compute_date')
    currency_unit_label = fields.Char(
        string="Currency Unit", tracking=True, help="Currency Unit Name")
    currency_subunit_label = fields.Char(
        string="Currency Subunit",
        tracking=True, help="Currency Subunit Name")

    _sql_constraints = [
        ('unique_name', 'unique (name)', 'The currency code must be unique!'),
        ('rounding_gt_zero', 'CHECK (rounding>0)', 'The rounding factor must be greater than 0!')
    ]

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(Currency, self).fields_view_get(
            view_id, view_type, toolbar=toolbar, submenu=submenu)
        company_id = self.env.context.get("company_id") or self.env.company.id
        company_obj = self.env['res.company'].browse(company_id)
        doc = etree.XML(result['arch'])
        if not company_obj.is_inverse_rate:
            if view_type == 'form':
                node = doc.xpath("//field[@name='conversion']")[0]
                node.set('invisible', '1')
                setup_modifiers(node, result['fields']['conversion'])
        result['arch'] = etree.tostring(doc, encoding='unicode')
        return result

    def _get_rates(self, company, date):
        self.env['res.currency.rate'].flush(['rate', 'currency_id', 'company_id', 'name'])
        query = """SELECT c.id,
                        COALESCE((SELECT r.rate FROM res_currency_rate r
                                  WHERE r.currency_id = c.id AND r.name <= %s
                                    AND (r.company_id IS NULL OR r.company_id = %s)
                               ORDER BY r.company_id, r.name DESC
                                  LIMIT 1), 1.0) AS rate
                   FROM res_currency c
                   WHERE c.id IN %s"""
        self._cr.execute(query, (date, company.id, tuple(self.ids)))
        currency_rates = dict(self._cr.fetchall())
        return currency_rates

    def _get_conversion(self, company, date):
        self.env['res.currency.rate'].flush(['conversion', 'currency_id', 'company_id', 'name'])
        query = """SELECT c.id,
                        COALESCE((SELECT r.conversion FROM res_currency_rate r
                                  WHERE r.currency_id = c.id AND r.name <= %s
                                    AND (r.company_id IS NULL OR r.company_id = %s)
                               ORDER BY r.company_id, r.name DESC
                                  LIMIT 1), 1.0) AS conversion
                   FROM res_currency c
                   WHERE c.id IN %s"""
        self._cr.execute(query, (date, company.id, tuple(self.ids)))
        currency_rates = dict(self._cr.fetchall())
        return currency_rates

    @api.depends('rate_ids.rate')
    def _compute_current_rate(self):
        date = self._context.get('date') or fields.Date.today()
        company = self.env['res.company'].browse(self._context.get('company_id')) or self.env.company
        # the subquery selects the last rate before 'date' for the given currency/company
        currency_rates = self._get_rates(company, date)
        _logger.info(currency_rates)
        for currency in self:
            currency.rate = currency_rates.get(currency.id) or 1.0

    @api.depends('rate_ids.conversion')
    def _compute_current_conversion(self):
        date = self._context.get('date') or fields.Date.today()
        company = self.env['res.company'].browse(self._context.get('company_id')) or self.env.company
        # the subquery selects the last rate before 'date' for the given currency/company
        currency_rates = self._get_conversion(company, date)
        _logger.info(currency_rates)
        for currency in self:
            currency.conversion = currency_rates.get(currency.id) or 1.0

    @api.depends('rounding')
    def _compute_decimal_places(self):
        for currency in self:
            if 0 < currency.rounding < 1:
                currency.decimal_places = int(math.ceil(math.log10(1/currency.rounding)))
            else:
                currency.decimal_places = 0

    @api.depends('rate_ids.name')
    def _compute_date(self):
        for currency in self:
            currency.date = currency.rate_ids[:1].name

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        results = super(Currency, self)._name_search(name, args, operator=operator, limit=limit, name_get_uid=name_get_uid)
        if not results:
            name_match = CURRENCY_DISPLAY_PATTERN.match(name)
            if name_match:
                results = super(Currency, self)._name_search(name_match.group(1), args, operator=operator, limit=limit, name_get_uid=name_get_uid)
        return results

    def name_get(self):
        return [(currency.id, tools.ustr(currency.name)) for currency in self]

    def amount_to_text(self, amount):
        self.ensure_one()
        def _num2words(number, lang):
            try:
                return num2words(number, lang=lang).title()
            except NotImplementedError:
                return num2words(number, lang='en').title()

        if num2words is None:
            logging.getLogger(__name__).warning("The library 'num2words' is missing, cannot render textual amounts.")
            return ""

        formatted = "%.{0}f".format(self.decimal_places) % amount
        parts = formatted.partition('.')
        integer_value = int(parts[0])
        fractional_value = int(parts[2] or 0)

        lang = tools.get_lang(self.env)
        amount_words = tools.ustr('{amt_value} {amt_word}').format(
                        amt_value=_num2words(integer_value, lang=lang.iso_code),
                        amt_word=self.currency_unit_label,
                        )
        if not self.is_zero(amount - integer_value):
            amount_words += ' ' + _('and') + tools.ustr(' {amt_value} {amt_word}').format(
                        amt_value=_num2words(fractional_value, lang=lang.iso_code),
                        amt_word=self.currency_subunit_label,
                        )
        return amount_words

    def round(self, amount):
        """Return ``amount`` rounded  according to ``self``'s rounding rules.

           :param float amount: the amount to round
           :return: rounded float
        """
        self.ensure_one()
        return tools.float_round(amount, precision_rounding=self.rounding)

    def compare_amounts(self, amount1, amount2):
        """Compare ``amount1`` and ``amount2`` after rounding them according to the
           given currency's precision..
           An amount is considered lower/greater than another amount if their rounded
           value is different. This is not the same as having a non-zero difference!

           For example 1.432 and 1.431 are equal at 2 digits precision,
           so this method would return 0.
           However 0.006 and 0.002 are considered different (returns 1) because
           they respectively round to 0.01 and 0.0, even though
           0.006-0.002 = 0.004 which would be considered zero at 2 digits precision.

           :param float amount1: first amount to compare
           :param float amount2: second amount to compare
           :return: (resp.) -1, 0 or 1, if ``amount1`` is (resp.) lower than,
                    equal to, or greater than ``amount2``, according to
                    ``currency``'s rounding.

           With the new API, call it like: ``currency.compare_amounts(amount1, amount2)``.
        """
        self.ensure_one()
        return tools.float_compare(amount1, amount2, precision_rounding=self.rounding)

    def is_zero(self, amount):
        """Returns true if ``amount`` is small enough to be treated as
           zero according to current currency's rounding rules.
           Warning: ``is_zero(amount1-amount2)`` is not always equivalent to
           ``compare_amounts(amount1,amount2) == 0``, as the former will round after
           computing the difference, while the latter will round before, giving
           different results for e.g. 0.006 and 0.002 at 2 digits precision.

           :param float amount: amount to compare with currency's zero

           With the new API, call it like: ``currency.is_zero(amount)``.
        """
        self.ensure_one()
        return tools.float_is_zero(amount, precision_rounding=self.rounding)

    @api.model
    def _get_conversion_rate(self, from_currency, to_currency, company, date):
        currency_rates = (from_currency + to_currency)._get_rates(company, date)
        res = currency_rates.get(to_currency.id) / currency_rates.get(from_currency.id)
        return res

    def _convert(self, from_amount, to_currency, company, date, round=True):
        """Returns the converted amount of ``from_amount``` from the currency
           ``self`` to the currency ``to_currency`` for the given ``date`` and
           company.

           :param company: The company from which we retrieve the convertion rate
           :param date: The nearest date from which we retriev the conversion rate.
           :param round: Round the result or not
        """
        self, to_currency = self or to_currency, to_currency or self
        assert self, "convert amount from unknown currency"
        assert to_currency, "convert amount to unknown currency"
        assert company, "convert amount from unknown company"
        assert date, "convert amount from unknown date"
        # apply conversion rate
        if self == to_currency:
            to_amount = from_amount
        else:
            to_amount = from_amount * self._get_conversion_rate(self, to_currency, company, date)
        # apply rounding
        return to_currency.round(to_amount) if round else to_amount

    @api.model
    def _compute(self, from_currency, to_currency, from_amount, round=True):
        _logger.warning('The `_compute` method is deprecated. Use `_convert` instead')
        date = self._context.get('date') or fields.Date.today()
        company = self.env['res.company'].browse(self._context.get('company_id')) or self.env.company
        return from_currency._convert(from_amount, to_currency, company, date)

    def compute(self, from_amount, to_currency, round=True):
        _logger.warning('The `compute` method is deprecated. Use `_convert` instead')
        date = self._context.get('date') or fields.Date.today()
        company = self.env['res.company'].browse(self._context.get('company_id')) or self.env.company
        return self._convert(from_amount, to_currency, company, date)

    def _select_companies_rates(self):
        return """
            SELECT
                r.currency_id,
                COALESCE(r.company_id, c.id) as company_id,
                r.rate,
                r.name AS date_start,
                (SELECT name FROM res_currency_rate r2
                 WHERE r2.name > r.name AND
                       r2.currency_id = r.currency_id AND
                       (r2.company_id is null or r2.company_id = c.id)
                 ORDER BY r2.name ASC
                 LIMIT 1) AS date_end
            FROM res_currency_rate r
            JOIN res_company c ON (r.company_id is null or r.company_id = c.id)
        """
