from odoo import api, fields, models, _

class AccountMapLineMasterdata(models.Model):
    _name = 'account.account.map.line'

    company_id = fields.Many2one('res.company', string='Parent Company', related='map_id.company_id')
    account_id = fields.Many2one('account.account', string='Parent Account', domain="[('company_id', '=', company_id)]")
    target_company = fields.Many2one('res.company', string='Target Company', related='map_id.child_company_id')
    target_account = fields.Many2one('account.account', string='Child Account', domain="[('company_id', '=', target_company)]")
    map_id = fields.Many2one('account.account.map', delegate=True)

class AccountMapMasterdata(models.Model):
    _name = 'account.account.map'

    company_id = fields.Many2one('res.company', string="Parent Company", required=True, readonly=True, default=lambda self: self.env.company.id, tracking=True)
    line_ids = fields.One2many('account.account.map.line', 'map_id', string='Accounts')

    child_company_id = fields.Many2one('res.company', string="Child Company", required=True, tracking=True)
    ownership  = fields.Float(string='Parent Ownership')
    sales_line_ids = fields.One2many('account.tax.map.sales.line', 'map_id', string="Sales Taxes", tracking=True)
    purchase_line_ids = fields.One2many('account.tax.map.purchase.line', 'map_id', string="Purchase Taxes ", tracking=True)

    @api.onchange('child_company_id')
    def _write_detail(self):
        context = dict(self.env.context)
        context.update({'allowed_company_ids' : [self.company_id.id, self.child_company_id.id]})
        self.env.context = context
        if self.child_company_id.id != False:
            self.ownership = 100.00
            line_id_child = self.env['account.account'].search([('company_id', '=', self.child_company_id.id)])            
            list_line=[(5,0,0)]
            n = len(line_id_child)
            i = 0
            while i < n:
                lines_dict = {'company_id' : self.company_id.id,
                              'account_id' : False,
                              'target_company' : self.child_company_id.id,
                              'target_account' : line_id_child[i].id,}
                list_line.append((0,0,lines_dict))
                i += 1
            self.line_ids = list_line
            
            line_id_child = self.env['account.tax'].search(['&', ('company_id', '=', self.child_company_id.id), ('type_tax_use', '=', 'sale')])            
            list_line=[(5,0,0)]
            n = len(line_id_child)
            i = 0
            while i < n:
                lines_dict = {'company_id' : self.company_id.id,
                              'sales_tax_id' : False    ,
                              'target_company' : self.child_company_id.id,
                              'target_sales_tax' : line_id_child[i].id,}
                list_line.append((0,0,lines_dict))
                i += 1
            self.sales_line_ids = list_line

            line_id_child = self.env['account.tax'].search(['&', ('company_id', '=', self.child_company_id.id), ('type_tax_use', '=', 'purchase')])
            list_line=[(5,0,0)]
            n = len(line_id_child)
            i = 0
            while i < n:
                lines_dict = {'company_id' : self.company_id.id,
                              'purchase_tax_id' : False,
                              'target_company' : self.child_company_id.id,
                              'target_purchase_tax' : line_id_child[i].id,}
                list_line.append((0,0,lines_dict))
                i += 1
            self.purchase_line_ids = list_line

class AccountTaxMapSales(models.Model):
    _name = 'account.tax.map.sales.line'

    company_id = fields.Many2one('res.company', string='Parent Company', related='map_id.company_id')
    sales_tax_id = fields.Many2one('account.tax', string='Parent Sales Tax', domain="['&', ('company_id', '=', company_id), ('type_tax_use', '=', 'sale')]")
    target_company = fields.Many2one('res.company', string='Child Company', related='map_id.child_company_id')
    target_sales_tax = fields.Many2one('account.tax', string='Child Sales Tax', domain="['&', ('company_id', '=', target_company), ('type_tax_use', '=', 'sale')]")
    map_id = fields.Many2one('account.account.map', delegate=True)

class AccountTaxMapPurchase(models.Model):
    _name = 'account.tax.map.purchase.line'

    company_id = fields.Many2one('res.company', string='Parent Company', related='map_id.company_id')
    purchase_tax_id = fields.Many2one('account.tax', string='Parent Purchase Tax', domain="['&', ('company_id', '=', company_id), ('type_tax_use', '=', 'purchase')]")
    target_company = fields.Many2one('res.company', string='Child Company', related='map_id.child_company_id')
    target_purchase_tax = fields.Many2one('account.tax', string='Child Purchase Tax', domain="['&', ('company_id', '=', target_company), ('type_tax_use', '=', 'purchase')]")
    map_id = fields.Many2one('account.account.map', delegate=True)
