from odoo import api , fields , models, _

class ResCompany(models.Model):
    _inherit = 'res.company'

    accounting = fields.Boolean(string="Accounting")

class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    group_om_account_budget = fields.Boolean(string="Budget Management", 
        implied_group='equip3_accounting_masterdata.group_om_account_budget')