
from odoo import api, fields, models, _

class BankAccounAccount(models.Model):
    _name = 'bank.account.account'
    _description = "Bank Account Account"
    _rec_name = 'acc_number'

    acc_number = fields.Char(string="Account Number", required=True)
    bank_id = fields.Many2one('res.bank', string="Bank")
    bank_bic = fields.Char(string="Bank Identifier Code")
    company_id = fields.Many2one('res.company', string="Company", default=lambda self: self.env.user.company_id.id)
    branch_id = fields.Many2one('res.branch', string="Branch")