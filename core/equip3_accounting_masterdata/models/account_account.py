from odoo import api, fields, models, _
from odoo.exceptions import UserError


class SetupBarBankConfigWizard(models.TransientModel):    
    _inherit = 'account.setup.bank.manual.config'

    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}


    @api.model
    def default_get(self, default_fields):
        res = super(SetupBarBankConfigWizard, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

    def validate(self):
        res = super(SetupBarBankConfigWizard, self).validate()
        for rec in self:
            vals = {
                'acc_number': rec.acc_number,
                'bank_id': rec.bank_id.id,
                'bank_bic': rec.bank_bic,
                'company_id': rec.company_id.id,
                'branch_id': rec.branch_id.id,            
            }
            self.env['bank.account.account'].create(vals)
        return res

class AccountReconcile(models.Model):
    _inherit = "account.reconcile.model"

    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}


    @api.model
    def default_get(self, default_fields):
        res = super(AccountReconcile, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class AccountAccount(models.Model):
    _inherit = "account.account"

    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}


    @api.model
    def default_get(self, default_fields):
        res = super(AccountAccount, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class AccountTax(models.Model):
    _inherit = "account.tax" 

    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}


    @api.model
    def default_get(self, default_fields):
        res = super(AccountTax, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class AccountJournal(models.Model):
    _inherit = "account.journal"

    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}


    @api.model
    def default_get(self, default_fields):
        res = super(AccountJournal, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class AccountFiscalPosition(models.Model):
    _inherit = "account.fiscal.position"

    branch_id = fields.Many2one('res.branch', string="Branch")
        
    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}

    @api.model
    def default_get(self, default_fields):
        res = super(AccountFiscalPosition, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        for rec in self:
            if rec.env.company:
                rec.company_id = rec.env.company

        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class AccountType(models.Model):
    _inherit = "account.account.type"

    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True,
        default=lambda self: self.env.company)
    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}


    @api.model
    def default_get(self, default_fields):
        res = super(AccountType, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class AccountTaxReport(models.Model):
    _inherit = "account.tax.report"

    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True,
        default=lambda self: self.env.company)
    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}

    @api.model
    def default_get(self, default_fields):
        res = super(AccountTaxReport, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class AccountBudget(models.Model):
    _inherit = "account.budget.post"

    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}

    @api.model
    def default_get(self, default_fields):
        res = super(AccountBudget, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class AccountAssetCategory(models.Model):
    _inherit = "account.asset.category"

    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}

    @api.model
    def default_get(self, default_fields):
        res = super(AccountAssetCategory, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class AccountAnalyticLine(models.Model):
    _inherit = "account.analytic.line"

    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}

    @api.model
    def default_get(self, default_fields):
        res = super(AccountAnalyticLine, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class AccountAnalyticAccount(models.Model):
    _inherit = "account.analytic.account"

    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}


    @api.model
    def default_get(self, default_fields):
        res = super(AccountAnalyticAccount, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class AccountAnalyticGroup(models.Model):
    _inherit = "account.analytic.group"

    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}

    @api.model
    def default_get(self, default_fields):
        res = super(AccountAnalyticGroup, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        for rec in self:
            if rec.env.company:
                rec.company_id = rec.env.company
        
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class AccountAnalyticDefault(models.Model):
    _inherit = "account.analytic.default"

    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}

    @api.model
    def default_get(self, default_fields):
        res = super(AccountAnalyticDefault, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        for rec in self:
            if rec.env.company:
                rec.company_id = rec.env.company
        
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class FiscalYear(models.Model):
    _inherit = "sh.fiscal.year"

    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True,
        default=lambda self: self.env.company)
    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}

    @api.model
    def default_get(self, default_fields):
        res = super(FiscalYear, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

class FiscalYearPeriod(models.Model):
    _inherit = "sh.account.period"

    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True,
        default=lambda self: self.env.company)
    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}

    @api.model
    def default_get(self, default_fields):
        res = super(FiscalYearPeriod, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")
