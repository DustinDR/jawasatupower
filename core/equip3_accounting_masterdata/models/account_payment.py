from odoo import api, fields, models, _
from odoo.exceptions import UserError

class AccountPaymentTerm(models.Model):
    _inherit = "account.payment.term"

    branch_id = fields.Many2one('res.branch', string="Branch")

    @api.model
    def default_get(self, default_fields):
        res = super(AccountPaymentTerm, self).default_get(default_fields)
        branch_id = False

        if self._context.get('branch_id'):
            branch_id = self._context.get('branch_id')
        elif self.env.user.branch_id:
            branch_id = self.env.user.branch_id.id
        res.update({
            'branch_id' : branch_id
        })
        return res

    @api.onchange('company_id')
    def _get_domain(self):
        return {'default':{'company_id':self.env.company},'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}

    @api.onchange('branch_id')
    @api.depends('branch_id')
    def _set_company(self):
        for rec in self:
            if rec.env.company:
                rec.company_id = rec.env.company

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

                