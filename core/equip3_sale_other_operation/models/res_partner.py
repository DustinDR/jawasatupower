
from odoo import api , fields , models
from datetime import datetime, date
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT


class ResPartner(models.Model):
    _inherit = 'res.partner'

    is_customer_invoice_overdue = fields.Boolean(string="Set Customer Invoice Overdue Days ?", tracking=True)
    customer_max_invoice_overdue = fields.Float(string="Customer Max Invoice Overdue Days", tracking=True)
    is_set_customer_on_hold = fields.Boolean(string="Set Customer On Hold (Invoice Overdue)", tracking=True)
    customer_credit = fields.Boolean('Set Customer Credit Limit ?', tracking=True)

    customer_credit_limit = fields.Float('Customer Available Credit Limit', compute="_compute_customer_credit_limit", store=True, tracking=True)

    set_customer_onhold = fields.Boolean(
        'Customer on Hold if Credit Limit Exceed', tracking=True)
    cust_credit_limit = fields.Float('Customer Credit Limit')

    is_over_credit_limit = fields.Boolean(string="Over Credit Limit", compute='_compute_over_limit_matrix')
    is_invoice_overdue = fields.Boolean(string="Invoice Overdue", compute='_compute_over_limit_matrix')
    open_invoice_limit = fields.Boolean(string="Open Invoice Limit", compute='_compute_over_limit_matrix')
    
    is_open_invoice_limit = fields.Boolean(string="Set Customer Number Open Invoice Limit ?", tracking=True)
    no_open_inv_limit = fields.Float(string="Number of Open Invoices Limit", tracking=True)
    avl_open_inv_limt = fields.Float(string="Available Open Invoices Quota", tracking=True, compute='_compute_open_limit', store=True)
    customer_on_hold_open_invoice = fields.Boolean(string="Customer On Hold If Number Open Invoice Exceed")

    def get_date(self):
        return datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT)

    @api.onchange('name')
    def _onchange_name(self):
        self._compute_over_limit_matrix()
        self._compute_open_limit()

    @api.depends('invoice_ids', 'invoice_ids.amount_total', 'invoice_ids.state', 'sale_order_ids', 'sale_order_ids.amount_total', 'sale_order_ids.state', 'cust_credit_limit')
    def _compute_customer_credit_limit(self):
        for record in self:
            sale_ids = record.sale_order_ids.filtered(lambda l: l.state == 'sale' and l.invoice_status in ('to invoice', 'no'))
            amount_value = sum(sale_ids.mapped('amount_total'))
            invoice_id = record.invoice_ids.filtered(lambda l: l.payment_state in ('not_paid', 'in_payment', 'partial'))
            amount_total_value = sum(invoice_id.mapped('amount_total'))
            customer_credit = record.cust_credit_limit - ( amount_value + amount_total_value)
            record.customer_credit_limit = customer_credit

    @api.depends('invoice_ids', 'invoice_ids.amount_total', 'invoice_ids.state', 'no_open_inv_limit')
    def _compute_open_limit(self):
        for record in self:
            invoice_id = len(record.invoice_ids.filtered(lambda l: l.payment_state in ('not_paid', 'in_payment', 'partial')))
            avl_limit = record.no_open_inv_limit - invoice_id
            record.avl_open_inv_limt = avl_limit

    @api.model
    def default_get(self, fields):
        res = super(ResPartner, self).default_get(fields)
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        credit_limit =  IrConfigParam.get_param('customer_credit_limit', 1000000)
        res['cust_credit_limit'] = credit_limit
        res['customer_credit_limit'] = credit_limit
        max_invoice_overdue_days = IrConfigParam.get_param('customer_max_invoice_overdue_days', 30)
        res['customer_max_invoice_overdue'] = max_invoice_overdue_days
        customer_open_invoice_limit = IrConfigParam.get_param('customer_open_invoice_limit', 0)
        res['no_open_inv_limit'] = customer_open_invoice_limit
        return res
    
    def _compute_over_limit_matrix(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        is_over_credit_limit = IrConfigParam.get_param('is_over_credit_limit', False)
        is_invoice_overdue = IrConfigParam.get_param('is_invoice_overdue', False)
        open_invoice_limit = IrConfigParam.get_param('open_invoice_limit', False)
        for record in self:
            record.is_over_credit_limit = is_over_credit_limit
            record.is_invoice_overdue = is_invoice_overdue
            record.open_invoice_limit = open_invoice_limit
