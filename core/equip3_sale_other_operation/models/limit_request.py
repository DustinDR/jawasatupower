
from odoo import api , fields , models, _
from datetime import datetime, date


class LimitRequest(models.Model):
    _name = 'limit.request'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _description = "Create Limit Request"

    name = fields.Char(string='Sequence', readonly=True)
    partner_id = fields.Many2one('res.partner', string='Customer', tracking=True)
    limit_type = fields.Selection([('credit_limit','Credit Limit'), ('open_invoice_limit', 'Number of Open Invoices Limit'), ('max_invoice_overdue_days','Max Invoice Overdue (Days)')],
        default = "credit_limit",
        string="Limit Type", tracking=True)
    credit_amount = fields.Float("New Credit Limit Amount",store=True, tracking=True)
    last_credit_limit = fields.Float("Last Credit Limit Amount", tracking=True, compute='_compute_last_limit', store=True)
    open_inv_no = fields.Float("New Number of Open Invoices Limit",store=True, tracking=True)
    last_open_inv_no = fields.Float("Last Number of Open Invoices Limit", tracking=True, compute='_compute_last_limit', store=True)
    new_max_invoice = fields.Float("New Max Invoice Overdue (Days)",store=True, tracking=True)
    last_max_invoice = fields.Float("Last Max Invoice Overdue (Days)", tracking=True, compute='_compute_last_limit', store=True)
    description = fields.Text("Description",store=True, tracking=True)
    state = fields.Selection([("draft","Draft"),
        ("waiting_approval","Waiting For Approval"),
        ("confirmed","Request Approved"),
        ("rejected","Request Rejected")
    ], string='State', default="draft", tracking=True)
    state1 = fields.Selection(related="state", tracking=False)
    state2 = fields.Selection(related="state", tracking=False)
    company_id = fields.Many2one('res.company', string="Company", required=True, default=lambda self: self.env.company, store=True)
    branch_id = fields.Many2one('res.branch', string="Branch", required=True, domain="[('company_id', '=', company_id)]", store=True)
    limit_approval_matrix = fields.Many2one('limit.approval.matrix', string="Approval Matrix", compute="_get_limit_matrix")
    approval_ids = fields.One2many('credit.limit.approval', 'credit_id', 'Approval Lines', tracking=True)
    user_approval_ids = fields.Many2many('res.users', string="User")
    current_user = fields.Many2one('res.users')
    approval = fields.Boolean("Approval")
    approval2 = fields.Boolean(compute="_get_approval")
    new_amount = fields.Float(compute="_get_new_amount")
    last_amount = fields.Float(compute="_get_last_amount")

    @api.depends('last_amount')
    def _get_last_amount(self):
        for res in self:
            if res.limit_type == 'credit_limit':
                res.last_amount = res.last_credit_limit
            elif res.limit_type == 'open_invoice_limit':
                res.last_amount = res.last_open_inv_no
            elif res.limit_type == 'max_invoice_overdue_days':
                res.last_amount = res.last_max_invoice

    @api.depends('new_amount')
    def _get_new_amount(self):
        for res in self:
            if res.limit_type == 'credit_limit':
                res.new_amount = res.credit_amount
            elif res.limit_type == 'open_invoice_limit':
                res.new_amount = res.open_inv_no
            elif res.limit_type == 'max_invoice_overdue_days':
                res.new_amount = res.new_max_invoice

    @api.depends('approval')
    def _get_approval(self):
        for res in self:
            if self.env.user in res.user_approval_ids:
                res.approval2 = True
            else:
                res.approval2 = False

    @api.constrains('user_approval_ids','current_user')
    def _show_approval(self):
        for res in self:
            if self.env.user in res.user_approval_ids:
                res.approval = True
            else:
                res.approval = False
            
        
    def action_approve_approval(self):
        for res in self:
            rec = 0
            if res.current_user in res.user_approval_ids:
                for line in res.approval_ids:
                    if res.current_user in line.user_ids:
                        if res.current_user not in line.user_approved_ids:
                            user = []
                            user.extend(line.user_approved_ids.ids)
                            user.extend(res.current_user.ids)
                            line.user_approved_ids = [(6, 0, user)]
                            line.approval += 1
                            # if line.status:
                            # 	line.status += "\n%s: Approved - %s" % (res.current_user.name, datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"))
                            # else:
                            # 	line.status = "%s: Approved - %s" % (res.current_user.name, datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"))
                            if line.status:
                                line.status += "\n%s: Approved" % (res.current_user.name)
                            else:
                                line.status = "%s: Approved" % (res.current_user.name)
                            if line.minimum_approver <= line.approval:
                                line.approved = True
                                line.time = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
                    if line.approved:
                        rec += 1
                if len(res.approval_ids) == rec:
                    res.request_approve()

    def action_rejected(self, reason):
        for res in self:
            if res.current_user in res.user_approval_ids:
                for line in res.approval_ids:
                    if res.current_user in line.user_ids:
                        if res.current_user not in line.user_approved_ids:
                            user = []
                            user.extend(line.user_approved_ids.ids)
                            user.extend(res.current_user.ids)
                            # line.user_approved_ids = [(6, 0, user)]
                            line.approval += 1
                            # line.approved = False
                            line.update({
                                'user_approved_ids': [(6, 0, user)],
                                'approved': False
                            })
                            res.state = 'rejected'
                            line.time = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
                            # if line.status:
                            # 	line.status += "\n%s: Rejected - %s" % (res.current_user.name, line.time)
                            # else:
                            # 	line.status = "%s: Rejected - %s" % (res.current_user.name, line.time)
                            if line.status:
                                line.status += "\n%s: Rejected" % (res.current_user.name)
                            else:
                                line.status = "%s: Rejected" % (res.current_user.name)
                            line.feedback = reason

    def action_reject_approval(self):
        return {
            'type': 'ir.actions.act_window',
            'name': _('Rejected Reason'),
            'res_model': 'cancel.credit.limit',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'context': {
                'default_limit_id': self.id,
            },
        }

    def request_approve(self):
        self.ensure_one()
        self.state = 'confirmed'
        if self.limit_type == 'credit_limit':
            self.partner_id.cust_credit_limit = self.credit_amount
        if self.limit_type == 'max_invoice_overdue_days':
            self.partner_id.customer_max_invoice_overdue = self.new_max_invoice
        if self.limit_type == 'open_invoice_limit':
            self.partner_id.no_open_inv_limit = self.open_inv_no
    
    def request_approval(self):
        for res in self:
            res.state = 'waiting_approval'
            res.set_user_approval()
            
    def set_user_approval(self):
        for res in self:
            for line in res.approval_ids:
                if not line.approved:
                    res.user_approval_ids = [(6, 0, line.user_ids.ids)]
                    break

    @api.depends('branch_id', 'credit_amount', 'new_max_invoice')
    def _get_limit_matrix(self):
        for res in self:
            res.current_user = self.env.user
            if res.branch_id:
                approval_id = self.env['limit.approval.matrix'].search([('minimum_amt', '<=', res.credit_amount), ('maximum_amt', '>=', res.credit_amount), ('branch_id', '=', res.branch_id.id), ('config', '=', res.limit_type)], limit=1)
                if approval_id:
                    res.limit_approval_matrix = approval_id
                    res.create_approval(approval_id)
                else:
                    res.limit_approval_matrix = False
            else:
                res.limit_approval_matrix = False

    def create_approval(self, approval_id):
        for res in self:
            if res.state not in ("waiting_approval","confirmed","rejected"):
                approval = self.env['credit.limit.approval']
                user = []
                approval_matrix_line = []
                for line in approval_id.approver_matrix_line_ids:
                    lines = approval.create({
                        'sequence': line.sequence,
                        'user_ids': [(6, 0, line.user_name_ids.ids)],
                        'minimum_approver': line.minimum_approver,
                    })
                    approval_matrix_line.append(lines.id)
                    user.extend(line.user_name_ids.ids)
                if approval_matrix_line:
                    res.approval_ids = [(6, 0, approval_matrix_line)]
                if user:
                    res.user_approval_ids = [(6, 0, user)]

    @api.onchange('limit_type')
    def _onchange_limit_type(self):
        if self.limit_type == 'credit_limit':
            self.credit_amount = 0.0
            self.new_max_invoice = False
            self.open_inv_no = False
        if self.limit_type == 'max_invoice_overdue_days':
            self.credit_amount = False
            self.new_max_invoice = 0.0
            self.open_inv_no = False
        if self.limit_type == 'open_invoice_limit':
            self.credit_amount = False
            self.new_max_invoice = False
            self.open_inv_no = 0.0

    @api.depends('partner_id', 'limit_type')
    def _compute_last_limit(self):
        for record in self:
            record.last_credit_limit = 0
            record.last_max_invoice = 0
            record.last_open_inv_no = 0
            if record.partner_id:
                record.last_credit_limit = record.partner_id.cust_credit_limit
                record.last_max_invoice = record.partner_id.customer_max_invoice_overdue
                record.last_open_inv_no = record.partner_id.no_open_inv_limit


    def request_confirm(self):
        self.ensure_one()
        if self.limit_type == 'credit_limit' and self.credit_amount < self.last_credit_limit:
            return{
                'name': "Credit Limit",
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'limit.request.wizard',
                'target': 'new',
            }
        elif self.limit_type == 'max_invoice_overdue_days' and self.new_max_invoice < self.last_max_invoice:
            return {
                'name': "Credit Limit",
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'limit.request.wizard',
                'target': 'new',
            }
        else:
            self.state = 'confirmed'
            if self.limit_type == 'credit_limit':
                self.partner_id.cust_credit_limit = self.credit_amount
            if self.limit_type == 'max_invoice_overdue_days':
                self.partner_id.customer_max_invoice_overdue = self.new_max_invoice

    @api.model
    def create(self, vals):
        vals['name'] = "CLR/" + datetime.strftime(datetime.today(), "%y/%m/%d/") + self.env['ir.sequence'].next_by_code('limit.sequences')
        return super(LimitRequest, self).create(vals)

class CreditLimitApproval(models.Model):
    _name = "credit.limit.approval"
    
    @api.model
    def default_get(self, fields):
        res = super(CreditLimitApproval, self).default_get(fields)
        if self._context:
            context_keys = self._context.keys()
            next_sequence = 1
            if 'approval_ids' in context_keys:
                if len(self._context.get('approval_ids')) > 0:
                    next_sequence = len(self._context.get('approval_ids')) + 1
            res.update({'sequence': next_sequence})
        return res
    
    sequence = fields.Integer(string="Sequence")
    sequence2 = fields.Integer(
        string="Sequence",
        related="sequence",
        readonly=True,
        store=True,
        tracking=True
    )
    user_ids = fields.Many2many('res.users', string="User")
    minimum_approver = fields.Integer(string="Minimum Approver")
    status = fields.Text("Approval Status")
    time = fields.Char("Timestamp")
    feedback = fields.Text("Feedback")
    approved = fields.Boolean("Approved")
    approval = fields.Integer("Approval")
    user_approved_ids = fields.Many2many('res.users', string="User Approved", relation='credit_approval_credit_rel')
    credit_id = fields.Many2one('limit.request', string="Credit Limit Customer")
    
    @api.constrains('approved')
    def set_user_approval(self):
        for res in self:
            if res.approved:
                res.credit_id.set_user_approval()

class CanselCreditLimit(models.TransientModel):
    _name = 'cancel.credit.limit'
    
    limit_id = fields.Many2one('limit.request', 'Source', required=True)
    reason = fields.Text("Reason", required=True)
    
    def action_cancel_limit(self):
        self.limit_id.action_rejected(self.reason)