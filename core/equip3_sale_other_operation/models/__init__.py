
from . import res_partner
from . import res_config_settings
from . import limit_request
from . import limit_approval_matrix
from . import sale_order
from . import customer_credit_limit_wizard