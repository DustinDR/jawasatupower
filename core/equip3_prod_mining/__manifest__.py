# -*- coding: utf-8 -*-

{
    "name": "Equip 3 - Prod Mining",
    "version": "1.1.2",
    "category": "",
    "summary": "Equip 3 - Prod Mining",
    "description": """Equip 3 - Prod Mining""",
    "author": "HashMicro",
    "website": "www.hashmicro.com",
    "depends": ['project','mail', 'branch', 'equip3_manuf_operations'],
    "data": [
        'views/mining_site_control.xml',
        'views/mining_project_control.xml',
        'sequence/sequence.xml',
        'security/ir.model.access.csv',
    ],
    "qweb": [],
    "installable": True,
    "application": True,
    "auto_install": False,
}
