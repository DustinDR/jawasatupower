from odoo import models, fields, api, _


class MiningSiteControl(models.Model):
    _name = 'mining.site.control'
    _description = 'Mining Site Control'
    _inherit = 'mail.thread'
    _rec_name = 'mining_site'

    mining_site = fields.Char('Mining Site Name',tracking=True)
    site_location = fields.Char('Site Location',tracking=True)
    ppic = fields.Many2one('res.users', 'PPIC',tracking=True)
    start_work = fields.Datetime('Start Date',tracking=True)
    approval_matrix = fields.Char('Approval Matrix',tracking=True)
    analytic_group = fields.Char('Analytic Group',tracking=True)
    company = fields.Many2one('res.company', 'Company',tracking=True)
    branch = fields.Char('Branch',tracking=True)
    operating_days = fields.Datetime('Operating Days',tracking=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('approval', 'Approval'),
        ('Approved', 'approved'),
        ('reject', 'Reject'),
        ('confirmed', 'Confirmed'),
        ('progress', 'Progress'),
        ('to_close', 'To Close'),
        ('done', 'Done'),
        ('cancel', 'Cancel'),
    ], string='State',tracking=True)
    mining_site_control_ids = fields.One2many('project.project', 'mining_site_control_id', string="Mining Site Control Ids")


class Project(models.Model):
    _inherit = "project.project"

    mining_site_control_id = fields.Many2one('mining.site.control', 'Mining Site Control')

