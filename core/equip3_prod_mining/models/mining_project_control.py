from odoo import models, fields, api, _


class MiningProjectControl(models.Model):
    _name = 'mining.project.control'
    _description = 'Mining Project Control'
    _inherit = 'mail.thread'

    @api.depends('company')
    def _compute_allowed_branch(self):
        user_branch = self.env.user.branch_ids
        for record in self:
            allowed_branch = user_branch.filtered(lambda b: b.company_id == record.company)
            record.allowed_branch_ids = [(6, 0, allowed_branch.ids)]

    name = fields.Char(string='Mining Project',tracking=True)
    mining_site_id = fields.Many2one('mining.site.control', string='Mining Site',tracking=True)
    ppic = fields.Many2one('res.users', 'PPIC', tracking=True)
    start_work = fields.Datetime('Start Date', tracking=True)
    approval_matrix = fields.Many2one('mrp.approval.matrix', 'Approval Matrix', tracking=True)
    analytic_group = fields.Char('Analytic Group', tracking=True)
    company = fields.Many2one('res.company', 'Company', default=lambda self: self.env.company.id, tracking=True)
    branch = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id,
                             domain="[('id', 'in', allowed_branch_ids)]", tracking=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('to_be_approved', 'To be Approved'),
        ('approved', 'Approved'),
        ('reject', 'Rejected'),
        ('confirmed', 'Confirmed'),
        ('progress', 'In Progress'),
        ('to_close', 'To Close'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], string='State', default='draft', tracking=True)
    is_matrix_on = fields.Boolean(compute='_get_is_matrix_on', default=False)
    allowed_branch_ids = fields.One2many('res.branch', compute=_compute_allowed_branch)
    project_control_lines = fields.One2many('mining.project.control.production.line', 'project_control_id', string='Production')

    def _get_is_matrix_on(self):
        for project_conrol in self:
            project_conrol.is_matrix_on = False

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('mining.project.control') or _('New')
        return super(MiningProjectControl, self).create(vals)


class MiningProjectControlProductionLine(models.Model):
    _name = 'mining.project.control.production.line'
    _description = 'Mining Project Control Production Line'

    name = fields.Char(string='Operation')
    assets_used = fields.Char(string='Assets Used')
    duration = fields.Float(string='Duration')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('to_be_approved', 'To be Approved'),
        ('approved', 'Approved'),
        ('reject', 'Rejected'),
        ('confirmed', 'Confirmed'),
        ('progress', 'In Progress'),
        ('to_close', 'To Close'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], string='State', default='draft')
    project_control_id = fields.Many2one('mining.project.control', string='Project Control')