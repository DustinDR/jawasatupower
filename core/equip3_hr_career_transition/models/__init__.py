# -*- coding: utf-8 -*-

from . import hr_career_transition
from . import career_transition_type
from . import hr_career_transition_letter
from . import hr_career_transition_approval_matrix
from . import res_config_settings
from . import hr_contract
from . import carrer_transition_category