from typing import Sequence
from odoo import fields,models,api


class CareerTransitionApprovalMatrix(models.Model):
    _name ='hr.career.transition.approval.matrix'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Career Transition Approval Matrix"
    _order = 'create_date desc'
    name = fields.Char()
    company_id = fields.Many2one('res.company',default=lambda self:self.env.company.id)
    apply_to = fields.Selection([('by_employee','By Employee'),('by_department','By Department'),('by_job_position','By Job Position')])
    employee_ids = fields.Many2many('hr.employee')
    deparment_ids = fields.Many2many('hr.department')
    job_ids = fields.Many2many('hr.job')
    level = fields.Integer(compute="_get_level")
    career_transition_type = fields.Many2many('career.transition.type')
    approval_matrix_ids = fields.One2many('hr.career.transition.approval.matrix.line','approval_matrix_id')
    
    @api.depends('approval_matrix_ids')
    def _get_level(self):
        for record in self:
            if record.approval_matrix_ids:
                record.level = len(record.approval_matrix_ids)
            else:
                record.level = 0
                
    
    
    

    
    
class CareerTransitionApprovalMatrixline(models.Model):
    _name = 'hr.career.transition.approval.matrix.line'
    approval_matrix_id = fields.Many2one('hr.career.transition.approval.matrix')
    sequence = fields.Integer()
    approvers = fields.Many2many('res.users')
    minimum_approver = fields.Integer(default=1)
    
    @api.model
    def default_get(self, fields):
        res = super(CareerTransitionApprovalMatrixline,self).default_get(fields)
        if self.env.context:
            context_keys = self.env.context.keys()
            next_sequence = 1
            if 'approval_matrix_ids' in context_keys:
                if len(self.env.context.get('approval_matrix_ids')) > 0:
                    next_sequence = len(self.env.context.get('approval_matrix_ids')) + 1
        res.update({'sequence': next_sequence})
        return res


    
    