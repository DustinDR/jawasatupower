from typing import Sequence
from odoo import models,api,fields
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta



class Equip3CareerTransition(models.Model):
    _name = 'hr.career.transition'
    _rec_name = 'number'
    _description="Career Transition"
    _order ='create_date desc'
    _inherit = ['mail.thread', 'mail.activity.mixin']

        
    number = fields.Char()
    employee_id = fields.Many2one('hr.employee',"Employee")
    career_transition = fields.Char()
    career_transition_type = fields.Many2one('career.transition.type')
    transition_date = fields.Date()
    description = fields.Text()
    status = fields.Selection([('draft','Draft'),('to_approve','To Approve'),('approve','Approved'),('rejected','Rejected')],default="draft")
    company_id = fields.Many2one('res.company',default=lambda self:self.env.user.company_id.id ,readonly=True)
    company_address = fields.Char(related='company_id.street')
    is_hide_confirm = fields.Boolean()
    is_hide_renew = fields.Boolean(default=True)
    is_hide_reject = fields.Boolean(default=True,compute='_get_is_hide')
    is_hide_approve = fields.Boolean(default=True,compute='_get_is_hide')
    employee_number_id = fields.Char(related='employee_id.sequence_code')
    same_as_previous = fields.Boolean()
    contract_id = fields.Many2one('hr.contract')
    contract_type_id = fields.Many2one('hr.contract.type',related='contract_id.type_id')
    job_id = fields.Many2one('hr.job',related='employee_id.job_id')
    employee_grade_id = fields.Many2one('employee.grade',related='employee_id.grade_id')
    department_id = fields.Many2one('hr.department',related='employee_id.department_id')
    work_location_id = fields.Many2one('work.location.object',related='employee_id.location_id')
    new_employee_number_id = fields.Char()
    new_contract_type_id = fields.Many2one('hr.contract.type')
    new_contract_id = fields.Char() 
    new_job_id = fields.Many2one('hr.job')
    transition_type_name = fields.Char(related='career_transition_type.name')
    new_employee_grade_id = fields.Many2one('employee.grade')
    new_department_id = fields.Many2one('hr.department')
    new_work_location_id = fields.Many2one('work.location.object')
    approval_matrix_ids = fields.One2many('hr.career.transition.matrix','career_transition_id')
    is_employee_sequence_number = fields.Boolean()
    user_approval_ids = fields.Many2many('res.users',compute="_is_hide_approve")
    date_of_joining = fields.Date(related="employee_id.date_of_joining")
    years_of_service = fields.Integer(related='employee_id.years_of_service')
    months = fields.Integer(related='employee_id.months')
    days = fields.Integer(related='employee_id.days')
    year = fields.Char(related='employee_id.year',string=' ', default='year(s) -')
    month = fields.Char(related='employee_id.month',string=' ', default='month(s) -')
    day = fields.Char(related='employee_id.day',string=' ', default='day(s)')
    transition_category_domain_ids = fields.Many2many('career.transition.category',compute="_is_category")
    transition_type_domain_ids = fields.Many2many('career.transition.type',compute="_is_type")
    transition_category_id = fields.Many2one('career.transition.category')
    is_readonly_employee = fields.Boolean()
    
    
    
    
    def custom_menu(self):
        views = [(self.env.ref('equip3_hr_career_transition.hr_career_transition_tree_view').id, 'tree'),
                        (self.env.ref('equip3_hr_career_transition.hr_career_transition_form_view').id, 'form')]
        if self.env.user.has_group('equip3_hr_career_transition.career_transition_self_service') and not self.env.user.has_group('equip3_hr_career_transition.career_transition_team_approver'):
            return {
                'type': 'ir.actions.act_window',
                'name': 'Career Transition Request',
                'res_model': 'hr.career.transition',
                'view_mode': 'tree,form',
                'domain': [('employee_id.user_id', '=', self.env.user.id)],
                'context':{'search_default_career_transition_type_group_by': 1},
                'views':views,
                'help':"""<p class="oe_view_nocontent_create">
                    There is no examples click here to add new Career Transition Request.
                </p>"""
                
            }
        else:
            return {
                'type': 'ir.actions.act_window',
                'name': 'Career Transition Request',
                'res_model': 'hr.career.transition',
                'view_mode': 'tree,form',
                'context':{'search_default_career_transition_type_group_by': 1},
                'views':views,
                'help':"""<p class="oe_view_nocontent_create">
                    There is no examples click here to add new Career Transition Request.
                </p>"""
                
            }

    
    
    
    
    @api.depends('company_id')
    def _is_category(self):
        for record in self:
            ids = []
            if self.env.user.has_group('equip3_hr_career_transition.career_transition_self_service') and not self.env.user.has_group('equip3_hr_career_transition.career_transition_team_approver'):
                group_ids = self.env['career.transition.category'].search([]).filtered(lambda line: self.env.ref('equip3_hr_career_transition.career_transition_self_service').id in line.group_ids.ids)
            if self.env.user.has_group('equip3_hr_career_transition.career_transition_team_approver') and not self.env.user.has_group('equip3_hr_career_transition.career_transition_all_approver'):
                group_ids = self.env['career.transition.category'].search([]).filtered(lambda line: self.env.ref('equip3_hr_career_transition.career_transition_team_approver').id in line.group_ids.ids)
            if self.env.user.has_group('equip3_hr_career_transition.career_transition_all_approver') and not self.env.user.has_group('equip3_hr_career_transition.career_transition_administrator'):
                group_ids = self.env['career.transition.category'].search([]).filtered(lambda line: self.env.ref('equip3_hr_career_transition.career_transition_all_approver').id in line.group_ids.ids)
            if  self.env.user.has_group('equip3_hr_career_transition.career_transition_administrator'):
                group_ids = self.env['career.transition.category'].search([]).filtered(lambda line: self.env.ref('equip3_hr_career_transition.career_transition_administrator').id in line.group_ids.ids) 
            if group_ids:
                data_id = [data.id for data in group_ids]
                ids.extend(data_id)
                record.transition_category_domain_ids = [(6,0,ids)]
            else:
                record.transition_category_domain_ids = False
                
                  
    @api.depends('company_id')
    def _is_type(self):
        for record in self:
            ids = []
            if self.env.user.has_group('equip3_hr_career_transition.career_transition_self_service') and not self.env.user.has_group('equip3_hr_career_transition.career_transition_team_approver'):
                group_ids = self.env['career.transition.type'].search([]).filtered(lambda line: self.env.ref('equip3_hr_career_transition.career_transition_self_service').id in line.group_ids.ids)
            if self.env.user.has_group('equip3_hr_career_transition.career_transition_team_approver') and not self.env.user.has_group('equip3_hr_career_transition.career_transition_all_approver'):
                group_ids = self.env['career.transition.type'].search([]).filtered(lambda line: self.env.ref('equip3_hr_career_transition.career_transition_team_approver').id in line.group_ids.ids)
            if self.env.user.has_group('equip3_hr_career_transition.career_transition_all_approver') and not self.env.user.has_group('equip3_hr_career_transition.career_transition_administrator'):
                group_ids = self.env['career.transition.type'].search([]).filtered(lambda line: self.env.ref('equip3_hr_career_transition.career_transition_all_approver').id in line.group_ids.ids)
            if  self.env.user.has_group('equip3_hr_career_transition.career_transition_administrator'):
                group_ids = self.env['career.transition.type'].search([]).filtered(lambda line: self.env.ref('equip3_hr_career_transition.career_transition_administrator').id in line.group_ids.ids) 
            if group_ids:
                data_id = [data.id for data in group_ids]
                ids.extend(data_id)
                record.transition_type_domain_ids = [(6,0,ids)]
            else:
                record.transition_type_domain_ids = False  
            
       
    
    @api.onchange('transition_category_id')
    def _onchange_transition_category_id(self):
        for record in self:
            if record.transition_category_id:
                record.career_transition = str(record.transition_category_id.name).lower()
    
    
    def unlink(self):
        for record in self:
            if record.status != "draft":
                raise ValidationError("Only career transition type status draft can be deleted")
        res =  super(Equip3CareerTransition, self).unlink()
        return res
    
  
    
    
    @api.depends('approval_matrix_ids')
    def _is_hide_approve(self):
        for record in self:
            if record.approval_matrix_ids:
                sequence = [data.sequence for data in record.approval_matrix_ids.filtered(lambda line:  len(line.approver_confirm.ids) != line.minimum_approver )]
                if sequence:
                    minimum_sequence = min(sequence)
                    approve_user= record.approval_matrix_ids.filtered(lambda line: self.env.user.id in line.approver_id.ids and self.env.user.id not in  line.approver_confirm.ids and line.sequence == minimum_sequence)
                    if approve_user:
                        record.user_approval_ids = [(6,0,[self.env.user.id])]
                    else:
                        record.user_approval_ids = False
                else:
                    record.user_approval_ids = False
            else:
                    record.user_approval_ids = False
    
    @api.depends('user_approval_ids')
    def _get_is_hide(self):
        for record in self:
            if not record.user_approval_ids:
                record.is_hide_approve = True
                record.is_hide_reject = True
            else:
                record.is_hide_approve = False
                record.is_hide_reject = False
                
                    
                        
    
    def print_on_page(self):
        for record in self:
            transition_date = datetime.strptime(str(record.transition_date), "%Y-%m-%d")
            transition_date_string = datetime(transition_date.year, transition_date.month,
                                                transition_date.day).strftime("%d %B %Y")
            create_date = datetime.strptime(str(record.create_date), "%Y-%m-%d %H:%M:%S.%f")
            create_date_string = datetime(create_date.year, create_date.month,
                                                create_date.day).strftime("%d %B %Y")
            if not record.career_transition_type.letter_id:
                raise ValidationError("Letter not set in transition type")        
            temp = record.career_transition_type.letter_id.letter_content
            letter_content_replace = record.career_transition_type.letter_id.letter_content
            if "$(company_id)" in letter_content_replace:
                if not record.company_id:
                    raise ValidationError("Company is empty")
                letter_content_replace = str(letter_content_replace).replace("$(company_id)", record.company_id.name)
            if "$(company_address)" in letter_content_replace:
                if not record.company_address:
                    raise ValidationError("Company address is empty")
                letter_content_replace = str(letter_content_replace).replace("$(company_address)", record.company_address)
            if "$(number)" in letter_content_replace:
                if not record.number:
                    raise ValidationError("Number is empty")               
                letter_content_replace = str(letter_content_replace).replace("$(number)", record.number)
            if "$(employee_id)" in letter_content_replace:
                if not record.employee_id:
                    raise ValidationError("Employee is empty")
                letter_content_replace = str(letter_content_replace).replace("$(employee_id)", record.employee_id.name)
            if "$(employee_number_id)" in letter_content_replace:
                if not record.employee_number_id:
                    raise ValidationError("Employee number is empty")
                letter_content_replace = str(letter_content_replace).replace("$(employee_number_id)", record.employee_number_id)
            if "$(job_id)" in letter_content_replace:
                if not record.job_id:
                    raise ValidationError("Job is empty")
                letter_content_replace = str(letter_content_replace).replace("$(job_id)", record.job_id.name)
            if "$(department_id)" in letter_content_replace:
                if not record.department_id:
                    raise ValidationError("Department is empty")
                letter_content_replace = str(letter_content_replace).replace("$(department_id)", record.department_id.name)
            if "$(work_location_id)" in letter_content_replace:
                if not record.work_location_id:
                    raise ValidationError("Work Location is empty")
                letter_content_replace = str(letter_content_replace).replace("$(work_location_id)", record.work_location_id.name)
            if "$(new_job_id)" in letter_content_replace:
                if not record.new_job_id:
                    raise ValidationError("New job is empty")
                letter_content_replace = str(letter_content_replace).replace("$(new_job_id)", record.new_job_id.name)
            if "$(new_department_id)" in letter_content_replace:
                if not record.new_department_id:
                    raise ValidationError("New department is empty")
                letter_content_replace = str(letter_content_replace).replace("$(new_department_id)", record.new_department_id.name)
            if "$(new_work_location_id)" in letter_content_replace:
                if not record.new_work_location_id:
                    raise ValidationError("New Work Location is empty")
                letter_content_replace = str(letter_content_replace).replace("$(new_work_location_id)", record.new_work_location_id.name)
            if "$(transition_date)" in letter_content_replace:
                if not record.transition_date:
                    raise ValidationError("Transition date is empty")
                letter_content_replace = str(letter_content_replace).replace("$(transition_date)", transition_date_string)
            if "$(create_date)" in letter_content_replace:
                letter_content_replace = str(letter_content_replace).replace("$(create_date)", create_date_string)
            if "$(new_contract_type_id)" in letter_content_replace:
                letter_content_replace = str(letter_content_replace).replace("$(new_contract_type_id)", record.new_contract_type_id.name)
            if "$(contract_id)" in letter_content_replace:
                if not record.contract_id:
                    raise ValidationError("Contract is empty")
                letter_content_replace = str(letter_content_replace).replace("$(contract_id)", record.contract_id.name)
            if "$(new_contract_id)" in letter_content_replace:
                letter_content_replace = str(letter_content_replace).replace("$(new_contract_id)", record.new_contract_id)
            if "$(employee_grade_id)" in letter_content_replace:
                if not record.new_employee_grade_id:
                    raise ValidationError("Employee grade empty")
                letter_content_replace = str(letter_content_replace).replace("$(employee_grade_id)", record.employee_grade_id.name)
            if "$(new_employee_number_id)" in letter_content_replace:
                letter_content_replace = str(letter_content_replace).replace("$(new_employee_number_id)", record.new_employee_number_id)
            if "$(new_employee_grade_id)" in letter_content_replace:
                if not record.new_employee_grade_id:
                    raise ValidationError("New employee grade empty")
                letter_content_replace = str(letter_content_replace).replace("$(new_employee_grade_id)", record.new_employee_grade_id.name)
            record.career_transition_type.letter_id.letter_content = letter_content_replace
            data = record.career_transition_type.letter_id.letter_content
            record.career_transition_type.letter_id.letter_content = temp
            
            return data
    
    
    @api.onchange('transition_category_id')
    def _onchange_career_transition(self):
        for record in self:
            if record.transition_category_id:
                if record.career_transition_type:
                    if record.career_transition_type.career_transition_category_id.id != record.transition_category_id.id:
                        record.career_transition_type = False
        
    
    @api.model
    def default_get(self, fields):
        res = super(Equip3CareerTransition, self).default_get(fields)
        emp_seq = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_masterdata_employee.emp_seq')
        if emp_seq:
            res['is_employee_sequence_number'] = emp_seq
            
        if self.env.user.has_group('equip3_hr_career_transition.career_transition_self_service') and not self.env.user.has_group('equip3_hr_career_transition.career_transition_team_approver'):
            employee_id = self.env['hr.employee'].search([('user_id','=',self.env.user.id)],limit=1)
            if not employee_id:
                raise ValidationError(f"Employee not set for User {self.env.user.name}")
            res['employee_id']= employee_id.id
            res['is_readonly_employee']= True
        
        return res
    
    @api.model
    def create(self,values):
        res= super(Equip3CareerTransition, self).create(values)
        sequence = self.env['ir.sequence'].search([('code','=',res._name)])
        if not sequence:
            raise ValidationError("Sequence for Transition not found")
        now = datetime.now()
        split_sequence = str(sequence.next_by_id()).split('/')
        transition_number = F"CTR/{split_sequence[0]}/{now.month}/{now.day}/{split_sequence[1]}"
        res.number = transition_number
        return res
    
    
    
    @api.onchange('employee_id','career_transition_type')
    def _onchange_employee_id(self):
        for record in self:
            setting = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_career_transition.type_approval')
            if record.employee_id and record.career_transition_type:
                contract = self.env['hr.contract'].search([('employee_id','=',record.employee_id.id),('state','=','open')],limit=1)
                if contract:
                    record.contract_id = contract.id
                record.new_employee_number_id = False
                record.new_contract_type_id = False
                record.new_job_id = False
                record.new_employee_grade_id = False
                record.new_department_id = False
                record.new_work_location_id = False
                record.same_as_previous = False
                
                record.same_as_previous = True
                record.new_employee_number_id = record.employee_number_id
                record.new_contract_type_id = record.contract_type_id
                record.new_job_id = record.job_id
                record.new_employee_grade_id = record.employee_grade_id
                record.new_department_id = record.department_id
                record.new_work_location_id = record.work_location_id
                if record.approval_matrix_ids:
                    remove = []
                    for line in record.approval_matrix_ids:
                        remove.append((2,line.id))
                    record.approval_matrix_ids =  remove
                if setting == 'employee_hierarchy':
                    record.approval_matrix_ids = self.approval_by_hierarchy(record)
                else:
                    self.approval_by_matrix(record)
    
    
    def approval_by_matrix(self,record):
        approval_matrix = self.env['hr.career.transition.approval.matrix'].search([('apply_to','=','by_employee')])
        matrix = approval_matrix.filtered(lambda line: record.employee_id.id in line.employee_ids.ids and record.career_transition_type.id  in line.career_transition_type.ids )
    
        if matrix:
            data_approvers = []
            for line in matrix[0].approval_matrix_ids:
                data_approvers.append((0,0,{'sequence':line.sequence,'minimum_approver':line.minimum_approver,'approver_id':[(6,0,line.approvers.ids)]}))
            record.approval_matrix_ids = data_approvers
        if not matrix:
            data_approvers = []
            approval_matrix = self.env['hr.career.transition.approval.matrix'].search([('apply_to','=','by_job_position')])
            matrix = approval_matrix.filtered(lambda line: record.job_id.id in line.job_ids.ids and record.career_transition_type.id  in line.career_transition_type.ids)
            if matrix:   
                for line in matrix[0].approval_matrix_ids:
                    data_approvers.append((0,0,{'sequence':line.sequence,'minimum_approver':line.minimum_approver,'approver_id':[(6,0,line.approvers.ids)]}))
                record.approval_matrix_ids = data_approvers
            if not matrix:
                data_approvers = []
                approval_matrix = self.env['hr.career.transition.approval.matrix'].search([('apply_to','=','by_department')])
                matrix = approval_matrix.filtered(lambda line: record.department_id.id in line.deparment_ids.ids and record.career_transition_type.id  in line.career_transition_type.ids)
                if matrix:
                    for line in matrix[0].approval_matrix_ids:
                        data_approvers.append((0,0,{'sequence':line.sequence,'minimum_approver':line.minimum_approver,'approver_id':[(6,0,line.approvers.ids)]}))
                    record.approval_matrix_ids = data_approvers
            
    def approval_by_hierarchy(self,record):
        approval_ids = []
        seq = 1
        data = 0
        line = self.get_manager(record,record.employee_id,data,approval_ids,seq)
        return line
        
        
    def get_manager(self,record,employee_manager,data,approval_ids,seq):
        setting_level = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_career_transition.level')
        if not setting_level:
            raise ValidationError("level not set")
        if not employee_manager['parent_id']['user_id']:
                return approval_ids
        while data < int(setting_level):
            approval_ids.append( (0,0,{'sequence':seq,'approver_id':[(4,employee_manager['parent_id']['user_id']['id'])]}))
            data += 1
            seq +=1
            if employee_manager['parent_id']['user_id']['id']:
                self.get_manager(record,employee_manager['parent_id'],data,approval_ids,seq)
                break
        
        return approval_ids
        
        
           
                
    def renew_contract(self):
        return {
        'type': 'ir.actions.act_window',
        'name': 'Contract',
        'res_model': 'hr.contract',
        'view_type': 'form',
        'view_id': False,
        'target':'new',
        'view_mode': 'form',
        'context':{'default_name':self.new_contract_id,
                   'default_employee_id':self.employee_id.id,
                   'default_type_id':self.new_contract_type_id.id,
                   'default_job_id':self.new_job_id.id,
                   'default_department_id':self.new_department_id.id,
                   'default_work_location_id':self.new_work_location_id.id,
                   'default_career_transition_id':self.id,
                   'default_date_start':self.transition_date,
                   },
        }         
            
            
            
            
        
                       
                    
    @api.onchange('same_as_previous')
    def _onchange_same_as_previous(self):
        for record in self:
            if record.same_as_previous:
                record.new_employee_number_id = record.employee_number_id
                record.new_contract_type_id = record.contract_type_id
                record.new_job_id = record.job_id
                record.new_employee_grade_id = record.employee_grade_id
                record.new_department_id = record.department_id
                record.new_work_location_id = record.work_location_id
            else:
                record.new_employee_number_id = False
                record.new_contract_type_id = False
                record.new_job_id = False
                record.new_employee_grade_id = False
                record.new_department_id = False
                record.new_work_location_id = False
    
    
    def set_confirm(self):
        for record in self:
            if not record.approval_matrix_ids:
                record.status = "approve"
                record.is_hide_confirm = True
                if record.contract_id:
                    transition_date = datetime.strptime(str(record.transition_date), "%Y-%m-%d") + timedelta(days=-1)
                    record.contract_id.date_end = transition_date 
            else:
                record.status = "to_approve"
                record.is_hide_confirm = True
                record.is_hide_reject = False
   
    def approve(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'career.transition.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'name':"Confirmation Message",
            'target': 'new',
            'context':{'default_transition_id':self.id,'default_state':'approve'},
        }
    
    def reject(self):
         return {
            'type': 'ir.actions.act_window',
            'res_model': 'career.transition.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'name':"Confirmation Message",
            'target': 'new',
            'context':{'default_transition_id':self.id,'default_state':'rejected'},
        }
    
    
class Equip3CareerTransitionApprovalMatrix(models.Model):
    _name = 'hr.career.transition.matrix'
    _description="Career Transition Approval Matrix"
    career_transition_id = fields.Many2one('hr.career.transition')
    sequence = fields.Integer()
    approver_id = fields.Many2many('res.users',string="Approvers")
    approver_confirm = fields.Many2many('res.users','matrix_line_user_approve_ids','user_id',string="Approvers confirm")
    approval_status = fields.Text()
    timestamp = fields.Text()
    feedback = fields.Text()
    minimum_approver = fields.Integer(default=1)


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    transition_line_ids = fields.One2many('employee.transition', 'career_employee_id', string="Transition History")

    def _compute_contracts_count(self):
        super(HrEmployee, self)._compute_contracts_count()
        self._compute_transisiton_list()

    def _compute_transisiton_list(self):
        for transition in self:
            if transition.transition_line_ids:
                transition.transition_line_ids = False
            transition_data = self.env['hr.career.transition'].search(
                [('employee_id', 'in', transition.ids), ('status', '=', 'approve')], order="id desc")
            for tra_data in transition_data:
                print (tra_data, 'tra_datatra_datatra_datatra_data')
                transition.transition_line_ids = [
                    (0, 0, {'emp_transition_id': tra_data.id, 'career_employee_id': transition.id})]



class EmployeeCarrerTransition(models.Model):
    _name = 'employee.transition'
    _description = 'Career Transition History'

    career_employee_id = fields.Many2one('hr.employee', string="Employee")
    emp_transition_id = fields.Many2one('hr.career.transition', string="Career Transition")
    transition_category_id = fields.Many2one('career.transition.category',
                                             related='emp_transition_id.transition_category_id')
    career_transition_type = fields.Many2one('career.transition.type',
                                             related='emp_transition_id.career_transition_type')
    transition_date = fields.Date(related='emp_transition_id.transition_date')
    description = fields.Text(related='emp_transition_id.description')
    company_id = fields.Many2one('res.company', related='emp_transition_id.company_id')
