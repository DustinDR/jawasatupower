from odoo import api, fields, models
from odoo.exceptions import ValidationError


class   Eequip3HrCareerTransitionResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'
    type_approval = fields.Selection([('employee_hierarchy','By Employee Hierarchy'),('approval_matrix','By Approval Matrix')],
                                     config_parameter='equip3_hr_career_transition.type_approval', default='employee_hierarchy')
    level = fields.Integer(config_parameter='equip3_hr_career_transition.level', default=1)

    @api.onchange("level")
    def _onchange_level(self):
        if self.level < 1:
            self.level = 1
    
    
    
    