# -*- coding: utf-8 -*-
{
    'name': "Accounting Budget",

    'summary': """
        """,

    'description': """
        Accounting Budget
    """,

    'author': "Hashmicro/Febri Zummiati",
    'website': "https://www.hashmmicro.com",
    'category': 'accounting',
    'version': '1.1.2',
    'depends': ['account', 'equip3_accounting_masterdata', 'equip3_accounting_operation'],
    'data': [
        'security/ir.model.access.csv',
        'views/budget_change_request_views.xml',
        'views/account_budget_views.xml',
        'wizard/account_budget_reject.xml',
        'wizard/account_budget_request_reject.xml',
    ],
    'demo': [],
    'auto_install': False,
    'installable': True,
    'application': True
}
