# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
import pytz
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class BudgetChangeReq(models.Model):
    _name = 'budget.change.req'
    _description = 'Budget Change Request'
    _inherit = ['mail.thread']

    requested_id = fields.Many2one('res.users', string='Requester', required=True)
    budget_std_id = fields.Many2one('crossovered.budget', string='Budget', required=True)
    date = fields.Date(string='Date', required=True)
    budget_line = fields.One2many('budget.change.req.line', 'budget_change_req_id', string="Budget Line")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('to_approve', 'Waiting For Approval'),
        ('rejected', 'Rejected'),
        ('approved', 'Approved')
    ], 'Status', default='draft', index=True, required=True, readonly=True, copy=False, tracking=True)
    approval_matrix = fields.Many2one('approval.matrix.accounting', string="Approval Matrix",
                                      compute='_get_approval_matrix')

    is_allowed_to_approval_matrix = fields.Boolean(string="Is Allowed Approval Matrix",
                                                   compute='_get_approve_status_from_config')
    approved_matrix_ids = fields.One2many('approval.matrix.accounting.lines', 'account_budget_request_id',
                                          string="Approved Matrix")
    approval_matrix_line_id = fields.Many2one('approval.matrix.accounting.lines', string='Approval Matrix Line',
                                              compute='_get_approve_button', store=False)
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button', store=False)

    @api.depends('budget_std_id','requested_id')
    def _get_approval_matrix(self):
        self._get_approve_status_from_config()
        for record in self:
            matrix_id = False
            matrix_id = self.env['approval.matrix.accounting'].search([
                  ('company_id', '=', record.budget_std_id.company_id.id),
                 ('branch_id', '=', record.budget_std_id.branch_id.id),
                ('approval_matrix_type', '=', 'budget_change_request_approval')
            ], limit=1)
            record.approval_matrix = matrix_id
            record._compute_approving_matrix_lines()

    @api.depends('is_allowed_to_approval_matrix')
    def _get_approve_status_from_config(self):
        for record in self:
            res_user = self.env['res.users'].search([('id', '=', self._uid)])
            if res_user.has_group('equip3_accounting_masterdata.group_om_account_budget'):
                record.is_allowed_to_approval_matrix = True

    def _get_approve_button(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved),
                                 key=lambda r: r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False

    def action_approve(self):
        for record in self:
            user = self.env.user
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_ids.ids and \
                        user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    utc_datetime = datetime.now()
                    local_timezone = pytz.timezone(self.env.user.tz)
                    local_datetime = utc_datetime.replace(tzinfo=pytz.utc)
                    local_datetime = local_datetime.astimezone(local_timezone).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                    if name != '':
                        name += "\n • %s: Approved - %s" % (self.env.user.name, local_datetime)
                    else:
                        name += "• %s: Approved - %s" % (self.env.user.name, local_datetime)

                    approval_matrix_line_id.write({
                        'last_approved': self.env.user.id, 'state_char': name,
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r: r.approved)):
                record.action_for_approved()

    def action_request_for_approval(self):
        for record in self:
            record.write({'state': 'to_approve'})

    def action_for_approved(self):
        for record in self:
            record.write({'state': 'approved'})



    def action_reject(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Account Budget Request Change',
            'res_model': 'budget.change.req.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
        }

    @api.onchange('budget_std_id')
    def _onchange_budget_std_id(self):
        budget_line_list = [(5, 0, 0)]
        for req in self:
            for line in req.budget_std_id.crossovered_budget_line:
                budget_line_list.append((0, 0, {'budgetary_position_id': line.general_budget_id.id,
                                                'planned_amount': line.planned_amount,
                                                }))
            req.budget_line = budget_line_list

    @api.onchange('approval_matrix')
    def _compute_approving_matrix_lines(self):
        data = [(5, 0, 0)]
        for record in self:
            if record.state == 'draft' and record.is_allowed_to_approval_matrix:
                record.approved_matrix_ids = []
                counter = 1
                record.approved_matrix_ids = []
                for rec in record.approval_matrix:
                    for line in rec.approval_matrix_line_ids:
                        data.append((0, 0, {
                            'sequence': counter,
                            'user_ids': [(6, 0, line.user_ids.ids)],
                            'minimum_approver': line.minimum_approver,
                        }))
                        counter += 1
                record.approved_matrix_ids = data

    @api.model
    def create(self, vals):
        result = super(BudgetChangeReq, self).create(vals)
        for req in result:
            for line in req.budget_line:
                crossovered_budget_lines = self.env['crossovered.budget.lines'].search(
                    [('general_budget_id', '=', line.budgetary_position_id.id),
                     ('crossovered_budget_id', '=', req.budget_std_id.id)])
                for budget_line in crossovered_budget_lines:
                    budget_line.write({'planned_amount': line.new_amount})

        return result

    def write(self, vals):
        result = super(BudgetChangeReq, self).write(vals)
        for req in self:
            for line in req.budget_line:
                crossovered_budget_lines = self.env['crossovered.budget.lines'].search(
                    [('general_budget_id', '=', line.budgetary_position_id.id),
                     ('crossovered_budget_id', '=', req.budget_std_id.id)])
                for budget_line in crossovered_budget_lines:
                    budget_line.write({'planned_amount': line.new_amount})
        return result


class BudgetChangeReqLine(models.Model):
    _name = 'budget.change.req.line'
    _description = 'Budget Change Request Line'

    budget_change_req_id = fields.Many2one('budget.change.req', string='Budget Change Request')
    budgetary_position_id = fields.Many2one('account.budget.post', 'Budgetary Position ID', required=True)
    planned_amount = fields.Float('Current Planned Amount')
    new_amount = fields.Float('New Planned Amount', required=True)


class ApprovalMatrixAccountingLines(models.Model):
    _inherit = "approval.matrix.accounting.lines"

    account_budget_request_id = fields.Many2one('budget.change.req', string='Account Request Budget')
