# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Equip3 Probation Extend',
    'version': '1.1.1',
    'author': 'Hashmicro / Kumar',
    'website': "https://www.hashmicro.com",
    'category': 'Human Resources/Employee Probation',
    'summary': """
    Added some new fields and overrided the list view.
    """,
    'depends': ['dev_employee_probation'],
    'data': [
        'views/employee.xml',
        'views/probation.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
