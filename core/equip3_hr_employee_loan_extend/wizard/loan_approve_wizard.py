from odoo import fields, models, api


class HrLoanWizard(models.TransientModel):
    _name = 'hr.loan.wizard'

    feedback = fields.Text()

    def submit(self):
        """ Prepare the Loan feedback and trigger Approve. """
        self.ensure_one()
        hr_loan = self.env['employee.loan.details'].browse(self._context.get('active_ids', []))
        hr_loan.feedback_parent = self.feedback
        hr_loan.action_approved()


class HrLoanCancelWizard(models.TransientModel):
    _name = 'hr.loan.cancel.wizard'

    feedback = fields.Text()

    def submit(self):
        """ Prepare the Loan feedback and trigger Approve. """
        self.ensure_one()
        hr_loan = self.env['employee.loan.cancelation'].browse(self._context.get('active_ids', []))
        hr_loan.feedback_parent = self.feedback
        hr_loan.action_approved()


class HrFullLoanWizard(models.TransientModel):
    _name = 'hr.full.loan.wizard'

    feedback = fields.Text()

    def submit(self):
        """ Prepare the Loan feedback and trigger Approve. """
        self.ensure_one()
        hr_full_loan = self.env['hr.full.loan.payment'].browse(self._context.get('active_ids', []))
        hr_full_loan.feedback_parent = self.feedback
        hr_full_loan.action_approved()