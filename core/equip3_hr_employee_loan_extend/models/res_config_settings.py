from odoo import api, fields, models


class LoanResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    loan_type_approval = fields.Selection(
        [('employee_hierarchy', 'By Employee Hierarchy'), ('approval_matrix', 'By Approval Matrix')], default='employee_hierarchy',
        config_parameter='equip3_hr_employee_loan_extend.loan_type_approval')
    loan_level = fields.Integer(config_parameter='equip3_hr_employee_loan_extend.loan_level', default=1)
    send_by_wa_loan = fields.Boolean(config_parameter='equip3_hr_employee_loan_extend.send_by_wa_loan')
    send_by_email_loan = fields.Boolean(config_parameter='equip3_hr_employee_loan_extend.send_by_email_loan', default=True)

    @api.onchange("loan_level")
    def _onchange_loan_level(self):
        if self.loan_level < 1:
            self.loan_level = 1