from odoo import api, fields, models

class LoanType(models.Model):
    _inherit = 'loan.type'

    apply_to = fields.Selection([
        ('employee_categories', 'By Employee Categorie'),
        ('employees', 'By Employees'),
        ('years_of_service', 'By Years of Services')
    ], string="Apply To", required=True)
    years_of_service = fields.Integer("Years of Service")
    months_of_service = fields.Integer("Months of Service")
    days_of_service = fields.Integer("Days of Service")
    payment_method = fields.Selection(selection_add=[('payroll', 'Payroll')], ondelete={'payroll': 'cascade'})
    deduction_based_period = fields.Selection([
        ('date_from', 'Date From'),
        ('date_to', 'Date To')
    ], string="Deduction Based On Period")