# -*- coding: utf-8 -*-
from odoo import api, fields, models, _, tools
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, timedelta
from pytz import timezone
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, float_compare
import time
from dateutil.relativedelta import relativedelta
from lxml import etree
import requests
headers = {'content-type': 'application/json'}

class EmployeeLoanDetails(models.Model):
    _inherit = "employee.loan.details"

    loan_approver_user_ids = fields.One2many('loan.approver.user', 'emp_loan_id', string='Approver')
    approvers_ids = fields.Many2many('res.users', 'emp_loan_approvers_rel', string='Approvers List')
    approved_user_ids = fields.Many2many('res.users', string='Approved by User')
    is_approver = fields.Boolean(string="Is Approver", compute='_compute_can_approve')
    approved_user_text = fields.Text(string="Approved User", tracking=True)
    approved_user = fields.Text(string="Approved User", tracking=True)
    job_id = fields.Many2one('hr.job', related='employee_id.job_id')
    department_id = fields.Many2one('hr.department', related='employee_id.department_id')
    feedback_parent = fields.Text(string='Parent Feedback')
    #insallment line items
    can_approve_installment = fields.Boolean(compute='_compute_can_approve_installment', string="Installment Approvers")
    is_readonly = fields.Boolean(compute='_compute_readonly')
    domain_employee_ids = fields.Many2many('hr.employee', string="Employee Domain", compute='_compute_employee_ids')
    
    
    @api.model
    def fields_view_get(self, view_id=None, view_type=None,
                        toolbar=False, submenu=False):
        res = super(EmployeeLoanDetails, self).fields_view_get(
            view_id=view_id, view_type=view_type)
        if self.env.context.get('search_default_toapprove'):
            if self.env.user.has_group('equip3_hr_employee_loan_extend.group_loan_finance'):
                root = etree.fromstring(res['arch'])
                root.set('create', 'true')
                root.set('edit', 'true')
                root.set('delete', 'true')
                res['arch'] = etree.tostring(root)
            else :
                root = etree.fromstring(res['arch'])
                root.set('create', 'false')
                root.set('edit', 'false')
                root.set('delete', 'false')
                res['arch'] = etree.tostring(root)
        return res
        
    
    
    def custom_menu(self):
            # views = [(self.env.ref('bi_employee_travel_managment.view_travel_req_tree').id, 'tree'),
        #              (self.env.ref('bi_employee_travel_managment.view_travel_req_form').id, 'form')]
        search_view_id = self.env.ref("hr_employee_loan.view_loan_filter")
        if self.env.user.has_group('equip3_hr_employee_loan_extend.group_loan_self_service') and not self.env.user.has_group('equip3_hr_employee_loan_extend.group_loan_supervisor'):
            return {
                'type': 'ir.actions.act_window',
                'name': 'Loan Requests',
                'res_model': 'employee.loan.details',
                'target': 'current',
                'view_mode': 'tree,form,calendar,graph',
                # 'views':views,
                'domain': [('employee_id.user_id', '=', self.env.user.id)],
                # 'context': {'default_holiday_type': 'company'},
                'help': """<p class="o_view_nocontent_smiling_face">
                    Create New Loan
                </p>""",
                'context':{},
                'search_view_id': search_view_id.id,

            }
        elif self.env.user.has_group(
                'equip3_hr_employee_loan_extend.group_loan_supervisor') and not self.env.user.has_group(
                'equip3_hr_employee_loan_extend.group_loan_finance'):
            employee_ids = []
            my_employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)])
            if my_employee:
                for child_record in my_employee.child_ids:
                    employee_ids.append(my_employee.id)
                    employee_ids.append(child_record.id)
                    child_record._get_amployee_hierarchy(employee_ids, child_record.child_ids, my_employee.id)
                return {
                    'type': 'ir.actions.act_window',
                    'name': 'Loan Requests',
                    'res_model': 'employee.loan.details',
                    'target': 'current',
                    'view_mode': 'tree,form,calendar,graph',
                    # 'views':views,
                    'domain': [('employee_id', 'in', employee_ids)],
                    'context': {},
                    'help': """<p class="o_view_nocontent_smiling_face">
                    Create New Loan
                    </p>""",
                    'search_view_id': search_view_id.id,

                }

        else:
            return {
                'type': 'ir.actions.act_window',
                'name': 'Loan Requests',
                'res_model': 'employee.loan.details',
                'target': 'current',
                'view_mode': 'tree,form,calendar,graph',
                'domain': [],
                'context': {},
                'help': """<p class="o_view_nocontent_smiling_face">
                Create New Loan
                </p>""",
                'search_view_id': search_view_id.id,
              
            }
    
    
    
    @api.depends('employee_id')
    def _compute_readonly(self):
        for record in self:
            if self.env.user.has_group('equip3_hr_employee_loan_extend.group_loan_self_service') and not self.env.user.has_group('equip3_hr_employee_loan_extend.group_loan_supervisor'):
                record.is_readonly = True
            else:
                record.is_readonly = False


    @api.depends('employee_id')
    def _compute_employee_ids(self):
        for record in self:
            employee_ids = []
            if self.env.user.has_group('equip3_hr_employee_loan_extend.group_loan_supervisor') and not self.env.user.has_group('equip3_hr_employee_loan_extend.group_loan_finance'):
                my_employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)])
                if my_employee:
                    for child_record in my_employee.child_ids:
                        employee_ids.append(my_employee.id)
                        employee_ids.append(child_record.id)
                        child_record._get_amployee_hierarchy(employee_ids, child_record.child_ids, my_employee.id)
                record.domain_employee_ids = [(6, 0, employee_ids)]
            else:
                all_employee = self.env['hr.employee'].sudo().search([])
                for data_employee in all_employee:
                    employee_ids.append(data_employee.id)
                record.domain_employee_ids = [(6, 0, employee_ids)]
   
   
    
    
    
    @api.onchange('employee_id', 'loan_type')
    def onchange_approver_user(self):
        for loan in self:
            if loan.loan_approver_user_ids:
                remove = []
                for line in loan.loan_approver_user_ids:
                    remove.append((2, line.id))
                loan.loan_approver_user_ids = remove
            setting = self.env['ir.config_parameter'].sudo().get_param(
                'equip3_hr_employee_loan_extend.loan_type_approval')
            if setting == 'employee_hierarchy':
                loan.loan_approver_user_ids = self.loan_emp_by_hierarchy(loan)
                self.app_list_loan_emp_by_hierarchy()
            if setting == 'approval_matrix':
                self.loan_approval_by_matrix(loan)
                
    @api.model
    def default_get(self, fields):
        res = super(EmployeeLoanDetails, self).default_get(fields)
        employees = self.env['hr.employee'].search([('user_id', '=', self.env.uid)])
        res['employee_id'] = employees.id
        return res

    def loan_emp_by_hierarchy(self, loan):
        approval_ids = []
        seq = 1
        data = 0
        line = self.get_manager(loan, loan.employee_id, data, approval_ids, seq)
        return line

    def get_manager(self, loan, employee_manager, data, approval_ids, seq):
        setting_level = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_employee_loan_extend.loan_level')
        if not setting_level:
            raise ValidationError("Level not set")
        if not employee_manager['parent_id']['user_id']:
            return approval_ids
        while data < int(setting_level):
            approval_ids.append(
                (0, 0, {'user_ids': [(4, employee_manager['parent_id']['user_id']['id'])]}))
            data += 1
            seq += 1
            if employee_manager['parent_id']['user_id']['id']:
                self.get_manager(loan, employee_manager['parent_id'], data, approval_ids, seq)
                break
        return approval_ids

    def app_list_loan_emp_by_hierarchy(self):
        for loan in self:
            app_list = []
            for line in loan.loan_approver_user_ids:
                app_list.append(line.user_ids.id)
            loan.approvers_ids = app_list

    def loan_approval_by_matrix(self, loan):
        app_list = []
        approval_matrix = self.env['hr.loan.approval.matrix'].search(
            [('apply_to', '=', 'by_employee'), ('maximum_amount', '>=', loan.principal_amount),
             ('minimum_amount', '<=', loan.principal_amount)])
        matrix = approval_matrix.filtered(lambda line: loan.employee_id.id in line.employee_ids.ids)
        if matrix:
            data_approvers = []
            for line in matrix[0].approval_matrix_ids:
                data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                              'user_ids': [(6, 0, line.approvers.ids)]}))
                for approvers in line.approvers:
                    app_list.append(approvers.id)
            loan.approvers_ids = app_list
            loan.loan_approver_user_ids = data_approvers

        if not matrix:
            data_approvers = []
            approval_matrix = self.env['hr.loan.approval.matrix'].search(
                [('apply_to', '=', 'by_job_position'), ('maximum_amount', '>=', loan.principal_amount),
                 ('minimum_amount', '<=', loan.principal_amount)])
            matrix = approval_matrix.filtered(lambda line: loan.job_id.id in line.job_ids.ids)
            if matrix:
                for line in matrix[0].approval_matrix_ids:
                    data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                                  'user_ids': [(6, 0, line.approvers.ids)]}))
                    for approvers in line.approvers:
                        app_list.append(approvers.id)
                loan.approvers_ids = app_list
                loan.loan_approver_user_ids = data_approvers
            if not matrix:
                data_approvers = []
                approval_matrix = self.env['hr.loan.approval.matrix'].search(
                    [('apply_to', '=', 'by_department'), ('maximum_amount', '>=', loan.principal_amount),
                     ('minimum_amount', '<=', loan.principal_amount)])
                matrix = approval_matrix.filtered(lambda line: loan.department_id.id in line.department_ids.ids)
                if matrix:
                    for line in matrix[0].approval_matrix_ids:
                        data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                                      'user_ids': [(6, 0, line.approvers.ids)]}))
                        for approvers in line.approvers:
                            app_list.append(approvers.id)
                    loan.approvers_ids = app_list
                    loan.loan_approver_user_ids = data_approvers

    @api.depends('state', 'employee_id')
    def _compute_can_approve(self):
        for loan in self:
            if loan.approvers_ids:
                setting = self.env['ir.config_parameter'].sudo().get_param(
                    'equip3_hr_employee_loan_extend.loan_type_approval')
                setting_level = self.env['ir.config_parameter'].sudo().get_param(
                    'equip3_hr_employee_loan_extend.loan_level')
                app_level = int(setting_level)
                current_user = loan.env.user
                if setting == 'employee_hierarchy':
                    matrix_line = sorted(loan.loan_approver_user_ids.filtered(lambda r: r.is_approve == True))
                    app = len(matrix_line)
                    a = len(loan.loan_approver_user_ids)
                    if app < app_level and app < a:
                        if current_user in loan.loan_approver_user_ids[app].user_ids:
                            loan.is_approver = True
                        else:
                            loan.is_approver = False
                    else:
                        loan.is_approver = False
                elif setting == 'approval_matrix':
                    matrix_line = sorted(loan.loan_approver_user_ids.filtered(lambda r: r.is_approve == True))
                    app = len(matrix_line)
                    a = len(loan.loan_approver_user_ids)
                    if app < a:
                        for line in loan.loan_approver_user_ids[app]:
                            if current_user in line.user_ids:
                                loan.is_approver = True
                            else:
                                loan.is_approver = False
                    else:
                        loan.is_approver = False

                else:
                    loan.is_approver = False
            else:
                loan.is_approver = False

    def get_url(self, obj):
        url = ''
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        menu_id = self.env['ir.model.data'].get_object_reference(
            'hr_employee_loan', 'loan_loans')[1]
        action_id = self.env['ir.model.data'].get_object_reference(
            'hr_employee_loan', 'action_loan')[1]
        url = base_url + "/web?db=" + str(self._cr.dbname) + "#id=" + str(
            obj.id) + "&view_type=form&model=employee.loan.details&menu_id=" + str(
            menu_id) + "&action=" + str(action_id)
        return url

    def approver_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.loan_approver_user_ids:
                matrix_line = sorted(rec.loan_approver_user_ids.filtered(lambda r: r.is_approve == True))
                approver = rec.loan_approver_user_ids[len(matrix_line)]
                for user in approver.user_ids:
                    try:
                        template_id = ir_model_data.get_object_reference(
                            'equip3_hr_employee_loan_extend',
                            'email_template_loan_approval_request')[1]
                    except ValueError:
                        template_id = False
                    ctx = self._context.copy()
                    url = self.get_url(self)
                    ctx.update({
                        'email_from': self.env.user.email,
                        'email_to': user.email,
                        'url': url,
                        'approver_name': user.name,
                        'emp_name': self.employee_id.name,
                        'loan_type_name': self.loan_type.name,
                        'principal_amount': self.principal_amount,
                        'duration': self.duration,
                    })
                    self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                              force_send=True)
                break

    def approved_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.loan_approver_user_ids:
                for rec in rec.loan_approver_user_ids.sorted(key=lambda r: r.name):
                    for user in rec.user_ids:
                        try:
                            template_id = ir_model_data.get_object_reference(
                                'equip3_hr_employee_loan_extend',
                                'email_template_edi_loan_request_approved')[1]
                        except ValueError:
                            template_id = False
                        ctx = self._context.copy()
                        url = self.get_url(self)
                        ctx.update({
                            'email_from': self.env.user.email,
                            'email_to': self.employee_id.user_id.email,
                            'url': url,
                            'emp_name': self.employee_id.name,
                        })
                        self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                                  force_send=True)
                    break

    def reject_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.loan_approver_user_ids:
                for rec in rec.loan_approver_user_ids.sorted(key=lambda r: r.name):
                    for user in rec.user_ids:
                        try:
                            template_id = ir_model_data.get_object_reference(
                                'equip3_hr_employee_loan_extend',
                                'email_template_edi_loan_request_reject')[1]
                        except ValueError:
                            template_id = False
                        ctx = self._context.copy()
                        url = self.get_url(self)
                        ctx.update({
                            'email_from': self.env.user.email,
                            'email_to': self.employee_id.user_id.email,
                            'url': url,
                            'approver_name': user.name,
                            'emp_name': self.employee_id.name,
                        })
                        self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                                  force_send=True)
                    break

    def approver_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_employee_loan_extend.send_by_wa_loan')
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        url = self.get_url(self)
        if send_by_wa:
            template = self.env.ref('equip3_hr_employee_loan_extend.loan_approver_wa_template')
            if template:
                if self.loan_approver_user_ids:
                    matrix_line = sorted(self.loan_approver_user_ids.filtered(lambda r: r.is_approve == True))
                    approver = self.loan_approver_user_ids[len(matrix_line)]
                    for user in approver.user_ids:
                        string_test = str(template.message)
                        if "${employee_name}" in string_test:
                            string_test = string_test.replace("${employee_name}", self.employee_id.name)
                        if "${name}" in string_test:
                            string_test = string_test.replace("${name}", self.name)
                        if "${approver_name}" in string_test:
                            string_test = string_test.replace("${approver_name}", user.name)
                        if "${loan_type}" in string_test:
                            string_test = string_test.replace("${loan_type}", self.loan_type.name)
                        if "${currency}" in string_test:
                            string_test = string_test.replace("${currency}", self.currency_id.symbol)
                        if "${amount}" in string_test:
                            string_test = string_test.replace("${amount}", str(self.principal_amount))
                        if "${month}" in string_test:
                            string_test = string_test.replace("${month}", str(self.duration))
                        if "${br}" in string_test:
                            string_test = string_test.replace("${br}", f"\n")
                        if "${url}" in string_test:
                            string_test = string_test.replace("${url}", url)
                        phone_num = str(user.mobile_phone)
                        if "+" in phone_num:
                            phone_num = int(phone_num.replace("+", ""))
                        param = {'body': string_test, 'phone': phone_num}
                        domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                        token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                        try:
                            request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                        except ConnectionError:
                            raise ValidationError("Not connect to API Chat Server. Limit reached or not active")

    def approved_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_employee_loan_extend.send_by_wa_loan')
        if send_by_wa:
            template = self.env.ref('equip3_hr_employee_loan_extend.loan_approved_wa_template')
            url = self.get_url(self)
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            if template:
                if self.loan_approver_user_ids:
                    string_test = str(template.message)
                    if "${employee_name}" in string_test:
                        string_test = string_test.replace("${employee_name}", self.employee_id.name)
                    if "${name}" in string_test:
                        string_test = string_test.replace("${name}", self.name)
                    if "${br}" in string_test:
                        string_test = string_test.replace("${br}", f"\n")
                    phone_num = str(self.employee_id.mobile_phone)
                    if "+" in phone_num:
                        phone_num = int(phone_num.replace("+", ""))
                    if "${url}" in string_test:
                        string_test = string_test.replace("${url}", url)
                    param = {'body': string_test, 'phone': phone_num}
                    domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                    token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                    try:
                        request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,
                                                       headers=headers, verify=True)
                    except ConnectionError:
                        raise ValidationError("Not connect to API Chat Server. Limit reached or not active")

    def rejected_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_employee_loan_extend.send_by_wa_loan')
        if send_by_wa:
            template = self.env.ref('equip3_hr_employee_loan_extend.loan_rejected_wa_template')
            url = self.get_url(self)
            if template:
                if self.loan_approver_user_ids:
                    string_test = str(template.message)
                    if "${employee_name}" in string_test:
                        string_test = string_test.replace("${employee_name}", self.employee_id.name)
                    if "${name}" in string_test:
                        string_test = string_test.replace("${name}", self.name)
                    if "${br}" in string_test:
                        string_test = string_test.replace("${br}", f"\n")
                    phone_num = str(self.employee_id.mobile_phone)
                    if "+" in phone_num:
                        phone_num = int(phone_num.replace("+", ""))
                    param = {'body': string_test, 'phone': phone_num}
                    domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                    token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                    try:
                        request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,
                                                       headers=headers, verify=True)
                    except ConnectionError:
                        raise ValidationError("Not connect to API Chat Server. Limit reached or not active")

    def action_applied(self):
        res = super(EmployeeLoanDetails, self).action_applied()
        self.approver_mail()
        if self.employee_id and self.loan_type:
            if self.loan_type.apply_to == 'years_of_service':
                if self.employee_id.years_of_service < self.loan_type.years_of_service and self.employee_id.months < self.loan_type.months_of_service and self.employee_id.days < self.loan_type.days_of_service:
                    raise ValidationError(_('Employee not qualify'))
        self.approver_wa_template()
        return res

    def action_approved(self):
        sequence_matrix = [data.name for data in self.loan_approver_user_ids]
        sequence_approval = [data.name for data in self.loan_approver_user_ids.filtered(
            lambda line: len(line.approved_employee_ids) != line.minimum_approver)]
        max_seq = max(sequence_matrix)
        min_seq = min(sequence_approval)
        approval = self.loan_approver_user_ids.filtered(
            lambda line: self.env.user.id in line.user_ids.ids and len(
                line.approved_employee_ids) != line.minimum_approver and line.name == min_seq)
        for record in self:
            current_user = self.env.uid
            setting = self.env['ir.config_parameter'].sudo().get_param(
                'equip3_hr_employee_loan_extend.loan_type_approval')
            now = datetime.now(timezone(self.env.user.tz))
            dateformat = f"{now.day}/{now.month}/{now.year} {now.hour}:{now.minute}:{now.second}"
            date_approved = time.strftime(DEFAULT_SERVER_DATE_FORMAT)
            date_approved_obj = datetime.strptime(date_approved, DEFAULT_SERVER_DATE_FORMAT)
            if setting == 'employee_hierarchy':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for user in record.loan_approver_user_ids:
                            if current_user == user.user_ids.id:
                                user.is_approve = True
                                user.timestamp = fields.Datetime.now()
                                user.approver_state = 'approved'
                                string_approval = []
                                if user.approval_status:
                                    string_approval.append(f"{self.env.user.name}:Approved")
                                    user.approval_status = "\n".join(string_approval)
                                    string_timestammp = [user.approved_time]
                                    string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                    user.approved_time = "\n".join(string_timestammp)
                                    if record.feedback_parent:
                                        feedback_list = [user.feedback,
                                                         f"{self.env.user.name}:{record.feedback_parent}"]
                                        final_feedback = "\n".join(feedback_list)
                                        user.feedback = f"{final_feedback}"
                                    elif user.feedback and not record.feedback_parent:
                                        user.feedback = user.feedback
                                    else:
                                        user.feedback = ""
                                else:
                                    user.approval_status = f"{self.env.user.name}:Approved"
                                    user.approved_time = f"{self.env.user.name}:{dateformat}"
                                    if record.feedback_parent:
                                        user.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                    else:
                                        user.feedback = ""
                                record.approved_user_ids = [(4, current_user)]
                        matrix_line = sorted(record.loan_approver_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            self.approved_mail()
                            record.write({'state': 'approved',
                                          'date_approved': date_approved})
                            self.approved_wa_template()
                        else:
                            self.approver_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has been approved the Request!'
                            if len(approval.approved_employee_ids) == approval.minimum_approver and not approval.name == max_seq:
                                self.approver_wa_template()
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved'
                    ))
            elif setting == 'approval_matrix':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for line in record.loan_approver_user_ids:
                            for user in line.user_ids:
                                if current_user == user.user_ids.id:
                                    line.timestamp = fields.Datetime.now()
                                    record.approved_user_ids = [(4, current_user)]
                                    var = len(line.approved_employee_ids) + 1
                                    if line.minimum_approver <= var:
                                        line.approver_state = 'approved'
                                        string_approval = []
                                        string_approval.append(line.approval_status)
                                        if line.approval_status:
                                            string_approval.append(f"{self.env.user.name}:Approved")
                                            line.approval_status = "\n".join(string_approval)
                                            string_timestammp = [line.approved_time]
                                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                            line.approved_time = "\n".join(string_timestammp)
                                            if record.feedback_parent:
                                                feedback_list = [line.feedback,
                                                                 f"{self.env.user.name}:{record.feedback_parent}"]
                                                final_feedback = "\n".join(feedback_list)
                                                line.feedback = f"{final_feedback}"
                                            elif line.feedback and not record.feedback_parent:
                                                line.feedback = line.feedback
                                            else:
                                                line.feedback = ""
                                        else:
                                            line.approval_status = f"{self.env.user.name}:Approved"
                                            line.approved_time = f"{self.env.user.name}:{dateformat}"
                                            if record.feedback_parent:
                                                line.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                            else:
                                                line.feedback = ""
                                        line.is_approve = True
                                    else:
                                        line.approver_state = 'pending'
                                        if line.approval_status:
                                            string_approval.append(f"{self.env.user.name}:Approved")
                                            line.approval_status = "\n".join(string_approval)
                                            string_timestammp = [line.approved_time]
                                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                            line.approved_time = "\n".join(string_timestammp)
                                            if record.feedback_parent:
                                                feedback_list = [line.feedback,
                                                                 f"{self.env.user.name}:{record.feedback_parent}"]
                                                final_feedback = "\n".join(feedback_list)
                                                line.feedback = f"{final_feedback}"
                                            elif line.feedback and not record.feedback_parent:
                                                line.feedback = line.feedback
                                            else:
                                                line.feedback = ""
                                        else:
                                            line.approval_status = f"{self.env.user.name}:Approved"
                                            line.approved_time = f"{self.env.user.name}:{dateformat}"
                                            if record.feedback_parent:
                                                line.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                            else:
                                                line.feedback = ""
                                    line.approved_employee_ids = [(4, current_user)]

                        matrix_line = sorted(record.loan_approver_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            self.approved_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Request!'
                            record.write({'state': 'approved',
                                          'date_approved': date_approved})
                            self.approved_wa_template()
                        else:
                            self.approver_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Request!'
                            if len(approval.approved_employee_ids) == approval.minimum_approver and not approval.name == max_seq:
                                self.approver_wa_template()
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved!'
                    ))
            else:
                raise ValidationError(_(
                    'Already approved!'
                ))

    def action_rejected(self):
        for record in self:
            for user in record.loan_approver_user_ids:
                for check_user in user.user_ids:
                    now = datetime.now(timezone(self.env.user.tz))
                    dateformat = f"{now.day}/{now.month}/{now.year} {now.hour}:{now.minute}:{now.second}"
                    if self.env.uid == check_user.id:
                        user.timestamp = fields.Datetime.now()
                        user.approver_state = 'refuse'
                        string_approval = []
                        string_approval.append(user.approval_status)
                        if user.approval_status:
                            string_approval.append(f"{self.env.user.name}:Refused")
                            user.approval_status = "\n".join(string_approval)
                            string_timestammp = [user.approved_time]
                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                            user.approved_time = "\n".join(string_timestammp)
                        else:
                            user.approval_status = f"{self.env.user.name}:Refused"
                            user.approved_time = f"{self.env.user.name}:{dateformat}"
            self.reject_mail()
            record.approved_user = self.env.user.name + ' ' + 'has been Rejected!'
            record.write({'state': 'rejected'})
            self.rejected_wa_template()

    def wizard_approve(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'hr.loan.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'name': "Confirmation Message",
            'target': 'new',
        }

    def _calc_max_loan_amt(self):
        res = super(EmployeeLoanDetails, self)._calc_max_loan_amt()
        for rec in self:
            for policy in rec.loan_policy_ids:
                if policy.policy_type == 'maxamt':
                    if policy.max_loan_type == 'salary_percentage':
                        if rec.employee_id.contract_id.wage:
                            rec.max_loan_amt = rec.employee_id.contract_id.wage * policy.policy_value / 100
        return res

    def _compute_can_approve_installment(self):
        current_user = self.env.uid
        for rec in self:
            if rec.approvers_ids and current_user in rec.approvers_ids.ids:
                rec.can_approve_installment = True
            else:
                rec.can_approve_installment = False

class LoanApproverUser(models.Model):
    _name = 'loan.approver.user'

    emp_loan_id = fields.Many2one('employee.loan.details', string="Employee Loan Id")
    name = fields.Integer('Sequence', compute="fetch_sl_no")
    user_ids = fields.Many2many('res.users', string="Approvers")
    approved_employee_ids = fields.Many2many('res.users', 'emp_loan_user_ids', string="Approved user")
    minimum_approver = fields.Integer(string="Minimum Approver", default=1)
    timestamp = fields.Datetime(string="Timestamp")
    approved_time = fields.Text(string="Timestamp")
    feedback = fields.Text()
    approver_state = fields.Selection([('draft', 'Draft'), ('pending', 'Pending'), ('approved', 'Approved'),
                                       ('refuse', 'Refused')], default='draft', string="Approval Status")
    approval_status = fields.Text()
    is_approve = fields.Boolean(string="Is Approve", default=False)

    @api.depends('emp_loan_id')
    def fetch_sl_no(self):
        sl = 0
        for line in self.emp_loan_id.loan_approver_user_ids:
            sl = sl + 1
            line.name = sl

class LoanInstallmentDetail(models.Model):
    _inherit = 'loan.installment.details'

    can_approve_installment = fields.Boolean(string="Installment Approvers", related='loan_id.can_approve_installment')
    is_book_interest = fields.Boolean(string="Book Interest Approvers", compute='_compute_is_book_interest')
    deduction_based_period = fields.Selection(
        related='loan_type.deduction_based_period',
        string='Deduction Based On Period',
        store=True,
    )
    payslip_id = fields.Many2one('hr.payslip', string="Payslip")
    paid_payroll = fields.Boolean(compute='_check_paid_payroll', string='Paid Payroll Status', store=True)
    
    
    def custom_menu(self):
            # views = [(self.env.ref('bi_employee_travel_managment.view_travel_req_tree').id, 'tree'),
        #              (self.env.ref('bi_employee_travel_managment.view_travel_req_form').id, 'form')]
        search_view_id = self.env.ref("hr_employee_loan.view_loan_installment_filter")
        if self.env.user.has_group('equip3_hr_employee_loan_extend.group_loan_self_service') and not self.env.user.has_group('equip3_hr_employee_loan_extend.group_loan_supervisor'):
            return {
                'type': 'ir.actions.act_window',
                'name': 'Loan Installments',
                'res_model': 'loan.installment.details',
                'target': 'current',
                'view_mode': 'tree,form,graph',
                # 'views':views,
                'domain': [('employee_id.user_id', '=', self.env.user.id)],
                # 'context': {'default_holiday_type': 'company'},
                'help': """<p class="o_view_nocontent_smiling_face">
                    Create New Loan Installments
                </p>""",
                'context':{},
                'search_view_id': search_view_id.id,

            }
        elif self.env.user.has_group(
                'equip3_hr_employee_loan_extend.group_loan_supervisor') and not self.env.user.has_group(
                'equip3_hr_employee_loan_extend.group_loan_finance'):
            employee_ids = []
            my_employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)])
            if my_employee:
                for child_record in my_employee.child_ids:
                    employee_ids.append(my_employee.id)
                    employee_ids.append(child_record.id)
                    child_record._get_amployee_hierarchy(employee_ids, child_record.child_ids, my_employee.id)
                return {
                    'type': 'ir.actions.act_window',
                    'name': 'Loan Installments',
                    'res_model': 'loan.installment.details',
                    'target': 'current',
                    'view_mode': 'tree,form,graph',
                    # 'views':views,
                    'domain': [('employee_id', 'in', employee_ids)],
                    'context': {},
                    'help': """<p class="o_view_nocontent_smiling_face">
                    Create New Loan Installments
                    </p>""",
                    'search_view_id': search_view_id.id,

                }

        else:
            return {
                'type': 'ir.actions.act_window',
                'name': 'Loan Installments',
                'res_model': 'loan.installment.details',
                'target': 'current',
                'view_mode': 'tree,form,graph',
                'domain': [],
                'context': {},
                'help': """<p class="o_view_nocontent_smiling_face">
                Create New Loan Installments
                </p>""",
                'search_view_id': search_view_id.id,
                
            }

    def _compute_is_book_interest(self):
        for rec in self:
            if rec.interest_amt == 0 or rec.int_move_id:
                rec.is_book_interest = True
            else:
                rec.is_book_interest = False

    @api.depends('loan_id', 'loan_id.loan_type', 'payslip_id', 'payslip_id.state')
    def _check_paid_payroll(self):
        payslip_obj = self.env['hr.payslip']
        for install in self:
            if install.loan_id:
                if install.loan_id.loan_type.payment_method == 'payroll':
                    if install.payslip_id:
                        if install.payslip_id.state == 'done':
                            for line in install.payslip_id.line_ids:
                                if line.salary_rule_id.code == "LOAN":
                                    install.paid_payroll = True
                                    self._cr.execute("update loan_installment_details set state='paid' where id = %s" % (install.id))