# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    def get_installment_loan(self, payslip, date_from, date_to):
        payslip_rec = self.sudo().browse(payslip)[0]
        amount = 0.0
        loan_installment = self.env['loan.installment.details'].sudo().search([('employee_id','=',payslip_rec.employee_id.id),('state','!=','paid'),('loan_repayment_method','=','payroll')])
        for rec in loan_installment:
            if rec.deduction_based_period == "date_from":
                if rec.date_from >= date_from and rec.date_from <= date_to:
                    amount = rec.total
                    rec.payslip_id = payslip_rec.id
            elif rec.deduction_based_period == "date_to":
                if rec.date_to >= date_from and rec.date_to <= date_to:
                    amount = rec.total
                    rec.payslip_id = payslip_rec.id
        return amount