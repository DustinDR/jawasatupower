from odoo import api, fields, models

class LoanPloicy(models.Model):
    _inherit = 'loan.policy'

    max_loan_type = fields.Selection(selection_add=[('salary_percentage', 'Salary Percentage')], ondelete={'salary_percentage': 'cascade'})