# -*- coding: utf-8 -*-

from . import hr_full_loan_payment
from . import hr_employee_loan
from . import hr_employee_loan_cancel
from . import hr_loan_approval_matrix
from . import res_config_settings
from . import loan_type
from . import loan_policy
from . import hr_payslip