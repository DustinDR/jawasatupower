# -*- coding: utf-8 -*-

from . import hr_other_inputs_generate_entries
from . import hr_payroll_payslips_by_employees
from . import payslip_send_email_message
from . import hr_my_payslips
from . import hr_generate_spt
from . import hr_report_bpjs_kesehatan
from . import hr_report_bpjs_kesehatan_excel
from . import hr_spt_report_wizard
from . import hr_spt_report_excel
from . import hr_spt_report_1721_I
from . import hr_spt_report_1721_I_attachment
from . import hr_report_bpjs_ketenagakerjaan
from . import hr_report_bpjs_ketenagakerjaan_excel
from . import hr_bca_bank_transfer
from . import hr_bca_bank_transfer_attachment
from . import hr_danamon_bank_transfer
from . import hr_danamon_bank_transfer_attachment
from . import hr_mayapada_bank_transfer
from . import hr_mayapada_bank_transfer_attachment