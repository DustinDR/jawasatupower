# -*- coding: utf-8 -*-
import calendar
from odoo import api, fields, models, _
from datetime import datetime
import time
from odoo.exceptions import ValidationError
import tempfile
import csv
import base64
from io import BytesIO

class HrMayapadaBankTransfer(models.TransientModel):
    _name = 'hr.mayapada.bank.transfer'

    def _compute_year_selection(self):
        year_list = []
        current_year = int(time.strftime('%Y'))
        # year_list = range(current_year, current_year - 11, -1)

        year_range = range(2015, current_year + 1)
        for year in reversed(year_range):
            year_list.append((str(year), str(year)))
        return year_list

    def _compute_month_selection(self):
        month_list = []
        for x in range(1, 13):
            month_list.append((str(calendar.month_name[x]), str(calendar.month_name[x])))
        return month_list

    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id, readonly=True)
    corporate_id = fields.Char('Company ID', default=lambda self: self.env.user.company_id.corporate_id, readonly=True)
    year = fields.Selection(selection=lambda self: self._compute_year_selection(), string="Year", default="none",
                            required=True)
    month = fields.Selection(selection=lambda self: self._compute_month_selection(), string="Month", default="none",
                             required=True)
    transaction_date = fields.Date('Transaction Date', default=datetime.today(), required=True)
    company_bank_account = fields.Many2one('res.partner.bank', string='Company Bank Account', required=True)
    remarks = fields.Char('Remarks', required=True)
    employee_ids = fields.Many2many('hr.employee', string='Employees', required=True)

    @api.onchange("company_id")
    def _onchange_company_id(self):
        domain = {'domain': {'company_bank_account': [('partner_id', '=', self.company_id.partner_id.id)]}}
        return domain

    @api.onchange('year', 'month')
    def onchange_employee(self):
        if (not self.year) or (not self.month):
            return

        self.remarks = _('Gaji %s %s') % (self.month, self.year)

    def action_print_csv(self):
        if not self.employee_ids:
            raise ValidationError(_('Please select employee.'))

        corporate_id = self.corporate_id.replace('/', '') if self.corporate_id else ''
        remarks = self.remarks
        transaction_date = self.transaction_date.strftime('%d%m%Y')
        company_bank_account = self.company_bank_account.acc_number if self.company_bank_account else ''

        datas = []
        jumlah_karyawan = 0
        total_salary = 0
        for emp in self.employee_ids:
            month_datetime = datetime.strptime(self.month, "%B")
            month_selected = month_datetime.month
            last_day_month = calendar.monthrange(int(self.year), month_selected)[1]
            month_selected = str('{:02d}'.format(month_selected))
            selected_month_start_date = self.year + '-' + month_selected + '-' + str('01')
            selected_month_start_date = datetime.strptime(selected_month_start_date, "%Y-%m-%d").date()
            selected_month_end_date = self.year + '-' + month_selected + '-' + str(last_day_month)
            selected_month_end_date = datetime.strptime(selected_month_end_date, "%Y-%m-%d").date()

            payslips = self.env['hr.payslip'].search([('employee_id', '=', emp.id), ('state', '=', 'done'),
                                                      ('payslip_report_date', '>=', selected_month_start_date),
                                                      ('payslip_report_date', '<=', selected_month_end_date),
                                                      ('payslip_pesangon', '=', False)])

            employee_id = emp.sequence_code if emp.sequence_code else ''
            employee_name = emp.name
            account_number = ""
            nilai_gaji = 0
            if emp.bank_ids:
                bank_id = emp.bank_ids.filtered(lambda r: r.is_used == True)[0]
                if bank_id:
                    account_number = bank_id.acc_number
            if payslips:
                currency = ''
                for payslip in payslips:
                    currency = payslip.currency_id.name if payslip.currency_id else ''
                    for line in payslip.line_ids:
                        if line.salary_rule_id.category_id.code == "NET":
                            nilai_gaji += line.total
                jumlah_karyawan += 1
                total_salary += nilai_gaji
                datas.append({
                    'employee_id': employee_id,
                    'account_number': str(account_number),
                    'employee_name': employee_name,
                    'nilai_gaji': str(int(nilai_gaji))
                })
        if datas:
            file_name = 'Mayapada Bank Transfer - ' + str(month_datetime.strftime("%b")) + str(self.year) + '.csv'
            file_path = tempfile.mktemp(suffix='.csv')
            with open(file_path, mode='w') as file:
                writer = csv.writer(file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                writer.writerow(['00', corporate_id, remarks] + [''] * 334)
                for line in datas:
                    writer.writerow(['01', 'IT', 'Payroll With Bii', 'ID', transaction_date] + [''] * 2 +
                                    [line.get('employee_id'), 'Reff 01', remarks, 'IDR', line.get('nilai_gaji'),
                                     'N', 'IDR', company_bank_account, line.get('account_number')] + [''] * 2 +
                                    ['Y', line.get('employee_name')] + [''] * 11 + ['ID'] + [''] * 69 + ['Payroll 01',
                                    remarks] + [''] * 6 + ['01'] + [''] * 227)
                writer.writerow(['99', jumlah_karyawan, int(total_salary)] + [''] * 334)
            with open(file_path, 'r', encoding="utf-8") as f2:
                data = str.encode(f2.read(), 'utf-8')
            export_id = self.env['hr.mayapada.bank.transfer.attachment'].create(
                {'attachment_file': base64.encodebytes(data), 'file_name': file_name})
            return {
                'view_mode': 'form',
                'res_id': export_id.id,
                'name': 'Mayapada Bank Transfer',
                'res_model': 'hr.mayapada.bank.transfer.attachment',
                'view_type': 'form',
                'type': 'ir.actions.act_window',
                'target': 'new',
            }
        else:
            raise ValidationError(_('There is no Data.'))