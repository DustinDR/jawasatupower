from odoo import _, api, fields, models

class HRTaxSetting(models.TransientModel):
    _name = 'hr.tax.setting'
    _inherit = 'res.config.settings'
    _description = 'HR Tax Setting'

    job_cost_rate = fields.Float(string='Job Title Cost Rate (%)', config_parameter='equip3_hr_payroll_extend_id.job_cost_rate', default=5)
    max_job_cost_rate_monthly = fields.Float(string='Max Job Title Cost (Monthly)', config_parameter='equip3_hr_payroll_extend_id.max_job_cost_rate_monthly', default=500000)
    max_job_cost_rate_annualized = fields.Float(string='Max Job Title Cost (Annualized)', config_parameter='equip3_hr_payroll_extend_id.max_job_cost_rate_annualized', default=6000000)
    pph26_rate = fields.Float(string='PPh26 Rate (%)', config_parameter='equip3_hr_payroll_extend_id.pph26_rate', default=20)