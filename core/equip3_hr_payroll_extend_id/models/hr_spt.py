# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class HrSptType(models.Model):
    _name = 'hr.spt.type'
    _description = 'HR SPT Type'

    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company, readonly=True)

    @api.constrains('code')
    def check_name(self):
        for record in self:
            if record.code:
                check_code = self.search([('code', '=', record.code), ('id', '!=', record.id)])
                if check_code:
                    raise ValidationError("Code must be unique!")

class HrSptCategory(models.Model):
    _name = 'hr.spt.category'
    _description = 'HR SPT Category'

    name = fields.Char('Name', required=True)
    spt_type = fields.Many2one('hr.spt.type', string='SPT Type')
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company, readonly=True)
