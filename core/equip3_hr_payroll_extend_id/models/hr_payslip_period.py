# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError

class Equip3HrPayslipPeriod(models.Model):
    _name = 'hr.payslip.period'
    _description = 'HR Payslip Period'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(string="Period Name", required=True)
    code = fields.Char(string="Code", required=True)
    start_date = fields.Date(string="Start Date", required=True)
    end_date = fields.Date(string="End Date", required=True)
    start_period_based_on = fields.Selection([('start_date', 'Start Date'), ('end_date', 'End Date')],
                     'Period Based on', default='', required=True)
    company_id = fields.Many2one("res.company", string="Company", default=lambda self: self.env.company,
                                 track_visibility='onchange')
    state = fields.Selection([('draft', 'Draft'), ('open', 'Open'), ('closed', 'Closed')],
                             string='Status')
    payslip_period_ids = fields.One2many('hr.payslip.period.line', 'period_id', states={'closed': [('readonly', True)]})
    is_hide_create_month = fields.Boolean(default=False)
    is_hide_open_period = fields.Boolean(default=True)
    is_hide_close_period = fields.Boolean(default=True)

    def unlink(self):
        for record in self:
            if record.state in ('closed', 'open'):
                raise ValidationError("Only Draft status can be deleted")
        data = super(Equip3HrPayslipPeriod, self).unlink()
        return data

    @api.constrains('code')
    def check_code(self):
        for record in self:
            if record.code:
                check_name = self.search([('code', '=', record.code), ('id', '!=', record.id)])
                if check_name:
                    raise ValidationError("Code must be unique!")

    @api.constrains('name')
    def check_name(self):
        for record in self:
            if record.name:
                check_name = self.search([('name', '=', record.name), ('id', '!=', record.id)])
                if check_name:
                    raise ValidationError("Period Name must be unique!")

    @api.onchange('start_date', 'end_date')
    def _onchange_date(self):
        for record in self:
            if record.start_date and record.end_date:
                if record.start_date > record.end_date:
                    raise ValidationError("End Date must be greater than Start Date!")

    def action_create_period(self):
        for period in self:
            period._create_period()
            period.state = 'draft'
            period.is_hide_create_month = True
            period.is_hide_open_period = False
            period.message_post(body=_('Periods Status: Draft'))

    def _create_period(self):
        self.ensure_one()
        obj_period = self.env["hr.payslip.period.line"]
        start_date = datetime.strptime(str(self.start_date), "%Y-%m-%d")
        ends_date = datetime.strptime(str(self.end_date), "%Y-%m-%d")
        while start_date.strftime("%Y-%m-%d") < ends_date.strftime("%Y-%m-%d"):
            end_date = start_date + relativedelta(months=+1, days=-1)

            if end_date.strftime("%Y-%m-%d") > ends_date.strftime("%Y-%m-%d"):
                end_date = ends_date

            if self.start_period_based_on == 'start_date':
                year_date = start_date.strftime("%Y")
                month_date = start_date.strftime("%B")
            elif self.start_period_based_on == 'end_date':
                year_date = end_date.strftime("%Y")
                month_date = end_date.strftime("%B")

            obj_period.create({
                "year": year_date,
                "month": month_date,
                "start_date": start_date.strftime("%Y-%m-%d"),
                "end_date": end_date.strftime("%Y-%m-%d"),
                "state": "draft",
                "period_id": self.id,
            })
            start_date = start_date + relativedelta(months=+1)

    def to_open(self):
        for record in self:
            if record.payslip_period_ids:
                data = []
                for line in record.payslip_period_ids:
                    data.append((1, line.id, {'state': 'open'}))
                record.payslip_period_ids = data
            record.state = "open"
            record.is_hide_open_period = True
            record.is_hide_close_period = False
            record.message_post(body=_('Periods Status: Draft -> Open'))

    def to_close(self):
        for record in self:
            if record.payslip_period_ids:
                data = []
                for line in record.payslip_period_ids:
                    data.append((1, line.id, {'state': 'closed'}))
                record.payslip_period_ids = data
            record.state = "closed"
            record.is_hide_close_period = True
            record.message_post(body=_('Periods Status: Open -> Closed'))

class Equip3HrPayslipPeriodLine(models.Model):
    _name = 'hr.payslip.period.line'

    def name_get(self):
        res = []
        for rec in self:
            res.append((rec.id, "%s" % (rec.month)))
        return res

    period_id = fields.Many2one('hr.payslip.period')
    year = fields.Char('Year')
    month = fields.Char("Month")
    start_date = fields.Date("Start Date")
    end_date = fields.Date("End Date")
    state = fields.Selection([('draft', 'Draft'), ('open', 'Open'), ('closed', 'Closed')], string='Status')