# -*- coding: utf-8 -*-

from . import res_config_settings
from . import hr_payslip_period
from . import hr_spt
from . import hr_salary_rule
from . import hr_other_input
from . import hr_other_input_entries
from . import hr_contract
from . import res_bank
from . import late_deduction_rule
from . import resource_calendar
from . import hr_tax_setting
from . import hr_pesangon_upmk_settings
from . import hr_tax_bracket_pesangon_upmk
from . import hr_payslip
from . import hr_spt_sequence
from . import thr_rule