# -*- coding: utf-8 -*-
from odoo import fields, models, api

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    payslip_allow_send_email = fields.Boolean(config_parameter='equip3_hr_payroll_extend_id.payslip_allow_send_email', string='Allow send payslip to email')
    bpjs_kesehatan_limit = fields.Float(config_parameter='equip3_hr_payroll_extend_id.bpjs_kesehatan_limit', string='BPJS kesehatan Limit', default=12000000)
    jaminan_pensiun_limit = fields.Float(config_parameter='equip3_hr_payroll_extend_id.jaminan_pensiun_limit', string='Jaminan Pensiun Limit', default=8939700)

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param('equip3_hr_payroll_extend_id.bpjs_kesehatan_limit', self.bpjs_kesehatan_limit)
        self.env['ir.config_parameter'].sudo().set_param('equip3_hr_payroll_extend_id.jaminan_pensiun_limit', self.jaminan_pensiun_limit)
