# -*- coding: utf-8 -*-
{
    'name': "equip3_pos_general",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "HashMicro",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'POS',
    'version': '1.1.2.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'web', 'pos_retail', 'branch'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/assets.xml',
        'views/AccountBankStatement.xml',
        'views/AccountMove.xml',
        'views/AccountPayment.xml',
        'views/PosConfig.xml',
        'views/PosPayment.xml',
        'views/PosOrder.xml',
        'views/PosSession.xml',
        'views/ProductTemplate.xml',
        'views/PurchaseOrder.xml',
        'views/ResPartner.xml',
        'views/ResUsers.xml',
        'views/SaleOrder.xml',
        'views/StockInventory.xml',
        'views/StockLocation.xml',
        'views/StockMove.xml',
        'views/StockPicking.xml',
        'views/StockWarehouse.xml',
    ],

    'qweb': [
        'static/src/xml/CustomerFacingScreenWidget.xml',
        'static/src/xml/HeaderButton.xml',
        'static/src/xml/Orderline.xml',
        'static/src/xml/PosOrderScreenWidget.xml',
        'static/src/xml/SaleOrderScreenWidget.xml',
        'static/src/xml/Themes.xml',
        'static/src/xml/TicketButton.xml',
        'static/src/xml/KitchenTickets.xml'
        'static/src/basic/static/src/xml/*.xml',
        'static/src/xml/TableWidget.xml'
    ],

    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    
    "images": ["static/description/icon.png"],
    
    'installable': True,
    'application': True,
    'auto_install': False,
}