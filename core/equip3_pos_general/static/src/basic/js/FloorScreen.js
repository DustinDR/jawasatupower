odoo.define('equip3_pos_general.FloorScreen', function(require) {
    'use strict';

    const FloorScreen = require('pos_restaurant.FloorScreen');
    const Registries = require('point_of_sale.Registries');
    var core = require('web.core');
    var QWeb = core.qweb;
    var _t = core._t;

    const POSFloorScreen = (FloorScreen) =>
        class extends FloorScreen {
            _onSelectTable(event) {
                super._onSelectTable(...arguments);
                if(!this.state.isEditMode){
                    if($(event.target).data('bs.popover') != undefined){
                        $(event.target).popover('hide');
                    }
                }
            }
        };
    Registries.Component.extend(FloorScreen, POSFloorScreen);

    return FloorScreen;
});
