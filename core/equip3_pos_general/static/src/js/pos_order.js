odoo.define('equip3_pos_general.pos_order', function (require) {
    'use strict';
    
    const OrderWidget = require('point_of_sale.OrderWidget');
    const Registries = require('point_of_sale.Registries');
    const ProductScreen = require('point_of_sale.ProductScreen')
    const OrderSummary = require('point_of_sale.OrderSummary');
    const {posbus} = require('point_of_sale.utils');

    const OrderSummaryExt = (OrderSummary) =>
        class extends OrderSummary {
            constructor() {
                super(...arguments);
            }
        }
    Registries.Component.extend(OrderSummary, OrderSummaryExt);

    const ProductScreenExt = (ProductScreen) =>
        class extends ProductScreen {
            constructor() {
                super(...arguments);
            }
            async OnClickCustomer() { // single screen
                this.env.pos.syncProductsPartners()
                posbus.trigger('set-screen', 'Clients') // single screen
                setTimeout(function () {
                    $('.searchbox-client >input').focus()
                }, 200)
            }
            async reloadMasterData() {
                await this.env.pos.syncProductsPartners()
                if (this.env.pos.config.pos_orders_management) {
                    await this.env.pos.getPosOrders();
                }
                const coupon_model = this.env.pos.models.find(m => m.model == 'coupon.coupon')
                if (coupon_model) {
                    await this.env.pos.load_server_data_by_model(coupon_model)
                }
                const pricelist_model = this.env.pos.models.find(m => m.model == 'product.pricelist')
                if (pricelist_model) {
                    await this.env.pos.load_server_data_by_model(pricelist_model)
                    this.env.pos.getProductPricelistItems()
                }
            }
            _onClickPayBtn() {
                this.showScreen('PaymentScreen');
            }
            async AddNewCustomer() {
                let {confirmed, payload: results} = await this.showPopup('PopUpCreateCustomer', {
                    title: this.env._t('Create New Customer'),
                    mobile: ''
                })
                if (confirmed) {
                    if (results.error) {
                        return this.env.pos.alert_message({
                            title: this.env._t('Error'),
                            body: results.error
                        })
                    }
                    const partnerValue = {
                        'name': results.name,
                    }
                    if (results.image_1920) {
                        partnerValue['image_1920'] = results.image_1920.split(',')[1]
                    }
                    if (results.title) {
                        partnerValue['title'] = results.title
                    }
                    if (!results.title && this.env.pos.partner_titles) {
                        partnerValue['title'] = this.env.pos.partner_titles[0]['id']
                    }
                    if (results.street) {
                        partnerValue['street'] = results.street
                    }
                    if (results.city) {
                        partnerValue['city'] = results.city
                    }
                    if (results.street) {
                        partnerValue['street'] = results.street
                    }
                    if (results.phone) {
                        partnerValue['phone'] = results.phone
                    }
                    if (results.mobile) {
                        partnerValue['mobile'] = results.mobile
                    }

                    if (results.birthday_date) {
                        partnerValue['birthday_date'] = results.birthday_date
                    }
                    if (results.barcode) {
                        partnerValue['barcode'] = results.barcode
                    }
                    if (results.comment) {
                        partnerValue['comment'] = results.comment
                    }
                    if (results.property_product_pricelist) {
                        partnerValue['property_product_pricelist'] = results.property_product_pricelist
                    } else {
                        partnerValue['property_product_pricelist'] = this.env.pos.pricelists[0].id
                    }
                    if (results.country_id) {
                        partnerValue['country_id'] = results.country_id
                    }
                    let partner_id = await this.rpc({
                        model: 'res.partner',
                        method: 'create',
                        args: [partnerValue],
                        context: {}
                    })
                    await this.reloadMasterData()
                    const partner = this.env.pos.db.partner_by_id[partner_id]
                    this.env.pos.get_order().set_client(partner)
                }
            }
            OpenFeatureButtons() {
                this.env.pos.showAllButton = !this.env.pos.showAllButton
                this.state.showButtons = this.env.pos.showAllButton
            }
            startFireAppetizer() {
                var appeCat = this.env.pos.config.fire_appetizer[0];
                console.log('startFireAppetizer');
                var ktOrders = this.env.pos.db.getOrderReceipts();
                if (ktOrders.length == 0) {
                    ktOrders = JSON.parse(this.env.pos.config.order_receipt_tickets);
                }
                console.log(ktOrders);
                ktOrders.forEach(o => {
                    // this.singleActionOrderItemReceiptDone(o, 'Done')
                    if (o && o.new) {
                        o.new.forEach((l) => {
                            //appeCat == l.categ.id && l.default_time && 
                            if (l.is_default_timer_active == 0) {
                                l.default_time_active_time = new Date().getTime();
                                l.is_default_timer_active = 1;
                            }
                        });
                    }
                });
                this.env.pos.db.saveOrderReceipts(ktOrders);

                // this.env.pos.showAllButton = !this.env.pos.showAllButton
                // this.state.showButtons = this.env.pos.showAllButton
            }
            async OnKeydown(event) {
                const order = this.env.pos.get_order();
                if (event.key === 'Enter' && this.state.inputCustomer != '') {
                    const partners = this.env.pos.db.search_partner(this.state.inputCustomer)
                    this.state.countCustomers = partners.length
                    if (partners.length > 1 && partners.length < 10) {
                        let list = []
                        for (let i = 0; i < partners.length; i++) {
                            let p = partners[i]
                            let pName = p.display_name
                            if (p.phone) {
                                pName += this.env._t(' , Phone: ') + p.phone
                            }
                            if (p.mobile) {
                                pName += this.env._t(' , Mobile: ') + p.mobile
                            }
                            if (p.email) {
                                pName += this.env._t(' , Email: ') + p.email
                            }
                            if (p.barcode) {
                                pName += this.env._t(' , Barcode: ') + p.barcode
                            }
                            list.push({
                                id: p.id,
                                label: pName,
                                isSelected: false,
                                item: p
                            })
                        }
                        let {confirmed, payload: client} = await this.showPopup('SelectionPopup', {
                            title: this.env._t('All Customers have Name or Phone/Mobile or Email or Barcode like Your Input: [ ' + this.state.inputCustomer + ' ]'),
                            list: list,
                            cancelText: this.env._t('Close')
                        })
                        if (confirmed) {
                            order.set_client(client);
                            this.state.countCustomers = 0
                            this.state.inputCustomer = ''
                        }
                    } else if (partners.length > 10) {
                        this.env.pos.alert_message({
                            title: this.env._t('Warning'),
                            body: this.env._t('have many Customers with your type, please type correct [name, phone, or email] customer')
                        })
                    } else if (partners.length == 1) {
                        order.set_client(partners[0]);
                        this.state.inputCustomer = ''
                        this.state.countCustomers = 0
                    } else if (partners.length == 0) {
                        this.env.pos.alert_message({
                            title: this.env._t('Warning'),
                            body: this.env._t('Sorry, We not Found any Customer with Your Type')
                        })
                    }
                } else {
                    const partners = this.env.pos.db.search_partner(this.state.inputCustomer)
                    this.state.countCustomers = partners.length
                }
            }
        };

    Registries.Component.extend(ProductScreen, ProductScreenExt);

    const RetailOrderWidgetExt = (OrderWidget) =>
        class extends OrderWidget {
            constructor() {
                super(...arguments);
            }
            async clearCart() {
                let selectedOrder = this.env.pos.get_order();
                if (selectedOrder.orderlines.models.length > 0) {
                    let {confirmed, payload: result} = await this.showPopup('ConfirmPopup', {
                        title: this.env._t('Warning !!!'),
                        body: this.env._t('Are you want remove all Items in Cart ?')
                    })
                    if (confirmed) {
                        selectedOrder.orderlines.models.forEach(l => selectedOrder.remove_orderline(l))
                        selectedOrder.orderlines.models.forEach(l => selectedOrder.remove_orderline(l))
                        selectedOrder.orderlines.models.forEach(l => selectedOrder.remove_orderline(l))
                        selectedOrder.orderlines.models.forEach(l => selectedOrder.remove_orderline(l))
                        selectedOrder.is_return = false;
                        this.env.pos.alert_message({
                            title: this.env._t('Successfully'),
                            body: this.env._t('Order is empty cart !')
                        })
                    }
                } else {
                    this.env.pos.alert_message({
                        title: this.env._t('Warning !!!'),
                        body: this.env._t('Your Order Cart is blank.')
                    })
                }
            }
            _updateSummary() {
                if (this.order && this.order.get_client() && this.env.pos.retail_loyalty) {
                    let points = this.order.get_client_points()
                    let plus_point = points['plus_point']
                    this.order.plus_point = plus_point
                    this.order.redeem_point = points['redeem_point']
                    this.order.remaining_point = points['remaining_point']
                }
                let productsSummary = {}
                let totalItems = 0
                let totalQuantities = 0
                let totalCost = 0
                if (this.order) {
                    for (let i = 0; i < this.order.orderlines.models.length; i++) {
                        let line = this.order.orderlines.models[i]
                        totalCost += line.product.standard_price * line.quantity
                        if (!productsSummary[line.product.id]) {
                            productsSummary[line.product.id] = line.quantity
                            totalItems += 1
                        } else {
                            productsSummary[line.product.id] += line.quantity
                        }
                        totalQuantities += line.quantity
                    }
                }
                const discount = this.order ? this.order.get_total_discounts() : 0;
                this.state.discount = this.env.pos.format_currency(discount);
                const totalWithOutTaxes = this.order ? this.order.get_total_without_tax() : 0;
                this.state.totalWithOutTaxes = this.env.pos.format_currency(totalWithOutTaxes);
                this.state.margin = this.env.pos.format_currency(totalWithOutTaxes - totalCost)
                this.state.totalItems = this.env.pos.format_currency_no_symbol(totalItems)
                this.state.totalQuantities = this.env.pos.format_currency_no_symbol(totalQuantities)
                super._updateSummary();
                const total = this.order ? this.order.get_total_with_tax() : 0;
                if (total <= 0) {
                    this.state.tax = this.env.pos.format_currency(0);
                }
                if (this.env.pos.config.customer_facing_screen) {
                    this.env.pos.trigger('refresh.customer.facing.screen');
                }
                if (this.env.pos.config.iface_customer_facing_display) {
                    this.env.pos.send_current_order_to_customer_facing_display();
                }
                if($('.order-total-btn .pos_order_total').length > 0){
                    $('.order-total-btn .pos_order_total')[0].innerText = this.state.totalWithOutTaxes
                }
            }
        };

    Registries.Component.extend(OrderWidget, RetailOrderWidgetExt);

    return { OrderSummary, OrderWidget, ProductScreen }
})