# -*- coding: utf-8 -*-
# This module and its content is copyright of Technaureus Info Solutions Pvt. Ltd. - ©
# Technaureus Info Solutions Pvt. Ltd 2020. All rights reserved.

from . import res_partner
from . import res_config_settings
from . import venue
from . import booking
from . import booking_dashboard
from . import account_move_line
from . import ir_ui_view
from . import ir_act_window_view
from . import website
