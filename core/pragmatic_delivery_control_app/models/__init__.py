# -*- coding: utf-8 -*-

from . import website_menu
from . import res_partner
from . import res_users
from . import sale_order
from . import stock
from . import delivery_carrier
from . import res_company
from . import res_config_settings
