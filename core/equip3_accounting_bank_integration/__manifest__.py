{
    'name': 'Accounting Bank Integration',
    'version': '1.1.4',
    'category': 'Accounting',
    'author': 'Mochamad Rezki',
    'depends': [
        'account','branch'
    ],
    'data': [
        'views/account_views.xml',
        'views/account_payment_cimb_send_views.xml',
        'views/account_payment_bca_send_views.xml',
#        'views/account_payment_registry_view.xml',
        'views/res_partner_view.xml',
        'data/account_cimb_data.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'auto_install': False,
    'installable': True,
    'application': True
}