from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError
import logging
_logger = logging.getLogger(__name__)


class AccountJournal(models.Model):
    _inherit = "account.journal"

    is_bank_cimb = fields.Boolean(
        string="Bank CIMB Integration", default=False,
        tracking=True)
    bank_cimb_token = fields.Char(
        'Bank CIMB API Token',
        tracking=True)
    bank_cimb_corp_id = fields.Char(
        'Bank CIMB Corporate ID',
        tracking=True)
    is_bank_bca = fields.Boolean(
        string="Bank BCA Integration",
        default=False,
        tracking=True,
    )
    bank_bca_key = fields.Char("Bank BCA API")
    bank_bca_signature = fields.Char("Bank BCA Signature")
    bank_bca_corp_id = fields.Char("Bank BCA Corporate")
    bank_bca_channel_id = fields.Char("Bank BCA Channel ID")
    bank_bca_credential_id = fields.Char("Bank BCA Credential ID")
    bank_bca_token = fields.Char("Bank BCA Token")

    @api.onchange('is_bank_bca')
    def _onchange_is_bank_bca(self):
        payment_method_id = self.env['account.payment.method'].search([('code', '=', 'bank_bca_transfer')])
        if self.is_bank_bca:
            if not self.outbound_payment_method_ids.filtered(lambda x:x._origin.id == payment_method_id[0].id):
                self.outbound_payment_method_ids = [(4, payment_method_id[0].id)]
        elif self.outbound_payment_method_ids.filtered(lambda x:x._origin.id==payment_method_id[0].id):
            self.outbound_payment_method_ids = [(3,payment_method_id[0].id)]

    @api.onchange('is_bank_cimb')
    def _onchange_is_bank_cimb(self):
        payment_method_id = self.env['account.payment.method'].search([('code','=','bank_cimb_transfer')])
        if self.is_bank_cimb:
            if not self.outbound_payment_method_ids.filtered(lambda x:x._origin.id==payment_method_id[0].id):
                self.outbound_payment_method_ids = [(4, payment_method_id[0].id)]
        elif self.outbound_payment_method_ids.filtered(lambda x:x._origin.id==payment_method_id[0].id):
            self.outbound_payment_method_ids = [(3,payment_method_id[0].id)]
    @api.model
    def create(self, vals):
        rec = super(AccountJournal, self).create(vals)
        if rec.is_bank_cimb:
            check_method = self.env.ref('equip3_accounting_bank_integration.mr_account_payment_method_cimb')
            for bank_journal in self:
                bank_journal.outbound_payment_method_ids += check_method
        return rec

    def write(self, vals):
        res = super(AccountJournal, self).write(vals)
        for record in self:
            if record.is_bank_cimb == True and record.is_bank_bca == True:
                raise ValidationError(
                    _("Please check Bank Integration setup! Cannot select Bank Integration more than once!"))
        if 'is_bank_cimb' in vals:
            if not vals['is_bank_cimb']:
                for bank_journal in self:
                    for data in bank_journal.outbound_payment_method_ids:
                        check_method = self.env.ref('equip3_accounting_bank_integration.mr_account_payment_method_cimb')
                        bank_journal.outbound_payment_method_ids -= check_method
            else:
                check_method = self.env.ref('equip3_accounting_bank_integration.mr_account_payment_method_cimb')
                for bank_journal in self:
                    bank_journal.outbound_payment_method_ids += check_method
        return res


class ResBank(models.Model):
    _inherit = "res.bank"

    code = fields.Char(
        'Bank Code',
        tracking=True)
    bank_provider = fields.Selection(
        [
            ('bank_other', 'Other'),
            ('bank_bca', 'BCA')
        ],"Bank Provider", default='bank_other')


class AccountPayment(models.Model):
    _inherit = "account.payment"

    service_type = fields.Selection(
        [
            ('ATM_TRANSFER', 'ATM Transfer'),
            ('SKN_TRANSFER', 'SKN Transfer'),
            ('RTGS_TRANSFER', 'RTGS Transfer')
        ], 'Transfer Service Type')

    mr_show_service_type = fields.Boolean(
        compute='_compute_mr_show_service_type',
    )

    payment_method_id = fields.Many2one('account.payment.method', string='Payment Method',
        readonly=False, store=True,
        compute='_compute_payment_method_id',
        domain="[('id', 'in', available_payment_method_ids), ('code', '!=', check_if_bca_or_cimb)]",
        help="Manual: Get paid by cash, check or any other method outside of Odoo.\n" \
             "Electronic: Get paid automatically through a payment acquirer by requesting a transaction on a card saved by the customer when buying or subscribing online (payment token).\n" \
             "Check: Pay bill by check and print it from Odoo.\n" \
             "Batch Deposit: Encase several customer checks at once by generating a batch deposit to submit to your bank. When encoding the bank statement in Odoo, you are suggested to reconcile the transaction with the batch deposit.To enable batch deposit, module account_batch_payment must be installed.\n" \
             "SEPA Credit Transfer: Pay bill from a SEPA Credit Transfer file you submit to your bank. To enable sepa credit transfer, module account_sepa must be installed ")

    bank_bca_transfer_type = fields.Selection(
        [
            ('LLG', 'LLG'),
            ('RTG', 'RTG')
        ], 'Bank BCA Transfer Type'
    )

    is_bca_transfer_type = fields.Boolean(compute='_compute_show_bca_transfer_type')

    check_if_bca_or_cimb = fields.Char(compute='_check_if_bca_or_cimb')

    @api.depends('journal_id')
    def _check_if_bca_or_cimb(self):
        for payment in self:
            if not payment.journal_id.is_bank_bca:
                payment.check_if_bca_or_cimb = 'bank_bca_transfer'
            else:
                payment.check_if_bca_or_cimb = ''

    @api.depends('payment_method_id')
    def _compute_show_bca_transfer_type(self):
        for payment in self:
            if payment.payment_method_id.code == 'bank_bca_transfer':
                payment.is_bca_transfer_type = True
            else:
                payment.is_bca_transfer_type = False

    @api.depends('payment_method_code','payment_method_id')
    def _compute_mr_show_service_type(self):
        for payment in self:
            if payment.payment_method_code == 'bank_cimb_transfer'\
                    and not payment.payment_method_id.code == 'manual':
                payment.mr_show_service_type = True
            else:
                payment.mr_show_service_type = False
    
    def action_post(self):
        result= super().action_post()
        if self.mr_show_service_type:
            data={
                'payment_date':self.date,
                'partner_id':self.partner_id.id,
                
                'partner_street':self.partner_id.street,
                'partner_street2':self.partner_id.street2,
                'currency_id':self.currency_id.id,
                'amount':self.amount,
                'memo':self.ref,
                'service_type':self.service_type,
                'request_datetime':fields.Datetime.now(),
                'created_by':self.user_id.id,
                'company_id':self.company_id.id,
                'branch_id':self.branch_id.id,
                }
            if self.journal_id:
                bank_id_dic={
                    'bank_account_id':self.journal_id.id,
                    'corp_id':self.journal_id.bank_cimb_corp_id
                    }
                data.update(bank_id_dic)
            if self.partner_bank_id:
                bank_dic={
                    'partner_account_holder':self.partner_bank_id.acc_holder_name,
                    'partner_bank_account_id':self.partner_bank_id.id,
                    'partner_bank_code':self.partner_bank_id.bank_id.code,
                    'partner_bank_bic':self.partner_bank_id.bank_id.bic,
                    'partner_bank_name':self.partner_bank_id.bank_id.name,
                    'partner_bank_street':self.partner_bank_id.bank_id.street,
                    'partner_bank_city':self.partner_bank_id.bank_id.city,
                    'partner_bank_country':self.partner_bank_id.bank_id.country.name
                }
                data.update(bank_dic)
                
            self.env['account.payment.cimb.send'].create(data)
            
        elif self.payment_method_id.code == 'bank_bca_transfer' and self.journal_id.type == 'bank' and self.state == 'posted':
            vals = {
                'access_token':self.journal_id.bank_bca_token,
                'api_key':self.journal_id.bank_bca_key,
                'timestamp':self.create_date,
                'signature':self.journal_id.bank_bca_signature,
                'channel_id':self.journal_id.bank_bca_channel_id,
                'credential_id':self.journal_id.bank_bca_credential_id,
                'source_account_number':self.journal_id.bank_account_id.id,
                'beneficial_account_number': self.partner_bank_id.acc_number,
                'beneficiary_bank_code':self.partner_bank_id.bank_id.code,
                'beneficiary_name':self.partner_bank_id.acc_holder_name,
                'amount':self.amount,
                'currency_code':self.currency_id.id,
                'corporate_id':self.journal_id.bank_bca_corp_id,
                'transfer_type': self.bank_bca_transfer_type,
                'transaction_date':self.date
            }
            self.env['account.payment.bca.send'].create(vals)
        return result
    
class AccountPaymentRegister(models.TransientModel):
    _inherit = "account.payment.register"

    service_type = fields.Selection(
        [
            ('ATM_TRANSFER', 'ATM Transfer'),
            ('SKN_TRANSFER', 'SKN Transfer'),
            ('RTGS_TRANSFER', 'RTGS Transfer'),
        ], 'Transfer Service Type ')
    payment_method_id = fields.Many2one('account.payment.method', string='Payment Method',
        readonly=False, store=True,
        compute='_compute_payment_method_id',
        domain="[('id', 'in', available_payment_method_ids), ('code', '!=', check_if_bca_or_cimb)]",
        help="Manual: Get paid by cash, check or any other method outside of Odoo.\n" \
                                             "Electronic: Get paid automatically through a payment acquirer by requesting a transaction on a card saved by the customer when buying or subscribing online (payment token).\n" \
                                             "Check: Pay bill by check and print it from Odoo.\n" \
                                             "Batch Deposit: Encase several customer checks at once by generating a batch deposit to submit to your bank. When encoding the bank statement in Odoo, you are suggested to reconcile the transaction with the batch deposit.To enable batch deposit, module account_batch_payment must be installed.\n" \
                                             "SEPA Credit Transfer: Pay bill from a SEPA Credit Transfer file you submit to your bank. To enable sepa credit transfer, module account_sepa must be installed ")
    mr_show_service_type = fields.Boolean(
        compute='_compute_mr_show_service_type',
    )
    bank_bca_transfer_type = fields.Selection(
        [
            ('LLG', 'LLG'),
            ('RTG', 'RTG')
        ],'BCA Transfer Type')
    is_bca_transfer_type = fields.Boolean(compute='_compute_show_bca_transfer_type')

    check_if_bca_or_cimb = fields.Char(compute='_check_if_bca_or_cimb')

    @api.depends('journal_id')
    def _check_if_bca_or_cimb(self):
        for payment in self:
            if not payment.journal_id.is_bank_bca:
                payment.check_if_bca_or_cimb = 'bank_bca_transfer'
            else:
                payment.check_if_bca_or_cimb = ''

    @api.depends('payment_method_id')
    def _compute_show_bca_transfer_type(self):
        for payment in self:
            if payment.payment_method_id.code == 'bank_bca_transfer':
                payment.is_bca_transfer_type = True
            else:
                payment.is_bca_transfer_type = False

    @api.depends('payment_method_id')
    def _compute_mr_show_service_type(self):
        for payment in self:
            if payment.payment_method_id.code == 'bank_cimb_transfer' \
                    and not payment.payment_method_id.code == 'manual':
                payment.mr_show_service_type = True
            else:
                payment.mr_show_service_type = False

    def _create_payment_vals_from_wizard(self):
        vals= super()._create_payment_vals_from_wizard()
        new_vals={
            'service_type':self.service_type,
        }
        vals.update(new_vals)
        return vals
