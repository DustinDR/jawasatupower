from odoo import api, fields, models, _
import logging
_logger = logging.getLogger(__name__)


class AccountPaymentCimbSend(models.Model):
    _name = "account.payment.cimb.send"

    transfer_id = fields.Char('Transfer ID')
    payment_date = fields.Date('Payment Date')
    bank_account_id = fields.Many2one('account.journal','Bank Account')
    partner_id = fields.Many2one('res.partner','Partner')
    partner_bank_account_id = fields.Many2one('res.partner.bank','Partner Bank Account')
    partner_account_holder = fields.Char('Partner Account Holder')
    partner_street = fields.Char('Partner Address')
    partner_street2 = fields.Char('Partner Address 2')
    partner_bank_code = fields.Char('Partner Bank Code')
    partner_bank_bic = fields.Char('Partner Bank BIC/SWIFT code')
    partner_bank_name = fields.Char('Partner Bank Name')
    partner_bank_street = fields.Char('Partner Bank Address')
    partner_bank_city = fields.Char('Partner Bank City')
    partner_bank_country = fields.Char('Partner Bank Country')
    currency_id = fields.Many2one('res.currency','Currency')
    amount = fields.Char('Amount')
    memo = fields.Char('Memo')
    service_type = fields.Char('Bank Service Type')
    corp_id = fields.Char('Corporation ID')
    request_id = fields.Char('Request ID')
    request_datetime = fields.Datetime('Created Date')
    api_respond_code = fields.Char('Responde Code')
    created_by = fields.Many2one('res.users',default=lambda self:self.env.user)
    company_id = fields.Many2one('res.company', string="Company",default=lambda self:self.env.company)
    branch_id = fields.Many2one('res.branch',string="Branch")
    state = fields.Selection([
        ('sent', 'Payment Sent'),
        ('confirmed', 'Payment Confirmed'),
        ('failed', 'Failed'),
    ], string='Status', default='sent')
