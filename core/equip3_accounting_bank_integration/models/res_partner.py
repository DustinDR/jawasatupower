from odoo import api, fields, models, _
class AccPartner(models.Model):
    _inherit='res.partner'
    
    bank_info = fields.Char('Bank info', compute="_compute_description_name")
    
    @api.depends('bank_ids','name')
    def _compute_description_name(self):
        for partner in self:
            partner.bank_info = 'The first partner’s bank account is the default bank account'