from odoo import api, fields, models, _
import logging
_logger = logging.getLogger(__name__)


class AccountPaymentBCASend(models.Model):
    _name = "account.payment.bca.send"
    _description = "BCA Send Payment"

    name = fields.Char('Transaction ID')
    access_token = fields.Char('Access Token')
    api_key = fields.Char('Api Key')
    timestamp = fields.Datetime('Timestamp')
    signature = fields.Char('Signature')
    channel_id = fields.Char('Channel ID')
    credential_id = fields.Char('Credential ID')
    # status = fields.Char()
    state = fields.Selection([
        ('sent', 'Payment Sent'),
        ('confirmed', 'Payment Confirmed'),
        ('failed', 'Failed'),
    ], string='Status', default='sent')
    transaction_date = fields.Date('Transaction Date')
    source_account_number = fields.Many2one('res.partner.bank', 'Source Account Number')
    beneficial_account_number = fields.Char('Beneficial Account Number')
    beneficiary_bank_code = fields.Char('Beneficiary Bank Code')
    beneficiary_name = fields.Char('Beneficiary Name')
    currency_code = fields.Many2one('res.currency', 'Currency')
    amount = fields.Monetary(currency_field='currency_code', string="Amount", tracking=True)
    transfer_type = fields.Selection(
        [
            ('LLG', 'LLG'),
            ('RTG', 'RTG')
        ],'Transfer Type')
    beneficiary_cust_type = fields.Char('Beneficiary Customer Type')
    beneficiary_cust_residence = fields.Char('Beneficiary Customer Residence')
    remark1 = fields.Char('Remark1')
    remark2 = fields.Char('Remark2')
    beneficiary_email = fields.Char('Beneficiary Email')
    reference_id = fields.Char('Reference')
    corporate_id = fields.Char('Corporate ID')
    memo = fields.Char('Memo')
    api_respond_code = fields.Char('Response Code')
