# -*- coding: utf-8 -*-
{
    'name': "E-Faktur",
    'author': "Irfan Suendi",
    'category': 'Accounting',
    'version': '1.3.2',
    'depends': ['account','web','l10n_id_efaktur','inputmask_widget'],
    'data': [
        'security/ir.model.access.csv',
        'views/efaktur_menuitem.xml',
        'views/efaktur_views.xml',
        'views/account_views.xml',
        'views/res_partner_views.xml',
        'views/account_ebupot_views.xml',
        'wizards/product_wizard_view.xml',
        'wizards/partner_wizard_view.xml',
        # 'views/buttton.xml',
    ],
    # 'qweb': ['static/src/xml/efaktur_template.xml'],
    'demo': [],
    'auto_install': False,
    'installable': True,
    'application': True
}
