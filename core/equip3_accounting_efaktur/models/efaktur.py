# -*- coding: utf-8 -*-

import time
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.osv import expression
from odoo.tools.misc import format_date

class AccountEFaktur(models.Model):
    _name = "account.efaktur"

    year = fields.Char(string='Year', size=4)
    name = fields.Char(string='eFaktur Number', size=15, help="E-Faktur Format xxx-xx-xxxxxxxx")
    is_used = fields.Boolean(string='Is Used',default=False, compute='_compute_is_use')
    invoice_id = fields.One2many('account.move','nomor_seri', string='Invoice', domain="[('nomor_seri', 'in', [False,'']), ('state', 'not in', ['cancel','draft']),('move_type', 'in', ['out_invoice','out_refund','out_receipt'])]")
    partner_id = fields.Many2one('res.partner', string='Customer', related='invoice_id.partner_id')
    invoice_date = fields.Date(string='last use on invoice date')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'The Nomor Seri Faktur Pajak is already generated. Please check your Nomor Seri Faktur Pajak')
    ]

    @api.depends('partner_id')
    def _compute_is_use(self):
        for rec in self:
            if rec.partner_id.id == False:
                rec.is_used = False
            else:
                rec.is_used = True
                inv_id=[]
                for x in rec.invoice_id:
                    if not isinstance(x.id, models.NewId):
                        inv_id.append(x._origin.id)
                inv = self.env['account.move'].search([('id','in',inv_id)], order='invoice_date desc',limit=1)
                if len(inv)>0:
                    rec.invoice_date = inv[0].invoice_date

    @api.model
    def default_get(self, default_fields):
        """If we're creating a new account through a many2one, there are chances that we typed the account invoice_date
        instead of its name. In that case, switch both fields values.
        """
        if 'name' not in default_fields and 'invoice_date' not in default_fields:
            return super().default_get(default_fields)
        default_name = self._context.get('default_name')
        default_invoice_date = self._context.get('default_invoice_date')
        if default_name and not default_invoice_date:
            try:
                default_invoice_date = default_name
            except ValueError:
                pass
            if default_invoice_date:
                default_name = False
        contextual_self = self.with_context(default_name=default_name, default_invoice_date=default_invoice_date)
        return super(AccountEFaktur, contextual_self).default_get(default_fields)

    # @api.model
    # def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
    #     args = args or []
    #     domain = []
    #     if name:
    #         domain = ['|', ('name', '= ilike', name.split(' ')[0] + '%'), ('name', operator, name)]
    #         if operator in expression.NEGATIVE_TERM_OPERATORS:
    #             domain = ['&', '!'] + domain[1:]
    #     return self._search(expression.AND([domain, args]), limit=limit, access_rights_uid=name_get_uid)

    def name_get(self):
        result = []
        for account in self:
            name = account.name + ' ' +  str(account.invoice_date)
            result.append((account.id, name))
        return result

    def name_get(self):
        result = []
        for res in self:
            if res.is_used == False:
                name = res.name
            else:
                invoice=''
                for inv in self.invoice_id:
                    invoice = format_date(self.env, inv.invoice_date) + ' '
                name = res.name + ' Last used on ' + format_date(self.env, res.invoice_date)
                # name = res.name + ' Last used on' + invoice
            result.append((res.id, name))
        return result


class AccountEFakturGenerate(models.Model):
    _name = "account.efaktur.generate"

    year = fields.Char(string='Year', size=4)
    start = fields.Char(string='Start', size=15, help="E-Faktur Format xxx-xx-xxxxxxxx")
    end = fields.Char(string='End', size=15, help="E-Faktur Format xxx-xx-xxxxxxxx")

    @api.onchange('year')
    def onchange_year(self):
        warning = {}
        if self.year and (self.year.isdigit() == False or (self.year.isdigit() == True and len(str(self.year)) > 4)):
            self.year = ''
            warning = {'title': 'Value Error', 'message': "Invalid year!"}
        return {'warning': warning}
        
    def _prepare_efaktur_line(self,prefix,year,start,end):
        list_line=[]
        for x in range(int(start),int(end)+1):
            lines_dict={
                        'year': year,
                        'name': str(prefix) + str(self._addnumb(x)),                            
                        }
            list_line.append(lines_dict)
        return list_line

    def _addnumb(self,number):
        numb = str(number)        
        while len(numb) < 8:
            numb = str('0') + str(numb)
        return numb

    def confirm(self):
        prefix=''
        years=''
        start_number = ''
        end_number = ''
        for vals in self:
            if vals.start and vals.end:
                if len(vals.start) < 15 or len(vals.end) < 15:
                    raise UserError(_("Please check your region code and year code at your Nomor E-Faktur !"))
            else:
                raise UserError(_("Please check your region code and year code at your Nomor E-Faktur !"))
            start = vals.start
            end = vals.end
            if start[3] != '-' or start[6] != '-' or end[3] != '-' or end[6] != '-' or start[4:6] != end[4:6] or start[:3] != end[:3]:
                raise UserError(_("Please check your region code and year code at your Nomor E-Faktur !"))
            prefix = vals.start[:7]
            years = vals.year
            start_number = vals.start[7:]
            end_number = vals.end[7:]
            if start[4:6].isnumeric() == False or end[4:6].isnumeric() == False or start[:3].isnumeric() == False or end[:3].isnumeric() == False or years.isnumeric() == False or end_number.isnumeric() == False or end_number.isnumeric() == False:
                raise UserError(_("Please check your region code and year code at your Nomor E-Faktur !"))
        list_line = self._prepare_efaktur_line(prefix,years,start_number,end_number)
        efaktur = self.env['account.efaktur'].create(list_line)
        return {
                'name':  _('E-Faktur'),
                'type': 'ir.actions.act_window',
                'view_mode': 'tree,kanban,form',
                'res_model': 'account.efaktur',
                'views_id': self.env.ref('equip3_accounting_efaktur.view_account_efaktur_tree').id,
                'domain': [('name', 'in', [x['name'] for x in list_line])]
            }

class EfakturExport(models.Model):
    _name = 'efaktur.export'

    partner_id = fields.Many2one('res.partner', string='Customer', domain="[('country_id','=',100)]")
    tax_report_id = fields.Selection([
        ('in', 'PPN Masukan'),('out','PPN Keluaran')
    ], string='Tax Report', default='out')
    date_from = fields.Date(string='Start Date')
    date_to = fields.Date(string='End Date')
    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.company, tracking=True, readonly=True)
    unduh = fields.Binary(
        string='Download All E-Faktur',
    )
    
    
    @api.onchange('date_from','date_to')
    def _checkvalid(self):
        for rec in self:
            if rec.date_from and rec.date_to:
                check = time.mktime(rec.date_to.timetuple()) - time.mktime(rec.date_from.timetuple())
                if check < 0:
                    raise UserError('End Date Must Be Greater Than Start Date!')
    
    def confirm(self):
        filt = [('state','=','posted'), ('l10n_id_tax_number','!=',''), ('move_type','=','out_invoice')]
        # filt = [('state','=','posted'), ('move_type','=','out_invoice')]
        for rec in self:
            if rec.partner_id:
                filt.append(('partner_id', '=', f'{rec.partner_id.name}'))
            if rec.date_from:
                filt.append(('invoice_date','>=',f'{rec.date_from}'))
            if rec.date_to:
                filt.append(('invoice_date','<=',f'{rec.date_to}'))
            return {
                'name':  _('PPN Keluaran'),
                'type': 'ir.actions.act_window',
                'view_mode': 'tree,kanban,form',
                'res_model': 'account.move',
                'views_id': self.env.ref('account.view_out_invoice_tree').id,
                'domain': f"{filt}",
                'context' : {'default_move_type': 'out_invoice',
                             'is_ppn_invisible': False,
                             'def_invisible': True,
                             'partner_id.l10n_id_pk': True,
                             'partner_id.vat': True
                             },
            }

class ExportEfaktur(models.Model):
    _name = 'export.efaktur'

    partner_id = fields.Many2one('res.partner', string='Vendor', domain="[('country_id','=',100)]")
    tax_report_id = fields.Selection([
        ('in', 'PPN Masukan'),('out','PPN Keluaran')
    ], string='Tax Reports', default='in')
    date_from = fields.Date(string='Start Date')
    date_to = fields.Date(string='End Date')
    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.company, tracking=True, readonly=True)
    
    @api.onchange('date_from','date_to')
    def _checkvalid(self):
        for rec in self:
            if rec.date_from and rec.date_to:
                check = time.mktime(rec.date_to.timetuple()) - time.mktime(rec.date_from.timetuple())
                if check < 0:
                    raise UserError('End Date Must Be Greater Than Start Date!')
    
    def confirm(self):
        filt = [('state','=','posted'), ('l10n_id_tax_number','!=',''), ('move_type','=','in_invoice')]
        # filt = [('state','=','posted'), ('move_type','=','in_invoice')]
        for rec in self:
            if rec.partner_id:
                filt.append(('partner_id', '=', f'{rec.partner_id.name}'))
            if rec.date_from:
                filt.append(('invoice_date','>=',f'{rec.date_from}'))
            if rec.date_to:
                filt.append(('invoice_date','<=',f'{rec.date_to}'))
            return {
                'name':  _('PPN Masukan'),
                'type': 'ir.actions.act_window',
                'view_mode': 'tree,kanban,form',
                'res_model': 'account.move',
                'views_id': self.env.ref('account.view_in_invoice_tree').id,
                'domain': f"{filt}",
                'context' : {'default_move_type': 'in_invoice',
                             'is_ppn_invisible': False,
                             'def_invisible': True,
                             'partner_id.l10n_id_pk':True,
                             'partner_id.vat':True
                             }
            }
