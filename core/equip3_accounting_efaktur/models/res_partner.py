# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class AccountMove(models.Model):
    _inherit = "res.partner"

    faktur_pajak_gabungan = fields.Boolean(string='Faktur Pajak Gabungan')
    blok = fields.Char()
    kelurahan = fields.Char()
    kecamatan = fields.Char()
    rukun_tetangga = fields.Char(string="RT")
    rukun_warga = fields.Char(string="RW")
    street_number = fields.Char(string="Nomor")
    street_number2 = fields.Char(string="Nomor2")

    def check_vat(self, country_code):
        pass

    
