# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from lxml import etree
import xml.etree.ElementTree as ET
from odoo.exceptions import UserError, ValidationError
import base64
import inspect
import os

class AccountMove(models.Model):
    _inherit = "account.move"

    status_code = fields.Selection(selection=[
                  ('0', '0'),
                  ('1', '1')
                  ], string='Kode Status')
    nomor_seri = fields.Many2one('account.efaktur', string="Nomor Seri")
    ebupot_id = fields.Many2one('account.ebupot', string="E-Bupot")
    code = fields.Char(string='Country Code', compute='check_code')
    check = fields.Boolean(string='Check field', compute='check_code')
    check_invisible = fields.Boolean(string='Check invis', compute='check_code')
    total_tax_ppn = fields.Monetary(string="Taxes", readonly=True, store=True, compute='_compute_invoice_taxes_ppn')
    total_inv_ppn = fields.Monetary(string="Subtotal", readonly=True, store=True, compute='_compute_invoice_taxes_ppn')
    subtotal_inv_ppn = fields.Monetary(string="Total", readonly=True, store=True, compute='_compute_invoice_taxes_ppn')
    tax_number_bupot = fields.Char(string='Nomor e-BupotUnifikasi')
    kode_dokumen = fields.Selection(selection=[
                  ('1', '1'),
                  ('2', '2')
                  ], string='Kode Dokumen')
    kode_seri = fields.Char(string='Kode Seri')
    nomor_seri_bupot = fields.Many2one('account.ebupot', string="Nomor Seri Bukti e-BupotUnifikasi")
    check_invisible_ebupot = fields.Boolean(string='Check invis Bupot', compute='check_code')
    ebupot_template = fields.Binary('Template', compute="_get_template")

    # efaktur_id = fields.Many2one('account.efaktur', string='E-Faktur', check_company=True, help="E-Faktur Number")

    # def write(self, vals_list):
    #     res_ids = super(AccountMove, self).write(vals_list)
    #     if 'nomor_seri' in vals_list:
    #         efaktur = self.env['account.efaktur'].browse(self.nomor_seri.id)
    #         efaktur.write({'invoice_id': [[6, False, [self.id]]]})
    #     return res_ids

    # @api.model
    # def create(self, vals_list):
    #     res_ids = super(AccountMove, self).create(vals_list)
    #     if 'nomor_seri' in vals_list:
    #         efaktur = self.env['account.efaktur'].browse(vals_list['nomor_seri'])
    #         efaktur.write({'invoice_id': [[6, False, [res_ids.id]]]})
    #     return res_ids


    def _get_template(self):
        dir_name = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        static_path = os.path.dirname(dir_name)
        css_path = os.path.join(static_path, 'static/src/file/FORMAT_UPLOAD_EBUPOT_UNIFIKASI_R1.xls')
        self.ebupot_template = base64.b64encode(open(css_path, "rb").read())

    def download_ebupot(self):
        active_ids = self.env.context.get('active_ids')
        if not active_ids:
            return ''
        for moveid in active_ids:
            return {
                    'type': 'ir.actions.act_url',
                    'name': 'ebupot',
                    'url': '/web/content/account.move/%s/ebupot_template/FORMAT_UPLOAD_EBUPOT_UNIFIKASI_R1.xls?download=true' %(moveid),
                    }

    @api.onchange('kode_dokumen', 'kode_seri', 'nomor_seri_bupot')
    def _onchange_ebupot(self):
        for rec in self:
            if rec.code:
                if rec.move_type == 'in_invoice':
                    rec.tax_number_bupot = str(rec.kode_dokumen or '') + str(rec.kode_seri or '') + '-' + str(rec.nomor_seri_bupot.name or '')

    @api.depends('partner_id')
    def _compute_need_kode_transaksi(self):
        for move in self:
            move.l10n_id_need_kode_transaksi = False

    @api.onchange('partner_id','partner_id.faktur_pajak_gabungan')
    def _onchange_domain(self):
        res={}
        if self.partner_id.faktur_pajak_gabungan == True:
            domain_line = "['|', ('invoice_id','=', False),'&', ('invoice_id', '!=', False), ('partner_id', '=', partner_id)]"
        else:
            domain_line = "[('invoice_id', '=', False)]"
        res['domain'] = {'nomor_seri' : domain_line}
        return res

    @api.onchange('partner_id')
    def check_code(self):   
        for rec in self:
            rec.code=rec.partner_id.country_id.code            
            if rec.code == 'ID':
                if rec.move_type in ['out_invoice']:
                    rec.check = False
                    rec.check_invisible = False
                    rec.check_invisible_ebupot = True

                    if rec.partner_id.l10n_id_pkp == True:
                        rec.check = False
                        rec.check_invisible = False
                    else:
                        rec.check = True
                        rec.check_invisible = True
                elif rec.move_type in ['in_invoice']:
                    rec.check = True
                    rec.check_invisible = True
                    rec.check_invisible_ebupot = False
                else:
                    rec.check = True
                    rec.check_invisible = True
                    rec.check_invisible_ebupot = True
            else:
                rec.check = True
                rec.check_invisible = True
                rec.check_invisible_ebupot = True

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(AccountMove, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if view_type == 'form':
            doc = etree.XML(result['arch'])
            tax_field = doc.xpath("//field[@name='l10n_id_tax_number']")
            ebupot_field = doc.xpath("//field[@name='tax_number_bupot']")
            move_type = self._context.get('default_move_type')
            if move_type in ['in_invoice']:
                tax_field[0].set("placeholder", "xxx-xxx-xx-xxxxxxxx")
                tax_field[0].set("widget", "mask")
                tax_field[0].set("data-inputmask", "'mask': '999-999-99-99999999'")
                result['arch'] = etree.tostring(doc, encoding='unicode')
            if move_type in ['out_invoice']:
                ebupot_field[0].set("placeholder", "x-xx-xxxxxxxx")
                ebupot_field[0].set("widget", "mask")
                ebupot_field[0].set("data-inputmask", "'mask': '9-99-99999999'")
                result['arch'] = etree.tostring(doc, encoding='unicode')        
        return result

    @api.onchange('status_code', 'nomor_seri','l10n_id_kode_transaksi')
    def country_tax_number(self):
        for rec in self:
            if rec.code:
                rec.l10n_id_tax_number = str(rec.l10n_id_kode_transaksi or '') + str(rec.status_code or '') + '-' + str(rec.nomor_seri.name or '')

    @api.constrains('l10n_id_kode_transaksi', 'line_ids')
    def _constraint_kode_ppn(self):
        ppn_tag = self.env.ref('l10n_id.ppn_tag')
        for move in self.filtered(lambda m: m.l10n_id_kode_transaksi != '08'):
            if any(ppn_tag.id in line.tax_tag_ids.ids for line in move.line_ids if line.exclude_from_invoice_tab is False and not line.display_type) \
                    and any(ppn_tag.id not in line.tax_tag_ids.ids for line in move.line_ids if line.exclude_from_invoice_tab is False and not line.display_type):
                # raise UserError(_('Cannot mix VAT subject and Non-VAT subject items in the same invoice with this kode transaksi.'))
                pass
        for move in self.filtered(lambda m: m.l10n_id_kode_transaksi == '08'):
            if any(ppn_tag.id in line.tax_tag_ids.ids for line in move.line_ids if line.exclude_from_invoice_tab is False and not line.display_type):
                raise UserError('Kode transaksi 08 is only for non VAT subject items.')

    def _post(self, soft=True):
        """Set E-Faktur number after validation."""
        for move in self:
            if move.l10n_id_need_kode_transaksi:
                # if not move.l10n_id_kode_transaksi:
                #     raise ValidationError(_('You need to put a Kode Transaksi for this partner.'))
                if move.l10n_id_replace_invoice_id.l10n_id_tax_number:
                    if not move.l10n_id_replace_invoice_id.l10n_id_attachment_id:
                        raise ValidationError(_('Replacement invoice only for invoices on which the e-Faktur is generated. '))
                    rep_efaktur_str = move.l10n_id_replace_invoice_id.l10n_id_tax_number
                    move.l10n_id_tax_number = '%s1%s' % (move.l10n_id_kode_transaksi, rep_efaktur_str[3:])
                else:
                    efaktur = self.env['l10n_id_efaktur.efaktur.range'].pop_number(move.company_id.id)
                    # if not efaktur:
                    #     raise ValidationError(_('There is no Efaktur number available.  Please configure the range you get from the government in the e-Faktur menu. '))
                    try:
                        move.l10n_id_tax_number = '%s0%013d' % (str(move.l10n_id_kode_transaksi), efaktur)
                    except:
                        move.l10n_id_tax_number = move.l10n_id_tax_number
                        

        return super()._post(soft)


    @api.depends('invoice_line_ids.quantity','invoice_line_ids.price_unit','invoice_line_ids.tax_ids', 'invoice_line_ids.tax_ids_ppn', 'state')
    def _compute_invoice_taxes_ppn(self):
        for sheet in self:
            sheet.subtotal_inv_ppn = sum(sheet.invoice_line_ids.mapped('price_total_ppn'))
            sheet.total_tax_ppn = sum(sheet.invoice_line_ids.mapped('taxes_ppn'))
            sheet.total_inv_ppn = sum(sheet.invoice_line_ids.mapped('amount_ppn'))


class AccountTax(models.Model):
    _inherit = "account.tax"

    is_ppn = fields.Boolean(string='Is PPN')

class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    tax_ids_ppn = fields.Many2many(comodel_name='account.tax', string="Taxes", compute='_get_tax_id', default=False)
    price_total_ppn = fields.Monetary(string="Total", readonly=True, store=True)
    amount_ppn = fields.Monetary(string="Subtotal", readonly=True, store=True)
    taxes_ppn = fields.Monetary(string="Tax", readonly=True, store=True)


    @api.depends('quantity', 'price_unit', 'tax_ids', 'price_tax','price_subtotal', 'tax_ids_ppn')
    def _get_tax_id(self):
        for sheet in self:
            sheet.tax_ids_ppn = False
            if self._context.get('default_move_type') != 'entry':
                if sheet.tax_ids:
                    for ppn in sheet.tax_ids:
                        if ppn.is_ppn == True:
                            sheet.tax_ids_ppn += ppn
                rec = self._get_price_total_and_subtotal_ppn_model(price_unit=sheet.price_unit, quantity=sheet.quantity, currency=sheet.currency_id, product=sheet.product_id, partner=sheet.partner_id, taxes=sheet.tax_ids_ppn)
                print('relk')
                print(rec)
                sheet.amount_ppn = rec['amount_ppn']
                sheet.price_total_ppn = rec['price_total_ppn']
                sheet.taxes_ppn = rec['taxes_ppn']
            
    def _get_price_total_and_subtotal_ppn_model(self, price_unit, quantity, currency, product, partner, taxes):
        res = {}
        # Compute 'price_subtotal'.
        line_discount_price_unit = price_unit
        subtotal = quantity * line_discount_price_unit
        # Compute 'price_total'.
        if taxes:
            force_sign = 1
            taxes_res = taxes._origin.with_context(force_sign=force_sign).compute_all(line_discount_price_unit,
                                                                                      quantity=quantity,
                                                                                      currency=currency,
                                                                                      product=product, partner=partner)
            res['amount_ppn'] = taxes_res['total_excluded']
            res['price_total_ppn'] = taxes_res['total_included']
            res['taxes_ppn'] = taxes_res['total_included'] - taxes_res['total_excluded']
        else:
            res['price_total_ppn'] = res['amount_ppn'] = subtotal
            res['taxes_ppn'] = 0

        if currency:
            res = {k: currency.round(v) for k, v in res.items()}
        return res