# -*- coding: utf-8 -*-

import time
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.osv import expression
from odoo.tools.misc import format_date
import base64
import inspect
import os

class AccountEBupot(models.Model):
    _name = "account.ebupot"
    _description = "Account E-Bupot"

    year = fields.Char(string='Year', size=4)
    name = fields.Char(string='eBupot Number', size=15, help="E-Faktur Format xxx-xx-xxxxxxxx")
    is_used = fields.Boolean(string='Is Used',default=False)
    invoice_ids = fields.One2many('account.move','ebupot_id', string='Invoice', domain="[('ebupot_id', 'in', [False,'']), ('state', 'not in', ['cancel','draft']),('move_type', 'in', ['out_invoice','out_refund','out_receipt'])]")
    tax_report_id = fields.Selection(selection=[
                  ('out', 'e-BupotUnifikasi')
                  ], string='Tax Report')
    date_from = fields.Date(string='Start Date')
    date_to = fields.Date(string='End Date')
    partner_id = fields.Many2one('res.partner', string='Vendor')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company.id,readonly=True)
    ebupot_template = fields.Binary('Template', compute="_get_template")

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'The Nomor Seri Faktur Pajak is already generated. Please check your Nomor Seri Faktur Pajak')
    ]

    def _get_template(self):
        dir_name = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        static_path = os.path.dirname(dir_name)
        path = os.path.join(static_path, 'static/src/file/FORMAT_UPLOAD_EBUPOT_UNIFIKASI_R1.xls')
        self.ebupot_template = base64.b64encode(open(path, "rb").read())

    def confirm_ebupot(self):
        return {
                'type': 'ir.actions.act_url',
                'name': 'ebupot',
                'url': '/web/content/account.ebupot/%s/ebupot_template/FORMAT_UPLOAD_EBUPOT_UNIFIKASI_R1.xls?download=true' %(self.id),
                'target':'new'
                }
    # def confirm_ebupot(self):
    #     return True


class AccountEBupotGenerate(models.Model):
    _name = "account.ebupot.generate"
    _description = "Account E-Bupot Generate"

    year = fields.Char(string='Year', size=4)
    start = fields.Char(string='Start', size=11, help="E-Faktur Format xxx-xxxxxxx")
    end = fields.Char(string='End', size=11, help="E-Faktur Format xxx-xxxxxxx")

    @api.onchange('year')
    def onchange_year(self):
        warning = {}
        if self.year and (self.year.isdigit() == False or (self.year.isdigit() == True and len(str(self.year)) > 4)):
            self.year = ''
            warning = {'title': 'Value Error', 'message': "Invalid year!"}
        return {'warning': warning}
        
    def _prepare_ebupot_line(self,prefix,year,start,end):
        list_line=[]
        for x in range(int(start),int(end)+1):
            lines_dict={
                        'year': year,
                        'name': str(self._addnumb(x)),                            
                        }
            list_line.append(lines_dict)
        return list_line

    def _addnumb(self,number):
        numb = str(number)
        while len(numb) < 7:
            numb = str('0') + str(numb)
        return numb

    def confirm(self):
        prefix=''
        years=''
        start_number = ''
        end_number = ''
        for vals in self:
            if vals.start and vals.end:
                if len(vals.start) < 7 or len(vals.end) < 7:
                    raise UserError(_("Please check your code at your Nomor E-Ebupot !"))
            else:
                raise UserError(_("Please check your code at your Nomor E-Ebupot !"))
            start = vals.start
            end = vals.end
            # if start[3] != '-' or end[3] != '-' or start[:3] != end[:3]:
            #     raise UserError(_("Please check your region code and year code at your Nomor E-Ebupot !"))
            prefix = vals.start[:4]
            years = vals.year
            if start.isnumeric() == False or end.isnumeric() == False or years.isnumeric() == False:
                raise UserError(_("Please check your code at your Nomor E-Ebupot !"))
        list_line = self._prepare_ebupot_line(prefix, years, start, end)
        ebupot = self.env['account.ebupot'].create(list_line)
        return {
                'name':  _('E-Bupot'),
                'type': 'ir.actions.act_window',
                'view_mode': 'tree,kanban,form',
                'res_model': 'account.ebupot',
                'views_id': self.env.ref('equip3_accounting_efaktur.view_account_ebupot_tree').id,
                'domain': [('name', 'in', [x['name'] for x in list_line])]
            }