# -*- coding: utf-8 -*-

{
    'name': 'equip3_kitchen_operations',
    'version': '1.1.14',
    'category': '',
    'description': '',
    'author': 'HashMicro',
    'website': 'www.hashmicro.com',
    'depends': [
        "mrp",
        "web",
        "mail",
        "stock",
        "branch",
        "account",
        "stock_account",
        "purchase_request",
        "general_template",
        "equip3_kitchen_masterdata",
        "equip3_kitchen_accessright_settings",
        "equip3_inventory_operation",
        "equip3_manuf_masterdata"
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence_data.xml',
        'templates/dashboard_templates.xml',
        'views/safety_stock_views.xml',
        'views/kitchen_production_views.xml',
        'views/product_template_views.xml',
        'views/product_product_views.xml',
        'views/kitchen_menuitems.xml',
        'views/purchase_request_views.xml',
        'views/stock_picking_views.xml',
        'report/kitchen_exclusive_template.xml',
        'report/kitchen_cooking_list_template.xml',
        'report/kitchen_cooking_list_report.xml',
        'wizard/kitchen_cooking_list_views.xml'
    ],
    'qweb': [
        'static/src/xml/dashboard.xml',
        'static/src/xml/kitchen_template.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
