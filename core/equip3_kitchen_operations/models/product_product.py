from odoo import models, fields, api, _
from odoo.http import request
from odoo.tools import float_round
from odoo.exceptions import UserError
from lxml import etree
import json


class ProductProduct(models.Model):
    _inherit = 'product.product'

    def _get_record_type(self):
        record_type = self.env.context.get('default_record_type')
        if record_type in ['kitchen', 'assemble', 'disassemble']:
            return record_type
        if self.env.company.central_kitchen:
            return 'kitchen'

    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(ProductProduct, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
                                                          submenu=submenu)
        if view_type != 'tree':
            return res

        kitchen_view_id = self.env.ref('equip3_kitchen_operations.view_kitchen_dashboard')
        if view_id != kitchen_view_id.id:
            return res

        record_type = self._get_record_type()
        if not record_type:

            action_param = self.env.context.get('params', dict()).get('action', False)

            if not action_param:
                raise UserError(_("Record type is not defined!"))

            actions = {}
            for rtype in ['kitchen', 'assemble', 'disassemble']:
                action_id = self.env.ref('equip3_kitchen_operations.action_view_dashboard_%s' % rtype)[0].id
                actions[action_id] = rtype
            record_type = actions.get(action_param)

            if not record_type:
                raise UserError(_("Record type is not defined!"))

        doc = etree.XML(res['arch'])
        to_produce_qty = doc.xpath("//field[@name='to_produce_qty']")
        action_produce = doc.xpath("//button[@name='action_produce']")

        record_type = record_type == 'kitchen' and 'produce' or record_type

        if to_produce_qty:
            if record_type == 'disassemble':
                if 'modifiers' in to_produce_qty[0].attrib:
                    modifiers = json.loads(to_produce_qty[0].attrib['modifiers'])
                    modifiers['column_invisible'] = 'true'
                    to_produce_qty[0].set("invisible", "1")
                    to_produce_qty[0].set("modifiers", json.dumps(modifiers))

            to_produce_qty[0].set("string", "To %s" % record_type.title())

        if action_produce:
            action_produce[0].set("string", record_type.title())

        res['arch'] = etree.tostring(doc, encoding='unicode')
        return res

    def action_view_dashboard_kitchen(self):
        if self.env.user.has_group('equip3_kitchen_accessright_settings.group_central_kitchen_is_a_chef'):
            xml_id = 'equip3_kitchen_operations.action_view_dashboard_kitchen'
        else:
            xml_id = 'equip3_kitchen_operations.action_view_dashboard_kitchen_no_access'
        return self.env['ir.actions.actions']._for_xml_id(xml_id)

    def _prepare_kitchen_context(self):
        context = {
            'from_date': self.env.context.get('from_date', False) or '',
            'to_date': self.env.context.get('to_date', False) or '',
            'warehouse': self.env.context.get('warehouse', False)
        }
        return context

    @api.model
    def retrieve_kitchen_dashboard(self):
        warehouses = {}
        for warehouse in self.env['stock.warehouse'].search([]):
            warehouses[warehouse.id] = warehouse.name

        record_type = self._get_record_type()
        context = self._prepare_kitchen_context()

        has_access = True if record_type != 'kitchen' else self.env.user.has_group(
            'equip3_kitchen_accessright_settings.group_central_kitchen_is_a_chef')
        kitchen_context = {'warehouses': warehouses, 'context': context, 'has_access': has_access}
        request.session['kitchen_context'] = context

        return kitchen_context

    @api.depends('stock_move_ids.product_qty', 'stock_move_ids.state')
    @api.depends_context('from_date', 'to_date', 'warehouse')
    def _compute_kitchen_quantities(self):
        products = self.filtered(lambda p: p.type != 'service')
        res = products._compute_kitchen_quantities_dict(
            self._context.get('from_date'),
            self._context.get('to_date')
        )
        for product in products:
            product.inventory_quantity = res[product.id]['inventory_quantity']
            product.safety_stock_qty = res[product.id]['safety_stock_qty']
            product.to_produce_qty = res[product.id]['to_produce_qty']

        # Services need to be set with 0.0 for all quantities
        services = self - products
        services.inventory_quantity = 0.0
        services.safety_stock_qty = 0.0
        services.to_produce_qty = 0.0

    def _get_safety_stock_qty(self, warehouse):
        self.ensure_one()
        domain = [('warehouse_id', '=', warehouse)] if warehouse else []
        safety_stock = self.env['safety.stock'].search(domain)
        safety_stock_lines = safety_stock.mapped('stock_line_ids')
        product_stock_lines = safety_stock_lines.filtered(lambda s: s.product_id == self)
        return sum(product_stock_lines.mapped('product_qty'))

    def _compute_kitchen_quantities_dict(self, from_date, to_date):
        domain_quant_loc, domain_move_in_loc, domain_move_out_loc = self._get_domain_locations()
        domain_quant = [('product_id', 'in', self.ids)] + domain_quant_loc

        # only to_date as to_date will correspond to qty_available
        dates_in_the_past = False
        to_date = fields.Datetime.to_datetime(to_date)
        if to_date and to_date < fields.Datetime.now():
            dates_in_the_past = True

        domain_move_in = [('product_id', 'in', self.ids)] + domain_move_in_loc
        domain_move_out = [('product_id', 'in', self.ids)] + domain_move_out_loc

        domain_move_in_done, domain_move_out_done = [], []
        if dates_in_the_past:
            domain_move_in_done = list(domain_move_in)
            domain_move_out_done = list(domain_move_out)

        if from_date:
            date_date_expected_domain_from = [('date', '>=', from_date)]
            domain_move_in += date_date_expected_domain_from
            domain_move_out += date_date_expected_domain_from

        if to_date:
            date_date_expected_domain_to = [('date', '<=', to_date)]
            domain_move_in += date_date_expected_domain_to
            domain_move_out += date_date_expected_domain_to

        Move = self.env['stock.move'].with_context(active_test=False)
        QuantInv = self.env['stock.quant'].with_context(active_test=False, inventory_mode=True)

        domain_move_out_todo = [('state', 'in',
                                 ('waiting', 'confirmed', 'assigned', 'partially_available'))] + domain_move_out
        moves_out_res = dict((item['product_id'][0], item['product_qty']) for item in
                             Move.read_group(domain_move_out_todo, ['product_id', 'product_qty'], ['product_id'],
                                             orderby='id'))
        quants_inv_res = dict((item['product_id'][0], item.get('inventory_quantity', 0.0)) for item in
                              QuantInv.read_group(domain_quant, ['product_id', 'quantity'], ['product_id'],
                                                  orderby='id'))

        moves_in_res_past, moves_out_res_past = {}, {}
        if dates_in_the_past:
            # Calculate the moves that were done before now to calculate back in time (as most questions will be recent ones)
            domain_move_in_done = [('state', '=', 'done'), ('date', '>', to_date)] + domain_move_in_done
            domain_move_out_done = [('state', '=', 'done'), ('date', '>', to_date)] + domain_move_out_done
            moves_in_res_past = dict((item['product_id'][0], item['product_qty']) for item in
                                     Move.read_group(domain_move_in_done, ['product_id', 'product_qty'], ['product_id'],
                                                     orderby='id'))
            moves_out_res_past = dict((item['product_id'][0], item['product_qty']) for item in
                                      Move.read_group(domain_move_out_done, ['product_id', 'product_qty'],
                                                      ['product_id'], orderby='id'))

        record_type = self._get_record_type()

        res = dict()
        for product in self.with_context(prefetch_fields=False):
            pid = product.id

            if not pid:
                res[pid] = {
                    'inventory_quantity': 0.0,
                    'safety_stock_qty': 0.0,
                    'to_produce_qty': 0.0
                }
                continue

            rounding = product.uom_id.rounding
            res[pid] = {}

            inventory_quantity = quants_inv_res.get(pid, 0.0)
            if dates_in_the_past:
                inventory_quantity -= moves_in_res_past.get(pid, 0.0) + moves_out_res_past.get(pid, 0.0)

            safety_stock_qty = self.browse(pid)._get_safety_stock_qty(self.env.context.get('warehouse'))
            res[pid]['inventory_quantity'] = float_round(inventory_quantity, precision_rounding=rounding)
            res[pid]['safety_stock_qty'] = float_round(safety_stock_qty, precision_rounding=rounding)
            res[pid]['outgoing_qty'] = float_round(moves_out_res.get(pid, 0.0), precision_rounding=rounding)

            to_produce_qty = 1.0
            if record_type != 'disassemble':
                Q = res[pid]
                to_produce_qty = max(0.0, Q['safety_stock_qty'] - Q['inventory_quantity'] + Q['outgoing_qty'])

            res[pid]['to_produce_qty'] = float_round(to_produce_qty, precision_rounding=rounding)

        return res

    def action_produce(self):
        self.ensure_one()

        record_type = self._get_record_type()

        if not record_type:
            raise UserError(_("Record type is not defined!"))

        kitchen_context = request.session.get('kitchen_context', dict())
        warehouse_id = kitchen_context.get('warehouse', False)

        to_produce_qty = self.env.context.get('to_produce_qty', 0.0)

        if to_produce_qty > 0:
            context = self.env.context.copy()

            context.update({
                'default_create_date': fields.Datetime.now(),
                'default_create_uid': self.env.user.id,
                'default_product_id': self.id,
                'default_finished_qty': to_produce_qty,
                'default_warehouse_id': warehouse_id,
                'readonly_fields': True,
                'return_action': True
            })

            action_name = record_type.replace('ble', 'bly') if record_type != 'kitchen' else record_type

            action = {
                'name': "%s Production Record" % action_name.title(),
                'type': 'ir.actions.act_window',
                'res_model': 'kitchen.production.record',
                'view_mode': 'form',
                'target': 'new',
                'context': context
            }
            return action

    inventory_quantity = fields.Float(
        'Inventory Quantity', compute='_compute_kitchen_quantities',
        digits='Product Unit of Measure', compute_sudo=False)
    safety_stock_qty = fields.Float(
        'Safety Stock', compute='_compute_kitchen_quantities',
        digits='Product Unit of Measure', compute_sudo=False)
    to_produce_qty = fields.Float(
        'To Produce', compute='_compute_kitchen_quantities',
        digits='Product Unit of Measure', compute_sudo=False)
