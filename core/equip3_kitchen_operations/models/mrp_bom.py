from odoo import models, fields


class MRPBomByProduct(models.Model):
	_inherit = 'mrp.bom.byproduct'

	allocated_cost = fields.Float(string='Allocated Cost (%)')
