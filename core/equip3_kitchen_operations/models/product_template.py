from odoo import models, fields
from lxml import etree


class ProductTemplate(models.Model):
	_inherit = 'product.template'

	produceable_in_kitchen = fields.Boolean(string='Produceable in Kitchen')

	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		result = super(ProductTemplate, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		record_type = self.env.context.get('default_record_type')

		if view_type != 'form' or self.env.company.central_kitchen:
			return result

		doc = etree.XML(result['arch'])
		kitchen_tab = doc.xpath("//page[@name='central_kitchen_tab']")
		if kitchen_tab:
			kitchen_tab[0].set('string', 'Assemble')

		produceable_in_kitchen = doc.xpath("//field[@name='produceable_in_kitchen']")
		if produceable_in_kitchen:
			produceable_in_kitchen[0].set('string', 'Can be Assemble/Disassemble?')

		result['arch'] = etree.tostring(doc, encoding='unicode')
		return result
