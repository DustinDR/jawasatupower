from odoo import models, fields


class PurchaseRequest(models.Model):
	_inherit = 'purchase.request'

	kitchen_id = fields.Many2one('kitchen.production.record', 'Kitchen Production', copy=False)
	is_readonly_origin = fields.Boolean()
