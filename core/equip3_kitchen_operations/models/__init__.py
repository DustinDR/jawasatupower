# from . import ir_sequence
from . import kitchen_production_record
from . import safety_stock
# from . import product
from . import product_template
from . import product_product
from . import stock_move
from . import account_move
# from . import mrp_bom
from . import purchase_request
from . import stock_picking
