from ast import literal_eval
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, AccessError, UserError
from dateutil.relativedelta import relativedelta
from odoo.tools import float_compare, float_round
from lxml import etree
import json
import datetime
import logging

_logger = logging.getLogger(__name__)


class KitchenProductionRecord(models.Model):
	_name = 'kitchen.production.record'
	_inherit = ['mail.thread', 'mail.activity.mixin']
	_description = 'Kitchen Production Record'

	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		res = super(KitchenProductionRecord, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		
		record_type = self.env.context.get('default_record_type')

		if record_type != 'disassemble':
			return res

		doc = etree.XML(res['arch'])

		if view_type == 'form':
			product_qty = doc.xpath("//label[@for='product_qty']")

			if product_qty:
				product_qty[0].set("string", "Quantity")

		elif view_type == 'tree':
			product_qty = doc.xpath("//field[@name='product_qty']")
			if product_qty:
				product_qty[0].set("string", "Quantity")
				product_qty[0].addprevious(etree.Element('field', {'name': 'assemble_record_id'}))
			
		res['arch'] = etree.tostring(doc, encoding='unicode')
		return res


	@api.model
	def _get_default_picking_type(self):
		if self.env.context.get('default_warehouse_id'):
			picking_type_id = self.env['stock.picking.type'].search([
				('code', '=', 'mrp_operation'),
				('warehouse_id', '=', self.env.context['default_warehouse_id'])
			], limit=1)
			if picking_type_id:
				return picking_type_id.id

		company_id = self.env.context.get('default_company_id', self.env.company.id)
		return self.env['stock.picking.type'].search([
			('code', '=', 'mrp_operation'),
			('warehouse_id.company_id', '=', company_id),
		], limit=1).id

	@api.model
	def _get_default_location_src_id(self):
		location = False
		company_id = self.env.context.get('default_company_id', self.env.company.id)
		if self.env.context.get('default_picking_type_id'):
			location = self.env['stock.picking.type'].browse(self.env.context['default_picking_type_id']).default_location_src_id
		if not location:
			location = self.env['stock.warehouse'].search([('company_id', '=', company_id)], limit=1).lot_stock_id
		return location and location.id or False

	@api.model
	def _get_default_location_dest_id(self):
		location = False
		company_id = self.env.context.get('default_company_id', self.env.company.id)
		if self._context.get('default_picking_type_id'):
			location = self.env['stock.picking.type'].browse(self.env.context['default_picking_type_id']).default_location_dest_id
		if not location:
			location = self.env['stock.warehouse'].search([('company_id', '=', company_id)], limit=1).lot_stock_id
		return location and location.id or False

	@api.model
	def _get_default_date_planned_finished(self):
		if self.env.context.get('default_date_planned_start'):
			return fields.Datetime.to_datetime(self.env.context.get('default_date_planned_start')) + datetime.timedelta(hours=1)
		return datetime.datetime.now() + datetime.timedelta(hours=1)

	@api.model
	def _get_default_date_planned_start(self):
		if self.env.context.get('default_date_deadline'):
			return fields.Datetime.to_datetime(self.env.context.get('default_date_deadline'))
		return datetime.datetime.now()

	@api.model
	def _get_default_product_tmpl_id(self):
		if self.env.context.get('default_product_id'):
			product = self.env['product.product'].browse(self.env.context['default_product_id'])
			return product.product_tmpl_id.id
		return False

	@api.depends('product_id', 'company_id')
	def _compute_production_location(self):
		if not self.company_id:
			return
		location_by_company = self.env['stock.location'].read_group([
			('company_id', 'in', self.company_id.ids),
			('usage', '=', 'production')
		], ['company_id', 'ids:array_agg(id)'], ['company_id'])
		location_by_company = {lbc['company_id'][0]: lbc['ids'] for lbc in location_by_company}
		for production in self:
			if production.product_id:
				production.production_location_id = production.product_id.with_company(production.company_id).property_stock_production
			else:
				production.production_location_id = location_by_company.get(production.company_id.id)[0]

	@api.depends('move_finished_ids.date_deadline')
	def _compute_date_deadline(self):
		for production in self:
			production.date_deadline = min(production.move_finished_ids.filtered('date_deadline').mapped('date_deadline'), default=production.date_deadline or False)

	def _set_date_deadline(self):
		for production in self:
			production.move_finished_ids.date_deadline = production.date_deadline

	@api.depends('state', 'record_type')
	def _compute_show_submit_button(self):
		for production in self:
			production.show_submit_button = False
			if production.state != 'draft' or production.record_type == 'disassemble':
				continue
			production.show_submit_button = True

	@api.depends(
		'disassemble_record_ids', 'disassemble_record_ids.state', 'disassemble_record_ids.product_qty', 
		'disassemble_record_ids.rejected_qty', 'record_type', 'product_qty', 'rejected_qty')
	def _compute_disassemble_remaining_qty(self):
		for record in self:
			record.disassemble_remaining_qty = 0.0
			if record.record_type == 'assemble':
				product_qty = record.product_qty + record.rejected_qty
				disassemble_record_ids = record.disassemble_record_ids.filtered(lambda d: d.state == 'confirm')
				disassemble_product_qty = sum(disassemble_record_ids.mapped('product_qty'))
				disassemble_rejected_qty = sum(disassemble_record_ids.mapped('rejected_qty'))
				record.disassemble_remaining_qty = product_qty - (disassemble_product_qty + disassemble_rejected_qty)

	@api.depends('move_finished_ids')
	def _compute_move_byproduct_ids(self):
		for order in self:
			order.move_byproduct_ids = order.move_finished_ids.filtered(lambda m: m.product_id != order.product_id)

	def _set_move_byproduct_ids(self):
		move_finished_ids = self.move_finished_ids.filtered(lambda m: m.product_id == self.product_id)
		self.move_finished_ids = move_finished_ids | self.move_byproduct_ids

	name = fields.Char(
		'Reference', copy=False, readonly=True, default=lambda x: _('New'))
	company_id = fields.Many2one(
		'res.company', 'Company', default=lambda self: self.env.company,
		index=True, required=True)
	product_id = fields.Many2one(
		'product.product', 'Product',
		domain="[('product_tmpl_id', '=', product_tmpl_id)]",
		readonly=True, required=True, check_company=True,
		states={'draft': [('readonly', False)]})

	product_tmpl_id = fields.Many2one(
		'product.template', 'Product Template',
		domain="""[
			('type', 'in', ['product', 'consu']),
			'|',
				('company_id', '=', False),
				('company_id', '=', company_id)
		]
		""",
		readonly=True, required=True, check_company=True,
		states={'draft': [('readonly', False)]},
		default=_get_default_product_tmpl_id
	)
	
	product_qty = fields.Float(
		'Produced Quantity',
		default=0.0, digits='Product Unit of Measure',
		readonly=True, required=True, tracking=True,
		states={'draft': [('readonly', False)]})

	rejected_qty = fields.Float(
		'Rejected Quantity',
		default=0.0, digits='Product Unit of Measure',
		readonly=True, required=True, tracking=True,
		states={'draft': [('readonly', False)]})

	product_uom_id = fields.Many2one(
		'uom.uom', 'Product Unit of Measure',
		readonly=True, required=True,
		states={'draft': [('readonly', False)]}, domain="[('category_id', '=', product_uom_category_id)]")
	
	product_uom_category_id = fields.Many2one(related='product_id.uom_id.category_id')

	bom_id = fields.Many2one(
		'mrp.bom', 'Bill of Materials',
		readonly=True, states={'draft': [('readonly', False)]},
		domain="""[
		'&',
			'|',
				('company_id', '=', False),
				('company_id', '=', company_id),
			'&',
				'|',
					('product_id','=',product_id),
					'&',
						('product_tmpl_id.product_variant_ids','=',product_id),
						('product_id','=',False),
		('type', '=', 'normal')]""",
		check_company=True,
		required=True,
		help="Bill of Materials allow you to define the list of required components to make a finished product.")

	assemble_bom_id = fields.Many2one(
		'mrp.bom', 'Assemble Bill of Materials',
		readonly=True, states={'draft': [('readonly', False)]},
		domain="""[
		'&',
			'|',
				('company_id', '=', False),
				('company_id', '=', company_id),
			'&',
				'|',
					('product_id','=',product_id),
					'&',
						('product_tmpl_id.product_variant_ids','=',product_id),
						('product_id','=',False),
		('type', '=', 'normal')]""",
		check_company=True,
		copy=False
	)
	branch_id = fields.Many2one('res.branch', string='Branch', readonly=True, default=lambda self: self.env.user.branch_id.id, tracking=True)
	confirm_date = fields.Datetime(string='Confirmed On')

	state = fields.Selection([
		('draft', 'Draft'),
		('confirm', 'Confirmed')], string='State',
		copy=False, index=True, readonly=True,
		tracking=True, required=True, default='draft')
	move_raw_ids = fields.One2many(
		'stock.move', 'kitchen_component_id', 'Components',
		copy=True, states={'done': [('readonly', True)], 'cancel': [('readonly', True)]},
		domain=[('scrapped', '=', False)])
	move_finished_ids = fields.One2many(
		'stock.move', 'kitchen_finished_id', 'Finished Products',
		copy=True, states={'done': [('readonly', True)], 'cancel': [('readonly', True)]},
		domain=[('scrapped', '=', False)])
	move_byproduct_ids = fields.One2many('stock.move', compute='_compute_move_byproduct_ids', inverse='_set_move_byproduct_ids')
	
	location_src_id = fields.Many2one(
		'stock.location', 'Components Location',
		default=_get_default_location_src_id,
		readonly=True, required=True,
		domain="[('usage','=','internal'), '|', ('company_id', '=', False), ('company_id', '=', company_id)]",
		states={'draft': [('readonly', False)]}, check_company=True,
		help="Location where the system will look for components.")
	location_dest_id = fields.Many2one(
		'stock.location', 'Finished Products Location',
		default=_get_default_location_dest_id,
		readonly=True, required=True,
		domain="[('usage','=','internal'), '|', ('company_id', '=', False), ('company_id', '=', company_id)]",
		states={'draft': [('readonly', False)]}, check_company=True,
		help="Location where the system will stock the finished products.")

	picking_type_id = fields.Many2one(
		'stock.picking.type', 'Operation Type',
		domain="[('code', '=', 'mrp_operation'), ('company_id', '=', company_id)]",
		default=_get_default_picking_type, required=True, check_company=True)

	date_planned_start = fields.Datetime(
		'Scheduled Date', copy=False, default=_get_default_date_planned_start,
		help="Date at which you plan to start the production.",
		index=True, required=True)
	date_planned_finished = fields.Datetime(
		'Scheduled End Date',
		default=_get_default_date_planned_finished,
		help="Date at which you plan to finish the production.",
		copy=False)
	date_deadline = fields.Datetime(
		'Deadline', copy=False, store=True, readonly=True, compute='_compute_date_deadline', inverse='_set_date_deadline',
		help="Informative date allowing to define when the manufacturing order should be processed at the latest to fulfill delivery on time.")

	procurement_group_id = fields.Many2one(
		'procurement.group', 'Procurement Group',
		copy=False)

	move_dest_ids = fields.One2many('stock.move', 'created_kitchen_id',
		string="Stock Movements of Produced Goods")

	production_location_id = fields.Many2one('stock.location', "Production Location", compute="_compute_production_location", store=True)

	warehouse_id = fields.Many2one('stock.warehouse', check_company=True, copy=False, required=True, readonly=True, states={'draft': [('readonly', False)]})
	product_tracking = fields.Selection(related='product_id.tracking')
	account_move_ids = fields.One2many('account.move', 'kitchen_id', string='Journal Entries', copy=False)

	purchase_request_ids = fields.One2many('purchase.request', 'kitchen_id', string='Purchase Requests')
	stock_picking_ids = fields.One2many('stock.picking', 'kitchen_id', string='Stock Picking Forms')

	record_type = fields.Selection(
		selection=[('kitchen', 'Kitchen'), ('assemble', 'Assemble'), ('disassemble', 'Disassemble')],
		string='Record Type',
		required=True,
		default='kitchen'
	)

	show_submit_button = fields.Boolean(compute=_compute_show_submit_button)

	assemble_record_id = fields.Many2one('kitchen.production.record', string='Assemble Production Record', domain="[('record_type', '=', 'assemble'), ('product_id', '=', product_id), ('warehouse_id', '=', warehouse_id), ('state', '=', 'confirm'), ('disassemble_remaining_qty', '>', 0.0)]", copy=False)
	disassemble_record_ids = fields.One2many('kitchen.production.record', 'assemble_record_id', string='Disassamble Production Records', domain="[('record_type', '=', 'disassemble'), ('product_id', '=', 'product_id'), ('warehouse_id', '=', 'warehouse_id')]")
	disassemble_remaining_qty = fields.Float(string='Disassemble Remaining Quantity', compute=_compute_disassemble_remaining_qty, store=True)

	def _get_moves_finished_values(self):

		moves = []
		for production in self:
			if production.record_type == 'disassemble':
				product_qty = production.product_qty
			else:
				product_qty = production.product_qty + production.rejected_qty

			if production.product_id in production.bom_id.byproduct_ids.mapped('product_id'):
				raise UserError(_("You cannot have %s  as the finished product and in the Byproducts", self.product_id.name))
			move_for = 'finished' if production.record_type != 'disassemble' else 'raw'
			moves.append(production._get_move_values(
				move_for='finished',
				product_id=production.product_id,
				product_uom_qty=product_qty, 
				product_uom=production.product_uom_id
			))
			if production.record_type == 'disassemble':
				continue
			for byproduct_id in production.bom_id.byproduct_ids:
				product_uom_factor = production.product_uom_id._compute_quantity(production.product_qty + production.rejected_qty, production.bom_id.product_uom_id)
				qty = byproduct_id.product_qty * (product_uom_factor / production.bom_id.product_qty)
				moves.append(production._get_move_values(
					move_for='finished',
					product_id=byproduct_id.product_id, 
					product_uom_qty=qty,
					product_uom=byproduct_id.product_uom_id,
					operation_id=byproduct_id.operation_id,
					byproduct_id=byproduct_id
				))
		return moves

	def _get_moves_raw_values(self):

		moves = []
		for production in self:
			if production.record_type == 'disassemble':
				product_qty = production.product_qty
			else:
				product_qty = production.product_qty + production.rejected_qty

			move_for = 'raw' if production.record_type != 'disassemble' else 'finished'
			factor = production.product_uom_id._compute_quantity(product_qty, production.bom_id.product_uom_id) / production.bom_id.product_qty
			boms, lines = production.bom_id.explode(production.product_id, factor, picking_type=production.bom_id.picking_type_id)
			for bom_line, line_data in lines:
				if bom_line.child_bom_id and bom_line.child_bom_id.type == 'phantom' or\
						bom_line.product_id.type not in ['product', 'consu']:
					continue
				operation_id = bom_line.operation_id.id or line_data['parent_line'] and line_data['parent_line'].operation_id
				moves.append(production._get_move_values(
					move_for='raw',
					product_id=bom_line.product_id,
					product_uom_qty=line_data['qty'],
					product_uom=bom_line.product_uom_id,
					operation_id=operation_id,
					bom_line_id=bom_line,
				))
		return moves

	def _get_move_values(self, move_for, product_id, product_uom_qty, product_uom, operation_id=False, bom_line_id=False, byproduct_id=False):

		if not isinstance(operation_id, int) and operation_id is not False:
			operation_id = operation_id.id

		location_production_id = self.product_id.with_company(self.company_id).property_stock_production.id

		if move_for == 'finished':
			date_planned_finished = self.date_planned_start + relativedelta(days=self.product_id.produce_delay)
			date_planned_finished = date_planned_finished + relativedelta(days=self.company_id.manufacturing_lead)
			if date_planned_finished == self.date_planned_start:
				date_planned_finished = date_planned_finished + relativedelta(hours=1)
			date = date_planned_finished
			date_deadline = self.date_deadline
			location_dest_id = self.location_dest_id.id
			location_id = location_production_id
			if self.record_type == 'disassemble':
				location_dest_id, location_id = location_id, location_dest_id
			warehouse_location = location_dest_id
		else:
			date = self.date_planned_start
			date_deadline = self.date_planned_start
			location_dest_id = location_production_id
			location_id = self.location_src_id.id
			if self.record_type == 'disassemble':
				location_dest_id, location_id = location_id, location_dest_id
			warehouse_location = location_id

		warehouse_id = self.env['stock.location'].browse(warehouse_location).get_warehouse()

		values = {
			'company_id': self.company_id.id,
			'date': date,
			'date_deadline': date_deadline,
			'group_id': self.procurement_group_id.id,
			'location_dest_id': location_dest_id,
			'location_id': location_id,
			'name': self.name,
			'operation_id': operation_id,
			'origin': self.name,
			'picking_type_id': self.picking_type_id.id,
			'price_unit': product_id.standard_price,
			'product_id': product_id.id,
			'product_uom': product_uom,
			'product_uom_qty': product_uom_qty,
			'warehouse_id': warehouse_id.id
		}

		if move_for == 'raw':
			extra_values = {
				'kitchen_component_id': self.id,
				'bom_line_id': bom_line_id.id if bom_line_id else False,
				'procure_method': 'make_to_stock',
				'sequence': bom_line_id.sequence if bom_line_id else 10,
				'state': 'draft'
			}
		else:
			group_orders = self.procurement_group_id.mrp_production_ids
			move_dest_ids = self.move_dest_ids
			if len(group_orders) > 1:
				move_dest_ids |= group_orders[0].move_finished_ids.filtered(lambda m: m.product_id == self.product_id).move_dest_ids
			extra_values = {
				'kitchen_finished_id': self.id,
				'byproduct_id': byproduct_id.id if byproduct_id else False,
				'move_dest_ids': [(4, x.id) for x in move_dest_ids],
			}

		values.update(extra_values)
		return values

	@api.onchange('product_id', 'picking_type_id', 'company_id')
	def onchange_product_id(self):
		""" Finds UoM of changed product. """
		if not self.product_id:
			self.bom_id = False
			self.assemble_record_id = False
		elif not self.bom_id or self.bom_id.product_tmpl_id != self.product_tmpl_id or (self.bom_id.product_id and self.bom_id.product_id != self.product_id):
			bom = self.env['mrp.bom']._bom_find(product=self.product_id, picking_type=self.picking_type_id, company_id=self.company_id.id, bom_type='normal')
			if bom:
				self.bom_id = bom.id
				self.product_qty = self.env.context.get('default_finished_qty') or self.bom_id.product_qty
				self.product_uom_id = self.bom_id.product_uom_id.id
				if self.record_type == 'disassemble':
					self.assemble_bom_id = self.bom_id.id
			else:
				self.bom_id = False
				self.product_uom_id = self.product_id.uom_id.id
			self.product_tmpl_id = self.product_id.product_tmpl_id.id

	@api.onchange('bom_id')
	def _onchange_bom_id(self):
		if not self.product_id and self.bom_id:
			self.product_id = self.bom_id.product_id or self.bom_id.product_tmpl_id.product_variant_ids[0]
		self.product_qty = self.env.context.get('default_finished_qty') or self.bom_id.product_qty or 1.0
		self.product_uom_id = self.bom_id and self.bom_id.product_uom_id.id or self.product_id.uom_id.id
		self.move_raw_ids = [(2, move.id) for move in self.move_raw_ids.filtered(lambda m: m.bom_line_id)]
		self.move_finished_ids = [(2, move.id) for move in self.move_finished_ids]
		self.picking_type_id = self.bom_id.picking_type_id or self.picking_type_id

	@api.onchange('date_planned_start', 'product_id')
	def _onchange_date_planned_start(self):
		if self.date_planned_start:
			date_planned_finished = self.date_planned_start + relativedelta(days=self.product_id.produce_delay)
			date_planned_finished = date_planned_finished + relativedelta(days=self.company_id.manufacturing_lead)
			if date_planned_finished == self.date_planned_start:
				date_planned_finished = date_planned_finished + relativedelta(hours=1)
			self.date_planned_finished = date_planned_finished
			self.move_raw_ids = [(1, m.id, {'date': self.date_planned_start}) for m in self.move_raw_ids]
			self.move_finished_ids = [(1, m.id, {'date': date_planned_finished}) for m in self.move_finished_ids]

	@api.onchange('bom_id', 'product_id', 'product_qty', 'rejected_qty', 'product_uom_id', 'assemble_record_id')
	def _onchange_move_raw(self):
		if self.record_type == 'disassemble' and self.assemble_record_id:

			product_qty = self.product_qty
			assemble_id = self.assemble_record_id
			should_update_qty = product_qty != self._origin.product_qty
			assemble_product_qty = assemble_id.product_qty + assemble_id.rejected_qty

			if should_update_qty:
				if self.product_qty > assemble_id.disassemble_remaining_qty:
					raise UserError(_("The quantity must <= quantity on APR!"))

			move_raw_ids = [(5,)]
			for move in assemble_id.move_raw_ids:
				product_uom_qty = move.product_uom_qty
				if should_update_qty:
					if move.bom_line_id:
						factor = move.bom_line_id.product_qty / assemble_id.bom_id.product_qty
					else:
						factor = move.product_uom_qty / assemble_product_qty
					product_uom_qty = factor * product_qty
				values = self._get_move_values(
					move_for='raw',
					product_id=move.product_id, 
					product_uom_qty=product_uom_qty, 
					product_uom=move.product_uom, 
					operation_id=move.operation_id,
					bom_line_id=move.bom_line_id
				)
				move_raw_ids.append((0, 0, values))
			self.move_raw_ids = move_raw_ids
		else:
			if not self.bom_id and not self._origin.product_id:
				return
			# Clear move raws if we are changing the product. In case of creation (self._origin is empty),
			# we need to avoid keeping incorrect lines, so clearing is necessary too.
			if self.product_id != self._origin.product_id:
				self.move_raw_ids = [(5,)]

			if self.bom_id and self.product_qty + self.rejected_qty > 0:
				# keep manual entries
				list_move_raw = [(4, move.id) for move in self.move_raw_ids.filtered(lambda m: not m.bom_line_id)]
				moves_raw_values = self._get_moves_raw_values()

				move_raw_dict = {move.bom_line_id.id: move for move in self.move_raw_ids.filtered(lambda m: m.bom_line_id)}
				for move_raw_values in moves_raw_values:
					move_bom_line = move_raw_values.get('bom_line_id') 
					if move_bom_line and move_bom_line in move_raw_dict:
						# update existing entries
						list_move_raw += [(1, move_raw_dict[move_bom_line].id, move_raw_values)]
					else:
						# add new entries
						list_move_raw += [(0, 0, move_raw_values)]
				self.move_raw_ids = list_move_raw

			else:
				self.move_raw_ids = [(2, move.id) for move in self.move_raw_ids.filtered(lambda m: m.bom_line_id)]		

	@api.onchange('bom_id', 'product_id', 'product_qty', 'rejected_qty', 'product_uom_id', 'assemble_record_id')
	def _onchange_move_finished(self):
		if self.record_type == 'disassemble' and self.assemble_record_id:

			product_qty = self.product_qty
			assemble_id = self.assemble_record_id
			should_update_qty = product_qty != self._origin.product_qty
			assemble_product_qty = assemble_id.product_qty + assemble_id.rejected_qty

			if should_update_qty:
				if self.product_qty > assemble_id.disassemble_remaining_qty:
					raise UserError(_("The quantity must <= quantity on APR!"))

			move_finished_ids = [(5,)]
			for move in assemble_id.move_finished_ids.filtered(lambda m: m.product_id == self.product_id):
				product_uom_qty = move.product_uom_qty
				if should_update_qty:
					product_uom_qty = product_qty
				values = self._get_move_values(
					move_for='finished',
					product_id=move.product_id,
					product_uom_qty=product_uom_qty, 
					product_uom=move.product_uom,
				)
				move_finished_ids.append((0, 0, values))
			self.move_finished_ids = move_finished_ids
		else:
			if self.product_id and self.product_qty + self.rejected_qty > 0:
				# keep manual entries
				list_move_finished = [(4, move.id) for move in self.move_finished_ids.filtered(
					lambda m: not m.byproduct_id and m.product_id != self.product_id)]
				moves_finished_values = self._get_moves_finished_values()
				moves_byproduct_dict = {move.byproduct_id.id: move for move in self.move_finished_ids.filtered(lambda m: m.byproduct_id)}
				move_finished = self.move_finished_ids.filtered(lambda m: m.product_id == self.product_id)
				for move_finished_values in moves_finished_values:
					if move_finished_values.get('byproduct_id') in moves_byproduct_dict:
						# update existing entries
						list_move_finished += [(1, moves_byproduct_dict[move_finished_values['byproduct_id']].id, move_finished_values)]
					elif move_finished_values.get('product_id') == self.product_id.id and move_finished:
						list_move_finished += [(1, move_finished.id, move_finished_values)]
					else:
						# add new entries
						list_move_finished += [(0, 0, move_finished_values)]
				self.move_finished_ids = list_move_finished
			else:
				self.move_finished_ids = [(2, move.id) for move in self.move_finished_ids.filtered(lambda m: m.bom_line_id)]


	@api.onchange('location_src_id', 'move_raw_ids', 'bom_id')
	def _onchange_location(self):
		field = 'location_id' if self.record_type != 'disassemble' else 'location_dest_id'
		source_location = self.location_src_id
		update_value_list = []
		for move in self.move_raw_ids:
			update_value_list += [(1, move.id, ({
				'warehouse_id': source_location.get_warehouse().id,
				field: source_location.id,
			}))]
		self.move_raw_ids = update_value_list

	@api.onchange(f'location_dest_id', 'move_finished_ids', 'bom_id')
	def _onchange_location_dest(self):
		field = 'location_dest_id' if self.record_type != 'disassemble' else 'location_id'
		destination_location = self.location_dest_id
		update_value_list = []
		for move in self.move_finished_ids:
			update_value_list += [(1, move.id, ({
				'warehouse_id': destination_location.get_warehouse().id,
				field: destination_location.id,
			}))]
		self.move_finished_ids = update_value_list

	@api.onchange('picking_type_id')
	def onchange_picking_type(self):
		location = self.env.ref('stock.stock_location_stock')
		try:
			location.check_access_rule('read')
		except (AttributeError, AccessError):
			location = self.env['stock.warehouse'].search([('company_id', '=', self.env.company.id)], limit=1).lot_stock_id
		self.move_raw_ids.update({'picking_type_id': self.picking_type_id})
		self.move_finished_ids.update({'picking_type_id': self.picking_type_id})
		self.location_src_id = self.env.context.get('default_location_src_id') or self.picking_type_id.default_location_src_id.id or location.id
		self.location_dest_id = self.env.context.get('default_location_dest_id') or self.picking_type_id.default_location_dest_id.id or location.id

	@api.onchange('warehouse_id')
	def _onchange_warehouse_id(self):
		if self.warehouse_id:
			picking_type_id = self.env['stock.picking.type'].search([
				('code', '=', 'mrp_operation'),
				('warehouse_id', '=', self.warehouse_id.id)
			], limit=1)
			if picking_type_id:
				self.picking_type_id = picking_type_id.id
			else:
				self.picking_type_id = False
		else:
			self.picking_type_id = False

	@api.onchange('assemble_record_id')
	def _onchange_assemble_record_id(self):
		if self.record_type != 'disassemble':
			return
		if self.assemble_record_id:
			self.bom_id = self.assemble_record_id.bom_id.id
			self.product_qty = self.assemble_record_id.disassemble_remaining_qty
			self.assemble_bom_id = False
		else:
			bom_ids = self.product_id.bom_ids
			if bom_ids:
				self.assemble_bom_id = bom_ids[0].id
			
	@api.onchange('assemble_bom_id')
	def _onchange_assemble_bom_id(self):
		if self.record_type != 'disassemble':
			return
		if self.assemble_bom_id:
			self.bom_id = self.assemble_bom_id.id
			self.product_qty = self.bom_id.product_qty
			if self.assemble_record_id:
				self.assemble_record_id = False

	@api.model
	def create(self, values):
		if values.get('product_qty', 0.0) <= 0:
			raise UserError(_("The quantity must be positive!"))
		if values.get('move_finished_ids', False):
			values['move_finished_ids'] = list(filter(lambda move: move[2]['byproduct_id'] is False, values['move_finished_ids']))
		if values.get('move_byproduct_ids', False):
			values['move_finished_ids'] = values.get('move_finished_ids', []) + values['move_byproduct_ids']
			del values['move_byproduct_ids']
		if not values.get('name', False) or values['name'] == _('New'):
			code = "%s.production.record.year" % values['record_type']
			values['name'] = self.env['ir.sequence'].next_by_code(code) or _('New')
		if not values.get('procurement_group_id'):
			procurement_group_vals = {'name': values['name']}
			values['procurement_group_id'] = self.env["procurement.group"].create(procurement_group_vals).id
		production = super(KitchenProductionRecord, self).create(values)
		(production.move_raw_ids | production.move_finished_ids).write({
			'group_id': production.procurement_group_id.id,
			'origin': production.name
		})
		production.move_raw_ids.write({'date': production.date_planned_start})
		production.move_finished_ids.write({'date': production.date_planned_finished})
		# Trigger move_raw creation when importing a file
		if 'import_file' in self.env.context:
			production._onchange_move_raw()
			production._onchange_move_finished()

		return production

	def write(self, values):
		product_qty = values.get('product_qty', self.product_qty)
		if product_qty <= 0:
			raise UserError(_("The quantity must be positive!"))
		return super(KitchenProductionRecord, self).write(values)

	def _check_disassemble_qty(self):
		self.ensure_one()
		if self.record_type == 'disassemble' and self.assemble_record_id:
			if self.assemble_record_id.disassemble_remaining_qty == 0:
				raise UserError(_('The APR you selected has been fully disassembled!'))

	def _check_to_consume_qty(self):
		self.ensure_one()
		
		if self.record_type != 'disassemble':
			moves_to_check = self.move_raw_ids
		else:
			moves_to_check = self.move_finished_ids.filtered(
				lambda move: move.product_id == self.product_id
			)

		not_available = []
		for move in moves_to_check:
			if move.product_uom_qty > move.product_free_qty:
				not_available.append(move.product_id.name)

		if not_available:
			raise UserError(_('Quantity to consume on following components is not available:\n%s' % '\n'.join(['- %s' % name for name in not_available])))

	def _get_accounting_journal(self):
		self.ensure_one()
		journal_id = None
		category_name = None
		for move in (self.move_raw_ids | self.move_finished_ids):
			move_journal_id = move.product_id.categ_id.property_stock_journal.id
			if not move_journal_id:
				raise UserError(_('Set Stock Journal for %s product category first!' % move.product_id.categ_id.name))
			
			if not journal_id:
				journal_id = move_journal_id
				category_name = move.product_id.categ_id.name
			else:
				if move_journal_id != journal_id:
					raise UserError(_('%s and %s product category has different Stock Journal' % (category_name, move.product_id.categ_id.name)))
		return journal_id

	def _account_entry_move(self):
		self.ensure_one()

		journal_id = self._get_accounting_journal()
		stock_valuation_layers = (self.move_finished_ids | self.move_raw_ids).stock_valuation_layer_ids
		move_lines = []

		cost_materials = -sum(self.move_raw_ids.stock_valuation_layer_ids.mapped('value'))
		if cost_materials == 0: return

		for svl in stock_valuation_layers:
			if not svl.product_id.valuation == 'real_time':
				continue
			if svl.currency_id.is_zero(svl.value):
				continue

			values = {
				'name': svl.description,
				'product_id': svl.product_id.id,
				'quantity': abs(svl.quantity),
				'product_uom_id': svl.product_id.uom_id.id,
				'ref': svl.description,
				'debit': svl.value if svl.value > 0 else 0,
				'credit': -svl.value if svl.value < 0 else 0,
				'account_id': svl.product_id.categ_id.property_stock_valuation_account_id.id,
			}
			move_lines.append((0, 0, values))

		account_move_values = {
			'kitchen_id': self.id,
			'journal_id': journal_id,
			'date': fields.Datetime.now(),
			'move_type': 'entry',
			'line_ids': move_lines,
			'stock_valuation_layer_ids': [(6, None, stock_valuation_layers.ids)]
		}
		account_move_id = self.env['account.move'].create(account_move_values)
		account_move_id._post()
		account_move_id.ref = self.name


	def action_confirm(self):

		for production in self:

			production._check_disassemble_qty()
			production._check_to_consume_qty()

			for move in (production.move_raw_ids | production.move_finished_ids):
				if move.state in ('done', 'cancel'):
					continue
				move.quantity_done = move.product_uom_qty
				move._action_done()

			production._account_entry_move()
			production.confirm_date = fields.Datetime.now()
			production.state = 'confirm'

		if self.env.context.get('return_action'):
			action = {
				'type': 'ir.actions.act_window',
				'res_model': 'kitchen.production.record',
				'view_mode': 'form', 
				'view_type': 'form',
				'res_id': self.id,
				'target': 'current'
			}

			if len(self) > 1:
				action.update({
					'view_mode': 'tree,form', 
					'view_type': 'tree,form',
					'domain': [('id', 'in', self.ids)]
				})
				del action['res_id']
			return action

	def action_get_account_moves(self):
		self.ensure_one()
		action_data = self.env['ir.actions.act_window']._for_xml_id('account.action_move_journal_line')
		action_data['domain'] = [('id', 'in', self.account_move_ids.ids)]
		return action_data

	def action_get_purchase_requests(self):
		self.ensure_one()
		action = {
			'name': 'Purchase Request',
			'type': 'ir.actions.act_window',
			'res_model': 'purchase.request',
			'target': 'current'
		}
		if len(self.purchase_request_ids) == 1:
			action['view_mode'] = 'form'
			action['res_id'] = self.purchase_request_ids[0].id
		else:
			action['name'] += 's'
			action['view_mode'] = 'tree,form'
			action['domain'] = [('id', 'in', self.purchase_request_ids.ids)]
		return action

	def action_get_stock_pickings(self):
		self.ensure_one()
		action = {
			'name': 'Transfer',
			'type': 'ir.actions.act_window',
			'res_model': 'stock.picking',
			'target': 'current'
		}

		picking_id = self.env.context.get('picking_pop_back')

		if len(self.stock_picking_ids) == 1 or (picking_id and isinstance(picking_id, int)):
			action['view_mode'] = 'form'
			if picking_id:
				action['res_id'] = picking_id
				action['target'] = 'new'
			else:
				action['res_id'] = self.stock_picking_ids[0].id
		else:
			action['name'] += 's'
			action['view_mode'] = 'tree,form'
			action['domain'] = [('id', 'in', self.stock_picking_ids.ids)]
		return action

	def action_submit_transfer_request(self):
		context = self.env.context.copy()

		line_ids = []
		for move in self.move_raw_ids:
			qty = move.product_uom_qty - move.product_free_qty

			if qty <= 0:
				continue

			values = {
				'name': move.product_id.name,
				'product_id': move.product_id.id,
				'product_uom': move.product_id.uom_id.id,
				'product_uom_qty': qty,
			}
			line_ids.append((0, 0, values))

		context = {
			'default_origin': self.name,
			'default_kitchen_id': self.id,
			'default_move_ids_without_package': line_ids,
			'default_picking_type_id': self.picking_type_id.id,
			'default_location_id': self.picking_type_id.default_location_src_id.id,
			'default_location_dest_id': self.picking_type_id.default_location_dest_id.id,
			'picking_type_code': 'mrp_operation',
			'default_is_readonly_origin': True,
			'kitchen_pop_back': self.id
		}

		action = {
			'name': 'Transfer',
			'type': 'ir.actions.act_window',
			'res_model': 'stock.picking',
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			'context': context,
		}

		return action

	def action_submit_purchase_request(self):
		context = self.env.context.copy()

		line_ids = []
		for move in self.move_raw_ids:
			qty = move.product_uom_qty - move.product_free_qty

			if qty <= 0:
				continue

			values = {
				'product_id': move.product_id.id,
				'name': move.product_id.name,
				'product_qty': qty,
				'product_uom_id': move.product_id.uom_id.id,
				'date_required': fields.Date.to_date(fields.Datetime.now())
			}
			line_ids.append((0, 0, values))

		context.update({
			'default_origin': self.name,
			'default_kitchen_id': self.id,
			'default_line_ids': line_ids,
			'default_picking_type_id': self.picking_type_id.id,
			'default_is_readonly_origin': True
		})

		action = {
			'name': 'Purchase Request',
			'type': 'ir.actions.act_window',
			'res_model': 'purchase.request',
			'view_mode': 'form',
			'target': 'new',
			'context': context,
		}

		return action

	def action_view_stock_valuation_layers(self):
		self.ensure_one()
		domain = [('id', 'in', (self.move_raw_ids + self.move_finished_ids).stock_valuation_layer_ids.ids)]
		action = self.env["ir.actions.actions"]._for_xml_id("stock_account.stock_valuation_layer_action")
		context = literal_eval(action['context'])
		context.update(self.env.context)
		context['no_at_date'] = True
		context['search_default_group_by_product_id'] = False
		return dict(action, domain=domain, context=context)
