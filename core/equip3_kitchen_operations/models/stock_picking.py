from odoo import models, fields, api
import logging

_logger = logging.getLogger(__name__)


class StockPicking(models.Model):
	_inherit = 'stock.picking'

	kitchen_id = fields.Many2one('kitchen.production.record', 'Kitchen Production', copy=False)
	is_readonly_origin = fields.Boolean()

	@api.onchange('location_id', 'location_dest_id')
	def onchange_location_id(self):
		self._compute_picking_type()
		self._compute_location()
		self._compute_show_location()
		context = dict(self.env.context) or {}
		picking_type_ids = self.env['stock.picking.type'].search([])
		for record in self:
			if context.get('picking_type_code') == "mrp_operation":
				continue

			record.picking_type_id = False
			if context.get('picking_type_code') == "outgoing":
				picking_type_id = picking_type_ids.filtered(lambda r:r.code == 'outgoing' and r.default_location_src_id.id == record.location_id.id)
			elif context.get('picking_type_code') == "incoming":
				picking_type_id = picking_type_ids.filtered(lambda r:r.code == 'incoming' and r.default_location_dest_id.id == record.location_dest_id.id)
			else:
				picking_type_id = False
			if picking_type_id:
				record.picking_type_id = picking_type_id[0].id

	def action_confirm(self):
		res = super(StockPicking, self).action_confirm()
		kitchen_id = self.env.context.get('kitchen_pop_back')
		if kitchen_id and isinstance(kitchen_id, int):
			kitchen_object = self.env['kitchen.production.record'].with_context(picking_pop_back=self.id)
			return kitchen_object.browse(kitchen_id).action_get_stock_pickings()
		return res

	def button_validate(self):
		res = super(StockPicking, self).button_validate()

		if res is True or res is None:
			kitchen_id = self.env.context.get('kitchen_pop_back')
			if kitchen_id and isinstance(kitchen_id, int):
				record_type = self.env['kitchen.production.record'].browse(kitchen_id).record_type
				action = self.env['ir.actions.actions']._for_xml_id('equip3_kitchen_operations.action_view_%s_production_record' % record_type)
				action['view_mode'] = 'form'
				action['views'] = [(False, 'form')]
				action['res_id'] = kitchen_id
				action['target'] = 'new'
				return action
		return res