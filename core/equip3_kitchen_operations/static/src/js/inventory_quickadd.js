odoo.define('equip3_kitchen_operations.KitchenDashboardListView', function (require) {
"use strict";

var core = require('web.core');
var viewRegistry = require('web.view_registry');
var ListView = require('web.ListView');
var ListModel = require('web.ListModel');
var ListRenderer = require('web.ListRenderer');
var ListController = require('web.ListController');
var SampleServer = require('web.SampleServer');

var QWeb = core.qweb;

let kitchenValues;
SampleServer.mockRegistry.add('product.product/get_product_based_warehouse', () => {
    return Object.assign({}, kitchenValues);
});


var kitchenRenderer = ListRenderer.extend({
    events:_.extend({}, ListRenderer.prototype.events, {
        'change .o_kitchen_dashboard': '_onKitchenWarehouseChange'
    }),

    _renderView: function(){
        var self = this;
        return this._super.apply(this, arguments).then(function () {
            var values = self.state.kitchenValues;
            var kitchen_dashboard = QWeb.render('equip3_kitchen_operations.dashboard_list_header', {
                values: values,
            });
            self.$el.prepend(kitchen_dashboard);
        });
    },

    _onKitchenWarehouseChange: function(e){
        e.preventDefault();
        var $action = $(e.currentTarget);
        this.trigger_up('kitchen_dashboard_open_action', {
            fieldName: $action.attr('name'),
            fieldValue: e.currentTarget.value
        });
    }
});

var kitchenModel = ListModel.extend({
    /**
     * @override
     */
    init: function () {
        this.kitchenValues = {};
        this._super.apply(this, arguments);
    },

    /**
     * @override
     */
    __get: function (localID) {
        var result = this._super.apply(this, arguments);
        if (_.isObject(result)) {
            result.kitchenValues = this.kitchenValues[localID];
        }
        return result;
    },
    /**
     * @override
     * @returns {Promise}
     */
    __load: function () {
        return this._loadKitchenDashboard(this._super.apply(this, arguments));
    },
    /**
     * @override
     * @returns {Promise}
     */
    __reload: function () {
        return this._loadKitchenDashboard(this._super.apply(this, arguments));
    },

    /**
     * @private
     * @param {Promise} super_def a promise that resolves with a dataPoint id
     * @returns {Promise -> string} resolves to the dataPoint id
     */
    _loadKitchenDashboard: function (super_def) {
        var self = this;
        var dashboard_def = this._rpc({
            model: 'product.product',
            method: 'get_default_kitchen_dashboard_values',
            args: [this.kitchenValues],
        });
        return Promise.all([super_def, dashboard_def]).then(function(results) {
            var id = results[0];
            kitchenValues = results[1];
            self.kitchenValues[id] = kitchenValues;
            return id;
        });
    },
});

var kitchenController = ListController.extend({
    custom_events: _.extend({}, ListController.prototype.custom_events, {
        kitchen_dashboard_open_action: '_onKitchenDashboardOpenAction',
    }),

    /**
     * @private
     * @param {OdooEvent} e
     */
    _onKitchenDashboardOpenAction: async function (e) {
        var state = this.model.get(this.handle);
        var kitchenValues = state.kitchenValues;

        var fieldName = e.data.fieldName.replace('kitchen_dashboard_', '');
        kitchenValues[fieldName] = e.data.fieldValue;

        var context = state.getContext();
        var res_ids = await this._rpc({
            model: 'product.product',
            method: 'get_product_based_warehouse',
            args: [kitchenValues],
            context: context
        });

        this.reload({
            context: context,
            domain: [["id", "in", res_ids]]
        });
    },
});

var kitchenListViewDashboard = ListView.extend({
    config: _.extend({}, ListView.prototype.config, {
        Model: kitchenModel,
        Renderer: kitchenRenderer,
        Controller: kitchenController,
    }),
});

viewRegistry.add('kitchen_dashboard', kitchenListViewDashboard);

return {
    kitchenModel: kitchenModel,
    kitchenRenderer: kitchenRenderer,
    kitchenController: kitchenController,
};
});
