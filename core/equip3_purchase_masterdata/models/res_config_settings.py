
from odoo import api , fields , models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        if self.is_vendor_approval_matrix:
            self.env.ref('equip3_purchase_masterdata.approval_matrix_vendor_configuration_menu').active = True
            self.env.ref('equip3_purchase_masterdata.menu_vendor_to_approve').active = True
        else:
            self.env.ref('equip3_purchase_masterdata.approval_matrix_vendor_configuration_menu').active = False
            self.env.ref('equip3_purchase_masterdata.menu_vendor_to_approve').active = False
        if self.is_vendor_pricelist_approval_matrix:
            self.env.ref('equip3_purchase_masterdata.approval_matrix_pricelist_vendor_configuration_menu').active = True
            self.env.ref('equip3_purchase_masterdata.menu_vendor_pricelist_to_approve').active = True
            self.env.ref('equip3_purchase_masterdata.menu_vendor_pricelist_changes_to_approve').active = True
        else:
            self.env.ref('equip3_purchase_masterdata.approval_matrix_pricelist_vendor_configuration_menu').active = False 
            self.env.ref('equip3_purchase_masterdata.menu_vendor_pricelist_to_approve').active = False
            self.env.ref('equip3_purchase_masterdata.menu_vendor_pricelist_changes_to_approve').active = False
