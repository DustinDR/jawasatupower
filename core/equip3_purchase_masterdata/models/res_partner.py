# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from email.policy import default
import string
from odoo import api, fields, models
from odoo.addons.website.models import ir_http
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT


class ResPartner(models.Model):
    _inherit = 'res.partner'
    
    vendor_sequence = fields.Char("Vendor ID", readonly=True, copy=False)
    is_vendor = fields.Boolean(string="Is a Vendor")
    state = fields.Selection([("draft","Draft"),
                              ("waiting_approval","Waiting For Approval"),
                              ("approved","Approved"),
                              ("rejected","Rejected")
                              ], string='State', default="draft")
    state1 = fields.Selection(related="state")
    is_approving_matrix_vendor = fields.Boolean(compute="_compute_approving_matrix", string="Approving Matrix Vendor")
    approved_user_ids = fields.Many2many('res.users', 'approved_res_user_rel', 'user_id', 'line_id', string="Approved User")
    branch_id = fields.Many2one('res.branch',"Branch")
    approving_matrix_vendor_id = fields.Many2one('approval.matrix.vendor', string="Approving Matrix", compute="_compute_matrix", store=True)
    approved_matrix_ids = fields.One2many('approval.matrix.vendor.line', 'app_matrix_id', compute="_compute_approving_matrix_lines", store=True, string="Approved Matrix")
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button', store=False)
    approval_matrix_line_id = fields.Many2one('approval.matrix.vendor.line', string='Vendor Approval Matrix Line', compute='_get_approve_button', store=False)

    @api.model
    def create(self, values):
        if values.get('is_vendor', True):
            sequence = self.env['ir.sequence'].next_by_code('vendor.id.sequence')
            values.update({'vendor_sequence': sequence})
            values.update({'supplier_rank': 1})
        else:
            if values.get('vendor_rank', 1):
                sequence = self.env['ir.sequence'].next_by_code('vendor.id.sequence')
                values.update({'vendor_sequence': sequence})

        res = super(ResPartner, self).create(values)
        return res

    def write(self, values):
        if values.get('is_vendor', True):
            res_data = self.env['res.partner'].search([('id', '=', self.ids[0])])
            if not res_data.is_vendor:
                sequence = self.env['ir.sequence'].next_by_code('vendor.id.sequence')
                values.update({'vendor_sequence': sequence})
                values.update({'supplier_rank': 1})
        else:
            is_data_exist = 0
            res_data = self.env['purchase.order'].search([('partner_id', '=', self.ids[0])], limit=1)
            if len(res_data) > 0:
                is_data_exist = 1

            if is_data_exist == 0:
                res_data = self.env['account.move'].search([('partner_id', '=', self.ids[0])], limit=1)
                if len(res_data) > 0:
                    is_data_exist = 1

            if is_data_exist == 1:
                values.update({'is_vendor': True})
            else:
                values.update({'is_vendor': False})
                values.update({'vendor_sequence': 0})
                values.update({'supplier_rank': 0})

        res = super(ResPartner, self).write(values)
        return res

    @api.depends('branch_id')
    def _compute_matrix(self):
        for res in self:
            IrConfigParam = self.env['ir.config_parameter'].sudo()
            approval =  IrConfigParam.get_param('is_vendor_approval_matrix')
            res.approving_matrix_vendor_id = False
            if approval:
                matrix_id = self.env['approval.matrix.vendor'].search([('branch_id', '=', res.branch_id.id)], limit=1)
                if matrix_id:
                    res.approving_matrix_vendor_id = matrix_id

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        domain = domain or []
        context = dict(self.env.context) or {}
        is_approving_matrix = self.env['ir.config_parameter'].sudo().get_param('is_vendor_approval_matrix', False)
        if is_approving_matrix and context.get('default_is_vendor'):
            domain.extend([('state', '=', 'approved')])
        return super(ResPartner, self).search_read(domain=domain, fields=fields, offset=offset, limit=limit, order=order)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        domain = domain or []
        context = dict(self.env.context) or {}
        is_approving_matrix = self.env['ir.config_parameter'].sudo().get_param('is_vendor_approval_matrix', False)
        if is_approving_matrix and context.get('default_is_vendor'):
            domain.extend([('state', '=', 'approved')])
        return super(ResPartner, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                                orderby=orderby, lazy=lazy)

    def _get_approve_button(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False

    @api.depends('approving_matrix_vendor_id', 'branch_id')
    def _compute_approving_matrix_lines(self):
        data = [(5, 0, 0)]
        for record in self:
            counter = 1
            record.approved_matrix_ids = []
            for line in record.approving_matrix_vendor_id.approval_matrix_line_ids:
                data.append((0, 0, {
                    'sequence' : counter,
                    'user_ids' : [(6, 0, line.user_ids.ids)],
                    'minimum_approver' : line.minimum_approver,
                }))
                counter += 1
            record.approved_matrix_ids = data

    def _compute_approving_matrix(self):
        is_approving_matrix = self.env['ir.config_parameter'].sudo().get_param('is_vendor_approval_matrix')
        for record in self:
            record.is_approving_matrix_vendor = is_approving_matrix

    @api.onchange('branch_id')
    def _onchange_branch(self):
        self._compute_approving_matrix()
        self._get_approve_button()
        self._compute_matrix()

    # @api.model
    # def create(self, values):
    #     res = super(ResPartner, self).create(values)
    #     if res.supplier_rank == 1:
    #         sequence = self.env['ir.sequence'].next_by_code('vendor.id.sequence')
    #         res.vendor_sequence = sequence
    #     else:
    #         res.state = 'approved'
    #     return res

    def action_request_for_approval(self):
        for record in self:
            record.write({'state': 'waiting_approval'})

    def action_approved(self):
        for record in self:
            user = self.env.user
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_ids.ids and \
                    user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    if name != '':
                        name += "\n • %s: Approved" % (self.env.user.name)
                    else:
                        name += "• %s: Approved" % (self.env.user.name)

                    approval_matrix_line_id.write({ 
                        'last_approved': self.env.user.id, 'state_char': name, 
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
                        # next_approval_matrix_line_id = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
                        # if next_approval_matrix_line_id and len(next_approval_matrix_line_id[0].approver) > 1:
                        #     pass
            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r:r.approved)):
                record.write({'state': 'approved'})
            

    def action_rejected(self):
        for record in self:
            return {
                    'type': 'ir.actions.act_window',
                    'name': 'Reject Reason',
                    'res_model': 'approval.matrix.vendor.reject',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'new',
                }