from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class VendorPricelistApprovalMatrix(models.Model):
    _name = "vendor.pricelist.approval.matrix"
    _description = "Vendor Pricelist Approval Matrix"
    
    
    name = fields.Char(string="Name", required=True)
    company_id = fields.Many2one('res.company', string="Company", required=True, readonly=True, default=lambda self: self.env.company.id)
    branch_id = fields.Many2one('res.branch', string="Branch", domain="[('company_id', '=', company_id)]")
    department_id = fields.Many2one('hr.department', 'Department', domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")
    created_by = fields.Many2one('res.users', "Created By", readonly=True, default=lambda self: self.env.user)
    created_date = fields.Date("Created On", readonly=True, default=fields.Date.today())
    approval_matrix_line_ids = fields.One2many('vendor.pricelist.approval.matrix.line', 'approval_matrix', string='Approving Matrix')
    # minimum_amt = fields.Float(string='Minimum Amount', required=True, tracking=True)
    # maximum_amt = fields.Float(string='Maximum Amount', required=True, tracking=True)
    

    @api.constrains('branch_id')
    def _check_existing_record(self):
        for record in self:
            approval_matrix_id = self.search([('branch_id', '=', record.branch_id.id), ('id', '!=', record.id)], limit=1)
            if approval_matrix_id:
                raise ValidationError("There are other approval matrix %s in same branch. Please change branch" % (approval_matrix_id.name))
    
    def _reset_sequence(self):
        for rec in self:
            current_sequence = 1
            for line in rec.approval_matrix_line_ids:
                line.sequence = current_sequence
                current_sequence += 1
    def copy(self, default=None):
        res = super(VendorPricelistApprovalMatrix, self.with_context(keep_line_sequence=True)).copy(default)
        return res
        
    @api.onchange('company_id')
    def onchange_company_id(self):
        self.branch_id = False

class VendorPricelistApprovalMatrixLine(models.Model):
    _name = "vendor.pricelist.approval.matrix.line"
    _description = "Vendor Pricelist Approval Matrix Line"
    
    @api.model
    def default_get(self, fields):
        res = super(VendorPricelistApprovalMatrixLine, self).default_get(fields)
        if self._context:
            context_keys = self._context.keys()
            next_sequence = 1
            if 'approval_matrix_line_ids' in context_keys:
                if len(self._context.get('approval_matrix_line_ids')) > 0:
                    next_sequence = len(self._context.get('approval_matrix_line_ids')) + 1
            res.update({'sequence': next_sequence})
        return res
    
    sequence = fields.Integer(string="Sequence")
    user_ids = fields.Many2many('res.users', string="User", domain="[('id', '!=', user_ids_domain)]", required=True)
    minimum_approver = fields.Integer(string="Minimum Approver", default=1, required=True)
    approval_matrix = fields.Many2one('vendor.pricelist.approval.matrix', string="Approval Matrix")
    sequence2 = fields.Integer(
        string="No.",
        related="sequence",
        readonly=True,
        store=True,
        tracking=True
    )
    user_ids_domain = fields.Many2many('res.users', string="User", compute="_compute_user_domain")

    @api.depends('user_ids')
    def _compute_user_domain(self):
        for rec in self:
            lines = []
            for line in rec.approval_matrix.approval_matrix_line_ids:
                lines.extend(line.user_ids.ids)
            rec.user_ids_domain = self.env['res.users'].browse(lines)
    
    def unlink(self):
        approval = self.approval_matrix
        res = super(VendorPricelistApprovalMatrixLine, self).unlink()
        approval._reset_sequence()
        return res
    
    @api.model
    def create(self, vals):
        res = super(VendorPricelistApprovalMatrixLine, self).create(vals)
        if not self.env.context.get("keep-line_sequence", False):
            res.approval_matrix._reset_sequence()
        return res 
