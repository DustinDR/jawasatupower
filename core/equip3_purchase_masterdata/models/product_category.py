from odoo import models, fields, api, _
from odoo.exceptions import Warning
from odoo.exceptions import ValidationError


class ProductCategory(models.Model):
	_inherit = 'product.category'
	
	product_limit = fields.Selection([('no_limit',"Don't Limit"),('limit_per','Limit by Precentage %'),('limit_amount','Limit by Amount'),('str_rule','Strictly Limit by Purchase Order')],
		string='Receiving Limit', tracking=True, default='no_limit')
	min_val = fields.Integer('Minimum Amount')
	max_val = fields.Integer('Maximum Amount')
	
	@api.onchange('min_val', 'max_val', 'product_limit')
	def _onchange_value(self):
		if self.product_limit == 'limit_per':
			if not 0 <= self.min_val <= 100 or not 0 <= self.max_val <= 100:
				raise ValidationError(_("The input value must range from 0 to 100."))