from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime,timedelta


class SupplierInfo(models.Model):
    _name = "product.supplierinfo"
    _inherit = ['product.supplierinfo', 'mail.thread', 'mail.activity.mixin', 'portal.mixin']
    
    # ////////////////////////////////
    
    name = fields.Many2one(
        'res.partner', 'Vendor',
        ondelete='cascade', required=True,
        help="Vendor of this product", check_company=True, tracking=True, domain="[('is_vendor', '=', True), '|', ('company_id', '=', False), ('company_id', '=', company_id)]")
    product_name = fields.Char(
        'Vendor Product Name', tracking=True,
        help="This vendor's product name will be used when printing a request for quotation. Keep empty to use the internal one.")
    product_code = fields.Char(
        'Vendor Product Code', tracking=True,
        help="This vendor's product code will be used when printing a request for quotation. Keep empty to use the internal one.")
    sequence = fields.Integer(
        'Sequence', default=1, help="Assigns the priority to the list of product vendor.")
    product_uom = fields.Many2one(
        'uom.uom', 'Unit of Measure', related='product_uom_new',
        help="This comes from the product form.")
    product_uom_category_id = fields.Many2one(related='product_tmpl_id.uom_id.category_id')
    product_uom_new = fields.Many2one(
        'uom.uom', 'Unit of Measure', domain="[('category_id', '=', product_uom_category_id)]",
        help="This comes from the product form.")
    vendor_uom = fields.Char('Vendor Unit of Measure')
    min_qty = fields.Float(
        'Minimal Quantity', default=1.0, required=True, digits="Product Unit Of Measure", tracking=True,
        help="The quantity to purchase from this vendor to benefit from the price, expressed in the vendor Product Unit of Measure if not any, in the default unit of measure of the product otherwise.")
    price = fields.Float(
        'Price', default=0.0, digits='Product Price', tracking=True,
        required=True, help="The price to purchase a product")
    company_id = fields.Many2one(
        'res.company', 'Company', tracking=True,
        default=lambda self: self.env.company.id, index=1)
    currency_id = fields.Many2one(
        'res.currency', 'Currency',
        default=lambda self: self.env.company.currency_id.id,
        required=True)
    date_start = fields.Date('Start Date', tracking=True, help="Start date for this vendor price")
    date_end = fields.Date('End Date', tracking=True, help="End date for this vendor price")
    product_id = fields.Many2one(
        'product.product', 'Product Variant', check_company=True, tracking=True,
        help="If not set, the vendor price will apply to all variants of this product.")
    product_tmpl_id = fields.Many2one(
        'product.template', 'Product Template', tracking=True, check_company=True,
        index=True, ondelete='cascade')
    product_variant_count = fields.Integer('Variant Count', related='product_tmpl_id.product_variant_count')
    delay = fields.Integer(
        'Delivery Lead Time', tracking=True, default=1, required=True,
        help="Lead time in days between the confirmation of the purchase order and the receipt of the products in your warehouse. Used by the scheduler for automatic computation of the purchase order planning.")
    
    # ////////////////////////////////
    
    state = fields.Selection([("draft","Draft"),
        ("waiting_approval","Waiting For Approval"),
        ("approved","Approved"),
        ("rejected","Rejected"),
        ("expire","Expired"),
    ], string='State', default="draft", tracking=True)
    state1 = fields.Selection(related="state", tracking=False)
    state2 = fields.Selection(related="state", tracking=False)
    vendor_pricelist_approval_matrix = fields.Many2one('vendor.pricelist.approval.matrix', string="Vendor Pricelist Approval Matrix", compute="_get_vendor_price")
    approval_ids = fields.One2many('product.supplierinfo.approval', 'supplier_id', 'Approval Lines', tracking=True)
    user_approval_ids = fields.Many2many('res.users', string="User")
    current_user = fields.Many2one('res.users', compute='_get_current_user')
    is_vendor_pricelist_approval_matrix = fields.Boolean(compute="_compute_approving_matrix_vendor_pricelist", string="Approving Matrix Vendor Pricelist", store=True)
    branch_id = fields.Many2one('res.branch',"Branch")
    changes = fields.Boolean('Changes', default=False)
    changes_id = fields.Integer('Changes ID', default=0)

    # //history

    name_old = fields.Many2one(
        'res.partner', 'Vendor',
        ondelete='cascade')
    product_name_old = fields.Char(
        'Vendor Product Name')
    product_code_old = fields.Char(
        'Vendor Product Code')
    delay_old = fields.Integer(
        'Delivery Lead Time')
    branch_id_old = fields.Many2one('res.branch',"Branch")
    date_old = fields.Datetime("Created on")
    product_id_old = fields.Many2one(
        'product.product', 'Product Variant')
    product_tmpl_id_old = fields.Many2one(
        'product.template', 'Product Template')
    product_uom_old = fields.Many2one(
        'uom.uom', 'Unit of Measure',
        related='product_tmpl_id_old.uom_po_id')
    min_qty_old = fields.Float(
        'Minimal Quantity', default=0.0)
    price_old = fields.Float(
        'Price', default=0.0)
    company_id_old = fields.Many2one(
        'res.company', 'Company')
    currency_id_old = fields.Many2one(
        'res.currency', 'Currency')
    date_start_old = fields.Date('Start Date', tracking=True, help="Start date for this vendor price")
    date_end_old = fields.Date('End Date', tracking=True, help="End date for this vendor price")
    vendor_pricelist_approval_matrix_old = fields.Many2one('vendor.pricelist.approval.matrix', string="Vendor Pricelist Approval Matrix")

    @api.onchange('product_tmpl_id')
    def set_uom(self):
        for res in self:
            if res.product_tmpl_id:
                res.product_uom_new = res.product_tmpl_id.uom_po_id

    @api.model
    def create(self, vals):
        if vals.get('min_qty') == 0:
            vals['min_qty'] = 1
        res = super(SupplierInfo, self).create(vals)
        if res.product_tmpl_id:
            res.product_uom_new = res.product_tmpl_id.uom_po_id.id
        if not res.is_vendor_pricelist_approval_matrix:
            res.state = 'approved'
        if res.changes_id > 0:
            id = self.env['product.supplierinfo'].search([('id', '=', res.changes_id)])
            if id:
                res.update({
                    'name_old': id.name.id or False,
                    'product_name_old': id.product_name or False,
                    'product_code_old': id.product_code_old or False,
                    'delay_old': id.delay or False,
                    'branch_id_old': id.branch_id.id or False,
                    'date_old': id.create_date or False,
                    'product_id_old':  id.product_id.id or False,
                    'product_tmpl_id_old': id.product_tmpl_id.id or False,
                    'min_qty_old': id.min_qty or False,
                    'price_old': id.price or False,
                    'company_id_old': id.company_id.id or False,
                    'currency_id_old': id.currency_id.id or False,
                    'date_start_old': id.date_start or False,
                    'date_end_old': id.date_end or False,
                    'product_uom_old': id.product_uom or False,
                    'product_uom': id.product_uom or False,
                    'vendor_pricelist_approval_matrix_old': id.vendor_pricelist_approval_matrix or False,
                })
        return res
        
    @api.depends('price')
    def _compute_approving_matrix_vendor_pricelist(self):
        is_approving_matrix = self.env['ir.config_parameter'].sudo().get_param('is_vendor_pricelist_approval_matrix')
        for record in self:
            record.is_vendor_pricelist_approval_matrix = is_approving_matrix

    @api.depends('price')
    def _get_current_user(self):
        for rec in self:
            rec.current_user = self.env.user

    @api.constrains('min_qty')
    def _check_min_qty(self):
        for record in self:
            if record.min_qty <= 0:
                raise ValidationError("Minimal Qty should be greater then zero.")

    @api.depends('price', 'is_vendor_pricelist_approval_matrix', 'branch_id')
    def _get_vendor_price(self):
        for res in self:
            if res.is_vendor_pricelist_approval_matrix:
                approval_id = self.env['vendor.pricelist.approval.matrix'].search([('branch_id', '=', res.branch_id.id)], limit=1)
                if approval_id:
                    res.vendor_pricelist_approval_matrix = approval_id
                    res.create_approval(approval_id)
                else:
                    res.vendor_pricelist_approval_matrix = False
            else:
                res.vendor_pricelist_approval_matrix = False
                
    def create_approval(self, approval_id):
        for res in self:
            if res.state not in ("waiting_approval","approved","rejected"):
                approval = self.env['product.supplierinfo.approval']
                approval_matrix = res.vendor_pricelist_approval_matrix
                approval_ids = approval.search([('supplier_id', '=', res.id)])
                approval_ids.sudo().unlink()
                res.vendor_pricelist_approval_matrix = approval_matrix
                user = []
                approval_matrix_line = []
                for line in approval_id.approval_matrix_line_ids:
                    lines = approval.create({
                        'sequence': line.sequence,
                        'user_ids': [(6, 0, line.user_ids.ids)],
                        'minimum_approver': line.minimum_approver,
                    })
                    approval_matrix_line.append(lines.id)
                    user.extend(line.user_ids.ids)
                if approval_matrix_line:
                    res.approval_ids = [(6, 0, approval_matrix_line)]
                if user:
                    res.user_approval_ids = [(6, 0, user)]
            
                
    def action_request_for_approval(self):
        for res in self:
            res.state = 'waiting_approval'

    @api.model
    def _vendor_price_list_expire(self):
        today_date = datetime.today()
        vendor_pricelist_ids = self.env['product.supplierinfo'].search([('state', 'in', ('draft', 'waiting_approval', 'approved')), ('date_end', '!=', False), ('date_end', '<', today_date)])
        vendor_pricelist_ids.write({'state': 'expire'})

    @api.model
    def action_vendors_pricelists_menu(self):
        self.env.ref('equip3_purchase_masterdata.menu_vendor_pricelist_to_approve').active = False
        self.env.ref('equip3_purchase_masterdata.menu_vendor_pricelist_changes_to_approve').active = False

        irconfigparam = self.env['ir.config_parameter'].sudo()
        is_vendor_pricelist_approval_matrix = irconfigparam.get_param('is_vendor_pricelist_approval_matrix', False)
        if is_vendor_pricelist_approval_matrix:
            self.env.ref('equip3_purchase_masterdata.menu_vendor_pricelist_to_approve').active = True
            self.env.ref('equip3_purchase_masterdata.menu_vendor_pricelist_changes_to_approve').active = True

    def action_approved(self):
        for res in self:
            rec = 0
            if res.current_user in res.user_approval_ids:
                for line in res.approval_ids:
                    if res.current_user in line.user_ids:
                        if res.current_user not in line.user_approved_ids:
                            user = []
                            user.extend(line.user_approved_ids.ids)
                            user.extend(res.current_user.ids)
                            line.user_approved_ids = [(6, 0, user)]
                            line.approval += 1
                            # if line.status:
                            #   line.status += "\n%s: Approved - %s" % (res.current_user.name, datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"))
                            # else:
                            #   line.status = "%s: Approved - %s" % (res.current_user.name, datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"))
                            if line.status:
                                line.status += "\n%s: Approved" % (res.current_user.name)
                            else:
                                line.status = "%s: Approved" % (res.current_user.name)
                            if line.minimum_approver <= line.approval:
                                line.approved = True
                                line.time = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
                    if line.approved:
                        rec += 1
                if len(res.approval_ids) == rec:
                    res.state = 'approved'
            if res.changes_id:
                old = self.env['product.supplierinfo'].sudo().browse(res.changes_id)
                old.state = 'expire'
    
    def action_reject_supplier(self):
        return {
            'type': 'ir.actions.act_window',
            'name': _('Reject Vendor Pricelist'),
            'res_model': 'cancel.supplier.memory',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'context': {
                'default_supplier_id': self.id,
            },
        }
    
    def action_rejected(self, reason):
        for res in self:
            if res.current_user in res.user_approval_ids:
                for line in res.approval_ids:
                    if res.current_user in line.user_ids:
                        if res.current_user not in line.user_approved_ids:
                            user = []
                            user.extend(line.user_approved_ids.ids)
                            user.extend(res.current_user.ids)
                            # line.user_approved_ids = [(6, 0, user)]
                            line.approval += 1
                            # line.approved = False
                            line.update({
                                'user_approved_ids': [(6, 0, user)],
                                'approved': False
                            })
                            res.state = 'rejected'
                            line.time = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
                            # if line.status:
                            #   line.status += "\n%s: Rejected - %s" % (res.current_user.name, line.time)
                            # else:
                            #   line.status = "%s: Rejected - %s" % (res.current_user.name, line.time)
                            if line.status:
                                line.status += "\n%s: Rejected" % (res.current_user.name)
                            else:
                                line.status = "%s: Rejected" % (res.current_user.name)
                            line.feedback = reason

class SupplierInfoApproval(models.Model):
    _name = "product.supplierinfo.approval"
    
    @api.model
    def default_get(self, fields):
        res = super(SupplierInfoApproval, self).default_get(fields)
        if self._context:
            context_keys = self._context.keys()
            next_sequence = 1
            if 'approval_ids' in context_keys:
                if len(self._context.get('approval_ids')) > 0:
                    next_sequence = len(self._context.get('approval_ids')) + 1
            res.update({'sequence': next_sequence})
        return res
    
    sequence = fields.Integer(string="Sequence")
    sequence2 = fields.Integer(
        string="Sequence",
        related="sequence",
        readonly=True,
        store=True,
        tracking=True
    )
    user_ids = fields.Many2many('res.users', string="User")
    minimum_approver = fields.Integer(string="Minimum Approver")
    status = fields.Text("Approval Status")
    time = fields.Char("Time Stamp")
    feedback = fields.Text("Feedback")
    approved = fields.Boolean("Approved")
    approval = fields.Integer("Approval")
    user_approved_ids = fields.Many2many('res.users', string="User Approved", relation='supplier_approval_supplier_rel')
    supplier_id = fields.Many2one('product.supplierinfo', string="Supplier", domain="[('state1', '=', 'approved')]")

class CanselSupplier(models.TransientModel):
    _name = 'cancel.supplier.memory'
    
    supplier_id = fields.Many2one('product.supplierinfo', 'Source', required=True, domain="[('state1', '=', 'approved')]")
    reason = fields.Text("Reason", required=True)
    
    def action_cancel_supplier(self):
        """
        Reject the specified supplier.
        :return:
        """
        self.supplier_id.action_rejected(self.reason)