
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class ApprovalMatrixVendor(models.Model):
    _name = "approval.matrix.vendor"
    _description = "Approval Matrix Vendor"


    name = fields.Char(string="Name", required=True)
    company_id = fields.Many2one('res.company', string="Company", required=True, readonly=True, default=lambda self: self.env.company.id)
    branch_id = fields.Many2one('res.branch', string="Branch", domain="[('company_id', '=', company_id)]")
    approval_matrix_line_ids = fields.One2many('approval.matrix.vendor.line', 'approval_matrix', string='Approving Matrix')


    @api.constrains('branch_id')
    def _check_existing_record_branch(self):
        for record in self:
            approval_matrix_id = self.search([('branch_id', '=', record.branch_id.id), ('id', '!=', record.id)], limit=1)
            if approval_matrix_id:
                raise ValidationError("There are other approval matrix %s in same branch. Please change branch" % (approval_matrix_id.name))

    def _reset_sequence(self):
        for rec in self:
            current_sequence = 1
            for line in rec.approval_matrix_line_ids:
                line.sequence = current_sequence
                current_sequence += 1
    def copy(self, default=None):
        res = super(ApprovalMatrixVendor, self.with_context(keep_line_sequence=True)).copy(default)
        return res

    @api.onchange('company_id')
    def onchange_company_id(self):
        self.branch_id = False
    
class ApprovalMatrixVendorLine(models.Model):
    _name = "approval.matrix.vendor.line"
    _description = "Approval Matrix Vendor Line"

    @api.model
    def default_get(self, fields):
        res = super(ApprovalMatrixVendorLine, self).default_get(fields)
        if self._context:
            context_keys = self._context.keys()
            next_sequence = 1
            if 'approval_matrix_line_ids' in context_keys:
                if len(self._context.get('approval_matrix_line_ids')) > 0:
                    next_sequence = len(self._context.get('approval_matrix_line_ids')) + 1
            res.update({'sequence': next_sequence})
        return res

    sequence = fields.Integer(string="Sequence")
    user_ids = fields.Many2many('res.users', string="User", required=True)
    minimum_approver = fields.Integer(string="Minimum Approver", default=1, required=True)
    approval_matrix = fields.Many2one('approval.matrix.vendor', string="Approval Matrix")
    approved_users = fields.Many2many('res.users', 'approved_users_res_patner_rel', 'app_mat_id', 'user_id', string='Users')
    state_char = fields.Text(string='Approval Status')
    time_stamp = fields.Datetime(string='TimeStamp')
    feedback = fields.Char(string='Feedback')
    last_approved = fields.Many2one('res.users', string='Users')
    approved = fields.Boolean('Approved')
    app_matrix_id = fields.Many2one('res.partner', string="Approved Matrix")
    sequence2 = fields.Integer(
        string="No.",
        related="sequence",
        readonly=True,
        store=True,
        tracking=True
    )

    def unlink(self):
        approval = self.approval_matrix
        res = super(ApprovalMatrixVendorLine, self).unlink()
        approval._reset_sequence()
        return res

    @api.model
    def create(self, vals):
        res = super(ApprovalMatrixVendorLine, self).create(vals)
        if not self.env.context.get("keep-line_sequence", False):
            res.approval_matrix._reset_sequence()
        return res 
