
from . import product_template
from . import product_category
from . import purchase_order
from . import res_partner
from . import approval_matrix_vendor
from . import res_config_settings
from . import vendor_pricelist_approval_matrix
from . import product_supplier
from . import purchase_agreement
from . import purchase_requisition
from . import vendor_evaluation