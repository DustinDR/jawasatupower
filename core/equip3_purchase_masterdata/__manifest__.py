# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Equip3 Purchase Masterdata',
    'version': '1.1.28',
    'summary': 'Manage your Purchase Masterdata',
    'description': "",
    'depends': ['branch', 'hr', 'purchase', 'vendor_evaluation', 'sh_all_in_one_purchase_tools', 'equip3_purchase_accessright_setting', 'setu_rfm_analysis', 'sh_po_tender_management', 'equip3_general_features', 'sh_vendor_signup', 'equip3_purchase_accessright_setting', 'vendor_evaluation', 'purchase_stock', 'purchase_last_price_info', 'dynamic_product_bundle'],
    'category': 'Purchase/Purchase',
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence.xml',
        'data/ir_cron_vendor_pricelist_expire.xml',
        'data/agreement_type_data.xml',
        'wizards/approval_matrix_vendor_reject_views.xml',
        'views/purchase_order_views.xml',
        'views/product_category.xml',
        'views/res_partner_views.xml',
        'views/approval_matrix_vendor_views.xml',
        'views/vendor_pricelist_approval_matrix_views.xml',
        'views/product_supplier_views.xml',
        'views/approval_matrix_vendor_sequence.xml',
        'views/product_template_views.xml',
        'views/vendor_evaluation_view.xml',
        # 'views/direct_purchase_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
