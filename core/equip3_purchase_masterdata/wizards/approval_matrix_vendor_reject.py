
from odoo import api , models, fields 
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

class ApprovalMatrixVendorReject(models.TransientModel):
    _name = 'approval.matrix.vendor.reject'

    reason = fields.Text(string="Reason")

    def action_confirm(self):
        vendor_request_id = self.env['res.partner'].browse([self._context.get('active_id')])
        user = self.env.user
        approving_matrix_line = sorted(vendor_request_id.approved_matrix_ids.filtered(lambda r:not r.approved), key=lambda r:r.sequence)
        if approving_matrix_line:
            matrix_line = approving_matrix_line[0]
            name = matrix_line.state_char or ''
            if name != '':
                name += "\n • %s: Rejected" % (user.name)
            else:
                name += "• %s: Rejected" % (user.name)
            matrix_line.write({'state_char': name, 'time_stamp': datetime.now(), 'feedback': self.reason})
            vendor_request_id.state = 'rejected'
