14.0.1 (23 Oct 2020)
----------------------------
- Initial Release


14.0.2 (23 Feb 2021)
==================
==> [Add] Global Document Search Added.
==> [Add] Scan Barcode and add into barcode field in product variant and template. 
==> [fix] choose of front and back camera not work properly.


14.0.3 (30 July 2021)
==================
==> [Add] Barcode scanner in search view.
