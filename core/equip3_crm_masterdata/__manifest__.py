# -*- coding: utf-8 -*-
{
   'name': 'Equip3 CRM Masterdata',
   'version': '1.1.1',
   'author': 'Hashmicro / Prince',
   'depends': ['base', 'equip3_accounting_efaktur', 'equip3_sale_masterdata'],
   'data': [
      "views/res_partner_views.xml",
   ],
   'installable': True,
}
