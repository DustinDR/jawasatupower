from odoo import api, fields, models
import logging
_logger = logging.getLogger(__name__)


class AccountAnalyticAccount(models.Model):
    _inherit = 'account.analytic.account'

    group_id = fields.Many2one(
        'account.analytic.group', string='Analytic Category',
        check_company=True, tracking=True)
    company_id = fields.Many2one(
        'res.company', string='Company',
        default=lambda self: self.env.company, tracking=True)
