from odoo import api, fields, models, _

class AccountAnalyticDistribution(models.Model):
    _inherit = 'account.analytic.distribution'
    
    analytic_group_id = fields.Many2one('account.analytic.group',string="Analytic Category", tracking=True)
