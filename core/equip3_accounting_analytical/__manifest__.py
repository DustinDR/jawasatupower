{
    'name': 'Accounting Analytical',
    'version': '1.1.5',
    'category' : 'Extra Tools',
    'depends': [
        'account',
        'analytic',
        'branch'
    ],
    'depends': ['account', 'analytic', 'branch'],
    'data': [
        'security/ir.model.access.csv', 'views/account_analytic_tag_views.xml',
        'views/inherit_account_analytic_tag_views.xml', 'data/data.xml'
    ],
    'demo': [],
    'auto_install': False,
    'installable': True,
    'application': True
}