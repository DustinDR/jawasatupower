from odoo import api, fields, models


class TravelResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    travel_type_approval = fields.Selection(
        [('employee_hierarchy', 'By Employee Hierarchy'), ('approval_matrix', 'By Approval Matrix')], default='employee_hierarchy',
        config_parameter='equip3_hr_travel_extend.travel_type_approval')
    travel_level = fields.Integer(config_parameter='equip3_hr_travel_extend.travel_level', default=1)

    @api.onchange("travel_level")
    def _onchange_travel_level(self):
        if self.travel_level < 1:
            self.travel_level = 1