# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import hr_travel_extend
from . import res_config_settings
from . import hr_travel_approval_matrix
from . import hr_travel_cancel