# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import _, api, fields, models
from datetime import date, datetime, timedelta
from pytz import timezone
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, float_compare
import time
from dateutil.relativedelta import relativedelta
from odoo.exceptions import Warning, UserError
from odoo.exceptions import UserError, ValidationError


class HrTravelRequest(models.Model):
    _inherit = "travel.request"
    _order = "create_date desc"


    def _default_employee(self):
        return self.env.user.employee_id
    
    name = fields.Char(string="Name", readonly=True, default='New')
    employee_id = fields.Many2one('hr.employee', 'Employee', default=_default_employee)
    cash_advance_ids = fields.One2many('travel.vendor.deposit', 'travel_cash_id', string="Cash Advance")
    cash_advance_orgin_id = fields.Many2one('vendor.deposit', string="Created Cash Advance")
    cash_advance_state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('approved', 'Approved'),
        ('post', 'Paid'),
        ('returned', 'Returned'),
        ('converted', 'Convert as Expense'),
        ('reconciled', 'Reconciled'),
        ('cancelled', 'Cancelled'),
        ('rejected', 'Rejected'),
    ],
        string="Status", readonly=True, related='cash_advance_orgin_id.state')
    state = fields.Selection(
        [('draft', 'Draft'), ('confirmed', 'To Approved'), ('approved', 'Approved'), ('cancelled', 'Cancelled'), ('rejected', 'Rejected'),
         ('cash_advance_submitted', 'Cash Advance Created'),
         ('returned', 'Returned'), ('submitted', 'Expenses Created')], default="draft", string="States")
    state1 = fields.Selection(related="state", tracking=False)
    is_return_button_visible = fields.Boolean('Return Button Visible', compute='compute_is_return', default=False)

    travel_approver_user_ids = fields.One2many('travel.approver.user', 'emp_travel_id', string='Approver')
    approvers_ids = fields.Many2many('res.users', 'emp_travel_approvers_rel', string='Approvers List')
    approved_user_ids = fields.Many2many('res.users', string='Approved by User')
    is_approver = fields.Boolean(string="Is Approver", compute='_compute_can_approve')
    approved_user_text = fields.Text(string="Approved User", tracking=True)
    approved_user = fields.Text(string="Approved User", tracking=True)
    feedback_parent = fields.Text(string='Parent Feedback')
    domain_employee_ids = fields.Many2many('hr.employee',string="Employee Domain",compute='_compute_employee_ids')
    is_readonly = fields.Boolean(compute='_compute_read_only')
    
    
    def action_confirm(self):
        self.write({'state': 'confirmed','confirm_date':fields.datetime.now(),
            'confirm_by': self.env.user.id})
        self.approver_mail()
        return
    
    def custom_menu(self):
        views = [(self.env.ref('bi_employee_travel_managment.view_travel_req_tree').id, 'tree'),
                     (self.env.ref('bi_employee_travel_managment.view_travel_req_form').id, 'form')]
    # search_view_id = self.env.ref("hr_contract.hr_contract_view_search")
        if  self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_travel_supervisor') and not self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_travel_manager'):
            employee_ids = []
            my_employee = self.env['hr.employee'].sudo().search([('user_id','=',self.env.user.id)])
            if my_employee:
                for child_record in my_employee.child_ids:
                    employee_ids.append(my_employee.id)
                    employee_ids.append(child_record.id)
                    child_record._get_amployee_hierarchy(employee_ids,child_record.child_ids,my_employee.id)
                return {
                    'type': 'ir.actions.act_window',
                    'name': 'Employee Travel Request',
                    'res_model': 'travel.request',
                    'target':'current',
                    'view_mode': 'tree,form',
                    'views':views,
                    'domain': [('employee_id', 'in', employee_ids)],
                    'context':{},
                    'help':"""<p class="o_view_nocontent_smiling_face">
                        Create a new Employee Travel Request
                    </p>"""
                    # 'context':{'search_default_current':1, 'search_default_group_by_state': 1},
                    # 'search_view_id':search_view_id.id,
                    
                }
        elif self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_travel_self_service') and not self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_travel_supervisor'):
            return {
                'type': 'ir.actions.act_window',
                'name': 'Employee Travel Request',
                'res_model': 'travel.request',
                'target':'current',
                'view_mode': 'tree,form',
                'domain': [('employee_id.user_id', '=', self.env.user.id)],
                'help':"""<p class="o_view_nocontent_smiling_face">
                    Create a new Employee Travel Request
                </p>""",
                'context':{},
                'views':views,
                # 'search_view_id':search_view_id.id,
            }
        else:
            return {
                'type': 'ir.actions.act_window',
                'name': 'Employee Travel Request',
                'res_model': 'travel.request',
                'target':'current',
                'view_mode': 'tree,form',
                'help':"""<p class="o_view_nocontent_smiling_face">
                    Create a new Employee Travel Request
                </p>""",
                'context':{},
                'views':views,
                # 'search_view_id':search_view_id.id,
            }
    
    
    @api.depends('employee_id')
    def _compute_read_only(self):
        for record in self:
            if self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_travel_self_service') and not self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_travel_supervisor'):
                record.is_readonly = True
            else:
                record.is_readonly = False
                
    @api.depends('employee_id')
    def _compute_employee_ids(self):
        for record in self:
            employee_ids = []
            if self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_travel_supervisor') and not self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_travel_manager'):
                my_employee = self.env['hr.employee'].sudo().search([('user_id','=',self.env.user.id)])
                if my_employee:
                    for child_record in my_employee.child_ids:
                        employee_ids.append(my_employee.id)
                        employee_ids.append(child_record.id)
                        child_record._get_amployee_hierarchy(employee_ids,child_record.child_ids,my_employee.id)
                record.domain_employee_ids = [(6,0,employee_ids)]
            else:
                all_employee = self.env['hr.employee'].sudo().search([])
                for data_employee in all_employee:
                    employee_ids.append(data_employee.id)
                record.domain_employee_ids = [(6,0,employee_ids)]

    def create_cash_advance(self):
        for rec in self:
            if len(rec.cash_advance_ids) == 0:
                raise ValidationError(_('Please fill Cash Advance Lines.'))
            else:
                ICP = self.env['ir.config_parameter'].sudo()
                vendor_deposit_id = self.env['vendor.deposit'].create({
                    'travel_id': rec.id,
                    'employee_id': rec.employee_id.id,
                    'advance_date': datetime.now(),
                    'communication': rec.travel_purpose,
                    'currency_id': rec.currency_id.id,
                    'deposit_reconcile_journal_id': int(ICP.get_param('deposit_reconcile_journal_id')),
                    'deposit_account_id': int(ICP.get_param('deposit_account_id')),
                    'journal_id': int(ICP.get_param('journal_id')),
                    'is_cash_advance': True,
                    'advance_line_ids': [(0, 0, {
                        "name": line["name"],
                        "amount": line["amount"],
                    }) for line in rec.cash_advance_ids],
                })
                vendor_deposit_id.onchange_amount()
                vendor_deposit_id.onchange_approver_user()
                rec.update({'cash_advance_orgin_id': vendor_deposit_id.id,
                            'state': 'cash_advance_submitted',
                            })

    @api.depends('state', 'cash_advance_state')
    def compute_is_return(self):
        for rec in self:
            if rec.state == 'approved':
                rec.is_return_button_visible = True
            elif rec.state == 'cash_advance_submitted' and rec.cash_advance_state == 'post':
                rec.is_return_button_visible = True
            else:
                rec.is_return_button_visible = False

    def action_create_expence(self):
        id_lst = []
        for rec in self:
            if len(rec.expense_ids) == 0:
                raise ValidationError(_('Please fill Expenses Lines.'))
            else:
                for line in rec.expense_ids:
                    id_lst.append(line.id)
                if len(rec.cash_advance_ids) == 0:
                    res = self.env['hr.expense.sheet'].create(
                        {'name': rec.travel_purpose, 'employee_id': rec.employee_id.id, 'travel_expense': True,
                         'expense_line_ids': [(6, 0, id_lst)], 'travel_id': rec.id})
                    res.exp_difference = res.total_amount
                    res.write({'state': 'submit'})
                    res.onchange_amount_sum()
                    res.activity_update()
                else:
                    res = self.env['hr.expense.sheet'].create(
                        {'name': rec.travel_purpose, 'employee_id': rec.employee_id.id, 'travel_expense': True,
                         'expense_line_ids': [(6, 0, id_lst)], 'expense_advance': True,
                         'cash_advance_number_ids': rec.cash_advance_orgin_id,
                         'cash_advance_amount': rec.cash_advance_orgin_id.amount, 'travel_id': rec.id,
                         'is_ca_travel': True})
                    res.exp_difference = res.total_amount
                    res.write({'state': 'submit'})
                    res.onchange_amount_sum()
                    res.activity_update()
                rec.expence_sheet_id = res.id
                rec.write({'state': 'submitted'})
            return

    @api.onchange('employee_id', 'travel_purpose')
    def onchange_approver_user(self):
        for travel in self:
            if travel.travel_approver_user_ids:
                remove = []
                for line in travel.travel_approver_user_ids:
                    remove.append((2, line.id))
                travel.travel_approver_user_ids = remove
            setting = self.env['ir.config_parameter'].sudo().get_param(
                'equip3_hr_travel_extend.travel_type_approval')
            if setting == 'employee_hierarchy':
                travel.travel_approver_user_ids = self.travel_emp_by_hierarchy(travel)
                self.app_list_travel_emp_by_hierarchy()
            if setting == 'approval_matrix':
                self.travel_approval_by_matrix(travel)

    def travel_emp_by_hierarchy(self, travel):
        approval_ids = []
        seq = 1
        data = 0
        line = self.get_manager(travel, travel.employee_id, data, approval_ids, seq)
        return line

    def get_manager(self, travel, employee_manager, data, approval_ids, seq):
        setting_level = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_travel_extend.travel_level')
        if not setting_level:
            raise ValidationError("Level not set")
        if not employee_manager['parent_id']['user_id']:
            return approval_ids
        while data < int(setting_level):
            approval_ids.append(
                (0, 0, {'user_ids': [(4, employee_manager['parent_id']['user_id']['id'])]}))
            data += 1
            seq += 1
            if employee_manager['parent_id']['user_id']['id']:
                self.get_manager(travel, employee_manager['parent_id'], data, approval_ids, seq)
                break
        return approval_ids

    def app_list_travel_emp_by_hierarchy(self):
        for travel in self:
            app_list = []
            for line in travel.travel_approver_user_ids:
                app_list.append(line.user_ids.id)
            travel.approvers_ids = app_list

    def travel_approval_by_matrix(self, travel):
        app_list = []
        approval_matrix = self.env['hr.travel.approval.matrix'].search([('apply_to', '=', 'by_employee')])
        matrix = approval_matrix.filtered(lambda line: travel.employee_id.id in line.employee_ids.ids)
        if matrix:
            data_approvers = []
            for line in matrix[0].approval_matrix_ids:
                data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                              'user_ids': [(6, 0, line.approvers.ids)]}))
                for approvers in line.approvers:
                    app_list.append(approvers.id)
            travel.approvers_ids = app_list
            travel.travel_approver_user_ids = data_approvers

        if not matrix:
            data_approvers = []
            approval_matrix = self.env['hr.travel.approval.matrix'].search([('apply_to', '=', 'by_job_position')])
            matrix = approval_matrix.filtered(lambda line: travel.job_id.id in line.job_ids.ids)
            if matrix:
                for line in matrix[0].approval_matrix_ids:
                    data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                                  'user_ids': [(6, 0, line.approvers.ids)]}))
                    for approvers in line.approvers:
                        app_list.append(approvers.id)
                travel.approvers_ids = app_list
                travel.travel_approver_user_ids = data_approvers
            if not matrix:
                data_approvers = []
                approval_matrix = self.env['hr.travel.approval.matrix'].search([('apply_to', '=', 'by_department')])
                matrix = approval_matrix.filtered(lambda line: travel.department_id.id in line.department_ids.ids)
                if matrix:
                    for line in matrix[0].approval_matrix_ids:
                        data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                                      'user_ids': [(6, 0, line.approvers.ids)]}))
                        for approvers in line.approvers:
                            app_list.append(approvers.id)
                    travel.approvers_ids = app_list
                    travel.travel_approver_user_ids = data_approvers

    @api.depends('state', 'employee_id')
    def _compute_can_approve(self):
        for travel in self:
            if travel.approvers_ids:
                setting = self.env['ir.config_parameter'].sudo().get_param(
                    'equip3_hr_travel_extend.travel_type_approval')
                setting_level = self.env['ir.config_parameter'].sudo().get_param(
                    'equip3_hr_travel_extend.travel_level')
                app_level = int(setting_level)
                current_user = travel.env.user
                if setting == 'employee_hierarchy':
                    matrix_line = sorted(travel.travel_approver_user_ids.filtered(lambda r: r.is_approve == True))
                    app = len(matrix_line)
                    a = len(travel.travel_approver_user_ids)
                    if app < app_level and app < a:
                        if current_user in travel.travel_approver_user_ids[app].user_ids:
                            travel.is_approver = True
                        else:
                            travel.is_approver = False
                    else:
                        travel.is_approver = False
                elif setting == 'approval_matrix':
                    matrix_line = sorted(travel.travel_approver_user_ids.filtered(lambda r: r.is_approve == True))
                    app = len(matrix_line)
                    a = len(travel.travel_approver_user_ids)
                    if app < a:
                        for line in travel.travel_approver_user_ids[app]:
                            if current_user in line.user_ids:
                                travel.is_approver = True
                            else:
                                travel.is_approver = False
                    else:
                        travel.is_approver = False

                else:
                    travel.is_approver = False
            else:
                travel.is_approver = False

    def action_approve(self):
        sequence_matrix = [data.name for data in self.travel_approver_user_ids]
        sequence_approval = [data.name for data in self.travel_approver_user_ids.filtered(
            lambda line: len(line.approved_employee_ids) != line.minimum_approver)]
        max_seq = max(sequence_matrix)
        min_seq = min(sequence_approval)
        approval = self.travel_approver_user_ids.filtered(
            lambda line: self.env.user.id in line.user_ids.ids and len(
                line.approved_employee_ids) != line.minimum_approver and line.name == min_seq)
        for record in self:
            current_user = self.env.uid
            setting = self.env['ir.config_parameter'].sudo().get_param(
                'equip3_hr_travel_extend.travel_type_approval')
            now = datetime.now(timezone(self.env.user.tz))
            dateformat = f"{now.day}/{now.month}/{now.year} {now.hour}:{now.minute}:{now.second}"
            date_approved = time.strftime(DEFAULT_SERVER_DATE_FORMAT)
            date_approved_obj = datetime.strptime(date_approved, DEFAULT_SERVER_DATE_FORMAT)
            if setting == 'employee_hierarchy':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for user in record.travel_approver_user_ids:
                            if current_user == user.user_ids.id:
                                user.is_approve = True
                                user.timestamp = fields.Datetime.now()
                                user.approver_state = 'approved'
                                string_approval = []
                                if user.approval_status:
                                    string_approval.append(f"{self.env.user.name}:Approved")
                                    user.approval_status = "\n".join(string_approval)
                                    string_timestammp = [user.approved_time]
                                    string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                    user.approved_time = "\n".join(string_timestammp)
                                    if record.feedback_parent:
                                        feedback_list = [user.feedback,
                                                         f"{self.env.user.name}:{record.feedback_parent}"]
                                        final_feedback = "\n".join(feedback_list)
                                        user.feedback = f"{final_feedback}"
                                    elif user.feedback and not record.feedback_parent:
                                        user.feedback = user.feedback
                                    else:
                                        user.feedback = ""
                                else:
                                    user.approval_status = f"{self.env.user.name}:Approved"
                                    user.approved_time = f"{self.env.user.name}:{dateformat}"
                                    if record.feedback_parent:
                                        user.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                    else:
                                        user.feedback = ""
                                record.approved_user_ids = [(4, current_user)]
                        matrix_line = sorted(record.travel_approver_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            record.write({'state': 'approved', 'approve_date': fields.datetime.now(),
                                          'approve_by': self.env.user.id})
                            self.approved_mail()
                        else:
                            record.approved_user = self.env.user.name + ' ' + 'has been approved the Request!'
                            if len(approval.approved_employee_ids) == approval.minimum_approver and not approval.name == max_seq:
                                self.approver_mail()
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved'
                    ))
            elif setting == 'approval_matrix':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for line in record.travel_approver_user_ids:
                            for user in line.user_ids:
                                if current_user == user.user_ids.id:
                                    line.timestamp = fields.Datetime.now()
                                    record.approved_user_ids = [(4, current_user)]
                                    var = len(line.approved_employee_ids) + 1
                                    if line.minimum_approver <= var:
                                        line.approver_state = 'approved'
                                        string_approval = []
                                        string_approval.append(line.approval_status)
                                        if line.approval_status:
                                            string_approval.append(f"{self.env.user.name}:Approved")
                                            line.approval_status = "\n".join(string_approval)
                                            string_timestammp = [line.approved_time]
                                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                            line.approved_time = "\n".join(string_timestammp)
                                            if record.feedback_parent:
                                                feedback_list = [line.feedback,
                                                                 f"{self.env.user.name}:{record.feedback_parent}"]
                                                final_feedback = "\n".join(feedback_list)
                                                line.feedback = f"{final_feedback}"
                                            elif line.feedback and not record.feedback_parent:
                                                line.feedback = line.feedback
                                            else:
                                                line.feedback = ""
                                        else:
                                            line.approval_status = f"{self.env.user.name}:Approved"
                                            line.approved_time = f"{self.env.user.name}:{dateformat}"
                                            if record.feedback_parent:
                                                line.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                            else:
                                                line.feedback = ""
                                        line.is_approve = True
                                    else:
                                        line.approver_state = 'pending'
                                        if line.approval_status:
                                            string_approval.append(f"{self.env.user.name}:Approved")
                                            line.approval_status = "\n".join(string_approval)
                                            string_timestammp = [line.approved_time]
                                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                            line.approved_time = "\n".join(string_timestammp)
                                            if record.feedback_parent:
                                                feedback_list = [line.feedback,
                                                                 f"{self.env.user.name}:{record.feedback_parent}"]
                                                final_feedback = "\n".join(feedback_list)
                                                line.feedback = f"{final_feedback}"
                                            elif line.feedback and not record.feedback_parent:
                                                line.feedback = line.feedback
                                            else:
                                                line.feedback = ""
                                        else:
                                            line.approval_status = f"{self.env.user.name}:Approved"
                                            line.approved_time = f"{self.env.user.name}:{dateformat}"
                                            if record.feedback_parent:
                                                line.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                            else:
                                                line.feedback = ""
                                    line.approved_employee_ids = [(4, current_user)]

                        matrix_line = sorted(record.travel_approver_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Request!'
                            record.write({'state': 'approved', 'approve_date': fields.datetime.now(),
                                          'approve_by': self.env.user.id})
                            self.approved_mail()
                        else:
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Request!'
                            if len(approval.approved_employee_ids) == approval.minimum_approver and not approval.name == max_seq:
                                self.approver_mail()
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved!'
                    ))
            else:
                raise ValidationError(_(
                    'Already approved!'
                ))

    def action_reject(self):
        for record in self:
            for user in record.travel_approver_user_ids:
                for check_user in user.user_ids:
                    now = datetime.now(timezone(self.env.user.tz))
                    dateformat = f"{now.day}/{now.month}/{now.year} {now.hour}:{now.minute}:{now.second}"
                    if self.env.uid == check_user.id:
                        user.timestamp = fields.Datetime.now()
                        user.approver_state = 'refuse'
                        string_approval = []
                        string_approval.append(user.approval_status)
                        if user.approval_status:
                            string_approval.append(f"{self.env.user.name}:Refused")
                            user.approval_status = "\n".join(string_approval)
                            string_timestammp = [user.approved_time]
                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                            user.approved_time = "\n".join(string_timestammp)
                        else:
                            user.approval_status = f"{self.env.user.name}:Refused"
                            user.approved_time = f"{self.env.user.name}:{dateformat}"
            record.approved_user = self.env.user.name + ' ' + 'has been Rejected!'
            record.write({'state': 'rejected'})
            self.reject_mail()

    def wizard_approve(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'travel.request.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'name': "Confirmation Message",
            'target': 'new',
        }

    def return_from_trip(self):
        # start_travel = self.req_departure_date.date()
        # while start_travel <= self.req_return_date.date():
        #     self.env['hr.attendance'].create({
        #         'employee_id': self.employee_id.id,
        #         'check_in': False,
        #         'check_out': False,
        #         'start_working_date': start_travel,
        #         'is_created': True,
        #         'attendance_status': 'travel'
        #     })
        #     start_travel += relativedelta(days=1)
        self.write({'state': 'returned'})
        id_lst = []
        for line in self.advance_payment_ids:
            id_lst.append(line.id)
        self.expense_ids = [(6, 0, id_lst)]
        return

    def _cron_create_attendance_travel(self):
        today = date.today()
        travels = self.search([('state', '=', 'approved')])
        if travels:
            for travel in travels:
                if travel.req_departure_date.date() == today:
                    start_travel = travel.req_departure_date.date()
                    while start_travel <= travel.req_return_date.date():
                        self.env['hr.attendance'].create({
                            'employee_id': travel.employee_id.id,
                            'check_in': False,
                            'check_out': False,
                            'start_working_date': start_travel,
                            'is_created': True,
                            'attendance_status': 'travel'
                        })
                        start_travel += relativedelta(days=1)

    def unlink(self):
        for rec in self:
            if rec.state not in ['draft']:
                raise Warning("You cannot delete a Travel Request which is in Approved state.")
            return super(HrTravelRequest, rec).unlink()

    # Emails
    def get_url(self, obj):
        url = ''
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        menu_id = self.env['ir.model.data'].get_object_reference(
            'bi_employee_travel_managment', 'menu_travel_request_approve')[1]
        action_id = self.env['ir.model.data'].get_object_reference(
            'bi_employee_travel_managment', 'action_travel_req_hr')[1]
        url = base_url + "/web?db=" + str(self._cr.dbname) + "#id=" + str(
            obj.id) + "&view_type=form&model=travel.request&menu_id=" + str(
            menu_id) + "&action=" + str(action_id)
        return url

    def approver_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.travel_approver_user_ids:
                matrix_line = sorted(rec.travel_approver_user_ids.filtered(lambda r: r.is_approve == True))
                approver = rec.travel_approver_user_ids[len(matrix_line)]
                for user in approver.user_ids:
                    try:
                        template_id = ir_model_data.get_object_reference(
                            'equip3_hr_travel_extend',
                            'email_template_travel_request_approval')[1]
                    except ValueError:
                        template_id = False
                    ctx = self._context.copy()
                    url = self.get_url(self)
                    ctx.update({
                        'email_from': self.env.user.email,
                        'email_to': user.email,
                        'url': url,
                        'approver_name': user.name,
                        'emp_name': self.employee_id.name,
                    })
                    if self.req_departure_date:
                        ctx.update(
                            {'date_from': fields.Datetime.from_string(self.req_departure_date).strftime('%d/%m/%Y %I:%M:%S %p')})
                    if self.req_return_date:
                        ctx.update(
                            {'date_to': fields.Datetime.from_string(self.req_return_date).strftime('%d/%m/%Y %I:%M:%S %p')})
                    self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id, force_send=True)
                break

    def approved_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.travel_approver_user_ids:
                try:
                    template_id = ir_model_data.get_object_reference(
                        'equip3_hr_travel_extend',
                        'email_template_travel_request_approved')[1]
                except ValueError:
                    template_id = False
                ctx = self._context.copy()
                url = self.get_url(self)
                ctx.update({
                    'email_from': self.env.user.email,
                    'email_to': self.employee_id.user_id.email,
                    'url': url,
                    'emp_name': self.employee_id.name,
                })
                self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                          force_send=True)
            break

    def reject_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.travel_approver_user_ids:
                try:
                    template_id = ir_model_data.get_object_reference(
                        'equip3_hr_travel_extend',
                        'email_template_travel_request_rejection')[1]
                except ValueError:
                    template_id = False
                ctx = self._context.copy()
                url = self.get_url(self)
                ctx.update({
                    'email_from': self.env.user.email,
                    'email_to': self.employee_id.user_id.email,
                    'url': url,
                    'emp_name': self.employee_id.name,
                })
                self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(rec.id,
                                                                                          force_send=True)
            break

class HrTravelVendorDeposit(models.Model):
    _name = 'travel.vendor.deposit'

    travel_cash_id = fields.Many2one('travel.request', string='Travel Id')
    name = fields.Char(string="Description", required=True)
    employee_id = fields.Many2one('hr.employee', string="Employee", related='travel_cash_id.employee_id')
    amount = fields.Float(string="Amount", required=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('approved', 'Approved'),
        ('post', 'Paid'),
        ('returned', 'Returned'),
        ('converted', 'Convert as Expense'),
        ('reconciled', 'Reconciled'),
        ('cancelled', 'Cancelled'),
        ('rejected', 'Rejected'),
    ],
        string="Status", readonly=True, related='travel_cash_id.cash_advance_state')


class HrTravelCashAdvance(models.Model):
    _inherit = 'vendor.deposit'

    travel_id = fields.Many2one('travel.request', string='Travel')

    def action_confirm(self):
        for rec in self:
            if rec.travel_id.cash_advance_ids:
                remove = []
                for line in rec.travel_id.cash_advance_ids:
                    remove.append((2, line.id))
                rec.travel_id.cash_advance_ids = remove
            if rec.travel_id:
                rec.travel_id.update({'state': 'cash_advance_submitted',
                                      'cash_advance_orgin_id': rec.id})
                cash_ca_lines = []
                for ca_lines in rec.advance_line_ids:
                    cash_ca_lines.append((0, 0, {'name': ca_lines.name,
                                                 'amount': ca_lines.amount}))
                rec.travel_id.cash_advance_ids = cash_ca_lines
            result = super(HrTravelCashAdvance, self).action_confirm()
            return result

    @api.onchange('travel_id')
    def onchange_trvel_ca_lines(self):
        for rec in self:
            if rec.advance_line_ids:
                remove = []
                for line in rec.advance_line_ids:
                    remove.append((2, line.id))
                rec.advance_line_ids = remove
            rec.communication = rec.travel_id.travel_purpose
            rec.update({'communication': rec.travel_id.travel_purpose})
            travel_ca_lines = []
            for travel_lines in rec.travel_id.cash_advance_ids:
                travel_ca_lines.append((0, 0, {'name': travel_lines.name,
                                               'amount': travel_lines.amount}))
            rec.advance_line_ids = travel_ca_lines


class HrExpenseSheet(models.Model):
    _inherit = 'hr.expense.sheet'

    travel_id = fields.Many2one('travel.request', string='Travel',
                                domain=lambda self: "[('employee_id', '=', employee_id), ('state', '=', 'returned')]")
    is_ca_travel = fields.Boolean('Is CA Travel', default=False)
    is_ca_number_readonly = fields.Boolean('CA Number Readonly', compute='compute_is_ca_number', default=False)
    is_exp_line_readonly = fields.Boolean('Is Expense Line', default=False)

    @api.depends('travel_id', 'cash_advance_amount')
    def compute_is_ca_number(self):
        for rec in self:
            if rec.is_ca_travel:
                rec.is_ca_number_readonly = True
            elif rec.state != 'draft':
                rec.is_ca_number_readonly = True
            else:
                rec.is_ca_number_readonly = False

    @api.onchange('travel_id', 'cash_advance_number_ids')
    def onchange_travel_id(self):
        for rec in self:
            if rec.travel_id and rec.travel_id.cash_advance_orgin_id:
                ca_orgin = rec.travel_id.cash_advance_orgin_id
                rec.expense_advance = True
                rec.cash_advance_number_ids = ca_orgin
                rec.cash_advance_amount = ca_orgin.amount
            elif not rec.travel_id:
                rec.get_ca_remaining_amount()
            else:
                rec.expense_advance = False
                rec.cash_advance_number_ids = False
                rec.cash_advance_amount = False

    def action_submit_sheet(self):
        for rec in self:
            if rec.travel_id and rec.travel_id.expense_ids:
                remove = []
                for line in rec.travel_id.expense_ids:
                    remove.append((2, line.id))
                rec.travel_id.expense_ids = remove
            if rec.travel_id:
                rec.travel_id.update({'expence_sheet_id': rec.id,
                                      'state': 'submitted'})
                expense_lines = []
                for exp_expense_lines in rec.expense_line_ids:
                    expense_lines.append((0, 0, {'date': exp_expense_lines.date,
                                                 'name': exp_expense_lines.name,
                                                 'employee_id': exp_expense_lines.employee_id.id,
                                                 'analytic_account_id': exp_expense_lines.analytic_account_id.id,
                                                 'tax_ids': [(6, 0, exp_expense_lines.tax_ids.ids)],
                                                 'total_amount': exp_expense_lines.total_amount,
                                                 'state': exp_expense_lines.state,
                                                 'unit_amount': exp_expense_lines.unit_amount}))
                rec.travel_id.expense_ids = expense_lines
            result = super(HrExpenseSheet, self).action_submit_sheet()
            return result

    @api.onchange('travel_id')
    def onchange_trvel_exp_lines(self):
        for rec in self:
            if rec.expense_line_ids:
                remove = []
                for line in rec.expense_line_ids:
                    remove.append((2, line.id))
                rec.expense_line_ids = remove
            expense_ca_lines = []
            for travel_expense_lines in rec.travel_id.expense_ids:
                expense_ca_lines.append((0, 0, {'date': travel_expense_lines.date,
                                                'name': travel_expense_lines.name,
                                                'employee_id': travel_expense_lines.employee_id.id,
                                                'analytic_account_id': travel_expense_lines.analytic_account_id.id,
                                                'tax_ids': [(6, 0, travel_expense_lines.tax_ids.ids)],
                                                'total_amount': travel_expense_lines.total_amount,
                                                'state': travel_expense_lines.state,
                                                'unit_amount': travel_expense_lines.unit_amount}))
            rec.expense_line_ids = expense_ca_lines
            if len(rec.travel_id.expense_ids) == 0:
                rec.is_exp_line_readonly = False
            else:
                rec.is_exp_line_readonly = True


class TravelApproverUser(models.Model):
    _name = 'travel.approver.user'

    emp_travel_id = fields.Many2one('travel.request', string="Employee Travel Id")
    name = fields.Integer('Sequence', compute="fetch_sl_no")
    user_ids = fields.Many2many('res.users', string="Approvers")
    approved_employee_ids = fields.Many2many('res.users', 'emp_travel_user_ids', string="Approved user")
    minimum_approver = fields.Integer(string="Minimum Approver", default=1)
    timestamp = fields.Datetime(string="Timestamp")
    approved_time = fields.Text(string="Timestamp")
    feedback = fields.Text()
    approver_state = fields.Selection([('draft', 'Draft'), ('pending', 'Pending'), ('approved', 'Approved'),
                                       ('refuse', 'Refused')], default='draft', string="Approval Status")
    approval_status = fields.Text()
    is_approve = fields.Boolean(string="Is Approve", default=False)

    @api.depends('emp_travel_id')
    def fetch_sl_no(self):
        sl = 0
        for line in self.emp_travel_id.travel_approver_user_ids:
            sl = sl + 1
            line.name = sl
