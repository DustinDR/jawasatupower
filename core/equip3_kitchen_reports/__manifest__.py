# -*- coding: utf-8 -*-

{
    'name': 'equip3_kitchen_reports',
    'version': '1.1.5',
    'category': '',
    'description': '',
    'author': 'HashMicro',
    'website': 'www.hashmicro.com',
    'depends': [
        'web',
        'mrp',
        'stock',
        'equip3_kitchen_accessright_settings',
        'equip3_kitchen_operations'
    ],
    'data': [
        'templates/templates.xml',
        'views/kitchen_production_views.xml',
        'views/stock_move_views.xml',
        'views/kitchen_menuitems.xml',
        'views/kitchen_cost_details.xml',
        'templates/equip3_kitchen_reports_templates.xml',
        'reports/kitchen_production_record_report.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
