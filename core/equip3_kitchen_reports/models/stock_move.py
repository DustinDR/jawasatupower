from odoo import models, fields, api
from lxml import etree
import logging

_logger = logging.getLogger(__name__)


PIVOT_SELECTED_FIELDS = [
	'warehouse_id', 'kitchen_product_id', 'kitchen_component_id', 
	'product_id', 'kitchen_confirm_date', 'create_uid', 'kitchen_bom_id',
]

PIVOT_SELECTED_MEASURES = [
	'product_uom_qty',
	'kitchen_svl_value'
]


class StockMove(models.Model):
	_inherit = 'stock.move'

	@api.depends('kitchen_component_id', 'kitchen_finished_id', 'stock_valuation_layer_ids', 'stock_valuation_layer_ids.value')
	def _compute_kitchen_svl_value(self):
		for move in self:
			move.kitchen_svl_value = 0.0
			if not move.kitchen_component_id and not move.kitchen_finished_id:
				continue
			move.kitchen_svl_value = sum(move.stock_valuation_layer_ids.mapped('value'))

	kitchen_record_type = fields.Selection(related='kitchen_component_id.record_type', copy=False, store=True)
	kitchen_confirm_date = fields.Datetime(related='kitchen_component_id.confirm_date', store=True, copy=False)
	kitchen_product_id = fields.Many2one('product.product', related='kitchen_component_id.product_id', store=True, copy=False)
	kitchen_bom_id = fields.Many2one('mrp.bom', related='kitchen_component_id.bom_id', store=True)
	kitchen_svl_value = fields.Float(compute=_compute_kitchen_svl_value, string='Cost', store=True)

	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		res = super(StockMove, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)

		if view_type != 'pivot':
			return res

		view_pivot_id = self.env.ref('equip3_kitchen_reports.view_material_consumed_pivot')[0].id
		if view_id != view_pivot_id:
			return res

		doc = etree.XML(res['arch'])
		pivot = doc.xpath('//pivot')
		
		all_fields = self.fields_get()
	
		label_to_change = {
			'create_uid': 'User', 
			'kitchen_confirm_date': 'Date', 
			'kitchen_component_id': 'Reference',
			'kitchen_product_id': 'Product',
			'product_id': 'Materials Consumed',
			'product_uom_qty': 'Consumed Quantity'
		}

		measure_selected = PIVOT_SELECTED_MEASURES[:]

		record_type = self.env.context.get('default_record_type')
		if record_type == 'disassemble':
			label_to_change['product_uom_qty'] = 'Produced Quantity'
			label_to_change['product_id'], label_to_change['kitchen_product_id'] = label_to_change['kitchen_product_id'], label_to_change['product_id']

		for i, (field_name, field) in enumerate(all_fields.items()):
			if field['type'] in ['one2many', 'many2many']:
				continue
			field_element = etree.SubElement(pivot[0], 'field')
			field_element.set('name', field_name)

			if field_name in label_to_change:
				attrs = 'string' if field_name not in measure_selected else 'add_string'
				field_element.set(attrs, label_to_change[field_name])

			if field_name in measure_selected:
				field_element.set('type', 'measure')
			else:
				if field_name not in PIVOT_SELECTED_FIELDS:
					field_element.set('invisible', '1')

		res['arch'] = etree.tostring(doc, encoding='unicode')
		return res
