================
Inventory Freeze
================

Installation
============
`Install <https://blog.miftahussalam.com/install-apps-odoo/>`__ this module in a usual way

Configuration
=============
No configuration needed

Usage
=====
See freeze status on location form

Uninstallation
==============
Uninstall this module in a usual way