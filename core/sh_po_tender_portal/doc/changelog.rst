Changelog
=========
14.0.1
-------------------------
Initial Release


Version 14.0.2 (Date : 4th Feb 2021)
---------------------------------------
[UPDATE] update input type from number to text for giving decimal value when update the bid.

Version 14.0.3 (Date : 9th Feb 2021)
---------------------------------------
[FIX] fix error of can not convert string to float when update price from portal. 

Version 14.0.4 (Date : 3rd June 2021)
-----------------------------------------
[FIX] fix bug of button click from portal tender list view.

Version 14.0.5 (Date : 23rd June 2021)
-----------------------------------------
[ADD] add configuration for manage portal tender documents. 
[ADD] display tender documents in portal if configuration enable and publish in portal is enable from attachment/document view.

Version 14.0.6 (Date : 15th July 2021)
-----------------------------------------
[ADD] inherit sh_rfq_portal controller and add one condition for tender portal and update values to rfq. 
