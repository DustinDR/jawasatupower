odoo.define('equip3_manuf_operations_contd.ConsumptionListRenderer', function (require) {
"use strict";

var ListRenderer = require('web.ListRenderer');

ListRenderer.include({
    _renderRow: function (record, index) {
        var $row = this._super.apply(this, arguments);
        if (this.addTrashIcon && 'delete-condition' in this.arch.attrs){
            var conditionMeet = eval('record.data.' + this.arch.attrs['delete-condition']);
            if (!conditionMeet){
                $row.children('td.o_list_record_remove').replaceWith('<td/>');
            }
        }
        return $row;
    },
});
});