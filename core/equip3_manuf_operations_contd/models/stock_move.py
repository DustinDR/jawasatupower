from odoo import fields, models, api, _
from odoo.exceptions import UserError


class StockMove(models.Model):
	_inherit = 'stock.move'

	@api.depends('product_id', 'location_id')
	def _compute_product_quant(self):
		stock_quant = self.env['stock.quant']
		for move in self:
			quants = stock_quant._gather(move.product_id, move.location_id)
			quantity = sum(quants.mapped('quantity'))
			reserved = sum(quants.mapped('reserved_quantity'))
			move.product_quant_reserved_qty = reserved
			move.product_quant_available_qty = quantity - reserved

	mrp_consumption_id = fields.Many2one('mrp.consumption', string='MPR Component')
	mrp_consumption_finished_id = fields.Many2one('mrp.consumption', string='MPR Finished')
	mrp_consumption_byproduct_id = fields.Many2one('mrp.consumption', string='MPR ByProduct')
	product_quant_reserved_qty = fields.Float(compute=_compute_product_quant, digits='Product Unit of Measure')
	product_quant_available_qty = fields.Float(compute=_compute_product_quant, digits='Product Unit of Measure')
	allocated_cost = fields.Float(string='Allocated Cost (%)', digits='Product Unit of Measure')
	is_editable_product = fields.Boolean(default=True)
	mpr_quantity_done = fields.Float(digits='Product Unit of Measure')
	sequence = fields.Integer('Sequence', default=10)

	def _prepare_move_split_vals(self, qty):
		defaults = super()._prepare_move_split_vals(qty)
		defaults['workorder_id'] = self.workorder_id.id
		return defaults

	@api.onchange('mpr_quantity_done')
	def onchange_mpr_quantity_done(self):
		self.quantity_done = self.mpr_quantity_done
		if self.is_editable_product:
			self.product_uom_qty = self.mpr_quantity_done

	def _get_src_account(self, accounts_data):
		return self.location_id.valuation_out_account_id.id or accounts_data['stock_input'].id

	def _get_dest_account(self, accounts_data):
		return self.location_dest_id.valuation_in_account_id.id or accounts_data['stock_output'].id

	def _get_accounting_data_for_valuation(self):
		""" Return the accounts and journal to use to post Journal Entries for
		the real-time valuation of the quant. """
		self.ensure_one()
		self = self.with_company(self.company_id)
		accounts_data = self.product_id.product_tmpl_id.get_product_accounts()

		acc_src = self._get_src_account(accounts_data)
		acc_dest = self._get_dest_account(accounts_data)

		acc_valuation = accounts_data.get('stock_valuation', False)
		if acc_valuation:
			acc_valuation = acc_valuation.id
		if not accounts_data.get('stock_journal', False):
			raise UserError(_('You don\'t have any stock journal defined on your product category, check if you have installed a chart of accounts.'))
		if not acc_src:
			raise UserError(_('Cannot find a stock input account for the product %s. You must define one on the product category, or on the location, before processing this operation.') % (self.product_id.display_name))
		if not acc_dest:
			raise UserError(_('Cannot find a stock output account for the product %s. You must define one on the product category, or on the location, before processing this operation.') % (self.product_id.display_name))
		if not acc_valuation:
			raise UserError(_('You don\'t have any stock valuation account defined on your product category. You must define one before processing this operation.'))
		journal_id = accounts_data['stock_journal'].id
		return journal_id, acc_src, acc_dest, acc_valuation

	def action_show_details(self):
		res = super(StockMove, self).action_show_details()
		if self.env.context.get('back_consumption_id'):
			res['context'].update({
				'back_consumption_id': self.env.context.get('back_consumption_id')
			})
		return res

	def action_save(self):
		res = super(StockMove, self).action_save()
		if self.env.context.get('back_consumption_id'):
			return {
				'name': _('Production Record'),
				'type': 'ir.actions.act_window',
				'view_mode': 'form',
				'res_model': 'mrp.consumption',
				'target': 'new',
				'res_id': self.env.context.get('back_consumption_id'),
			}
		return res

	def _is_all_production_moves(self):
		for move in self:
			if not move.raw_material_production_id and not move.production_id:
				return False
		return True

	def _get_production_in_svl_vals(self, forced_quantity=None):
		self.ensure_one()
		self = self.with_company(self.company_id)
		valued_move_lines = self._get_in_move_lines()
		valued_quantity = 0
		for valued_move_line in valued_move_lines:
			valued_quantity += valued_move_line.product_uom_id._compute_quantity(valued_move_line.qty_done, self.product_id.uom_id)
		unit_cost = abs(self._get_price_unit())  # May be negative (i.e. decrease an out move).
		if self.product_id.cost_method == 'standard':
			unit_cost = self.product_id.standard_price

		svl_vals = self.product_id._prepare_in_svl_vals(forced_quantity or valued_quantity, unit_cost)
		svl_vals.update(self._prepare_common_svl_vals())
		if forced_quantity:
			svl_vals['description'] = 'Correction of %s (modification of past move)' % self.picking_id.name or self.name
		return svl_vals

	def _get_production_out_svl_vals(self, forced_quantity=None):
		self.ensure_one()
		self = self.with_company(self.company_id)
		valued_move_lines = self._get_out_move_lines()
		valued_quantity = 0
		for valued_move_line in valued_move_lines:
			valued_quantity += valued_move_line.product_uom_id._compute_quantity(valued_move_line.qty_done, self.product_id.uom_id)
		svl_vals = self.product_id._prepare_out_svl_vals(forced_quantity or valued_quantity, self.company_id)
		svl_vals.update(self._prepare_common_svl_vals())
		if forced_quantity:
			svl_vals['description'] = 'Correction of %s (modification of past move)' % self.picking_id.name or self.name
		svl_vals['description'] += svl_vals.pop('rounding_adjustment', '')

		return svl_vals

	def _update_production_finished_cost(self, forced_quantity, quantity):
		self.ensure_one()

		if self.mrp_consumption_finished_id:
			consumption_id = self.mrp_consumption_finished_id
			value = consumption_id.get_finished_cost()
			for consumption in self.production_id.consumption_ids.filtered(
					lambda c: c.state == 'confirm' and not c.valued
			):
				value += consumption.get_finished_cost()
		else:
			consumption_id = self.mrp_consumption_byproduct_id
			value = consumption_id.get_byproduct_cost(move_id=self.id)

		unit_cost = value / (forced_quantity or quantity)

		return {'unit_cost': unit_cost, 'value': value}

	def _create_in_svl(self, forced_quantity=None):
		if not self._is_all_production_moves():
			return super(StockMove, self)._create_in_svl(forced_quantity=forced_quantity)

		svl_vals_list = []
		for move in self:
			svl_vals = move._get_production_in_svl_vals(forced_quantity=forced_quantity)
			if move.production_id:
				svl_vals.update(move._update_production_finished_cost(forced_quantity, svl_vals['quantity']))
			
			svl_vals_list.append(svl_vals)

		return self.env['stock.valuation.layer'].sudo().create(svl_vals_list)

	def _create_out_svl(self, forced_quantity=None):
		if not self._is_all_production_moves():
			return super(StockMove, self)._create_out_svl(forced_quantity=forced_quantity)

		svl_vals_list = []
		for move in self:
			svl_vals = move._get_production_out_svl_vals(forced_quantity=forced_quantity)
			if move.production_id:
				svl_vals.update(move._update_production_finished_cost(forced_quantity, svl_vals['quantity']))
			
			svl_vals_list.append(svl_vals)

		return self.env['stock.valuation.layer'].sudo().create(svl_vals_list)

	def _account_entry_move(self, qty, description, svl_id, cost):
		if self.raw_material_production_id or self.production_id:
			# create moves from mrp.consumption model instead
			return False
		return super(StockMove, self)._account_entry_move(qty, description, svl_id, cost)

	@api.depends('product_uom_qty',
				 'raw_material_production_id', 'raw_material_production_id.product_qty',
				 'raw_material_production_id.qty_produced',
				 'production_id', 'production_id.product_qty', 'production_id.qty_produced')
	def _compute_unit_factor(self):
		super(StockMove, self)._compute_unit_factor()
		for move in self:
			move.unit_factor = move.product_uom_qty

	@api.depends('raw_material_production_id.qty_producing', 'product_uom_qty', 'product_uom')
	def _compute_should_consume_qty(self):
		super(StockMove, self)._compute_should_consume_qty()
		for move in self:
			if move.raw_material_production_id:
				move.should_consume_qty = move.product_uom_qty
