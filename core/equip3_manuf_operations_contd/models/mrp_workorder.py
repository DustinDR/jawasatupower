from odoo import fields, models, api, _


class MrpWorkorder(models.Model):
	_inherit = 'mrp.workorder'

	@api.depends('consumption_ids', 'consumption_ids.state', 'production_state', 'state')
	def _compute_should_hide_duration(self):
		for workorder in self:
			should_hide_duration = False
			if workorder.production_state in ('draft', 'approval', 'approved', 'reject'):
				should_hide_duration = True
			else:
				for consumption in workorder.consumption_ids:
					if consumption.state == 'approval':
						should_hide_duration = True
						break
			workorder.should_hide_duration = should_hide_duration

	consumption_ids = fields.One2many('mrp.consumption', 'workorder_id', 'Production Records', readonly=True)
	stock_valuation_layer_ids = fields.One2many('stock.valuation.layer', 'mrp_workorder_id', string='Valuations', readonly=True)
	should_hide_duration = fields.Boolean(compute=_compute_should_hide_duration)

	def name_get(self):
		res = []
		for wo in self:
			res.append((wo.id, "%s - %s" % (wo.workorder_id, wo.name)))
		return res

	def button_start(self):
		res = super(MrpWorkorder, self).button_start()
		self.qty_producing = self.qty_remaining
		return res

	def should_process_consumption(self):
		self.ensure_one()
		consumption_ids = self.consumption_ids.filtered(lambda c: c.state == 'confirm')
		finished_qty = sum(consumption_ids.mapped('finished_product'))
		rejected_qty = sum(consumption_ids.mapped('rejected_product'))
		return finished_qty + rejected_qty < self.qty_production

	def button_finish(self):

		return_action = self.env.context.get('return_action')
		bypass_consumption = self.env.context.get('bypass_consumption')
		bypass_approval = self.env.context.get('bypass_approval')
		is_approval_matrix_on = self.env.company.production_record_conf

		for workorder in self:
			if workorder.should_process_consumption() and not bypass_consumption:
				consumption_id = workorder.get_consumption_id()
				if return_action:
					return workorder.action_open_consumption(res_id=consumption_id.id)

				if is_approval_matrix_on:
					consumption_id.action_approval()

				if not is_approval_matrix_on or bypass_approval:
					action = consumption_id.button_confirm()
					if action:
						return action
			else:
				super(MrpWorkorder, workorder).button_finish()

				# create journal entry when move for finished good created
				consumption_ids = workorder.consumption_ids
				if consumption_ids.filtered(lambda c: c.move_finished_ids):
					workorder.production_id.update_valuations()

		return True

	def action_open_consumption(self, res_id=False):
		self = self.with_context(return_action=False, pop_back=True)
		action = {
			'name': _('Production Record'),
			'view_mode': 'form',
			'res_model': 'mrp.consumption',
			'view_id': self.env.ref('equip3_manuf_operations_contd.mrp_consumption_form').id,
			'res_id': res_id,
			'type': 'ir.actions.act_window',
			'context': self.env.context,
			'target': 'new'
		}
		return action

	def _prepare_consumption_vals(self):
		self.ensure_one()
		finished_product = self.qty_remaining
		rejected_product = 0.0

		all_workorder_ids = sorted(self.production_id.workorder_ids.ids)
		move_finished_product = all_workorder_ids[-1] == self.id

		end_date = fields.Datetime.now()
		move_raw_ids = self.move_raw_ids.filtered(lambda m: m.state not in ('done', 'cancel'))
		byproduct_ids = self.byproduct_ids.filtered(lambda m: m.state not in ('done', 'cancel'))
		move_finished_ids = self.env['stock.move']
		if move_finished_product:
			move_finished_ids = self.production_id.move_finished_ids.filtered(
				lambda m: m.state not in ('done', 'cancel') and m.product_id == self.production_id.product_id
			)

		(move_raw_ids | byproduct_ids).write({'is_editable_product': False})

		# get default approval matrix
		approval_matrix = self.env['mrp.approval.matrix'].sudo().search([
			('company', '=', self.env.company.id),
			('branch', '=', int(self.env.user.branch_id.id)),
			('matrix_type', '=', 'pr')
		], limit=1)
		vals = {
			'manufacturing_plan': self.mrp_plan_id.id,
			'create_date': fields.Datetime.now(),
			'create_uid': self.env.uid,
			'manufacturing_order_id': self.production_id.id,
			'workorder_id': self.id,
			'product_id': self.product_id.id,
			'finished_product': finished_product,
			'rejected_product': rejected_product,
			'date_finished': end_date,
			'move_finished_product': move_finished_product,
			'move_raw_ids': [(6, 0, move_raw_ids.ids)],
			'move_finished_ids': [(6, 0, move_finished_ids.ids)],
			'byproduct_ids': [(6, 0, byproduct_ids.ids)],
			'approval_matrix': approval_matrix.id or False,
			'product_uom_id': self.production_id.product_uom_id.id
		}
		return vals

	def create_consumption(self):
		vals = self._prepare_consumption_vals()
		consumption_id = self.env['mrp.consumption'].create(vals)
		consumption_id.oc_finished_product_rejected_product()
		return consumption_id

	def get_consumption_id(self):
		self.ensure_one()
		consumption_id = self.env['mrp.consumption'].search([
			('workorder_id', '=', self.id),
			('state', '!=', 'confirm')
		], limit=1)

		if not consumption_id:
			consumption_id = self.create_consumption()
		return consumption_id
