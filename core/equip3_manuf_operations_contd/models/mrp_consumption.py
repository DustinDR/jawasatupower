# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import datetime, timedelta
import pytz


class Equip3MrpConsumption(models.Model):
    _name = 'mrp.consumption'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'MRP Consumption'

    @api.depends('product_id', 'product_tracking', 'finished_product', 'finished_product_sn',
                 'rejected_product', 'rejected_product_sn')
    def _compute_is_disable_generate(self):
        for record in self:
            generated = False
            finished_qty = record.finished_product
            rejected_qty = record.rejected_product
            finished_lots = len(record.finished_product_sn)
            rejected_lots = len(record.rejected_product_sn)
            if record.product_tracking == 'serial':
                generated = finished_lots >= finished_qty and rejected_lots >= rejected_qty
            elif record.product_tracking == 'lot':
                finished_generated = (finished_qty > 0.0 and finished_lots) or finished_qty <= 0.0
                rejected_generated = (rejected_qty > 0.0 and rejected_lots) or rejected_qty <= 0.0
                generated = finished_generated and rejected_generated
            record.is_disable_generate = generated

    @api.depends('move_raw_ids', 'byproduct_ids')
    def _compute_move_done(self):
        for record in self:
            record.move_raw_done_ids = record.move_raw_ids.filtered(lambda m: m.state == 'done')
            record.byproduct_done_ids = record.byproduct_ids

    name = fields.Char(string='Reference', required=True, copy=False, readonly=True, default=_('New'), tracking=True)
    manufacturing_plan = fields.Many2one('mrp.plan', 'Manufacturing Plan', tracking=True)
    manufacturing_order_id = fields.Many2one('mrp.production', string='Manufacturing Order', tracking=True)
    workorder_id = fields.Many2one('mrp.workorder', string='Work Order', tracking=True)
    product_id = fields.Many2one('product.product', string='Product', readonly=True, tracking=True)
    product_uom_id = fields.Many2one('uom.uom', readonly=True)

    finished_product = fields.Float(
        string='Finished Product', digits='Product Unit of Measure', readonly=True,
        states={'draft': [('readonly', False)]}, tracking=True)
    rejected_product = fields.Float(
        string='Rejected Product', digits='Product Unit of Measure', readonly=True,
        states={'draft': [('readonly', False)]}, tracking=True)
    finished_product_sn = fields.One2many(
        'stock.production.lot', 'consumption_finished_product_id', string='Finished Lot/Serial Number', tracking=True)
    rejected_product_sn = fields.One2many(
        'stock.production.lot', 'consumption_rejected_product_id', string='Rejected Lot/Serial Number', tracking=True)

    branch_id = fields.Many2one(
        'res.branch', string="Branch", store=True, readonly=True,
        default=lambda self: self.env.user.branch_id, tracking=True)
    company_id = fields.Many2one(
        'res.company', string='Company', store=True, readonly=True,
        default=lambda self: self.env.company, tracking=True)

    move_raw_ids = fields.One2many(
        'stock.move', 'mrp_consumption_id', 'Components', readonly=True,
        states={'draft': [('readonly', False)]})
    byproduct_ids = fields.One2many(
        'stock.move', 'mrp_consumption_byproduct_id', 'By-Products', readonly=True,
        states={'draft': [('readonly', False)]})
    move_finished_ids = fields.One2many(
        'stock.move', 'mrp_consumption_finished_id', 'Finished', readonly=True)

    state = fields.Selection([
        ('draft', 'Draft'),
        ('approval', 'To be Approved '),
        ('approved', 'Approved'),
        ('reject', 'Rejected '),
        ('confirm', 'Confirmed')
    ], string='Status', default='draft', tracking=True)

    state_1 = fields.Selection(related='state', tracking=False)
    state_2 = fields.Selection(related='state', tracking=False)
    state_3 = fields.Selection(related='state', tracking=False)
    state_4 = fields.Selection(related='state', tracking=False)

    date_finished = fields.Datetime('Date Finished', readonly=True)
    move_finished_product = fields.Boolean()
    product_tracking = fields.Selection(related='product_id.tracking')
    stock_valuation_layer_ids = fields.One2many('stock.valuation.layer', 'mrp_consumption_id', readonly=True)
    is_disable_generate = fields.Boolean(compute=_compute_is_disable_generate)
    consumption = fields.Selection(related='manufacturing_order_id.bom_id.consumption')

    picking_type_id = fields.Many2one('stock.picking.type', related='manufacturing_order_id.picking_type_id')
    location_src_id = fields.Many2one('stock.location', related='workorder_id.location_id')
    location_dest_id = fields.Many2one('stock.location', related='manufacturing_order_id.location_dest_id')
    production_location_id = fields.Many2one('stock.location', related='manufacturing_order_id.production_location_id')
    procurement_group_id = fields.Many2one('procurement.group', related='manufacturing_order_id.procurement_group_id')

    expiry_days = fields.Integer('Expiry Days', related='product_id.expiration_time')
    product_use_expiration_date = fields.Boolean(related='product_id.use_expiration_date')

    # technical fields for show component and byproduct only when state == 'done'
    move_raw_done_ids = fields.One2many('stock.move', compute=_compute_move_done)
    byproduct_done_ids = fields.One2many('stock.move', compute=_compute_move_done)
    valued = fields.Boolean()

    request_approval_time = fields.Datetime(string='Request Approval Time')


    def _generate_order_by(self, order_spec, query):
        my_order = "CASE WHEN state='reject'  THEN 100   WHEN state = 'confirm'  THEN 10 ELSE 1 END, name asc"            
        if order_spec:
            return super(Equip3MrpConsumption, self)._generate_order_by(order_spec, query) + ", " + my_order
        return my_order

    @api.model
    def create(self, vals):
        self._check_quantity(vals)
        if vals.get('name', _('New')) == _('New'):
            seq_date = None
            vals['name'] = self.env['ir.sequence'].next_by_code(
                'mrp.consumption', sequence_date=seq_date
            ) or _('New')
        return super(Equip3MrpConsumption, self).create(vals)

    def write(self, vals):
        finished_qty = vals.get('finished_product', self.finished_product)
        rejected_qty = vals.get('rejected_product', self.rejected_product)
        production_id = vals.get('manufacturing_order_id', self.manufacturing_order_id.id)
        workorder_id = vals.get('workorder_id', self.workorder_id.id)

        new_vals = {
            'finished_product': finished_qty,
            'rejected_product': rejected_qty,
            'manufacturing_order_id': production_id,
            'workorder_id': workorder_id
        }

        self._check_quantity(new_vals)
        return super(Equip3MrpConsumption, self).write(vals)

    @api.onchange('finished_product', 'rejected_product')
    def oc_finished_product_rejected_product(self):
        finished_qty = self.finished_product
        rejected_qty = self.rejected_product
        product_qty = finished_qty + rejected_qty
        origin = self._origin

        err_message = self._check_quantity(raise_error=False)
        if err_message:
            if finished_qty != origin.finished_product:
                self.finished_product = origin.finished_product
            elif rejected_qty != origin.rejected_product:
                self.rejected_product = origin.rejected_product
            return {'warning': {'title': _('Quantity Error'), 'message': err_message}}

        original_move_raws = self.move_raw_ids.filtered(lambda m: m.bom_line_id)
        original_byproducts = self.byproduct_ids.filtered(lambda m: m.byproduct_id)
        bypass_onchange = self.env.context.get('bypass_onchange')
        for move in original_move_raws | original_byproducts:
            if move.state in ('done', 'cancel') or (move.byproduct_id and bypass_onchange):
                continue
            line = move.bom_line_id or move.byproduct_id
            mpr_quantity_done = 0.0
            if line.bom_id.product_qty > 0.0:
                mpr_quantity_done = (line.product_qty / line.bom_id.product_qty) * product_qty
            move.mpr_quantity_done = mpr_quantity_done
            move.onchange_mpr_quantity_done()

        if finished_qty != int(finished_qty) or rejected_qty != int(rejected_qty):
            return

        product_id = self.product_id
        tracking = self.product_tracking

        serial_autogenerate = product_id.is_sn_autogenerate and tracking == 'serial'
        lot_autogenerate = product_id.is_in_autogenerate and tracking == 'lot'
        any_autogenerate = serial_autogenerate or lot_autogenerate

        if not any_autogenerate:
            return

        finished_lots = self.finished_product_sn
        rejected_lots = self.rejected_product_sn
        created_lots = finished_lots | rejected_lots

        if tracking == 'serial':
            digits = product_id.digits
            to_update = 'current_sequence'
            qty_needed = int(product_qty)
        else:
            digits = product_id.in_digits
            to_update = 'in_current_sequence'
            qty_needed = (finished_qty > 0.0 or rejected_qty > 0.0) and 1 or 0

        current_seq = product_id[to_update]
        qty_to_remove = len(created_lots) - qty_needed
        if qty_to_remove <= 0:
            return

        sequence = str(int(current_seq) - qty_to_remove).zfill(digits)
        picked_lots = created_lots[:qty_needed]
        self.finished_product_sn = [(6, 0, picked_lots[:int(finished_qty)].ids)]
        self.finished_product_sn = [(6, 0, picked_lots[int(finished_qty):].ids)]

        product_id.update({to_update: sequence})

    def _check_quantity(self, vals=None, raise_error=True):
        if self:
            self.ensure_one()
        return False
        # if vals:
        #     product_qty = vals.get('finished_product', 0.0) + vals.get('rejected_product', 0.0)
        #     production_id = vals.get('manufacturing_order_id', False)
        #     workorder_id = vals.get('workorder_id', False)
        # else:
        #     product_qty = self.finished_product + self.rejected_product
        #     production_id = self.manufacturing_order_id.id
        #     workorder_id = self.workorder_id.id
        #
        # production_product_qty = self.env['mrp.production'].browse(production_id).product_qty
        #
        # consumption_ids = self.search([
        #     ('manufacturing_order_id', '=', production_id),
        #     ('workorder_id', '=', workorder_id),
        #     ('state', '=', 'confirm'),
        # ])
        #
        # total_finished = sum(consumption_ids.mapped('finished_product'))
        # total_rejected = sum(consumption_ids.mapped('rejected_product'))
        # qty_produced = total_finished + total_rejected
        #
        # message = False
        # if product_qty + qty_produced > production_product_qty:
        #     if qty_produced:
        #         message = _("The manufacturing order already produce %s quantity for this product, \
        #         you can't create more than %s!" % (qty_produced, production_product_qty))
        #     else:
        #         message = _("The maximum produced quantity for this manufacturing order is %s, \
        #         you can't create more than that!" % production_product_qty)
        #
        # if not raise_error:
        #     return message
        #
        # if message:
        #     raise UserError(message)
        #
        # return False

    def _update_stock_valuation_layers(self):
        self.ensure_one()

        production_id = self.manufacturing_order_id
        svl_components = self.move_raw_ids.stock_valuation_layer_ids
        svl_finished = self.move_finished_ids.stock_valuation_layer_ids
        svl_byproducts = self.byproduct_ids.stock_valuation_layer_ids

        svl_components.type = 'component'
        svl_byproducts.type = 'byproduct'
        svl_finished.type = 'finished'

        svl_ids = svl_components | svl_byproducts | svl_finished
        svl_ids.update({
            'mrp_plan_id': production_id.mrp_plan_id.id,
            'mrp_production_id': production_id.id,
            'mrp_consumption_id': self.id,
            'mrp_workorder_id': self.workorder_id.id,
        })

        self.write({'stock_valuation_layer_ids': [(6, 0, svl_ids.ids)]})

    def finish_next_workorder(self):
        self.ensure_one()
        workorder_ids = self.manufacturing_order_id.workorder_ids.filtered(
            lambda w: w.state not in ('done', 'cancel')
        )
        if workorder_ids:
            workorder_id = workorder_ids[0]
            workorder_id.button_finish()

    def button_confirm(self):
        company_id = self.company_id or self.env.company
        err_message, action = self._pre_button_confirm(
            check_default=not company_id.production_record_conf,
            do_check_consumption=True
        )
        if err_message:
            raise UserError(err_message)
        if action:
            return action

        self._action_confirm()
        self._update_stock_valuation_layers()
        #self._account_entry_move()
        self.state = 'confirm'

        qty_to_produce = self.finished_product + self.rejected_product
        self.workorder_id.qty_produced += qty_to_produce
        if self.workorder_id.qty_remaining <= 0:
            self.workorder_id.button_finish()
        else:
            # self.workorder_id.button_pending()
            self.workorder_id.end_previous()

        if self.manufacturing_order_id.qty_produced == self.manufacturing_order_id.product_qty:
            self.finish_next_workorder()

        if self.request_approval_time:
            self.workorder_id.time_ids.filtered(lambda t: t.date_end is False).write({
                'date_end': self.request_approval_time
            })

        if self.env.context.get('pop_back'):
            return self.workorder_id.action_open_consumption(res_id=self.id)

    def _handle_tracking(self, moves, finished=False):
        qty_to_produce = self.finished_product + self.rejected_product
        lot_producing_ids = self.finished_product_sn | self.rejected_product_sn

        for lot_producing_id in lot_producing_ids:
            lot_producing_id.expiration_date = lot_producing_id.mrp_consumption_expiration_date

        for move in moves:

            quantity_done = move.quantity_done
            move.quantity_done = 0.0

            if finished:
                qty_produced = qty_to_produce
                line_arange = int(qty_to_produce)
            else:
                qty_produced = quantity_done
                line_arange = int(quantity_done)

            tracking = move.product_id.tracking
            arange = tracking == 'serial' and line_arange or 1
            qty_done = tracking == 'serial' and 1.0 or qty_produced

            values = []
            for i in range(arange):
                vals = move._prepare_move_line_vals(quantity=0)
                vals['qty_done'] = qty_done

                if tracking in ('lot', 'serial'):
                    if tracking == 'serial':
                        vals['product_uom_id'] = move.product_id.uom_id.id

                    if finished:
                        vals['lot_id'] = lot_producing_ids[i].id

                values.append((0, 0, vals))
            move.move_line_ids = values

    def _check_accounting_data(self):
        self.ensure_one()

        err_message = False
        finished_good = self.manufacturing_order_id.product_id
        if not finished_good.categ_id.property_stock_journal:
            err_message = _('Please set Stock Journal for %s first!' % finished_good.name)

        if not finished_good.categ_id.mrp_wip_account_id:
            err_message = _("Please set Manufacturing WiP Account for %s first!" % finished_good.name)

        if not finished_good.categ_id.property_stock_valuation_account_id:
            err_message = _("Please set Stock Valuation Account for %s first!" % finished_good.name)

        for move in self.move_raw_ids | self.byproduct_ids:
            move_product = move.product_id
            if not move_product.categ_id.property_stock_valuation_account_id.id:
                err_message = _("Please set Stock Valuation Account for %s first!" % move_product.name)
                break

        return err_message

    def _pre_button_confirm(self, check_default=True, do_check_consumption=False):
        self.ensure_one()

        if check_default:
            # check tracking
            if self.move_finished_product:
                if not self.env.context.get('check_serial'):
                    self.action_generate_serial()

                need_to_fill = False
                if self.product_tracking == 'serial':
                    if self.finished_product != len(self.finished_product_sn):
                        need_to_fill = 'finished'
                    if self.rejected_product != len(self.rejected_product_sn):
                        need_to_fill = 'rejected' if not need_to_fill else ' and rejected'

                elif self.product_tracking == 'lot':
                    if self.finished_product and not self.finished_product_sn:
                        need_to_fill = 'finished'
                    if self.rejected_product and not self.rejected_product_sn:
                        need_to_fill = 'rejected' if not need_to_fill else ' and rejected'

                if need_to_fill:
                    err_message = _('You need generate lot/serial number for %s product first!' % need_to_fill)
                    return err_message, False

            # check availability
            for move in self.move_raw_ids:
                uom_qty = move.product_uom._compute_quantity(move.quantity_done, move.product_id.uom_id)
                if uom_qty > move.product_quant_reserved_qty + move.product_quant_available_qty:
                    err_message = _('There is not enough stock for product %s on location %s' % (
                        move.product_id.name, move.location_id.complete_name))
                    return err_message, False

            # check accounting data
            err_message = self._check_accounting_data()
            if err_message:
                return err_message, False

        if do_check_consumption:
            # check consumption
            confirmed = self.env.context.get('consumption_confirmed')
            check_consumption = self.env.context.get('check_consumption')
            if check_consumption and self.consumption == 'warning' and not confirmed:
                if self._get_not_expected_moves():
                    action = {
                        'name': 'Consumption Warning',
                        'type': 'ir.actions.act_window',
                        'res_model': 'mrp.flexible.consumption.warning',
                        'view_mode': 'form',
                        'target': 'new',
                        'context': {'default_consumption_id': self.id}
                    }
                    return False, action

        return False, False

    def _get_not_expected_moves(self):
        self.ensure_one()
        product_qty = self.finished_product + self.rejected_product
        not_expected_moves = self.env['stock.move']
        for move in self.move_raw_ids | self.byproduct_ids:
            if move.product_uom_qty <= 0.0:
                continue

            line = move.bom_line_id or move.byproduct_id
            if not line:
                not_expected_moves |= move
                continue

            expected_qty_done = 0.0
            if line.bom_id.product_qty > 0.0:
                expected_qty_done = (line.product_qty / line.bom_id.product_qty) * product_qty
            if expected_qty_done != move.quantity_done:
                not_expected_moves |= move
                continue

            if move.byproduct_id:
                if move.byproduct_id.allocated_cost != move.allocated_cost:
                    not_expected_moves |= move

        return not_expected_moves

    def get_material_cost(self):
        self.ensure_one()
        material_cost = 0.0
        for move in self.move_raw_ids.filtered(lambda m: m.state == 'done'):
            material_cost += sum(move.stock_valuation_layer_ids.mapped('value'))
        return material_cost

    def get_byproduct_cost(self, material_cost=None, move_id=None):
        self.ensure_one()
        if material_cost is None:
            material_cost = self.get_material_cost()
        byproduct_cost = 0.0
        for move in self.byproduct_ids.filtered(lambda m: m.state == 'done'):
            if move_id is not None and move.id != move_id:
                continue
            byproduct_cost += (move.allocated_cost * material_cost) / 100
        return -byproduct_cost

    def get_finished_cost(self):
        self.ensure_one()
        self.valued = True
        material_cost = self.get_material_cost()
        return -(material_cost + self.get_byproduct_cost(material_cost=material_cost))

    def _prepare_move_line_vals(self, product_id, quantity, account_id):
        return {
            'name': product_id.name,
            'ref': product_id.name,
            'product_id': product_id.id,
            'product_uom_id': product_id.uom_id.id,
            'quantity': quantity,
            'account_id': account_id.id,
            'debit': 0.0,
            'credit': 0.0
        }

    def _create_component_move_lines(self, svl_product_id, svl_quantity, fg_product_id, fg_quantity, value):
        self.ensure_one()
        credit_account_id = svl_product_id.categ_id.property_stock_valuation_account_id
        credit_line = self._prepare_move_line_vals(svl_product_id, svl_quantity, credit_account_id)
        credit_line['credit'] = value

        debit_account_id = fg_product_id.categ_id.mrp_wip_account_id
        debit_line = self._prepare_move_line_vals(fg_product_id, fg_quantity, debit_account_id)
        debit_line['debit'] = value
        return [(0, 0, debit_line), (0, 0, credit_line)]

    def _create_byproduct_move_lines(self, svl_product_id, svl_quantity, fg_product_id, fg_quantity, value):
        return self._create_finished_move_lines(svl_product_id, svl_quantity, fg_product_id, fg_quantity, value)

    def _create_finished_move_lines(self, svl_product_id, svl_quantity, fg_product_id, fg_quantity, value):
        self.ensure_one()
        debit_account_id = svl_product_id.categ_id.property_stock_valuation_account_id
        debit_line = self._prepare_move_line_vals(svl_product_id, svl_quantity, debit_account_id)
        debit_line['debit'] = value

        credit_account_id = fg_product_id.categ_id.mrp_wip_account_id
        credit_line = self._prepare_move_line_vals(fg_product_id, fg_quantity, credit_account_id)
        credit_line['credit'] = value
        return [(0, 0, debit_line), (0, 0, credit_line)]

    def _account_entry_move(self):
        self.ensure_one()

        fg_product_id = self.product_id
        fg_product_qty = self.finished_product + self.rejected_product
        fg_journal_id = fg_product_id.categ_id.property_stock_journal

        svl_ids = self.stock_valuation_layer_ids.filtered(lambda s: s.type != 'none')
        data = dict([(svl.type, {'lines': [], 'svl_ids': []}) for svl in svl_ids])

        for svl in svl_ids:
            data[svl.type]['lines'] += getattr(self, '_create_%s_move_lines' % svl.type)(
                svl.product_id, svl.quantity, fg_product_id, fg_product_qty, abs(svl.value)
            )
            data[svl.type]['svl_ids'] += [svl.id]

        account_move = self.env['account.move']
        ref = '-'.join([self.manufacturing_order_id.name, self.workorder_id.workorder_id, self.name])
        for svl_type, values in data.items():
            account_move_values = {
                'consumption_id': self.id,
                'journal_id': fg_journal_id.id,
                'date': fields.Datetime.now(),
                'move_type': 'entry',
                'line_ids': values['lines'],
                'stock_valuation_layer_ids': [(6, False, values['svl_ids'])]
            }
            account_move_id = account_move.create(account_move_values)
            account_move_id._post()
            account_move_id.ref = ref

    def _update_manual_moves(self, moves):
        self.ensure_one()
        for i, move in enumerate(moves):
            values = {
                'sequence': 20 + i,
                'name': _('New'),
                'origin': self.manufacturing_order_id.name,
                'mrp_plan_id': self.manufacturing_plan.id,
                'workorder_id': self.workorder_id.id
            }
            if move.mrp_consumption_id:
                values.update({'raw_material_production_id': self.manufacturing_order_id.id})
                warehouse_id = self.location_src_id.get_warehouse()
            else:
                values.update({'production_id': self.manufacturing_order_id.id})
                warehouse_id = self.location_dest_id.get_warehouse()

            if warehouse_id:
                values.update({'warehouse_id': warehouse_id.id})

            move.update(values)

    def _action_confirm(self):
        self.ensure_one()

        move_raw_ids = self.move_raw_ids.filtered(lambda m: m.product_uom_qty > 0.0)
        byproduct_ids = self.byproduct_ids.filtered(lambda m: m.product_uom_qty > 0.0)

        # update missing field for move that created manually
        new_move_raw_ids = move_raw_ids.filtered(lambda m: not m.bom_line_id)
        self._update_manual_moves(new_move_raw_ids)

        new_byproduct_ids = byproduct_ids.filtered(lambda m: not m.byproduct_id)
        self._update_manual_moves(new_byproduct_ids)

        self._handle_tracking(move_raw_ids)
        move_raw_ids._action_done()
        for move in move_raw_ids:
            if not move.bom_line_id:
                move.product_uom_qty = 0.0

        self._handle_tracking(byproduct_ids)
        byproduct_ids._action_done()

        self._handle_tracking(self.move_finished_ids, finished=True)
        self.move_finished_ids._action_done()

    def action_generate_serial(self):
        self.ensure_one()
        if not self.move_finished_product:
            return

        product_tracking = self.product_tracking
        if product_tracking not in ('serial', 'lot'):
            return

        finished_qty = int(self.finished_product)
        rejected_qty = int(self.rejected_product)
        if finished_qty + rejected_qty <= 0.0:
            return

        finished_lots = len(self.finished_product_sn)
        rejected_lots = len(self.rejected_product_sn)

        finished_to_create = 0
        rejected_to_create = 0
        if product_tracking == 'serial':
            limit = finished_qty
            finished_to_create = finished_qty - finished_lots
            rejected_to_create = rejected_qty - rejected_lots
        else:
            limit = 1
            if finished_qty > 0.0 and not finished_lots:
                finished_to_create = 1
            if rejected_qty > 0.0 and not rejected_lots:
                rejected_to_create = 1

        expiration_date = False
        if self.product_use_expiration_date:
            now = fields.Datetime.now()
            expiration_date = now + timedelta(days=self.expiry_days)

        created_lots = self.finished_product_sn | self.rejected_product_sn
        for i in range(finished_to_create + rejected_to_create):
            created_lots |= self.product_id.create_next_lot_and_serial(expiration_date=expiration_date)

        self.finished_product_sn = [(6, 0, created_lots[:limit].ids)]
        self.rejected_product_sn = [(6, 0, created_lots[limit:].ids)]

        if self.env.context.get('pop_back'):
            return self.workorder_id.action_open_consumption(res_id=self.id)

    @api.depends('state')
    def _compute_hide_confirm_btn(self):
        use_matrix = self.env.company.production_record_conf
        for record in self:
            state = record.state
            is_hide = (not use_matrix and state == 'confirm') or (use_matrix and state != 'approved')
            record.hide_confirm_btn = is_hide

    @api.depends('approval_matrix')
    def _compute_matrix_line_ids(self):
        lines = []
        for record in self:
            record.mrp_plan_approval_matrix_lines_ids = False
            for app_matrix in record.approval_matrix.mrp_approval_matrix_ids:
                lines.append((0, 0, {
                    'mrp_con_approval_matrix_id': record.approval_matrix.id,
                    'sequence': app_matrix.sequence,
                    'approve': [(6, 0, app_matrix.approve.ids)],
                    'minimum_approver': app_matrix.minimum_approver,
                }))
            record.mrp_plan_approval_matrix_lines_ids = lines

    def _compute_hide_approval_matrix_btn(self):
        use_matrix = self.env.company.production_record_conf
        for record in self:
            record.hide_approval_matrix_btn = not use_matrix

    def _compute_is_user_approver(self):
        user = self.env.user
        for record in self:
            approver = False
            next_approved = 0
            matrix_line_ids = record.mrp_plan_approval_matrix_lines_ids
            for line in matrix_line_ids:
                if line.state == 'approved':
                    next_approved += 1
                if line.state == 'rejected':
                    next_approved += 1

            if len(matrix_line_ids) != next_approved:
                if user.id in matrix_line_ids[next_approved].approve.ids:
                    approver = True
                if user.id in matrix_line_ids.approve_by.ids:
                    approver = False
            record.user_is_approver = approver

    approval_matrix = fields.Many2one('mrp.approval.matrix', 'Approval Matrix', store=True)
    mrp_plan_approval_matrix_lines_ids = fields.One2many(
        'mrp.plan.approval.matrix.line', 'mrp_con_approval_matrix_id', string='Production Record',
        compute='_compute_matrix_line_ids', store=True
    )
    hide_confirm_btn = fields.Boolean(compute='_compute_hide_confirm_btn')
    hide_approval_matrix_btn = fields.Boolean(compute='_compute_hide_approval_matrix_btn')
    user_is_approver = fields.Boolean(compute='_compute_is_user_approver')

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        if self.branch_id:
            approval_matrix = self.env['mrp.approval.matrix'].sudo().search([
                ('company', '=', self.env.company.id), ('branch', '=', self.branch_id.id),
                ('matrix_type', '=', 'pr')
            ], limit=1)
            if approval_matrix:
                self.approval_matrix = approval_matrix.id

    def action_approval(self):
        for record in self:

            err_message, action = record._pre_button_confirm()
            if err_message:
                raise UserError(err_message)

            for line in record.mrp_plan_approval_matrix_lines_ids:
                line.requested_time = fields.Datetime.now()

            record.write({
                'request_approval_time': fields.Datetime.now(),
                'state': 'approval'
            })

        if self.env.context.get('pop_back'):
            return self.workorder_id.action_open_consumption(res_id=self.id)

    def action_approve(self):
        tz = pytz.timezone(self.env.user.tz) or 'UTC'
        current_time = datetime.now(tz).strftime('%m/%d/%Y %H:%M:%S')
        user = self.env.user
        user_name = user.name or 'User'

        for record in self:
            matrix_line_ids = record.mrp_plan_approval_matrix_lines_ids
            for line in matrix_line_ids:
                if user.id not in line.approve.ids:
                    continue

                matrix_status = ''
                if line.approval_matrix_status:
                    matrix_status = _('%s\r\n' % line.approval_matrix_status)

                approve_msg = _('%sApproved By: %s, %s' % (matrix_status, user_name, current_time))

                line.write({
                    'approve_by': [(4, user.id)],
                    'approval_matrix_status': approve_msg
                })

                if line.minimum_approver < 2 or line.approve_by == line.approve:
                    line.write({'state': 'approved'})

            if all([line.state == 'approved' for line in matrix_line_ids]):
                record.write({'state': 'approved'})

            self.check_approve_reject_state()

        if self.env.context.get('pop_back'):
            return self.workorder_id.action_open_consumption(res_id=self.id)

    def action_reject(self, reject_reason=''):
        tz = pytz.timezone(self.env.user.tz) or 'UTC'
        current_time = datetime.now(tz).strftime('%m/%d/%Y %H:%M:%S')
        user = self.env.user
        user_name = user.name or 'User'

        for record in self:
            matrix_line_ids = record.mrp_plan_approval_matrix_lines_ids
            for line in matrix_line_ids:
                if user.id not in line.approve.ids:
                    continue

                matrix_status = ''
                if line.approval_matrix_status:
                    matrix_status = _('%s\r\n' % line.approval_matrix_status)

                reject_msg = _('%sRejected By: %s, %s' % (matrix_status, user_name, current_time))

                if reject_reason:
                    reject_msg = _('%s Reason: %s' % (reject_msg, reject_reason))

                line.write({
                    'state': 'rejected',
                    'approval_matrix_status': reject_msg
                })

            if all([line.state == 'rejected' for line in matrix_line_ids if not line.minimum_approver >= 1]):
                record.write({'state': 'reject'})

            self.check_approve_reject_state()

    def action_reject_wiz(self):
        view_id = self.env.ref('equip3_manuf_operations.manu_plan_reject_reason_wiz').id
        return {
            'type': 'ir.actions.act_window',
            'name': _('Reject Reason'),
            'res_model': 'mrp.plan.matrix.reject.reason.wizard',
            'target': 'new',
            'view_mode': 'form',
            'views': [[view_id, 'form']],
        }

    def check_approve_reject_state(self):
        for record in self:
            line_ids = record.mrp_plan_approval_matrix_lines_ids
            approved_state = len(line_ids.filtered(lambda l: l.state == 'approved'))
            rejected_state = len(line_ids.filtered(lambda l: l.state == 'rejected'))
            if all([line.state for line in line_ids]):
                state = 'reject'
                if approved_state >= 2:
                    if approved_state >= rejected_state:
                        state = 'approved'
                else:
                    if rejected_state < 1:
                        state = 'approved'
                record.write({'state': state})
