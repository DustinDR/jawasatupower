from odoo import models, fields, api


class StockProductionLot(models.Model):
	_inherit = 'stock.production.lot'

	consumption_finished_product_id = fields.Many2one('mrp.consumption', string='Consumption Finished Product')
	consumption_rejected_product_id = fields.Many2one('mrp.consumption', string='Consumption Rejected Product')
	mrp_consumption_expiration_date = fields.Datetime(string='MRP Consumption Expiration Date')

	@api.model
	def create(self, vals):
		rec = super(StockProductionLot, self).create(vals)
		if self.env.context.get('force_blank_expiration_date'):
			rec.expiration_date = False
		return rec
