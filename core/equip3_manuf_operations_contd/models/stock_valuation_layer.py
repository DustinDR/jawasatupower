from odoo import models, fields, api


class StockValuationLayer(models.Model):
	_inherit = 'stock.valuation.layer'

	def default_get(self, fields_list):
		if self.env.context.get('default_type') not in [False, 'none', 'component', 'byproduct', 'finished']:
			self = self.with_context(default_type='none')
		return super(StockValuationLayer, self).default_get(fields_list)

	mrp_plan_id = fields.Many2one('mrp.plan', 'Manufacturing Plan', readonly=True, copy=False)
	mrp_production_id = fields.Many2one('mrp.production', 'Manufacturing Order', readonly=True, copy=False)
	mrp_workorder_id = fields.Many2one('mrp.workorder', 'Work Order', readonly=True, copy=False)
	mrp_consumption_id = fields.Many2one('mrp.consumption', 'Production Record', readonly=True, copy=False)

	type = fields.Selection(selection=[
		('none', 'None'),
		('component', 'Material'),
		('byproduct', 'By-Product'),
		('finished', 'Finished Goods')
	], string='Type', required=True, copy=False, default='none', readonly=True)

	quantity = fields.Float(digits='Product Unit of Measure')
	remaining_qty = fields.Float(digits='Product Unit of Measure')
	purchase_order = fields.Many2one('purchase.order', 'Purchase Order', readonly=True, copy=False)
