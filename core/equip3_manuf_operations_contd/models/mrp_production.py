from odoo import models, fields, api, _
from odoo.exceptions import UserError


class MRPProduction(models.Model):
    _inherit = 'mrp.production'

    def _compute_lot_ids(self):
        for production in self:
            consumption_ids = production.consumption_ids
            lot_ids = consumption_ids.finished_product_sn | consumption_ids.rejected_product_sn
            production.lot_ids = [(6, 0, lot_ids.ids)]

    has_clicked_mark_done = fields.Boolean()
    bom_id_consumption = fields.Selection(related='bom_id.consumption')

    consumption_ids = fields.One2many('mrp.consumption', 'manufacturing_order_id', string='Production Records', readonly=True)
    stock_valuation_layer_ids = fields.One2many('stock.valuation.layer', 'mrp_production_id', string='Valuations', readonly=True)
    lot_ids = fields.Many2many('stock.production.lot', string='Lot/Serial Number', compute=_compute_lot_ids)

    def action_view_production_records(self):
        if self.consumption_ids:
            action = {
                'name': 'Production Records',
                'type': 'ir.actions.act_window',
                'res_model': 'mrp.consumption',
                'view_mode': 'tree,form',
                'domain': [('id', 'in', self.consumption_ids.ids)],
                'target': 'current'
            }
            return action

    def _set_qty_producing(self):
        pass

    def _get_move_finished_values(self, product_id, product_uom_qty, product_uom, operation_id=False,
                                  byproduct_id=False):
        res = super(MRPProduction, self)._get_move_finished_values(product_id, product_uom_qty, product_uom,
                                                                   operation_id, byproduct_id)
        standard_price = self.env['product.product'].browse(product_id).standard_price
        allocated_cost = 0.0
        if byproduct_id:
            allocated_cost = self.env['mrp.bom.byproduct'].browse(byproduct_id).allocated_cost
        res['price_unit'] = standard_price
        res['allocated_cost'] = allocated_cost
        return res

    @api.depends(
        'move_raw_ids.state', 'move_raw_ids.quantity_done', 'move_finished_ids.state',
        'workorder_ids', 'workorder_ids.state', 'product_qty', 'qty_producing', 'has_clicked_mark_done')
    def _compute_state(self):
        super(MRPProduction, self)._compute_state()
        for production in self:
            if production.has_clicked_mark_done:
                production.state = 'done'
            elif any(wo_state in ('progress', 'pause') for wo_state in production.workorder_ids.mapped('state')):
                production.state = 'progress'
            elif any(wo_state in ('ready', 'block') for wo_state in production.workorder_ids.mapped('state')) and any(
                    wo_duration > 0 for wo_duration in production.workorder_ids.mapped('duration')):
                production.state = 'progress'
            elif any(wo_state != 'progress' and wo_state == 'block' for wo_state in
                     production.workorder_ids.mapped('state')):
                production.state = 'confirmed'
            else:
                if production.state == 'done':
                    production.state = 'to_close'

            production.reservation_state = False
            if production.state not in ('draft', 'done', 'cancel'):
                relevant_move_state = production.move_raw_ids._get_relevant_state_among_moves()
                if relevant_move_state == 'partially_available':
                    if production.bom_id.operation_ids and production.bom_id.ready_to_produce == 'asap':
                        production.reservation_state = production._get_ready_to_produce_state()
                    else:
                        production.reservation_state = 'confirmed'
                elif relevant_move_state != 'draft':
                    production.reservation_state = relevant_move_state

    def button_mark_done(self):

        warning = self.env['button.mark.done.warning']
        skip_all_wo_done = self.env.context.get('skip_all_wo_done')

        for production_id in self:
            workorder_ids = production_id.workorder_ids

            if not skip_all_wo_done:
                all_workorder_done = all(wo.state in ('done', 'cancel') for wo in workorder_ids)
                if not all_workorder_done:

                    order = 'order'
                    if len(workorder_ids.filtered(lambda w: w.state != 'done')) > 1:
                        order += 's'

                    warning_id = warning.create({
                        'production_id': production_id.id,
                        'message': _('There are unfinished work %s, are you sure want to force done?' % order)
                    })
                    return warning_id.open_self()

            for workorder in workorder_ids:
                if workorder.state == 'progress':

                    if workorder.is_user_working:
                        raise UserError(_('Please finish work order %s first!' % workorder.workorder_id))
                    else:
                        workorder.with_context(bypass_consumption=True).button_finish()
                        move_to_cancel = (production_id.move_raw_ids | production_id.move_finished_ids).filtered(
                            lambda
                                m: m.product_id != production_id.product_id and m.workorder_id == workorder and m.state != 'done'
                        )
                        move_to_cancel._action_cancel()

                elif workorder.state in ('pending', 'ready'):
                    workorder.action_cancel()
                    move_to_cancel = (production_id.move_raw_ids | production_id.move_finished_ids).filtered(
                        lambda m: m.product_id != production_id.product_id and m.workorder_id == workorder
                    )
                    move_to_cancel._action_cancel()

            production_id.write({
                'has_clicked_mark_done': True,  # will trigger state to done (see compute_state)
                'date_finished': fields.Datetime.now(),
                'priority': '0',
                'is_locked': True,
            })

    def update_valuations(self):
        self.ensure_one()
        svl_ids = self.stock_valuation_layer_ids.filtered(lambda s: not s.account_move_id)

        svl_materials = svl_ids.filtered(lambda s: s.type == 'component')
        svl_byproducts = svl_ids.filtered(lambda s: s.type == 'byproduct')
        svl_finished = svl_ids.filtered(lambda s: s.type == 'finished')

        material_cost = sum(svl_materials.mapped('value'))

        # for subcontracting and future update svl type
        other_cost = sum(svl_ids.filtered(
            lambda s: s.type not in ('component', 'byproduct', 'finished')
        ).mapped('value'))

        total_cost = material_cost + other_cost
        byproduct_cost = 0.0
        for svl_byproduct in svl_byproducts:
            move_id = svl_byproduct.stock_move_id
            value = (move_id.allocated_cost * abs(total_cost)) / 100
            unit_cost = value / svl_byproduct.quantity
            byproduct_cost += value
            svl_byproduct.write({'value': value, 'unit_cost': unit_cost})

        finished_value = abs(total_cost + byproduct_cost)
        finished_unit_cost = finished_value / sum(svl_finished.mapped('quantity'))
        svl_finished.write({'value': finished_value, 'unit_cost': finished_unit_cost})

        for consumption in self.consumption_ids.filtered(lambda c: c.state == 'confirm'):
            consumption._account_entry_move()
