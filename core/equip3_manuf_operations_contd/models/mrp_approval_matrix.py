# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class MrpApprovalMatrixInherit(models.Model):
    _inherit = 'mrp.approval.matrix'

    matrix_type = fields.Selection(selection_add=[('pr', 'Production Record')])
    
    
    @api.model
    def create(self, vals):
        if 'branch' in vals and vals.get('branch') != '':
            if 'matrix_type' in vals and vals.get('matrix_type') == 'pr' and self.env['mrp.approval.matrix'].sudo().search([('company', '=', self.env.company.id), ('branch', '=', int(vals.get('branch'))), ('matrix_type', '=', 'pr')], limit=1):
                raise UserError(
                    _("The branch already used for another Approval Matrix. Please choose another branch."))
        return super(MrpApprovalMatrixInherit, self).create(vals)

    def write(self, values):
        if 'branch' in values and values.get('branch') != '':
            if 'matrix_type' in values and values.get('matrix_type') == 'pr' and self.env['mrp.approval.matrix'].sudo().search([('company', '=', self.env.company.id), ('branch', '=', int(values.get('branch'))), ('matrix_type', '=', 'pr')], limit=1):
                raise UserError(_("The branch already used for another Approval Matrix. Please choose another branch."))
        return super(MrpApprovalMatrixInherit, self).write(values)

class MrpPlanApprovalMatrixLineInherit(models.Model):
    _inherit = 'mrp.plan.approval.matrix.line'

    mrp_con_approval_matrix_id = fields.Many2one('mrp.consumption', 'Production Record')