from odoo import models, fields


class ResCompany(models.Model):
	_inherit = 'res.company'

	def write(self, vals):
		res = super(ResCompany, self).write(vals)
		manufacturing_pr_conf_menu = self.env.ref("equip3_manuf_operations_contd.con_mrp_consumption_menu")
		for company in self:
			if not company == self.env.company:
				continue
			manufacturing_pr_conf_menu.active = company.production_record_conf
		return res
