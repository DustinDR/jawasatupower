# -*- coding: utf-8 -*-
from odoo import fields, models, tools, api


class WizardMRPPlanApprovalMatrixRejectReasonInherit(models.TransientModel):
    _inherit = 'mrp.plan.matrix.reject.reason.wizard'

    def confirm(self):
        mrp_con = self.env["mrp.consumption"].browse(self._context.get("active_id"))
        if mrp_con:
            mrp_con.action_reject(reject_reason=str(self.reject_reason))