from odoo import models


class ProductProduct(models.Model):
	_inherit = 'product.product'

	def create_next_lot_and_serial(self, expiration_date=False, force_company=False):
		self.ensure_one()

		stock_production_lot = self.env['stock.production.lot'].with_context(force_blank_expiration_date=True)
		company_id = force_company or self.company_id.id or self.env.company.id

		values = {
			'product_id': self.id,
			'company_id': company_id,
			'mrp_consumption_expiration_date': expiration_date
		}

		if not self.is_sn_autogenerate and not self.is_in_autogenerate:
			values.update({'name': self.env['ir.sequence'].next_by_code('stock.lot.serial')})
			return stock_production_lot.create(values)

		if self.tracking == 'serial':
			digits = self.digits
			seq_to_update = 'current_sequence'
			current_seq = int(self.current_sequence)
		else:
			digits = self.in_digits
			seq_to_update = 'in_current_sequence'
			current_seq = int(self.in_current_sequence)

		consumption = self.env['mrp.consumption']
		while True:
			auto_sequence = self.product_tmpl_id._get_next_lot_and_serial(current_sequence=current_seq)
			lot_id = stock_production_lot.search([('name', '=', auto_sequence)])
			domain = ['|', ('finished_product_sn', 'in', [lot_id.id]), ('rejected_product_sn', 'in', [lot_id.id])]
			if not lot_id or not consumption.search(domain):
				break
			current_seq += 1

		if not lot_id:
			values.update({'name': auto_sequence})
			lot_id = stock_production_lot.create(values)

		# update for next sequence
		self.write({seq_to_update: str(current_seq + 1).zfill(digits)})

		return lot_id

	def fix_auto_sequence_error(self):
		self.ensure_one()
		sequences = ['current_sequence', 'in_current_sequence']
		digits = [self.digits, self.in_digits]
		get_next = self.product_tmpl_id._get_next_lot_and_serial

		for seq, digit in zip(sequences, digits):
			new_seq = 1
			while True:
				auto_seq = get_next(current_sequence=new_seq)
				if not self.env['stock.production.lot'].search([('name', '=', auto_seq)]):
					break
				new_seq += 1

			self.write({seq: str(new_seq).zfill(digit)})

	def reset_sequence(self):
		pass


class ProductTemplate(models.Model):
	_inherit = 'product.template'

	def reset_sequence(self):
		pass
