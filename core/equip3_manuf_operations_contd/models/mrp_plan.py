from odoo import models, fields, api


class MrpPlan(models.Model):
	_inherit = 'mrp.plan'

	consumption_ids = fields.One2many('mrp.consumption', 'manufacturing_plan', string='Production Records', readonly=True)
	stock_valuation_layer_ids = fields.One2many('stock.valuation.layer', 'mrp_plan_id', string='Valuations', readonly=True)
