# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from datetime import datetime


class WizardEquip3MrpProduction(models.TransientModel):
    _name = 'wizard.mrp.consumption'

    manufacturing_plan = fields.Char('Manufacturing Plan')
    manufacturing_order_id = fields.Many2one('mrp.production', string='Manufacturing Order')
    workorder_id = fields.Many2one('mrp.workorder', string='Work Order')
    product_id = fields.Many2one('product.product', string='Product')
    finished_product = fields.Float('Finished Product')
    finished_product_uom_id = fields.Many2one('uom.uom', 'Finished Product UoM')
    finished_product_sn = fields.Char('Lot/Serial Number')
    rejected_product = fields.Float('Rejected Product')
    rejected_product_uom_id = fields.Many2one('uom.uom', 'Rejected Product UoM')
    rejected_product_sn = fields.Char('Lot/Serial Number')

    move_raw_ids = fields.One2many('stock.move', 'raw_material_production_id', 'Components')
    branch_id = fields.Many2one('res.branch', string="Branch", store=True, readonly=True, default=lambda self: self.env.user.branch_id)
    company_id = fields.Many2one('res.company', string='Company', store=True, readonly=True, default=lambda self: self.env.company)

    def save_record(self):
        self.env['mrp.consumption'].create({
            'name': self.product_id.name,
            'create_date': datetime.now(),
            'create_uid': self.env.uid,
            'manufacturing_order_id': self.manufacturing_order_id.id,
            'workorder_id': self.workorder_id.id,
            'product_id': self.product_id.id,
            'finished_product': self.finished_product,
            'finished_product_uom_id': self.finished_product_uom_id.id,
            'finished_product_sn': self.finished_product_sn,
            'rejected_product': self.rejected_product,
            'rejected_product_uom_id': self.rejected_product_uom_id.id,
            'rejected_product_sn': self.rejected_product_sn,
            'move_raw_ids': [
                [
                    1,
                    "",
                    {
                        "name": "New",
                        "product_id": line["product_id"].id,
                        "product_uom_qty": line["product_uom_qty"],
                        "product_uom": line["product_uom"].id,
                        "location_id": line["product_id"].location_id,
                        "location_dest_id": line["location_dest_id"].id,
                        "raw_material_production_id": line["raw_material_production_id"].id,
                    },
                ]
                for line in self.move_raw_ids
            ] 
        })
        self.workorder_id.button_finish()
        return {'type': 'ir.actions.act_window_close'}

