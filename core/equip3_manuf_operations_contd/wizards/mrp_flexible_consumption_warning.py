from odoo import models, fields, api


class MrpFlexibleConsumptionWarning(models.TransientModel):
    _name = 'mrp.flexible.consumption.warning'
    _description = 'MRP Flexible Consumption Warning'

    @api.depends('consumption_id')
    def compute_stock_move(self):
        for record in self:
            consumption_id = record.consumption_id
            not_expected_moves = consumption_id._get_not_expected_moves()

            byproduct_ids = not_expected_moves.filtered(lambda m: m.mrp_consumption_byproduct_id)
            component_ids = not_expected_moves - byproduct_ids

            record.byproduct_ids = [(6, 0, byproduct_ids.ids)]
            record.component_ids = [(6, 0, component_ids.ids)]

    component_ids = fields.One2many('stock.move', string='Materials', compute=compute_stock_move)
    byproduct_ids = fields.One2many('stock.move', string='By-Product', compute=compute_stock_move)
    consumption_id = fields.Many2one('mrp.consumption', string='Consumption', readonly=True, required=True)

    def action_confirm(self):
        self.ensure_one()
        return self.consumption_id.with_context(consumption_confirmed=True).button_confirm()
