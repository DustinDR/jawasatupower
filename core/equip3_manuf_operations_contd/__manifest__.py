# -*- coding: utf-8 -*-

{
    'name': 'Equip 3 - Manufacturing Operations Continued',
    'version': '1.1.22',
    'category': 'Manufacturing',
    'summary': 'Manufacturing Operations Continued',
    'description': '''
    i. Production Record
    ii. Production Record Approval Matrix
    ''',
    'author': 'HashMicro',
    'website': 'www.hashmicro.com',
    'depends': [
        "web",
        "mrp",
        "stock",
        "account",
        "mrp_account",
        "branch",
        "equip3_manuf_masterdata",
        "equip3_manuf_operations",
        "equip3_general_features",
        "stock_account",
        "equip3_inventory_masterdata"
    ],
    'data': [
        "security/ir.model.access.csv",
        "security/ir_rule.xml",
        "data/ir_sequence_data.xml",
        "views/mrp_consumption_view.xml",
        "views/mrp_workorder_view.xml",
        "views/mrp_production_views.xml",
        "views/mrp_plan_views.xml",
        "views/stock_move_views.xml",
        "views/stock_valuation_layer_views.xml",
        "views/product_template_views.xml",
        "views/mrp_approval_matrix.xml",
        "wizards/mrp_flexible_consumption_warning_views.xml",
        "wizards/button_mark_done_warning_views.xml",
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
