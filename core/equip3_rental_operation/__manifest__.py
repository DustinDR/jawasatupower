# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Equip3 Rental Operation',

    'summary': 'Rental Operation Management',

    'description': """
        This module manages these features :
        1. Rental Orders
        2. Deposit
    """,

    'depends': [
        "browseinfo_rental_management",
    ],

    'author': "Hashmicro",
    'category': 'Rental',
    'version': '1.1.2',

    'data': [
        "security/ir.model.access.csv",
        "views/account_move_views.xml",
        "views/rental_order_views.xml",
        "views/rental_order_checklist_item_views.xml",
    ],
    'installable': True,   
}