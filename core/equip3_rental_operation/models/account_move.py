
from odoo import api , fields , models


class AccountMove(models.Model):
	_inherit = "account.move"

	rental_order_id = fields.Many2one('rental.order', string="Rental Order")
	is_deposit_invoice = fields.Boolean(string="Is Deposit Invoice")
	is_deposit_return_invoice = fields.Boolean(string="Is Deposit Return Invoice")