
from odoo import api , fields , models, _
from odoo.exceptions import UserError, ValidationError, Warning

class RentalOrderChecklistLine(models.Model):
    _name = 'rental.order.checklist.line'
    _description = "Rental Order Checklist Line"

    item_id = fields.Many2one('rental.order.checklist.item', string="Item")
    checklist_id = fields.Many2one('rental.order.checklist', string="Checklist")
    is_available = fields.Boolean(string="Available", default=True)
    price = fields.Float(string='Price')
    order_id = fields.Many2one('rental.order', string="Rental Order")

    @api.onchange('item_id')
    def _onchange_item_id(self):
        if self.item_id:
            self.price = self.item_id.price
        else:
            self.price = False
