
# from dataclasses import field
from locale import currency
from odoo import api , fields , models, _
from odoo.exceptions import UserError, ValidationError


class RentalOrder(models.Model):
    _inherit = "rental.order"

    currency_id = fields.Many2one('res.currency', related='company_id.currency_id')
    amount = fields.Monetary(string="Amount Deposit")
    invoice_deposit_received = fields.Boolean(compute='_compute_invoice_deposit_received', string='Deposit Received?', copy=False)
    is_invoice_deposit_return = fields.Boolean(compute='_compute_invoice_deposit_return', string='Deposit Returned?', copy=False)
    count_deposit = fields.Integer(compute="_compute_deposit", string="Deposit Count")
    is_checking = fields.Boolean(string="Checking", default=False)
    attachment_ids = fields.Many2many('ir.attachment', string="Attachments")
    checklist_line_ids = fields.One2many('rental.order.checklist.line', 'order_id' ,string="Checklist Line")
    total_accessories = fields.Float(string='Total (Checklist Item)',readonly=True, compute="_compute_price_total")
    missing_cost = fields.Float(string="Missing Cost", readonly=True, compute="_compute_price_total")
    damage_cost = fields.Float(string="Damage Cost", readonly=True )
    damage_order_cost = fields.Float(string="Damage Cost")
    total = fields.Float(string="Total", readonly=True)
    notes = fields.Text(string="Detail & Notes")
    is_verified = fields.Boolean(string="Verified", default=False)

    def action_button_checking_rental(self):
        self.write({'is_checking': True})

    def action_button_close_rental(self):
        if self.checklist_line_ids and not self.is_verified:
            raise ValidationError('Can’t close rental order if the checklist not verified yet. Please verify the checklist first.')
        return super(RentalOrder, self).action_button_close_rental()

    def _compute_deposit(self):
        obj = self.env['account.move']
        for record in self:
            record.count_deposit = obj.search_count([
                ('rental_order_id', '=', record.id),
            ])

    def action_button_verify_rental(self):
        self.write({'is_verified': True})
        self._create_invoice()

    @api.depends('checklist_line_ids', 'checklist_line_ids.price', 'damage_order_cost')
    def _compute_price_total(self):
        for record in self:
            record.total_accessories = sum(record.checklist_line_ids.mapped('price'))
            record.missing_cost = sum(record.checklist_line_ids.filtered(lambda m: not m.is_available).mapped('price'))
            record.damage_cost = record.damage_order_cost
            record.total = record.missing_cost + record.damage_cost

    def _create_invoice(self):
        inv_obj = self.env['account.move']
        inv_line = []
        for checklist in self:
            account_id = False
            product_id = checklist.rental_line and checklist.rental_line[0].product_id or False
            if product_id:
                account_id = product_id.categ_id.property_account_income_categ_id.id
            if not account_id:
                raise UserError(
                    _('There is no income account defined for this product: "%s". You may have to install a chart of account from Accounting app, settings menu.') % \
                    (checklist.name,))
            inv_line.append((0, 0, {
                'name' : "Damage Cost",
                'account_id': account_id,
                'price_unit': checklist.damage_cost,
                'quantity': 1.0,
                'product_uom_id': product_id.uom_id.id,
            }))
            missing_cost_name = 'Missing Cost (Detail missing item: '
            missing_cost_name += ', '.join(checklist.checklist_line_ids.filtered(lambda r: not r.is_available).mapped('item_id.name'))
            missing_cost_name += ')'
            inv_line.append((0, 0, {
                'name' : missing_cost_name,
                'account_id': account_id,
                'price_unit': checklist.missing_cost,
                'quantity': 1.0,
                'product_uom_id': product_id.uom_id.id,
            }))
            invoice = inv_obj.create({
                'name': checklist.name or " ",
                'invoice_origin': checklist.name or " ",
                'move_type': 'out_invoice',
                'rental_id': checklist.id,
                'ref': False,
                'partner_id': checklist.partner_invoice_id.id,
                'invoice_line_ids': inv_line,
                'currency_id': checklist.pricelist_id.currency_id.id,
                'user_id':checklist.user_id.id,
                'rental_start_date': checklist.start_date,
                'rental_end_date' : checklist.end_date,
                'from_rent_order' :True,
            })
        return invoice

    @api.depends('amount')
    def _compute_invoice_deposit_return(self):
        for record in self:
            credit_ids = self.env['account.move'].search([
                ('rental_order_id', '=', record.id),
                ('move_type', '=', 'out_refund'),
                ('state', 'in', ['posted']),
                ('is_deposit_return_invoice', '=', True)
            ])
            residual_amt = 0.0
            record.is_invoice_deposit_return = False
            if credit_ids:
                residual_amt = sum(
                    [credit_inv.amount_residual for credit_inv in
                     credit_ids if credit_inv.amount_residual > 0.0])
                if residual_amt > 0.0:
                    record.is_invoice_deposit_return = False
                else:
                    record.is_invoice_deposit_return = True

    @api.depends('amount')
    def _compute_invoice_deposit_received(self):
        for record in self:
            deposit_ids = self.env['account.move'].search([
                ('rental_order_id', '=', record.id),
                ('move_type', '=', 'out_invoice'),
                ('state', 'in', ['posted']),
                ('is_deposit_invoice', '=', True)
            ])
            residua_amt = 0.0
            record.invoice_deposit_received = False
            if deposit_ids:
                residua_amt = sum(
                    [dp_inv.amount_residual for dp_inv in
                     deposit_ids if dp_inv.amount_residual > 0.0])
                if residua_amt > 0.0:
                    record.invoice_deposit_received = False
                else:
                    record.invoice_deposit_received = True

    def action_deposite_return(self):
        for record in self:
            deposit_ids = self.env['account.move'].search([
                ('rental_order_id', '=', record.id),
                ('state', 'in', ('draft', 'posted')),
                ('payment_state', '!=', 'paid'),
            ])
            if deposit_ids:
                raise UserError(_("Deposit Return invoice is already Pending\n"
                                  "Please proceed that Return invoice first"))
            self.ensure_one()
            purch_journal = record.env['account.journal'].search([('type', '=', 'sale')], limit=1)
            invoice_line_value = {
                'name': 'Deposit Return' or "",
                'quantity': 1,
                'account_id': False,
                'price_unit': record.amount or 0.00,
            }
            invoice_id = record.env['account.move'].create({
                'invoice_origin': 'Deposit Return For' + record.name or "",
                'move_type': 'out_refund',
                'partner_id': record.partner_id.id or False,
                'invoice_line_ids': [(0, 0, invoice_line_value)],
                'invoice_date': fields.Date.today() or False,
                'rental_order_id': record.id,
                'is_deposit_return_invoice': True,
                'journal_id': purch_journal and purch_journal.id or False,
            })
            record.write({'invoice_id': invoice_id.id})
        return True

    def action_deposite_receive(self):
        for record in self:
            if record.amount < 1:
                raise UserError(_("Deposit amount should not be zero.\n"
                                  "Please Enter Deposit Amount."))
            deposit_ids = self.env['account.move'].search([
                ('rental_order_id', '=', record.id),
                ('payment_state', '!=', 'paid'),
                ('state', 'in', ('draft', 'posted')),
            ])
            if deposit_ids:
                raise UserError(_("Deposit invoice is already Pending\n"
                                  "Please proceed that deposit invoice first"))
            invoice_line = {
                'name': 'Deposit Receive' or "",
                'quantity': 1,
                'price_unit': record.amount or 0.00,
            }
            invoice_id = record.env['account.move'].create({
                'move_type': 'out_invoice',
                'partner_id': record.partner_id.id or False,
                'invoice_line_ids': [(0, 0, invoice_line)],
                'invoice_date': fields.Date.today() or False,
                'rental_order_id': record.id,
                'is_deposit_invoice': True
            })
            record.write({'invoice_id': invoice_id.id})
            return True