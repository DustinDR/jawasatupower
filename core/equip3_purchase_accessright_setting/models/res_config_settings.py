
from odoo import api , fields , models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    is_vendor_approval_matrix = fields.Boolean(string="Vendor Approval Matrix")
    is_purchase_order_approval_matrix = fields.Boolean(string="Purchase Order Approval Matrix")
    is_purchase_request_approval_matrix = fields.Boolean(string="Purchase Request Approval Matrix")
    is_vendor_pricelist_approval_matrix = fields.Boolean(string="Vendor Pricelist Approval Matrix")
    is_purchase_tender_approval_matrix = fields.Boolean(string="Purchase Tender Approval Matrix")
    purchase_approval_matrix = fields.Boolean(string="Approval Matrix")
    is_blanket_order_approval_matrix = fields.Boolean(string="Blanket Order Approval Matrix")
    is_good_services_order = fields.Boolean(string="Goods Order and Services Order Menu")
    is_purchase_tender = fields.Boolean(string="Purchase Request to Purchase Tender")
    is_blanket_order = fields.Boolean(string="Purchase Request to Blanket Order")
    is_direct_purchase_approval_matrix = fields.Boolean(string="Direct Purchase Approval Matrix")
    retail = fields.Boolean(string="Retail")

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        res.update({
            'is_vendor_approval_matrix': IrConfigParam.get_param('is_vendor_approval_matrix', False),
            'is_purchase_order_approval_matrix': IrConfigParam.get_param('is_purchase_order_approval_matrix', False),
            'is_purchase_request_approval_matrix': IrConfigParam.get_param('is_purchase_request_approval_matrix', False),
            'is_vendor_pricelist_approval_matrix': IrConfigParam.get_param('is_vendor_pricelist_approval_matrix', False),
            'is_purchase_tender_approval_matrix': IrConfigParam.get_param('is_purchase_tender_approval_matrix', False),
            # 'purchase_approval_matrix': IrConfigParam.get_param('purchase_approval_matrix', False),
            'is_blanket_order_approval_matrix': IrConfigParam.get_param('is_blanket_order_approval_matrix', False),
            'is_good_services_order': IrConfigParam.get_param('is_good_services_order', False),
            'is_purchase_tender': self.env['ir.config_parameter'].sudo().get_param('is_purchase_tender', False),
            'is_direct_purchase_approval_matrix': self.env['ir.config_parameter'].sudo().get_param('is_direct_purchase_approval_matrix', False),
            'retail': self.env['ir.config_parameter'].sudo().get_param('retail', False),
        })
        return res

    def set_values(self):
        # if self.purchase == False:
        #     self.update({
        #         'purchase_approval_matrix': False,
        #     })
        if self.purchase == False:
            self.update({
                'is_vendor_approval_matrix': False,
                'is_purchase_order_approval_matrix': False,
                'is_purchase_request_approval_matrix': False,
                'is_vendor_pricelist_approval_matrix': False,
                'is_purchase_tender_approval_matrix': False,
                'is_blanket_order_approval_matrix': False,
                'is_good_services_order': False,
                'is_purchase_tender': False,
                'is_direct_purchase_approval_matrix': False,
                'retail': False

            })
        super(ResConfigSettings, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param('is_vendor_approval_matrix', self.is_vendor_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_purchase_order_approval_matrix', self.is_purchase_order_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_purchase_request_approval_matrix', self.is_purchase_request_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_vendor_pricelist_approval_matrix', self.is_vendor_pricelist_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_purchase_tender_approval_matrix', self.is_purchase_tender_approval_matrix)
        # self.env['ir.config_parameter'].sudo().set_param('purchase_approval_matrix', self.purchase_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_blanket_order_approval_matrix', self.is_blanket_order_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_good_services_order', self.is_good_services_order)
        self.env['ir.config_parameter'].sudo().set_param('is_purchase_tender', self.is_purchase_tender)
        self.env['ir.config_parameter'].sudo().set_param('is_direct_purchase_approval_matrix', self.is_direct_purchase_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('retail', self.retail)

        if self.is_purchase_tender:
            self.env.ref('equip3_purchase_accessright_setting.action_purchase_request_tender_line').create_action()
        else:
            self.env.ref('equip3_purchase_accessright_setting.action_purchase_request_tender_line').unlink_action()
