from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

WEEKDAYS = [
    ('0', 'Monday'),
    ('1', 'Tuesday'),
    ('2', 'Wednesday'),
    ('3', 'Thursday'),
    ('4', 'Friday'),
    ('5', 'Saturday'),
    ('6', 'Sunday'),
]


class resource_calendar(models.Model):
    _name = 'resource.calendar'
    _inherit = ['resource.calendar', 'mail.thread']

    @api.model
    def default_get(self, fields):
        res = super(resource_calendar, self).default_get(fields)
        for line in self.attendance_ids:
            line.unlink()
        return res

    schedule = fields.Selection([('fixed_schedule', 'Fixed Schedule'),
                                 ('shift_pattern', 'Shift Schedule'),
                                 ('roster_schedule', 'Roster Schedule'),
                                 ], string='Schedule Type', default='fixed_schedule', tracking=True)
    no_of_off_days = fields.Integer('No of Days Off', tracking=True)
    number_of_variation = fields.Integer('Number of Variation', tracking=True)
    week_working_day = fields.Integer(string='Number of working days', tracking=True)
    interval = fields.Selection([
        ('weekly', 'Weekly'),
        ('monthly', 'Monthly'), ], string='Interval', default='weekly', tracking=True)
    shift_pattern_line_ids = fields.One2many('shift.pattern.line', 'resource_calendar_id', 'Variations 1')
    shift_pattern_line_ids_2 = fields.One2many('shift.pattern.line2', 'resource_calendar_id_2', 'Variations 2')
    shift_pattern_line_ids_3 = fields.One2many('shift.pattern.line3', 'resource_calendar_id_3', 'Variations 3')
    shift_pattern_line_ids_4 = fields.One2many('shift.pattern.line4', 'resource_calendar_id_4', 'Variations 4')
    shift_pattern_line_ids_5 = fields.One2many('shift.pattern.line5', 'resource_calendar_id_5', 'Variations 5')
    shift_pattern_line_ids_6 = fields.One2many('shift.pattern.line6', 'resource_calendar_id_6', 'Variations 6')
    shift_pattern_line_ids_7 = fields.One2many('shift.pattern.line7', 'resource_calendar_id_7', 'Variations 7')
    variation = fields.Boolean(default=False)
    variation2 = fields.Boolean(default=False)
    variation3 = fields.Boolean(default=False)
    variation4 = fields.Boolean(default=False)
    variation5 = fields.Boolean(default=False)
    variation6 = fields.Boolean(default=False)
    variation7 = fields.Boolean(default=False)
    allow_public_holidays = fields.Boolean('Allow Schedule on Public Holidays')
    branch_id = fields.Many2one("res.branch", string="Branch", domain="[('company_id', '=', company_id)]",
                                tracking=True)
    is_shift_roaster_schedule = fields.Boolean(string="Is Shift / Roaster Schedule", default=False,
                                               compute='_compute_is_schedule_check')

    @api.constrains('shift_pattern_line_ids')
    def _check_exist_shift_pattern_line_ids(self):
        for rec in self:
            exist_product_list = []
            for line in rec.shift_pattern_line_ids:
                if line.variation_name:
                    if line.variation_name in exist_product_list:
                        raise ValidationError(_('Day should be one per line.'))
                exist_product_list.append(line.variation_name)

    @api.constrains('shift_pattern_line_ids_2')
    def _check_exist_shift_pattern_line_ids2(self):
        for rec in self:
            exist_product_list = []
            for line in rec.shift_pattern_line_ids_2:
                if line.variation_name in exist_product_list and line.variation_name:
                    raise ValidationError(_('Day should be one per line.'))
                exist_product_list.append(line.variation_name)

    @api.constrains('shift_pattern_line_ids_3')
    def _check_exist_shift_pattern_line_ids3(self):
        for rec in self:
            exist_product_list = []
            for line in rec.shift_pattern_line_ids_3:
                if line.variation_name in exist_product_list and line.variation_name:
                    raise ValidationError(_('Day should be one per line.'))
                exist_product_list.append(line.variation_name)

    @api.constrains('shift_pattern_line_ids_4')
    def _check_exist_shift_pattern_line_ids4(self):
        for rec in self:
            exist_product_list = []
            for line in rec.shift_pattern_line_ids_4:
                if line.variation_name in exist_product_list and line.variation_name:
                    raise ValidationError(_('Day should be one per line.'))
                exist_product_list.append(line.variation_name)

    @api.constrains('shift_pattern_line_ids_5')
    def _check_exist_shift_pattern_line_ids5(self):
        for rec in self:
            exist_product_list = []
            for line in rec.shift_pattern_line_ids_5:
                if line.variation_name in exist_product_list and line.variation_name:
                    raise ValidationError(_('Day should be one per line.'))
                exist_product_list.append(line.variation_name)

    @api.constrains('shift_pattern_line_ids_6')
    def _check_exist_shift_pattern_line_ids6(self):
        for rec in self:
            exist_product_list = []
            for line in rec.shift_pattern_line_ids_6:
                if line.variation_name in exist_product_list and line.variation_name:
                    raise ValidationError(_('Day should be one per line.'))
                exist_product_list.append(line.variation_name)

    @api.constrains('shift_pattern_line_ids_7')
    def _check_exist_shift_pattern_line_ids7(self):
        for rec in self:
            exist_product_list = []
            for line in rec.shift_pattern_line_ids_7:
                if line.variation_name in exist_product_list and line.variation_name:
                    raise ValidationError(_('Day should be one per line.'))
                exist_product_list.append(line.variation_name)

    # @api.constrains('attendance_ids')
    # def _check_exist_attendance_line(self):
    #     for rec in self:
    #         exist_product_list = []
    #         for line in rec.attendance_ids:
    #             if line.dayofweek in exist_product_list:
    #                 raise ValidationError(_('Name of Day should be one per line.'))
    #             exist_product_list.append(line.dayofweek)

    @api.constrains('week_working_day')
    def _check_week_working_day(self):
        if self.week_working_day <= 0 or self.week_working_day > 7:
            if self.schedule == 'shift_pattern':
                raise ValidationError(_('Number of working days in a week should be 1-7.'))

    @api.onchange('week_working_day', 'number_of_variation')
    def onchange_number_of_variation(self):
        self.attendance_ids = False
        self.shift_pattern_line_ids = False
        self.shift_pattern_line_ids_2 = False
        self.shift_pattern_line_ids_3 = False
        self.shift_pattern_line_ids_4 = False
        self.shift_pattern_line_ids_5 = False
        self.shift_pattern_line_ids_6 = False
        self.shift_pattern_line_ids_7 = False

        if self.week_working_day >= 8 and self.schedule != 'roster_schedule':
            raise UserError('Please add Week Working Day between 1 to 7')

        if self.week_working_day:
            list_days = ['0', '1', '2', '3', '4', '5', '6', '7']
            vals = []
            for line in range(self.week_working_day):
                if self.schedule == 'shift_pattern':
                    vals.append((0, 0, {'name': "Variation", 'variation_name': list_days[line]}))
                elif self.schedule == 'roster_schedule':
                    vals.append((0, 0, {'name': "Variation"}))
                elif self.schedule == 'fixed_schedule':
                    vals.append((0, 0, {'name': "Variation", 'dayofweek': list_days[line]}))
            if self.schedule != 'fixed_schedule':
                return {
                    'value': {'shift_pattern_line_ids': vals,
                              'shift_pattern_line_ids_2': vals,
                              'shift_pattern_line_ids_3': vals,
                              'shift_pattern_line_ids_4': vals,
                              'shift_pattern_line_ids_5': vals,
                              'shift_pattern_line_ids_6': vals,
                              'shift_pattern_line_ids_7': vals
                              }
                }
            else:
                return {
                    'value': {'attendance_ids': vals}
                }
        else:
            return {
                'value': {'shift_pattern_line_ids': False}
            }

    @api.onchange('number_of_variation')
    def onchange_create_number_of_variation(self):
        if self.number_of_variation >= 8:
            raise UserError('Please add Number of Variation between 1 to 7')

        self.variation = False
        self.variation2 = False
        self.variation3 = False
        self.variation4 = False
        self.variation5 = False
        self.variation6 = False
        self.variation7 = False
        if self.number_of_variation == 1:
            self.variation = True
            self.variation2 = False
            self.variation3 = False
            self.variation4 = False
            self.variation5 = False
            self.variation6 = False
            self.variation7 = False
        if self.number_of_variation == 2:
            self.variation = True
            self.variation2 = True
            self.variation3 = False
            self.variation4 = False
            self.variation5 = False
            self.variation6 = False
            self.variation7 = False
        if self.number_of_variation == 3:
            self.variation = True
            self.variation2 = True
            self.variation3 = True
            self.variation4 = False
            self.variation5 = False
            self.variation6 = False
            self.variation7 = False
        if self.number_of_variation == 4:
            self.variation = True
            self.variation2 = True
            self.variation3 = True
            self.variation4 = True
            self.variation5 = False
            self.variation6 = False
            self.variation7 = False
        if self.number_of_variation == 5:
            self.variation = True
            self.variation2 = True
            self.variation3 = True
            self.variation4 = True
            self.variation5 = True
            self.variation6 = False
            self.variation7 = False
        if self.number_of_variation == 6:
            self.variation = True
            self.variation2 = True
            self.variation3 = True
            self.variation4 = True
            self.variation5 = True
            self.variation6 = True
            self.variation7 = False
        if self.number_of_variation == 7:
            self.variation = True
            self.variation2 = True
            self.variation3 = True
            self.variation4 = True
            self.variation5 = True
            self.variation6 = True
            self.variation7 = True

    def mass_update_fixed(self):
        action = self.env.ref('equip3_hr_working_schedule.action_mass_update_fixed').read()[0]
        return action

    def mass_update_1(self):
        action = self.env.ref('equip3_hr_working_schedule.action_mass_update_one').read()[0]
        return action

    def mass_update_2(self):
        action = self.env.ref('equip3_hr_working_schedule.action_mass_update_two').read()[0]
        return action

    def mass_update_3(self):
        action = self.env.ref('equip3_hr_working_schedule.action_mass_update_three').read()[0]
        return action

    def mass_update_4(self):
        action = self.env.ref('equip3_hr_working_schedule.action_mass_update_four').read()[0]
        return action

    def mass_update_5(self):
        action = self.env.ref('equip3_hr_working_schedule.action_mass_update_five').read()[0]
        return action

    def mass_update_6(self):
        action = self.env.ref('equip3_hr_working_schedule.action_mass_update_six').read()[0]
        return action

    def mass_update_7(self):
        action = self.env.ref('equip3_hr_working_schedule.action_mass_update_seven').read()[0]
        return action

    @api.constrains('attendance_ids')
    def _check_attendance(self):
        # Avoid superimpose in attendance
        for calendar in self:
            attendance_ids = calendar.attendance_ids.filtered(
                lambda attendance: not attendance.resource_id and attendance.display_type is False)
            if calendar.two_weeks_calendar:
                calendar._check_overlap(attendance_ids.filtered(lambda attendance: attendance.week_type == '0'))
                calendar._check_overlap(attendance_ids.filtered(lambda attendance: attendance.week_type == '1'))

    @api.depends('schedule')
    def _compute_is_schedule_check(self):
        for line in self:
            if line.schedule == 'shift_pattern' or line.schedule == 'roster_schedule':
                line.is_shift_roaster_schedule = True
            else:
                line.is_shift_roaster_schedule = False


class resource_calendar_attendance_in(models.Model):
    _inherit = 'resource.calendar.attendance'

    name = fields.Char('Name', compute="fetch_sl_no")
    grace_time_for_late = fields.Float('Tolerance for Late')
    break_from = fields.Float('Break From')
    break_to = fields.Float('Break To')
    half_day = fields.Boolean('Allow Half Day')
    minimum_hours = fields.Float(string='Minimum Hours', required=0)

    @api.depends('dayofweek')
    def fetch_sl_no(self):
        for line in self:
            if line.dayofweek:
                line.name = line.dayofweek


class ShiftPatternLine(models.Model):
    _name = 'shift.pattern.line'

    name = fields.Boolean('Variation')
    variation = fields.Boolean(related='resource_calendar_id.variation')
    variation_name = fields.Selection(WEEKDAYS, string='Name of Day')
    break_from = fields.Float('Break From')
    break_to = fields.Float('Break To')
    resource_calendar_id = fields.Many2one('resource.calendar', ondelete='cascade')
    half_day = fields.Boolean('Allow Half Day')
    schedule = fields.Selection([('fixed_schedule', 'Fixed Schedule'),
                                 ('shift_pattern', 'Shift Pattern'),
                                 ('roster_schedule', 'Roster Schedule'),
                                 ], string='Schedule', related='resource_calendar_id.schedule')
    sequence = fields.Integer('Sequence', compute="fetch_sl_no")
    grace_time_for_late = fields.Float('Tolerance for Late')
    shift_name = fields.Char('Name')
    start_time = fields.Float('Work From')
    end_time = fields.Float('Work To')
    minimum_hours = fields.Float(string='Minimum Hours')

    @api.depends('name')
    def fetch_sl_no(self):
        sl = 0
        for line in self.resource_calendar_id.shift_pattern_line_ids:
            sl = sl + 1
            line.sequence = sl


class ShiftPatternLine2(models.Model):
    _name = 'shift.pattern.line2'

    @api.depends('name')
    def fetch_sl_no(self):
        sl = 0
        for line in self.resource_calendar_id_2.shift_pattern_line_ids_2:
            sl = sl + 1
            line.sequence = sl

    schedule = fields.Selection([('fixed_schedule', 'Fixed Schedule'),
                                 ('shift_pattern', 'Shift Pattern'),
                                 ('roster_schedule', 'Roster Schedule'),
                                 ], string='Schedule', related='resource_calendar_id_2.schedule')
    sequence = fields.Integer('Sequence', compute="fetch_sl_no")
    name = fields.Boolean('Variation')
    variation2 = fields.Boolean(related='resource_calendar_id_2.variation2')
    variation_name = fields.Selection(WEEKDAYS, string='Name of Day')
    start_time = fields.Float('Work From')
    end_time = fields.Float('Work To')
    break_from = fields.Float('Break From')
    break_to = fields.Float('Break To')
    resource_calendar_id_2 = fields.Many2one('resource.calendar', ondelete='cascade')
    half_day = fields.Boolean('Allow Half Day')
    grace_time_for_late = fields.Float('Tolerance for Late')
    shift_name = fields.Char('Name')
    minimum_hours = fields.Float(string='Minimum Hours')


class ShiftPatternLine3(models.Model):
    _name = 'shift.pattern.line3'

    @api.depends('name')
    def fetch_sl_no(self):
        sl = 0
        for line in self.resource_calendar_id_3.shift_pattern_line_ids_3:
            sl = sl + 1
            line.sequence = sl

    schedule = fields.Selection([('fixed_schedule', 'Fixed Schedule'),
                                 ('shift_pattern', 'Shift Pattern'),
                                 ('roster_schedule', 'Roster Schedule'),
                                 ], string='Schedule', related='resource_calendar_id_3.schedule')
    sequence = fields.Integer('Sequence', compute="fetch_sl_no")
    name = fields.Boolean('Variation')
    variation3 = fields.Boolean(related='resource_calendar_id_3.variation3')
    variation_name = fields.Selection(WEEKDAYS, string='Name of Day')
    start_time = fields.Float('Work From')
    end_time = fields.Float('Work To')
    break_from = fields.Float('Break From')
    break_to = fields.Float('Break To')
    resource_calendar_id_3 = fields.Many2one('resource.calendar', ondelete='cascade')
    half_day = fields.Boolean('Allow Half Day')
    grace_time_for_late = fields.Float('Tolerance for Late')
    shift_name = fields.Char('Name')
    minimum_hours = fields.Float(string='Minimum Hours')


class ShiftPatternLine4(models.Model):
    _name = 'shift.pattern.line4'

    @api.depends('name')
    def fetch_sl_no(self):
        sl = 0
        for line in self.resource_calendar_id_4.shift_pattern_line_ids_4:
            sl = sl + 1
            line.sequence = sl

    schedule = fields.Selection([('fixed_schedule', 'Fixed Schedule'),
                                 ('shift_pattern', 'Shift Pattern'),
                                 ('roster_schedule', 'Roster Schedule'),
                                 ], string='Schedule', related='resource_calendar_id_4.schedule')
    sequence = fields.Integer('Sequence', compute="fetch_sl_no")
    name = fields.Boolean('Variation')
    variation4 = fields.Boolean(related='resource_calendar_id_4.variation4')
    variation_name = fields.Selection(WEEKDAYS, string='Name of Day')
    start_time = fields.Float('Work From')
    end_time = fields.Float('Work To')
    break_from = fields.Float('Break From')
    break_to = fields.Float('Break To')
    resource_calendar_id_4 = fields.Many2one('resource.calendar', ondelete='cascade')
    half_day = fields.Boolean('Allow Half Day')
    grace_time_for_late = fields.Float('Tolerance for Late')
    shift_name = fields.Char('Name')
    minimum_hours = fields.Float(string='Minimum Hours')


class ShiftPatternLine5(models.Model):
    _name = 'shift.pattern.line5'

    @api.depends('name')
    def fetch_sl_no(self):
        sl = 0
        for line in self.resource_calendar_id_5.shift_pattern_line_ids_5:
            sl = sl + 1
            line.sequence = sl

    schedule = fields.Selection([('fixed_schedule', 'Fixed Schedule'),
                                 ('shift_pattern', 'Shift Pattern'),
                                 ('roster_schedule', 'Roster Schedule'),
                                 ], string='Schedule', related='resource_calendar_id_5.schedule')
    sequence = fields.Integer('Sequence', compute="fetch_sl_no")
    name = fields.Boolean('Variation')
    variation5 = fields.Boolean(related='resource_calendar_id_5.variation5')
    variation_name = fields.Selection(WEEKDAYS, string='Name of Day')
    start_time = fields.Float('Work From')
    end_time = fields.Float('Work To')
    break_from = fields.Float('Break From')
    break_to = fields.Float('Break To')
    resource_calendar_id_5 = fields.Many2one('resource.calendar', ondelete='cascade')
    half_day = fields.Boolean('Allow Half Day')
    grace_time_for_late = fields.Float('Tolerance for Late')
    shift_name = fields.Char('Name')
    minimum_hours = fields.Float(string='Minimum Hours')


class ShiftPatternLine6(models.Model):
    _name = 'shift.pattern.line6'

    @api.depends('name')
    def fetch_sl_no(self):
        sl = 0
        for line in self.resource_calendar_id_6.shift_pattern_line_ids_6:
            sl = sl + 1
            line.sequence = sl

    schedule = fields.Selection([('fixed_schedule', 'Fixed Schedule'),
                                 ('shift_pattern', 'Shift Pattern'),
                                 ('roster_schedule', 'Roster Schedule'),
                                 ], string='Schedule', related='resource_calendar_id_6.schedule')
    sequence = fields.Integer('Sequence', compute="fetch_sl_no")
    name = fields.Boolean('Variation')
    variation6 = fields.Boolean(related='resource_calendar_id_6.variation6')
    variation_name = fields.Selection(WEEKDAYS, string='Name of Day')
    start_time = fields.Float('Work From')
    end_time = fields.Float('Work To')
    break_from = fields.Float('Break From')
    break_to = fields.Float('Break To')
    resource_calendar_id_6 = fields.Many2one('resource.calendar', ondelete='cascade')
    half_day = fields.Boolean('Allow Half Day')
    grace_time_for_late = fields.Float('Tolerance for Late')
    shift_name = fields.Char('Name')
    minimum_hours = fields.Float(string='Minimum Hours')


class ShiftPatternLine7(models.Model):
    _name = 'shift.pattern.line7'

    @api.depends('name')
    def fetch_sl_no(self):
        sl = 0
        for line in self.resource_calendar_id_7.shift_pattern_line_ids_7:
            sl = sl + 1
            line.sequence = sl

    schedule = fields.Selection([('fixed_schedule', 'Fixed Schedule'),
                                 ('shift_pattern', 'Shift Pattern'),
                                 ('roster_schedule', 'Roster Schedule'),
                                 ], string='Schedule', related='resource_calendar_id_7.schedule')
    sequence = fields.Integer('Sequence', compute="fetch_sl_no")
    name = fields.Boolean('Variation')
    variation7 = fields.Boolean(related='resource_calendar_id_7.variation7')
    variation_name = fields.Selection(WEEKDAYS, string='Name of Day')
    start_time = fields.Float('Work From')
    end_time = fields.Float('Work To')
    break_from = fields.Float('Break From')
    break_to = fields.Float('Break To')
    resource_calendar_id_7 = fields.Many2one('resource.calendar', ondelete='cascade')
    half_day = fields.Boolean('Allow Half Day')
    grace_time_for_late = fields.Float('Tolerance for Late')
    shift_name = fields.Char('Name')
    minimum_hours = fields.Float(string='Minimum Hours')
