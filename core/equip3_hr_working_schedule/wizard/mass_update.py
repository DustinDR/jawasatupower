from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import UserError, ValidationError


class mass_update_fixed(models.TransientModel):
    _name = 'mass.update.fixed'

    resource_id = fields.Many2one('resource.calendar', "Resource")
    name = fields.Char('Name')
    hour_from = fields.Float('Work From', required=True)
    hour_to = fields.Float('Work To', required=True)
    grace_time_for_late = fields.Float('Tolerance for Late', required=True)
    break_from = fields.Float('Break From', required=True)
    break_to = fields.Float('Break To', required=True)
    half_day = fields.Boolean('Allow Half Day', required=True)
    is_shift_roaster_schedule = fields.Boolean(string="Is Shift / Roaster Schedule", default=False)
    minimum_hours = fields.Float(string='Minimum Hours')

    @api.model
    def default_get(self, fields):
        res = super(mass_update_fixed, self).default_get(fields)
        context = self._context
        if context.get('active_model') == 'resource.calendar':
            res_calendar = self.env['resource.calendar'].sudo().search(
                [('id', '=', context.get('active_id'))], limit=1)
            if res_calendar:
                res.update({
                    'is_shift_roaster_schedule': res_calendar.is_shift_roaster_schedule
                })
            return res

    def update_mass_fixed(self):
        active_id = self.env.context.get('active_ids')
        for resource in self.resource_id.browse(active_id):
            for mass in resource.attendance_ids:
                mass.hour_from = self.hour_from
                mass.hour_to = self.hour_to
                mass.grace_time_for_late = self.grace_time_for_late
                mass.break_from = self.break_from
                mass.break_to = self.break_to
                mass.half_day = self.half_day
                mass.minimum_hours = self.minimum_hours


class mass_update_one(models.TransientModel):
    _name = 'mass.update.one'

    resource_id = fields.Many2one('resource.calendar', "Resource")
    name = fields.Char('Name')
    hour_from = fields.Float('Work From', required=True)
    hour_to = fields.Float('Work To', required=True)
    grace_time_for_late = fields.Float('Tolerance for Late', required=True)
    break_from = fields.Float('Break From', required=True)
    break_to = fields.Float('Break To', required=True)
    half_day = fields.Boolean('Allow Half Day', required=True)
    is_shift_roaster_schedule = fields.Boolean(string="Is Shift / Roaster Schedule", default=False)
    minimum_hours = fields.Float(string='Minimum Hours')

    @api.model
    def default_get(self, fields):
        res = super(mass_update_one, self).default_get(fields)
        context = self._context
        if context.get('active_model') == 'resource.calendar':
            res_calendar = self.env['resource.calendar'].sudo().search(
                [('id', '=', context.get('active_id'))], limit=1)
            if res_calendar:
                res.update({
                    'is_shift_roaster_schedule': res_calendar.is_shift_roaster_schedule
                })
            return res

    def update_mass_one(self):
        active_id = self.env.context.get('active_ids')
        for resource in self.resource_id.browse(active_id):
            for mass in resource.shift_pattern_line_ids:
                mass.start_time = self.hour_from
                mass.end_time = self.hour_to
                mass.grace_time_for_late = self.grace_time_for_late
                mass.break_from = self.break_from
                mass.break_to = self.break_to
                mass.half_day = self.half_day
                mass.minimum_hours = self.minimum_hours


class mass_update_two(models.TransientModel):
    _name = 'mass.update.two'

    resource_id = fields.Many2one('resource.calendar', "Resource")
    name = fields.Char('Name')
    hour_from = fields.Float('Work From', required=True)
    hour_to = fields.Float('Work To', required=True)
    grace_time_for_late = fields.Float('Tolerance for Late', required=True)
    break_from = fields.Float('Break From', required=True)
    break_to = fields.Float('Break To', required=True)
    half_day = fields.Boolean('Allow Half Day', required=True)
    is_shift_roaster_schedule = fields.Boolean(string="Is Shift / Roaster Schedule", default=False)
    minimum_hours = fields.Float(string='Minimum Hours')

    @api.model
    def default_get(self, fields):
        res = super(mass_update_two, self).default_get(fields)
        context = self._context
        if context.get('active_model') == 'resource.calendar':
            res_calendar = self.env['resource.calendar'].sudo().search(
                [('id', '=', context.get('active_id'))], limit=1)
            if res_calendar:
                res.update({
                    'is_shift_roaster_schedule': res_calendar.is_shift_roaster_schedule
                })
            return res

    def update_mass_two(self):
        active_id = self.env.context.get('active_ids')
        for resource in self.resource_id.browse(active_id):
            for mass in resource.shift_pattern_line_ids_2:
                mass.start_time = self.hour_from
                mass.end_time = self.hour_to
                mass.grace_time_for_late = self.grace_time_for_late
                mass.break_from = self.break_from
                mass.break_to = self.break_to
                mass.half_day = self.half_day
                mass.minimum_hours = self.minimum_hours


class mass_update_three(models.TransientModel):
    _name = 'mass.update.three'

    resource_id = fields.Many2one('resource.calendar', "Resource")
    name = fields.Char('Name')
    hour_from = fields.Float('Work From', required=True)
    hour_to = fields.Float('Work To', required=True)
    grace_time_for_late = fields.Float('Tolerance for Late', required=True)
    break_from = fields.Float('Break From', required=True)
    break_to = fields.Float('Break To', required=True)
    half_day = fields.Boolean('Allow Half Day', required=True)
    is_shift_roaster_schedule = fields.Boolean(string="Is Shift / Roaster Schedule", default=False)
    minimum_hours = fields.Float(string='Minimum Hours')

    @api.model
    def default_get(self, fields):
        res = super(mass_update_three, self).default_get(fields)
        context = self._context
        if context.get('active_model') == 'resource.calendar':
            res_calendar = self.env['resource.calendar'].sudo().search(
                [('id', '=', context.get('active_id'))], limit=1)
            if res_calendar:
                res.update({
                    'is_shift_roaster_schedule': res_calendar.is_shift_roaster_schedule
                })
            return res

    def update_mass_three(self):
        active_id = self.env.context.get('active_ids')
        for resource in self.resource_id.browse(active_id):
            for mass in resource.shift_pattern_line_ids_3:
                mass.start_time = self.hour_from
                mass.end_time = self.hour_to
                mass.grace_time_for_late = self.grace_time_for_late
                mass.break_from = self.break_from
                mass.break_to = self.break_to
                mass.half_day = self.half_day
                mass.minimum_hours = self.minimum_hours


class mass_update_four(models.TransientModel):
    _name = 'mass.update.four'

    resource_id = fields.Many2one('resource.calendar', "Resource")
    name = fields.Char('Name')
    hour_from = fields.Float('Work From', required=True)
    hour_to = fields.Float('Work To', required=True)
    grace_time_for_late = fields.Float('Tolerance for Late', required=True)
    break_from = fields.Float('Break From', required=True)
    break_to = fields.Float('Break To', required=True)
    half_day = fields.Boolean('Allow Half Day', required=True)
    is_shift_roaster_schedule = fields.Boolean(string="Is Shift / Roaster Schedule", default=False)
    minimum_hours = fields.Float(string='Minimum Hours')

    @api.model
    def default_get(self, fields):
        res = super(mass_update_four, self).default_get(fields)
        context = self._context
        if context.get('active_model') == 'resource.calendar':
            res_calendar = self.env['resource.calendar'].sudo().search(
                [('id', '=', context.get('active_id'))], limit=1)
            if res_calendar:
                res.update({
                    'is_shift_roaster_schedule': res_calendar.is_shift_roaster_schedule
                })
            return res

    def update_mass_four(self):
        active_id = self.env.context.get('active_ids')
        for resource in self.resource_id.browse(active_id):
            for mass in resource.shift_pattern_line_ids_4:
                mass.start_time = self.hour_from
                mass.end_time = self.hour_to
                mass.grace_time_for_late = self.grace_time_for_late
                mass.break_from = self.break_from
                mass.break_to = self.break_to
                mass.half_day = self.half_day
                mass.minimum_hours = self.minimum_hours


class mass_update_five(models.TransientModel):
    _name = 'mass.update.five'

    resource_id = fields.Many2one('resource.calendar', "Resource")
    name = fields.Char('Name')
    hour_from = fields.Float('Work From', required=True)
    hour_to = fields.Float('Work To', required=True)
    grace_time_for_late = fields.Float('Tolerance for Late', required=True)
    break_from = fields.Float('Break From', required=True)
    break_to = fields.Float('Break To', required=True)
    half_day = fields.Boolean('Allow Half Day', required=True)
    is_shift_roaster_schedule = fields.Boolean(string="Is Shift / Roaster Schedule", default=False)
    minimum_hours = fields.Float(string='Minimum Hours')

    @api.model
    def default_get(self, fields):
        res = super(mass_update_five, self).default_get(fields)
        context = self._context
        if context.get('active_model') == 'resource.calendar':
            res_calendar = self.env['resource.calendar'].sudo().search(
                [('id', '=', context.get('active_id'))], limit=1)
            if res_calendar:
                res.update({
                    'is_shift_roaster_schedule': res_calendar.is_shift_roaster_schedule
                })
            return res

    def update_mass_five(self):
        active_id = self.env.context.get('active_ids')
        for resource in self.resource_id.browse(active_id):
            for mass in resource.shift_pattern_line_ids_5:
                mass.start_time = self.hour_from
                mass.end_time = self.hour_to
                mass.grace_time_for_late = self.grace_time_for_late
                mass.break_from = self.break_from
                mass.break_to = self.break_to
                mass.half_day = self.half_day
                mass.minimum_hours = self.minimum_hours


class mass_update_six(models.TransientModel):
    _name = 'mass.update.six'

    resource_id = fields.Many2one('resource.calendar', "Resource")
    name = fields.Char('Name')
    hour_from = fields.Float('Work From', required=True)
    hour_to = fields.Float('Work To', required=True)
    grace_time_for_late = fields.Float('Tolerance for Late', required=True)
    break_from = fields.Float('Break From', required=True)
    break_to = fields.Float('Break To', required=True)
    half_day = fields.Boolean('Allow Half Day', required=True)
    is_shift_roaster_schedule = fields.Boolean(string="Is Shift / Roaster Schedule", default=False)
    minimum_hours = fields.Float(string='Minimum Hours')

    @api.model
    def default_get(self, fields):
        res = super(mass_update_six, self).default_get(fields)
        context = self._context
        if context.get('active_model') == 'resource.calendar':
            res_calendar = self.env['resource.calendar'].sudo().search(
                [('id', '=', context.get('active_id'))], limit=1)
            if res_calendar:
                res.update({
                    'is_shift_roaster_schedule': res_calendar.is_shift_roaster_schedule
                })
            return res

    def update_mass_six(self):
        active_id = self.env.context.get('active_ids')
        for resource in self.resource_id.browse(active_id):
            for mass in resource.shift_pattern_line_ids_6:
                mass.start_time = self.hour_from
                mass.end_time = self.hour_to
                mass.grace_time_for_late = self.grace_time_for_late
                mass.break_from = self.break_from
                mass.break_to = self.break_to
                mass.half_day = self.half_day
                mass.minimum_hours = self.minimum_hours


class mass_update_seven(models.TransientModel):
    _name = 'mass.update.seven'

    resource_id = fields.Many2one('resource.calendar', "Resource")
    name = fields.Char('Name')
    hour_from = fields.Float('Work From', required=True)
    hour_to = fields.Float('Work To', required=True)
    grace_time_for_late = fields.Float('Tolerance for Late', required=True)
    break_from = fields.Float('Break From', required=True)
    break_to = fields.Float('Break To', required=True)
    half_day = fields.Boolean('Allow Half Day', required=True)
    is_shift_roaster_schedule = fields.Boolean(string="Is Shift / Roaster Schedule", default=False)
    minimum_hours = fields.Float(string='Minimum Hours')

    @api.model
    def default_get(self, fields):
        res = super(mass_update_seven, self).default_get(fields)
        context = self._context
        if context.get('active_model') == 'resource.calendar':
            res_calendar = self.env['resource.calendar'].sudo().search(
                [('id', '=', context.get('active_id'))], limit=1)
            if res_calendar:
                res.update({
                    'is_shift_roaster_schedule': res_calendar.is_shift_roaster_schedule
                })
            return res

    def update_mass_seven(self):
        active_id = self.env.context.get('active_ids')
        for resource in self.resource_id.browse(active_id):
            for mass in resource.shift_pattern_line_ids_7:
                mass.start_time = self.hour_from
                mass.end_time = self.hour_to
                mass.grace_time_for_late = self.grace_time_for_late
                mass.break_from = self.break_from
                mass.break_to = self.break_to
                mass.half_day = self.half_day
                mass.minimum_hours = self.minimum_hours
