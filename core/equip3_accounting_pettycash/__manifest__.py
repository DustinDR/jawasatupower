{
    'name': "Accounting Petty Cash",
    'author': "Hashmicro/Prince",
    'category': 'Equip3',
    'version': '1.1.7',
    'depends': ['base','account','branch'],
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence_data.xml',
        "wizards/replenish_wizards.xml",
        'wizards/account_pettycash_wizard.xml',
        'wizards/account_pettycash_fund_reconcile_wizards.xml',
        'wizards/account_pettycash_fund_change_wizards.xml',
        'wizards/account_pettycash_fund_close_wizards.xml',
        'views/product_template_view.xml',
        'views/account_pettycash_views.xml',
        'views/account_pettycash_voucher.xml',
        ],
    'demo': [],
    'auto_install': False,
    'installable': True,
    'application': True
}
