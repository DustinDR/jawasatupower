# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class CloseFundWizard(models.TransientModel):
    _name = 'account.pettycash.fund.close'
    _description = 'Petty Cash Fund Closing Wizard'

    @api.model
    def _get_fund(self):
        fund_id = self.env.context.get('active_id', False)
        return fund_id

    fund = fields.Many2one(
        'account.pettycash', default=_get_fund, required=True)
    receivable_account = fields.Many2one(
        'account.account', domain=[('user_type_id.name', 'in', ['Receivable', 'Bank and Cash'])])
    effective_date = fields.Date(string='Accounting Date', required=True)
    close_balance = fields.Float(related='fund.balance', string='Close Balance', store=True)

    def close_fund(self):
        for wizard in self:
            wizard.fund.close_fund(
                wizard.effective_date, wizard.receivable_account)
