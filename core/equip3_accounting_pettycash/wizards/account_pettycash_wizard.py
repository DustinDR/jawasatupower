# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class AccountPettycashVoucherhWizard(models.Model):
    _name = 'account.pettycash.voucher.wizard'
    _description = "Account Petty Cash Voucher Wizard"

    name = fields.Char(string='Name', readonly=True, required=True, copy=False, default='New')
    number = fields.Char(string="Number", readonly=True, required=True, copy=False, default='New')
    fund = fields.Many2one('account.pettycash', string='Fund', readonly=True, domain=[('state', '!=', ('closed'))])
    partner_id = fields.Many2one('res.users', string='Partner')
    date = fields.Date(string="Date", required=True)
    ba_ca_journal_id = fields.Many2one(related="fund.journal", string='Bank/Cash Journal', readonly=True, store=True)
    payment_reference = fields.Char(string="Payment Reference")
    submitter_id = fields.Many2one('res.users', string='Submitter', default=lambda self: self.env.user)
    attachment = fields.Binary()
    currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.company.currency_id,
                                  readonly=True, store=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company.id,
                                 readonly=True, store=True)
    state = fields.Selection(selection=[
        ('draft', 'Draft'),
        ('submitted', 'Submitted'),
        ('approved', 'Approved'),
        ('posted', 'Posted'),
        ('cancelled', 'Cancelled'),
    ], string='Status', default='draft', readonly=True)
    voucher_line = fields.One2many('account.pettycash.voucher.wizard.line', 'line_id', string='Voucher Line')
    total = fields.Monetary(string='Total', store=True, readonly=True, compute='_compute_amount')
    is_pettycash_voucher_approved = fields.Boolean('Petty Cash Voucher Approved')
    move_id = fields.Many2one('account.move', 'Journal Entry', copy=False)
    narration = fields.Text('Notes', readonly=True, states={'draft': [('readonly', False)]})
    voucher_type = fields.Selection([
        ('sale', 'Receipt'),
        ('purchase', 'Payment')
    ], string='Type', readonly=True, states={'draft': [('readonly', False)]}, oldname="type", default='sale')
    tax_amount = fields.Monetary(readonly=True, store=True, compute='_compute_total')

    @api.model
    def create(self, vals):
        if 'company_id' in vals:
            self = self.with_company(vals['company_id'])
        if vals.get('name', _('New')) == _('New'):
            seq_date = None
            if 'date' in vals:
                seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['date']))
            vals['name'] = self.env['ir.sequence'].next_by_code('account.pettycash.voucher.wizard.seq',
                                                                sequence_date=seq_date) or _('New')
            vals['state'] = 'submitted'
        result = super(AccountPettycashVoucherhWizard, self).create(vals)
        return result

    @api.depends('voucher_line', 'voucher_line.amount')
    def _compute_total(self):
        tax_calculation_rounding_method = self.env.user.company_id.tax_calculation_rounding_method
        for voucher in self:
            total = 0
            tax_amount = 0
            tax_lines_vals_merged = {}
            for line in voucher.voucher_line:
                tax_info = line.tax_ids.compute_all(line.price_unit, voucher.currency_id, line.quantity,
                                                    line.product_id, voucher.partner_id)
                if tax_calculation_rounding_method == 'round_globally':
                    total += tax_info.get('total_excluded', 0.0)
                    for t in tax_info.get('taxes', False):
                        key = (
                            t['id'],
                            t['account_id'],
                        )
                        if key not in tax_lines_vals_merged:
                            tax_lines_vals_merged[key] = t.get('amount', 0.0)
                        else:
                            tax_lines_vals_merged[key] += t.get('amount', 0.0)
                else:
                    total += tax_info.get('total_included', 0.0)
                    tax_amount += sum([t.get('amount', 0.0) for t in tax_info.get('taxes', False)])
            if tax_calculation_rounding_method == 'round_globally':
                tax_amount = sum([voucher.currency_id.round(t) for t in tax_lines_vals_merged.values()])
            voucher.tax_amount = tax_amount

    def account_move_get(self):
        for record in self:
            if not record.number or record.number == '/':
                seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(record.date))
                new_number = self.env['ir.sequence'].next_by_code('account.voucher', sequence_date=seq_date) or _('New')
            else:
                new_number = record.number
            record.number = new_number
            move = {
                'journal_id': record.ba_ca_journal_id.id,
                'narration': record.narration,
                'date': record.date,
                'ref': record.payment_reference,
            }
            return move

    def _convert(self, amount):
        for voucher in self:
            return voucher.currency_id._convert(amount, voucher.company_id.currency_id, voucher.company_id,
                                                voucher.date)

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        domain = domain or []
        context = dict(self.env.context) or {}
        user = self.env.user
        domain.append(('submitter_id', '=', user.id))
        return super(AccountPettycashVoucherhWizard, self).search_read(domain=domain, fields=fields, offset=offset,
                                                                       limit=limit, order=order)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        domain = domain or []
        context = dict(self.env.context) or {}
        user = self.env.user
        domain.append(('submitter_id', '=', user.id))
        return super(AccountPettycashVoucherhWizard, self).read_group(domain=domain, fields=fields, groupby=groupby,
                                                                      offset=offset, limit=limit,
                                                                      orderby=orderby, lazy=lazy)

    def first_move_line_get(self, move_id, company_currency, current_currency):
        debit = credit = 0.0
        if (self.voucher_type == 'sale' and self._convert(self.total) < 0.0) or (
                self.voucher_type == 'purchase' and self._convert(self.total) > 0.0):
            debit = self._convert(self.total)
        elif (self.voucher_type == 'sale' and self._convert(self.total) > 0.0) or (
                self.voucher_type == 'purchase' and self._convert(self.total) < 0.0):
            credit = self._convert(self.total)
        if debit < 0.0: debit = 0.0
        if credit < 0.0: credit = 0.0
        sign = debit - credit < 0 and -1 or 1
        # set the first line of the voucher
        move_line = {
            'name': '/',
            'debit': debit,
            'credit': credit,
            'account_id': self.fund.journal.default_account_id.id,
            'move_id': move_id,
            'journal_id': self.ba_ca_journal_id.id,
            'partner_id': self.partner_id.commercial_partner_id.id,
            'currency_id': company_currency != current_currency and current_currency or False,
            'amount_currency': (sign * abs(self.total)  # amount < 0 for refunds
                                if company_currency != current_currency else 0.0),
            'date': self.date,
        }
        return move_line

    def action_move_line_create(self):
        ''' PAY NOW IS DIRECT JOURNAL NON ACTIVE RECONCILED BEHAVIOUR
        Confirm the vouchers given in ids and create the journal entries for each of them
        '''
        for voucher in self:
            local_context = dict(self._context)
            if voucher.move_id:
                continue
            company_currency = voucher.ba_ca_journal_id.company_id.currency_id.id
            current_currency = voucher.currency_id.id or company_currency
            # we select the context to use accordingly if it's a multicurrency case or not
            # But for the operations made by _convert, we always need to give the date in the context
            ctx = local_context.copy()
            ctx['date'] = voucher.date
            ctx['check_move_validity'] = False
            # Create the account move record.
            move = self.env['account.move'].create(voucher.account_move_get())

            # Get the name of the account_move just created
            # Create the first line of the voucher
            move_line = self.env['account.move.line'].with_context(ctx).create(
                voucher.with_context(ctx).first_move_line_get(move.id, company_currency, current_currency))
            line_total = move_line.debit - move_line.credit
            if voucher.voucher_type == 'sale':
                line_total = line_total - voucher._convert(voucher.tax_amount)
            elif voucher.voucher_type == 'purchase':
                line_total = line_total + voucher._convert(voucher.tax_amount)
            # Create one move line per voucher line where amount is not 0.0
            line_total = voucher.with_context(ctx).voucher_move_line_create(line_total, move.id, company_currency,
                                                                            current_currency)

            # # Add tax correction to move line if any tax correction specified
            # if voucher.tax_correction != 0.0:
            #     tax_move_line = self.env['account.move.line'].search([('move_id', '=', move.id), ('tax_line_id', '!=', False)], limit=1)
            #     if len(tax_move_line):
            #         tax_move_line.write({'debit': tax_move_line.debit + voucher.tax_correction if tax_move_line.debit > 0 else 0,
            #             'credit': tax_move_line.credit + voucher.tax_correction if tax_move_line.credit > 0 else 0})
            move._post()
            voucher.write({
                'name': move.name,
                'move_id': move.id,
                'state': 'posted',
            })
        return True

    def _prepare_voucher_move_line(self, line, amount, move_id, company_currency, current_currency):
        line_subtotal = line.amount
        if self.voucher_type == 'sale':
            line_subtotal = -1 * line.amount

        if (self.voucher_type == 'sale' and amount > 0.0) or (self.voucher_type == 'purchase' and amount < 0.0):
            debit = abs(amount)
            credit = 0.0
        elif (self.voucher_type == 'sale' and amount < 0.0) or (self.voucher_type == 'purchase' and amount > 0.0):
            debit = 0.0
            credit = abs(amount)
        move_line = {
            'journal_id': self.ba_ca_journal_id.id,
            'name': '/',
            'account_id': line.expense_account.id,
            'move_id': move_id,
            'quantity': line.quantity,
            'product_id': line.product_id.id,
            'partner_id': self.partner_id.commercial_partner_id.id,
            'credit': abs(amount) if credit > 0.0 else 0.0,
            'debit': abs(amount) if debit > 0.0 else 0.0,
            'date': self.date,
            'tax_ids': [(4, t.id) for t in line.tax_ids],
            'amount_currency': line_subtotal if current_currency != company_currency else 0.0,
            'currency_id': company_currency != current_currency and current_currency or False,
            'payment_id': self._context.get('payment_id'),
        }
        return move_line

    def voucher_move_line_create(self, line_total, move_id, company_currency, current_currency):
        for line in self.voucher_line:
            if not line.amount:
                continue
            amount = self._convert(line.price_unit * line.quantity)
            move_line = self._prepare_voucher_move_line(line, amount, move_id, company_currency, current_currency)
            if (line.tax_ids):
                tax_group = line.tax_ids.compute_all(line.price_unit, line.currency_id, line.quantity, line.product_id,
                                                     self.partner_id)
                if move_line['debit']: move_line['debit'] = tax_group['total_excluded']
                if move_line['credit']: move_line['credit'] = tax_group['total_excluded']
                for tax_vals in tax_group['taxes']:
                    if tax_vals['amount']:
                        tax = self.env['account.tax'].browse([tax_vals['id']])
                        if not tax_vals['account_id']:
                            raise UserError(_('You have to setup account taxes for %s.' % tax_vals['name']))
                        account_id = (amount > 0 and tax_vals['account_id'])
                        if not account_id:
                            account_id = line.account_id.id
                        temp = {
                            'account_id': account_id,
                            'name': tax_vals['name'],
                            'tax_line_id': tax_vals['id'],
                            'move_id': move_id,
                            'date': self.date,
                            'partner_id': self.partner_id.id,
                            'debit': self.voucher_type == 'sale' and tax_vals['amount'] or 0.0,
                            'credit': self.voucher_type != 'sale' and tax_vals['amount'] or 0.0,
                        }
                        if company_currency != current_currency:
                            ctx = {}
                            if self.date:
                                ctx['date'] = self.date
                            temp['currency_id'] = current_currency.id
                            temp['amount_currency'] = company_currency._convert(tax_vals['amount'], current_currency,
                                                                                line.company_id,
                                                                                self.date or fields.Date.today(),
                                                                                round=True)
                        self.env['account.move.line'].create(temp)

            self.env['account.move.line'].create(move_line)
        return line_total

    def proforma_voucher(self):
        self.action_move_line_create()

    def action_approved(self):
        for rec in self:
            rec.state = 'approved'

    def action_create_voucher(self):
        if self._context.get('dont_redirect'):
            return True

    @api.depends('voucher_line.amount')
    def _compute_amount(self):
        for rec in self:
            total = 0.0
            for line in rec.voucher_line:
                total += line.amount
            rec.total = total


class AccountPettycashVoucherhWizardLine(models.Model):
    _name = 'account.pettycash.voucher.wizard.line'
    _description = "Account Petty Cash Voucher Wizard Line"

    line_id = fields.Many2one('account.pettycash.voucher.wizard', string='Voucher Line', readonly=True, store=True)
    product_id = fields.Many2one('product.product', string='Product', domain="[('use_on_petty_cash','=',True)]")
    name = fields.Char(string="Description")
    expense_account = fields.Many2one('account.account', string='Expense Account')
    quantity = fields.Float(string='Quantity', default=1)
    price_unit = fields.Float(string="Unit Price")
    tax_ids = fields.Many2one('account.tax', string='Tax', domain="[('type_tax_use','=','purchase')]")
    taxes = fields.Monetary(string="Taxes", readonly=True, store=True)
    price_total = fields.Monetary(string="price total", readonly=True, store=True)
    amount = fields.Monetary(string="Amount", readonly=True, store=True)
    currency_id = fields.Many2one('res.currency', related='line_id.currency_id')
    company_id = fields.Many2one('res.company', related='line_id.company_id')
    partner_id = fields.Many2one('res.users', related='line_id.partner_id')

    @api.onchange('product_id')
    def _onchange_product_id(self):
        for line in self:
            line.name = line._get_computed_name()
            line.expense_account = line._get_computed_account()
            taxes = line._get_computed_taxes()
            line.tax_ids = taxes
            line.expense_account = line._get_expense_account()

    @api.onchange('quantity', 'price_unit', 'tax_ids')
    def _onchange_price_subtotal(self):
        for line in self:
            line.update(line._get_price_total_and_subtotal())

    def _get_computed_name(self):
        self.ensure_one()
        if not self.product_id:
            return ''

        if self.partner_id.lang:
            product = self.product_id.with_context(lang=self.partner_id.lang)
        else:
            product = self.product_id

        values = []
        if product.partner_ref:
            values.append(product.partner_ref)
        if product.description_purchase:
            values.append(product.description_purchase)
        return '\n'.join(values)

    def _get_computed_account(self):
        self.ensure_one()
        self = self.with_company(self.line_id.ba_ca_journal_id.company_id)

        if not self.product_id:
            return

        accounts = self.product_id.product_tmpl_id.get_product_accounts(fiscal_pos=None)
        return accounts['expense'] or self.expense_account

    def _get_expense_account(self):
        self.ensure_one()
        expense_account = int(self._get_config_value('petty_cash_expense_account_id')) if self._get_config_value(
            'petty_cash_expense_account_id') else False
        return expense_account

    def _get_computed_taxes(self):
        self.ensure_one()

        if self.product_id.supplier_taxes_id:
            tax_ids = self.product_id.supplier_taxes_id.filtered(lambda tax: tax.company_id == self.line_id.company_id)
        elif self.expense_account.tax_ids:
            tax_ids = self.expense_account.tax_ids
        else:
            tax_ids = self.env['account.tax']

        if not tax_ids:
            tax_ids = self.line_id.company_id.account_purchase_tax_id

        if self.company_id and tax_ids:
            tax_ids = tax_ids.filtered(lambda tax: tax.company_id == self.company_id)

        return tax_ids

    def _get_price_total_and_subtotal(self, price_unit=None, quantity=None, currency=None, product=None, partner=None,
                                      taxes=None):
        self.ensure_one()
        return self._get_price_total_and_subtotal_model(
            price_unit=price_unit or self.price_unit,
            quantity=quantity or self.quantity,
            currency=currency or self.currency_id,
            product=product or self.product_id,
            partner=partner or self.partner_id,
            taxes=taxes or self.tax_ids,
        )

    @api.model
    def _get_price_total_and_subtotal_model(self, price_unit, quantity, currency, product, partner, taxes):
        res = {}
        # Compute 'price_subtotal'.
        line_discount_price_unit = price_unit
        subtotal = quantity * line_discount_price_unit
        # Compute 'price_total'.
        if taxes:
            force_sign = 1
            taxes_res = taxes._origin.with_context(force_sign=force_sign).compute_all(line_discount_price_unit,
                                                                                      quantity=quantity,
                                                                                      currency=currency,
                                                                                      product=product, partner=partner)
            res['price_total'] = taxes_res['total_excluded']
            res['amount'] = taxes_res['total_included']
            res['taxes'] = taxes_res['total_included'] - taxes_res['total_excluded']
        else:
            res['amount'] = res['price_total'] = subtotal
            res['taxes'] = 0

        if currency:
            res = {k: currency.round(v) for k, v in res.items()}
        return res

    def _get_config_value(self, para):
        value = self.env['ir.config_parameter'].sudo().get_param(para)
        return value
