# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.tools import float_compare
from odoo.exceptions import ValidationError


class AccountMove(models.Model):
    _inherit = 'account.move'

    pettycash_id = fields.Many2one('account.pettycash', string='pattycash')
    is_petty_cash_voucher = fields.Boolean(string='Is Petty Cash Voucher')

class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    use_on_petty_cash = fields.Boolean(string='Use on Petty Cash')

class AccountPettycash(models.Model):
    _name = 'account.pettycash'
    _description = "Account Petty Cash"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']

    number = fields.Char(string="Number", readonly=True, tracking=True)
    name = fields.Char(string="Name", readonly=True, required=True, tracking=True)
    custodian = fields.Many2one('res.users', string='Custodian', readonly=True, required=True, tracking=True)
    main_cash_account_id = fields.Many2one('account.account', domain="[('user_type_id.type', 'in', ['liquidity'])]", string='Cash Account', readonly=True, required=True, tracking=True)
    journal = fields.Many2one('account.journal',domain="[('type', 'in', ['bank','cash'])]" , string='Petty Cash Journal', readonly=True, required=True, tracking=True)
    amount = fields.Float(string="Fund Amount", required=True, tracking=True)
    balance = fields.Float(string="Balance", compute='compute_amount', tracking=True, store = True)
    virtual_balance = fields.Float(string="Virtual Balance", compute='compute_amount', tracking=True, store=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company.id, tracking=True, readonly=True)
    branch_id = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id.id, tracking=True)
    effective_date = fields.Date(string="Effective Date", tracking=True)
    create_uid = fields.Many2one('res.users', string='Created by', readonly=True, tracking=True)
    create_date = fields.Datetime(string="Created Date", readonly=True, tracking=True)
    state = fields.Selection(selection=[
            ('draft', 'Draft'),
            ('open', 'Open'),
            ('closed', 'Closed')
        ], string='State', default='draft', tracking=True)
    move_id = fields.One2many('account.move', 'pettycash_id', string='Cash Activity', invisible=True, readonly=True)
    voucher_id = fields.One2many('account.pettycash.voucher.wizard', 'fund', string='Voucher', domain=[('state', 'not in', ('posted', 'cancelled'))])
    paid_voucher_ids = fields.One2many('account.pettycash.voucher.wizard', 'fund', string='Petty Cash Voucher', domain=[('state', 'in', ('posted', 'cancelled'))])
    is_custodian_user = fields.Boolean(compute='_get_custodian_user', string='Custodian User')
    custodian_partner = fields.Many2one('res.partner', related='custodian.partner_id', readonly=True)
    is_replenished = fields.Boolean(compute='_get_replenish', string='Replenished')

    def _get_replenish(self):
        for record in self:
            record.is_replenished = False
            if record.balance == record.amount:
                record.is_replenished = True

    def _get_custodian_user(self):
        for record in self:
            record.is_custodian_user = False
            if record.custodian == self.env.user:
                record.is_custodian_user = True
    
    @api.depends('amount', 'voucher_id.total', 'voucher_id', 'paid_voucher_ids', 'move_id')
    def compute_amount(self):
        for rec in self:
            total_voucher = 0
            for line in rec.voucher_id:
                total_voucher += line.total

            if rec.state == 'draft':
                amount = rec.amount
            else:
                amount = sum([move.amount_total_signed for move in rec.move_id])

            rec.balance = amount if rec.state != 'closed' else 0
            total_virtual = rec.balance - total_voucher
            rec.virtual_balance = total_virtual if rec.state != 'closed' else 0

    def close_fund(self, date, receivable_account):
        for fund in self:
            if fund.voucher_id and len(fund.voucher_id) > 0:
                raise ValidationError(_("Petty Cash fund (%s) has un-reconciled vouchers" % (fund.name)))
            desc = _("Close Petty Cash fund (%s)" % (fund.name))
            if fund.balance != 0.00:
                if fund.balance < 0.00:
                    balance = -(fund.balance)
                else:
                    balance = fund.balance
                move = fund.create_receivable_journal_entry(fund, receivable_account.id, date, balance, desc)
                move.amount_total_signed = -(move.amount_total_signed)
                move.write({'pettycash_id': fund.id})
                fund.write({'amount': 0.0, 'balance': 0.0, 'state': 'closed'})
            else:
                fund.write({'amount': 0.0, 'balance': 0.0, 'state': 'closed'})

    @api.onchange('company_id')
    def _get_domain(self):
        return {'domain':{'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]"}}

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        selected_brach = self.branch_id
        if selected_brach:
            user_id = self.env['res.users'].browse(self.env.uid)
            allowed_branch = user_id.sudo().branch_ids
            if allowed_branch and selected_brach.id not in [ids.id for ids in allowed_branch] :
                raise UserError("Please select active branch only. Other may create the Multi branch issue. \n\ne.g: If you wish to add other branch then Switch branch from the header and set that.")

    @api.model
    def create(self, vals):
        if 'company_id' in vals:
            self = self.with_company(vals['company_id'])
        if vals.get('number', _('New')) == _('New'):
            seq_date = None
            if 'effective_date' in vals:
                seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['effective_date']))
            vals['number'] = self.env['ir.sequence'].next_by_code('account.pettycash.sequence', sequence_date=seq_date) or _('New')
        result = super(AccountPettycash, self).create(vals)
        return result

    def validate(self):
        line_ids_det = []
        line = {
                'date_maturity': self.effective_date,
                'name': 'Petty Cash : ' + (self.name),
                'account_id': self.journal.payment_debit_account_id.id,
                'debit': self.amount,
                'credit': 0.0
                }
        line_ids_det.append((0,0,line))
        line = {
                'date_maturity': self.effective_date,
                'name': 'Petty Cash : ' + (self.name),
                'account_id': self.journal.default_account_id.id,
                'debit':  0.0,
                'credit': self.amount
                }
        line_ids_det.append((0,0,line))
        all_move_vals = {
                'pettycash_id' : self.id,
                'date': self.effective_date,
                'journal_id': self.journal.id,
                'line_ids': line_ids_det
            }
                
        AccountMove = self.env['account.move']
        moves = AccountMove.create(all_move_vals)
        moves.post()
        self.update({'state' : 'open',
                     'balance' : self.amount,
                     'virtual_balance' : self.amount})

    def cancel(self):
        self.update({'state' : 'draft'})

    def create_voucher(self):
        return {
            'name': _('Create Voucher'),
            'res_model': 'account.pettycash.voucher.wizard',
            'view_mode': 'form',
            'context': {
                'active_model': 'account.pettycash',
                'active_ids': self.id,
                'default_fund': self.id,
                'default_ba_ca_journal_id': self.journal.id
            },
            'target': 'new',
            'type': 'ir.actions.act_window',
        }

    @api.model
    def create_journal_entry_common(self, _type, fnd, account_id, date, amount, desc):
        AccountMove = self.env['account.move']
        # Set debit and credit accounts according to type of entry. Default
        # to payable.
        debit_account = fnd.journal.payment_debit_account_id.id
        credit_account = account_id
        if _type == 'receivable':
            debit_account = account_id
            credit_account = fnd.journal.payment_credit_account_id.id
        # First, create the move
        # move_vals = AccountMove.account_move_prepare(
        #     fnd.journal.id, date=date)
        move_vals = AccountMove.default_get(AccountMove._fields)
        if fnd.journal:
            move_vals.update({'journal_id': fnd.journal.id, 'date': date})
        move_vals.update({'narration': desc,
                          'currency_id': self.env.user.company_id.currency_id.id or self.env.user.currency_id.id or self.env.user.partner_id.currency_id.id})
        # Create the first line
        move_line1_vals = {
            'name': desc,
            'debit': amount,
            'credit': 0.0,
            'account_id': debit_account,
            'journal_id': fnd.journal.id,
            'partner_id': fnd.custodian_partner.id,
            'date': date,
        }
        # Create the second line
        move_line2_vals = {
            'name': desc,
            'debit': 0.0,
            'credit': amount,
            'journal_id': fnd.journal.id,
            'account_id': credit_account,
            'partner_id': fnd.custodian_partner.id,
            'quantity': 1,
            'date': date,
            'date_maturity': date,
        }
        # Update the journal entry and post
        move_vals.update({
            'line_ids': [(0, 0, move_line2_vals), (0, 0, move_line1_vals)]
        })
        move = AccountMove.create(move_vals)
        move.post()
        move.write({'pettycash_id': fnd.id})
        return move

    @api.model
    def create_payable_journal_entry(self, fnd, account_id, date, amount, desc):
        return self.create_journal_entry_common('payable', fnd, account_id, date, amount, desc)

    @api.model
    def create_receivable_journal_entry(self, fnd, account_id, date, amount, desc):
        return self.create_journal_entry_common('receivable', fnd, account_id, date, amount, desc)

    def change_fund_amount(self, new_amount):
        for fund in self:
            # If this is a decrease in funds and there are unreconciled
            # vouchers do not allow the user to proceed.
            diff = float_compare(new_amount, fund.amount, precision_digits=2)
            if diff == -1 and fund.vouchers and len(fund.vouchers) > 0:
                raise ValidationError(_("Petty Cash fund (%s) has unreconciled vouchers" % (fund.name)))
            fund.amount = new_amount
