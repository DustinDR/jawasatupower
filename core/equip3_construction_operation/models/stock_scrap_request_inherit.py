from datetime import datetime
from odoo import api, fields, models, _

class StockScrapRequestInherit(models.Model):
    _inherit = 'stock.scrap.request'
    _description = "Stock Scrap Request Inherit"

    project = fields.Many2one ('project.project', string="Project")
    cost_sheet = fields.Many2one('job.cost.sheet', string='Cost Sheet', force_save="1")
    project_budget = fields.Many2one('project.budget', string='Project Budget', force_save="1")
    work_orders = fields.Many2one('project.task', string='Work Orders')
    material_type = fields.Selection([('material','Material'),('subcon','Subcon'),('equipment','Equipment'),('labour','Labour'),('overhead','Overhead')],string = "Material Type")

    @api.onchange('project')
    def _onchange_project(self):
        data = False
        date1 = False
        for rec in self:
            for proj in rec.project:
                self.cost_sheet = rec.env['job.cost.sheet'].search([('project_id', '=', self.project.id)])
                self.analytic_tag_ids = proj.analytic_idz
            date1 = datetime.today()
            data = rec.env['budget.period.line'].search([('start_date', '<', date1.date()),
                                                        ('end_date', '>', date1.date())])
            self.project_budget = rec.env['project.budget'].search([('project_id', '=', self.project.id),
                                                                    ('cost_sheet', '=', self.cost_sheet.id),
                                                                    ('month', '=', data.id)])

    # @api.depends('create_date')
    # def _onchange_create_date(self):
    #     data = False
    #     for rec in self:
    #         data = rec.env['budget.period.line'].search([('start_date', '<', self.create_date.date()),
    #                                                     ('end_date', '>', self.create_date.date())])
    #         self.project_budget = rec.env['project.budget'].search([('project_id', '=', self.project.id),
    #                                                                 ('cost_sheet', '=', self.cost_sheet.id),
    #                                                                 ('month', '=', data.id)])

    @api.onchange('work_orders')
    def _onchange_work_orders(self):
        self.scrap_ids = [(5, 0, 0)] 
        bmaterial = []
        bsubcon = []
        blabour = []
        boverhead = []
        bequipment = []
        for line in self.scrap_ids:
            if self.project_budget:
                for bud in self.project_budget:
                    for bmat in bud.budget_material_ids:
                        res = (0, 0, {
                            line.project_scope: bmat.project_scope,
                            line.section: bmat.section_name,
                            line.variable: bmat.variable,
                            line.group_of_product: bmat.group_of_product,
                            line.product_id: bmat.product_id,
                            # line.budget_qty: bmat.qty_left,
                            line.product_uom_id: bmat.uom_id,
                        })
                        bmaterial.append(res)
                    for bsub in bud.budget_subcon_ids:
                        res = (0, 0, {
                            line.project_scope: bsub.project_scope,
                            line.section: bsub.section_name,
                            line.subcon: bsub.subcon_id,
                            line.group_of_product: bsub.group_of_product,
                            line.product_id: bsub.product_id,
                            # line.budget_qty: bsub.qty_left,
                            line.product_uom_id: bsub.uom_id,
                        })
                        bsubcon.append(res)
                    for blab in bud.budget_subcon_ids:
                        res = (0, 0, {
                            line.project_scope: blab.project_scope,
                            line.section: blab.section_name,
                            line.variable: blab.variable,
                            line.group_of_product: blab.group_of_product,
                            line.product_id: blab.product_id,
                            # line.budget_qty: blab.qty_left,
                            line.product_uom_id: blab.uom_id,
                        })
                        blabour.append(res)
                    for bove in bud.budget_subcon_ids:
                        res = (0, 0, {
                            line.project_scope: bove.project_scope,
                            line.section: bove.section_name,
                            line.variable: bove.variable,
                            line.group_of_product: bove.group_of_product,
                            line.product_id: bove.product_id,
                            # line.budget_qty: bove.qty_left,
                            line.product_uom_id: bove.uom_id,
                        })
                        boverhead.append(res)
                    for bequ in bud.budget_subcon_ids:
                        res = (0, 0, {
                            line.project_scope: bequ.project_scope,
                            line.section: bequ.section_name,
                            line.variable: bequ.variable,
                            line.group_of_product: bequ.group_of_product,
                            line.product_id: bequ.product_id,
                            # line.budget_qty: bequ.qty_left,
                            line.product_uom_id: bequ.uom_id,
                        })
                        bequipment.append(res)
            else:
                for cost in self.cost_sheet:
                    for cmat in cost.material_ids:
                        res = (0, 0, {
                            line.project_scope: cmat.project_scope,
                            line.section: cmat.section_name,
                            line.variable: cmat.variable,
                            line.group_of_product: cmat.group_of_product,
                            line.product_id: cmat.product_id,
                            # line.budget_qty: cmat.qty_left,
                            line.product_uom_id: cmat.uom_id,
                        })
                        bmaterial.append(res)
                    for csub in cost.material_subcon_ids:
                        res = (0, 0, {
                            line.project_scope: csub.project_scope,
                            line.section: csub.section_name,
                            line.subcon: csub.subcon_id,
                            line.group_of_product: csub.group_of_product,
                            line.product_id: csub.product_id,
                            # line.budget_qty: csub.qty_left,
                            line.product_uom_id: csub.uom_id,
                        })
                        bsubcon.append(res)
                    for clab in cost.material_labour_ids:
                        res = (0, 0, {
                            line.project_scope: clab.project_scope,
                            line.section: clab.section_name,
                            line.variable: clab.variable,
                            line.group_of_product: clab.group_of_product,
                            line.product_id: clab.product_id,
                            # line.budget_qty: clab.qty_left,
                            line.product_uom_id: clab.uom_id,
                        })
                        blabour.append(res)
                    for cove in cost.material_overhead_ids:
                        res = (0, 0, {
                            line.project_scope: cove.project_scope,
                            line.section: cove.section_name,
                            line.variable: cove.variable,
                            line.group_of_product: cove.group_of_product,
                            line.product_id: cove.product_id,
                            # line.budget_qty: cove.qty_left,
                            line.product_uom_id: cove.uom_id,
                        })
                        boverhead.append(res)
                    for cequ in cost.material_equipment_ids:
                        res = (0, 0, {
                            line.project_scope: cequ.project_scope,
                            line.section: cequ.section_name,
                            line.variable: cequ.variable,
                            line.group_of_product: cequ.group_of_product,
                            line.product_id: cequ.product_id,
                            # line.budget_qty: cequ.qty_left,
                            line.product_uom_id: cequ.uom_id,
                        })
                        bequipment.append(res)

        if self.material_type == 'material':
            self.env['stock.scrap'].sudo().create(bmaterial)
        if self.material_type == 'subcon':
            self.env['stock.scrap'].sudo().create(bsubcon)
        if self.material_type == 'labour':
            self.env['stock.scrap'].sudo().create(blabour)
        if self.material_type == 'overhead':
            self.env['stock.scrap'].sudo().create(boverhead)
        if self.material_type == 'equipment':
            self.env['stock.scrap'].sudo().create(bequipment)
        
    


class StockScrapInherit(models.Model):
    _inherit = 'stock.scrap'

    project_scope = fields.Many2one('project.scope.line', string="Project Scope")
    section = fields.Many2one('section.estimate', string="Section")
    variable = fields.Many2one('variable.template', string="Variable")
    group_of_product = fields.Many2one('group.of.product', string="Group of Product")
    subcon = fields.Many2one('variable.template', string='Subcon', domain="[('variable_subcon', '=', True)]")