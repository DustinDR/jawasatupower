from odoo import api, fields, models, _
from datetime import datetime, date
from odoo.exceptions import ValidationError


class ProjectBudget(models.Model):
    _name = 'project.budget'
    _description = "Project Budget"

    name = fields.Char(string="Name", compute ='_compute_name')
    state = fields.Selection([('draft','Draft'),('waiting_approval','Waiting for Approval'),('approved','Approved'),('in_progress','In Progress'),('complete','Complete')],string = "State", readonly=True, default='draft')
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True,readonly=True,
                                 default=lambda self: self.env.company)
    project_id = fields.Many2one('project.project', string='Project', required=True)
    cost_sheet = fields.Many2one('job.cost.sheet', 'Cost Sheet',required=True)
    analytic_group_id = fields.Many2one('account.analytic.tag', string='Analytic Group')
    period = fields.Many2one('project.budget.period',required=True)
    month = fields.Many2one('budget.period.line',required=True)
    budget_material_ids = fields.One2many('budget.material','budget_id')
    budget_labour_ids = fields.One2many('budget.labour','budget_id')
    budget_subcon_ids = fields.One2many('budget.subcon','budget_id')
    budget_overhead_ids = fields.One2many('budget.overhead','budget_id')
    budget_equipment_ids = fields.One2many('budget.equipment','budget_id')

    def _compute_name(self):
        for rec in self:
            record = rec.cost_sheet.cost_sheet_name + ' - ' + rec.month.month + ' - ' + rec.period.name
            rec.write({'name' : record })

    @api.onchange('project_id')
    def _onchange_project(self):
        self.budget_material_ids = [(5, 0, 0)]
        self.budget_labour_ids = [(5, 0, 0)]
        self.budget_subcon_ids = [(5, 0, 0)]
        self.budget_overhead_ids = [(5, 0, 0)]
        self.budget_equipment_ids = [(5, 0, 0)]
        cost = []
        for rec in self:
            for proj in rec.project_id:
                cost = rec.env['job.cost.sheet'].search([('project_id', '=', self.project_id.id)])
                self.analytic_group_id = proj.analytic_idz
                self.write({'cost_sheet' : cost})
            for cos in self.cost_sheet:
                for material in cos.material_ids:
                    self.budget_material_ids = [(0, 0, {
                        'project_scope': material.project_scope.id,
                        'section_name': material.section_name.id,
                        'variable': material.variable_ref.id,
                        'group_of_product': material.group_of_product.id,
                        'product_id': material.product_id.id,
                        'description': material.description,
                        'qty_left': material.budgeted_qty_left,
                        'uom_id': material.uom_id.id,
                        'budget_quantity': material.product_qty,
                    })]

                for labour in cos.material_labour_ids:
                    self.budget_labour_ids = [(0, 0, {
                        'project_scope': labour.project_scope.id,
                        'section_name': labour.section_name.id,
                        'variable': labour.variable_ref.id,
                        'group_of_product': labour.group_of_product.id,
                        'product_id': labour.product_id.id,
                        'description': labour.description,
                        'qty_left': labour.budgeted_qty_left,
                        'uom_id': labour.uom_id.id,
                        'budget_quantity': labour.product_qty,
                    })]

                for subcon in cos.material_subcon_ids:
                    self.budget_subcon_ids = [(0, 0, {
                        'project_scope': subcon.project_scope.id,
                        'section_name': subcon.section_name.id,
                        'subcon_id': subcon.variable.id,
                        'description': subcon.description,
                        'qty_left': subcon.budgeted_qty_left,
                        'uom_id': subcon.uom_id.id,
                        'budget_quantity': subcon.product_qty,
                    })]
                    
                for overhead in cos.material_overhead_ids:
                    self.budget_overhead_ids = [(0, 0, {
                        'project_scope': overhead.project_scope.id,
                        'section_name': overhead.section_name.id,
                        'variable': overhead.variable_ref.id,
                        'group_of_product': overhead.group_of_product.id,
                        'product_id': overhead.product_id.id,
                        'description': overhead.description,
                        'qty_left': overhead.budgeted_qty_left,
                        'uom_id': overhead.uom_id.id,
                        'budget_quantity': overhead.product_qty,
                    })]

                for equipment in cos.material_equipment_ids:
                    self.budget_equipment_ids = [(0, 0, {
                        'project_scope': equipment.project_scope.id,
                        'section_name': equipment.section_name.id,
                        'variable': equipment.variable_ref.id,
                        'group_of_product': equipment.group_of_product.id,
                        'product_id': equipment.product_id.id,
                        'description': equipment.description,
                        'qty_left': equipment.budgeted_qty_left,
                        'uom_id': equipment.uom_id.id,
                        'budget_quantity': equipment.product_qty,
                    })]

    def btn_confirm_project(self):
        self.state = 'approved'

    def btn_confirm(self):
        self.state = 'in_progress'

    def btn_approve(self):
        pass




class BudgetMaterials(models.Model):
    _name = 'budget.material'
    _description = "Budget Materials"
    _order = 'sequence'
    _check_company_auto = True

    budget_id = fields.Many2one('project.budget', string='Budget')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date = fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product', compute="_compute_group_of_product")
    product_id = fields.Many2one('product.product', string='Product',
                                 check_company=True, tracking=True, required=True)
    description = fields.Text('Description', tracking=True, required=True)
    quantity = fields.Float(string="Quantity", default=1)
    qty_left = fields.Float(string='Budget Left', readonly=True, force_save="1")
    budget_quantity = fields.Float(string="Budget quantity")
    unallocated_quantity = fields.Float(string="Unallocated Quantity")
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')
    company_id = fields.Many2one('res.company',default=lambda self: self.env.company)

    @api.depends('budget_id.budget_material_ids', 'budget_id.budget_material_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.budget_id.budget_material_ids:
                no += 1
                l.sr_no = no


    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.description = self.product_id.name
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False

    @api.onchange('product_id')
    def _compute_group_of_product(self):
        if self.product_id:
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.group_of_product = False


class BudgetLabour(models.Model):
    _name = 'budget.labour'
    _description = "Budget Labour"
    _order = 'sequence'
    _check_company_auto = True

    budget_id = fields.Many2one('project.budget', string='Budget')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date = fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product', compute="_compute_group_of_product")
    product_id = fields.Many2one('product.product', string='Product',
                                 check_company=True, tracking=True, required=True)
    description = fields.Text('Description', tracking=True, required=True)
    quantity = fields.Float(string="Quantity", default=1)
    qty_left = fields.Float(string='Budget Left', readonly=True)
    budget_quantity = fields.Float(string="Budget quantity")
    unallocated_quantity = fields.Float(string="Unallocated Quantity")
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')
    company_id = fields.Many2one('res.company',default=lambda self: self.env.company)

    @api.depends('budget_id.budget_labour_ids', 'budget_id.budget_labour_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.budget_id.budget_labour_ids:
                no += 1
                l.sr_no = no


    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.description = self.product_id.name
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False

    @api.onchange('product_id')
    def _compute_group_of_product(self):
        if self.product_id:
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.group_of_product = False


class BudgetSubcon(models.Model):
    _name = 'budget.subcon'
    _description = "Budget Subcon"
    _order = 'sequence'
    _check_company_auto = True

    budget_id = fields.Many2one('project.budget', string='Budget')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date = fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    subcon_id = fields.Many2one('variable.template', string='Subcon',
                                 check_company=True, tracking=True, required=True)
    description = fields.Text('Description', tracking=True, required=True)
    quantity = fields.Float(string="Quantity", default=1)
    qty_left = fields.Float(string='Budget Left', readonly=True)
    budget_quantity = fields.Float(string="Budget quantity")
    unallocated_quantity = fields.Float(string="Unallocated Quantity")
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')
    company_id = fields.Many2one('res.company',default=lambda self: self.env.company)

    @api.depends('budget_id.budget_subcon_ids', 'budget_id.budget_subcon_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.budget_id.budget_subcon_ids:
                no += 1
                l.sr_no = no


    @api.onchange('subcon_id')
    def onchange_product_id(self):
        if self.subcon_id:
            self.uom_id = self.subcon_id.variable_uom.id
            self.quantity = 1.0
            self.description = self.subcon_id.name
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False


class BudgetOverhead(models.Model):
    _name = 'budget.overhead'
    _description = "Budget Overhead"
    _order = 'sequence'
    _check_company_auto = True

    budget_id = fields.Many2one('project.budget', string='Budget')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date = fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product', compute="_compute_group_of_product")
    product_id = fields.Many2one('product.product', string='Product',
                                 check_company=True, tracking=True, required=True)
    description = fields.Text('Description', tracking=True, required=True)
    quantity = fields.Float(string="Quantity", default=1)
    qty_left = fields.Float(string='Budget Left', readonly=True)
    budget_quantity = fields.Float(string="Budget quantity")
    unallocated_quantity = fields.Float(string="Unallocated Quantity")
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')
    company_id = fields.Many2one('res.company',default=lambda self: self.env.company)

    @api.depends('budget_id.budget_overhead_ids', 'budget_id.budget_overhead_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.budget_id.budget_overhead_ids:
                no += 1
                l.sr_no = no


    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.description = self.product_id.name
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False

    @api.onchange('product_id')
    def _compute_group_of_product(self):
        if self.product_id:
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.group_of_product = False


class BudgetEquipment(models.Model):
    _name = 'budget.equipment'
    _description = "Budget Equipment"
    _order = 'sequence'
    _check_company_auto = True

    budget_id = fields.Many2one('project.budget', string='Budget')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date = fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product', compute="_compute_group_of_product")
    product_id = fields.Many2one('product.product', string='Product',
                                 check_company=True, tracking=True, required=True)
    description = fields.Text('Description', tracking=True, required=True)
    quantity = fields.Float(string="Quantity", default=1)
    qty_left = fields.Float(string='Budget Left', readonly=True)
    budget_quantity = fields.Float(string="Budget quantity")
    unallocated_quantity = fields.Float(string="Unallocated Quantity")
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')
    company_id = fields.Many2one('res.company',default=lambda self: self.env.company)

    @api.depends('budget_id.budget_equipment_ids', 'budget_id.budget_equipment_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.budget_id.budget_equipment_ids:
                no += 1
                l.sr_no = no


    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.description = self.product_id.name
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False

    @api.onchange('product_id')
    def _compute_group_of_product(self):
        if self.product_id:
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.group_of_product = False
