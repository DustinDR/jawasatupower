from odoo import api, fields, models, _
from datetime import datetime, date
from odoo.exceptions import ValidationError

class JobCostSheet_Inherit(models.Model):
    _name = 'job.cost.sheet'
    _inherit = ['job.cost.sheet', 'mail.thread', 'mail.activity.mixin']
    _rec_name = 'number'
    _order = 'id DESC' 
    _check_company_auto = True

    @api.model
    def create(self , vals):
        vals['number'] = self.env['ir.sequence'].next_by_code('job.cost.sheet.sequence') 
        return super(JobCostSheet_Inherit, self).create(vals)

    number = fields.Char(string='Sheet Number', required=True, copy=False, readonly=True, states={'draft': [('readonly', True)]}, index=True, default=lambda self: _('New'))

    job_reference = fields.Many2many('job.estimate', ondelete="cascade", string="Job Estimates Reference")
    partner_id = fields.Many2one('res.partner',string="Customer")
    account_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Group', domain="[('company_id', '=', company_id)]")

    approved_date = fields.Datetime(string="Approved Date", tracking=True)

    # create_uid = fields.Many2one('res.users', index=True)
    company_currency_id = fields.Many2one('res.currency', string='Currency',related='company_id.currency_id')
    sale_order_ref = fields.Many2many('sale.order.const', string='Sale Order Reference', tracking=True, ondelete="cascade")

    # supplier
    supplier_id = fields.Char(string = 'Supplier')

    #table
    material_ids = fields.One2many('material.material','job_sheet_id', string = 'Materials', states={'done': [('readonly', True)]})
    material_labour_ids = fields.One2many('material.labour','job_sheet_id', string = 'Labour', states={'done': [('readonly', True)]})
    material_overhead_ids = fields.One2many('material.overhead','job_sheet_id', string = 'Overhead', states={'done': [('readonly', True)]})
    material_equipment_ids = fields.One2many('material.equipment','job_sheet_id', string = 'Equipment', states={'done': [('readonly', True)]})
    material_subcon_ids = fields.One2many('material.subcon','job_sheet_id', string = 'Subcon', states={'done': [('readonly', True)]})
    contract_history_ids = fields.One2many('contract.history', 'job_sheet_id', string = 'Contract History' )
    
    amount_material = fields.Monetary(string='Material Cost', readonly=True, compute = '_amount_material')
    amount_labour = fields.Monetary(string='Labour Cost', readonly=True, compute = '_amount_labour')
    amount_overhead = fields.Monetary(string='Overhead Cost', readonly=True, compute = '_amount_overhead')
    amount_subcon = fields.Monetary(string='Subcon Cost', readonly=True, compute = '_amount_subcon')
    amount_equipment = fields.Monetary(string='Equipment Cost', readonly=True, compute = '_amount_equipment')
    
    amount_total = fields.Monetary(string='Total Cost', readonly=True, compute = '_amount_total', store = True)

    total_job_estimate = fields.Integer(string="Job Estimate",compute='_comute_job_estimate')
    total_sale_order = fields.Integer(string="Sales Order",compute='_comute_sales_orders')
    total_material_request = fields.Integer(string="Material Request",compute='_comute_material_request')
    total_purchase_agreement = fields.Integer(string="Purchase Agreement",compute='_comute_purchase_agreement')
    total_project_budget = fields.Integer(string="Project budget", compute='_comute_project_budget')


    @api.onchange('project_id')
    def _get_customer(self):
        for proj in self.project_id:
            self.partner_id = proj.partner_id.id
            self.account_tag_ids = proj.analytic_idz

    @api.onchange('sale_order_ref')
    def _onchange_sale_order_ref(self):
        self.contract_history_ids = [(5, 0, 0)]
        self.job_reference = [(5, 0, 0)]
        self.material_ids = [(5, 0, 0)]
        self.material_labour_ids = [(5, 0, 0)]
        self.material_subcon_ids = [(5, 0, 0)]
        self.material_overhead_ids = [(5, 0, 0)]
        self.material_equipment_ids = [(5, 0, 0)]
        ids = self.job_reference.ids
        for res in self.sale_order_ref:
            ids.append(res.job_reference.id)
            #self.partner_id = res.partner_id.id
            #self.project_id = res.project_id.id
            #self.account_tag_ids = res.analytic_idz
            self.job_reference =[(6,0,ids)]
            self.contract_history_ids = [(0,0, {
                'contract_history':res._origin.id,
                'contract_category' : res.contract_category,
                'job_reference': res.job_reference.id,
                'date_order': res.date_order,
                'subtotal': res.amount_untaxed,
            })]
 
            #ord = self.sale_order_ref
            #self.job_reference = ord.job_reference
            #for res in ord:
                #self.contract_history_ids = [(0,0, {
                    #'contract_history': res._origin.id,
                    #'contract_category' : res.contract_category,
                    #'job_reference': res.job_reference,
                    #'date_order': res.date_order,
                    #'subtotal': res.amount_untaxed,
                    #'analytic_idz':res.analytic_idz,
                    #'created_by' : res.create_uid,
                    #'pricelist_id': res.pricelist_id,
                    #'currency_id': res.currency_id,
                    #'company_currency_id': res.company_currency_id,
                #})]
            
    @api.onchange('job_reference')
    def _onchange_job_reference(self):
        self.material_ids = [(5, 0, 0)]
        self.material_labour_ids = [(5, 0, 0)]
        self.material_subcon_ids = [(5, 0, 0)]
        self.material_overhead_ids = [(5, 0, 0)]
        self.material_equipment_ids = [(5, 0, 0)]

        if self.job_reference:
            job = self.job_reference
            for material in job.material_estimation_ids:
                self.material_ids = [(0, 0, {
                    'project_scope': material.project_scope.id,
                    'section_name': material.section_name.id,
                    'variable_ref': material.variable_ref.id,
                    'group_of_product': material.group_of_product.id,
                    'product_id': material.product_id.id,
                    'description': material.description,
                    'product_qty': material.quantity,
                    'uom_id': material.uom_id.id,
                    'price_unit': material.unit_price,
                    'material_amount_total': material.subtotal,
                })]
                
            for labour in job.labour_estimation_ids:
                self.material_labour_ids = [(0, 0, {
                    'project_scope': labour.project_scope.id,
                    'section_name': labour.section_name.id,
                    'variable_ref': labour.variable_ref.id,
                    'group_of_product': labour.group_of_product.id,
                    'product_id': labour.product_id.id,
                    'description': labour.description,
                    'product_qty': labour.quantity,
                    'uom_id': labour.uom_id.id,
                    'price_unit': labour.unit_price,
                    'labour_amount_total': labour.subtotal,
                })]

            for subcon in job.subcon_estimation_ids:
                self.material_subcon_ids = [(0, 0, {
                    'project_scope': subcon.project_scope.id,
                    'section_name': subcon.section_name.id,
                    'variable_ref': subcon.variable_ref.id,
                    'variable': subcon.variable.id,
                    'description': subcon.description,
                    'product_qty': subcon.quantity,
                    'uom_id': subcon.uom_id.id,
                    'price_unit': subcon.unit_price,
                    'subcon_amount_total': subcon.subtotal,
                })]
                
            for overhead in job.overhead_estimation_ids:
                self.material_overhead_ids = [(0, 0, {
                    'project_scope': overhead.project_scope.id,
                    'section_name': overhead.section_name.id,
                    'variable_ref': overhead.variable_ref.id,
                    'group_of_product': overhead.group_of_product.id,
                    'product_id': overhead.product_id.id,
                    'description': overhead.description,
                    'product_qty': overhead.quantity,
                    'uom_id': overhead.uom_id.id,
                    'price_unit': overhead.unit_price,
                    'overhead_amount_total': overhead.subtotal,
                })]

            for equipment in job.equipment_estimation_ids:
                self.material_equipment_ids = [(0, 0, {
                    'project_scope': equipment.project_scope.id,
                    'section_name': equipment.section_name.id,
                    'variable_ref': equipment.variable_ref.id,
                    'group_of_product': equipment.group_of_product.id,
                    'product_id': equipment.product_id.id,
                    'description': equipment.description,
                    'product_qty': equipment.quantity,
                    'uom_id': equipment.uom_id.id,
                    'price_unit': equipment.unit_price,
                    'equipment_amount_total': equipment.subtotal,
                })]
        

    def action_approved(self):
        if not self.material_ids and not self.material_labour_ids and not self.material_overhead_ids and not self.material_subcon_ids and not self.material_equipment_ids:
            raise ValidationError(_( "Add at least one material for estimation."))
        return self.write({'state': 'approved', 'approved_date' : datetime.now()})

    @api.depends('material_subcon_ids.subcon_amount_total')
    def _amount_subcon(self):
        for sheet in self:
            amount_subcon = 0.0
            for line in sheet.material_subcon_ids:
                amount_subcon += line.subcon_amount_total
            sheet.update({'amount_subcon': round(amount_subcon)})

    @api.depends('material_equipment_ids.equipment_amount_total')
    def _amount_equipment(self):
        for sheet in self:
            amount_equipment = 0.0
            for line in sheet.material_equipment_ids:
                amount_equipment += line.equipment_amount_total
            sheet.update({'amount_equipment': round(amount_equipment)})

    @api.depends('material_ids.material_amount_total','material_labour_ids.labour_amount_total','material_overhead_ids.overhead_amount_total', 'material_subcon_ids.subcon_amount_total', 'material_equipment_ids.equipment_amount_total')
    def _amount_total(self):
        for cost_sheet in self:
            amount_material = 0.0
            amount_labour = 0.0
            amount_overhead = 0.0
            amount_subcon = 0.0
            amount_equipment = 0.0
            for line in cost_sheet.material_ids:
                amount_material += line.material_amount_total
            for line in cost_sheet.material_labour_ids:
                amount_labour += line.labour_amount_total
            for line in cost_sheet.material_subcon_ids:
                amount_subcon += line.subcon_amount_total
            for line in cost_sheet.material_overhead_ids:
                amount_overhead += line.overhead_amount_total
            for line in cost_sheet.material_equipment_ids:
                amount_equipment += line.equipment_amount_total
            cost_sheet.amount_total = (amount_material + amount_labour + amount_subcon + amount_overhead + amount_equipment)
    
    def create_pa(self):
        variable_line = []
        for record in self:
            for var_line in record.material_subcon_ids:
                variable_line.append((0, 0, {'cs_subcon_id': var_line.job_sheet_id.id,
                                            'project_scope': var_line.project_scope.id,
                                            'section': var_line.section_name.id,
                                            'variable': var_line.variable.id,
                                            'budget_quantity': var_line.budgeted_qty_left,
                                            'uom': var_line.uom_id.id,
            }))

        return {
            'type': 'ir.actions.act_window',
            'res_model': 'purchase.agreement.wiz',
            'name': _("Create Purchase Agreement"),
            'purchase_agreement_line': variable_line,
            'target': 'new',
            'view_type': 'form',
            'view_mode': 'form',
        }
    
    def _comute_job_estimate(self):
        for job in self:
            job_count = self.env['job.estimate'].search_count([('project_id', '=', self.project_id.id), ('state', '=', 'sale')])
            job.total_job_estimate = job_count

    def _comute_sales_orders(self):
        for order in self:
            order_count = self.env['sale.order.const'].search_count([('project_id', '=', self.project_id.id), ('state', '=', ('sale','done'))])
            order.total_sale_order = order_count

    def _comute_material_request(self):
        for rec in self:
            material_request_count = self.env['material.request'].search_count([('job_cost_sheet', '=', rec.id)])
            rec.total_material_request = material_request_count

    def _comute_purchase_agreement(self):
        for rec in self:
            purchase_agreement_count = self.env['purchase.request'].search_count([('cost_sheet','=',rec.id)])
            rec.total_purchase_agreement = purchase_agreement_count

    def _comute_project_budget(self):
        for rec in self:
            project_budget_count = self.env['project.budget'].search_count([('cost_sheet','=',rec.id)])
            rec.total_project_budget = project_budget_count

    def action_job_estimate(self):
        return {
            'name': ("Job Estimates"),
            'view_mode': 'tree,form',
            'res_model': 'job.estimate',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('project_id', '=', self.project_id.id), ('state', '=', 'sale')],
        }

    def action_sale_order(self):
        return {
            'name': ("Sales Order"),
            'view_mode': 'tree,form',
            'res_model': 'sale.order.const',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('project_id', '=', self.project_id.id), ('state', '=', ('sale','done'))],
        }
    
    def action_material_request(self):
        return {
            'name': ("Material Request"),
            'view_mode': 'tree,form',
            'res_model': 'material.request',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('job_cost_sheet','=',self.id)],
        }

    def action_purchase_agreement(self):
        return {
            'name': ("Purchase Agreement"),
            'view_mode': 'tree,form',
            'res_model': 'purchase.request',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('cost_sheet','=',self.id)],
        }
    
    def action_project_budget(self):
        return {
            'name': ("Project Budget"),
            'view_mode': 'tree,form',
            'res_model': 'project.budget',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('cost_sheet','=',self.id)],
        }

class MaterialMaterial(models.Model):
    _inherit = 'material.material'
    _description = "Material"
    _order = 'sequence'

    job_sheet_id = fields.Many2one('job.cost.sheet', string = 'Job Sheet')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date =  fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product')
    product_id = fields.Many2one('product.product', string = 'Product')
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')
    purchased_qty = fields.Float('Purchased Quantity', default=0.00)
    billed_qty = fields.Float('Billed Quantity', default=0.00)
    billed_amt = fields.Float('Billed Amount', default=0.00)
    budgeted_qty_left = fields.Float('Budgeted Quantity Left', compute="_budget_quntity_left", force_save="1")
    budgeted_amt_left = fields.Float('Budgeted Amount Left', compute="_budget_amount_left", force_save="1")
    reserved_qty = fields.Float('Reserved Budget Quantity')
    reserved_amt = fields.Float('Reserved Budget Amount')
    actual_used_qty = fields.Float('Actual Used Quantity', default=0.00)
    actual_used_amt = fields.Float('Actual Used Amount', default=0.00)
    allocated_budget_qty = fields.Float('Allocated Budget Quantity', default=0.00)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    company_id = fields.Many2one(related='job_sheet_id.company_id', string='Company', store=True, readonly=True, index=True)

    @api.depends('job_sheet_id.material_ids', 'job_sheet_id.material_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.job_sheet_id.material_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.product_qty = 1.0
            self.price_unit = self.product_id.list_price
            self.description = self.product_id.display_name
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.description = False
            self.uom_id = False
            self.product_qty = False
            self.price_unit = False
            self.group_of_product = False

    @api.onchange('quantity', 'price_unit')
    def onchange_quantity(self):
        price = 0.00
        for line in self:
            price = (line.product_qty * line.price_unit)
            line.material_amount_total = price

    @api.onchange('product_qty', 'reserved_qty' ,'actual_used_qty')
    def _budget_quntity_left(self):
        for line in self:
            line.budgeted_qty_left = (line.product_qty - line.reserved_qty - line.actual_used_qty)

    @api.onchange('material_amount_total', 'reserved_amt','actual_used_amt')
    def _budget_amount_left(self):
        for line in self:
            line.budgeted_amt_left = (line.material_amount_total - line.reserved_amt - line.actual_used_amt)


class MaterialLabour(models.Model):
    _inherit = 'material.labour'
    _description = "Labour"
    _order = 'sequence'

    job_sheet_id = fields.Many2one('job.cost.sheet', string = 'Job Sheet')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date =  fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product')
    product_id = fields.Many2one('product.product', string = 'Product')
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')
    purchased_qty = fields.Float('Purchased Quantity', default=0.00)
    billed_qty = fields.Float('Billed Quantity', default=0.00)
    billed_amt = fields.Float('Billed Amount', default=0.00)
    reserved_qty = fields.Float('Reserved Budget Quantity')
    reserved_amt = fields.Float('Reserved Budget Amount')
    budgeted_qty_left = fields.Float('Budgeted Quantity Left', compute="_budget_quntity_left", force_save="1")
    budgeted_amt_left = fields.Float('Budgeted Amount Left', compute="_budget_amount_left", force_save="1")
    actual_used_qty = fields.Float('Actual Used Quantity', default=0.00)
    actual_used_amt = fields.Float('Actual Used Amount', default=0.00)
    allocated_budget_qty = fields.Float('Allocated Budget Quantity', default=0.00)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    company_id = fields.Many2one(related='job_sheet_id.company_id', string='Company', store=True, readonly=True, index=True)

    @api.depends('job_sheet_id.material_labour_ids', 'job_sheet_id.material_labour_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.job_sheet_id.material_labour_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.product_qty = 1.0
            self.price_unit = self.product_id.list_price
            self.description = self.product_id.display_name
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.description = False
            self.uom_id = False
            self.product_qty = False
            self.price_unit = False
            self.group_of_product = False
        
    @api.onchange('quantity', 'price_unit')
    def onchange_quantity(self):
        price = 0.00
        for line in self:
            price = (line.product_qty * line.price_unit)
            line.labour_amount_total = price

    @api.onchange('product_qty', 'reserved_qty' ,'actual_used_qty')
    def _budget_quntity_left(self):
        for line in self:
            line.budgeted_qty_left = (line.product_qty - line.reserved_qty - line.actual_used_qty)

    @api.onchange('labour_amount_total', 'actual_used_amt')
    def _budget_amount_left(self):
        for line in self:
            line.budgeted_amt_left = (line.labour_amount_total - line.reserved_amt - line.actual_used_amt)

class MaterialOverhead(models.Model):
    _inherit = 'material.overhead'
    _description = "Overhead"
    _order = 'sequence'

    job_sheet_id = fields.Many2one('job.cost.sheet', string = 'Job Sheet')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date =  fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product')
    product_id = fields.Many2one('product.product', string = 'Product')
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')
    purchased_qty = fields.Float('Purchased Quantity', default=0.00)
    billed_qty = fields.Float('Billed Quantity', default=0.00)
    billed_amt = fields.Float('Billed Amount', default=0.00)
    reserved_qty = fields.Float('Reserved Budget Quantity')
    reserved_amt = fields.Float('Reserved Budget Amount')
    budgeted_qty_left = fields.Float('Budgeted Quantity Left', compute="_budget_quntity_left", force_save="1")
    budgeted_amt_left = fields.Float('Budgeted Amount Left', compute="_budget_amount_left", force_save="1")
    actual_used_qty = fields.Float('Actual Used Quantity', default=0.00)
    actual_used_amt = fields.Float('Actual Used Amount', default=0.00)
    allocated_budget_qty = fields.Float('Allocated Budget Quantity', default=0.00)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    company_id = fields.Many2one(related='job_sheet_id.company_id', string='Company', store=True, readonly=True, index=True)

    @api.depends('job_sheet_id.material_overhead_ids', 'job_sheet_id.material_overhead_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.job_sheet_id.material_overhead_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.product_qty = 1.0
            self.price_unit = self.product_id.list_price
            self.description = self.product_id.display_name
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.description = False
            self.uom_id = False
            self.product_qty = False
            self.price_unit = False
            self.group_of_product = False
        
    @api.onchange('quantity', 'price_unit')
    def onchange_quantity(self):
        price = 0.00
        for line in self:
            price = (line.product_qty * line.price_unit)
            line.overhead_amount_total = price


    @api.onchange('product_qty', 'reserved_qty' ,'actual_used_qty')
    def _budget_quntity_left(self):
        for line in self:
            line.budgeted_qty_left = (line.product_qty - line.reserved_qty - line.actual_used_qty)

    @api.onchange('overhead_amount_total', 'reserved_amt','actual_used_amt')
    def _budget_amount_left(self):
        for line in self:
            line.budgeted_amt_left = (line.overhead_amount_total - line.reserved_amt - line.actual_used_amt)


class MaterialSubcon(models.Model):
    _name = 'material.subcon'
    _description = "Subcon"
    _order = 'sequence'

    job_sheet_id = fields.Many2one('job.cost.sheet', string = 'Job Sheet')
    description = fields.Char(string = 'Description')
    product_qty = fields.Float(string = 'Budgeted Quantity', default = '0.00')
    price_unit = fields.Float(string = 'Unit Price', default = '0.00')
    subcon_amount_total = fields.Float(string = 'Budgeted Amount')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date =  fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    variable = fields.Many2one('variable.template', string='Subcon', 
               domain="[('variable_subcon', '=', True), ('company_id', '=', parent.company_id)]")
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')
    purchased_qty = fields.Float('Purchased Quantity', default=0.00)
    billed_qty = fields.Float('Billed Quantity', default=0.00)
    billed_amt = fields.Float('Billed Amount', default=0.00)
    reserved_qty = fields.Float('Reserved Budget Quantity')
    reserved_amt = fields.Float('Reserved Budget Amount')
    budgeted_qty_left = fields.Float('Budgeted Quantity Left', compute="_budget_quntity_left", force_save="1")
    budgeted_amt_left = fields.Float('Budgeted Amount Left', compute="_budget_amount_left", force_save="1")
    actual_used_qty = fields.Float('Actual Used Quantity', default=0.00)
    actual_used_amt = fields.Float('Actual Used Amount', default=0.00)
    allocated_budget_qty = fields.Float('Allocated Budget Quantity', default=0.00)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    company_id = fields.Many2one(related='job_sheet_id.company_id', string='Company', store=True, readonly=True, index=True)

    @api.depends('job_sheet_id.material_subcon_ids', 'job_sheet_id.material_subcon_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.job_sheet_id.material_subcon_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('variable')
    def onchange_variable(self):
        if self.variable:
            self.uom_id = self.variable.variable_uom.id
            self.product_qty = 1.0
            self.price_unit = self.variable.total_variable
            self.description = self.variable.name
        else:
            self.description = False
            self.uom_id = False
            self.product_qty = False
            self.price_unit = False

        
    @api.onchange('product_qty', 'price_unit')
    def onchange_quantity(self):
        price = 0.00
        for line in self:
            price = (line.product_qty * line.price_unit)
            line.subcon_amount_total = price

    @api.onchange('product_qty', 'reserved_qty' ,'actual_used_qty')
    def _budget_quntity_left(self):
        for line in self:
            line.budgeted_qty_left = (line.product_qty - line.reserved_qty - line.actual_used_qty)

    @api.onchange('subcon_amount_total', 'reserved_amt','actual_used_amt')
    def _budget_amount_left(self):
        for line in self:
            line.budgeted_amt_left = (line.subcon_amount_total - line.reserved_amt - line.actual_used_amt)


class MaterialEquipment(models.Model):
    _name = 'material.equipment'
    _description = "Equipment"
    _order = 'sequence'

    job_sheet_id = fields.Many2one('job.cost.sheet', string = 'Job Sheet')
    product_id = fields.Many2one('product.product', string = 'Product', required=True)
    description = fields.Char(string = 'Description')
    product_qty = fields.Float(string = 'Budgeted Quantity', default = '0.00')
    price_unit = fields.Float(string = 'Unit Price', default = '0.00')
    equipment_amount_total = fields.Float(string = 'Budgeted Amount')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date =  fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product')
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')
    purchased_qty = fields.Float('Purchased Quantity', default=0.00)
    billed_qty = fields.Float('Billed Quantity', default=0.00)
    billed_amt = fields.Float('Billed Amount', default=0.00)
    reserved_qty = fields.Float('Reserved Budget Quantity')
    reserved_amt = fields.Float('Reserved Budget Amount')
    budgeted_qty_left = fields.Float('Budgeted Quantity Left', compute="_budget_quntity_left", force_save="1")
    budgeted_amt_left = fields.Float('Budgeted Amount Left', compute="_budget_amount_left", force_save="1")
    actual_used_qty = fields.Float('Actual Used Quantity', default=0.00)
    actual_used_amt = fields.Float('Actual Used Amount', default=0.00)
    allocated_budget_qty = fields.Float('Allocated Budget Quantity', default=0.00)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    company_id = fields.Many2one(related='job_sheet_id.company_id', string='Company', store=True, readonly=True, index=True)

    @api.depends('job_sheet_id.material_equipment_ids', 'job_sheet_id.material_equipment_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.job_sheet_id.material_equipment_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.product_qty = 1.0
            self.price_unit = self.product_id.list_price
            self.description = self.product_id.display_name
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.description = False
            self.uom_id = False
            self.product_qty = False
            self.price_unit = False
            self.group_of_product = False
        
    @api.onchange('product_qty', 'price_unit')
    def onchange_quantity(self):
        price = 0.00
        for line in self:
            price = (line.product_qty * line.price_unit)
            line.equipment_amount_total = price

    @api.onchange('product_qty', 'reserved_qty' ,'actual_used_qty')
    def _budget_quntity_left(self):
        for line in self:
            line.budgeted_qty_left = (line.product_qty - line.reserved_qty - line.actual_used_qty)

    @api.onchange('equipment_amount_total', 'reserved_amt','actual_used_amt')
    def _budget_amount_left(self):
        for line in self:
            line.budgeted_amt_left = (line.equipment_amount_total - line.reserved_amt - line.actual_used_amt)
            
            
class ContractHistory(models.Model):
    _name = 'contract.history'
    _description = "Contract History"
    _order = 'sequence'
    _check_company_auto = True

    job_sheet_id = fields.Many2one('job.cost.sheet', string="Cost Sheet")
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer(string="No", compute="_sequence_ref")
    contract_history = fields.Many2one('sale.order.const', string="Contract History")
    job_reference = fields.Many2one('job.estimate', string="Job Estimates Reference")
    date_order = fields.Datetime(string="Order Date")
    subtotal = fields.Float(string="Budgeted Amount")
    contract_category = fields.Selection([
                        ('main', 'Main Contract'), 
                        ('var', 'Variation Order')
                        ],string='Contract Category',default='main')
    company_id = fields.Many2one(related='job_sheet_id.company_id', string='Company', readonly=True)
    analytic_idz = fields.Many2many('account.analytic.tag', string='Analytic Group', domain="[('company_id', '=', parent.company_id)]")
    created_by = fields.Many2one('res.users', string='Created By')
    pricelist_id = fields.Many2one('product.pricelist', string='Pricelist')
    currency_id = fields.Many2one('product.pricelist', string="Sale Order Currency")
    company_currency_id = fields.Many2one('res.currency', string='Company Currency')
    
    
    @api.depends('job_sheet_id.contract_history_ids', 'job_sheet_id.contract_history_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.job_sheet_id.contract_history_ids:
                no += 1
                l.sr_no = no
