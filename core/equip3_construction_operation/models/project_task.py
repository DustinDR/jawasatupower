from odoo import api, fields, models, _
from datetime import datetime, date
from odoo.exceptions import ValidationError


class ProjectTaskNew(models.Model):
    _inherit = 'project.task'
    _description = "Work Order"

    stage = fields.Many2one('project.stage', string="Stage")
    stage_computed = fields.Many2many('project.stage', string='Stages', compute='get_stages')
    stage_weightage = fields.Float(string="Stage Weightage")
    actual_start_date = fields.Datetime(string="Actual Start Date")
    actual_end_date = fields.Datetime(string="Actual End Date")
    planned_start_date = fields.Date(string="Planned Start Date")
    planned_end_date = fields.Date(string="Planned End Date")
    project_director = fields.Many2one('res.users', string='Project Director')
    sub_contractor = fields.Many2one('res.partner', string="Sub Contractor")
    work_weightage = fields.Float(string="Work Order Weightage")
    assigned_to = fields.Many2one('res.users', string="PIC")
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True,
                                 default=lambda self: self.env.company)
    consumed_material_ids = fields.One2many('consumed.material', 'consumed_id')
    consumed_subcon_ids = fields.One2many('consumed.subcon', 'consumed_id')
    consumed_equipment_ids = fields.One2many('consumed.equipment', 'consumed_id')
    consumed_labour_ids = fields.One2many('consumed.labour', 'consumed_id')
    consumed_overhead_ids = fields.One2many('consumed.overhead', 'consumed_id')
    consumed_history_ids = fields.One2many('consumed.history', 'consumed_id')

    consumed_material_history_ids = fields.One2many(comodel_name='consumed.material.history', inverse_name='consumed_id', string='Consumed History')
    consumed_subcon_history_ids = fields.One2many(comodel_name='consumed.subcon.history', inverse_name='consumed_id', string='Consumed History')
    consumed_equipment_history_ids = fields.One2many(comodel_name='consumed.equipment.history', inverse_name='consumed_id', string='Consumed History')
    consumed_labour_history_ids = fields.One2many(comodel_name='consumed.labour.history', inverse_name='consumed_id', string='Consumed History')
    consumed_overhead_history_ids = fields.One2many(comodel_name='consumed.overhead.history', inverse_name='consumed_id', string='Consumed History')
    
    subtask_ids = fields.One2many('project.subtask', 'subtask_id')
    project_progress = fields.Float(string="Project Progress", compute="compute_project_progress")
    number = fields.Char(string='Work Order ID', required=True, copy=False, readonly=True,
                         states={'draft': [('readonly', True)]}, index=True, default=lambda self: _('New'))
    is_subtask = fields.Boolean(string="Is a Subtask", default=False)
    parent_task = fields.Many2one('project.task', string="Parent")
    is_subcon = fields.Boolean(string='Is Subcon', default=False)
    cost_sheet = fields.Many2one(comodel_name='job.cost.sheet', string='Cost Sheet', required=True)
    project_budget = fields.Many2one(comodel_name='project.budget', string='Project Budget', readonly=True)
    
    @api.onchange('sub_contractor')
    def onchange_sub_contractor(self):
        if self.sub_contractor:
            self.is_subcon = True
        else:
            self.is_subcon = False
        for record in self:
            automated_id = self.env['account.analytic.line'].search([('task_id', '=', record.name)])
            for result in automated_id:
                result.write({'is_subcon':record.is_subcon})
                result.write({'subcon_id':record.sub_contractor})

    @api.onchange('work_weightage')
    def onchange_work_weightage(self):
        if self.work_weightage > 100:
            raise ValidationError(_("The work weightage is more than 100%.\nPlease, re-set the weightage of the work order."))
    
    @api.model
    def create(self, vals):
        res = super(ProjectTaskNew, self).create(vals)
        vals['number'] = self.env['ir.sequence'].next_by_code('project.task.sequence')
        return res

    def unlink(self):
        subtsk = self.env['project.subtask'].search([('name', '=', self.name)])
        subtsk.unlink()
        return super(ProjectTaskNew, self).unlink()

    @api.onchange('project_id')
    def onchange_project_id(self):
        self.project_director = False
        self.partner_id = False
        self.stage = False
        if self.project_id:
            project = self.project_id
            self.partner_id = project.partner_id
            self.project_director = project.project_director

    @api.depends('project_id')
    def get_stages(self):
        for rec in self:
            if rec.project_id and rec.project_id.stage_ids:
                rec.stage_computed = [(6, 0, rec.project_id.stage_ids.ids)]
            else:
                rec.stage_computed = [(6, 0, [])]

    @api.onchange('stage')
    def onchange_stage_weightage(self):
        self.stage_weightage = False
        if self.stage:
            sta = self.stage
            self.stage_weightage = sta.stage_weightage

    @api.depends('stage_weightage', 'work_weightage', 'progress')
    def compute_project_progress(self):
        for rec in self:
            rec.project_progress = ((rec.progress / 100) * (rec.work_weightage / 100) * (rec.stage_weightage / 100)) * 100

    def action_complete(self):
        if self.progress != 100:
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'complete.confirm.wiz',
                'name': _("Confirmation"),
                'target': 'new',
                'view_type': 'form',
                'view_mode': 'form',
                'context': {'res_id': self.id}
            }
        else:
            res = self.write({'state': 'complete'})
            return res

    def create_subtask(self):
        context = {'default_parent_task': self.id}
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'create.subtask.wiz',
            'name': _("Create Subtasks"),
            "context": context,
            'target': 'new',
            'view_type': 'form',
            'view_mode': 'form',
        }

    def product_usage(self):
        # context = {'default_parent_task': self.id}
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'product.usage.wiz',
            'name': _("Create Product Usage"),
            # "context": context,
            'target': 'new',
            'view_type': 'form',
            'view_mode': 'form',
        }


class ConsumedMaterials(models.Model):
    _name = 'consumed.material'
    _description = "Consumed Materials"
    _order = 'sequence'
    _check_company_auto = True

    consumed_id = fields.Many2one('project.task', string='Work Order')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date = fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product', compute="_compute_group_of_product")
    product_id = fields.Many2one('product.product', string='Product',
                                 check_company=True, tracking=True, required=True)
    company_id = fields.Many2one(related='consumed_id.company_id', string='Company', readonly=True)
    description = fields.Text('Description', tracking=True, required=True)
    quantity = fields.Float(string="Quantity Used", default=0.00)
    quantity_left = fields.Float(string="Quantity Left", default=0.00)
    budget_quantity = fields.Float(string="Budget Quantity", default=0.00)
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')

    @api.depends('consumed_id.consumed_material_ids', 'consumed_id.consumed_material_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.consumed_id.consumed_material_ids:
                no += 1
                l.sr_no = no

    @api.model
    @api.constrains('quantity')
    def _check_quantity(self):
        for material in self:
            if not material.quantity > 0.00:
                raise ValidationError(
                    _('Quantity of material consumed must be greater than 0.')
                )

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.description = self.product_id.name
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False

    @api.onchange('product_id')
    def _compute_group_of_product(self):
        if self.product_id:
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.group_of_product = False


class ConsumedSubcon(models.Model):
    _name = 'consumed.subcon'
    _description = "Consumed Subcon"
    _order = 'sequence'
    _check_company_auto = True

    consumed_id = fields.Many2one('project.task', string='Work Order')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date = fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product', compute="_compute_group_of_product")
    product_id = fields.Many2one('product.product', string='Product',
                                 check_company=True, tracking=True, required=True)
    company_id = fields.Many2one(related='consumed_id.company_id', string='Company', readonly=True)
    description = fields.Text('Description', tracking=True, required=True)
    quantity = fields.Float(string="Quantity Used", default=0.00)
    quantity_left = fields.Float(string="Quantity Left", default=0.00)
    budget_quantity = fields.Float(string="Budget Quantity", default=0.00)
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')

    @api.depends('consumed_id.consumed_subcon_ids', 'consumed_id.consumed_subcon_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.consumed_id.consumed_subcon_ids:
                no += 1
                l.sr_no = no

    @api.model
    @api.constrains('quantity')
    def _check_quantity(self):
        for material in self:
            if not material.quantity > 0.00:
                raise ValidationError(
                    _('Quantity of material consumed must be greater than 0.')
                )

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.description = self.product_id.name
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False

    @api.onchange('product_id')
    def _compute_group_of_product(self):
        if self.product_id:
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.group_of_product = False


class ConsumedEquipment(models.Model):
    _name = 'consumed.equipment'
    _description = "Consumed Equipment"
    _order = 'sequence'
    _check_company_auto = True

    consumed_id = fields.Many2one('project.task', string='Work Order')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date = fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product', compute="_compute_group_of_product")
    product_id = fields.Many2one('product.product', string='Product',
                                 check_company=True, tracking=True, required=True)
    company_id = fields.Many2one(related='consumed_id.company_id', string='Company', readonly=True)
    description = fields.Text('Description', tracking=True, required=True)
    quantity = fields.Float(string="Quantity Used", default=0.00)
    quantity_left = fields.Float(string="Quantity Left", default=0.00)
    budget_quantity = fields.Float(string="Budget Quantity", default=0.00)
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')

    @api.depends('consumed_id.consumed_equipment_ids', 'consumed_id.consumed_equipment_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.consumed_id.consumed_equipment_ids:
                no += 1
                l.sr_no = no

    @api.model
    @api.constrains('quantity')
    def _check_quantity(self):
        for material in self:
            if not material.quantity > 0.00:
                raise ValidationError(
                    _('Quantity of material consumed must be greater than 0.')
                )

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.description = self.product_id.name
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False

    @api.onchange('product_id')
    def _compute_group_of_product(self):
        if self.product_id:
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.group_of_product = False


class ConsumedLabour(models.Model):
    _name = 'consumed.labour'
    _description = "Consumed Labour"
    _order = 'sequence'
    _check_company_auto = True

    consumed_id = fields.Many2one('project.task', string='Work Order')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date = fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product', compute="_compute_group_of_product")
    product_id = fields.Many2one('product.product', string='Product',
                                 check_company=True, tracking=True, required=True)
    company_id = fields.Many2one(related='consumed_id.company_id', string='Company', readonly=True)
    description = fields.Text('Description', tracking=True, required=True)
    quantity = fields.Float(string="Quantity Used", default=0.00)
    quantity_left = fields.Float(string="Quantity Left", default=0.00)
    budget_quantity = fields.Float(string="Budget Quantity", default=0.00)
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')

    @api.depends('consumed_id.consumed_labour_ids', 'consumed_id.consumed_labour_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.consumed_id.consumed_labour_ids:
                no += 1
                l.sr_no = no

    @api.model
    @api.constrains('quantity')
    def _check_quantity(self):
        for material in self:
            if not material.quantity > 0.00:
                raise ValidationError(
                    _('Quantity of material consumed must be greater than 0.')
                )

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.description = self.product_id.name
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False

    @api.onchange('product_id')
    def _compute_group_of_product(self):
        if self.product_id:
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.group_of_product = False


class ConsumedOverhead(models.Model):
    _name = 'consumed.overhead'
    _description = "Consumed Overhead"
    _order = 'sequence'
    _check_company_auto = True

    consumed_id = fields.Many2one('project.task', string='Work Order')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date = fields.Date(string="Date", default=date.today())
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product', compute="_compute_group_of_product")
    product_id = fields.Many2one('product.product', string='Product',
                                 check_company=True, tracking=True, required=True)
    company_id = fields.Many2one(related='consumed_id.company_id', string='Company', readonly=True)
    description = fields.Text('Description', tracking=True, required=True)
    quantity = fields.Float(string="Quantity Used", default=0.00)
    quantity_left = fields.Float(string="Quantity Left", default=0.00)
    budget_quantity = fields.Float(string="Budget Quantity", default=0.00)
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure')

    @api.depends('consumed_id.consumed_overhead_ids', 'consumed_id.consumed_overhead_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.consumed_id.consumed_overhead_ids:
                no += 1
                l.sr_no = no

    @api.model
    @api.constrains('quantity')
    def _check_quantity(self):
        for material in self:
            if not material.quantity > 0.00:
                raise ValidationError(
                    _('Quantity of material consumed must be greater than 0.')
                )

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.description = self.product_id.name
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False

    @api.onchange('product_id')
    def _compute_group_of_product(self):
        if self.product_id:
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.group_of_product = False


class ProjectSubtasks(models.Model):
    _name = 'project.subtask'
    _description = "Subtasks"
    _order = 'sequence'

    subtask_id = fields.Many2one('project.task', string='Work Order', ondelete='cascade')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    name = fields.Char(string="Subtask", required="1")
    description = fields.Text(string="Subtask Description")
    assigned_to = fields.Many2one('res.users', string="PIC")
    assigned_date = fields.Datetime(string="Assigned Date")
    planned_hour = fields.Float(string="Planned Hours")
    actual_hour = fields.Float(string="Actual Hours")
    remaining_hour = fields.Float(string="Remaining Hours")
    progress_completion = fields.Float(string="Progress Completion (%)")

    @api.depends('subtask_id.subtask_ids', 'subtask_id.subtask_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.subtask_id.subtask_ids:
                no += 1
                l.sr_no = no

    # @api.onchange('actual_hour', 'planned_hour')
    # def _progress_and_remaining_hours(self):
    #    for line in self:
    #        if line.planned_hour != 0 and line.actual_hour != 0: 
    #            line.remaining_hour = (line.planned_hour - line.actual_hour)
    #            line.progress_completion = (line.actual_hour/line.planned_hour) * 100
    #        else:
    #            line.progress_completion = 0.0

    # @api.model
    # @api.constrains('actual_hour')
    # def _check_actual_hour(self):
    #    for line in self:
    #        if not line.actual_hour < 1:
    #            raise ValidationError(
    #                _('Actual Hours must be greater than 0.')
    #            )


class ConsumedHistory(models.Model):
    _name = 'consumed.history'
    _description = "Consumed History"
    _order = 'sequence'

    consumed_id = fields.Many2one('project.task', string='Work Order', ondelete='cascade')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    usage_id = fields.Many2one('stock.scrap.request', string='Work Order', ondelete='cascade')
    date_used = fields.Date(string="Date")
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
    product_id = fields.Many2one('product.product', string='Product',check_company=True, tracking=True, required=True)
    quantity = fields.Float(string="Quantity")
    company_id = fields.Many2one(related='consumed_id.company_id', string='Company', readonly=True)
    material_type = fields.Selection([('material','Material'),('subcon','Subcon'),('equipment','Equipment'),('labour','Labour'),('overhead','Overhead')], string = "Material Type")


class ConsumedMaterialHistory(models.Model):
    _name = 'consumed.material.history'
    _description = "Consumed Material History"
    _order = 'sequence'

    consumed_id = fields.Many2one('project.task', string='Work Order', ondelete='cascade')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    usage_id = fields.Many2one('stock.scrap.request', string='Work Order', ondelete='cascade')
    date_used = fields.Date(string="Date")
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
    product_id = fields.Many2one('product.product', string='Product',check_company=True, tracking=True, required=True)
    quantity = fields.Float(string="Quantity")
    company_id = fields.Many2one(related='consumed_id.company_id', string='Company', readonly=True)


class ConsumedSubconHistory(models.Model):
    _name = 'consumed.subcon.history'
    _description = "Consumed Subcon History"
    _order = 'sequence'

    consumed_id = fields.Many2one('project.task', string='Work Order', ondelete='cascade')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    usage_id = fields.Many2one('stock.scrap.request', string='Work Order', ondelete='cascade')
    date_used = fields.Date(string="Date")
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
    product_id = fields.Many2one('product.product', string='Product',check_company=True, tracking=True, required=True)
    quantity = fields.Float(string="Quantity")
    company_id = fields.Many2one(related='consumed_id.company_id', string='Company', readonly=True)


class ConsumedEquipmentHistory(models.Model):
    _name = 'consumed.equipment.history'
    _description = "Consumed Equipment History"
    _order = 'sequence'

    consumed_id = fields.Many2one('project.task', string='Work Order', ondelete='cascade')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    usage_id = fields.Many2one('stock.scrap.request', string='Work Order', ondelete='cascade')
    date_used = fields.Date(string="Date")
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
    product_id = fields.Many2one('product.product', string='Product',check_company=True, tracking=True, required=True)
    quantity = fields.Float(string="Quantity")
    company_id = fields.Many2one(related='consumed_id.company_id', string='Company', readonly=True)


class ConsumedLabourHistory(models.Model):
    _name = 'consumed.labour.history'
    _description = "Consumed Labour History"
    _order = 'sequence'

    consumed_id = fields.Many2one('project.task', string='Work Order', ondelete='cascade')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    usage_id = fields.Many2one('stock.scrap.request', string='Work Order', ondelete='cascade')
    date_used = fields.Date(string="Date")
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
    product_id = fields.Many2one('product.product', string='Product',check_company=True, tracking=True, required=True)
    quantity = fields.Float(string="Quantity")
    company_id = fields.Many2one(related='consumed_id.company_id', string='Company', readonly=True)


class ConsumedOverheadHistory(models.Model):
    _name = 'consumed.overhead.history'
    _description = "Consumed Overhead History"
    _order = 'sequence'

    consumed_id = fields.Many2one('project.task', string='Work Order', ondelete='cascade')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    usage_id = fields.Many2one('stock.scrap.request', string='Work Order', ondelete='cascade')
    date_used = fields.Date(string="Date")
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
    product_id = fields.Many2one('product.product', string='Product',check_company=True, tracking=True, required=True)
    quantity = fields.Float(string="Quantity")
    company_id = fields.Many2one(related='consumed_id.company_id', string='Company', readonly=True)
