# -*- coding: utf-8 -*-
{
    'name': "Equip3 Construction Operation",

    'summary': """
        This module to manage actual vs budget, and all operation of construction management""",

    'description': """
        This module manages these features :
        - Cost Sheet
        - Work Order
        - Project Masterdata
    """,

    'author': "Hashmicro",
    'website': "http://www.hashmicro.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Construction',
    'version': '1.1.14',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail', 'abs_construction_management', 'project', 'equip3_construction_sales_operation',
                'bi_job_cost_estimate_customer', 'equip3_construction_masterdata', 'purchase','equip3_inventory_control', 'hr_timesheet'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'wizard/material_request_wiz_view.xml',
        'wizard/purchase_agreement_wiz_view.xml',
        'wizard/task_complete_confirm_wiz_view.xml',
        'wizard/product_usage_wiz_view.xml',
        'views/project_budget_view.xml',
        'views/job_cost_sheet_view.xml',
        'data/cost_sheet_sequence.xml',
        'views/construction_management_css_assets.xml',
        'views/job_cost_sheet_css.xml',
        'views/project_task_view.xml',
        'data/task_sequence.xml',
        'wizard/subtask_wiz_view.xml',
        'views/stock_scrap_request_inherit_view.xml',
    ],
}
