# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import material_request_wiz
from . import purchase_aggrement_wiz
from . import task_complete_confirm_wiz
from . import subtask_wiz
from . import product_usage_wiz
