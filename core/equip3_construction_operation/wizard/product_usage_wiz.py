from odoo import _, api, fields, models
from datetime import datetime
from odoo.exceptions import ValidationError


class ProductUsageWiz(models.TransientModel):
    _name = 'product.usage.wiz'
    _description = 'Create Product Usage'

    def product_usage(self):
        project_task = self.env['project.task'].browse(self.env.context.get('active_id'))
        product_usage = self.env['stock.scrap.request'].create({
            'project': project_task.project_id.id,
            'scrap_request_name': self.name,
            'material_type': self.material_type,
            'warehouse_id': self.warehouse.id,
            'responsible_id': self.responsible.id,
            'analytic_tag_ids': self.analytic_groups.ids,
            })

        return {
            'type': 'ir.actions.act_window',
            'name': 'Product Usage',
            'view_mode': 'form',
            'res_model': 'stock.scrap.request',
            'res_id' : product_usage.id,
            'target': 'current'
        }

    name = fields.Char('Name')
    material_type = fields.Selection([('material','Material'),('subcon','Subcon'),('equipment','Equipment'),('labour','Labour'),('overhead','Overhead')], string = "Material Type")
    warehouse = fields.Many2one('stock.warehouse','Warehouse')
    responsible = fields.Many2one('res.users','Responsible')
    analytic_groups = fields.Many2one('account.analytic.tag','Analytic Groups')

