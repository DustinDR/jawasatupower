from odoo import _, api, fields, models
from datetime import datetime
from odoo.exceptions import ValidationError


class CreateSubtaskWiz(models.TransientModel):
    _name = 'create.subtask.wiz'
    _description = 'Create Subtasks'

    def create_subtask(self):
        if self.parent_task:
            parent_task = self.parent_task
        else:
            parent_task = self.env['project.task'].browse(self.env.context.get('active_id'))

        list_subtask = []
        for sub in self.subtasks_ids:
            vals = {
                'project_id': parent_task.project_id.id,
                'partner_id': parent_task.partner_id.id,
                'stage': parent_task.stage.id,
                'name': sub.name,
                # 'description_pad':'8878',
                'assigned_to': sub.assigned_to.id,
                'is_subtask': True,
                'planned_hours': sub.planned_hour,
                'actual_start_date': sub.assigned_date,
            }
            self.env['project.task'].create(vals)
            list_subtask.append(
                (0, 0, {'subtask_id': parent_task and parent_task.id or False,
                        'name': sub.name,
                        'description': sub.description,
                        'assigned_to': sub.assigned_to and sub.assigned_to.id or False,
                        'assigned_date': sub.assigned_date,
                        'planned_hour': sub.planned_hour
                        }
                 ))
        self.parent_task.write({
            'subtask_ids': list_subtask,
        })
        if not self.subtasks_ids:
            raise ValidationError(_("Add at least one subtask to create."))
        return

    txt = fields.Text(string="Information", default="This wizard will create subtasks of current work order.")
    subtasks_ids = fields.One2many('create.subtask', 'subtask_id')
    parent_task = fields.Many2one('project.task', string="Work Order")


class SubtaskWiz(models.TransientModel):
    _name = 'create.subtask'
    _description = 'Create Subtasks'

    subtask_id = fields.Many2one('create.subtask.wiz', string='Subtask')
    name = fields.Char(string="Subtask")
    description = fields.Text(string="Subtask Description")
    assigned_to = fields.Many2one('res.users', string="PIC")
    assigned_date = fields.Datetime(string="Assigned Date", default=fields.Datetime.now)
    planned_hour = fields.Float(string="Planned Hours")
