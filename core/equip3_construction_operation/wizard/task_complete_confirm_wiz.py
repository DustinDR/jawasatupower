from odoo import _, api, fields, models
from datetime import datetime
from odoo.exceptions import ValidationError

class CompleteConfirmation(models.TransientModel):
    _name = 'complete.confirm.wiz'
    _description = 'Complete Confirmation Wizard'

    txt = fields.Text(string="Confirmation",default="Looks like the work order has not been completed.\nDo you want to finish it now?")

    def yes_button(self):
        res = self._context.get('res_id', False)
        if res:
            res_id = self.env['project.task'].browse(res)
            if res_id:
                res_id.write({'state': 'complete',
                             'progress': 100
                             })
        

    def no_button(self):
        res = self._context.get('res_id', False)
        if res:
            res_id = self.env['project.task'].browse(res)
            if res_id:
                res_id.write({'state': 'inprogress'
                             })
    