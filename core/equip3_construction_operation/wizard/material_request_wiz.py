from odoo import _, api, fields, models


class MaterialRequestWiz(models.TransientModel):
    _name = 'material.request.wiz'
    _description = 'Create Material Request Wizard'

    destination_warehouse = fields.Many2one('stock.warehouse', required=True, string="Destination warehouse")
    analytic_group = fields.Many2many('account.analytic.tag', required=True, string="Analytic group")
    type_of_mr = fields.Selection([
        ('material', 'Material'), ('labour', 'Labour'), ('assets', 'Assets'), ('overhead', 'Overhead')],
        string="Type of MR", required=True)

    def create_material_request_submit(self):
        Job_cost_sheet = self.env['job.cost.sheet'].browse(self.env.context.get('active_id'))
        product_ids = []
        if self.type_of_mr == 'material':
            for mat in Job_cost_sheet.material_ids:
                res = (0, 0, {
                    'project_scope': mat.project_scope.id,
                    'section': mat.section_name.id,
                    'variable': mat.variable_ref.id,
                    'product': mat.product_id.id,
                    'quantity': mat.product_qty,
                    'budget_quantity': mat.budgeted_qty_left,
                })
                product_ids.append(res)
        if self.type_of_mr == 'labour':
            for labour in Job_cost_sheet.material_labour_ids:
                res = (0, 0, {
                    'project_scope': labour.project_scope.id,
                    'section': labour.section_name.id,
                    'variable': labour.variable_ref.id,
                    'product': labour.product_id.id,
                    'quantity': labour.product_qty,
                    'budget_quantity': labour.budgeted_qty_left,
                })
                product_ids.append(res)

        if self.type_of_mr == 'overhead':
            for over in Job_cost_sheet.material_overhead_ids:
                res = (0, 0, {
                    'project_scope': over.project_scope.id,
                    'section': over.section_name.id,
                    'variable': over.variable_ref.id,
                    'product': over.product_id.id,
                    'quantity': over.product_qty,
                    'budget_quantity': over.budgeted_qty_left,
                })
                product_ids.append(res)

        if self.type_of_mr == 'assets':
            for assets in Job_cost_sheet.material_equipment_ids:
                res = (0, 0, {
                    'project_scope': assets.project_scope.id,
                    'section': assets.section_name.id,
                    'variable': assets.variable_ref.id,
                    'product': assets.product_id.id,
                    'quantity': assets.product_qty,
                    'budget_quantity': assets.budgeted_qty_left,
                })
                product_ids.append(res)

        product_lines = {
            'project': Job_cost_sheet.project_id.id,
            'job_cost_sheet': Job_cost_sheet.id,
            'destination_warehouse_id': self.destination_warehouse.id,
            'analytic_account_group_ids': self.analytic_group.ids,
            'schedule_date': self.write_date,
            'product_line': product_ids
        }
        self.env['material.request'].sudo().create(product_lines)



    @api.model
    def default_get(self, fields):
        Job_cost_sheet = self.env['job.cost.sheet'].browse(self.env.context.get('active_id'))
        res = super(MaterialRequestWiz, self).default_get(fields)
        account_ids = []
        for rec in Job_cost_sheet.account_tag_ids:
            account_ids.append(rec.id)
        res['analytic_group'] = [(6, 0, account_ids)]
        return res
