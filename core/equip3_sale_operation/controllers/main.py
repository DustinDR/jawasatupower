# -*- coding: utf-8 -*-

import base64
import io
import pytz
from odoo import http
from odoo.http import request
from werkzeug.utils import redirect
from datetime import datetime, time, timedelta
from pytz import timezone, UTC
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

class AttachmentController(http.Controller):

    @http.route(['/attachment/download/<int:attachment_id>'], type='http', auth="public", website=True)
    def download_attcahment(self, attachment_id=None, **post):
        attachment = request.env['ir.attachment'].sudo().search_read(
            [('id', '=', int(attachment_id))],
            ["name", "datas", "type", "res_model", "res_id", "type", "url", "expiry_date"]
        )
        if attachment:
            attachment = attachment[0]
            IrConfigParam = request.env['ir.config_parameter'].sudo()
            expired_date = IrConfigParam.get_param('expired_date', 1)
            expiry_date = attachment.get('expiry_date') + timedelta(hours=int(expired_date))
            timezone = pytz.timezone(request._context.get('tz') or 'UTC')
            local_date = pytz.UTC.localize(fields.Datetime.from_string(datetime.now()))
            local_convert_date = datetime.strptime(local_date.astimezone(timezone).strftime(DEFAULT_SERVER_DATETIME_FORMAT), DEFAULT_SERVER_DATETIME_FORMAT)
            expiry_local_date = pytz.UTC.localize(fields.Datetime.from_string(expiry_date))
            expiry_local_convert_date = datetime.strptime(expiry_local_date.astimezone(timezone).strftime(DEFAULT_SERVER_DATETIME_FORMAT), DEFAULT_SERVER_DATETIME_FORMAT)
            if local_convert_date > expiry_local_convert_date:
                return request.not_found()
        else:
            return redirect('/attachment/download/%s' % attachment_id)

        if attachment["type"] == "url":
            if attachment["url"]:
                return redirect(attachment["url"])
            else:
                return request.not_found()
        elif attachment["datas"]:
            data = io.BytesIO(base64.standard_b64decode(attachment["datas"]))
            return http.send_file(data, filename=attachment['name'], as_attachment=True)
        else:
            return request.not_found()
