# -*- coding: utf-8 -*-
{
    'name': "Equip3 Sale Operation",
    'summary': """
        Manage your quotations and sale orders activities""",
    'description': """
        This module manages these features :
        1. Quotation
        2. Quotation Revision
        3. Sale Order
        4. Sale Order Revision
        5. Quotation & SO Approval Matrix (Master and Transaction)
        6. Invoicing
        7. Shipment Single Delivery Address, Single Warehouse, Single Delivery Date
        8. Shipment Multi Delivery Address, Multi Warehouse, Multi Delivery Date
        9. Quotation, Sale Order, Invoice send by whatsapp
    """,
    'author': "Hashmicro",
    'category': 'Sales',
    'version': '1.5.22',
    'depends': ['sale_margin', 'web', 'branch', 
                'equip3_sale_accessright_setting',
                'bi_sale_purchase_discount_with_tax',
                'web_digital_sign',
                'sh_sale_credit_limit',
                'sale_stock_location_oin',
                'sale_quotation_number',
                'sale_order_terms_knk',
                'sh_so_po',
                'app_sale_superbar',
                'acrux_chat_sale', 
                'quotation_revision',
                'ss_whatsapp_connector',
                'contract_recurring_invoice_analytic',
                'sr_manual_currency_exchange_rate',
                'dynamic_product_bundle'
            ],
    'data': [
        'security/ir.model.access.csv',
        'security/so_multipule_do_security.xml',
        'data/ir_cron.xml',
        'data/ir_sequence_data.xml',
        'data/sale_order_template.xml',
        'wizards/approval_matrix_sale_reject_views.xml',
        'wizards/MessageWizard.xml',
        'wizards/sale_select_product_view.xml',
        'views/sale_order_views.xml',
        'views/sale_sequence_views.xml',
        'views/account_move_views.xml',
        'views/res_setting_config_view.xml',
        'views/approval_matrix_sale_order_views.xml',
        'views/res_users_views.xml',
        'views/sale_terms_and_conditions_views.xml',
        'views/sale_report.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
