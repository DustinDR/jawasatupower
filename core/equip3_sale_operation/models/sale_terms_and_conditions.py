from odoo import api, fields, models, _

class SaleTermsAndConditions(models.Model):
    _name = 'sale.terms.and.conditions'
    _description = "Sale Terms And Conditions"

    name = fields.Char(string="Name", required=True)
    description = fields.Text(string='Description', required=True)
    terms_and_conditions = fields.Html(string="Terms & Conditions")
