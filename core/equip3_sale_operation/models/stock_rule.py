
from odoo import SUPERUSER_ID, _, api, fields, models, registry


class StockRule(models.Model):
    _inherit = 'stock.rule'

    def _get_stock_move_values(self, product_id, product_qty, product_uom, location_id, name, origin, company_id, values):
        res = super(StockRule, self)._get_stock_move_values(product_id, product_qty, product_uom, location_id, name, origin, company_id, values)
        if values.get('multiple_do'):
            warehouse_id = self.env['stock.warehouse'].browse([values.get('line_warehouse_id')])
            location_obj = self.env['stock.location']
            store_location_id = warehouse_id.view_location_id.id
            addtional_ids = location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')], order="id")
            res.update({
                'warehouse_id': warehouse_id.id,
                'picking_type_id': warehouse_id.out_type_id.id,
                'location_id': addtional_ids and addtional_ids[0].id,
            })
            if values.get('delivery_address_id'):
                res.update({
                    'partner_id': values.get('delivery_address_id')
                })
        return res
