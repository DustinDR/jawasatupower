# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import sale_order_deduction
from . import account_move
from . import approval_matrix_sale_order
from . import res_config_settings
from . import sale_order
from . import stock_rule
from . import sale_terms_and_conditions
from . import stock_picking