# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import SUPERUSER_ID, _, api, fields, models
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare, float_round
from odoo.exceptions import ValidationError, UserError
import re

class SaleOrderLine(models.Model):

    _name = 'sale.order.line'
    _inherit = ['sale.order.line','portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']

    customer_id = fields.Many2one(related='order_id.partner_id', string='Customer')
    user_id = fields.Many2one(related='order_id.user_id', string='Salesperson')
    sequence_no = fields.Char(related='order_id.name', string="Sequence Number")
    product_id = fields.Many2one(
        'product.product', string='Product', domain="[('sale_ok', '=', True), '|', ('company_id', '=', False), ('company_id', '=', company_id)]",
        change_default=True, ondelete='restrict', check_company=True,tracking=True)  # Unrequired company
    product_uom_qty = fields.Float(string='Quantity', digits='Product Unit of Measure', required=True, default=1.0,tracking=True)
    price_unit = fields.Float('Unit Price', required=True, digits='Product Price', default=0.0,tracking=True)
    customer_lead = fields.Float(
        'Lead Time', required=True, default=0.0,
        help="Number of days between the order confirmation and the shipping of the products to the customer",tracking=True)
    company_id = fields.Many2one(related='order_id.company_id', string='Company', store=True, readonly=True, index=True)
    name = fields.Text(string='Description', required=True,tracking=True)

    last_sale_price = fields.Float(string="Last Sale Price", compute='calculate_last_price', store=True)
    last_customer_sale_price = fields.Float(string="Last Sale Price Of Customer", compute='calculate_last_price_customer', store=True)
    account_tag_ids = fields.Many2many('account.analytic.tag', 'account_analytic_tag_order_line_rel', 'sale_line_id', 'tag_id', string="Analytic Group")
    analytic_account_id = fields.Many2one('account.analytic.account', string="Analytic Account")
    net_amount = fields.Monetary(string='Net Amount', compute='_get_net_amount', store=True)
    location_dest_id = fields.Many2one('stock.location', string="Delivery To")
    line_warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse')
    delivery_address_id = fields.Many2one('res.partner', string="Delivery Address")
    multiple_do_date = fields.Date(string='Delivery Date', index=True)
    multi_discount = fields.Char('Multi Discount')
    discount_method = fields.Selection([('fix', 'Fixed'), ('per', 'Percentage')], 'Discount Method', default='fix')
    filter_delivery_address_id = fields.Many2many('res.partner', compute="_compute_partner_address", string="Partner Address")

    def get_disocunt(self,percentage,amount):
        new_amount = (percentage * amount)/100
        return (amount - new_amount)

    @api.onchange('multi_discount')
    def _onchange_multi_discount(self):
        if self.multi_discount:
            amount = 100
            splited_discounts = self.multi_discount.split("+")
            for disocunt in splited_discounts:
                try:
                    amount = self.get_disocunt(float(disocunt),amount)
                except ValueError:
                    raise ValidationError("Please Enter Valid Multi Discount")
            self.discount_amount = 100 - amount
        else:
            self.discount_amount = 0

    @api.onchange('product_uom_qty')
    def set_default_multi_disc(self):
        for res in self:
            if res.order_id.discount_type == 'global':
                res.discount_method = res.order_id.discount_method
                res.discount_amount = res.order_id.discount_amount
                if res.order_id.multilevel_disc:
                    res.multi_discount = res.order_id.multi_discount

    @api.onchange('product_uom_qty')
    def set_address(self):
        deliv = 0
        inv = 0
        if self.order_id.partner_id.child_ids:
            for line in self.order_id.partner_id.child_ids:
                if line.type == 'delivery':
                    deliv += 1
            if deliv < 1:
                self.delivery_address_id = self.order_id.partner_id.id


    @api.onchange('product_uom_qty')
    def set_account_group(self):
        for res in self:
            res.account_tag_ids = res.order_id.account_tag_ids
            if res.order_id.is_single_warehouse:
                res.line_warehouse_id = res.order_id.warehouse_id
            if res.order_id.is_single_delivery_date:
                res.multiple_do_date = res.order_id.commitment_date

    @api.depends('price_subtotal', 'product_uom_qty', 'price_reduce_taxinc')
    def _get_net_amount(self):
        for res in self:
            res.net_amount = res.price_reduce_taxinc * res.product_uom_qty

    @api.model
    def default_get(self, fields):
        res = super(SaleOrderLine, self).default_get(fields)
        if self._context:
            context_keys = self._context.keys()
            next_sequence = 1
            if 'order_line' in context_keys:
                if len(self._context.get('order_line')) > 0:
                    next_sequence = len(self._context.get('order_line')) + 1
            res.update({'sale_line_sequence': next_sequence})
        return res

    sale_line_sequence = fields.Char(string='No')

    @api.onchange('product_id')
    def product_id_change(self):
        if self.customer_id:
            partner_delivery_ids = self.env['res.partner'].search([('parent_id', '=', self.customer_id.id), ('type', '=', 'delivery')], limit=1).ids
            if partner_delivery_ids:
                self.delivery_address_id = partner_delivery_ids[0]
            else:
                self.delivery_address_id = self.customer_id.id

        res = super(SaleOrderLine, self).product_id_change()
        name = self.product_id.name
        if self.product_id.description_sale:
            name = name + " " + self.product_id.description_sale
        self.name = name
        self._compute_partner_address()
        return res

    def _compute_partner_address(self):
        for record in self:
            partner_ids = record.customer_id.child_ids.filtered(lambda r: r.type == 'delivery')
            if record.customer_id and record.customer_id.child_ids and partner_ids:
                record.filter_delivery_address_id = [(6, 0, partner_ids.ids)]
            else:
                record.filter_delivery_address_id = [(6, 0, record.customer_id.ids)]


    @api.depends('product_id')
    def calculate_last_price(self):
        for record in self:
            if isinstance(record.id, models.NewId):
                sale_order_line_id = self.search([('state','in',('sale','done')),('product_id', '=', record.product_id.id)], limit=1, order="id desc")
            else:
                sale_order_line_id = self.search([('state','in',('sale','done')),('product_id', '=', record.product_id.id), ('id', '!=', record.id)], limit=1, order="id desc")
            record.last_sale_price = sale_order_line_id.price_unit

    @api.depends('product_id', 'order_id', 'order_id.partner_id')
    def calculate_last_price_customer(self):
        for record in self:
            if isinstance(record.id, models.NewId):
                sale_order_line_id = self.search([('state','in',('sale','done')),('product_id', '=', record.product_id.id), ('order_id.partner_id', '=', record.order_id.partner_id.id)], limit=1, order="id desc")
            else:
                sale_order_line_id = self.search([('state','in',('sale','done')),('product_id', '=', record.product_id.id), ('order_id.partner_id', '=', record.order_id.partner_id.id), ('id', '!=', record.id)], limit=1, order="id desc")
            record.last_customer_sale_price = sale_order_line_id.price_unit

    def _action_launch_stock_rule(self, previous_product_uom_qty=False):
        """
        Launch procurement group run method with required/custom fields genrated by a
        sale order line. procurement group will launch '_run_pull', '_run_buy' or '_run_manufacture'
        depending on the sale order line product rule.
        """
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        if self.user_has_groups('equip3_sale_operation.group_multi_do'):
            temp_list = []
            line_list_vals = []
            for record in self:
                if {'line_warehouse_id': record.line_warehouse_id.id, 
                    'delivery_address_id': record.delivery_address_id.id, 
                    'delivery_date': record.multiple_do_date,
                    'product_id': record.product_id.id} in temp_list:
                    filter_line = list(filter(lambda r:r.get('line_warehouse_id') == record.line_warehouse_id.id and 
                                                       r.get('delivery_address_id') == record.delivery_address_id.id 
                                                    and r.get('delivery_date') == record.multiple_do_date and 
                                                    r.get('product_id') == record.product_id.id, line_list_vals))
                    if filter_line:
                        filter_line[0]['lines'].append(record)
                else:
                    temp_list.append({
                        'line_warehouse_id': record.line_warehouse_id.id,
                        'delivery_address_id': record.delivery_address_id.id,
                        'delivery_date': record.multiple_do_date,
                        'product_id': record.product_id.id
                    })
                    line_list_vals.append({
                        'line_warehouse_id': record.line_warehouse_id.id,
                        'delivery_address_id': record.delivery_address_id.id,
                        'delivery_date': record.multiple_do_date,
                        'lines': [record]
                    })
            for value in line_list_vals:
                procurements = []
                group_id = False
                for line in value.get('lines'):
                    if line.state != 'sale' or not line.product_id.type in ('consu','product'):
                        continue
                    qty = line._get_qty_procurement(previous_product_uom_qty)
                    if float_compare(qty, line.product_uom_qty, precision_digits=precision) >= 0:
                        continue
                    group_vals = line._prepare_procurement_group_vals()
                    if not group_id:
                        group_id = line._get_procurement_group()
                        # if not group_id:
                        group_id = self.env['procurement.group'].create(group_vals)
                    line.order_id.procurement_group_id = group_id
                    # else:
                    #     # In case the procurement group is already created and the order was
                    #     # cancelled, we need to update certain values of the group.
                    #     updated_vals = {}
                    #     if group_id.partner_id != line.order_id.partner_shipping_id:
                    #         updated_vals.update({'partner_id': line.order_id.partner_shipping_id.id})
                    #     if group_id.move_type != line.order_id.picking_policy:
                    #         updated_vals.update({'move_type': line.order_id.picking_policy})
                    #     if updated_vals:
                    #         group_id.write(updated_vals)

                    values = line._prepare_procurement_values(group_id=group_id)
                    values.update({
                        'line_warehouse_id': value.get('line_warehouse_id'),
                        'delivery_address_id': value.get('delivery_address_id'),
                        'delivery_date': value.get('delivery_date'),
                        'date_planned': value.get('delivery_date'),
                        'date_deadline': value.get('delivery_date'),
                        'multiple_do': True,
                        })
                    product_qty = line.product_uom_qty - qty

                    line_uom = line.product_uom
                    quant_uom = line.product_id.uom_id
                    product_qty, procurement_uom = line_uom._adjust_uom_quantities(product_qty, quant_uom)
                    procurements.append(self.env['procurement.group'].Procurement(
                        line.product_id, product_qty, procurement_uom,
                        line.order_id.partner_shipping_id.property_stock_customer,
                        line.name, line.order_id.name, line.order_id.company_id, values))
                if procurements:
                    self.env['procurement.group'].run(procurements)
            return True
        else:
            temp_list = []
            line_list_vals = []
            for record in self:
                if {'line_warehouse_id': record.line_warehouse_id.id} in temp_list:
                    filter_line = list(filter(lambda r:r.get('line_warehouse_id') == record.line_warehouse_id.id, line_list_vals))
                    if filter_line:
                        filter_line[0]['lines'].append(record)
                else:
                    temp_list.append({
                        'line_warehouse_id': record.line_warehouse_id.id,
                    })
                    line_list_vals.append({
                        'line_warehouse_id': record.line_warehouse_id.id,
                        'lines': [record]
                    })
            for value in line_list_vals:
                procurements = []
                group_id = False
                for line in value.get('lines'):
                    if line.state != 'sale' or not line.product_id.type in ('consu','product'):
                        continue
                    qty = line._get_qty_procurement(previous_product_uom_qty)
                    if float_compare(qty, line.product_uom_qty, precision_digits=precision) >= 0:
                        continue
                    group_vals = line._prepare_procurement_group_vals()
                    if not group_id:
                        group_id = line._get_procurement_group()
                        # if not group_id:
                        group_id = self.env['procurement.group'].create(group_vals)
                    line.order_id.procurement_group_id = group_id
                    # else:
                    #     # In case the procurement group is already created and the order was
                    #     # cancelled, we need to update certain values of the group.
                    #     updated_vals = {}
                    #     if group_id.partner_id != line.order_id.partner_shipping_id:
                    #         updated_vals.update({'partner_id': line.order_id.partner_shipping_id.id})
                    #     if group_id.move_type != line.order_id.picking_policy:
                    #         updated_vals.update({'move_type': line.order_id.picking_policy})
                    #     if updated_vals:
                    #         group_id.write(updated_vals)

                    values = line._prepare_procurement_values(group_id=group_id)
                    values.update({
                        'line_warehouse_id': value.get('line_warehouse_id'),
                        'multiple_do': True,
                    })
                    product_qty = line.product_uom_qty - qty

                    line_uom = line.product_uom
                    quant_uom = line.product_id.uom_id
                    product_qty, procurement_uom = line_uom._adjust_uom_quantities(product_qty, quant_uom)
                    procurements.append(self.env['procurement.group'].Procurement(
                        line.product_id, product_qty, procurement_uom,
                        line.order_id.partner_shipping_id.property_stock_customer,
                        line.name, line.order_id.name, line.order_id.company_id, values))
                if procurements:
                    self.env['procurement.group'].run(procurements)
            return True
            # return super(SaleOrderLine,self)._action_launch_stock_rule(previous_product_uom_qty=False)
class SaleOrderTemplate(models.Model):
    _inherit = 'sale.order.template'

    customer_ids = fields.Many2many('res.partner', 'order_template_partner_rel','partner_id', 'template_id', string="Customer")


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    order_line_count = fields.Integer(string="Order Line", compute='order_line_calc', store=True, tracking=True)
    date_order = fields.Datetime(default=fields.Datetime.now, tracking=True)
    validity_date = fields.Date(string='Expiration', readonly=True, copy=False,tracking=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},)
    payment_term_id = fields.Many2one(
    'account.payment.term', string='Payment Terms', check_company=True,  tracking=True , # Unrequired company
    domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]",)
    sale_order_template_id = fields.Many2one(
        'sale.order.template', 'Quotation Template',
        readonly=True, check_company=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        domain="['|',('customer_ids','=', False), ('customer_ids','in', partner_id)]",tracking=True)
    sale_state = fields.Selection([
                        ('pending', 'Pending'),
                        ('in_progress', 'In Progress'),
                        ('done', 'Done'),
                        ('cancel', 'Cancelled')
                ], string="Sale State", tracking=True)
    delivered_state = fields.Selection([
        ('pending', 'Pending Delivery'),
        ('partially', 'Partially Delivered'),
        ('fully', 'Fully Delivered')
    ], string="Delivered State", tracking=True)
    # ('partially', 'Partially Delivery'),
    # ('fully', 'Fully Delivery')
    state = fields.Selection(selection_add=[
            ('waiting_for_approval', 'Waiting For Sale Order Approval'),
            ('quotation_approved', 'Quotation Approved'),
            ('reject', 'Quotation Rejected'),
            ('revised', 'Order Revised'),
            ('sent',)
            ])
    sale_state_1 = fields.Selection(related='sale_state')
    state_2 = fields.Selection(related='state')
    revised_state = fields.Selection(related='state')
    approval_matrix_state = fields.Selection(related='state')
    approval_matrix_state_1 = fields.Selection(related='state')
    branch_id = fields.Many2one('res.branch', domain="[('company_id', '=', company_id)]")
    account_tag = fields.Many2one('account.analytic.tag', string="Account Analytic Group")
    account_tag_ids = fields.Many2many('account.analytic.tag', 'account_analytic_tag_order_rel', 'sale_id', 'tag_id', string="Analytic Group")
    analytic_account_id = fields.Many2one('account.analytic.account', string="Analytic Account")
    approving_matrix_sale_id = fields.Many2many('approval.matrix.sale.order', string="Sale Order Approval Matrix", compute='_compute_approving_customer_matrix', store=True)
    approved_matrix_ids = fields.One2many('approval.matrix.sale.order.lines', 'order_id', store=True, string="Approved Matrix", compute='_compute_approving_matrix_lines')
    is_customer_approval_matrix = fields.Boolean(string="Custome Matrix", store=False, compute='_compute_is_customer_approval_matrix')
    hide_proforma = fields.Boolean(string="Customer Limit Approval Matrix", store=False, compute='_compute_is_customer_approval_matrix')
    is_approval_matrix_filled = fields.Boolean(string="Custome Matrix", store=False, compute='_compute_approval_matrix_filled')
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button', store=False)
    approval_matrix_line_id = fields.Many2one('approval.matrix.sale.order.lines', string='Sale Approval Matrix Line', compute='_get_approve_button', store=False)
    is_quotation_cancel = fields.Boolean(string='Is Quotation Cancel', default=False)

    company_id = fields.Many2one(readonly=True)
    deduction_ids = fields.One2many('sale.order.deduction', 'sale_id', string='Deduction Lines')
    analytic_accounting = fields.Boolean("Analyic Account", compute="get_analytic_accounting", store=True)
    terms_conditions_id = fields.Many2one('sale.terms.and.conditions', string='Terms and Conditions')
    group_multi_do = fields.Boolean(string="Multiple Delivery Address?", compute='_compute_group_multi_do', store=False)
    is_single_warehouse = fields.Boolean(string="Single Warehouse", default=True)
    is_single_delivery_date = fields.Boolean(string="Single Delivery Date", default=True)
    commitment_date = fields.Datetime('Delivery Date', copy=False, default=fields.Datetime.now,
                                      states={'done': [('readonly', True)], 'cancel': [('readonly', True)]},
                                      help="This is the delivery date promised to the customer. "
                                           "If set, the delivery order will be scheduled based on "
                                           "this date rather than product lead times.")
    revised_sale_order_ids = fields.Many2many('sale.order', 'sale_order_revision_rel', 'sale_id', 'revision_id', string="Revised Sale Order")
    revised_sale_order_id = fields.Many2one('sale.order', string="Revised Sale Order")
    is_revision_so = fields.Boolean(string='Is Revision SO')
    is_print_report = fields.Boolean(string="Is Print Reports", store=False, compute='_compute_is_print_report')
    qty_delivered = fields.Integer("Delivery Qty", store=False, compute='_compute_delivery_qty')
    count_delivered = fields.Integer("Delivery Count", store=False, compute='_compute_delivery_qty')
    multilevel_disc = fields.Boolean(string="Multi Level Discount", compute="_compute_multilevel_disc")
    multi_discount = fields.Char('Multi Discount')
    discount_method = fields.Selection([('fix', 'Fixed'), ('per', 'Percentage')], 'Discount Method', default='fix')
    date_confirm = fields.Datetime(string='Confirm Date', readonly=True, copy=False, help="Confirmation date of confirmed orders.")
    is_sale_order = fields.Boolean(string="Is Sale Order", default=True, compute='_compute_is_sale_order')
    partner_name = fields.Char(related='partner_id.name', readonly=True, store=True)

    def action_quotation_send(self):
        ''' Opens a wizard to compose an email, with relevant mail template loaded by default '''
        self.ensure_one()
        template_id = self._find_mail_template()
        lang = self.env.context.get('lang')
        template = self.env['mail.template'].browse(template_id)
        if template.lang:
            lang = template._render_lang(self.ids)[self.id]
        ctx = {
            'default_model': 'sale.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            'custom_layout': "mail.mail_notification_paynow",
            'proforma': self.env.context.get('proforma', False),
            'force_email': True,
            'model_description': self.with_context(lang=lang).type_name,
        }
        return {
            'name': 'Send Email',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(False, 'form')],
            'view_id': False,
            'target': 'new',
            'context': ctx,
        }

    @api.model
    def _compute_is_sale_order(self):
        # import wdb; wdb.set_trace()
        string = self.name
        cari = re.search("^SO", string)
        if cari:
            self.is_sale_order = True
        else:
            self.is_sale_order = False

    @api.onchange('sale_order_template_id')
    def onchange_sale_order_template_id(self):
        res = super(SaleOrder, self).onchange_sale_order_template_id()
        if self.order_line:
            for line in self.order_line:
                line.set_account_group()
            self.set_del_add_line()
            self.set_account_group_lines()
        return res

    @api.depends("state")
    def _compute_multilevel_disc(self):
        for res in self:
            res.multilevel_disc = self.env['ir.config_parameter'].sudo().get_param('multilevel_disc_sale')

    def get_disocunt(self,percentage,amount):
        new_amount = (percentage * amount)/100
        return (amount - new_amount)

    @api.onchange('multi_discount')
    def _onchange_multi_discount(self):
        if self.multi_discount:
            amount = 100
            splited_discounts = self.multi_discount.split("+")
            for disocunt in splited_discounts:
                try:
                    amount = self.get_disocunt(float(disocunt),amount)
                except ValueError:
                    raise ValidationError("Please Enter Valid Multi Discount")
            self.discount_amount = 100 - amount
        else:
            self.discount_amount = 0

    @api.onchange('discount_type', 'discount_amount', 'multi_discount', 'discount_method')
    def _set_discount_line(self):
        for res in self:
            if res.discount_amount:
                for line in res.order_line:
                    line.discount_amount = res.discount_amount
                    line.discount_method = res.discount_method
                    if res.multilevel_disc:
                        line.multi_discount = res.multi_discount

    @api.onchange('partner_id')
    def set_del_add_line(self):
        for res in self:
            if res.order_line:
                address = self.env['res.partner'].search([('type', '=', 'delivery'), ('parent_id', '=', res.partner_id.id)], limit=1)
                for line in res.order_line:
                    if address:
                        line.delivery_address_id = address.id
                    else:
                        line.delivery_address_id = res.partner_id.id
                    line._compute_partner_address()


    @api.depends('picking_ids')
    def _compute_delivery_qty(self):
        for res in self:
            if self.state == 'sale':
                for line in res.order_line:
                    res.qty_delivered += line.qty_delivered
                    res.count_delivered += line.product_uom_qty
            else:
                res.qty_delivered = 0
                res.count_delivered = 0

    @api.depends('state')
    def _compute_is_print_report(self):
        for record in self:
            record.is_print_report = False
            if record.state == "sale":
                record.is_print_report = True
            elif record.state == "draft" and not record.is_customer_approval_matrix:
                record.is_print_report = True
            elif record.state == "quotation_approved" and record.is_customer_approval_matrix:
                record.is_print_report = True
            if record.qty_delivered < 1:
                record.delivered_state = 'pending'
            elif 1 <= record.qty_delivered < record.count_delivered:
                record.delivered_state = 'partially'
            else:
                record.delivered_state = 'fully'

    def action_show_revisions(self):
        context = dict(self.env.context) or {}
        revision_id = self.search([('parent_saleorder_id', '=', self.id)], limit=1)
        context.update({'active_id': revision_id.id})
        return {
            'name': 'Revision Sale Orders',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'sale.order',
            'type': 'ir.actions.act_window',
            'domain': [('parent_saleorder_id', '=', self.id)],
            'context': context,
            'target': 'current',
        }

    def quotation_send_report(self):
        return self.env.ref('sale.action_report_saleorder').report_action(self)

    def proforma_invoice_send_report(self):
        return self.env.ref('sale.action_report_pro_forma_invoice').report_action(self)

    def _prepare_confirmation_values(self):
        return {
            'state': 'sale',
        }

    def so_revision_quote(self):
        for cur_rec in self:
            if cur_rec.is_revision_so:
                so_count = self.search([("revised_sale_order_id", '=', cur_rec.revised_sale_order_id.id)])
                split_name = self.name.split('/')
                if split_name[-1].startswith('R'):
                    split_name[-1] = 'R%d' % (len(so_count) + 1)
                else:
                    split_name.append('R%d' % (len(so_count) + 1))
                name = '/'.join(split_name)
            else:
                so_count = self.search([("revised_sale_order_id", '=', cur_rec.id)])
                name = _('%s/R%d') % (self.name, len(so_count) + 1)
            if cur_rec.is_revision_so:
                cur_id = self.revised_sale_order_id.id
            else:
                cur_id = self.id
            vals = {
                'name': name,
                'state': 'draft',
                'parent_saleorder_id': self.id,
                'revised_sale_order_id': cur_id,
                'is_revision_so': True
            }
            new_sale_id = cur_rec.copy(default=vals)
            cur_rec.write({'state': 'revised'})
            new_sale_id.write({
                'name': name,
                'origin': self.name,
            })
            if cur_rec.is_revision_so:
                new_sale_id.revised_sale_order_ids = [(6, 0, new_sale_id.revised_sale_order_id.ids + so_count.ids)]
            else:
                new_sale_id.revised_sale_order_ids = [(6, 0, self.ids)]

    @api.constrains('order_line')
    def _check_product_location(self):
        pass

    @api.constrains('order_line', 'order_line.line_warehouse_id',
                    'order_line.delivery_address_id', 'order_line.delivery_date')
    def check_sale_line(self):
        for order in self:
            temp_list = []
            line_list_vals = []
            if self.user_has_groups('equip3_sale_operation.group_multi_do'):
                for record in order.order_line:
                    if {'line_warehouse_id': record.line_warehouse_id.id,
                        'delivery_address_id': record.delivery_address_id.id,
                        'delivery_date': record.multiple_do_date,
                        'product_id': record.product_id.id} in temp_list:
                        raise ValidationError('You cannot add same Product, Warehouse, Delivery Address and Delivery Date Record Twice!')
                    else:
                        temp_list.append({
                            'line_warehouse_id': record.line_warehouse_id.id,
                            'delivery_address_id': record.delivery_address_id.id,
                            'delivery_date': record.multiple_do_date,
                            'product_id': record.product_id.id
                        })
            else:
                for record in order.order_line:
                    if {'line_warehouse_id': record.line_warehouse_id.id,
                        'product_id': record.product_id.id} in temp_list:
                        raise ValidationError('You cannot add same product with the same warehouse twice.')
                    else:
                        temp_list.append({
                            'line_warehouse_id': record.line_warehouse_id.id,
                            'product_id': record.product_id.id,
                        })


    def _compute_group_multi_do(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        for record in self:
            record.group_multi_do = IrConfigParam.get_param('group_multi_do', False)

    @api.onchange('commitment_date', 'is_single_delivery_date')
    def onchange_delivery_date(self):
        for record in self:
            if record.is_single_delivery_date and record.commitment_date:
                for line in record.order_line:
                    line.multiple_do_date = record.commitment_date

    @api.onchange('warehouse_id', 'is_single_warehouse')
    def onchange_destination_warehouse(self):
        for record in self:
            if record.is_single_warehouse and record.warehouse_id:
                for line in record.order_line:
                    line.line_warehouse_id = record.warehouse_id

    @api.onchange('terms_conditions_id')
    def _onchange_terms_conditions_id(self):
        self._compute_group_multi_do()
        if self.terms_conditions_id:
            self.note = self.terms_conditions_id.terms_and_conditions

    @api.onchange('account_tag_ids')
    def set_account_group_lines(self):
        self._compute_group_multi_do()
        for res in self:
            for line in res.order_line:
                line.account_tag_ids = res.account_tag_ids

    @api.depends('company_id')
    def get_analytic_accounting(self):
        for res in self:
            res.analytic_accounting = res.user_has_groups('analytic.group_analytic_tags')

    @api.model
    def default_get(self, fields):
        res = super(SaleOrder, self).default_get(fields)
        analytic_priority_ids = self.env['analytic.priority'].search([], order="priority")
        for analytic_priority in analytic_priority_ids:
            if analytic_priority.object_id == 'user' and self.env.user.analytic_tag_ids:
                res.update({
                    'account_tag_ids': [(6, 0, self.env.user.analytic_tag_ids.ids)]
                })
                break
            elif analytic_priority.object_id == 'branch' and self.env.user.branch_id.analytic_tag_ids:
                res.update({
                    'account_tag_ids': [(6, 0, self.env.user.branch_id.analytic_tag_ids.ids)]
                })
                break
        res['note'] = ""
        return res

    @api.depends('picking_ids')
    def _compute_picking_ids(self):
        res = super(SaleOrder, self)._compute_picking_ids()
        if self.user_has_groups('equip3_sale_operation.group_multi_do'):
            for record in self:
                procurement_group_id = self.env['procurement.group'].search([('sale_id', '=', record.id)])
                pickings = self.env['stock.picking'].search([('group_id', 'in', procurement_group_id.ids)])
                if pickings:
                    record.delivery_count = len(pickings)
        return res

    def action_view_delivery(self):
        '''
        This function returns an action that display existing delivery orders
        of given sales order ids. It can either be a in a list or in a form
        view, if there is only one delivery order to show.
        '''
        self = self.with_context(outgoing=False)
        if self.user_has_groups('equip3_sale_operation.group_multi_do'):
            action = self.env["ir.actions.actions"]._for_xml_id("stock.action_picking_tree_all")
            procurement_group_id = self.env['procurement.group'].search([('sale_id', '=', self.id)])
            pickings = self.env['stock.picking'].search([('group_id', 'in', procurement_group_id.ids)])
            if len(pickings) > 1:
                action['domain'] = [('id', 'in', pickings.ids)]
            elif pickings:
                form_view = [(self.env.ref('stock.view_picking_form').id, 'form')]
                if 'views' in action:
                    action['views'] = form_view + [(state,view) for state,view in action['views'] if view != 'form']
                else:
                    action['views'] = form_view
                action['res_id'] = pickings.id
            # Prepare the context.
            picking_id = pickings.filtered(lambda l: l.picking_type_id.code == 'outgoing')
            if picking_id:
                picking_id = picking_id[0]
            else:
                picking_id = pickings[0]
            action['context'] = dict(self._context, default_partner_id=self.partner_id.id, default_picking_type_id=picking_id.picking_type_id.id, default_origin=self.name, default_group_id=picking_id.group_id.id)
            return action
        else:
            return super(SaleOrder,self).action_view_delivery()

    @api.depends('approving_matrix_sale_id')
    def _compute_approval_matrix_filled(self):
        for record in self:
            record.is_approval_matrix_filled = False
            if record.approving_matrix_sale_id:
                record.is_approval_matrix_filled = True

    def _get_approve_button(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_name_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False

    def action_quotation_sent(self):
        if self.filtered(lambda so: so.state not in ('draft', 'quotation_approved')):
            raise UserError(_('Only draft orders can be marked as sent directly.'))
        for order in self:
            order.message_subscribe(partner_ids=order.partner_id.ids)
        self.write({'state': 'sent'})

    @api.onchange('partner_id')
    def _set_domain_partner_invoice_id(self):
        b = {}
        if self.partner_id:
            if self.sale_order_template_id:
                self.order_line = False
                self.sale_order_template_id = False
            partner_inv_ids = self.partner_id.child_ids.filtered(lambda r:r.type == 'invoice').ids
            partner_shipping_ids = self.partner_id.child_ids.filtered(lambda r:r.type == 'delivery').ids
            b = {'domain': {'partner_invoice_id': [('id', 'in', partner_inv_ids)], 'partner_shipping_id': [('id', 'in', partner_shipping_ids)]}}
        return b

    @api.onchange('name')
    def _onchange_sale_name(self):
        self._compute_is_customer_approval_matrix()
        self._compute_approval_matrix_filled()
        self._compute_group_multi_do()

    @api.depends('approving_matrix_sale_id')
    def _compute_approving_matrix_lines(self):
        data = [(5, 0, 0)]
        for record in self:
            if record.state == 'draft' and record.is_customer_approval_matrix:
                record.approved_matrix_ids = []
                counter = 1
                record.approved_matrix_ids = []
                for rec in record.approving_matrix_sale_id:
                    for line in rec.approver_matrix_line_ids:
                        data.append((0, 0, {
                            'sequence' : counter,
                            'user_name_ids' : [(6, 0, line.user_name_ids.ids)],
                            'minimum_approver' : line.minimum_approver,
                            'approval_type': rec.config,
                        }))
                        counter += 1
                record.approved_matrix_ids = data

    def write(self, vals):
        res = super(SaleOrder, self).write(vals)
        if self.recurring_interval and not self.recurring_rule_type:
            raise ValidationError('you have to fill in recurring type')
        return res

    @api.model
    def create(self, vals):
        if 'recurring_interval' in vals:
            if vals['recurring_interval'] > 0:
                if 'recurring_rule_type' in vals:
                    if not vals['recurring_rule_type']:
                        raise ValidationError('you have to fill in recurring type')
        res = super(SaleOrder, self).create(vals)
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        auto_mail = IrConfigParam.get_param('auto_mail', False)
        # self.env.ref('sale.model_sale_order_action_quotation_sent').unlink_action()
        if auto_mail:
            res.action_sale_send_mail()
        if not res.is_revision_so:
            res.state = 'draft'
            keep_name_so = IrConfigParam.get_param('keep_name_so', False)
            if keep_name_so:
                res.name = self.env['ir.sequence'].next_by_code('sale.order.quotation.order')
            else:
                res.name = self.env['ir.sequence'].next_by_code('sale.order.quotation')
        return res

    def order_confirm(self):
        res = super(SaleOrder, self).order_confirm()
        for record in self:
            IrConfigParam = self.env['ir.config_parameter'].sudo()
            auto_mail = IrConfigParam.get_param('auto_mail', False)
            if auto_mail:
                record.action_sale_send_mail()
        return res

    def action_sale_send_mail(self):
        template_id = self._find_mail_template()
        lang = self.env.context.get('lang')
        template = self.env['mail.template'].browse(template_id)
        if template.lang:
            lang = template._render_lang(self.ids)[self.id]

        if self.state != 'sale':
            subject = '%s Quotation (Ref %s)'%(self.company_id.name, self.name)
        else:
            subject = '%s Order (Ref %s)'%(self.company_id.name, self.name)

        body = self.env['mail.render.mixin']._render_template(template.body_html, 'sale.order', self.ids)[self.id]
        ctx = {
            'default_body': body,
            'default_subject': subject,
            'default_model': 'sale.order',
            'default_partner_ids': self.partner_id.ids,
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            'custom_layout': "mail.mail_notification_paynow",
            'proforma': self.env.context.get('proforma', False),
            'force_email': True,
            'model_description': self.with_context(lang=lang).type_name,
        }
        mail_compose_message_id = self.env['mail.compose.message'].with_context(ctx).create({})
        values = mail_compose_message_id.generate_email_for_composer(
            template.id, [self.id],
            ['subject', 'body_html', 'email_from', 'email_to', 'partner_to', 'email_cc',  'reply_to', 'attachment_ids', 'mail_server_id']
        )[self.id]
        attachment_ids = []
        Attachment = self.env['ir.attachment']
        for attach_fname, attach_datas in values.pop('attachments', []):
            data_attach = {
                'name': attach_fname,
                'datas': attach_datas,
                'res_model': 'mail.compose.message',
                'res_id': 0,
                'type': 'binary',  # override default_type from context, possibly meant for another model!
            }
            attachment_ids.append(Attachment.create(data_attach).id)
        mail_compose_message_id.attachment_ids = [(6, 0, attachment_ids)]
        # mail_compose_message_id.send_mail()
        mail_message_id = self.env['mail.message'].search([('res_id', '=', self.id), ('model', '=', 'sale.order')], limit=1)
        mail_message_id.write({'subject': subject})
        mail_message_id.res_id = 0
        if self.state != 'sale':
            self.message_post(body=_("Email has been sent to %s for Quotation information") % self.partner_id.name)
        else:
            self.message_post(body=_("Email has been sent to %s for Sale Order confirmation") % self.partner_id.name)

    @api.depends('partner_id')
    def _compute_is_customer_approval_matrix(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        is_customer_approval_matrix = IrConfigParam.get_param('is_customer_approval_matrix')
        is_customer_limit_matrix = IrConfigParam.get_param('is_customer_limit_matrix')
        for record in self:
            record.is_customer_approval_matrix = is_customer_approval_matrix
            if is_customer_limit_matrix or is_customer_approval_matrix and record.state != "quotation_approved":
                record.hide_proforma = True
            else:
                record.hide_proforma = False

    @api.depends('amount_total', 'margin', 'discount_amt', 'discount_amt_line')
    def _compute_approving_customer_matrix(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        is_total_amount = IrConfigParam.get_param('is_total_amount', False)
        is_margin_amount = IrConfigParam.get_param('is_margin_amount', False)
        is_discount_amount = IrConfigParam.get_param('is_discount_amount', False)
        total_sequence = IrConfigParam.get_param('total_sequence', 0)
        margin_sequence = IrConfigParam.get_param('margin_sequence', 0)
        discount_sequence = IrConfigParam.get_param('discount_sequence', 0)
        data = []
        if is_total_amount:
            data.insert(int(total_sequence) - 1, 'total_amt')
        if is_margin_amount:
            data.insert(int(margin_sequence) - 1, 'margin_amt')
        if is_discount_amount:
            data.insert(int(discount_sequence) - 1, 'discount_amt')
        for record in self:
            matrix_ids = []
            if record.is_customer_approval_matrix:
                record.approving_matrix_sale_id = False
                for sale_matrix_config in data:
                    if sale_matrix_config == 'total_amt':
                        matrix_id = self.env['approval.matrix.sale.order'].search([('config', '=', 'total_amt'),
                                    ('minimum_amt', '<=', record.amount_total),
                                    ('maximum_amt', '>=', record.amount_total), ('company_id', '=', record.company_id.id)], limit=1)
                        if matrix_id:
                            matrix_ids.append(matrix_id.id)
                    elif sale_matrix_config == 'margin_amt':
                        matrix_id = self.env['approval.matrix.sale.order'].search([('config', '=', 'margin_amt'),
                                    ('minimum_amt', '<=', record.margin), ('maximum_amt', '>=', record.margin),
                                    ('company_id', '=', record.company_id.id)], limit=1)
                        if matrix_id:
                            matrix_ids.append(matrix_id.id)
                    elif sale_matrix_config == 'discount_amt':
                        if record.discount_type == 'line':
                            matrix_id = self.env['approval.matrix.sale.order'].search([('config', '=', 'discount_amt'),
                                        ('minimum_amt', '<=', record.discount_amt_line),
                                        ('maximum_amt', '>=', record.discount_amt_line),
                                        ('company_id', '=', record.company_id.id)], limit=1)
                            if matrix_id:
                                matrix_ids.append(matrix_id.id)
                        else:
                            matrix_id = self.env['approval.matrix.sale.order'].search([('config', '=', 'discount_amt'),
                                        ('minimum_amt', '<=', record.discount_amt),
                                        ('maximum_amt', '>=', record.discount_amt),
                                        ('company_id', '=', record.company_id.id)], limit=1)
                            if matrix_id:
                                matrix_ids.append(matrix_id.id)
                record.approving_matrix_sale_id = [(6 ,0, matrix_ids)]
            else:
                record.approving_matrix_sale_id = False

    def action_confirm_approving(self):
        for record in self:
            record.write({'state': 'quotation_approved'})

    def action_request_for_approving(self):
        for record in self:
            record.action_request_for_approving_sale_matrix()

    def action_request_for_approving_sale_matrix(self):
        for record in self:
            action_id = self.env.ref('sale.action_quotations_with_onboarding')
            template_id = self.env.ref('equip3_sale_operation.email_template_internal_sale_order_approval')
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = base_url + '/web#id=' + str(record.id) + '&action='+ str(action_id.id) + '&view_type=form&model=sale.order'
            if record.approved_matrix_ids and len(record.approved_matrix_ids[0].user_name_ids) > 1:
                for approved_matrix_id in record.approved_matrix_ids[0].user_name_ids:
                    approver = approved_matrix_id
                    ctx = {
                        'email_from' : self.env.user.company_id.email,
                        'email_to' : approver.partner_id.email,
                        'approver_name' : approver.name,
                        'date': date.today(),
                        'url' : url,
                    }
                    template_id.with_context(ctx).send_mail(record.id, True)
            else:
                approver = record.approved_matrix_ids[0].user_name_ids[0]
                ctx = {
                    'email_from' : self.env.user.company_id.email,
                    'email_to' : approver.partner_id.email,
                    'approver_name' : approver.name,
                    'date': date.today(),
                    'url' : url,
                }
                template_id.with_context(ctx).send_mail(record.id, True)
            record.write({'state': 'waiting_for_approval'})

    def action_confirm_approving_matrix(self):
        for record in self:
            action_id = self.env.ref('sale.action_quotations_with_onboarding')
            template_id = self.env.ref('equip3_sale_operation.email_template_reminder_for_sale_order_approval')
            user = self.env.user
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = base_url + '/web#id=' + str(record.id) + '&action='+ str(action_id.id) + '&view_type=form&model=sale.order'
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_name_ids.ids and \
                    user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    if name != '':
                        name += "\n • %s: Approved" % (self.env.user.name)
                    else:
                        name += "• %s: Approved" % (self.env.user.name)

                    approval_matrix_line_id.write({
                        'last_approved': self.env.user.id, 'state_char': name,
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
                        approver_name = ' and '.join(approval_matrix_line_id.mapped('user_name_ids.name'))
                        next_approval_matrix_line_id = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
                        if next_approval_matrix_line_id and len(next_approval_matrix_line_id[0].user_name_ids) > 1:
                            for approving_matrix_line_user in next_approval_matrix_line_id[0].user_name_ids:
                                ctx = {
                                    'email_from' : self.env.user.company_id.email,
                                    'email_to' : approving_matrix_line_user.partner_id.email,
                                    'approver_name' : approving_matrix_line_user.name,
                                    'date': date.today(),
                                    'submitter' : approver_name,
                                    'url' : url,
                                }
                                template_id.sudo().with_context(ctx).send_mail(record.id, True)
                        else:
                            if next_approval_matrix_line_id and next_approval_matrix_line_id[0].user_name_ids:
                                ctx = {
                                    'email_from' : self.env.user.company_id.email,
                                    'email_to' : next_approval_matrix_line_id[0].user_name_ids[0].partner_id.email,
                                    'approver_name' : next_approval_matrix_line_id[0].user_name_ids[0].name,
                                    'date': date.today(),
                                    'submitter' : approver_name,
                                    'url' : url,
                                }
                                template_id.sudo().with_context(ctx).send_mail(record.id, True)
            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r:r.approved)):
                record.write({'state': 'quotation_approved'}) 

    def action_reject_approving_matrix(self):
        for record in self:
            return {
                    'type': 'ir.actions.act_window',
                    'name': 'Reject Reason',
                    'res_model': 'approval.matrix.sale.reject',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'new',
                }

    def action_set_quotation(self):
        for record in self:
            record.approved_matrix_ids.write({
                            'approved_users': False,
                            'last_approved': False,
                            'approved': False,
                            'feedback': False,
                            'time_stamp': False,
                            'state_char': False,
                            })
            record.write({'state': 'draft'})

    @api.model
    def _action_sale_order_cancel(self):
        today_date = date.today()
        sale_order_ids = self.search([('validity_date', '<', today_date), ('state', 'in', ('draft', 'sent'))])
        sale_order_ids.write({'sale_state': 'cancel', 'state': 'cancel'})

    def action_cancel(self):
        for record in self:
            if record.state == 'sale':
                # if any(picking_id.state != 'draft' for picking_id in record.picking_ids) or \
                #    (not record.picking_ids and record.invoice_ids):
                #     raise ValidationError(_("There is already Delivery Order or Invoice created."))

                # elif all(picking_id.state == 'draft' for picking_id in record.picking_ids) or \
                #    (not record.picking_ids and not record.invoice_ids):
                #     record.write({'sale_state': 'cancel', 'state': 'cancel'})

                if record.picking_ids and not record.invoice_ids and \
                    any(picking_id.state != 'cancel' for picking_id in record.picking_ids):
                    raise ValidationError(_("There is an unfinish delivery order. Please cancel DO first !."))
                elif record.picking_ids.filtered(lambda r: r.state == 'done') and record.invoice_ids.filtered(lambda l: l.state == 'posted' and l.payment_state == 'paid'):
                    raise ValidationError("You Cannot Cancel Confirmed SO!")
                msg = {}
                if record.delivery_count or record.invoice_count:
                    self._cr.execute("""
                            SELECT state
                            FROM stock_picking
                            WHERE origin = '%s'
                        """ % (record.name, )
                                     )

                    state_do = self._cr.dictfetchall()

                    self._cr.execute("""
                            SELECT state, payment_state
                            FROM account_move
                            WHERE invoice_origin = '%s'
                        """ % (record.name, )
                                     )

                    state_inv = self._cr.dictfetchall()

                    for state in state_do:
                        if "cancel" not in state['state']:
                            if "ready" != state['assigned']:
                                msg["1"] = "There is an unfinish delivery order. Please cancel DO first."

                    for state in state_inv:
                        if "cancel" not in state['state']:
                            if "paid" != state['payment_state']:
                                msg["2"] = "There is an unfinish invoice. Please cancel the invoice first."

                    if "1" in msg and "2" in msg:
                        msg["3"] = "There is and unfinish delivery order or invoice. Please cancel the DO or invoice first."

                    if msg:
                        raise ValidationError(_(msg[list(msg.keys())[len(msg)-1]]))
                    else:
                        record.write({'state': 'cancel', 'sale_state': 'cancel', 'is_quotation_cancel': False})
                else:
                    record.write({'state': 'cancel', 'sale_state': 'cancel', 'is_quotation_cancel': False})
            else:
                record.write({'state': 'cancel', 'sale_state': 'cancel', 'is_quotation_cancel': True})

    def _action_confirm(self):
        res = super(SaleOrder, self)._action_confirm()
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        for record in self:
            record.write({'date_confirm': datetime.today()})
            record.write({'sale_state': 'in_progress'})
            keep_name_so = IrConfigParam.get_param('keep_name_so', False)
            if not keep_name_so:
                if record.origin:
                    record.origin += "," + record.name
                else:
                    record.origin = record.name
                record.name = self.env['ir.sequence'].next_by_code('sale.quotation.order')
        return res

    def unlink(self):
        for record in self:
            if record.picking_ids.filtered(lambda r: r.state == 'done') and record.invoice_ids.filtered(lambda l: l.state == 'posted' and l.payment_state == 'paid'):
                raise ValidationError("You Cannot Delete Confirmed SO!")
        return super(SaleOrder, self).unlink()

    def action_draft(self):
        orders = self.filtered(lambda s: s.state in ['cancel',
                            'sent', 'over_limit_reject', 'reject'])
        return orders.write({
            'state': 'draft',
            'signature': False,
            'signed_by': False,
            'signed_on': False,
        })

    # def action_done(self):
    #     for record in self:
    #         if not self.env.user.has_group('sale.group_auto_done_setting'):
    #             if all(picking_id.state == 'done' for picking_id in record.picking_ids) and \
    #                all(invoice_id.payment_state == 'paid' for invoice_id in record.invoice_ids):
    #                record.write({'sale_state': 'done'})
    #             else:
    #                 raise ValidationError(_("You Have an Unfinished Shipment or Invoice."))
    #         else:
    #             return super(SaleOrder, self).action_done()

    @api.depends('order_line')
    def order_line_calc(self):
        for record in self:
            record.order_line_count = len(record.order_line)

    def sh_sale_adv_btn(self):
        if self:
            for data in self:
                view = data.env.ref(
                    'equip3_sale_operation.sh_sale_adv_wizard_form_view'
                )
                ori_arch = """
    <form string="Select Products Advance">
                <notebook>
                    <page string="List">
                        <field name="product_ids">
                            <tree create="false" editable='bottom'>
                                <button name="add_to_specific" type="object" string="Add"/>
                                <field name="product_id" readonly="1" />
                                <field name="default_code" readonly="1" />
                                <field name="standard_price" readonly="1" />
                                <field name="uom_po_id" readonly="1" />
                                <field name="qty" />
                            </tree>
                        </field>
                    </page>
                    <page string="Specific">
                        <field name="specific_product_ids">
                            <tree create="false" editable='bottom'>
                                <field name="product_id" readonly="1" />
                                <field name="default_code" readonly="1" />
                                <field name="standard_price" readonly="1" />
                                <field name="uom_po_id" readonly="1" />
                                <field name="qty" />
                            </tree>
                        </field>
                    </page>
                </notebook>
                <footer>
                    <button name="sh_sale_adv_select_btn" string="Select List" type="object" class="oe_highlight"/>
                    <button name="sh_sale_adv_select_specific_btn" string="Select Specific" type="object" class="oe_highlight"/>
                    <button string="Cancel" class="oe_link" special="cancel"/>
                </footer>
            </form>

                """

                sh_cid = self.env.user.company_id
                sale_model_id = self.env[
                    'ir.model'
                ].sudo().search([
                    ('model', '=', 'sale.adv.wizard')
                ], limit=1)
                int_operators = [
                    ('=', '='),
                    ('!=', '!='),
                    ('>', '>'),
                    ('<', '<'),
                    ('>=', '>='),
                    ('<=', '<=')
                ]
                char_operators = [
                    ('=', '='),
                    ('!=', '!='),
                    ('like', 'like'),
                    ('ilike', 'ilike'),
                    ('=like', '=like'),
                    ('not like', 'not like'),
                    ('not ilike', 'not ilike')
                ]
                if sale_model_id and sh_cid:
                    ir_model_fields_obj = self.env['ir.model.fields']
                    str_obj = ori_arch
                    first_str = """<?xml version="1.0"?>
    <form string="Select Products Advance">
                    """

                    if sh_cid.sh_sale_pro_field_ids:
                        for rec in sh_cid.sh_sale_pro_field_ids:
                            #add custom selection operators fields here
                            if rec.ttype not in ['boolean', 'many2one', 'selection']:
                                search_opt_field = ir_model_fields_obj.sudo().search([
                                    ('name', '=', 'x_opt_'+ rec.name),
                                    ('model_id', '=', sale_model_id.id),
                                ], limit=1)
                                if not search_opt_field:
                                    opt_field_vals = {
                                        'name': 'x_opt_'+rec.name,
                                        'model': 'sale.adv.wizard',
                                        'field_description': 'x_opt_' + rec.field_description,
                                        'model_id': sale_model_id.id,
                                        'ttype': 'selection',
                                    }
                                    if rec.ttype in ['integer', 'float']:
                                        opt_field_vals.update({
                                            'selection': str(int_operators)
                                        })
                                    if rec.ttype == 'char':
                                        opt_field_vals.update({
                                            'selection': str(char_operators)
                                        })
                                    ir_model_fields_obj.sudo().create(opt_field_vals)

                            #create duplicate fields from product.product
                            #first search field if not found than create it.
                            search_field = ir_model_fields_obj.sudo().search([
                                ('name', '=', 'x_'+ rec.name),
                                ('model_id', '=', sale_model_id.id),
                            ], limit = 1)
                            #update selection fields key value pair each time when wizard is load
                            if search_field and search_field.ttype == 'selection':
                                selection_key_value_list = False
                                selection_key_value_list = self.env[
                                    rec.model
                                ].sudo()._fields[
                                    rec.name
                                ].selection
                                if selection_key_value_list:
                                    selection_field_dic = {}
                                    selection_field_dic.update({
                                        'selection': str(selection_key_value_list)
                                    })
                                    search_field.sudo().write(selection_field_dic)
                                else:
                                    raise UserError(
                                        _('Key value pair for this selection field not found - '+ search_field.name
                                          ))

                            selection_key_value_list = False
                            #crate new custome field here
                            if not search_field:
                                field_vals = {
                                    'name': 'x_'+rec.name,
                                    'model': 'sale.adv.wizard',
                                    'field_description': rec.field_description,
                                    'model_id': sale_model_id.id,
                                    'ttype': rec.ttype,
                                }
                                if rec.relation:
                                    field_vals.update({'relation': rec.relation})
                                if rec.ttype == 'selection':
                                    selection_key_value_list = self.env[
                                        rec.model
                                    ].sudo()._fields[
                                        rec.name
                                    ].selection
                                    if selection_key_value_list:
                                        field_vals.update({
                                            'selection': str(selection_key_value_list)
                                        })
                                    else:
                                        raise UserError(
                                            _('Key value pair for this selection field not found - '+ rec.name
                                              ))
                                ir_model_fields_obj.sudo().create(field_vals)

                    #product attributes code start here
                    #create attributes m2o fields if never exist
                    if sh_cid.sh_sale_pro_attr_ids:
                        for rec in sh_cid.sh_sale_pro_attr_ids:
                            search_attr_field = ir_model_fields_obj.sudo().search([
                                ('name', '=', 'x_attr_' + str(rec.id)),
                                ('model_id', '=', sale_model_id.id),
                            ], limit = 1)
                            if not search_attr_field:
                                attr_field_vals = {
                                    'name': 'x_attr_' + str(rec.id),
                                    'model': 'sale.adv.wizard',
                                    'field_description': rec.name,
                                    'model_id': sale_model_id.id,
                                    'ttype': 'many2one',
                                    'relation': 'product.attribute.value'
                                }
                                ir_model_fields_obj.sudo().create(attr_field_vals)

                    no_create_str = """ options="{'no_create': True }" """
                    middle_str = ''
                    if sh_cid.sh_sale_pro_field_ids:
                        for rec in sh_cid.sh_sale_pro_field_ids:
                            middle_str += "<div class='row' style='border-bottom:1px solid #ccc; margin:5px 10px;margin: 9px 0px;'>"
                            middle_str += "<div class='col-sm-3 text-right' style='font-weight:bold'> <label for='x_" + rec.name + "'/></div>"
                            if rec.ttype not in ['boolean','many2one','selection']:
                                middle_str += "<div class='col-sm-3'> <field name='x_opt_" + rec.name + "'/></div>"
                            if rec.ttype == 'many2one':
                                middle_str += "<div class='col-sm-3'> <field name='x_" + rec.name + "' "+ ' ' + no_create_str+"/></div>"
                            elif rec.ttype in ['boolean','selection']:
                                middle_str += "<div class='col-sm-3'> <field name='x_" + rec.name + "'/></div>"
                            else:
                                middle_str += "<div class='col-sm-6'> <field name='x_" + rec.name + "'/></div>"
                            middle_str += "</div>"

                    #add prodduct attributes fields in wizard view
                    if sh_cid.sh_sale_pro_attr_ids:
                        for rec in sh_cid.sh_sale_pro_attr_ids:
                            domain_str = """ domain="[('attribute_id','=',""" +  str(rec.id) + """)]" """
                            middle_str += "<div class='row' style='border-bottom:1px solid #ccc; margin:5px 10px;margin: 9px 0px;'>"
                            middle_str += "<div class='col-sm-3 text-right' style='font-weight:bold'> <label for='x_attr_" + str(rec.id) + "'/></div>"
                            middle_str += "<div class='col-sm-3'> <field name='x_attr_" + str(rec.id) + "' "+  domain_str +' '+ no_create_str +"/></div>"
                            middle_str += "<div class='col-sm-6'> </div>"
                            middle_str += "</div>"


                    #add the many2many attribute fields here.
                    no_create_str = """ options="{'no_create': True}" """
                    middle_str += "<div class='row' style='border-bottom:1px solid #ccc; margin:5px 10px;margin: 9px 0px;'>"
                    middle_str += "<div class='col-sm-3 text-right' style='font-weight:bold'> <label for='product_attr_ids'/> </div>"
                    middle_str += "<div class='col-sm-6'> <field name='product_attr_ids' widget='many2many_tags'"+' '+ no_create_str +"/> </div>"
                    middle_str += "</div>"

                    #button start here
                    middle_str += "<div class='col-md-12 text-center'>"
                    middle_str += "<button name='filter_products' string='Filter Products' type='object'/>"
                    middle_str += "<button name='reset_filter' string='Reset Filter' type='object'/>"
                    middle_str += "<button name='reset_list' string='Reset List' type='object'/>"
                    middle_str += "<button name='reset_specific' string='Reset Specific' type='object'/>"
                    middle_str += "</div>"
                    last_str = str_obj.split(">",1)[1]
                    final_arch = first_str + middle_str + last_str

                    if view:
                        view.sudo().write({'arch': final_arch})

                context = self.env.context
                return {
                    'name': 'Select Products Advance',
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'sale.adv.wizard',
                    'views': [(view.id, 'form')],
                    'view_id': view.id,
                    'target': 'new',
                    'context': context,
                }

class MailMessage(models.Model):
    _inherit = 'mail.message'

    @api.model
    def create(self, vals):
        if vals.get('model') and \
            vals.get('model') == 'sale.order' and vals.get('tracking_value_ids'):
            sale_state_1 = self.env['ir.model.fields']._get('sale.order', 'sale_state_1').id
            state_2 = self.env['ir.model.fields']._get('sale.order', 'state_2').id
            approval_matrix_state = self.env['ir.model.fields']._get('sale.order', 'approval_matrix_state').id
            approval_matrix_state_1 = self.env['ir.model.fields']._get('sale.order', 'approval_matrix_state_1').id
            vals['tracking_value_ids'] = [rec for rec in vals.get('tracking_value_ids') if 
                                        rec[2].get('field') not in (sale_state_1, state_2, 
                                        approval_matrix_state, approval_matrix_state_1)]
        return super(MailMessage, self).create(vals)


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    auto_mail = fields.Boolean(string="Automation Email")
    group_multi_do = fields.Boolean(string="Multiple Delivery Address?", implied_group='equip3_sale_operation.group_multi_do')

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        res.update({
            'auto_mail': IrConfigParam.get_param('auto_mail', False),
            'group_multi_do': IrConfigParam.get_param('group_multi_do', False),
        })
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values() 
        self.env['ir.config_parameter'].sudo().set_param('auto_mail', self.auto_mail)
        self.env['ir.config_parameter'].sudo().set_param('group_multi_do', self.group_multi_do)

class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    def action_send_mail(self):
        context = dict(self.env.context) or {}
        if context.get('default_model') and context.get('default_res_id'):
            record = self.env[context.get('default_model')].browse([context.get('default_res_id')])
            if record._name == 'sale.order':
                if record.state != 'sale':
                    record.write({'state': 'sent'})
                record.message_post(body="Quotation Send by Email")
            elif record._name == 'purchase.order':
                if record.state != 'purchase':
                    record.write({'state': 'sent'})
                record.message_post(body="Quotation Send by Email")
        return super(MailComposer, self).action_send_mail()

class ModProductBundle(models.TransientModel):
    _inherit = 'wizard.product.bundle.bi'

    def mod_button_add_product_bundle_bi(self):
        next_num = 0
        order_line_data = self.env['sale.order.line'].search([('order_id','=', self._context['active_id'])])
        sale_data = self.env['sale.order'].search([('id', '=', self._context['active_id'])]) 
        if order_line_data:
            next_num = next_num + int(self.env['sale.order.line'].browse(order_line_data.ids[-1]).sale_line_sequence) + 1
        else:
            next_num += 1
        if self.bi_pack_ids:
            for pack in self.bi_pack_ids:
                sale_order_id = self.env['sale.order.line'].search([('order_id','=', self._context['active_id']),('product_id','=',pack.product_id.id)])
                if sale_order_id and  sale_order_id[0] :
                    sale_order_line_obj = sale_order_id[0]
                    sale_order_line_obj.write({'product_uom_qty': sale_order_line_obj.product_uom_qty + (pack.qty_uom * self.product_qty)})


                else:
                    self.env['sale.order.line'].create({'sale_line_sequence':str(next_num),
                                                    'order_id':self._context['active_id'],
                                                    'product_id':pack.product_id.id,
                                                    'name':pack.product_id.name,
                                                    'price_unit':pack.product_id.list_price,
                                                    'product_uom':pack.uom_id.id,
                                                    'product_uom_qty':pack.qty_uom * self.product_qty,
                                                    'multiple_do_date': sale_data.commitment_date,
                                                    'delivery_address_id': sale_data.partner_invoice_id.id,
                                                    'line_warehouse_id': sale_data.warehouse_id.id,
                                                    'account_tag_ids': sale_data.account_tag_ids.ids,
                                                    })
                    next_num += 1

        return True
