
from odoo import api, fields, models, _

class AccountMove(models.Model):
    _inherit = 'account.move'

    branch_id = fields.Many2one(
        'res.branch',
        default=lambda self: self.env.user.branch_id,
        related="",
        readonly=False)
    sale_order_reference_id = fields.Many2one('sale.order', string="Sale Order Reference")
    filter_sale_order_reference_ids = fields.Many2many('sale.order', string='Sale Order Reference', compute='_get_filter_sale_order', store=False)

    @api.depends('company_id')
    def _get_filter_sale_order(self):
        sale_order = []
        for record in self:
            sale_order_ids = record.env['sale.order'].search([('sale_state', '=', 'in_progress')])
            for sale_order_id in sale_order_ids:
                if not sale_order_id.invoice_ids:
                    sale_order.append(sale_order_id.id)
            record.filter_sale_order_reference_ids = [(6, 0, sale_order)]

    @api.onchange('sale_order_reference_id')
    def get_sale_order_reference(self):
        name = self.sale_order_reference_id.name
        if self.sale_order_reference_id:
            self.invoice_line_ids = False
            self.line_ids = False
            sale_order_line = []
            fiscal_position = self.sale_order_reference_id.fiscal_position_id
            for order_line in self.sale_order_reference_id.order_line:
                accounts = order_line.product_id.product_tmpl_id.get_product_accounts(fiscal_pos=fiscal_position)
                vals = order_line._prepare_invoice_line()
                vals.update({
                    'account_id': accounts['income'].id, 
                    'quantity' : order_line.product_uom_qty,
                    'price_tax': order_line.price_tax,
                    'price_subtotal': order_line.price_subtotal,
                    'sale_line_ids': [(6, 0, order_line.ids)]
                })
                sale_order_line.append((0, 0, vals))
            customer_reference = name + ("-") + self.sale_order_reference_id.partner_id.name
            self.update({'partner_id': self.sale_order_reference_id.partner_id.id,
                        'invoice_payment_term_id' : self.sale_order_reference_id.payment_term_id.id,
                        'branch_id' : self.sale_order_reference_id.branch_id.id,
                        'partner_shipping_id' : self.sale_order_reference_id.partner_shipping_id.id,
                        'invoice_line_ids' : sale_order_line,
                        'move_type': 'out_invoice',
                        'invoice_user_id' : self.sale_order_reference_id.user_id.id,
                        'team_id' : self.sale_order_reference_id.team_id.id,
                        'fiscal_position_id' : self.sale_order_reference_id.fiscal_position_id.id,
                        'ref' : customer_reference,
                         'discount_amt': self.sale_order_reference_id.discount_amt,
                         'discount_method': self.sale_order_reference_id.discount_method,
                         'discount_amount': self.sale_order_reference_id.discount_amount,
                        })
            self._onchange_invoice_line_ids()
            total = 0
            for line in self.invoice_line_ids:
                journal_filter_line = self.line_ids.filtered(lambda r:r.account_id.id == line.account_id.id)
                journal_filter_line.credit = line.price_unit
                total += line.price_unit
            partner_filter_line = self.line_ids.filtered(lambda r:r.account_id.id == self.partner_id.property_account_receivable_id.id)
            partner_filter_line.debit = total
        else:
            self.update({'partner_id': False,
                        'invoice_payment_term_id' : False,
                        'branch_id': self.env.user.branch_id,
                        'partner_shipping_id' : False,
                        'invoice_line_ids' : False,
                        'line_ids': False,
                        # 'invoice_user_id' : False,
                        # 'team_id' : False,
                        'fiscal_position_id' : False,
                        'ref' : False  
                        })

    @api.model_create_multi
    def create(self, vals_list):
        lines = super(AccountMove, self).create(vals_list)
        for line in lines:
            if line.sale_order_reference_id:
                line.message_post(body=(_("This journal entry has been created from : %s") % line.sale_order_reference_id.name))
                for invoice_line in line.invoice_line_ids:
                    filter_order_line = line.sale_order_reference_id.order_line.filtered(lambda r:r.product_id.id == invoice_line.product_id.id)
                    if filter_order_line:
                        invoice_line.sale_line_ids = [(6, 0, filter_order_line[0].ids)]
        return lines

class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    sale_line_ids = fields.Many2many(
        'sale.order.line',
        'sale_order_line_invoice_rel',
        'invoice_line_id', 'order_line_id',
        string='Sales Order Lines', readonly=False, copy=False)