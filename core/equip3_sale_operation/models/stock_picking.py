from odoo import api, fields, models, _
from odoo.exceptions import Warning


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.constrains('sale_id')
    def set_analytic(self):
        for res in self:
           if res.sale_id:
                res.analytic_account_group_ids = res.sale_id.account_tag_ids