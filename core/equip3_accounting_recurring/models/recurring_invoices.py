# -*- coding: utf-8 -*-

from odoo import models, fields, api

class RecurringInvoices(models.Model):
    _inherit = 'invoice.recurring'

    created_by = fields.Many2one(comodel_name='res.users', string='Created By', default=lambda self: self.env.user, tracking=True)
    created_date = fields.Date(string='Created Date', default=fields.Datetime.now, tracking=True)
    branch_id = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id.id, tracking=True)
    prepayment_journal = fields.Selection([('customer','Customer'), ('vendor','Vendor')])

    @api.model
    def create(self, vals):
        if 'company_id' in vals:
            self = self.with_company(vals['company_id'])

        if vals.get('name', ('New')) == ('New'):
            seq_date = None
            if 'start_date' in vals:
                seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['start_date']))
                
            if vals['type'] == 'out_invoice':
                recurring_seq = self.env['ir.sequence'].next_by_code('recurring.in.invoice.seq', sequence_date=seq_date)
            elif vals['type'] == 'in_invoice':
                recurring_seq = self.env['ir.sequence'].next_by_code('recurring.out.invoice.seq', sequence_date=seq_date)
            elif vals['type'] == 'entry':
                recurring_seq = self.env['ir.sequence'].next_by_code('recurring.entry.seq', sequence_date=seq_date)
                if vals['prepayment_journal'] == 'customer':
                    recurring_seq = self.env['ir.sequence'].next_by_code('recurring.customer.prepayment.seq', sequence_date=seq_date)
                elif vals['prepayment_journal'] == 'vendor':
                    recurring_seq = self.env['ir.sequence'].next_by_code('recurring.vendor.prepayment.seq', sequence_date=seq_date)
                
        result = super(RecurringInvoices, self).create(vals)
        result['name'] = recurring_seq
        result['state'] = 'confirm'
        

        return result

    def confirm(self):
        for rec in self:
            rec.state = 'confirm'

    def pending(self):
        for rec in self:
            rec.state = 'pending'

    def done(self):
        for rec in self:
            rec.state = 'done'

    def cancel(self):
        for rec in self:
            rec.state = 'cancel'

    @api.onchange('company_id')
    def _get_domain(self):
        for rec in self:
            field = ''
            if rec.type == 'out_invoice':
                field = ['sale','']
            elif rec.type == 'in_invoice':
                field = ['purchase','']
            if rec.type == 'entry':
                field = ['general', 'bank', 'cash']

            if rec.type in ('out_invoice', 'in_invoice'):
                getval = self.env['account.journal'].search([('type','in',field)])
                rec.journal_id = getval[0]
            
        return {
            'domain':{
                'branch_id':f"[('id','in',{[x.id for x in self.env.user.branch_ids]})]",
                'journal_id':f"[('type','in',{field})]"
                }
        }