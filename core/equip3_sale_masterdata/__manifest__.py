# -*- coding: utf-8 -*-
{
    'name': "Equip3 Sale Master Data",

    'summary': """
        Manage your master data in sale""",

    'description': """
        This module manages these features :
        1. Customer
        2. Product
        3. Pricelist
        4. Pricelist Approval Matrix
    """,

    'author': "Hashmicro",
    'category': 'Sales',
    'version': '1.1.13',

    # any module necessary for this one to work correctly
    'depends': [
        'sale', 'equip3_general_features', 'product', 'sale_product_configurator', 'web', 'ss_whatsapp_connector', 'acrux_chat_sale', 'dynamic_product_bundle', 'contract_recurring_invoice_analytic'
    ],

    # always loaded
    'data': [
        'data/ir_sequence_data.xml',
        "data/customer_degree_trust_data.xml",
        'security/ir.model.access.csv',
        'views/product_template_views.xml',
        'views/sale_order_views.xml',
        'views/res_partner_views.xml',
        'views/customer_degree_trust.xml',
        'wizard/sale_order_alternative_products.xml',
        
    ],
    # only loaded in demonstration mode
    'demo': [
    
    ],
}