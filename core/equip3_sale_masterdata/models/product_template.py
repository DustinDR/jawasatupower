
from odoo import models, fields, api, _


class ProductTemplate(models.Model):
	_inherit = 'product.template'

	alternative_product_ids = fields.Many2many('product.product', 'product_product_alternative_rel', 'product_id', 'tmpl_id', string="Alternative Product")
	is_pack = fields.Boolean(string='Is Product Bundle')