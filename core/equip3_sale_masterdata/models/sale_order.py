
from odoo import models, fields, api, _


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    def action_alternative_product(self):
        context = dict(self.env.context) or {}
        alternative = self.product_id.alternative_product_ids.ids
        alternative.extend(self.product_id.product_tmpl_id.alternative_product_ids.ids)
        context.update({
            'default_product_id': self.product_id.id,
            'default_alter_product_ids': [(6, 0, alternative)],
            'default_sale_line_id' : self.id,
        })
        return {
                'type': 'ir.actions.act_window',
                'name': 'Alternatives Products',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'sale.order.alternative.product',
                'target': 'new',
                'context' : context
            }

    def action_readonly(self):
        return True