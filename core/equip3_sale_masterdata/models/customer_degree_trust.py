
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class CustomerDegreeTrust(models.Model):
    _name = 'customer.degree.trust'
    _description = "Customer Degree Trust"
     
    name = fields.Char(string="Name", required=True)
    overdue_day = fields.Integer(string="Overdue Days")
    no_of_overdue_invoices = fields.Integer(string="Number Of Overdue Invoices")
    index = fields.Integer(string="Index", compute="_compute_index", store=True)

    @api.depends('overdue_day', 'no_of_overdue_invoices')
    def _compute_index(self):
        for record in self:
            record.index = record.overdue_day * record.no_of_overdue_invoices

    @api.constrains('name', 'index')
    def _check_existing_record(self):
        for record in self:
            customer_degree_trust_id = self.search([('id', '!=', record.id), '|', ('name', 'ilike', record.name), ('index', '=', record.index)], limit=1)
            if customer_degree_trust_id:
                raise ValidationError("Data can't be the same like other degree of trust !")
