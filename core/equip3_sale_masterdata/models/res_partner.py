
from odoo import models, fields, api, _
from datetime import datetime

class ResPartner(models.Model):
    _inherit = "res.partner"

    customer_sequence = fields.Char(string="Customer ID", readonly=True, copy=False)
    debtor_id = fields.Many2one('customer.degree.trust', string="Trust Degree", compute="_compute_debtor_id", store=False)
    is_customer = fields.Boolean(string='Customer')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company, tracking=True)

    @api.model
    def create(self, values):
        if values.get('is_customer'):
            sequence = self.env['ir.sequence'].next_by_code('res.partner.customer.sequence')
            values.update({'customer_sequence': sequence})
            values.update({'customer_rank': 1})
        else:
            if values.get('customer_rank', 1):
                sequence = self.env['ir.sequence'].next_by_code('res.partner.customer.sequence')
                values.update({'customer_sequence': sequence})

        res = super(ResPartner, self).create(values)
        return res

    def write(self, values):
        res = super(ResPartner, self).write(values)
        if values.get('is_customer'):
            sequence = self.env['ir.sequence'].next_by_code('res.partner.customer.sequence')
            values.update({'customer_sequence': sequence})
            values.update({'customer_rank': 1})
            res = super(ResPartner, self).write(values)
        else:
            if self.create_date != self.write_date:
                is_data_exist = 0
                res_data = self.env['sale.order'].search([('partner_id', '=', self.ids[0])], limit=1)
                if len(res_data) > 0:
                    is_data_exist = 1

                if is_data_exist == 0:
                    res_data = self.env['account.move'].search([('partner_id', '=', self.ids[0])], limit=1)
                    if len(res_data) > 0:
                        is_data_exist = 1

                if is_data_exist == 1:
                    values.update({'is_customer': True})
                    res = super(ResPartner, self).write(values)
                else:
                    values.update({'is_customer': False})
                    values.update({'customer_sequence': 0})
                    values.update({'customer_rank': 0})
                    res = super(ResPartner, self).write(values)
        return res

    def _compute_debtor_id(self):
        no_of_invoices_overdue = 0
        no_of_invoice_days_overdue = 0
        for record in self:
            record.debtor_id = False
            invoice_ids = self.env['account.move'].search([('partner_id', '=', record.id),
                                                        ('invoice_payment_term_id', '!=', False),
                                                        ('move_type', '=', 'out_invoice'),
                                                        ('invoice_date_due', '<', datetime.now().date()),
                                                        ('state', '=', 'posted'),
                                                        ('payment_state','in',('not_paid','in_payment','partial')),
                                                        ])
            no_of_invoices_overdue = sorted(invoice_ids, key=lambda k: k['invoice_date_due'])
            if len(no_of_invoices_overdue) > 0:
                invoice_date = datetime.now().date() - no_of_invoices_overdue[0].invoice_date_due
                no_of_invoice_days_overdue += invoice_date.days
            index = len(no_of_invoices_overdue) * no_of_invoice_days_overdue
            customer_degree_trust_ids = self.env['customer.degree.trust'].search([('index', '<=', index)])
            sort_customer_degree_trust_ids = sorted(customer_degree_trust_ids, key=lambda k: k['index'])
            if sort_customer_degree_trust_ids:
                record.debtor_id = sort_customer_degree_trust_ids[-1].id
