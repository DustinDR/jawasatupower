odoo.define('equip3_POS_master_data_retail.models', function (require) {
"use strict";

var models = require('point_of_sale.models');
var sssmodels = require('pos_retail.order');
var PosDB = require('point_of_sale.DB');

models.load_models({
    model: 'group.card',
    fields: ['card_group_name','card_group_active','branch_id','company_id'],
    domain: function(self){ return [['company_id', '=', self.company && self.company.id || false], ['card_group_active', '=', true]]; },
    loaded: function(self,card_groups){
    	self.db.set_card_groups(card_groups);
    },
});

models.load_models({
    model: 'card.payment',
    fields: ['card_name','card_active','card_group','BIN', 'branch_id', 'company_id'],
    domain: function(self){ return []; },
    loaded: function(self,card_payments){
        self.db.set_card_payments(card_payments);
    },
});

let _super_order_sp = models.Order.prototype;
models.Order = models.Order.extend({
	initialize: function(attr,options) {
        _super_order_sp.initialize.apply(this,arguments);
        this.selected_card_payment_id = this.selected_card_payment_id || false;
    },
    set_selected_card_payment_id: function(selected_card_payment_id){
    	this.selected_card_payment_id = selected_card_payment_id;
    	this.trigger('change');
    },
    get_selected_card_payment_id: function(){
    	return this.selected_card_payment_id;
    },
    init_from_JSON: function(json) {
    	_super_order_sp.init_from_JSON.apply(this,arguments);
    	this.set_selected_card_payment_id(json.selected_card_payment_id);
    },
    export_as_JSON: function() {
    	var res = _super_order_sp.export_as_JSON.apply(this,arguments);
    	res['selected_card_payment_id'] = this.get_selected_card_payment_id();
    	return res;
    }
});

let _super_payment_sp = models.Paymentline.prototype;
models.Paymentline = models.Paymentline.extend({
	export_as_JSON: function(){
		var res = _super_payment_sp.export_as_JSON.apply(this,arguments);
		res['payment_card_id'] = this.order ? this.order.get_selected_card_payment_id(): false;
		return res;
	}
});


});