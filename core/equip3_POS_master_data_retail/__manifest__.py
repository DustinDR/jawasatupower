# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Equip3 POS Master Data',
    'author': 'Hashmicro',
    'version': '1.1.1.1',
    'summary': 'Manage your stock operation activities.',
    'depends': ['point_of_sale','branch'],
    'category': 'Point of Sale',
    'data': [
        'security/ir.model.access.csv',
        'views/assets.xml',
        'views/pos_payment_card.xml',
        'views/pos_promotion.xml',
    ],
    'qweb': [
        'static/src/xml/pos.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
