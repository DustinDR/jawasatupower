# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import _, api, fields, models

class Pos_Config(models.Model):
    _inherit = 'pos.promotion'
    # _inherit = ['mail.thread', 'mail.activity.mixin']


    is_stack = fields.Boolean(string="Stack Promotions")

    