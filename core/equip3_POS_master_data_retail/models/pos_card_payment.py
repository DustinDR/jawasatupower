# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import _, api, fields, models

class Pos_GroupCard(models.Model):
    _name = 'group.card'
    _rec_name = 'card_group_name'
    _inherit = ['mail.thread', 'mail.activity.mixin']


    card_group_img = fields.Binary(tracking=True)
    card_group_name = fields.Char(string="Group Card", tracking=True)
    card_group_active = fields.Boolean(string="Active in Point of Sale", tracking=True)
    branch_id = fields.Many2one('res.branch' ,string = "Branch", tracking=True)
    company_id = fields.Many2one('res.company',string= "Company",default=lambda self: self.env.user.company_id ,tracking=True)
    # Created_by = fields. Many2one('res.users', string = "Created by", tracking=True)

class Pos_CardPayment(models.Model):
    _name = 'card.payment'
    _rec_name = 'card_name'
    _inherit = ['mail.thread', 'mail.activity.mixin']


    card_img = fields.Binary(tracking=True)
    card_name = fields.Char(string="Card", tracking=True)
    card_active = fields.Boolean(string = "Active in Point of Sale", tracking=True)
    card_group = fields.Many2one('group.card', string = "Group Card", tracking=True)
    BIN = fields.Char(string = "BIN", tracking=True)
    branch_id = fields.Many2one('res.branch' ,string = "Branch" , tracking=True)
    company_id = fields.Many2one('res.company',string= "Company",default=lambda self: self.env.user.company_id , tracking=True)
    # Created_by = fields. Many2one('res.users', string = "Created by", tracking=True)


class PosPayment(models.Model):
    _inherit = 'pos.payment'

    payment_card_id = fields.Many2one('card.payment', string="Card Payment")