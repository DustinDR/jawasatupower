# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
class PosOrder(models.Model):
    _inherit = "pos.order"

    @api.model
    def _payment_fields(self, order, ui_paymentline):
        res = super(PosOrder, self)._payment_fields(order, ui_paymentline)
        res['payment_card_id'] = ui_paymentline.get('payment_card_id') or False
        return res