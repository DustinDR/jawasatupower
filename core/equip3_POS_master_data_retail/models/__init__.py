# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import pos_card_payment
from . import pos_promotion
from . import pos_order
