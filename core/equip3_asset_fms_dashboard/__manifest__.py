
{
    'name': "Equip3 Asset Fms Dashboard",
    'version': '1.1.1',
    'category': 'Extra Tools',
    'summary': """Create Configurable Dashboards Easily""",
    'description': """Create Configurable Dashboard Dynamically to get the information that are relevant to your business, department, or a specific process or need, Dynamic Dashboard, Dashboard, Dashboard Odoo""",
    'author': 'Hashmicro / Chirag Chauhan',
    'website': "https://www.Hashmicro.com",
    'company': 'Hashmicro / Chirag Chauhan',
    'maintainer': 'Hashmicro / Chirag Chauhan',
    'depends': ['base', 'maintenance', 'equip3_asset_fms_masterdata', 'equip3_asset_fms_operation', 'ks_dashboard_ninja'],
    'data': [
        'data/assets_data.xml',
    ],
   'license': "AGPL-3",
    'installable': True,
    'auto_install': False,
    'application': False,
}
