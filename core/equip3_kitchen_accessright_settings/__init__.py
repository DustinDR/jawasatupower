# -*- coding: utf-8 -*-
from . import models
import logging

_logger = logging.getLogger(__name__)

from odoo import api, SUPERUSER_ID

def _default_init(cr, registry):
	env = api.Environment(cr, SUPERUSER_ID, {})

	for company in env['res.company'].search([]):
		company.manufacturing = False
		company.assembly = False
		company.central_kitchen = False
