# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from ast import literal_eval
import logging

_logger = logging.getLogger(__name__)


class ResCompany(models.Model):
	_inherit = 'res.company'

	@api.model
	def toggle_active_menus(self):
		
		if not self:
			self = self.search([])

		kitchen_menus = self.env.ref("equip3_kitchen_accessright_settings.menu_kitchen_root")
		assembly_menus = self.env.ref("equip3_kitchen_accessright_settings.menu_assembly_root")
		mrp_menus = self.env.ref("mrp.menu_mrp_root")
		user = self.env.user

		for company in self:
			assembly_menus.active = company.assembly
			kitchen_menus.active = company.central_kitchen
			mrp_menus.active = company.manufacturing

			# remove assembly accessright when switch to kitchen
			if company.central_kitchen:
				for group in ['labor', 'manager', 'administrator']:
					assembly_group = self.env.ref('equip3_kitchen_accessright_settings.group_assembly_%s' % group)
					if user in assembly_group.users:
							assembly_group.users = [(3, user.id)]


	def write(self, vals):
		res = super(ResCompany, self).write(vals)
		self.toggle_active_menus()
		return res
