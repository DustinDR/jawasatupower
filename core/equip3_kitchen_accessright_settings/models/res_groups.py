from odoo import models, fields, api, _
from collections import defaultdict
import logging

_logger = logging.getLogger(__name__)


class GroupsView(models.Model):
	_inherit = 'res.groups'

	def get_application_groups(self, domain):
		kitchen_domain = [('category_id', '!=', self.env.ref('equip3_kitchen_accessright_settings.module_central_kitchen').id)]
		assembly_domain = [('category_id', '!=', self.env.ref('equip3_kitchen_accessright_settings.module_assembly').id)]
		if not self.env.company.assembly:
			domain += assembly_domain
		if not self.env.company.central_kitchen:
			domain += kitchen_domain
		return super(GroupsView, self).get_application_groups(domain)
