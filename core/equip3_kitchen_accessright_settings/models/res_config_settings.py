from odoo import models, fields, api


class ResConfigSettings(models.TransientModel):
	_inherit = 'res.config.settings'

	@api.model
	def create(self, values):
		central_kitchen = values.get('central_kitchen') is True
		simple_manufacturing = values.get('simple_manufacturing') is True

		kitchen_group_id = self.env.ref('equip3_kitchen_accessright_settings.group_kitchen_menus')
		simple_group_id = self.env.ref('equip3_kitchen_accessright_settings.group_simple_menus')

		if central_kitchen:
			if self.env.user not in kitchen_group_id.users:
				kitchen_group_id.users = [(4, self.env.user.id)]
			if self.env.user in simple_group_id.users:
				simple_group_id.users = [(3, self.env.user.id)]
		elif simple_manufacturing:
			if self.env.user in kitchen_group_id.users:
				kitchen_group_id.users = [(3, self.env.user.id)]
			if self.env.user not in simple_group_id.users:
				simple_group_id.users = [(4, self.env.user.id)]
		else:
			if self.env.user in simple_group_id.users:
				simple_group_id.users = [(3, self.env.user.id)]
			if self.env.user in kitchen_group_id.users:
				kitchen_group_id.users = [(3, self.env.user.id)]

		return super(ResConfigSettings, self).create(values)
