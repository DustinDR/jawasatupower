
from odoo import api , fields , models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    is_customer_approval_matrix = fields.Boolean(string="Sale Order Approval Matrix")
    is_customer_limit_matrix = fields.Boolean(string="Customer Limit Approval Matrix")
    is_over_limit_validation = fields.Boolean(string="Over Limit Validation")
    is_bo_approval_matrix = fields.Boolean(string="Blanket Order Approval Matrix")

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        is_customer_approval_matrix = IrConfigParam.get_param('is_customer_approval_matrix')
        if is_customer_approval_matrix is None:
            is_customer_approval_matrix = True
        is_customer_limit_matrix = IrConfigParam.get_param('is_customer_limit_matrix')
        if is_customer_limit_matrix is None:
            is_customer_limit_matrix = True
        is_bo_approval_matrix = IrConfigParam.get_param('is_bo_approval_matrix')
        if is_bo_approval_matrix is None:
            is_bo_approval_matrix = True
        is_over_limit_validation = IrConfigParam.get_param('is_over_limit_validation', False)
        sales = IrConfigParam.get_param('sales', False)
        res.update({
            'is_customer_approval_matrix': is_customer_approval_matrix,
            'is_customer_limit_matrix': is_customer_limit_matrix,
            'is_bo_approval_matrix': is_bo_approval_matrix,
            'sales': sales,
            'is_over_limit_validation': is_over_limit_validation,
        })
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values() 
        self.env['ir.config_parameter'].sudo().set_param('is_customer_approval_matrix', self.is_customer_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_customer_limit_matrix', self.is_customer_limit_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_over_limit_validation', self.is_over_limit_validation)
        self.env['ir.config_parameter'].sudo().set_param('sales', self.sales)
        if not self.sales:
            self.is_bo_approval_matrix = False
        self.env['ir.config_parameter'].sudo().set_param('is_bo_approval_matrix', self.is_bo_approval_matrix)
