from odoo import models, fields, api


class MrpBOM(models.Model):
    _inherit = 'mrp.bom'

    @api.depends('company_id')
    def _compute_assembly_or_kitchen_active(self):
        user_company = self.env.company
        for bom in self:
            company_id = user_company or bom.company_id
            bom.is_assembly_or_kitchen_active = company_id.central_kitchen or company_id.assembly

    is_assembly_or_kitchen_active = fields.Boolean(compute=_compute_assembly_or_kitchen_active)
