# -*- coding: utf-8 -*-

{
    'name': 'Equip 3 - Manufacturing Master Data',
    'version': '1.1.18',
    'category': 'Manufacturing',
    'summary': 'Manufacturing Master Data',
    'description': '''
    i. Bill of Material (Manage Finished Products, Components, Operations, By-Product, BoM Structure and Cost)
    ii. Work Center (Location Management, Overhead Cost, and OEE)
    ''',
    "author": "HashMicro",
    "website": "www.hashmicro.com",
    'depends': [
        'base',
        'mrp',
        'branch',
        'mail',
        'product',
        'account',
        'equip3_manuf_accessright_settings',
        'mrp_subcontracting',
        #'pos_retail',
        'mrp_workcenter_overview'
    ],
    'data': [
        'view/mrp_menuitems.xml',
        'view/mrp_workcenter_view.xml',
        'view/mrp_bom.xml',
        'view/mrp_routing_workcenter_views.xml',
        'view/product_category_view.xml',
        'view/mrp_workcenter_group.xml',
        'view/mrp_labor_group_views.xml',
        'reports/mrp_report_bom_structure.xml',
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
    ],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,

}
