# -*- coding: utf-8 -*-
##############################################################################
#    Copyright (C) 2015 - Present, Braincrew Apps (<http://www.braincrewapps.com>). All Rights Reserved

# Odoo Proprietary License v1.0
#
# This software and associated files (the "Software") may only be used (executed,
# modified, executed after modifications) if you have purchased a valid license
# from the authors, typically via Odoo Apps, braincrewapps.com, or if you have received a written
# agreement from the authors of the Software.
#
# You may develop Odoo modules that use the Software as a library (typically
# by depending on it, importing it and using its resources), but without copying
# any source code or material from the Software. You may distribute those
# modules under the license of your choice, provided that this license is
# compatible with the terms of the Odoo Proprietary License (For example:
# LGPL, MIT, or proprietary licenses similar to this one).
#
# It is forbidden to publish, distribute, sublicense, or sell copies of the Software
# or modified copies of the Software.
#
# The above copyright notice and this permission notice must be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

##############################################################################
from odoo import fields, models, api, _


class MrpWorkCenterGroup(models.Model):
    _name = 'mrp.work.center.group'
    _description = 'MRP Work Center Group'
    _inherit = 'mail.thread'
    _rec_name = 'work_center_group_name'

    @api.depends('company')
    def _compute_allowed_branch(self):
        user_branch = self.env.user.branch_ids
        for record in self:
            allowed_branch = user_branch.filtered(lambda b: b.company_id == record.company)
            record.allowed_branch_ids = [(6, 0, allowed_branch.ids)]

    work_center_group_code = fields.Char('Group Code', tracking=True)
    work_center_group_name = fields.Char('Group Name', tracking=True)
    company = fields.Many2one('res.company', 'Company', tracking=True, default=lambda self: self.env.company)
    allowed_branch_ids = fields.One2many('res.branch', compute=_compute_allowed_branch)
    branch = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id,
                             domain="[('id', 'in', allowed_branch_ids)]", tracking=True)
    mrp_work_center_group_ids = fields.One2many('mrp.work.center.group.line','mrp_work_center_group_id', string='Work Center Group Id')


class MrpWorkCenter(models.Model):
    _name = 'mrp.work.center.group.line'
    _description = 'MRP Work Center Group Line'

    work_center = fields.Many2one('mrp.workcenter', 'Work Center')
    mrp_work_center_group_id = fields.Many2one('mrp.work.center.group', 'Work Center Group Id')
