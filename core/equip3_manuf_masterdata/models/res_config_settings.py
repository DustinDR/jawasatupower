from odoo import models, api


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    @api.model
    def create(self, vals):
        vals.update({'group_mrp_routings': True, 'module_mrp_workorder': True})
        return super(ResConfigSettings, self).create(vals)
