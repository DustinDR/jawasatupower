from odoo import models, api
from odoo.tools import float_round


class MrpReportBomStructure(models.AbstractModel):
    _inherit = 'report.mrp.report_bom_structure'

    @api.model
    def _get_bom(self, bom_id=False, product_id=False, line_qty=False, line_id=False, level=False):
        bom = self.env['mrp.bom'].browse(bom_id)
        bom_quantity = line_qty
        if product_id:
            product = self.env['product.product'].browse(int(product_id))
        else:
            product = bom.product_id or bom.product_tmpl_id.product_variant_id
        lines = super(MrpReportBomStructure, self)._get_bom(bom_id=bom_id, product_id=product_id, line_qty=line_qty, line_id=line_id, level=level)
        byproducts, byproduct_total = self._get_byproduct_lines(bom, bom_quantity, product, line_id, level, lines['total'])
        lines['byproducts'] = byproducts
        lines['total'] -= byproduct_total
        return lines

    def _get_byproduct_lines(self, bom, bom_quantity, product, line_id, level, total_material):
        byproducts = []
        byproduct_total = 0
        for line in bom.byproduct_ids:
            line_quantity = (bom_quantity / (bom.product_qty or 1.0)) * line.product_qty
            company = bom.company_id or self.env.company
            sub_total = self.env.company.currency_id.round((line.allocated_cost * total_material) / 100)
            byproducts.append({
                'prod_id': line.product_id.id,
                'prod_name': line.product_id.display_name,
                'prod_qty': line_quantity,
                'prod_uom': line.product_uom_id.name,
                'prod_cost': company.currency_id.round(line.product_id.standard_price),
                'parent_id': bom.id,
                'line_id': line.id,
                'level': level or 0,
                'total': sub_total,
            })
            byproduct_total += sub_total
        return byproducts, byproduct_total

    def _get_operation_line(self, bom, qty, level):
        operations = []
        total = 0.0
        for operation in bom.operation_ids:
            workcenter_id = operation._get_best_workcenter()
            operation_cycle = float_round(qty / workcenter_id.capacity, precision_rounding=1, rounding_method='UP')
            duration_expected = operation_cycle * operation.time_cycle + workcenter_id.time_stop + workcenter_id.time_start
            total = ((duration_expected / 60.0) * workcenter_id.costs_hour)
            operations.append({
                'level': level or 0,
                'operation': operation,
                'name': operation.name + ' - ' + workcenter_id.name,
                'duration_expected': duration_expected,
                'total': self.env.company.currency_id.round(total),
            })
        return operations
