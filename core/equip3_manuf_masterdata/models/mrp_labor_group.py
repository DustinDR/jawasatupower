from odoo import models, fields, api


class MrpLaborGroup(models.Model):
    _name = 'mrp.labor.group'
    _description = 'MRP Labor Group'

    @api.depends('company_id')
    def _compute_allowed_branch(self):
        user_branch = self.env.user.branch_ids
        for record in self:
            allowed_branch = user_branch.filtered(lambda b: b.company_id == record.company_id)
            record.allowed_branch_ids = [(6, 0, allowed_branch.ids)]

    name = fields.Char(required=True)
    head_id = fields.Many2one('hr.employee', string='Head of Group', required=True, domain="[('active', '=', True)]")
    labor_ids = fields.Many2many('hr.employee', string='Labors', required=True, domain="[('active', '=', True)]")
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company, readonly=True)
    allowed_branch_ids = fields.One2many('res.branch', compute=_compute_allowed_branch)
    branch_id = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id, domain="[('id', 'in', allowed_branch_ids)]")
