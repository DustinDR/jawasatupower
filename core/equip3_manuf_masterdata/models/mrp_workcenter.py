# -*- coding: utf-8 -*-
##############################################################################
#    Copyright (C) 2015 - Present, Braincrew Apps (<http://www.braincrewapps.com>). All Rights Reserved

# Odoo Proprietary License v1.0
#
# This software and associated files (the "Software") may only be used (executed,
# modified, executed after modifications) if you have purchased a valid license
# from the authors, typically via Odoo Apps, braincrewapps.com, or if you have received a written
# agreement from the authors of the Software.
#
# You may develop Odoo modules that use the Software as a library (typically
# by depending on it, importing it and using its resources), but without copying
# any source code or material from the Software. You may distribute those
# modules under the license of your choice, provided that this license is
# compatible with the terms of the Odoo Proprietary License (For example:
# LGPL, MIT, or proprietary licenses similar to this one).
#
# It is forbidden to publish, distribute, sublicense, or sell copies of the Software
# or modified copies of the Software.
#
# The above copyright notice and this permission notice must be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

##############################################################################
from odoo import fields, models, api, _


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    overhead_cost = fields.Boolean('Overhead Cost', default=False)

    @api.onchange('type')
    def onchange_product_type(self):
        if self.type == 'product' and self.overhead_cost:
            self.overhead_cost = False


class MrpWorkcenter(models.Model):
    _name = 'mrp.workcenter'
    _inherit = ['mrp.workcenter', 'mail.thread', 'mail.activity.mixin']

    @api.depends('company_id')
    def _compute_allowed_branch(self):
        user_branch = self.env.user.branch_ids
        for record in self:
            allowed_branch = user_branch.filtered(lambda b: b.company_id == record.company_id)
            record.allowed_branch_ids = [(6, 0, allowed_branch.ids)]

    name = fields.Char('Work Center', related='resource_id.name', store=True, readonly=False, tracking=True)
    wc_location = fields.Many2one('stock.location', 'Location', tracking=True)
    finished_good_loc = fields.Many2one('stock.location', 'Finished', tracking=True)
    rejected_good_loc = fields.Many2one('stock.location', 'Rejected', tracking=True)
    byproduct_loc = fields.Many2one('stock.location', 'By-Products', tracking=True)
    oh_time = fields.One2many('overhead.time', 'mrp_workcenter_id', string="product")
    ov_material = fields.One2many('overhead.material', 'mrp_wc_id', tracking=True)
    costs_hour = fields.Float(string='Total Cost per hour', help='Specify cost of work center per hour.', default=0.0,
                              compute='_compute_cost_per_hour', tracking=True)
    allowed_branch_ids = fields.One2many('res.branch', compute=_compute_allowed_branch)
    branch = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id,
                             domain="[('id', 'in', allowed_branch_ids)]", tracking=True)
    create_uid = fields.Many2one('res.users', string='Create By', default=lambda self: self.env.user, tracking=True)
    alternative_workcenter_ids = fields.Many2many(
        'mrp.workcenter',
        'mrp_workcenter_alternative_rel',
        'workcenter_id',
        'alternative_workcenter_id',
        domain="[('id', '!=', id), '|', ('company_id', '=', company_id), ('company_id', '=', False)]",
        string="Alternative Workcenters", check_company=True,
        help="Alternative workcenters that can be substituted to this one in order to dispatch production", tracking=True
    )
    code = fields.Char('Code', copy=False, tracking=True)
    company_id = fields.Many2one(
        'res.company', required=True, index=True, readonly=True, tracking=True,
        default=lambda self: self.env.company)
    time_efficiency = fields.Float('Time Efficiency', related='resource_id.time_efficiency', default=100, store=True,
                                   readonly=False, tracking=True)
    capacity = fields.Float(
        'Capacity', default=1.0,
        help="Number of pieces that can be produced in parallel. In case the work center has a capacity of 5 and you have to produce 10 units on your work order, the usual operation time will be multiplied by 2.", tracking=True)
    oee_target = fields.Float(string='OEE Target', help="Overall Effective Efficiency Target in percentage", default=90, tracking=True)
    time_start = fields.Float('Time before prod.', help="Time in minutes for the setup.", tracking=True)
    time_stop = fields.Float('Time after prod.', help="Time in minutes for the cleaning.", tracking=True)


    def _compute_cost_per_hour(self):
        for rec in self:
            total_time = sum(rec.oh_time.mapped('cost_hr'))
            total_material = sum(line.consumed * line.product_price for line in rec.ov_material)
            rec.costs_hour = total_time + total_material

    @api.onchange('wc_location')
    def onchange_wc_location(self):
        if self.wc_location:
            if not self.finished_good_loc:
                self.finished_good_loc = self.wc_location.id
            if not self.rejected_good_loc:
                self.rejected_good_loc = self.wc_location.id
            if not self.byproduct_loc:
                self.byproduct_loc = self.wc_location.id
    type = fields.Selection([
        ('asset', 'Asset'),
        ('non_asset', 'Non asset'),
    ], string='Type', default='non_asset')
    asset_id = fields.Many2one('account.asset.asset', 'Asset Name',)
    
    
    @api.onchange('type', 'asset_id')
    def _onchange_type(self):
        for rec in self:
            if rec.type == 'asset':
                rec.name = rec.asset_id.name or ""
            else:
                rec.name = 'WC'
    

                
class OverheadTime(models.Model):
    _name = 'overhead.time'
    _description = 'Overhead Time Management'
    _rec_name = 'product'

    product = fields.Many2one('product.product', string='Product')
    cost_hr = fields.Float(string="Cost/Hour", default=0.0)
    mrp_workcenter_id = fields.Many2one('mrp.workcenter', string='Product')


class OverheadMaterial(models.Model):
    _name = 'overhead.material'
    _description = 'Overhead Material Management'
    _rec_name = 'product'

    product = fields.Many2one('product.product', string='Product')
    product_price = fields.Float(string="Product Cost Price", related='product.standard_price')
    consumed = fields.Float(string='Quantity Consumed/Hour', default=1.0)
    consumed_uom = fields.Many2one('uom.uom', string='Unit of Measure')
    mrp_wc_id = fields.Many2one('mrp.workcenter', string='Workcenter')

    @api.onchange('product')
    def onchange_product(self):
        if self.product and self.product.uom_id:
            self.consumed_uom = self.product.uom_id.id
        else:
            self.consumed_uom = False


class WorkCenterOperation(models.Model):
    _inherit = 'mrp.routing.workcenter'

    @api.depends('work_center', 'work_center_group', 'work_center_group.work_center_group_name',
                 'workcenter_id', 'workcenter_id.name')
    def _compute_operation_name(self):
        for record in self:
            name = ''
            if record.work_center == 'with_group':
                name = record.work_center_group.work_center_group_name
            else:
                if record.workcenter_id:
                    name = record.workcenter_id.name
            record.operation_name = name

    branch = fields.Many2one(related='bom_id.branch')
    workcenter_id = fields.Many2one('mrp.workcenter', 'Name', required=False, domain="[('branch', '=', branch), '|', ('company_id', '=', company_id), ('company_id', '=', False)]")
    wc_location = fields.Many2one(related='workcenter_id.wc_location')
    work_center = fields.Selection([
        ('with_group', 'With Group'),
        ('without_group', 'Without Group'),
    ], string='Work Center', default='without_group')
    w_center = fields.Boolean('W Center')
    work_center_group = fields.Many2one('mrp.work.center.group', 'Work Center Group')
    operation_name = fields.Char(compute=_compute_operation_name)

    def _get_best_workcenter(self):
        workcenter_id = self.env['mrp.workcenter']
        if self.work_center == 'with_group':
            workorder_ids = self.env['mrp.workorder']
            for workcenter in self.work_center_group.mrp_work_center_group_ids:
                workorder_id = self.env['mrp.workorder'].search([
                    ('workcenter_id', '=', workcenter.work_center.id),
                    ('date_planned_finished', '!=', False)
                ], order='date_planned_finished desc', limit=1)
                workorder_ids |= workorder_id

            if workorder_ids:
                workcenter_id = sorted(workorder_ids, key=lambda w: w.date_planned_finished)[0].workcenter_id
        else:
            workcenter_id = self.workcenter_id

        return workcenter_id

    @api.onchange('work_center')
    def _onchange_partner_ids(self):
        if self.work_center == 'with_group':
            self.w_center = True
            self.workcenter_id = False
        else:
            self.w_center = False


class WorkcenterProductivity(models.Model):
    _inherit = "mrp.workcenter.productivity"