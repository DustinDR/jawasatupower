# -*- coding: utf-8 -*-
##############################################################################
#    Copyright (C) 2015 - Present, Braincrew Apps (<http://www.braincrewapps.com>). All Rights Reserved

# Odoo Proprietary License v1.0
#
# This software and associated files (the "Software") may only be used (executed,
# modified, executed after modifications) if you have purchased a valid license
# from the authors, typically via Odoo Apps, braincrewapps.com, or if you have received a written
# agreement from the authors of the Software.
#
# You may develop Odoo modules that use the Software as a library (typically
# by depending on it, importing it and using its resources), but without copying
# any source code or material from the Software. You may distribute those
# modules under the license of your choice, provided that this license is
# compatible with the terms of the Odoo Proprietary License (For example:
# LGPL, MIT, or proprietary licenses similar to this one).
#
# It is forbidden to publish, distribute, sublicense, or sell copies of the Software
# or modified copies of the Software.
#
# The above copyright notice and this permission notice must be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

##############################################################################
from odoo import fields, models, api, _


class MrpBom(models.Model):
	_inherit = 'mrp.bom'

	def _get_default_product_uom_id(self):
		return self.env['uom.uom'].search([], limit=1, order='id').id

	@api.depends('company_id')
	def _compute_allowed_branch(self):
		user_branch = self.env.user.branch_ids
		for record in self:
			allowed_branch = user_branch.filtered(lambda b: b.company_id == record.company_id)
			record.allowed_branch_ids = [(6, 0, allowed_branch.ids)]

	allowed_branch_ids = fields.One2many('res.branch', compute=_compute_allowed_branch)
	branch = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id, domain="[('id', 'in', allowed_branch_ids)]", tracking=True)
	create_uid = fields.Many2one('res.users', string='Create By', default=lambda self: self.env.user, tracking=True)
	product_tmpl_id = fields.Many2one(
		'product.template', 'Product',
		check_company=True, index=True,
		domain="[('type', 'in', ['product', 'consu']), '|', ('company_id', '=', False), ('company_id', '=', company_id)]", required=True, tracking=True)
	product_id = fields.Many2one(
		'product.product', 'Product Variant',
		check_company=True, index=True,
		domain="['&', ('product_tmpl_id', '=', product_tmpl_id), ('type', 'in', ['product', 'consu']),  '|', ('company_id', '=', False), ('company_id', '=', company_id)]",
		help="If a product variant is defined the BOM is available only for this product.", tracking=True)
	product_qty = fields.Float(
		'Quantity', default=1.0,
		digits='Product Unit of Measure', required=True, tracking=True)
	code = fields.Char('Reference', tracking=True)
	type = fields.Selection([
		('normal', 'Manufacture this product'),
		('phantom', 'Kit')], 'BoM Type',
		default='normal', required=True, tracking=True)
	bom_type = fields.Selection([
		('normal', 'Manufacture this product'),
		('phantom', 'Kit')], 'BoM Type',
		default='normal', required=True)

	@api.onchange('bom_type')
	def _onchange_bom_type(self):
		self.type = self.bom_type
	
	can_be_subcontracted = fields.Boolean(
		string='Subcontracted', default=False
	)
	
	company_id = fields.Many2one(
		'res.company', 'Company', index=True, readonly=True, tracking=True,
		default=lambda self: self.env.company)
	product_uom_id = fields.Many2one(
		'uom.uom', 'Unit of Measure',
		default=_get_default_product_uom_id, required=True,
		help="Unit of Measure (Unit of Measure) is the unit of measurement for the inventory control", tracking=True, domain="[('category_id', '=', product_uom_category_id)]")


class MrpByProduct(models.Model):
	_inherit = 'mrp.bom.byproduct'

	allocated_cost = fields.Float(string='Allocated Cost(%)', digits='Product Unit of Measure')


class MrpBomLine(models.Model):
	_inherit = 'mrp.bom.line'

	branch = fields.Many2one(related='bom_id.branch')
	product_id = fields.Many2one(domain="[('type', '=', 'product'), '|', ('company_id', '=', company_id), ('company_id', '=', False)]")
	alternative_component_ids = fields.Many2many('product.product', string='Alternative Material')


class MrpDocument(models.Model):
	_inherit = 'mrp.document'
