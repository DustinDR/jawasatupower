# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools, _

class ProductCategory(models.Model):
    _inherit = "product.category"

    mrp_wip_account_id = fields.Many2one('account.account', string='Manufacturing WIP Account')
    
    @api.model
    def _get_default_hide_wip_account(self):
        if not self.env.company.manufacturing:
            return True
        else:
            return False
    hide_wip_account = fields.Boolean(default=lambda self: self._get_default_hide_wip_account(), string='Hide WIP Account', compute='_compute_hide_wip_account')
    def _compute_hide_wip_account(self):
        for rec in self:
            if not self.env.company.manufacturing:
                rec.hide_wip_account = True
            else:
                rec.hide_wip_account = False