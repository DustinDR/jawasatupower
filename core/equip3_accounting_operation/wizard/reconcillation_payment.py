from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError
from json import dumps
import json

class AccountPayment(models.Model):
    _inherit = "account.payment"

    reconcile_invisible = fields.Boolean(string='reconcile', compute='_check_invisible_reconcile', default = True)

    @api.depends('state','is_reconciled')
    def _check_invisible_reconcile(self):
        self.ensure_one()
        if self.state == 'posted' and self.is_reconciled == False:
            self.reconcile_invisible = False
        else:
            self.reconcile_invisible = True

    def action_reconcile_payment(self):
        print(self.reconciled_invoice_ids)
        name=''
        if self.payment_type:
            if self.payment_type == 'outbound':
                name = 'Reconcile Bill'
            else:
                name = 'Reconcile Invoice'

        return {
                'name':  _(name),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'account.payment.reconcile',
                'views_id': self.env.ref('equip3_accounting_operation.view_form_reconcile_payment').id,
                'domain': [('payment_id', '=', self.id)],
                'context': {'default_date_reconcile': self.date, 'default_payment_id': self.id},
                'target': 'new'
                }

class AccountPaymentReconcile(models.Model):
    _name = "account.payment.reconcile"

    payment_id = fields.Many2one('account.payment', string='Payment')
    payment_type = fields.Selection([
        ('outbound', 'Send Money'),
        ('inbound', 'Receive Money'),
    ], string='Payment Type', related='payment_id.payment_type', required=True)
    partner_id = fields.Many2one('res.partner', string='Partner', related='payment_id.partner_id')    
    date_reconcile = fields.Date(string='Date')
    allocation_line_ids = fields.One2many('account.payment.reconcile.line', 'reconcile_line_id', string='Account Move reconcile')
    payment_amount = fields.Float(string='total Amount', compute='_payment_amount', readonly = True)
    limit_amount = fields.Float(string='Limit Amount', compute='_payment_limit', readonly = True)    
    

    def button_reconcille_payment(self):
        for rec in self.allocation_line_ids:
            if rec.allocation_amount == 0:
                continue
            domain = [('account_internal_type', 'in', ('receivable', 'payable')), ('reconciled', '=', False)]
            payment_lines = self.payment_id.line_ids.filtered_domain(domain)
            lines = payment_lines
            lines += rec.invoice_id.line_ids.filtered(lambda line: line.account_id == lines[0].account_id and not line.reconciled)
            lines.reconcile()

        if self.payment_id.reconciled_invoice_ids:
            self.payment_id.update({'ref' : self._get_batch(self.payment_id.reconciled_invoice_ids)})
        if self.payment_id.reconciled_bill_ids:
            self.payment_id.update({'ref' : self._get_batch(self.payment_id.reconciled_bill_ids)})
        if self.payment_id.reconciled_statement_ids:
            self.payment_id.update({'ref' : self._get_batch(self.payment_id.reconciled_statement_ids)})

    def _get_batch(self, invoice):
        labels = set(line.name for line in invoice)
        return ' '.join(sorted(labels))
        
    @api.depends('payment_id')
    def _payment_amount(self):
        amount = 0
        if self.payment_id:
            domain = [('account_internal_type', 'in', ('receivable', 'payable')), ('reconciled', '=', False)]
            lines = self.payment_id.move_id.line_ids.filtered_domain(domain)
            if lines.account_internal_type == 'receivable':
                amount = lines.amount_residual*-1
            else:
                amount = lines.amount_residual
            self.payment_amount = amount

    @api.depends('allocation_line_ids.invoice_id')
    def _payment_limit(self):
        total = 0
        for x in self.allocation_line_ids:
            total = total + x.allocation_amount
        subtotal = self.payment_amount
        self.limit_amount = subtotal - total
              
    
class AccountPaymentReconcileLine(models.Model):
    _name = "account.payment.reconcile.line"

    reconcile_line_id = fields.Many2one('account.payment.reconcile', string='reconcile line', ondelete="cascade",)    
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, readonly=True, related='invoice_id.currency_id')
    payment_type = fields.Selection([
        ('outbound', 'Send Money'),
        ('inbound', 'Receive Money'),
    ], string='Payment Type', related='reconcile_line_id.payment_id.payment_type', required=True)
    partner_id = fields.Many2one('res.partner', string='Partner', related='reconcile_line_id.partner_id')
    amount_due = fields.Monetary(string='Amount Due Signed', related='invoice_id.amount_residual_signed')
    invoice_id = fields.Many2one('account.move', string='Invoice')
    allocation_amount = fields.Float(string='Allocation Amount', readonly=True)

    @api.onchange('payment_type')
    def _onchange_domain(self):   
        res={}
        if self.payment_type:
            if self.payment_type == 'outbound':
                domain_line = [('state', '=', 'posted'), ('payment_state', 'in', ['not_paid','partial']),('partner_id', '=', self.partner_id.id), ('journal_id.type', '=', 'purchase')]
            else:
                domain_line = [('state', '=', 'posted'), ('payment_state', 'in', ['not_paid','partial']),('partner_id', '=', self.partner_id.id), ('journal_id.type', '=', 'sale')]
        res['domain'] = {'invoice_id' : domain_line}
        return res

    @api.onchange('amount_due')
    def _onchange_amount(self):
        if self.amount_due:
            amount = self.amount_due*-1 if self.amount_due < 0 else self.amount_due
            self.allocation_amount = self.reconcile_line_id.limit_amount if amount > self.reconcile_line_id.limit_amount else amount
