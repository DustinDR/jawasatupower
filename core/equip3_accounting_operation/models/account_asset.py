from odoo import api, fields, models, _


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    user_id = fields.Many2one('res.users',
                              'User',
                              default=lambda self: self.env.user)
    branch_id = fields.Many2one('res.branch',
                                related='user_id.branch_id',
                                readonly=False)

    @api.onchange('date', 'category_id', 'date_first_depreciation')
    def branch_domain(self):
        res = {}
        if self.user_id and self.user_id.branch_ids:
            res = {
                'domain': {
                    'branch_id': [('id', 'in', self.user_id.branch_ids.ids)]
                }
            }
        return res
    
    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        return
    
class AccountAssetDepreciationLine(models.Model):
    _inherit = 'account.asset.depreciation.line'

    currency_id = fields.Many2one(
        'res.currency',
        string='Currency',
        required=True,
        readonly=True,
        default=lambda self: self.env.user.company_id.currency_id.id)
