
import pytz
from pytz import timezone, UTC
from odoo import api, fields, models, _
from datetime import datetime, date, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import UserError, ValidationError


class AccountPayment(models.Model):
    _inherit = "account.payment"

    payment_transaction_credit = fields.One2many('account.multipayment.credit', 'payment_id', string='Children')
    payment_transaction_credit_count = fields.Integer(string="payment transaction count", default='0')
    payment_transaction_debit = fields.One2many('account.multipayment.debit', 'payment_id', string='Children')
    payment_transaction_debit_count = fields.Integer(string="payment transaction count", default='0')

    def button_payment_credit(self):
        self.ensure_one()
        action = {
            'name': _("Payments"),
            'type': 'ir.actions.act_window',
            'res_model': 'account.multipayment.credit',
            'context': {'create': False},
        }
        if len(self.payment_transaction_credit) == 1:
            action.update({
                'view_mode': 'form',
                'res_id': self.payment_transaction_credit.id,
            })
        else:
            action.update({
                'view_mode': 'list,form',
                'domain': [('id', 'in', self.payment_transaction_credit.ids)],
            })
        return action

    def button_payment_debit(self):
        self.ensure_one()
        action = {
            'name': _("Payments"),
            'type': 'ir.actions.act_window',
            'res_model': 'account.multipayment.debit',
            'context': {'create': False},
        }
        if len(self.payment_transaction_debit) == 1:
            action.update({
                'view_mode': 'form',
                'res_id': self.payment_transaction_debit.id,
            })
        else:
            action.update({
                'view_mode': 'list,form',
                'domain': [('id', 'in', self.payment_transaction_debit.ids)],
            })
        return action


class accountmultipayment(models.Model):
    _name = "account.multipayment"
    _description = "Multipayment"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']

    name = fields.Char(string='Number', readonly=True, tracking=True)
    partner_id = fields.Many2one('res.partner', required=True, tracking=True)
    amount = fields.Monetary(string='Receipt Amount', required=True, tracking=True)
    journal_id = fields.Many2one('account.journal', 'Payment Method', required=True,
                                 domain="[('type', 'in', ['bank','cash'])]", tracking=True)
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.company.currency_id, string='Currency',
                                  required=True, tracking=True)
    date = fields.Date(string='Date', required=True, tracking=True)
    ref = fields.Char(string='Reference', tracking=True)
    memo = fields.Char(string='Memo', tracking=True)
    line_credit_ids = fields.One2many('account.multipayment.credit', 'line_id', string='Credits')
    line_debit_ids = fields.One2many('account.multipayment.debit', 'line_id', string='Debits')
    diff_amount = fields.Monetary(string='Payment Difference', compute='_compute_diff_amount', store=True)
    writeoff_account_id = fields.Many2one('account.account', 'Counterpart Account')
    writeoff_label = fields.Char(string='Counterpart Comment')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('to_approve', 'Waiting For Approval'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('post', 'Payment'),
        ('canceled', 'Cancel'),
    ], string='Status', default='draft', tracking=True)
    state1 = fields.Selection(related="state")
    state2 = fields.Selection(related="state")
    partner_type = fields.Selection([
        ('customer', 'Customer'),
        ('supplier', 'Vendor')
    ], string='Partner Type', tracking=True)
    total_amount_credit = fields.Monetary(string="Total Amount", compute='_calculate_credit_amount_total', store=True,
                                          tracking=True)
    total_amount_debit = fields.Monetary(string="Total Amount", compute='_calculate_credit_amount_total', store=True,
                                         tracking=True)
    payment_id_count = fields.Integer(string="Payment", compute='_calculate_credit_amount_total')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company.id,
                                 tracking=True, readonly=True)
    branch_id = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id.id,
                                tracking=True)
    create_uid = fields.Many2one('res.users', string='Created by', readonly=True, tracking=True)
    create_date = fields.Datetime(string="Created Date", readonly=True, tracking=True)
    different_move_id = fields.Many2one('account.move', string='Different Journal', readonly=True, tracking=True)
    request_partner_id = fields.Many2one('res.partner', string="Requested Partner")

    # deleted field
    payment_method = fields.Many2one('account.journal', 'Payment Method', domain="[('type', 'in', ['bank','cash'])]")

    multipul_payment_approval_matrix_id = fields.Many2one('approval.matrix.accounting', string="Approval Matrix", compute='_get_multi_payment_approval_matrix')
    is_multi_payment_approval_matrix = fields.Boolean(string="Is Customer and Vendor Approval Matrix", compute='_get_multi_payment_approve_button_from_config')
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button_multi_receipt', store=False)
    approved_matrix_ids = fields.One2many('approval.matrix.accounting.lines', 'multi_receipt_id', string="Approved Matrix")
    approval_matrix_line_id = fields.Many2one('approval.matrix.accounting.lines', string='Approval Matrix Line', compute='_get_approve_button_multi_receipt', store=False)

    action_validate_boolean = fields.Boolean(string='Confirm', compute='_get_default_invisible')
    action_draft_boolean = fields.Boolean(string='Reset to Draft', compute='_get_default_invisible')
    action_cancel_boolean = fields.Boolean(string='Cancel', compute='')
    request_for_approval_boolean = fields.Boolean(string='Request For Approval', compute='_get_default_invisible')
    action_approved_rp_boolean = fields.Boolean(string='Approved', compute='_get_default_invisible')
    rp_reject_boolean = fields.Boolean(string='Reject', compute='_get_default_invisible')
    state_boolean = fields.Boolean(string='state', compute='_get_default_invisible')
    state1_boolean = fields.Boolean(string='state1', compute='_get_default_invisible')
    state2_boolean = fields.Boolean(string='state2', compute='_get_default_invisible')
    
    difference_ids = fields.One2many('account.multipayment.difference', 'payment_id', string="Difference Accounts")
    payment_difference_amount = fields.Monetary(compute='_compute_payment_difference_amount')
    
    
    @api.depends('difference_ids.payment_amount','diff_amount')
    def _compute_payment_difference_amount(self):
        for payment in self:
            total = sum([line.payment_amount for line in payment.difference_ids]) or 0.00
            payment.payment_difference_amount = total

    @api.depends('is_multi_payment_approval_matrix', 'state', 'is_approve_button')
    @api.onchange('is_multi_payment_approval_matrix', 'state', 'is_approve_button')
    def _get_default_invisible(self):
        if self.state != 'post':
            self.action_validate_boolean = False
            self.action_draft_boolean = False
        else:
            self.action_validate_boolean = True
            self.action_draft_boolean = True

        if self.is_multi_payment_approval_matrix == True:
            self.action_validate_boolean = True
            self.action_draft_boolean = True
            self.state_boolean = True
        else:
            self.state_boolean = False
        
        if self.is_multi_payment_approval_matrix == True and self.state != 'post':
            self.action_cancel_boolean = True
        else:
            self.action_cancel_boolean = False

        if self.is_multi_payment_approval_matrix == False and self.state != 'draft':
            self.request_for_approval_boolean = True
        else:
            self.request_for_approval_boolean = False

        if self.is_multi_payment_approval_matrix == False and self.is_approve_button == False and self.state != 'to_approve':
            self.action_approved_rp_boolean = True
            self.rp_reject_boolean = True
        else:
            self.action_approved_rp_boolean = False
            self.rp_reject_boolean = False
        if self.state1 == 'rejected':
            self.state1_boolean = True
        else:
            if self.is_multi_payment_approval_matrix == False:
                self.state1_boolean = True
            else:
                self.state1_boolean = False
                self.state_boolean = True

        if self.state2 != 'rejected':
            self.state2_boolean = True
        else:
            if self.is_multi_payment_approval_matrix == False:
                self.state2_boolean = True
            else:
                self.state2_boolean = False
                self.state_boolean = True

        # if self.state == 'post':
        #     self.action_validate_boolean = True
        #     self.action_draft_boolean = False

        # if self.state == 'draft':
        #     self.action_draft_boolean = True

# if self.is_multi_payment_approval_matrix == True:
#             self.state_boolean = True
#         else:
#             self.state_boolean = False
        
#         if self.state != 'post':
#             self.action_validate_boolean = True
#             self.action_draft_boolean = True
#             self.action_cancel_boolean = True
#         else:
#             if self.is_multi_payment_approval_matrix == True:
#                 self.action_validate_boolean = True
#                 self.action_draft_boolean = True
#                 self.action_cancel_boolean = True
#             else:
#                 self.action_validate_boolean = False
#                 self.action_draft_boolean = False
#                 self.action_cancel_boolean = False

#         if self.state != 'draft':
#             self.request_for_approval_boolean = True
#         else:
#             if self.is_multi_payment_approval_matrix == False:
#                 self.request_for_approval_boolean = True
#             else:
#                 self.request_for_approval_boolean = False

#         if self.state != 'to_approve':
#             self.action_approved_rp_boolean = True
#             self.rp_reject_boolean = True
#         else:
#             if self.is_approve_button == False:
#                 if self.is_multi_payment_approval_matrix == False:
#                     self.action_approved_rp_boolean = True
#                     self.rp_reject_boolean = True
#                 else:
#                     self.action_approved_rp_boolean = False
#                     self.rp_reject_boolean = False
#             else:
#                 if self.is_multi_payment_approval_matrix == False:
#                     self.action_approved_rp_boolean = True
#                     self.rp_reject_boolean = True
#                 else:
#                     self.action_approved_rp_boolean = False
#                     self.rp_reject_boolean = False

#         if self.state1 == 'rejected':
#             self.state1_boolean = True
#         else:
#             if self.is_multi_payment_approval_matrix == False:
#                 self.state1_boolean = True
#             else:
#                 self.state1_boolean = False

#         if self.state2 != 'rejected':
#             self.state2_boolean = True
#         else:
#             if self.is_multi_payment_approval_matrix == False:
#                 self.state2_boolean = True
#             else:
#                 self.state2_boolean = False
    
    @api.depends('amount', 'company_id', 'branch_id')
    def _get_multi_payment_approval_matrix(self):
        for record in self:
            matrix_id = False
            if record.partner_type == 'customer':
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.amount),
                        ('max_amount', '>=', record.amount),
                        ('approval_matrix_type', '=', 'customer_multi_receipt_approval_matrix')
                    ], limit=1)
            elif record.partner_type == 'supplier':
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.amount),
                        ('max_amount', '>=', record.amount),
                        ('approval_matrix_type', '=', 'vendor_multi_receipt_approval_matrix')
                    ], limit=1)
            elif record.partner_type == 'customer' and record.payment_type == 'giro':
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.amount),
                        ('max_amount', '>=', record.amount),
                        ('approval_matrix_type', '=', 'receipt_giro_approval_matrix')
                    ], limit=1)
            record.multipul_payment_approval_matrix_id = matrix_id

    def _get_multi_payment_approve_button_from_config(self):
        for record in self:
            is_multi_payment_approval_matrix = False
            if record.partner_type == 'customer':
                is_multi_payment_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_customer_multi_receipt_approval_matrix', False)
            elif record.partner_type == 'supplier':
                is_multi_payment_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_vendor_multipayment_approval_matrix', False)
            elif record.payment_type == 'giro' and record.partner_type == 'customer':
                is_multi_payment_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_receipt_giro_approval_matrix', False)
            record.is_multi_payment_approval_matrix = is_multi_payment_approval_matrix

    def _get_approve_button_multi_receipt(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False

    @api.onchange('multipul_payment_approval_matrix_id')
    def _compute_approving_matrix_lines(self):
        data = [(5, 0, 0)]
        for record in self:
            if record.state == 'draft' and record.is_multi_payment_approval_matrix:
                record.approved_matrix_ids = []
                counter = 1
                record.approved_matrix_ids = []
                for rec in record.multipul_payment_approval_matrix_id: 
                    for line in rec.approval_matrix_line_ids:
                        data.append((0, 0, {
                            'sequence' : counter,
                            'user_ids' : [(6, 0, line.user_ids.ids)],
                            'minimum_approver' : line.minimum_approver,
                        }))
                        counter += 1
                record.approved_matrix_ids = data

    def request_for_approval(self):
        for record in self:
            if record.partner_type == "supplier":
                action_id = self.env.ref('equip3_accounting_operation.action_payment_giro')
                template_id = self.env.ref('equip3_accounting_operation.email_template_payment_giro_approval_matrix')
            else:
                action_id = self.env.ref('equip3_accounting_operation.action_receipt_giro')
                template_id = self.env.ref('equip3_accounting_operation.email_template_receipt_giro_approval_matrix')
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = base_url + '/web#id=' + str(record.id) + '&action='+ str(action_id.id) + '&view_type=form&model=account.multipayment'
            record.request_partner_id = self.env.user.partner_id.id
            currency = ""
            if record.currency_id.position == 'before':
                currency = record.currency_id.symbol + str(record.amount)
            else:
                currency = str(record.amount) + ' ' + record.currency_id.symbol
            if record.approved_matrix_ids and len(record.approved_matrix_ids[0].user_ids) > 1:
                for approved_matrix_id in record.approved_matrix_ids[0].user_ids:
                    approver = approved_matrix_id
                    ctx = {
                        'email_from' : self.env.user.company_id.email,
                        'email_to' : approver.partner_id.email,
                        'approver_name' : approver.name,
                        'date': date.today(),
                        'submitter' : self.env.user.name,
                        'url' : url,
                        "due_date": record.due_date,
                        "date_invoice": record.date,
                        "currency": currency,
                    }
                    template_id.with_context(ctx).send_mail(record.id, True)
            else:
                approver = record.approved_matrix_ids[0].user_ids[0]
                ctx = {
                    'email_from' : self.env.user.company_id.email,
                    'email_to' : approver.partner_id.email,
                    'approver_name' : approver.name,
                    'date': date.today(),
                    'submitter' : self.env.user.name,
                    'url' : url,
                    "due_date": record.due_date,
                    "date_invoice": record.date,
                    "currency": currency,
                }
                template_id.with_context(ctx).send_mail(record.id, True)
            record.write({'state': 'to_approve'})

    def action_approved_rp(self):
        for record in self:
            if record.partner_type == "supplier":
                action_id = self.env.ref('equip3_accounting_operation.action_payment_giro')
                template_id = self.env.ref('equip3_accounting_operation.email_template_payment_giro_approval_matrix')
                template_id_submitter = self.env.ref('equip3_accounting_operation.email_template_payment_giro_submitter_approval_matrix')
            else:
                action_id = self.env.ref('equip3_accounting_operation.action_receipt_giro')
                template_id = self.env.ref('equip3_accounting_operation.email_template_receipt_giro_approval_matrix')
                template_id_submitter = self.env.ref('equip3_accounting_operation.email_template_receipt_giro_submitter_approval_matrix')
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = base_url + '/web#id=' + str(record.id) + '&action='+ str(action_id.id) + '&view_type=form&model=account.multipayment'
            user = self.env.user
            currency = ''
            if record.currency_id.position == 'before':
                currency = record.currency_id.symbol + str(record.amount)
            else:
                currency = str(record.amount) + ' ' + record.currency_id.symbol
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_ids.ids and \
                    user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    utc_datetime = datetime.now()
                    local_timezone = pytz.timezone(self.env.user.tz)
                    local_datetime = utc_datetime.replace(tzinfo=pytz.utc)
                    local_datetime = local_datetime.astimezone(local_timezone).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                    if name != '':
                        name += "\n • %s: Approved - %s" % (self.env.user.name, local_datetime)
                    else:
                        name += "• %s: Approved - %s" % (self.env.user.name, local_datetime)

                    approval_matrix_line_id.write({ 
                        'last_approved': self.env.user.id, 'state_char': name, 
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
                        next_approval_matrix_line_id = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
                        if next_approval_matrix_line_id and len(next_approval_matrix_line_id[0].user_ids) > 1:
                             for approving_matrix_line_user in next_approval_matrix_line_id[0].user_ids:
                                ctx = {
                                    'email_from' : self.env.user.company_id.email,
                                    'email_to' : approving_matrix_line_user.partner_id.email,
                                    'approver_name' : approving_matrix_line_user.name,
                                    'date': date.today(),
                                    'submitter' : self.env.user.name,
                                    'url' : url,
                                    "due_date": record.due_date,
                                    "date_invoice": record.date,
                                    "currency": currency,
                                }
                                template_id.sudo().with_context(ctx).send_mail(record.id, True)
                        else:
                            if next_approval_matrix_line_id and next_approval_matrix_line_id[0].user_ids:
                                # approver = record.approved_matrix_ids[0].user_ids[0]
                                ctx = {
                                    'email_from' : self.env.user.company_id.email,
                                    'email_to' : next_approval_matrix_line_id[0].user_ids[0].partner_id.email,
                                    'approver_name' : next_approval_matrix_line_id[0].user_ids[0].name,
                                    'date': date.today(),
                                    'submitter' : self.env.user.name,
                                    'url' : url,
                                    "due_date": record.due_date,
                                    "date_invoice": record.date,
                                    "currency": currency,
                                }
                                template_id.sudo().with_context(ctx).send_mail(record.id, True)
            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r:r.approved)):
                record.write({'state': 'approved'})
                record.action_validate()
                ctx = {
                    'email_from' : self.env.user.company_id.email,
                    'email_to' : record.request_partner_id.email,
                    'approver_name' : record.name,
                    'date': date.today(),
                    'create_date': record.create_date.date(),
                    'submitter' : self.env.user.name,
                    'url' : url,
                    "due_date": record.due_date,
                    "date_invoice": record.date,
                    "currency": currency,
                }
                template_id_submitter.sudo().with_context(ctx).send_mail(record.id, True)

    def rp_reject(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Approval Marix Reject',
            'res_model': 'multi.receipt.matrix.reject',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
        }


    @api.depends('line_credit_ids', 'line_debit_ids', 'line_credit_ids.payment_id', 'line_debit_ids.payment_id')
    def _calculate_credit_amount_total(self):
        for rec in self:
            rec.total_amount_credit = sum(rec.line_credit_ids.mapped('amount'))
            rec.total_amount_debit = sum(rec.line_debit_ids.mapped('amount'))
            payment_count = 0
            if len(rec.line_credit_ids) > 0:
                for record in rec.line_credit_ids:
                    if record.payment_id.id != False:
                        payment_count += 1
            if len(rec.line_debit_ids) > 0:
                for record in rec.line_debit_ids:
                    if record.payment_id.id != False:
                        payment_count += 1
            rec.payment_id_count = payment_count

    @api.onchange('total_amount_credit', 'total_amount_debit')
    def compute_payment_amount(self):
        for rec in self:
            if self.partner_type == 'customer':
                rec.amount = rec.total_amount_debit - rec.total_amount_credit
            else:
                rec.amount = rec.total_amount_credit - rec.total_amount_debit

    @api.depends('amount', 'total_amount_credit', 'total_amount_debit')
    def _compute_diff_amount(self):
        for rec in self:
            if self.partner_type == 'customer':
                rec.diff_amount = rec.amount - (rec.total_amount_debit - rec.total_amount_credit)
            else:
                rec.diff_amount = rec.amount - (rec.total_amount_credit - rec.total_amount_debit)

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            seq_date = None
            if 'date' in vals:
                seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['date']))
            if vals['partner_type'] == 'customer':
                vals['name'] = self.env['ir.sequence'].next_by_code('customer.account.multipayment',
                                                                    sequence_date=seq_date) or _('New')
            elif vals['partner_type'] == 'supplier':
                vals['name'] = self.env['ir.sequence'].next_by_code('vendor.account.multipayment',
                                                                    sequence_date=seq_date) or _('New')
        result = super(accountmultipayment, self).create(vals)
        return result

    @api.onchange('partner_id')
    def _write_line_detail(self):
        for rec in self:
            rec._get_multi_payment_approve_button_from_config()
            list_line_credits = [(5, 0, 0)]
            list_line_debits = [(5, 0, 0)]
            invoices = self.env['account.move'].search(
                ['&', '&', '&', '&', ('partner_id', '=', rec.partner_id.id), ('currency_id', '=', rec.currency_id.id),
                 ('move_type', 'in', ['out_invoice', 'in_refund', 'in_invoice', 'out_refund']),
                 ('state', '=', 'posted'), ('payment_state', 'in', ['not_paid', 'partial']),
                 ('amount_residual_signed', '!=', 0)])
            if invoices:
                for invoice in invoices:
                    if self.partner_type == 'customer':
                        map_account = ['out_invoice', 'out_refund']
                        map_debit = ['out_invoice', 'in_refund']
                        map_credit = ['in_invoice', 'out_refund']

                        lines_dict = {
                            'invoice_id': invoice.id,
                            'currency_id': invoice.currency_id.id,
                            'account_id': invoice.partner_id.property_account_receivable_id if invoice.move_type in map_account else invoice.partner_id.property_account_payable_id,
                            'invoice_date': invoice.invoice_date,
                            'invoice_date_due': invoice.invoice_date_due,
                            'original_amount': invoice.amount_total,
                            'base_amount': abs(invoice.amount_total_signed),
                            'original_unreconcile': invoice.amount_residual,
                            'base_unreconcile': abs(invoice.amount_residual_signed)
                        }

                        if invoice.move_type in map_credit:
                            list_line_credits.append((0, 0, lines_dict))
                        elif invoice.move_type in map_debit:
                            list_line_debits.append((0, 0, lines_dict))

                    elif self.partner_type == 'supplier':
                        map_account = ['out_invoice', 'out_refund']
                        map_debit = ['out_invoice', 'in_refund']
                        map_credit = ['in_invoice', 'out_refund']

                        lines_dict = {
                            'invoice_id': invoice.id,
                            'currency_id': invoice.currency_id.id,
                            'account_id': invoice.partner_id.property_account_receivable_id if invoice.move_type in map_account else invoice.partner_id.property_account_payable_id,
                            'invoice_date': invoice.invoice_date,
                            'invoice_date_due': invoice.invoice_date_due,
                            'original_amount': invoice.amount_total,
                            'base_amount': abs(invoice.amount_total_signed),
                            'original_unreconcile': invoice.amount_residual,
                            'base_unreconcile': abs(invoice.amount_residual_signed)
                        }

                        if invoice.move_type in map_credit:
                            list_line_credits.append((0, 0, lines_dict))
                        elif invoice.move_type in map_debit:
                            list_line_debits.append((0, 0, lines_dict))

            self.line_credit_ids = list_line_credits
            self.line_debit_ids = list_line_debits

    def prepare_payment_vals(self, invoices):
        payment_type_id = ""

        if invoices.invoice_id.move_type in ['out_invoice', 'in_refund']:
            payment_type_id = 'inbound'
        elif invoices.invoice_id.move_type in ['in_invoice', 'out_refund']:
            payment_type_id = 'outbound'

        payment_methods = self.journal_id.outbound_payment_method_ids if self.partner_type == 'customer' else self.journal_id.inbound_payment_method_ids

        payment_vals = {
            'date': self.date,
            'amount': invoices.amount,
            'payment_type': payment_type_id,
            'partner_type': self.partner_type,
            'ref': invoices.invoice_id.name,
            'journal_id': self.journal_id.id,
            'currency_id': self.currency_id.id,
            'partner_id': self.partner_id.id,
            'partner_bank_id': False,
            'payment_method_id': 1,
            'destination_account_id': invoices.account_id.id,
            'apply_manual_currency_exchange': invoices.invoice_id.apply_manual_currency_exchange,
            'manual_currency_exchange_rate': invoices.invoice_id.manual_currency_exchange_rate,
            'active_manual_currency_rate': invoices.invoice_id.active_manual_currency_rate
        }
        payment_difference = invoices.amount - invoices.base_unreconcile

        return payment_vals

    def _get_batches(self, invoices):
        ''' Group the account.move.line linked to the wizard together.
        :return: A list of batches, each one containing:
            * key_values:   The key as a dictionary used to group the journal items together.
            * moves:        An account.move recordset.
        '''
        self.ensure_one()
        # Keep lines having a residual amount to pay.
        available_lines = self.env['account.move.line']
        for line in invoices.line_ids:
            if line.move_id.state != 'posted':
                raise UserError(_("You can only register payment for posted journal entries."))

            if line.account_internal_type not in ('receivable', 'payable'):
                continue
            if line.currency_id:
                if line.currency_id.is_zero(line.amount_residual_currency):
                    continue
            else:
                if line.company_currency_id.is_zero(line.amount_residual):
                    continue
            available_lines |= line

        # Check.
        if not available_lines:
            raise UserError(
                _("You can't register a payment because there is nothing left to pay on the selected journal items."))
        if len(invoices.line_ids.company_id) > 1:
            raise UserError(_("You can't create payments for entries belonging to different companies."))
        if len(set(available_lines.mapped('account_internal_type'))) > 1:
            raise UserError(
                _("You can't register payments for journal items being either all inbound, either all outbound."))

        # res['line_ids'] = [(6, 0, available_lines.ids)]

        lines = available_lines

        if len(lines.company_id) > 1:
            raise UserError(_("You can't create payments for entries belonging to different companies."))
        if not lines:
            raise UserError(
                _("You can't open the register payment wizard without at least one receivable/payable line."))

        batches = {}
        payments = self.env['account.payment.register']
        for line in lines:
            batch_key = payments._get_line_batch_key(line)

            serialized_key = '-'.join(str(v) for v in batch_key.values())
            batches.setdefault(serialized_key, {
                'key_values': batch_key,
                'lines': self.env['account.move.line'],
            })
            batches[serialized_key]['lines'] += line
        return list(batches.values())

    def _create_payments(self, invoices):
        self.ensure_one()
        batches = self._get_batches(invoices.invoice_id)
        edit_mode = True
        to_reconcile = []
        payment_vals = self.prepare_payment_vals(invoices)
        payment_vals_list = [payment_vals]
        to_reconcile.append(batches[0]['lines'])

        payments = self.env['account.payment'].create(payment_vals_list)

        # If payments are made using a currency different than the source one, ensure the balance match exactly in
        # order to fully paid the source journal items.
        # For example, suppose a new currency B having a rate 100:1 regarding the company currency A.
        # If you try to pay 12.15A using 0.12B, the computed balance will be 12.00A for the payment instead of 12.15A.
        if edit_mode:
            for payment, lines in zip(payments, invoices):
                # Batches are made using the same currency so making 'lines.currency_id' is ok.
                if payment.currency_id != lines.currency_id:
                    liquidity_lines, counterpart_lines, writeoff_lines = payment._seek_for_lines()
                    source_balance = abs(sum(lines.mapped('amount_residual')))
                    payment_rate = liquidity_lines[0].amount_currency / liquidity_lines[0].balance
                    source_balance_converted = abs(source_balance) * payment_rate
                    # Translate the balance into the payment currency is order to be able to compare them.
                    # In case in both have the same value (12.15 * 0.01 ~= 0.12 in our example), it means the user
                    # attempt to fully paid the source lines and then, we need to manually fix them to get a perfect
                    # match.
                    payment_balance = abs(sum(counterpart_lines.mapped('balance')))
                    payment_amount_currency = abs(sum(counterpart_lines.mapped('amount_currency')))
                    if not payment.currency_id.is_zero(source_balance_converted - payment_amount_currency):
                        continue

                    delta_balance = source_balance - payment_balance

                    # Balance are already the same.
                    if self.company_currency_id.is_zero(delta_balance):
                        continue

                    # Fix the balance but make sure to peek the liquidity and counterpart lines first.
                    debit_lines = (liquidity_lines + counterpart_lines).filtered('debit')
                    credit_lines = (liquidity_lines + counterpart_lines).filtered('credit')

                    payment.move_id.write({'line_ids': [
                        (1, debit_lines[0].id, {'debit': debit_lines[0].debit + delta_balance}),
                        (1, credit_lines[0].id, {'credit': credit_lines[0].credit + delta_balance}),
                    ]})

        payments.action_post()

        domain = [('account_internal_type', 'in', ('receivable', 'payable')), ('reconciled', '=', False)]
        for payment, lines in zip(payments, to_reconcile):

            # When using the payment tokens, the payment could not be posted at this point (e.g. the transaction failed)
            # and then, we can't perform the reconciliation.
            if payment.state != 'posted':
                continue

            payment_lines = payment.line_ids.filtered_domain(domain)
            for account in payment_lines.account_id:
                (payment_lines + lines) \
                    .filtered_domain([('account_id', '=', account.id), ('reconciled', '=', False)]) \
                    .reconcile()

        return payments

    def action_validate(self):
        for rec in self:
            if len(rec.line_credit_ids) > 0:
                for inv_credit in rec.line_credit_ids:
                    if inv_credit.amount == 0:
                        continue
                    payment = self._create_payments(inv_credit)
                    inv_credit.payment_id = payment.id
                    payment.update({'payment_transaction_credit_count': '1'})
            if len(rec.line_debit_ids) > 0:
                for inv_debit in rec.line_debit_ids:
                    if inv_debit.amount == 0:
                        continue
                    payment = self._create_payments(inv_debit)
                    inv_debit.payment_id = payment.id
                    payment.update({'payment_transaction_debit_count': '1'})
            if self.diff_amount == 0:
                rec.write({'state': 'post'})
            else:
                if rec.diff_amount != rec.payment_difference_amount:
                    raise ValidationError(_("Post Difference Amount are not equal to Difference Amount."))
                moves = self.create_diff_Journal()
                rec.write({'state': 'post', 'different_move_id': moves.id})

    def create_diff_Journal(self):
        line_ids_det = []
        
        default_line_name = self.env['account.move.line']._get_default_line_name(
                    _("Customer Multi Payment") if self.partner_type == 'customer' else _("Vendor Multi Payment"),
                    abs(self.diff_amount),
                    self.currency_id,
                    self.date,
                    partner=self.partner_id,
                )
        
        if self.partner_type == 'customer':

            line = {
                'name': default_line_name,
                'account_id': self.journal_id.default_account_id.id,
                'debit': self.diff_amount if self.diff_amount > 0.0 else 0.0,
                'credit': -self.diff_amount if self.diff_amount < 0.0 else 0.0,
            }
            line_ids_det.append((0, 0, line))
            
            for diff in self.difference_ids:
                line = {
                    'name':  _("Difference Account - ") + diff.name,
                    'account_id': diff.account_id.id,
                    'debit': -diff.payment_amount if diff.payment_amount < 0.0 else 0.0,
                    'credit': diff.payment_amount if diff.payment_amount > 0.0 else 0.0,
                }
                line_ids_det.append((0, 0, line))
            

        elif self.partner_type == 'supplier':
            line = {
                'name': default_line_name,
                'account_id': self.journal_id.default_account_id.id,
                'debit': -self.diff_amount if self.diff_amount < 0.0 else 0.0,
                'credit': self.diff_amount if self.diff_amount > 0.0 else 0.0,
            }
            line_ids_det.append((0, 0, line))

            for diff in self.difference_ids:
                line = {
                    'name':  _("Difference Account - ") + diff.name,
                    'account_id': diff.account_id.id,
                    'debit': diff.payment_amount if diff.payment_amount > 0.0 else 0.0,
                    'credit': -diff.payment_amount if diff.payment_amount < 0.0 else 0.0,
                }
                line_ids_det.append((0, 0, line))
            
        all_move_vals = {
            'ref': default_line_name,
            'date': self.date,
            'journal_id': self.journal_id.id,
            'line_ids': line_ids_det
        }
        AccountMove = self.env['account.move']
        moves = AccountMove.create(all_move_vals)
        moves.post()
        return moves

    def action_cancel(self):
        payment = self.env['account.payment']
        if len(self.line_credit_ids) > 0:
            for rec in self.line_credit_ids:
                if rec.payment_id.id != False:
                    rec.payment_id.action_draft()
                    rec.payment_id.action_cancel()                
        if len(self.line_debit_ids) > 0:
            for rec in self.line_debit_ids:
                if rec.payment_id.id != False:
                    rec.payment_id.action_draft()
                    rec.payment_id.action_cancel()                
        if self.different_move_id:
            self.different_move_id.button_draft()
            self.different_move_id.button_cancel()
        self.write({'state': 'canceled'})

    def action_draft(self):
        payment = self.env['account.payment']
        if len(self.line_credit_ids) > 0:
            for rec in self.line_credit_ids:
                if rec.payment_id.id != False:
                    rec.payment_id.action_draft()
                    rec.payment_id.action_cancel()
                    rec.update({'payment_id' : False})
        if len(self.line_debit_ids) > 0:
            for rec in self.line_debit_ids:
                if rec.payment_id.id != False:
                    rec.payment_id.action_draft()
                    rec.payment_id.action_cancel()
                    rec.update({'payment_id' : False})
        if self.different_move_id:
            self.different_move_id.button_draft()
            self.different_move_id.button_cancel()
            self.update({'different_move_id' : False})
        self.write({'state': 'draft'})

    def button_payment(self):
        self.ensure_one()
        payment_ids = []
        if len(self.line_credit_ids) > 0:
            for rec in self.line_credit_ids:
                if rec.payment_id.id != False:
                    payment_ids.append(rec.payment_id.id)
        if len(self.line_debit_ids) > 0:
            for rec in self.line_debit_ids:
                if rec.payment_id.id != False:
                    payment_ids.append(rec.payment_id.id)
        action = {
            'name': _("Payments"),
            'type': 'ir.actions.act_window',
            'res_model': 'account.payment',
            'context': {'create': False},
        }
        action.update({
            'view_mode': 'list,form',
            'domain': [('id', 'in', payment_ids)],
        })
        return action


class accountmultipaymentcredit(models.Model):
    _name = "account.multipayment.credit"

    line_id = fields.Many2one('account.multipayment', string='Detail', readonly=True)
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.company.currency_id, string='Currency')
    invoice_id = fields.Many2one('account.move', string='Invoice', readonly=True)
    account_id = fields.Many2one('account.account', string='Account', readonly=True)
    invoice_date = fields.Date(string='Date', readonly=True)
    invoice_date_due = fields.Date(string='Due Date', readonly=True)
    original_amount = fields.Monetary(string='Original Currency Amount', readonly=True)
    base_amount = fields.Monetary(string='Base Currency Amount', readonly=True)
    original_unreconcile = fields.Monetary(string='Original Open Balance', readonly=True)
    base_unreconcile = fields.Monetary(string='Base Currency Open Balancet', readonly=True)
    is_full_reconcile = fields.Boolean(string='Full Reconcile')
    amount = fields.Monetary(string='Amount')
    payment_id = fields.Many2one('account.payment', string='Payment')

    @api.onchange('is_full_reconcile')
    def calculate_credit_amount_base(self):
        for rec in self:
            if rec.is_full_reconcile:
                rec.amount = rec.base_unreconcile
            else:
                rec.amount = False


class accountmultipaymentdebit(models.Model):
    _name = "account.multipayment.debit"

    line_id = fields.Many2one('account.multipayment', string='Detail', readonly=True)
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.company.currency_id, string='Currency')
    invoice_id = fields.Many2one('account.move', string='Invoice', readonly=True)
    account_id = fields.Many2one('account.account', string='Account', readonly=True)
    invoice_date = fields.Date(string='Date', readonly=True)
    invoice_date_due = fields.Date(string='Due Date', readonly=True)
    original_amount = fields.Monetary(string='Original Currency Amount', readonly=True)
    base_amount = fields.Monetary(string='Base Currency Amount', readonly=True)
    original_unreconcile = fields.Monetary(string='Original Open Balance', readonly=True)
    base_unreconcile = fields.Monetary(string='Base Currency Open Balancet', readonly=True)
    is_full_reconcile = fields.Boolean(string='Full Reconcile')
    amount = fields.Monetary(string='Amount')
    payment_id = fields.Many2one('account.payment', string='Payment')

    @api.onchange('is_full_reconcile')
    def calculate_debit_amount_base(self):
        for rec in self:
            if rec.is_full_reconcile:
                rec.amount = rec.base_unreconcile
            else:
                rec.amount = False

class ApprovalMatrixAccountingLines(models.Model):
    _inherit = "approval.matrix.accounting.lines"

    multi_receipt_id = fields.Many2one('account.multipayment', string='Multipul Receipt Payment')
    
    
class AccountMultipaymentDifference(models.Model):
    _name = 'account.multipayment.difference'
    _description = 'Payment Difference Lines'
    
    name = fields.Char(string='Description', required=True)
    payment_id = fields.Many2one('account.multipayment', string='Payment')
    account_id = fields.Many2one('account.account', string="Difference Account", copy=False, domain="[('deprecated', '=', False), ('company_id', '=', company_id)]", required=True)
    payment_amount = fields.Monetary(string='Allocation Amount', required=True)
    currency_id = fields.Many2one('res.currency', string='Currency', related='payment_id.currency_id', help="The payment's currency.")
    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True, default=lambda self: self.env.company)