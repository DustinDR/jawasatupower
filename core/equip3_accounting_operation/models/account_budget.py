from odoo import api, fields, models, _


class CrossoveredBudget(models.Model):
    _inherit = "crossovered.budget"

    branch_id = fields.Many2one('res.branch',
                                related='user_id.branch_id',
                                readonly=False)

    @api.onchange('user_id', 'date_from', 'date_to')
    def branch_domain(self):
        res = {}
        if self.user_id and self.user_id.branch_ids:
            res = {
                'domain': {
                    'branch_id': [('id', 'in', self.user_id.branch_ids.ids)]
                }
            }
        return res
    
    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        return
    