
import pytz
from pytz import timezone, UTC
from odoo import models, fields, api, _
from datetime import datetime, date, timedelta
from odoo.exceptions import ValidationError, UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from lxml import etree
import logging

_logger = logging.getLogger(__name__)


class ResCompany(models.Model):
    _inherit = 'res.company'

    def _compute_show_analytic_account(self):
        for data in self:
            if data.user_has_groups('analytic.group_analytic_tags'):
                data.mr_show_analytic_account = False
            else:
                data.mr_show_analytic_account = True

    mr_show_analytic_account = fields.Boolean(
        compute='_compute_show_analytic_account')


class AccountMove(models.Model):
    _inherit = "account.move"

    branch_id = fields.Many2one(
        'res.branch',
        related='user_id.branch_id',
        readonly=False)
    approval_matrix_id = fields.Many2one('approval.matrix.accounting', string="Approval Matrix", compute='_get_approval_matrix')
    is_invoice_approval_matrix = fields.Boolean(string="Is Invoice Approval Matrix", compute='_get_approve_button_from_config')
    approved_matrix_ids = fields.One2many('approval.matrix.accounting.lines', 'move_id', string="Approved Matrix")
    approval_matrix_line_id = fields.Many2one('approval.matrix.accounting.lines', string='Approval Matrix Line', compute='_get_approve_button', store=False)
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button', store=False)
    state = fields.Selection(selection_add=[
        ('to_approve', 'Waiting For Approval'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('posted',)
    ],  ondelete={'to_approve': 'cascade', 'approved': 'cascade', 'rejected': 'cascade'})
    state1 = fields.Selection(related="state", tracking=False)
    state2 = fields.Selection(related="state", tracking=False)
    is_register_payment_done = fields.Boolean(string='Is Register Payment Done')

    analytic_group_ids = fields.Many2many('account.analytic.tag',domain="[('company_id', '=', company_id)]", string="Analytic Group")

    invoice_origin_id = fields.Many2one('account.move', string='Invoice No', readonly=True, copy=False, states={'draft': [('readonly', False)]}, 
                                        domain="[('partner_id','=',partner_id),('move_type','=',(move_type == 'out_refund' and 'out_invoice') or 'in_invoice'),('state','!=','draft')]")
    invoice_origin_date = fields.Date(string='Invoice Date', readonly=True, copy=False, states={'draft': [('readonly', False)]})
    reason = fields.Char(string="Reason", readonly=True, copy=False, states={'draft': [('readonly', False)]})
    ref_no = fields.Text(string="Ref No", readonly=True, copy=False, states={'draft': [('readonly', False)]})
    request_partner_id = fields.Many2one('res.partner', string="Requested Partner")
    is_fiscal_book_exclude = fields.Boolean(string='Exclude on Fiscal Book', default=False)

    @api.model
    def default_get(self, fields):
        res = super(AccountMove, self).default_get(fields)
        analytic_priority_ids = self.env['analytic.priority'].search([], order="priority")        
        for analytic_priority in analytic_priority_ids:
            self.env.user.analytic_tag_ids
            if analytic_priority.object_id == 'user' and self.env.user.analytic_tag_ids:
                analytic_tags_ids = self.env['account.analytic.tag'].search([('id','in',self.env.user.analytic_tag_ids.ids), ('company_id','=', self.company_id.id)])
                self.analytic_group_ids = analytic_tags_ids
                break
            elif analytic_priority.object_id == 'branch' and self.env.user.branch_id.analytic_tag_ids:
                analytic_tags_ids = self.env['account.analytic.tag'].search([('id','in',self.env.user.branch_id.analytic_tag_ids.ids), ('company_id','=', self.company_id.id)])
                self.analytic_group_ids = analytic_tags_ids
                break
        return res

    def action_request_for_approval(self):
        for record in self:
            if record.move_type == "out_refund":
                action_id = self.env.ref('account.action_move_out_refund_type')
                template_id = self.env.ref('equip3_accounting_operation.email_template_credit_notes_approval_matrix')
            elif record.move_type == "in_refund":
                action_id = self.env.ref('account.action_move_in_refund_type')
                template_id = self.env.ref('equip3_accounting_operation.email_template_refunds_approval_matrix')
            else:
                action_id = self.env.ref('account.action_move_out_invoice_type')
                template_id = self.env.ref('equip3_accounting_operation.email_template_inv_approval_matrix')
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = base_url + '/web#id=' + str(record.id) + '&action='+ str(action_id.id) + '&view_type=form&model=account.move'
            currency = ''
            invoice_name = 'Draft Invoice' if record.state != 'posted' else record.name
            if record.currency_id.position == 'before':
               currency = record.currency_id.symbol + ' ' + str(record.amount_total)
            else:
                currency = str(record.amount_total) + ' ' + record.currency_id.symbol
            record.request_partner_id = self.env.user.partner_id.id
            if record.approved_matrix_ids and len(record.approved_matrix_ids[0].user_ids) > 1:
                for approved_matrix_id in record.approved_matrix_ids[0].user_ids:
                    approver = approved_matrix_id
                    ctx = {
                        'email_from' : self.env.user.company_id.email,
                        'email_to' : approver.partner_id.email,
                        'approver_name' : approver.name,
                        'date': date.today(),
                        'submitter' : self.env.user.name,
                        'url' : url,
                        'invoice_name': invoice_name,
                        "due_date": record.invoice_date_due,
                        "date_invoice": record.invoice_date,
                        "currency": currency,
                    }
                    template_id.with_context(ctx).send_mail(record.id, True)
            else:
                approver = record.approved_matrix_ids[0].user_ids[0]
                ctx = {
                    'email_from' : self.env.user.company_id.email,
                    'email_to' : approver.partner_id.email,
                    'approver_name' : approver.name,
                    'date': date.today(),
                    'submitter' : self.env.user.name,
                    'url' : url,
                    'invoice_name': invoice_name,
                    "due_date": record.invoice_date_due,
                    "date_invoice": record.invoice_date,
                    "currency": currency,
                }
                template_id.with_context(ctx).send_mail(record.id, True)
            record.write({'state': 'to_approve'})

    def action_approve(self):
        for record in self:
            if record.move_type == "out_refund":
                action_id = self.env.ref('account.action_move_out_refund_type')
                template_id = self.env.ref('equip3_accounting_operation.email_template_credit_notes_approval_matrix')
                template_id_submitter = self.env.ref('equip3_accounting_operation.email_template_credit_notes_submitter_approval_matrix')
            elif record.move_type == "in_refund":
                action_id = self.env.ref('account.action_move_in_refund_type')
                template_id = self.env.ref('equip3_accounting_operation.email_template_refunds_approval_matrix')
                template_id_submitter = self.env.ref('equip3_accounting_operation.email_template_refunds_submitter_approval_matrix')
            else:
                action_id = self.env.ref('account.action_move_out_invoice_type')
                template_id = self.env.ref('equip3_accounting_operation.email_template_inv_approval_matrix')
                template_id_submitter = self.env.ref('equip3_accounting_operation.email_template_submitter_approval_matrix')
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = base_url + '/web#id=' + str(record.id) + '&action='+ str(action_id.id) + '&view_type=form&model=account.move'
            user = self.env.user
            currency = ''
            invoice_name = 'Draft Invoice' if record.state != 'posted' else record.name
            if record.currency_id.position == 'before':
               currency = record.currency_id.symbol + str(record.amount_total)
            else:
                currency = str(record.amount_total) + ' ' + record.currency_id.symbol
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_ids.ids and \
                    user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    utc_datetime = datetime.now()
                    local_timezone = pytz.timezone(self.env.user.tz)
                    local_datetime = utc_datetime.replace(tzinfo=pytz.utc)
                    local_datetime = local_datetime.astimezone(local_timezone).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                    if name != '':
                        name += "\n • %s: Approved - %s" % (self.env.user.name, local_datetime)
                    else:
                        name += "• %s: Approved - %s" % (self.env.user.name, local_datetime)

                    approval_matrix_line_id.write({ 
                        'last_approved': self.env.user.id, 'state_char': name, 
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
                        next_approval_matrix_line_id = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
                        if next_approval_matrix_line_id and len(next_approval_matrix_line_id[0].user_ids) > 1:
                            for approving_matrix_line_user in next_approval_matrix_line_id[0].user_ids:
                                ctx = {
                                    'email_from' : self.env.user.company_id.email,
                                    'email_to' : approving_matrix_line_user.partner_id.email,
                                    'approver_name' : approving_matrix_line_user.name,
                                    'date': date.today(),
                                    'submitter' : self.env.user.name,
                                    'url' : url,
                                    'invoice_name': invoice_name,
                                    "due_date": record.invoice_date_due,
                                    "date_invoice": record.invoice_date,
                                    "currency": currency,
                                }
                                template_id.sudo().with_context(ctx).send_mail(record.id, True)
                        else:
                            if next_approval_matrix_line_id and next_approval_matrix_line_id[0].user_ids:
                                # approver = record.approved_matrix_ids[0].user_ids[0]
                                ctx = {
                                    'email_from' : self.env.user.company_id.email,
                                    'email_to' : next_approval_matrix_line_id[0].user_ids[0].partner_id.email,
                                    'approver_name' : next_approval_matrix_line_id[0].user_ids[0].name,
                                    'date': date.today(),
                                    'submitter' : self.env.user.name,
                                    'url' : url,
                                    'invoice_name': invoice_name,
                                    "due_date": record.invoice_date_due,
                                    "date_invoice": record.invoice_date,
                                    "currency": currency,
                                }
                                template_id.sudo().with_context(ctx).send_mail(record.id, True)
            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r:r.approved)):
                record.write({'state': 'approved'})
                record.action_post()
                if record.move_type in ("out_refund", "in_refund"):
                    email_to = record.request_partner_id.email
                else:
                    email_to = record.partner_id.email
                ctx = {
                    'email_from' : self.env.user.company_id.email,
                    'email_to' : email_to,
                    'approver_name' : record.name,
                    'date': date.today(),
                    'create_date': record.create_date.date(),
                    'submitter' : self.env.user.name,
                    'url' : url,
                    'invoice_name': invoice_name,
                    "due_date": record.invoice_date_due,
                    "date_invoice": record.invoice_date,
                    "currency": currency,
                }
                template_id_submitter.sudo().with_context(ctx).send_mail(record.id, True)

    def action_reject(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Accounting Matrix Reject ',
            'res_model': 'accounting.matrix.reject',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
        }

    @api.depends('amount_total', 'company_id', 'branch_id')
    def _get_approval_matrix(self):
        for record in self:
            matrix_id = False
            if record.move_type == "out_invoice":
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.amount_total),
                        ('max_amount', '>=', record.amount_total),
                        ('approval_matrix_type', '=', 'invoice')
                    ], limit=1)
            elif record.move_type == "in_invoice":
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.amount_total),
                        ('max_amount', '>=', record.amount_total),
                        ('approval_matrix_type', '=', 'bill')
                    ], limit=1)
            elif record.move_type == "out_refund":
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.amount_total),
                        ('max_amount', '>=', record.amount_total),
                        ('approval_matrix_type', '=', 'credit_note')
                    ], limit=1)
            elif record.move_type == "in_refund":
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.amount_total),
                        ('max_amount', '>=', record.amount_total),
                        ('approval_matrix_type', '=', 'refund_approval_matrix')
                    ], limit=1)
            record.approval_matrix_id = matrix_id
            record._compute_approving_matrix_lines()

    def _get_approve_button_from_config(self):
        for record in self:
            is_invoice_approval_matrix = False
            if record.move_type == 'out_invoice':
                is_invoice_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_invoice_approval_matrix', False)
            elif record.move_type == 'in_invoice':
                is_invoice_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_bill_approval_matrix', False)
            elif record.move_type == 'out_refund':
                is_invoice_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_credit_note_approval_matrix', False)
            elif record.move_type == 'in_refund':
                is_invoice_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_refund_approval_matrix', False)
            record.is_invoice_approval_matrix = is_invoice_approval_matrix

    def _get_approve_button(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False


    @api.onchange('invoice_origin_id')
    def _onchange_invoice_origin_id(self):
        for record in self:
            if record.state == 'draft':
                invoice_origin_id = record.invoice_origin_id
                new_invoice_line_ids = [(5,0,0)]
                
                invoice_lines = invoice_origin_id.invoice_line_ids
                for inv_line in invoice_lines:
                    # Fill missing 'account_id and description'.
                    journal = self.env['account.journal'].browse(self._context.get('default_journal_id') or self._context.get('journal_id') or self.journal_id.id)
                    product = inv_line.product_id
                    
                    values = []
                    if product.partner_ref:
                        values.append(product.partner_ref)
                    if journal.type == 'sale':
                        if product.description_sale:
                            values.append(product.description_sale)
                    elif journal.type == 'purchase':
                        if product.description_purchase:
                            values.append(product.description_purchase)
                    name = '\n'.join(values)

                    new_invoice_line_ids.append((0,0,{'product_id' : inv_line.product_id and inv_line.product_id.id or False, 
                                                      'name' : inv_line.name or name, 
                                                      'account_id' : inv_line.account_id.id or (journal and journal.default_account_id.id),
                                                      'quantity' : inv_line.quantity or 0.0,
                                                      'price_unit' : inv_line.price_unit or 0.0,
                                                      'product_uom_id' : inv_line.product_uom_id.id,
                                                      'exclude_from_invoice_tab' : inv_line.exclude_from_invoice_tab,
                                                      'tax_ids' : [(6,0,[x.id for x in inv_line.tax_ids])],
                                                      'price_tax' : inv_line.price_tax,
                                                      'amount_currency' : inv_line.amount_currency,
                                                      'currency_id' : inv_line.currency_id.id,
                                                      'debit' : inv_line.credit,
                                                      'credit' : inv_line.debit,
                                            }))

                record.invoice_line_ids = new_invoice_line_ids
                record._onchange_invoice_line_ids()
                record.invoice_origin_date = invoice_origin_id and invoice_origin_id.invoice_date or False
                

    @api.onchange('approval_matrix_id')
    def _compute_approving_matrix_lines(self):
        data = [(5, 0, 0)]
        for record in self:
            if record.state == 'draft' and record.is_invoice_approval_matrix:
                record.approved_matrix_ids = []
                counter = 1
                record.approved_matrix_ids = []
                for rec in record.approval_matrix_id: 
                    for line in rec.approval_matrix_line_ids:
                        data.append((0, 0, {
                            'sequence' : counter,
                            'user_ids' : [(6, 0, line.user_ids.ids)],
                            'minimum_approver' : line.minimum_approver,
                        }))
                        counter += 1
                record.approved_matrix_ids = data

    @api.onchange('partner_id','attn','ref','date')
    def branch_domain(self):
        res={}
        self._get_approve_button_from_config()
        if self.user_id and self.user_id.branch_ids:
            res={
            'domain': {
            'branch_id': [('id', 'in', self.user_id.branch_ids.ids)]
            }
        }
        return res 
    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        return


    @api.model
    def _get_default_journal(self):
        ''' Get the default journal.
        It could either be passed through the context using the 'default_journal_id' key containing its id,
        either be determined by the default type.
        '''
        move_type = self._context.get('default_move_type', 'entry')
        if move_type in self.get_sale_types(include_receipts=True):
            journal_types = ['sale']
        elif move_type in self.get_purchase_types(include_receipts=True):
            journal_types = ['purchase']
        else:
            journal_types = self._context.get('default_move_journal_types',
                                              ['general'])

        if self._context.get('default_journal_id'):
            journal = self.env['account.journal'].browse(
                self._context['default_journal_id'])

            if move_type != 'entry' and journal.type not in journal_types:
                raise UserError(
                    _(
                        "Cannot create an invoice of type %(move_type)s with a journal having %(journal_type)s as type.",
                        move_type=move_type,
                        journal_type=journal.type,
                    ))
        else:
            journal = self._search_default_journal(journal_types)

        return journal

    @api.model
    def _get_default_invoice_incoterm(self):
        ''' Get the default incoterm for invoice. '''
        return self.env.company.incoterm_id

    show_name_warning = fields.Boolean(store=False, tracking=True)
    date = fields.Date(string='Date',
                       required=True,
                       index=True,
                       readonly=True,
                       states={'draft': [('readonly', False)]},
                       copy=False,
                       default=fields.Date.context_today,
                       tracking=True)
    narration = fields.Text(string='Terms and Conditions', tracking=True)
    posted_before = fields.Boolean(
        help="Technical field for knowing if the move has been posted before",
        copy=False,
        tracking=True)
    to_check = fields.Boolean(
        string='To Check',
        default=False,
        tracking=True,
        help=
        'If this checkbox is ticked, it means that the user was not sure of all the related information at the time of the creation of the move and that the move needs to be checked again.'
    )
    journal_id = fields.Many2one('account.journal',
                                 string='Journal',
                                 required=True,
                                 readonly=True,
                                 states={'draft': [('readonly', False)]},
                                 check_company=True,
                                 domain="[('id', 'in', suitable_journal_ids)]",
                                 default=_get_default_journal,
                                 tracking=True)
    company_currency_id = fields.Many2one(string='Company Currency',
                                          readonly=True,
                                          related='company_id.currency_id',
                                          tracking=True)
    line_ids = fields.One2many('account.move.line',
                               'move_id',
                               string='Journal Items',
                               copy=True,
                               readonly=True,
                               tracking=True,
                               states={'draft': [('readonly', False)]})
    commercial_partner_id = fields.Many2one(
        'res.partner',
        string='Commercial Entity',
        store=True,
        readonly=True,
        tracking=True,
        compute='_compute_commercial_partner_id')
    partner_bank_id = fields.Many2one(
        'res.partner.bank',
        string='Recipient Bank',
        tracking=True,
        help=
        'Bank Account Number to which the invoice will be paid. A Company bank account if this is a Customer Invoice or Vendor Credit Note, otherwise a Partner bank account number.',
        check_company=True)
    payment_reference = fields.Char(
        string='Payment Reference',
        index=True,
        copy=False,
        tracking=True,
        help="The payment reference to set on journal items.")
    payment_id = fields.Many2one(index=True,
                                 comodel_name='account.payment',
                                 tracking=True,
                                 string="Payment",
                                 copy=False,
                                 check_company=True)
    statement_line_id = fields.Many2one(
        comodel_name='account.bank.statement.line',
        tracking=True,
        string="Statement Line",
        copy=False,
        check_company=True)

    # === Amount fields ===
    amount_tax = fields.Monetary(string='Tax',
                                 store=True,
                                 readonly=True,
                                 tracking=True,
                                 compute='_compute_amount')
    amount_total = fields.Monetary(string='Total',
                                   store=True,
                                   readonly=True,
                                   tracking=True,
                                   compute='_compute_amount',
                                   inverse='_inverse_amount_total')
    amount_residual = fields.Monetary(string='Amount Due',
                                      store=True,
                                      tracking=True,
                                      compute='_compute_amount')
    amount_untaxed_signed = fields.Monetary(
        string='Untaxed Amount Signed',
        store=True,
        readonly=True,
        tracking=True,
        compute='_compute_amount',
        currency_field='company_currency_id')
    amount_tax_signed = fields.Monetary(string='Tax Signed',
                                        store=True,
                                        readonly=True,
                                        tracking=True,
                                        compute='_compute_amount',
                                        currency_field='company_currency_id')
    amount_total_signed = fields.Monetary(string='Total Signed',
                                          store=True,
                                          readonly=True,
                                          tracking=True,
                                          compute='_compute_amount',
                                          currency_field='company_currency_id')
    amount_residual_signed = fields.Monetary(
        string='Amount Due Signed',
        store=True,
        tracking=True,
        compute='_compute_amount',
        currency_field='company_currency_id')
    amount_by_group = fields.Binary(
        string="Tax amount by group",
        tracking=True,
        compute='_compute_invoice_taxes_by_group',
        help='Edit Tax amounts if you encounter rounding issues.')

    # ==== Cash basis feature fields ====
    tax_cash_basis_rec_id = fields.Many2one(
        'account.partial.reconcile',
        string='Tax Cash Basis Entry of',
        tracking=True,
        help=
        "Technical field used to keep track of the tax cash basis reconciliation. "
        "This is needed when cancelling the source: it will post the inverse journal entry to cancel that part too."
    )
    tax_cash_basis_move_id = fields.Many2one(
        comodel_name='account.move',
        string="Origin Tax Cash Basis Entry",
        tracking=True,
        help=
        "The journal entry from which this tax cash basis journal entry has been created."
    )

    # ==== Auto-post feature fields ====
    auto_post = fields.Boolean(
        string='Post Automatically',
        default=False,
        copy=False,
        tracking=True,
        help=
        'If this checkbox is ticked, this entry will be automatically posted at its date.'
    )

    # ==== Reverse feature fields ====
    reversed_entry_id = fields.Many2one('account.move',
                                        string="Reversal of",
                                        readonly=True,
                                        copy=False,
                                        tracking=True,
                                        check_company=True)
    reversal_move_id = fields.One2many('account.move',
                                       'reversed_entry_id',
                                       tracking=True)

    # =========================================================
    # Invoice related fields
    # =========================================================

    # ==== Business fields ====
    fiscal_position_id = fields.Many2one(
        'account.fiscal.position',
        string='Fiscal Position',
        readonly=True,
        states={'draft': [('readonly', False)]},
        check_company=True,
        tracking=True,
        domain="[('company_id', '=', company_id)]",
        ondelete="restrict",
        help=
        "Fiscal positions are used to adapt taxes and accounts for particular customers or sales orders/invoices. "
        "The default value comes from the customer.")
    invoice_date = fields.Date(string='Invoice/Bill Date',
                               readonly=True,
                               index=True,
                               copy=False,
                               tracking=True,
                               states={'draft': [('readonly', False)]})
    invoice_date_due = fields.Date(string='Due Date',
                                   readonly=True,
                                   index=True,
                                   copy=False,
                                   tracking=True,
                                   states={'draft': [('readonly', False)]})
    invoice_payment_term_id = fields.Many2one(
        'account.payment.term',
        string='Payment Terms',
        check_company=True,
        tracking=True,
        readonly=True,
        states={'draft': [('readonly', False)]})
    # /!\ invoice_line_ids is just a subset of line_ids.
    invoice_line_ids = fields.One2many('account.move.line',
                                       'move_id',
                                       string='Invoice lines',
                                       tracking=True,
                                       copy=False,
                                       readonly=True,
                                       domain=[('exclude_from_invoice_tab',
                                                '=', False)],
                                       states={'draft': [('readonly', False)]})
    invoice_incoterm_id = fields.Many2one(
        'account.incoterms',
        string='Incoterm',
        tracking=True,
        default=_get_default_invoice_incoterm,
        help=
        'International Commercial Terms are a series of predefined commercial terms used in international transactions.'
    )
    display_qr_code = fields.Boolean(string="Display QR-code",
                                     related='company_id.qr_code',
                                     tracking=True)
    qr_code_method = fields.Selection(
        string="Payment QR-code",
        tracking=True,
        selection=lambda self: self.env[
            'res.partner.bank'].get_available_qr_methods_in_sequence(),
        help=
        "Type of QR-code to be generated for the payment of this invoice, when printing it. If left blank, the first available and usable method will be used."
    )

    # ==== Payment widget fields ====
    invoice_outstanding_credits_debits_widget = fields.Text(
        groups="account.group_account_invoice,account.group_account_readonly",
        tracking=True,
        compute='_compute_payments_widget_to_reconcile_info')
    invoice_has_outstanding = fields.Boolean(
        groups="account.group_account_invoice,account.group_account_readonly",
        tracking=True,
        compute='_compute_payments_widget_to_reconcile_info')
    invoice_payments_widget = fields.Text(
        groups="account.group_account_invoice,account.group_account_readonly",
        tracking=True,
        compute='_compute_payments_widget_reconciled_info')

    # ==== Vendor bill fields ====
    invoice_vendor_bill_id = fields.Many2one(
        'account.move',
        store=False,
        tracking=True,
        check_company=True,
        string='Vendor Bill',
        help="Auto-complete from a past bill.")
    invoice_partner_display_name = fields.Char(
        compute='_compute_invoice_partner_display_info',
        tracking=True,
        store=True)

    # ==== Cash rounding fields ====
    invoice_cash_rounding_id = fields.Many2one(
        'account.cash.rounding',
        string='Cash Rounding Method',
        tracking=True,
        readonly=True,
        states={'draft': [('readonly', False)]},
        help=
        'Defines the smallest coinage of the currency that can be used to pay by cash.'
    )

    # ==== Display purpose fields ====
    invoice_filter_type_domain = fields.Char(
        compute='_compute_invoice_filter_type_domain',
        tracking=True,
        help=
        "Technical field used to have a dynamic domain on journal / taxes in the form view."
    )
    bank_partner_id = fields.Many2one(
        'res.partner',
        tracking=True,
        help='Technical field to get the domain on the bank',
        compute='_compute_bank_partner_id')
    invoice_has_matching_suspense_amount = fields.Boolean(
        compute='_compute_has_matching_suspense_amount',
        tracking=True,
        groups='account.group_account_invoice,account.group_account_readonly',
        help=
        "Technical field used to display an alert on invoices if there is at least a matching amount in any supsense account."
    )
    tax_lock_date_message = fields.Char(
        compute='_compute_tax_lock_date_message',
        tracking=True,
        help=
        "Technical field used to display a message when the invoice's accounting date is prior of the tax lock date."
    )
    # Technical field to hide Reconciled Entries stat button
    has_reconciled_entries = fields.Boolean(
        compute="_compute_has_reconciled_entries", tracking=True)
    show_reset_to_draft_button = fields.Boolean(
        compute='_compute_show_reset_to_draft_button', tracking=True)

    # ==== Hash Fields ====
    restrict_mode_hash_table = fields.Boolean(
        related='journal_id.restrict_mode_hash_table', tracking=True)
    secure_sequence_number = fields.Integer(
        string="Inalteralbility No Gap Sequence #",
        tracking=True,
        readonly=True,
        copy=False)
    inalterable_hash = fields.Char(string="Inalterability Hash",
                                   tracking=True,
                                   readonly=True,
                                   copy=False)
    string_to_hash = fields.Char(compute='_compute_string_to_hash',
                                 tracking=True,
                                 readonly=True)

    mr_show_analytic_account = fields.Boolean(
        related='company_id.mr_show_analytic_account')

    def action_post(self):
        if self.period_id.id == False:
            raise UserError("Please define the Fiscal Year and Period first before post any Journal Entry")
        elif self.period_id.id != False and self.period_id.state == 'done':
            raise UserError("You can not post any journal entry already on Closed Period")
        self._post(soft=False)
        return False

    def button_draft(self):
        if self.period_id.id == False:
            raise UserError("Please define the Fiscal Year and Period first before  reset to draft any Journal Entry")
        elif self.period_id.id != False and self.period_id.state == 'done':
            raise UserError("You can not reset to draft any journal entry already on Closed Period")

        AccountMoveLine = self.env['account.move.line']
        excluded_move_ids = []

        if self._context.get('suspense_moves_mode'):
            excluded_move_ids = AccountMoveLine.search(AccountMoveLine._get_suspense_moves_domain() + [('move_id', 'in', self.ids)]).mapped('move_id').ids

        for move in self:
            if move in move.line_ids.mapped('full_reconcile_id.exchange_move_id'):
                raise UserError(_('You cannot reset to draft an exchange difference journal entry.'))
            if move.tax_cash_basis_rec_id:
                raise UserError(_('You cannot reset to draft a tax cash basis journal entry.'))
            if move.restrict_mode_hash_table and move.state == 'posted' and move.id not in excluded_move_ids:
                raise UserError(_('You cannot modify a posted entry of this journal because it is in strict mode.'))
            # We remove all the analytics entries for this journal
            move.mapped('line_ids.analytic_line_ids').unlink()

        self.mapped('line_ids').remove_move_reconcile()
        self.write({'state': 'draft', 'is_move_sent': False})

    @api.onchange('journal_id')
    def _onchange_journal_id_fs_book(self):
        for sheet in self:
            if sheet.journal_id:
                sheet.is_fiscal_book_exclude = sheet.journal_id.is_fiscal_book_exclude

class ValidateAccountMove(models.TransientModel):
    _inherit = "validate.account.move"

    def validate_move(self):
        if self._context.get('active_model') == 'account.move':
            domain = [('id', 'in', self._context.get('active_ids', [])), ('state', '=', 'draft')]
        elif self._context.get('active_model') == 'account.journal':
            domain = [('journal_id', '=', self._context.get('active_id')), ('state', '=', 'draft')]
        else:
            raise UserError(_("Missing 'active_model' in context."))

        moves = self.env['account.move'].search(domain).filtered('line_ids')
        if not moves:
            raise UserError(_('There are no journal items in the draft state to post.'))
        for move in moves:            
            if move.period_id.id == False:
                raise UserError("Please define the Fiscal Year and Period first before  reset to draft any Journal Entry")
            elif move.period_id.id != False and move.period_id.state == 'done':
                raise UserError("You can not reset to draft any journal entry already on Closed Period")        
        moves._post(not self.force_post)
        return {'type': 'ir.actions.act_window_close'}

class AccountMoveLine(models.Model):
    _name = "account.move.line"
    _inherit = ['account.move.line', 'mail.thread', 'mail.activity.mixin']

    move_id = fields.Many2one('account.move',
                              string='Journal Entry',
                              index=True,
                              required=True,
                              readonly=True,
                              auto_join=True,
                              ondelete="cascade",
                              check_company=True,
                              tracking=True,
                              help="The move of this entry line.")
    move_name = fields.Char(string='Number',
                            tracking=True,
                            related='move_id.name',
                            store=True,
                            index=True)
    date = fields.Date(related='move_id.date',
                       tracking=True,
                       store=True,
                       readonly=True,
                       index=True,
                       copy=False,
                       group_operator='min')
    ref = fields.Char(related='move_id.ref',
                      tracking=True,
                      store=True,
                      copy=False,
                      index=True,
                      readonly=False)
    parent_state = fields.Selection(related='move_id.state',
                                    tracking=True,
                                    store=True,
                                    readonly=True)
    journal_id = fields.Many2one(related='move_id.journal_id',
                                 tracking=True,
                                 store=True,
                                 index=True,
                                 copy=False)
    company_id = fields.Many2one(related='move_id.company_id',
                                 store=True,
                                 tracking=True,
                                 readonly=True,
                                 default=lambda self: self.env.company)
    company_currency_id = fields.Many2one(
        related='company_id.currency_id',
        string='Company Currency',
        tracking=True,
        readonly=True,
        store=True,
        help='Utility field to express amount currency')
    tax_fiscal_country_id = fields.Many2one(
        comodel_name='res.country',
        tracking=True,
        related='move_id.company_id.account_tax_fiscal_country_id')
    account_id = fields.Many2one(
        'account.account',
        string='Account',
        index=True,
        ondelete="cascade",
        domain=
        "[('deprecated', '=', False), ('company_id', '=', 'company_id'),('is_off_balance', '=', False)]",
        check_company=True,
        tracking=True)
    account_internal_type = fields.Selection(
        related='account_id.user_type_id.type',
        tracking=True,
        string="Internal Type",
        readonly=True)
    account_internal_group = fields.Selection(
        related='account_id.user_type_id.internal_group',
        tracking=True,
        string="Internal Group",
        readonly=True)
    account_root_id = fields.Many2one(related='account_id.root_id',
                                      tracking=True,
                                      string="Account Root",
                                      store=True,
                                      readonly=True)
    sequence = fields.Integer(default=10, tracking=True)
    name = fields.Char(string='Label', tracking=True)
    quantity = fields.Float(
        string='Quantity',
        tracking=True,
        default=1.0,
        digits='Product Unit of Measure',
        help=
        "The optional quantity expressed by this line, eg: number of product sold. "
        "The quantity is not a legal requirement but is very useful for some reports."
    )
    price_unit = fields.Float(string='Unit Price',
                              tracking=True,
                              digits='Product Price')
    discount = fields.Float(string='Discount (%)',
                            tracking=True,
                            digits='Discount',
                            default=0.0)
    debit = fields.Monetary(string='Debit',
                            tracking=True,
                            default=0.0,
                            currency_field='company_currency_id')
    credit = fields.Monetary(string='Credit',
                             tracking=True,
                             default=0.0,
                             currency_field='company_currency_id')
    balance = fields.Monetary(
        string='Balance',
        tracking=True,
        store=True,
        currency_field='company_currency_id',
        compute='_compute_balance',
        help=
        "Technical field holding the debit - credit in order to open meaningful graph views from reports"
    )
    cumulated_balance = fields.Monetary(
        string='Cumulated Balance',
        tracking=True,
        store=False,
        currency_field='company_currency_id',
        compute='_compute_cumulated_balance',
        help=
        "Cumulated balance depending on the domain and the order chosen in the view."
    )
    amount_currency = fields.Monetary(
        string='Amount in Currency',
        tracking=True,
        store=True,
        copy=True,
        help=
        "The amount expressed in an optional other currency if it is a multi-currency entry."
    )
    price_subtotal = fields.Monetary(string='Subtotal',
                                     tracking=True,
                                     store=True,
                                     readonly=True,
                                     currency_field='currency_id')
    price_total = fields.Monetary(string='Total',
                                  tracking=True,
                                  store=True,
                                  readonly=True,
                                  currency_field='currency_id')
    reconciled = fields.Boolean(compute='_compute_amount_residual',
                                tracking=True,
                                store=True)
    blocked = fields.Boolean(
        string='No Follow-up',
        tracking=True,
        default=False,
        help=
        "You can check this box to mark this journal item as a litigation with the associated partner"
    )
    date_maturity = fields.Date(
        string='Due Date',
        index=True,
        tracking=True,
        help=
        "This field is used for payable and receivable journal entries. You can put the limit date for the payment of this line."
    )
    currency_id = fields.Many2one('res.currency',
                                  string='Currency',
                                  tracking=True,
                                  required=True)
    partner_id = fields.Many2one('res.partner',
                                 string='Partner',
                                 tracking=True,
                                 ondelete='restrict')
    product_uom_id = fields.Many2one(
        'uom.uom',
        string='Unit of Measure',
        tracking=True,
        domain="[('category_id', '=', product_uom_category_id)]")
    product_id = fields.Many2one('product.product',
                                 string='Product',
                                 tracking=True,
                                 ondelete='restrict')
    product_uom_category_id = fields.Many2one(
        'uom.category', tracking=True, related='product_id.uom_id.category_id')

    # ==== Origin fields ====
    reconcile_model_id = fields.Many2one('account.reconcile.model',
                                         string="Reconciliation Model",
                                         tracking=True,
                                         copy=False,
                                         readonly=True,
                                         check_company=True)
    payment_id = fields.Many2one('account.payment',
                                 index=True,
                                 tracking=True,
                                 store=True,
                                 string="Originator Payment",
                                 related='move_id.payment_id',
                                 help="The payment that created this entry")
    statement_line_id = fields.Many2one(
        'account.bank.statement.line',
        index=True,
        tracking=True,
        store=True,
        string="Originator Statement Line",
        related='move_id.statement_line_id',
        help="The statement line that created this entry")
    statement_id = fields.Many2one(
        related='statement_line_id.statement_id',
        tracking=True,
        store=True,
        index=True,
        copy=False,
        help="The bank statement used for bank reconciliation")

    # ==== Tax fields ====
    tax_ids = fields.Many2many(comodel_name='account.tax',
                               string="Taxes",
                               context={'active_test': False},
                               check_company=True,
                               tracking=True,
                               help="Taxes that apply on the base amount")
    tax_line_id = fields.Many2one(
        'account.tax',
        string='Originator Tax',
        tracking=True,
        ondelete='restrict',
        store=True,
        compute='_compute_tax_line_id',
        help="Indicates that this journal item is a tax line")
    tax_group_id = fields.Many2one(
        related='tax_line_id.tax_group_id',
        string='Originator tax group',
        readonly=True,
        tracking=True,
        store=True,
        help='technical field for widget tax-group-custom-field')
    tax_base_amount = fields.Monetary(string="Base Amount",
                                      store=True,
                                      readonly=True,
                                      tracking=True,
                                      currency_field='company_currency_id')
    tax_exigible = fields.Boolean(
        string='Appears in VAT report',
        default=True,
        readonly=True,
        tracking=True,
        help=
        "Technical field used to mark a tax line as exigible in the vat report or not (only exigible journal items"
        " are displayed). By default all new journal items are directly exigible, but with the feature cash_basis"
        " on taxes, some will become exigible only when the payment is recorded."
    )
    tax_repartition_line_id = fields.Many2one(
        comodel_name='account.tax.repartition.line',
        string="Originator Tax Distribution Line",
        ondelete='restrict',
        readonly=True,
        tracking=True,
        check_company=True,
        help=
        "Tax distribution line that caused the creation of this move line, if any"
    )
    tax_tag_ids = fields.Many2many(
        string="Tags",
        comodel_name='account.account.tag',
        ondelete='restrict',
        tracking=True,
        help=
        "Tags assigned to this line by the tax creating it, if any. It determines its impact on financial reports."
    )
    tax_audit = fields.Char(
        string="Tax Audit String",
        compute="_compute_tax_audit",
        store=True,
        tracking=True,
        help=
        "Computed field, listing the tax grids impacted by this line, and the amount it applies to each of them."
    )

    # ==== Reconciliation fields ====
    amount_residual = fields.Monetary(
        string='Residual Amount',
        store=True,
        currency_field='company_currency_id',
        tracking=True,
        compute='_compute_amount_residual',
        help=
        "The residual amount on a journal item expressed in the company currency."
    )
    amount_residual_currency = fields.Monetary(
        string='Residual Amount in Currency',
        store=True,
        tracking=True,
        compute='_compute_amount_residual',
        help=
        "The residual amount on a journal item expressed in its currency (possibly not the company currency)."
    )
    full_reconcile_id = fields.Many2one('account.full.reconcile',
                                        string="Matching",
                                        copy=False,
                                        tracking=True,
                                        index=True,
                                        readonly=True)
    matched_debit_ids = fields.One2many(
        'account.partial.reconcile',
        'credit_move_id',
        string='Matched Debits',
        tracking=True,
        help='Debit journal items that are matched with this journal item.',
        readonly=True)
    matched_credit_ids = fields.One2many(
        'account.partial.reconcile',
        'debit_move_id',
        string='Matched Credits',
        tracking=True,
        help='Credit journal items that are matched with this journal item.',
        readonly=True)
    matching_number = fields.Char(
        string="Matching #",
        compute='_compute_matching_number',
        tracking=True,
        store=True,
        help=
        "Matching number for this line, 'P' if it is only partially reconcile, or the name of the full reconcile if it exists."
    )

    # ==== Analytic fields ====
    analytic_line_ids = fields.One2many('account.analytic.line',
                                        'move_id',
                                        tracking=True,
                                        string='Analytic lines')
    analytic_account_id = fields.Many2one('account.analytic.account',
                                          string='Analytic Account',
                                          index=True,
                                          tracking=True,
                                          compute="_compute_analytic_account",
                                          store=True,
                                          readonly=False,
                                          check_company=True,
                                          copy=True)
    analytic_tag_ids = fields.Many2many('account.analytic.tag',
                                        string='Analytic Groups',
                                        compute="_compute_analytic_account",
                                        tracking=True,
                                        store=True,
                                        readonly=False,
                                        check_company=True,
                                        copy=True,
                                        )

    # ==== Onchange / display purpose fields ====
    recompute_tax_line = fields.Boolean(
        store=False,
        tracking=True,
        readonly=True,
        help=
        "Technical field used to know on which lines the taxes must be recomputed."
    )
    display_type = fields.Selection([
        ('line_section', 'Section'),
        ('line_note', 'Note'),
    ],
                                    default=False,
                                    tracking=True,
                                    help="Technical field for UX purpose.")
    is_rounding_line = fields.Boolean(
        help="Technical field used to retrieve the cash rounding line.",
        tracking=True)
    exclude_from_invoice_tab = fields.Boolean(
        help=
        "Technical field used to exclude some lines from the invoice_line_ids tab in the form view.",
        tracking=True)
    is_fiscal_book_exclude = fields.Boolean(string='Exclude on Fiscal Book', related='move_id.is_fiscal_book_exclude')

    @api.depends('move_id', 'move_id.analytic_group_ids')
    def _compute_analytic_account(self):
        # res = super(AccountMoveLine, self)._compute_analytic_account()
        for record in self:
            if record.account_id.user_type_id.internal_group in ['income','expense']: 
                if record.analytic_tag_ids:
                    record.analytic_tag_ids = record.analytic_tag_ids
                else:
                    record.analytic_tag_ids = [(6, 0 , record.move_id.analytic_group_ids.ids)]
        # return res
        
    
    def _set_price_and_tax_after_fpos(self):
        self.ensure_one()
        # Manage the fiscal position after that and adapt the price_unit.
        # E.g. mapping a price-included-tax to a price-excluded-tax must
        # remove the tax amount from the price_unit.
        # However, mapping a price-included tax to another price-included tax must preserve the balance but
        # adapt the price_unit to the new tax.
        # E.g. mapping a 10% price-included tax to a 20% price-included tax for a price_unit of 110 should preserve
        # 100 as balance but set 120 as price_unit.
        if self.tax_ids and self.move_id.fiscal_position_id and self.move_id.fiscal_position_id.tax_ids:
            price_subtotal = self._get_price_total_and_subtotal(
            )['price_subtotal']
            self.tax_ids = self.move_id.fiscal_position_id.map_tax(
                self.tax_ids._origin, partner=self.move_id.partner_id)
            accounting_vals = self._get_fields_onchange_subtotal(
                price_subtotal=price_subtotal,
                currency=self.move_id.company_currency_id)
            amount_currency = accounting_vals['amount_currency']
            business_vals = self._get_fields_onchange_balance(
                amount_currency=amount_currency)
            if 'price_unit' in business_vals:
                self.price_unit = business_vals['price_unit']

class ApprovalMatrixAccountingLines(models.Model):
    _inherit = "approval.matrix.accounting.lines"

    move_id = fields.Many2one('account.move', string='Move')
