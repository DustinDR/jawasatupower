
import pytz
from pytz import timezone, UTC
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime, date, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from lxml import etree
import logging
_logger = logging.getLogger(__name__)


class AccountPayment(models.Model):
    _inherit = "account.payment"
    
    user_id = fields.Many2one('res.users','User',default=lambda self:self.env.user)
    move_id = fields.Many2one(
        comodel_name='account.move',
        string='Journal Entry', required=True,
        readonly=True, ondelete='cascade',
        tracking=True,
        check_company=True)

    is_reconciled = fields.Boolean(
        string="Is Reconciled", store=True,
        tracking=True,
        compute='_compute_reconciliation_status',
        help="Technical field indicating if the payment is already reconciled.")
    is_matched = fields.Boolean(
        string="Is Matched With a Bank Statement", store=True,
        tracking=True,
        compute='_compute_reconciliation_status',
        help="Technical field indicating if the payment has been matched with a statement line.")
    partner_bank_id = fields.Many2one(
        'res.partner.bank', string="Recipient Bank Account",
        tracking=True,
        readonly=False, store=True,
        compute='_compute_partner_bank_id',
        domain="[('partner_id', '=', partner_id)]",
        check_company=True)
    is_internal_transfer = fields.Boolean(
        string="Is Internal Transfer",
        tracking=True,
        readonly=False, store=True,
        compute="_compute_is_internal_transfer")
    qr_code = fields.Char(
        string="QR Code",
        tracking=True,
        compute="_compute_qr_code",
        help="QR-code report URL to use to generate the QR-code to scan with a banking app to perform this payment.")

    # == Payment methods fields ==
    payment_method_id = fields.Many2one(
        'account.payment.method', string='Payment Method',
        tracking=True,
        readonly=False, store=True,
        compute='_compute_payment_method_id',
        domain="[('id', 'in', available_payment_method_ids)]",
        help="Manual: Get paid by cash, check or any other method outside of Odoo.\n"\
        "Electronic: Get paid automatically through a payment acquirer by requesting a transaction on a card saved by the customer when buying or subscribing online (payment token).\n"\
        "Check: Pay bill by check and print it from Odoo.\n"\
        "Batch Deposit: Encase several customer checks at once by generating a batch deposit to submit to your bank. When encoding the bank statement in Odoo, you are suggested to reconcile the transaction with the batch deposit.To enable batch deposit, module account_batch_payment must be installed.\n"\
        "SEPA Credit Transfer: Pay bill from a SEPA Credit Transfer file you submit to your bank. To enable sepa credit transfer, module account_sepa must be installed ")
    available_payment_method_ids = fields.Many2many(
        'account.payment.method',
        tracking=True,
        compute='_compute_payment_method_fields')
    hide_payment_method = fields.Boolean(
        compute='_compute_payment_method_fields',
        tracking=True,
        help="Technical field used to hide the payment method if the selected journal has only one available which is 'manual'")

    # == Synchronized fields with the account.move.lines ==
    amount = fields.Monetary(
        currency_field='currency_id',
        tracking=True)
    payment_type = fields.Selection(
        [
            ('outbound', 'Send Money'),
            ('inbound', 'Receive Money'),
        ], tracking=True,
        string='Payment Type', default='inbound', required=True)
    payment_reference = fields.Char(
        string="Payment Reference", copy=False,
        tracking=True,
        help="Reference of the document used to issue this payment. Eg. check number, file name, etc.")
    currency_id = fields.Many2one(
        'res.currency', string='Currency',
        tracking=True, store=True, readonly=False,
        compute='_compute_currency_id',
        help="The payment's currency.")
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string="Customer/Vendor",
        store=True, readonly=False, ondelete='restrict',
        compute='_compute_partner_id',
        domain="['|', ('parent_id','=', False), ('is_company','=', True)]",
        tracking=True,
        check_company=True)
    destination_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Destination Account',
        store=True, readonly=False,
        compute='_compute_destination_account_id',
        domain="[('user_type_id.type', 'in', ('receivable', 'payable')), ('company_id', '=', company_id)]",
        tracking=True,
        check_company=True)

    # == Stat buttons ==
    reconciled_invoice_ids = fields.Many2many(
        'account.move', string="Reconciled Invoices",
        compute='_compute_stat_buttons_from_reconciliation',
        tracking=True,
        help="Invoices whose journal items have been reconciled with these payments.")
    reconciled_invoices_count = fields.Integer(
        string="# Reconciled Invoices",
        tracking=True,
        compute="_compute_stat_buttons_from_reconciliation")
    reconciled_bill_ids = fields.Many2many(
        'account.move', string="Reconciled Bills",
        tracking=True,
        compute='_compute_stat_buttons_from_reconciliation',
        help="Invoices whose journal items have been reconciled with these payments.")
    reconciled_bills_count = fields.Integer(
        string="# Reconciled Bills",
        tracking=True,
        compute="_compute_stat_buttons_from_reconciliation")
    reconciled_statement_ids = fields.Many2many(
        'account.move', string="Reconciled Statements",
        tracking=True,
        compute='_compute_stat_buttons_from_reconciliation',
        help="Statements matched to this payment")
    reconciled_statements_count = fields.Integer(
        string="# Reconciled Statements",
        tracking=True,
        compute="_compute_stat_buttons_from_reconciliation")

    # == Display purpose fields ==
    payment_method_code = fields.Char(
        related='payment_method_id.code',
        tracking=True,
        help="Technical field used to adapt the interface to the payment type selected.")
    show_partner_bank_account = fields.Boolean(
        compute='_compute_show_require_partner_bank',
        tracking=True,
        help="Technical field used to know whether the field `partner_bank_id` needs to be displayed or not in the payments form views")
    require_partner_bank_account = fields.Boolean(
        compute='_compute_show_require_partner_bank',
        tracking=True,
        help="Technical field used to know whether the field `partner_bank_id` needs to be required or not in the payments form views")
    country_code = fields.Char(
        related='company_id.country_id.code', tracking=True)
    voucher_id = fields.Many2one('payment.voucher','Payment Voucher')
    
    branch_id = fields.Many2one(
        'res.branch',
        related='user_id.branch_id',
        readonly=False)

    payment_receipt_approval_matrix_id = fields.Many2one('approval.matrix.accounting', string="Approval Matrix", compute='_get_payment_receipt_approval_matrix')
    is_receipt_approval_matrix = fields.Boolean(string="Is Receipt Approval Matrix", compute='_get_payment_receipt_approve_button_from_config')
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button_receipt', store=False)
    approved_matrix_ids = fields.One2many('approval.matrix.accounting.lines', 'receipt_id', string="Approved Matrix")
    approval_matrix_line_id = fields.Many2one('approval.matrix.accounting.lines', string='Approval Matrix Line', compute='_get_approve_button_receipt', store=False)
    approval_invoice_id = fields.Many2one('account.move', string='Account Move')


    @api.onchange('branch_id')
    def _get_company_id(self):
        self.company_id = self.env.company.id

    @api.onchange('payment_receipt_approval_matrix_id')
    def _compute_approving_matrix_lines(self):
        data = [(5, 0, 0)]
        for record in self:
            if record.state in ('draft', 'to_approve') and record.is_receipt_approval_matrix:
                record.approved_matrix_ids = []
                counter = 1
                record.approved_matrix_ids = []
                for rec in record.payment_receipt_approval_matrix_id: 
                    for line in rec.approval_matrix_line_ids:
                        data.append((0, 0, {
                            'sequence' : counter,
                            'user_ids' : [(6, 0, line.user_ids.ids)],
                            'minimum_approver' : line.minimum_approver,
                        }))
                        counter += 1
                record.approved_matrix_ids = data

    @api.depends('amount', 'company_id', 'branch_id')
    def _get_payment_receipt_approval_matrix(self):
        for record in self:
            matrix_id = False
            context = dict(self.env.context) or {}
            if context.get('default_payment_type') == 'inbound':
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.amount),
                        ('max_amount', '>=', record.amount),
                        ('approval_matrix_type', '=', 'receipt_approval_matrix')
                    ], limit=1)
            elif context.get('default_payment_type') == 'outbound':
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.amount),
                        ('max_amount', '>=', record.amount),
                        ('approval_matrix_type', '=', 'payment_approval_matrix')
                    ], limit=1)
            record.payment_receipt_approval_matrix_id = matrix_id

    def _get_payment_receipt_approve_button_from_config(self):
        for record in self:
            is_receipt_approval_matrix = False
            context = dict(self.env.context) or {}
            if context.get('default_payment_type') == 'inbound':
                is_receipt_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_receipt_approval_matrix', False)
            elif context.get('default_payment_type') == 'outbound':
                is_receipt_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_payment_approval_matrix', False)
            record.is_receipt_approval_matrix = is_receipt_approval_matrix

    @api.onchange('partner_id')
    def branch_domain(self):
        res={}
        self._get_payment_receipt_approve_button_from_config()
        if self.user_id and self.user_id.branch_ids:
            res={
            'domain': {
            'branch_id': [('id', 'in', self.user_id.branch_ids.ids)]
            }
        }
        return res

    def _get_approve_button_receipt(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
    
    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        return
    
    def request_for_approval(self):
        for record in self:
            record.write({'state': 'to_approve'})

    def action_approved_rp(self):
        for record in self:
            user = self.env.user
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_ids.ids and \
                    user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    utc_datetime = datetime.now()
                    local_timezone = pytz.timezone(self.env.user.tz)
                    local_datetime = utc_datetime.replace(tzinfo=pytz.utc)
                    local_datetime = local_datetime.astimezone(local_timezone).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                    if name != '':
                        name += "\n • %s: Approved - %s" % (self.env.user.name, local_datetime)
                    else:
                        name += "• %s: Approved - %s" % (self.env.user.name, local_datetime)

                    approval_matrix_line_id.write({ 
                        'last_approved': self.env.user.id, 'state_char': name, 
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
                        # next_approval_matrix_line_id = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
                        # if next_approval_matrix_line_id and len(next_approval_matrix_line_id[0].approver) > 1:
                        #     pass
            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r:r.approved)):
                record.write({'state': 'approved'})
                record.action_post()

    def rp_reject(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Approval Marix Reject ',
            'res_model': 'receipt.payment.matrix.reject',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
        }
    
    
        
    def _synchronize_from_moves(self, changed_fields):
        ''' Update the account.payment regarding its related account.move.
        Also, check both models are still consistent.
        :param changed_fields: A set containing all modified fields on account.move.
        '''
        if self._context.get('skip_account_move_synchronization'):
            return

        for pay in self.with_context(skip_account_move_synchronization=True):

            # After the migration to 14.0, the journal entry could be shared between the account.payment and the
            # account.bank.statement.line. In that case, the synchronization will only be made with the statement line.
            if pay.move_id.statement_line_id:
                continue

            move = pay.move_id
            move_vals_to_write = {}
            payment_vals_to_write = {}

            if 'journal_id' in changed_fields:
                if pay.journal_id.type not in ('bank', 'cash'):
                    raise UserError(_("A payment must always belongs to a bank or cash journal."))

            if 'line_ids' in changed_fields:
                all_lines = move.line_ids
                liquidity_lines, counterpart_lines, writeoff_lines = pay._seek_for_lines()

                if len(liquidity_lines) != 1 or len(counterpart_lines) != 1:
                    raise UserError(_(
                        "The journal entry %s reached an invalid state relative to its payment.\n"
                        "To be consistent, the journal entry must always contains:\n"
                        "- one journal item involving the outstanding payment/receipts account.\n"
                        "- one journal item involving a receivable/payable account.\n"
                        "- optional journal items, all sharing the same account.\n\n"
                    ) % move.display_name)

                '''To allow multiple writeoff with different account'''
                #if writeoff_lines and len(writeoff_lines.account_id) != 1:
                #    raise UserError(_(
                #        "The journal entry %s reached an invalid state relative to its payment.\n"
                #        "To be consistent, all the write-off journal items must share the same account."
                #    ) % move.display_name)

                if any(line.currency_id != all_lines[0].currency_id for line in all_lines):
                    raise UserError(_(
                        "The journal entry %s reached an invalid state relative to its payment.\n"
                        "To be consistent, the journal items must share the same currency."
                    ) % move.display_name)

                if any(line.partner_id != all_lines[0].partner_id for line in all_lines):
                    raise UserError(_(
                        "The journal entry %s reached an invalid state relative to its payment.\n"
                        "To be consistent, the journal items must share the same partner."
                    ) % move.display_name)

                if counterpart_lines.account_id.user_type_id.type == 'receivable':
                    partner_type = 'customer'
                else:
                    partner_type = 'supplier'

                liquidity_amount = liquidity_lines.amount_currency

                move_vals_to_write.update({
                    'currency_id': liquidity_lines.currency_id.id,
                    'partner_id': liquidity_lines.partner_id.id,
                })
                payment_vals_to_write.update({
                    'amount': abs(liquidity_amount),
                    'partner_type': partner_type,
                    'currency_id': liquidity_lines.currency_id.id,
                    'destination_account_id': counterpart_lines.account_id.id,
                    'partner_id': liquidity_lines.partner_id.id,
                })
                if liquidity_amount > 0.0:
                    payment_vals_to_write.update({'payment_type': 'inbound'})
                elif liquidity_amount < 0.0:
                    payment_vals_to_write.update({'payment_type': 'outbound'})

            move.write(move._cleanup_write_orm_values(move, move_vals_to_write))
            pay.write(move._cleanup_write_orm_values(pay, payment_vals_to_write))
            
        
        
    def _prepare_move_line_default_vals_custom(self, multiple_write_off_line_vals=[]):
        '''This is a new function which is the custom of _prepare_move_line_default_vals.
        Prepare the dictionary to create the default account.move.lines for the current payment.
        :param of each item in multiple_write_off_line_vals: Optional dictionary to create a write-off account.move.line easily containing:
            * amount:       The amount to be added to the counterpart amount.
            * name:         The label to set on the line.
            * account_id:   The account on which create the write-off.
        :return: A list of python dictionary to be passed to the account.move.line's 'create' method.
        '''
        self.ensure_one()
        multiple_write_off_line_vals = multiple_write_off_line_vals or []

        if not self.journal_id.payment_debit_account_id or not self.journal_id.payment_credit_account_id:
            raise UserError(_(
                "You can't create a new payment without an outstanding payments/receipts account set on the %s journal.",
                self.journal_id.display_name))

        # Compute amounts.
        write_off_amount_currency = 0
        for vals in multiple_write_off_line_vals:
            write_off_amount_currency += vals.get('amount', 0.0)

        if self.payment_type == 'inbound':
            # Receive money.
            liquidity_amount_currency = self.amount
        elif self.payment_type == 'outbound':
            # Send money.
            liquidity_amount_currency = -self.amount
            write_off_amount_currency *= -1
        else:
            liquidity_amount_currency = write_off_amount_currency = 0.0

        write_off_balance = self.currency_id._convert(
            write_off_amount_currency,
            self.company_id.currency_id,
            self.company_id,
            self.date,
        )
        liquidity_balance = self.currency_id._convert(
            liquidity_amount_currency,
            self.company_id.currency_id,
            self.company_id,
            self.date,
        )

        # If Liquidity amount less than Invoice amount (Less payment)
        # If Less payment, write_off_balance will be grater than zero (write_off_balance > 0)
        # If Liquidity amount greater than Invoice amount (overpayment)
        # If Overpayment, write_off_balance will be less than zero (write_off_balance < 0)
        overpayment = write_off_balance < 0 or False
        currency_id = self.currency_id.id
        counterpart_amount_currency = -liquidity_amount_currency - write_off_amount_currency
        counterpart_balance = -liquidity_balance - write_off_balance
        

        if self.is_internal_transfer:
            if self.payment_type == 'inbound':
                liquidity_line_name = _('Transfer to %s', self.journal_id.name)
            else: # payment.payment_type == 'outbound':
                liquidity_line_name = _('Transfer from %s', self.journal_id.name)
        else:
            liquidity_line_name = self.payment_reference

        # Compute a default label to set on the journal items.

        payment_display_name = {
            'outbound-customer': _("Customer Reimbursement"),
            'inbound-customer': _("Customer Payment"),
            'outbound-supplier': _("Vendor Payment"),
            'inbound-supplier': _("Vendor Reimbursement"),
        }

        default_line_name = self.env['account.move.line']._get_default_line_name(
            _("Internal Transfer") if self.is_internal_transfer else payment_display_name['%s-%s' % (self.payment_type, self.partner_type)],
            self.amount,
            self.currency_id,
            self.date,
            partner=self.partner_id,
        )

        line_vals_list = [
            # Liquidity line.
            {
                'name': liquidity_line_name or default_line_name,
                'date_maturity': self.date,
                'amount_currency': liquidity_amount_currency,
                'currency_id': currency_id,
                'debit': liquidity_balance if liquidity_balance > 0.0 else 0.0,
                'credit': -liquidity_balance if liquidity_balance < 0.0 else 0.0,
                'partner_id': self.partner_id.id,
                'account_id': self.journal_id.payment_credit_account_id.id if liquidity_balance < 0.0 else self.journal_id.payment_debit_account_id.id,
            },
            # Receivable / Payable.
            {
                'name': self.payment_reference or default_line_name,
                'date_maturity': self.date,
                'amount_currency': counterpart_amount_currency,
                'currency_id': currency_id,
                'debit': counterpart_balance if counterpart_balance > 0.0 else 0.0,
                'credit': -counterpart_balance if counterpart_balance < 0.0 else 0.0,
                'partner_id': self.partner_id.id,
                'account_id': self.destination_account_id.id,
            },
        ]
        if not self.currency_id.is_zero(write_off_amount_currency):
            # Write-off lines.
            if all(vals.get('amount', 0.00) > 0 for vals in multiple_write_off_line_vals) or all(vals.get('amount', 0.00) < 0 for vals in multiple_write_off_line_vals):
                for vals in multiple_write_off_line_vals:
                    amount = vals.get('amount', 0.00)
                    if self.payment_type == 'outbound' or (overpayment and amount > 0):
                        amount *= -1

                    write_off_amount = self.currency_id._convert(
                        amount,
                        self.company_id.currency_id,
                        self.company_id,
                        self.date,
                    )
                    amt_currency = amount
                    debit_amt = write_off_amount if write_off_amount > 0.0 else 0.0
                    credit_amt = -write_off_amount if write_off_amount < 0.0 else 0.0
                    
                    line_vals_list.append({
                        'name': _("Difference Account - ") + vals.get('name') or default_line_name,
                        'amount_currency': amt_currency,
                        'currency_id': currency_id,
                        'debit': debit_amt,
                        'credit': credit_amt,
                        'partner_id': self.partner_id.id,
                        'account_id': vals.get('account_id'),
                    })
            else:
                for vals in multiple_write_off_line_vals:
                    amount = vals.get('amount', 0.00)
                    write_off_amount = self.currency_id._convert(
                        amount,
                        self.company_id.currency_id,
                        self.company_id,
                        self.date,
                    )
                    
                    if self.payment_type == 'outbound':
                        debit_amt  = -write_off_amount if write_off_amount < 0 else 0.00
                        credit_amt  = write_off_amount if write_off_amount > 0 else 0.00
                    else:
                        debit_amt  = write_off_amount if write_off_amount > 0 else 0.00
                        credit_amt  = -write_off_amount if write_off_amount < 0 else 0.00
                    amt_currency = debit_amt-credit_amt
                    
                    line_vals_list.append({
                        'name': _("Difference Account - ") + vals.get('name') or default_line_name,
                        'amount_currency': amt_currency,
                        'currency_id': currency_id,
                        'debit': debit_amt,
                        'credit': credit_amt,
                        'partner_id': self.partner_id.id,
                        'account_id': vals.get('account_id'),
                    })

        return line_vals_list
    
     
    @api.model_create_multi
    def create(self, vals_list):
        multiple_write_off_line_vals = []
        
        # Hack to add a custom write-off line.
        for vals in vals_list:
            if vals.get('multiple_write_off_line_vals',[]):
                multiple_write_off_line_vals += vals.pop('multiple_write_off_line_vals', None)
        
        if len(multiple_write_off_line_vals) > 0:
            payments = super(AccountPayment, self.with_context(multiple_write_off_line=True)).create(vals_list)

            for i, pay in enumerate(payments):
                to_write = {'payment_id': pay.id}
                for k, v in vals_list[i].items():
                    if k in self._fields and self._fields[k].store and k in pay.move_id._fields and pay.move_id._fields[k].store:
                        to_write[k] = v
                
                if 'line_ids' not in vals_list[i]:
                    line_ids = [(5, 0, 0)]
                    for line_vals in pay._prepare_move_line_default_vals_custom(multiple_write_off_line_vals=multiple_write_off_line_vals):
                        line_ids.append((0, 0, line_vals))
                    to_write['line_ids'] = line_ids

                pay.move_id.write(to_write)
                
            return payments
        
        else:
            return super(AccountPayment, self).create(vals_list)        



class ApprovalMatrixAccountingLines(models.Model):
    _inherit = "approval.matrix.accounting.lines"

    receipt_id = fields.Many2one('account.payment', string='Payment')