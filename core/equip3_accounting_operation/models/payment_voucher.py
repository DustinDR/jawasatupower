
import pytz
from pytz import timezone, UTC
from odoo import fields, models, api, _
from datetime import datetime
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, ValidationError
from num2words import num2words
from datetime import datetime, date, timedelta
from odoo.exceptions import ValidationError, UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import logging

_logger = logging.getLogger(__name__)
class PaymentVoucher(models.Model):
    _name = 'payment.voucher'
    _description = 'Payment Voucher'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    def _default_currency_id(self):
        company_id = self.env.context.get('force_company') or self.env.context.get('company_id') or self.env.company.id
        return self.env['res.company'].browse(company_id).currency_id

    create_by = fields.Many2one('res.users',
                                'Created By',
                                default=lambda self: self.env.user)
    name = fields.Char('Payment Voucher',
                       required=True,
                       default=lambda self: self.env['ir.sequence'].
                       next_by_code('payment.voucher.code'))
    partner_id = fields.Many2one('res.partner', 'Payee Name')
    partner_ids = fields.Many2many('res.partner',
                                   'payment_voucher_rel',
                                   'payment_voucher_rec',
                                   'partner_id',
                                   string='Payee Names',
                                   required=True)
    date = fields.Date('Date', default=fields.Date.today())

    bank_id = fields.Many2one('account.journal',
                              domain="[('type','in',['bank','cash'])]",
                              string='Bank',
                              required=True)
    cheque_number = fields.Char('Cheque Number')
    cheque_date = fields.Date('Cheque Date')
    vendor_bill_ids = fields.Many2many(
        'account.move',
        relation='payment_vendor_bills_rel',
        string='Vendor Bills',
        domain="[('move_type','=','in_invoice')]",
        required=True)
    remarks = fields.Text('Remarks')
    company_id = fields.Many2one('res.company',
                                 'Company',
                                 required=True,
                                 default=lambda self: self.env.company)
    currency_id = fields.Many2one('res.currency', 'Currency', required=True, default=_default_currency_id)
    line_ids = fields.One2many('payment.voucher.line', 'voucher_id',
                               'Invoice Information')
    branch_id = fields.Many2one('res.branch',
                                'Branch',
                                default=lambda self: self.env.user.branch_id.id)
    state = fields.Selection([('draft', 'Draft'),
                              ('to_approve', 'Waiting For Approval'),
                              ('approved', 'Approved'),
                              ('rejected', 'Rejected'), 
                              ('submitted', 'Submitted'),
                              ('verified', 'Verified'), ('paid', 'Paid'),
                              ('canceled', 'Canceled')],
                             track_visibility='onchange',
                             default="draft")
    amount = fields.Monetary('Total', currency_field='currency_id',compute='_compute_voucher_total')
    payment_ids = fields.One2many('account.payment', 'voucher_id', 'Payments')
    payment_count = fields.Integer(compute='_get_payment_count')
    approval_matrix_id = fields.Many2one('approval.matrix.accounting', string="Approval Matrix", compute='_get_approval_matrix')
    is_payment_voucher_approval_matrix = fields.Boolean(string="Is Payment Voucher Approval Matrix", compute='_get_approve_button_from_config')
    approved_matrix_ids = fields.One2many('approval.matrix.accounting.lines', 'payment_voc_id', string="Approved Matrix")
    approval_matrix_line_id = fields.Many2one('approval.matrix.accounting.lines', string='Approval Matrix Line', compute='_get_approve_button', store=False)
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button', store=False)
    state1 = fields.Selection(related="state", tracking=False)
    state2 = fields.Selection(related="state", tracking=False)


    @api.depends('line_ids', 'line_ids.amount', 'company_id', 'branch_id')
    def _get_approval_matrix(self):
        for record in self:
            amount = sum(record.line_ids.mapped('amount'))
            matrix_id = self.env['approval.matrix.accounting'].search([
                    ('company_id', '=', record.company_id.id),
                    ('branch_id', '=', record.branch_id.id),
                    ('min_amount', '<=', amount),
                    ('max_amount', '>=', amount),
                    ('approval_matrix_type', '=', 'payment_voucher')
                ], limit=1)
            record.approval_matrix_id = matrix_id
            record._compute_approving_matrix_lines()

    def _get_approve_button_from_config(self):
        for record in self:
            is_payment_voucher_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_payment_voucher_approval_matrix', False)
            record.is_payment_voucher_approval_matrix = is_payment_voucher_approval_matrix

    def _get_approve_button(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False

    @api.onchange('approval_matrix_id')
    def _compute_approving_matrix_lines(self):
        data = [(5, 0, 0)]
        for record in self:
            if record.state == 'draft' and record.is_payment_voucher_approval_matrix:
                record.approved_matrix_ids = []
                counter = 1
                record.approved_matrix_ids = []
                for rec in record.approval_matrix_id: 
                    for line in rec.approval_matrix_line_ids:
                        data.append((0, 0, {
                            'sequence' : counter,
                            'user_ids' : [(6, 0, line.user_ids.ids)],
                            'minimum_approver' : line.minimum_approver,
                        }))
                        counter += 1
                record.approved_matrix_ids = data


    def action_request_for_approval(self):
        for record in self:
            record.write({'state': 'to_approve'})


    def action_approve(self):
        for record in self:
            user = self.env.user
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_ids.ids and \
                    user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    utc_datetime = datetime.now()
                    local_timezone = pytz.timezone(self.env.user.tz)
                    local_datetime = utc_datetime.replace(tzinfo=pytz.utc)
                    local_datetime = local_datetime.astimezone(local_timezone).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                    if name != '':
                        name += "\n • %s: Approved - %s" % (self.env.user.name, local_datetime)
                    else:
                        name += "• %s: Approved - %s" % (self.env.user.name, local_datetime)

                    approval_matrix_line_id.write({ 
                        'last_approved': self.env.user.id, 'state_char': name, 
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
                        # next_approval_matrix_line_id = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
                        # if next_approval_matrix_line_id and len(next_approval_matrix_line_id[0].approver) > 1:
                        #     pass
            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r:r.approved)):
                record.write({'state': 'approved'})
                record.verify()

    def action_reject(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Payment Voucher Marix Reject ',
            'res_model': 'payment.voucher.matrix.reject',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
        }

    @api.depends('line_ids')
    def _compute_voucher_total(self):
        for payment in self:
            total=0
            for line in payment.line_ids:
                total+=line.amount 
            payment.amount = total
            if payment.line_ids:
                payment.line_ids._onchange_invoice_number()

    @api.onchange('partner_id', 'date')
    def branch_domain(self):
        res = {}
        self._get_approve_button_from_config()
        if self.create_by and self.create_by.branch_ids:
            res = {
                'domain': {
                    'branch_id': [('id', 'in', self.create_by.branch_ids.ids)]
                }
            }
        return res

    @api.onchange('branch_id')
    def _onchange_branch_id(self):
        return

    @api.onchange('vendor_bill_ids')
    def _onchange_vendor_bill_ids(self):
        for bill in self.vendor_bill_ids:
            if not self.line_ids.filtered(
                    lambda x: x.invoice_number_id.id == bill._origin.id):
                voucher_line_id = self.env['payment.voucher.line'].create({
                    'invoice_number_id':
                    bill._origin.id,
                    'voucher_id':
                    self._origin.id
                })
                voucher_line_id._onchange_invoice_number()
                self.line_ids = [(4, voucher_line_id.id)]
        for line in self.line_ids:
            if not self.vendor_bill_ids.filtered(
                    lambda x: x._origin.id == line.invoice_number_id.id):
                self.line_ids = [(2, line.id)]

    def set_to_draft(self):
        self.state = 'draft'

    def submit(self):
        self.state = 'submitted'

    def verify(self):
        self.state = 'verified'

    def pay(self):
        for line in self.line_ids:
            to_reconcile = []
            payment_values = {
                'company_id': self.company_id.id,
                'partner_id': line.invoice_number_id.partner_id.id,
                'amount': line.amount,
                'date': self.date,
                'ref': self.cheque_number,
                'payment_type': 'outbound',
                'partner_type': 'supplier',
                'currency_id': self.currency_id.id,
                # 'destination_account_id': line.invoice_number_id.line_ids[0].account_id.id,
                'destination_account_id': line.invoice_number_id.partner_id.property_account_payable_id.id,
                'journal_id': self.bank_id.id,
                'voucher_id':                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             self.id,
            }
            if line.invoice_number_id.partner_id and line.invoice_number_id.partner_id.bank_ids:
                payment_values['partner_bank_id'] = line.invoice_number_id.partner_id.bank_ids[0].id
            if self.bank_id.is_bank_cimb and self.bank_id.outbound_payment_method_ids.filtered(lambda x: x.payment_methode_code =='bank_cimb_transfer'):
                payment_values['payment_method_id'] = self.invoice_number_id.bank_id.outbound_payment_method_ids.filtered(lambda x: x.payment_methode_code == 'bank_cimb_transfer')[0].id
            elif self.bank_id.outbound_payment_method_ids:
                payment_values['payment_method_id'] = self.bank_id.outbound_payment_method_ids[0].id

            payment_id = self.env['account.payment'].create(payment_values)
            for line in line.invoice_number_id.line_ids:
                to_reconcile.append(line)
            
            for payment, lines in zip(payment_id, to_reconcile):
                # Batches are made using the same currency so making 'lines.currency_id' is ok.
                if payment.currency_id != lines.currency_id:
                    liquidity_lines, counterpart_lines, writeoff_lines = payment._seek_for_lines()
                    source_balance = abs(sum(lines.mapped('amount_residual')))
                    payment_rate = liquidity_lines[0].amount_currency / liquidity_lines[0].balance
                    source_balance_converted = abs(source_balance) * payment_rate

                    # Translate the balance into the payment currency is order to be able to compare them.
                    # In case in both have the same value (12.15 * 0.01 ~= 0.12 in our example), it means the user
                    # attempt to fully paid the source lines and then, we need to manually fix them to get a perfect
                    # match.
                    payment_balance = abs(sum(counterpart_lines.mapped('balance')))
                    payment_amount_currency = abs(sum(counterpart_lines.mapped('amount_currency')))
                    if not payment.currency_id.is_zero(source_balance_converted - payment_amount_currency):
                        continue

                    delta_balance = source_balance - payment_balance

                    # Balance are already the same.
                    if self.company_id.currency_id.is_zero(delta_balance):
                        continue

                    # Fix the balance but make sure to peek the liquidity and counterpart lines first.
                    debit_lines = (liquidity_lines + counterpart_lines).filtered('debit')
                    credit_lines = (liquidity_lines + counterpart_lines).filtered('credit')

                    payment.move_id.write({'line_ids': [
                        (1, debit_lines[0].id, {'debit': debit_lines[0].debit + delta_balance}),
                        (1, credit_lines[0].id, {'credit': credit_lines[0].credit + delta_balance}),
                    ]})
            payment_id.action_post()
            domain = [('account_internal_type', 'in', ('receivable',
                                                       'payable')),
                      ('reconciled', '=', False)]
            for payment, lines in zip(payment_id, to_reconcile):
                if payment.state != 'posted':
                    continue
                payment_lines = payment.line_ids.filtered_domain(domain)
                for account in payment_lines.account_id:
                    (payment_lines + lines)\
                        .filtered_domain([('account_id', '=', account.id), ('reconciled', '=', False)])\
                        .reconcile()
        self.state = 'paid'
        self.line_ids._onchange_invoice_number()

    def cancel(self):
        self.state = 'canceled'

    def _get_payment_count(self):
        self.payment_count = len(self.payment_ids)

    def get_payments(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Payments',
            'view_mode': 'tree',
            'res_model': 'account.payment',
            'domain': [('id', 'in', self.payment_ids.ids)],
            'context': "{'create': False}"
        }


class PaymentVoucherLine(models.Model):
    _name = 'payment.voucher.line'
    _description = 'Payment voucher Line'

    invoice_number_id = fields.Many2one('account.move', 'Invoice Number')
    invoice_date = fields.Date('Date', related='invoice_number_id.date')
    currency_id = fields.Many2one('res.currency',
                                  readonly=True,
                                  related='invoice_number_id.currency_id')
    untaxed_amount = fields.Monetary(
        'Untaxed Amount',
        #related='invoice_number_id.amount_untaxed',
        store=True)
    tax_amount = fields.Monetary(
        'Tax Amount',
        #related='invoice_number_id.amount_tax',
        store=True)
    total_invoice_amount = fields.Monetary(
        'Total'
        #related='invoice_number_id.amount_total'
    )
    amount_due = fields.Monetary(
        'Amount Due',
        #related='invoice_number_id.amount_residual',
        store=True)
    amount = fields.Monetary('Amount')
    voucher_id = fields.Many2one('payment.voucher', 'Payment Voucher')
    state = fields.Selection([('processing', 'Processing'), ('paid', 'Paid')],
                             default='processing',
                             compute='_get_voucherline_state',
                             store=True)

    @api.depends('amount_due')
    def _get_voucherline_state(self):
        for voucher in self:
            if voucher.amount_due <= 0:
                voucher.state = 'paid'
            else:
                voucher.state = 'processing'

    @api.onchange('invoice_number_id')
    def _onchange_invoice_number(self):
        for voucher in self:
            if voucher.invoice_number_id:
                voucher.untaxed_amount = voucher.invoice_number_id.amount_untaxed
                voucher.tax_amount = voucher.invoice_number_id.amount_tax
                voucher.total_invoice_amount = voucher.invoice_number_id.amount_total
                voucher.amount_due = voucher.invoice_number_id.amount_residual

class ApprovalMatrixAccountingLines(models.Model):
    _inherit = "approval.matrix.accounting.lines"

    payment_voc_id = fields.Many2one('payment.voucher', string='Payment Voucher')