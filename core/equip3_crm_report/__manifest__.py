# -*- coding: utf-8 -*-
{
    'name': "Equip3 CRM Report",

    'summary': """
        Manage CRM Reports """,

    'description': """
        This module manages these features :
        1. Leads Analysis
    """,

    'author': "Hashmicro",
    'category': 'CRM',
    'version': '1.1.6',

    # any module necessary for this one to work correctly
    'depends': [
        'ks_dashboard_ninja',
        'equip3_crm_operation',
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/ks_crm_data.xml',
        'views/views.xml',
        "views/calendar_event_views.xml",
        'views/meeting_analysis_report_views.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}