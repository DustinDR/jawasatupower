from odoo import tools
from odoo import models, fields, api
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta

class MeetingAnalysisReport(models.Model):
    _name = 'meeting.analysis.report'
    _description = "Meeting Analysis Report"
    _auto = False

    user_id = fields.Many2one('res.users', string="Sales Person")
    start = fields.Datetime(string='Starting at')
    duration = fields.Float(string='Meeting Duration (Hours)')
    meeting_count = fields.Integer(string='Meeting (Count)')
    reschedule_count = fields.Integer(string='Rescheduled (Count)')
    done_count = fields.Integer(string='Done (Count)')
    cancel_count = fields.Integer(string='Cancelled (Count)')

    def _query(self, with_clause='', fields={}, groupby='', from_clause=''):
        with_ = ("WITH %s" % with_clause) if with_clause else ""

        select_ = """
            min(ce.id) as id,
            ce.user_id as user_id,
            ce.start as start,
            ce.duration_count as duration,
            ce.meeting_count as meeting_count,
            ce.reschedule_count as reschedule_count,
            ce.done_count as done_count,
            ce.cancel_count as cancel_count
        """

        for field in fields.values():
            select_ += field

        from_ = """
            calendar_event ce
        """

        where_ = """
            ce.user_id is not null
        """

        groupby_ = """
            ce.user_id,
            ce.start,
            ce.duration_count,
            ce.meeting_count,
            ce.reschedule_count,
            ce.done_count,
            ce.cancel_count
        """

        return '%s (SELECT %s FROM %s WHERE %s GROUP BY %s)' % (with_, select_, from_, where_, groupby_)

    def init(self):
        # self._table = sale_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (%s)""" % (self._table, self._query()))
