# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class CrmLead(models.Model):
    _inherit = "crm.lead"

    # day_open = fields.Float('Duration to Assign (Days)', compute='_compute_day_open', store=True)

    lost_date = fields.Datetime('Lost Date')
    won_date = fields.Datetime('Won Date')
    quotation_date = fields.Datetime('Quotation Date')
    meeting_date = fields.Datetime('First Meeting Date')

    lost_day = fields.Integer('Duration to Lost (Days)', default=0)
    won_day = fields.Integer('Duration to Won (Days)', defult=0)
    quotation_day = fields.Integer('Duration to Quotation (Days)', defult=0)
    meeting_day = fields.Integer('Duration to First Meeting (Hour)', defult=0)
    stage_day = fields.Integer('Duration First Move Stage (Hour)', defult=0)

    first_to_second_meeting_days = fields.Integer('First to Second Meeting (days)', default=0)
    created_to_second_meeting_days = fields.Integer('Leads Created to Second Meeting (days)', default=0)

    def write(self, vals):
        diff = (fields.Datetime.now() - self.create_date)
        days = diff.days
        sec = diff.seconds
        hour = days * 24 + sec // 3600
        if 'active' in vals:
            if not vals['active']:
                vals['lost_date'] = fields.Datetime.now()
                vals['lost_day'] = days
            else:
                vals['lost_date'] = False
                vals['lost_day'] = False
        if self.stage_id.id == 1 and not self.stage_day:
            vals['stage_day'] = hour
        if 'stage_id' in vals:
            stage_name = self.env['crm.stage'].browse(vals['stage_id']).name
            if stage_name == "Won":
                vals['won_date'] = fields.Datetime.now()
                vals['won_day'] = days
        res = super(CrmLead, self).write(vals)
        return res