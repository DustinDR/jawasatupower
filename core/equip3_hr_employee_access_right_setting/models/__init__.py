# -*- coding: utf-8 -*-

from . import hr_employee
from . import hr_contract
from . import hr_employee_category
from . import hr_skill
from . import gamification_badges
from . import hr_department
from . import mail_activity_type
from . import hr_resume_line_type