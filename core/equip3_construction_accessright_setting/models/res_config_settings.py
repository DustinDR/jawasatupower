from odoo import api , fields , models


class ResCompany(models.Model):
    _inherit = 'res.company'

    construction_sales = fields.Boolean(string="Construction Sales") 

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    construction_sales = fields.Boolean(string="Sales", related='company_id.construction_sales', readonly=False)
    temp_const = fields.Boolean(string="Sales", readonly=False)
    is_customer_approval_matrix_const = fields.Boolean(string="Sale Order Approval Matrix")
    is_job_estimate_approval_matrix = fields.Boolean(string="Job Estimate Approval Matrix")

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        is_customer_approval_matrix_const = IrConfigParam.get_param('is_customer_approval_matrix_const')
        if is_customer_approval_matrix_const is None:
            is_customer_approval_matrix_const = True
        is_job_estimate_approval_matrix = IrConfigParam.get_param('is_job_estimate_approval_matrix')
        if is_job_estimate_approval_matrix is None:
            is_job_estimate_approval_matrix = True
        construction_sales = IrConfigParam.get_param('construction_sales', False)
        res.update({
            'is_customer_approval_matrix_const': is_customer_approval_matrix_const,
            'is_job_estimate_approval_matrix': is_job_estimate_approval_matrix,
            'construction_sales': construction_sales,
        })
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values() 
        self.env['ir.config_parameter'].sudo().set_param('is_customer_approval_matrix_const', self.is_customer_approval_matrix_const)
        self.env['ir.config_parameter'].sudo().set_param('is_job_estimate_approval_matrix', self.is_job_estimate_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('construction_sales', self.construction_sales)


