from odoo import api, fields, models, _
from datetime import datetime, timedelta, date
from odoo.exceptions import UserError


class MaintenanceWorkOrder(models.Model):
    _name = 'maintenance.work.order'
    _inherit = ['maintenance.facilities.area','mail.thread', 'mail.activity.mixin']
    _description = 'Maintenance Work Order'
    _parent_name = "parent_location"
    _rec_name = 'name'

    @api.depends('maintenance_materials_list_ids.price_total')
    def _amount_all_material(self):
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.maintenance_materials_list_ids:
                line._compute_amount_price()
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.currency_id.round(amount_untaxed),
                'amount_tax': order.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax,
            })
    
    name = fields.Char("Work Order",default='New')
    facility = fields.Many2one(comodel_name='maintenance.facilities.area', string='Facilities Area')
    partner_id = fields.Many2one('res.partner', string='Customer')
    startdate = fields.Date(string='Start Date',default=datetime.today().date())
    enddate = fields.Date(string='End Date',default=datetime.today().date())
    maintenanceteam = fields.Many2one(comodel_name='maintenance.teams', string='Maintenance Team', ondelete='restrict')
    maintenanceassign = fields.Many2one('maintenance.assignation.type', string='Maintenance Assignation Type')
    created_date = fields.Datetime(string='Create Date', readonly=True)
    user_id = fields.Many2one('res.users', 'Created By', required=True, readonly=True, default=lambda self: self.env.user)
    company_id = fields.Many2one("res.company", "Company", default=lambda self: self.env.user.company_id)
    instructions = fields.Html(string='instructions')
    work_order_line_ids = fields.One2many('maintenance.work.order.line','work_order_id', string='WO Line')
    complete_name = fields.Char('Complete Name', compute='_compute_complete_name', store=True)
    ref = fields.Char('Refrence Document', readonly=True)
    remarks = fields.Text('Remarks')
    analytic_group_id = fields.Many2many('account.analytic.tag', string='Analytic Group')
    task_check_list_ids = fields.One2many('plan.task.check.list','maintenance_wo_id',string='Asset Checklist')
    maintenance_materials_list_ids = fields.One2many('maintenance.materials.list','maintenance_wo_id', string='Materials')
    tools_materials_list_ids = fields.One2many('tools.materials.list', 'maintenance_wo_id', string='Tools Materials')
    repair_count = fields.Integer(string='Repair Count', compute='_compute_repair_count')
    repair_ids = fields.Many2many('maintenance.repair.order', string='Repair Orders', compute="_compute_repair_count")
    attach_count = fields.Integer(string='Attachment', compute="_compute_attachment_count")
    maintenance_plan_id = fields.Many2one('maintenance.plan', string='Refrence Document', readonly=True)
    equipment_id = fields.Many2one(comodel_name='maintenance.equipment', string='Asset')
    task = fields.Text('Task')
    parent_location = fields.Many2one(
        'maintenance.work.order', 'Parent Location', index=True, ondelete='cascade', check_company=True,
        help="The parent location that includes this location. Example : The 'Dispatch Zone' is the 'Gate 1' parent location.")
    child_ids = fields.One2many('maintenance.work.order', 'parent_location', 'Contains')
    currency_id = fields.Many2one('res.currency', 'Currency', required=True,
        default=lambda self: self.env.company.currency_id.id)
    state_id = fields.Selection(
        [('draft', 'Draft'),
         ('in_progress', 'In Progress'),
         ('pending', 'Post'),
         ('done', 'Done'),
         ('cancel', 'Cancelled')], string="State", default='draft')
    time_ids = fields.One2many(
        'time.progress', 'maintenance_wo_id', copy=False)
    time_in_progress = fields.Float('Time in progress', compute='_compute_duration', inverse='_set_duration',
        readonly=False, store=True, copy=False)
    time_post_ids = fields.One2many(
        'time.post', 'maintenance_wo_id', copy=False)
    time_post = fields.Float('Time Post', compute='_compute_post_duration', inverse='_set_post_duration',
        readonly=False, store=True, copy=False)
    amount_untaxed = fields.Monetary(string='Untaxed Amount', store=True, readonly=True, compute='_amount_all_material', tracking=True)
    amount_tax = fields.Monetary(string='Taxes', store=True, readonly=True, compute='_amount_all_material')
    amount_total = fields.Monetary(string='Total', store=True, readonly=True, compute='_amount_all_material')
    
    def _prepare_timeline_vals(self, duration, date_start, date_end=False):
        # Need a loss in case of the real time exceeding the expected
        return {
            'maintenance_wo_id': self.id,
            'date_start': date_start,
            'date_end': date_end,
        }

    @api.depends('time_ids.duration', 'state_id')
    def _compute_duration(self):
        for order in self:
            order.time_in_progress = sum(order.time_ids.mapped('duration'))

    def _set_duration(self):

        def _float_duration_to_second(duration):
            minutes = duration // 1
            seconds = (duration % 1) * 60
            return minutes * 60 + seconds

        for order in self:
            old_order_duation = sum(order.time_ids.mapped('duration'))
            new_order_duration = order.time_in_progress
            if new_order_duration == old_order_duation:
                continue

            delta_duration = new_order_duration - old_order_duation

            if delta_duration > 0:
                date_start = datetime.now() - timedelta(seconds=_float_duration_to_second(delta_duration))
                self.env['time.progress'].create(
                    order._prepare_timeline_vals(delta_duration, date_start, datetime.now())
                )
            else:
                duration_to_remove = abs(delta_duration)
                timelines = order.time_ids.sorted(lambda t: t.date_start)
                timelines_to_unlink = self.env['time.progress']
                for timeline in timelines:
                    if duration_to_remove <= 0.0:
                        break
                    if timeline.duration <= duration_to_remove:
                        duration_to_remove -= timeline.duration
                        timelines_to_unlink |= timeline
                    else:
                        new_time_line_duration = timeline.duration - duration_to_remove
                        timeline.date_start = timeline.date_end - timedelta(seconds=_float_duration_to_second(new_time_line_duration))
                        break
                timelines_to_unlink.unlink()

    @api.depends('time_post_ids.duration', 'state_id')
    def _compute_post_duration(self):
        for order in self:
            order.time_post = sum(order.time_post_ids.mapped('duration'))

    def _set_post_duration(self):

        def _float_duration_to_second(duration):
            minutes = duration // 1
            seconds = (duration % 1) * 60
            return minutes * 60 + seconds

        for order in self:
            old_order_duation = sum(order.time_post_ids.mapped('duration'))
            new_order_duration = order.time_post
            if new_order_duration == old_order_duation:
                continue

            delta_duration = new_order_duration - old_order_duation

            if delta_duration > 0:
                date_start = datetime.now() - timedelta(seconds=_float_duration_to_second(delta_duration))
                self.env['time.post'].create(
                    order._prepare_timeline_vals(delta_duration, date_start, datetime.now())
                )
            else:
                duration_to_remove = abs(delta_duration)
                timelines = order.time_post_ids.sorted(lambda t: t.date_start)
                timelines_to_unlink = self.env['time.post']
                for timeline in timelines:
                    if duration_to_remove <= 0.0:
                        break
                    if timeline.duration <= duration_to_remove:
                        duration_to_remove -= timeline.duration
                        timelines_to_unlink |= timeline
                    else:
                        new_time_line_duration = timeline.duration - duration_to_remove
                        timeline.date_start = timeline.date_end - timedelta(seconds=_float_duration_to_second(new_time_line_duration))
                        break
                timelines_to_unlink.unlink()
    
    def check_equipment_state(self):
        for order in self:
            for line in order.task_check_list_ids:
                if line.equipment_id and line.equipment_id.state == 'maintenance':
                    raise UserError(_('The asset and/or vehicle that you want to maintain is still under maintenance from other Maintenance Work Order and/or Maintenance Repair Order. Please finish that particular order first.'))

    def update_equipment_state(self):
        plan_obj = self.env['plan.task.check.list']
        for order in self:
            for line in order.task_check_list_ids:
                if line.equipment_id:
                    if order.state_id == 'in_progress':
                        line.equipment_id.write({'state': 'maintenance'})
                    # elif order.state_id == 'pending':
                    #     line.equipment_id.write({'state': 'breakdown'})
                    elif order.state_id == 'done':
                        lines = plan_obj.search([('equipment_id', '=', line.equipment_id.id), ('maintenance_wo_id.state_id', 'in', ('in_progress', 'pending'))])
                        if not lines:
                            line.equipment_id.write({'state': 'operative'})
                    elif order.state_id == 'cancel':
                        lines = plan_obj.search([('equipment_id', '=', line.equipment_id.id), ('maintenance_wo_id.state_id', 'in', ('in_progress', 'pending'))])
                        if not lines:
                            line.equipment_id.write({'state': 'operative'})

    def state_confirm(self):
        # self.check_equipment_state()

        self.write({'state_id': 'in_progress'})
        self.env['time.progress'].create(
            self._prepare_timeline_vals(self.time_in_progress, datetime.now())
        )

        self.update_equipment_state()

    def state_done(self):
        self.write({'state_id': 'done'})
        time_ids = self.time_ids.filtered(lambda r:not r.date_end)
        if time_ids:
            time_ids.write({'date_end': datetime.now()})
        time_post_ids = self.time_post_ids.filtered(lambda r:not r.date_end)
        if time_post_ids:
            time_post_ids.write({'date_end': datetime.now()})

        self.update_equipment_state()

    def state_pending(self):
        self.write({'state_id': 'pending'})
        time_ids = self.time_ids.filtered(lambda r:not r.date_end)
        if time_ids:
            time_ids.write({'date_end': datetime.now()})
        self.env['time.post'].create(
            self._prepare_timeline_vals(self.time_post, datetime.now())
        )

        self.update_equipment_state()

    def action_start_again(self):
        # self.check_equipment_state()

        self.write({'state_id': 'in_progress'})
        self.env['time.progress'].create(
            self._prepare_timeline_vals(self.time_in_progress, datetime.now())
        )
        time_post_ids = self.time_post_ids.filtered(lambda r:not r.date_end)
        if time_post_ids:
            time_post_ids.write({'date_end': datetime.now()})

        self.update_equipment_state()

    def state_cancel(self):
        self.write({'state_id': 'cancel'})
        time_ids = self.time_ids.filtered(lambda r:not r.date_end)
        if time_ids:
            time_ids.write({'date_end': datetime.now()})
        time_post_ids = self.time_post_ids.filtered(lambda r:not r.date_end)
        if time_post_ids:
            time_post_ids.write({'date_end': datetime.now()})
        
        self.update_equipment_state()

    def _compute_repair_count(self):
        self.repair_ids = None
        self.repair_count = None
        for wo in self:
            repairs = self.env['maintenance.repair.order'].search([('work_order_id', '=', self.id)])
            if repairs:
                wo.repair_ids = repairs.ids
                wo.repair_count = len(repairs)

    def _compute_attachment_count(self):
        self.attach_count = None
        for wo in self:
            attach = self.env['ir.attachment'].search(['&', ('res_id', '=', self.id), ('res_model', '=', self._name)])
            if attach:
                # wo.repair_ids = repairs.ids
                wo.attach_count = len(attach)

    @api.depends('parent_location.name')
    def _compute_complete_name(self):
        for group in self:
            if group.parent_location:
                group.complete_name = '%s / %s' % (group.parent_location.name, group.name)
            else:
                group.complete_name = group.name

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('maintenance.work.order.sequence') or 'New'
        return super(MaintenanceWorkOrder, self).create(vals)


    def action_create_repair_order(self):
        repair_order_id = self.env['maintenance.repair.order'].create({
            'created_date': self.created_date,
            'user_id': self.user_id.id,
            'company_id': self.company_id.id,
            'branch_id': self.branch.id,
            'date_start': self.startdate,
            'date_stop': self.enddate,
            'facilities_area': self.facility.id,
            'maintenance_team': self.maintenanceteam.id,
            'maintenance_assignation_type': self.maintenanceassign.id,
            'ref': self.name,
            'remarks': self.remarks,
            'analytic_group_id': self.analytic_group_id.id,
            'work_order_id': self.id,
        })
        if repair_order_id:
            for tas in self.task_check_list_ids:
                repair_order_id.task_check_list_ids = [(0, 0, {
                    'equipment_id': tas.equipment_id.id,
                    'task': tas.task,
                })]
            for tools in self.tools_materials_list_ids:
                repair_order_id.tools_materials_list_ids = [(0, 0, {
                    'product_id': tools.product_id.id,
                    'uom_id': tools.uom_id.id,
                })]
            for material in self.maintenance_materials_list_ids:
                repair_order_id.maintenance_materials_list_ids = [(0, 0, {
                    'product_id': material.product_id.id,
                    'uom_id': material.uom_id.id,
                })]

    def repair_action_link(self):
        self.ensure_one()
        view_form_id = self.env.ref('equip3_asset_fms_operation.maintenance_repair_order_form_view').id
        view_list_id = self.env.ref('equip3_asset_fms_operation.maintenance_repair_order_tree_view').id
        action = {
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', self.repair_ids.ids)],
            'view_mode': 'kanban,form',
            'name': _('Repair Order'),
            'res_model': 'maintenance.repair.order',
        }
        if len(self.repair_ids) == 1:
            action.update({'views': [(view_form_id, 'form')], 'res_id': self.repair_ids.id})
        else:
            action['views'] = [(view_list_id, 'list'), (view_form_id, 'form')]
        return action

    def get_attachment(self):
        self.ensure_one()
        res = self.env['ir.actions.act_window']._for_xml_id('base.action_attachment')
        res['domain'] = [('res_model', '=', 'maintenance.work.order'), ('res_id', 'in', self.ids)]
        res['context'] = {'default_res_model': 'maintenance.work.order', 'default_res_id': self.id}
        return res

    @api.model
    def get_import_templates(self):
        return [{
            'label': _('Import Template for Maintenance Work Order '),
            'template': '/equip3_asset_fms_operation/static/xls/work_order_template.xls'
        }]


class MaintenanceWorkOrderLine(models.Model):
    _name = 'maintenance.work.order.line'

    asset_id = fields.Many2one(comodel_name='maintenance.equipment', string='Asset')
    work_order_id = fields.Many2one(comodel_name='maintenance.work.order', string='Asset')


