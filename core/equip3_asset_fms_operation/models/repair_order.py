from odoo import models, fields, api, _
from datetime import datetime,date
from odoo.exceptions import UserError
from odoo.tools import float_compare

class MaintenanceRepairOrder(models.Model):
    _name = 'maintenance.repair.order'
    _description = 'Maintenance Repair Order'

    @api.depends('maintenance_materials_list_ids.price_total')
    def _amount_all_material(self):
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.maintenance_materials_list_ids:
                line._compute_amount_price()
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.currency_id.round(amount_untaxed),
                'amount_tax': order.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax,
            })
    
    name = fields.Char(string="Repair Order")
    user_id = fields.Many2one('res.users', 'Created By', required=True, readonly=True, default=lambda self: self.env.user)
    created_date = fields.Date(string='Create Date', default=datetime.today().date(), readonly=True)
    company_id = fields.Many2one("res.company", "Company", default=lambda self: self.env.user.company_id)
    branch_id = fields.Many2one('res.branch', string='Branch')
    partner_id = fields.Many2one('res.partner', string='Customer')
    facilities_area = fields.Many2one(comodel_name='maintenance.facilities.area', string='Facilities Area')
    maintenance_team = fields.Many2one(comodel_name='maintenance.teams', string='Maintenance Team', ondelete='restrict')
    maintenance_assignation_type = fields.Many2one(comodel_name='maintenance.assignation.type', string='Maintenance Assignation Type')
    date_start = fields.Date(string='Start Date', required=True, default=datetime.today())
    date_stop = fields.Date(string='End Date', required=True, default=datetime.today())
    ref = fields.Char('Refrence Document', readonly=True)
    equipment_ids = fields.One2many('maintenance.equipment', 'repair_order_id')
    instruction = fields.Html(string='Instruction')
    remarks = fields.Text('Remarks')
    analytic_group_id = fields.Many2many('account.analytic.tag', string='Analytic Group')
    work_order_id = fields.Many2one('maintenance.work.order', string='Work Order')
    task_check_list_ids = fields.One2many('plan.task.check.list','maintenance_ro_id',string='Task Checklist')
    maintenance_materials_list_ids = fields.One2many('maintenance.materials.list','maintenance_ro_id', string='Materials')
    tools_materials_list_ids = fields.One2many('tools.materials.list', 'maintenance_ro_id', string='Tools Materials')
    currency_id = fields.Many2one('res.currency', 'Currency', required=True,
        default=lambda self: self.env.company.currency_id.id)
    state_id = fields.Selection(
        [('draft', 'Draft'), ('in_progress', 'In Progress'),
         ('pending', 'Post'),
         ('done', 'Done'),
         ('cancel', 'Cancelled')],string="State", default='draft')
    time_ids = fields.One2many(
        'time.progress', 'repair_id', copy=False)
    time_in_progress = fields.Float('Time in progress', compute='_compute_duration', inverse='_set_duration',
        readonly=False, store=True, copy=False)
    time_post_ids = fields.One2many(
        'time.post', 'repair_id', copy=False)
    time_post = fields.Float('Time Post', compute='_compute_post_duration', inverse='_set_post_duration',
        readonly=False, store=True, copy=False)
    amount_untaxed = fields.Monetary(string='Untaxed Amount', store=True, readonly=True, compute='_amount_all_material', tracking=True)
    amount_tax = fields.Monetary(string='Taxes', store=True, readonly=True, compute='_amount_all_material')
    amount_total = fields.Monetary(string='Total', store=True, readonly=True, compute='_amount_all_material')
    
    def _prepare_timeline_vals(self, duration, date_start, date_end=False):
        # Need a loss in case of the real time exceeding the expected
        return {
            'repair_id': self.id,
            'date_start': date_start,
            'date_end': date_end,
        }

    @api.depends('time_ids.duration', 'state_id')
    def _compute_duration(self):
        for order in self:
            order.time_in_progress = sum(order.time_ids.mapped('duration'))

    def _set_duration(self):

        def _float_duration_to_second(duration):
            minutes = duration // 1
            seconds = (duration % 1) * 60
            return minutes * 60 + seconds

        for order in self:
            old_order_duation = sum(order.time_ids.mapped('duration'))
            new_order_duration = order.time_in_progress
            if new_order_duration == old_order_duation:
                continue

            delta_duration = new_order_duration - old_order_duation

            if delta_duration > 0:
                date_start = datetime.now() - timedelta(seconds=_float_duration_to_second(delta_duration))
                self.env['time.progress'].create(
                    order._prepare_timeline_vals(delta_duration, date_start, datetime.now())
                )
            else:
                duration_to_remove = abs(delta_duration)
                timelines = order.time_ids.sorted(lambda t: t.date_start)
                timelines_to_unlink = self.env['time.progress']
                for timeline in timelines:
                    if duration_to_remove <= 0.0:
                        break
                    if timeline.duration <= duration_to_remove:
                        duration_to_remove -= timeline.duration
                        timelines_to_unlink |= timeline
                    else:
                        new_time_line_duration = timeline.duration - duration_to_remove
                        timeline.date_start = timeline.date_end - timedelta(seconds=_float_duration_to_second(new_time_line_duration))
                        break
                timelines_to_unlink.unlink()

    @api.depends('time_post_ids.duration', 'state_id')
    def _compute_post_duration(self):
        for order in self:
            order.time_post = sum(order.time_post_ids.mapped('duration'))

    def _set_post_duration(self):

        def _float_duration_to_second(duration):
            minutes = duration // 1
            seconds = (duration % 1) * 60
            return minutes * 60 + seconds

        for order in self:
            old_order_duation = sum(order.time_post_ids.mapped('duration'))
            new_order_duration = order.time_post
            if new_order_duration == old_order_duation:
                continue

            delta_duration = new_order_duration - old_order_duation

            if delta_duration > 0:
                date_start = datetime.now() - timedelta(seconds=_float_duration_to_second(delta_duration))
                self.env['time.post'].create(
                    order._prepare_timeline_vals(delta_duration, date_start, datetime.now())
                )
            else:
                duration_to_remove = abs(delta_duration)
                timelines = order.time_post_ids.sorted(lambda t: t.date_start)
                timelines_to_unlink = self.env['time.post']
                for timeline in timelines:
                    if duration_to_remove <= 0.0:
                        break
                    if timeline.duration <= duration_to_remove:
                        duration_to_remove -= timeline.duration
                        timelines_to_unlink |= timeline
                    else:
                        new_time_line_duration = timeline.duration - duration_to_remove
                        timeline.date_start = timeline.date_end - timedelta(seconds=_float_duration_to_second(new_time_line_duration))
                        break
                timelines_to_unlink.unlink()

    @api.depends('maintenance_materials_list_ids.price_subtotal')
    def _amount_all(self):
        for record in self:
            record.amount_total = sum(record.maintenance_materials_list_ids.mapped('price_subtotal'))
          
    def check_equipment_state(self):
        for order in self:
            for line in order.task_check_list_ids:
                if line.equipment_id and line.equipment_id.state == 'maintenance':
                    raise UserError(_('The asset and/or vehicle that you want to maintain is still under maintenance from other Maintenance Work Order and/or Maintenance Repair Order. Please finish that particular order first.'))

    def update_equipment_state(self):
        plan_obj = self.env['plan.task.check.list']
        for order in self:
            for line in order.task_check_list_ids:
                if line.equipment_id:
                    if order.state_id == 'in_progress':
                        line.equipment_id.write({'state': 'maintenance'})
                    # elif order.state_id == 'pending':
                    #     line.equipment_id.write({'state': 'breakdown'})
                    elif order.state_id == 'done':
                        lines = plan_obj.search([('equipment_id', '=', line.equipment_id.id), ('maintenance_ro_id.state_id', 'in', ('in_progress', 'pending'))])
                        if not lines:
                            line.equipment_id.write({'state': 'operative'})
                    elif order.state_id == 'cancel':
                        lines = plan_obj.search([('equipment_id', '=', line.equipment_id.id), ('maintenance_wo_id.state_id', 'in', ('in_progress', 'pending'))])
                        if not lines:
                            line.equipment_id.write({'state': 'operative'})

    def state_confirm(self):
        # self.check_equipment_state()

        self.write({'state_id': 'in_progress'})
        self.env['time.progress'].create(
            self._prepare_timeline_vals(self.time_in_progress, datetime.now())
        )

        self.update_equipment_state()

    def state_pending(self):
        self.write({'state_id': 'pending'})
        time_ids = self.time_ids.filtered(lambda r:not r.date_end)
        if time_ids:
            time_ids.write({'date_end': datetime.now()})
        self.env['time.post'].create(
            self._prepare_timeline_vals(self.time_post, datetime.now())
        )

        self.update_equipment_state()

    def action_start_again(self):
        # self.check_equipment_state()
        
        self.write({'state_id': 'in_progress'})
        self.env['time.progress'].create(
            self._prepare_timeline_vals(self.time_in_progress, datetime.now())
        )
        time_post_ids = self.time_post_ids.filtered(lambda r:not r.date_end)
        if time_post_ids:
            time_post_ids.write({'date_end': datetime.now()})

        self.update_equipment_state()

    def state_cancel(self):
        self.write({'state_id': 'cancel'})
        time_ids = self.time_ids.filtered(lambda r:not r.date_end)
        if time_ids:
            time_ids.write({'date_end': datetime.now()})
        time_post_ids = self.time_post_ids.filtered(lambda r:not r.date_end)
        if time_post_ids:
            time_post_ids.write({'date_end': datetime.now()})

        self.update_equipment_state()

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code(
                'maintenance.repair.order.sequence') or 'New'
        return super(MaintenanceRepairOrder, self).create(vals)
    
    def state_done(self):
        self.write({'state_id': 'done'})

        self.update_equipment_state()
        
        time_ids = self.time_ids.filtered(lambda r:not r.date_end)
        if time_ids:
            time_ids.write({'date_end': datetime.now()})
        time_post_ids = self.time_post_ids.filtered(lambda r:not r.date_end)
        if time_post_ids:
            time_post_ids.write({'date_end': datetime.now()})
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        Move = self.env['stock.move']
        move = self.env['stock.move']
        for repair in self:
            # Try to create move with the appropriate owner

            moves = self.env['stock.move']
            for operation in repair.maintenance_materials_list_ids:
                if operation.types == "add": 
                    move = Move.create({
                        'name': repair.name,
                        'product_id': operation.product_id.id,
                        'product_uom_qty': operation.product_uom_qty,
                        'product_uom': operation.uom_id.id,
                        'partner_id': repair.partner_id.id,
                        'location_id': operation.location_id.id,
                        'location_dest_id': operation.location_dest_id.id,
                        'origin': repair.name,
                        'company_id': repair.company_id.id,
                    })

                    # Best effort to reserve the product in a (sub)-location where it is available
                    product_qty = move.product_uom._compute_quantity(
                        operation.product_uom_qty, move.product_id.uom_id, rounding_method='HALF-UP')
                    available_quantity = self.env['stock.quant']._get_available_quantity(
                        move.product_id,
                        move.location_id,
                        strict=False,
                    )
                    move._update_reserved_quantity(
                        product_qty,
                        available_quantity,
                        move.location_id,
                        strict=False,
                    )
                    # Then, set the quantity done. If the required quantity was not reserved, negative
                    # quant is created in operation.location_id.
                    move._set_quantity_done(operation.product_uom_qty)

                    moves |= move
                    operation.write({'move_id': move.id})
            if moves:
                consumed_lines = moves.mapped('move_line_ids')
                produced_lines = move.move_line_ids
                moves |= move
                moves._action_done()
                produced_lines.write({'consume_line_ids': [(6, 0, consumed_lines.ids)]})

    @api.model
    def get_import_templates(self):
        return [{
            'label': _('Import Template for Maintenance Repair Order '),
            'template': '/equip3_asset_fms_operation/static/xls/repair_order_template.xls'
        }]

class MaintenanceEquipmentExtend(models.Model):
    _inherit = "maintenance.equipment"

    repair_order_id = fields.Many2one('maintenance.repair.order', string='Maintenance Repair Order')
    workorder_ids = fields.Many2many('maintenance.work.order', string='Workorder', compute="_get_workorder")
    workorder_count = fields.Integer(string='Workorder', compute='_get_workorder')
    repair_count = fields.Integer(compute='_get_workorder')

    def _get_workorder(self):
        self.workorder_ids = None
        self.workorder_count = None
        self.repair_count = None
        for wo in self:
            workorders = self.env['plan.task.check.list'].search([('equipment_id', '=', wo.id), ('maintenance_wo_id', '!=', False)])
            if workorders:
                wo.workorder_ids = workorders.maintenance_wo_id.ids
                wo.workorder_count = len(workorders.maintenance_wo_id.ids)
                repairorders = self.env['maintenance.repair.order'].search([('work_order_id', 'in', wo.workorder_ids.ids)])
                wo.repair_count = len(repairorders.ids)

    def action_view_workorder_ids(self):
        self.ensure_one()
        view_form_id = self.env.ref('equip3_asset_fms_operation.maintenance_work_order_view_form').id
        view_tree_id = self.env.ref('equip3_asset_fms_operation.maintenance_work_order_view_tree').id
        action = {
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', self.workorder_ids.ids)],
            'view_mode': 'tree,form',
            'name': _('Workorder'),
            'res_model': 'maintenance.work.order',
        }
        if len(self.workorder_ids) == 1:
            action.update({'views': [(view_form_id, 'form')], 'res_id': self.workorder_ids.id})
        else:
            action['views'] = [(view_tree_id, 'tree'), (view_form_id, 'form')]
        return action


    # def name_get(self):
    #     result = []
    #     result_all = []
    #     equipment = self.env['maintenance.equipment'].search([])
    #     for rec in equipment:
    #         name = rec.name
    #         result_all += [(rec.id, name)]
    #         if rec.vehicle_checkbox:
    #             result += [(rec.id, name)]
    #     result = list(set(result))
    #     result_all = list(set(result_all))
    #     if self._context.get('is_odometer_m_plan') or self._context.get('fuel_logs'):
    #         return result
    #     else:
    #         return result_all

    def repair_action_link(self):
        res = super(MaintenanceEquipmentExtend, self).repair_action_link()
        res['domain'] = [('work_order_id', 'in', self.workorder_ids.ids)]
        return res

class Repair(models.Model):
    _inherit = 'repair.order'

    def action_repair_done(self):
        if self.filtered(lambda repair: not repair.repaired):
            raise UserError(_("Repair must be repaired in order to make the product moves."))
        self._check_company()
        self.operations._check_company()
        self.fees_lines._check_company()
        res = {}
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        Move = self.env['stock.move']
        for repair in self:
            owner_id = False
            available_qty_owner = self.env['stock.quant']._get_available_quantity(repair.product_id, repair.location_id, repair.lot_id, owner_id=repair.partner_id, strict=True)
            if float_compare(available_qty_owner, repair.product_qty, precision_digits=precision) >= 0:
                owner_id = repair.partner_id.id

            moves = self.env['stock.move']
            for operation in repair.operations:
                if operation.type == 'add':
                    move = Move.create({
                        'name': repair.name,
                        'product_id': operation.product_id.id,
                        'product_uom_qty': operation.product_uom_qty,
                        'product_uom': operation.product_uom.id,
                        'partner_id': repair.address_id.id,
                        'location_id': operation.location_id.id,
                        'location_dest_id': operation.location_dest_id.id,
                        'repair_id': repair.id,
                        'origin': repair.name,
                        'company_id': repair.company_id.id,
                    })
                    product_qty = move.product_uom._compute_quantity(
                        operation.product_uom_qty, move.product_id.uom_id, rounding_method='HALF-UP')
                    available_quantity = self.env['stock.quant']._get_available_quantity(
                        move.product_id,
                        move.location_id,
                        lot_id=operation.lot_id,
                        strict=False,
                    )
                    move._update_reserved_quantity(
                        product_qty,
                        available_quantity,
                        move.location_id,
                        lot_id=operation.lot_id,
                        strict=False,
                    )
                    move._set_quantity_done(operation.product_uom_qty)

                    if operation.lot_id:
                        move.move_line_ids.lot_id = operation.lot_id

                    moves |= move
                    operation.write({'move_id': move.id, 'state': 'done'})
            if moves:  
                move = Move.create({
                    'name': repair.name,
                    'product_id': repair.product_id.id,
                    'product_uom': repair.product_uom.id or repair.product_id.uom_id.id,
                    'product_uom_qty': repair.product_qty,
                    'partner_id': repair.address_id.id,
                    'location_id': repair.location_id.id,
                    'location_dest_id': repair.location_id.id,
                    'move_line_ids': [(0, 0, {'product_id': repair.product_id.id,
                                            'lot_id': repair.lot_id.id,
                                            'product_uom_qty': 0,  # bypass reservation here
                                            'product_uom_id': repair.product_uom.id or repair.product_id.uom_id.id,
                                            'qty_done': repair.product_qty,
                                            'package_id': False,
                                            'result_package_id': False,
                                            'owner_id': owner_id,
                                            'location_id': repair.location_id.id,
                                            'company_id': repair.company_id.id,
                                            'location_dest_id': repair.location_id.id,})],
                    'repair_id': repair.id,
                    'origin': repair.name,
                    'company_id': repair.company_id.id,
                })
                consumed_lines = moves.mapped('move_line_ids')
                produced_lines = move.move_line_ids
                moves |= move
                moves._action_done()
                produced_lines.write({'consume_line_ids': [(6, 0, consumed_lines.ids)]})
                res[repair.id] = move.id
        return res
