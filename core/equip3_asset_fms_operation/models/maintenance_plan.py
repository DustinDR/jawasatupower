from email.policy import default
from odoo import models, fields, api, _
from datetime import datetime, date, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, float_repr



class PlanTaskCheckList(models.Model):
    _name = 'plan.task.check.list'

    equipment_id = fields.Many2one('maintenance.equipment', string='Asset Type')
    task = fields.Text('Task')
    maintenance_plan_id = fields.Many2one('maintenance.plan')
    maintenance_wo_id = fields.Many2one('maintenance.work.order')
    maintenance_ro_id = fields.Many2one('maintenance.repair.order')
    is_odometer_m_plan = fields.Boolean(string='Odometer Plan')

    @api.model
    def default_get(self, fields_list):
        defaults = super(PlanTaskCheckList, self).default_get(fields_list)
        if self._context.get('is_odometer_m_plan'):
            defaults['is_odometer_m_plan'] = True
            if self._context.get('params'):
                defaults['maintenance_plan_id'] = self._context.get('params').get('id')
        return defaults

PlanTaskCheckList()


class MaintenanceMaterialsList(models.Model):
    _name = 'maintenance.materials.list'

    equipment_id = fields.Many2one('maintenance.equipment', string='Materials')
    notes = fields.Char('Notes')
    maintenance_plan_id = fields.Many2one('maintenance.plan')
    maintenance_wo_id = fields.Many2one('maintenance.work.order')
    maintenance_ro_id = fields.Many2one('maintenance.repair.order')
    product_id = fields.Many2one('product.product', string='Tools')
    uom_id = fields.Many2one('uom.uom', string='Tools')
    product_uom_qty = fields.Float(string='Quantity', default=1)
    analytic_group_ids = fields.Many2many('account.analytic.tag', string='Analytic Group')
    location_id = fields.Many2one('stock.location', string="Source Location")
    location_dest_id = fields.Many2one('stock.location', string="Destination Location", domain="[('usage', '=', 'internal')]")
    move_id = fields.Many2one('stock.move', string="Move")
    filter_location_ids = fields.Many2many('stock.location', stirng='Source Location', compute='_get_locations', store=False)
    currency_id = fields.Many2one('res.currency', 'Currency', required=True,
        default=lambda self: self.env.company.currency_id.id)
    taxes_id = fields.Many2many('account.tax', string='Taxes')
    price_unit = fields.Float(string='Unit Price', digits='Product Price')

    price_subtotal = fields.Monetary(compute='_compute_amount_price', string='Subtotal', store=True)
    price_total = fields.Monetary(compute='_compute_amount_price', string='Total', store=True)
    price_tax = fields.Float(compute='_compute_amount_price', string='Tax', store=True)
    types = fields.Selection([("add","Add"),("remove","Remove")], string='Type', default="add")
    
    
    @api.depends('product_uom_qty', 'price_unit', 'taxes_id')
    def _compute_amount_price(self):
        for line in self:
            vals = line._prepare_compute_all_values()
            taxes = line.taxes_id.compute_all(
                vals['price_unit'],
                vals['currency_id'],
                vals['product_uom_qty'])
            line.update({
                'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })

    def _prepare_compute_all_values(self):
        self.ensure_one()
        return {
            'price_unit': self.price_unit,
            'currency_id': self.maintenance_wo_id.currency_id,
            'currency_id': self.maintenance_ro_id.currency_id,
            'product_uom_qty': self.product_uom_qty,
        }

    @api.depends('product_id')
    def _get_locations(self):
        for record in self:
            data_ids = []
            location_ids = []
            stock_quant = record.env['stock.quant'].search([('product_id','=', record.product_id.id)])
            for quant in stock_quant:
                if quant.available_quantity > 0 and quant.location_id.usage == 'internal':
                    location_ids.append(quant.location_id.id)
            record.filter_location_ids = [(6, 0, location_ids)]

    @api.depends('product_uom_qty','price_unit')
    def _amount_all(self):
        for rec in self:
            rec.price_subtotal = rec.product_uom_qty * rec.price_unit
    
    @api.onchange('product_id')
    def on_change_product(self):
        self.uom_id = self.product_id.uom_id.id
        self.price_unit = self.product_id.standard_price

MaintenanceMaterialsList()


class MaintenanceFrequency(models.Model):
    _name = 'maintenance.frequency'
    frequency = fields.Float(string='Frequency')

class MaintenanceThreshold(models.Model):
    _name = 'maintenance.threshold'

    threshold = fields.Float(string='Threshold')
    unit = fields.Char(string='Unit', readonly=True)
    maintenance_plan_id = fields.Many2one('maintenance.plan')

    @api.model
    def default_get(self, fields_list):
        defaults = super(MaintenanceThreshold, self).default_get(fields_list)
        if self._context.get('is_hourmeter_m_plan'):
            defaults['unit'] = 'hours'
        if self._context.get('is_odometer_m_plan'):
            defaults['unit'] = 'km'
        return defaults


class ToolsMaterialsList(models.Model):
    _name = 'tools.materials.list'
    _order = 'maintenance_wo_id, maintenance_ro_id'

    product_id = fields.Many2one('product.product', string='Tools')
    uom_id = fields.Many2one('uom.uom', string='Tools')
    product_uom_qty = fields.Float(string='Quantity', default=1)
    notes = fields.Char('Notes')
    maintenance_plan_id = fields.Many2one('maintenance.plan')
    maintenance_wo_id = fields.Many2one('maintenance.work.order')
    maintenance_ro_id = fields.Many2one('maintenance.repair.order')

    @api.onchange('product_id')
    def on_change_product(self):
        self.uom_id = self.product_id.uom_id.id

ToolsMaterialsList()


class MaintenancePlan(models.Model):
    _inherit = 'maintenance.plan'

    facility_area = fields.Many2one ('maintenance.facilities.area', string='Facilities Area')
    m_assignation_type = fields.Many2one('maintenance.assignation.type', string='Maintenance Assignation Type')
    remarks = fields.Text('Remarks')
    created_on = fields.Date(string='Created On', default=datetime.now(), readonly=True)
    user_id = fields.Many2one('res.users', 'Created By', required=True, default=lambda self: self.env.user, readonly=True)
    branch_id = fields.Many2one('res.branch', string='Branch')
    company_id = fields.Many2one("res.company", "Company", default=lambda self: self.env.user.company_id, readonly=True)
    partner_id = fields.Many2one('res.partner', string='Customers')

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    meter_interval_number = fields.Integer(string='Odoo Meter Interval')
    frequency_interval_number = fields.Integer(string='Frequency')
    hm_interval_number = fields.Integer(string='HM Interval')
    task_check_list_ids = fields.One2many('plan.task.check.list','maintenance_plan_id',string='Task Checklist')
    maintenance_materials_list_ids = fields.One2many('maintenance.materials.list','maintenance_plan_id', string='Materials')
    tools_materials_list_ids = fields.One2many('tools.materials.list', 'maintenance_plan_id', string='Tools Materials')
    is_preventive_m_plan = fields.Boolean(string='Preventive Maintenance Plan', default=False)
    is_hourmeter_m_plan = fields.Boolean(string='Hour Meter Maintenance Plan', default=False)
    is_odometer_m_plan = fields.Boolean(string='Odo Meter Maintenance Plan', default=False)
    maintenance_category_ids = fields.Many2many('maintenance.equipment.category', string='Asset Category')
    maintenance_frequency_ids = fields.Float(string='Frequency')
    maintenance_threshold_ids = fields.One2many('maintenance.threshold', 'maintenance_plan_id', string='Asset Category')
    wo_ids = fields.Many2many('maintenance.repair.order', string='Repair Orders', compute="_compute_wo_count")
    workorder_ids = fields.One2many('maintenance.work.order', 'maintenance_plan_id', string='Maintenance Work Order')
    wo_count = fields.Integer(string='Workorder Count', compute='_compute_wo_count')
    maintenance_team_id = fields.Many2one("maintenance.teams", ondelete='restrict')
    next_wo_date = fields.Date(string='next work order', readonly=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('active', 'Active'),
        ('cancel', 'Cancelled'),
    ], string='State', readonly=True,default='draft')
    is_show_fac_area = fields.Boolean(string='Show Facility Area', compute="_compute_show_fac")

    def action_move_print(self):
        return self.env.ref('equip3_asset_fms_report.action_report_preventive').report_action(self)
    
    @api.onchange('partner_id')
    def on_change_product(self):
        self._compute_show_fac()
    
    def _compute_show_fac(self):
        for rec in self:
            if rec.is_odometer_m_plan or rec.is_hourmeter_m_plan:
                rec.is_show_fac_area = True
            else:
                rec.is_show_fac_area = False
    
    def _compute_wo_count(self):
        self.wo_ids = None
        self.wo_count = None
        for plan in self:
            workorders = self.env['maintenance.work.order'].search([('maintenance_plan_id', '=', self.id)])
            if workorders:
                plan.wo_ids = workorders.ids
                plan.wo_count = len(workorders)

    def action_move_active(self):
        for record in self:
            if record.is_odometer_m_plan or record.is_hourmeter_m_plan:
                categ_id = self.env['maintenance.equipment'].search([('category_id', 'in', record.maintenance_category_ids.ids)])
                categ_id += record.task_check_list_ids.mapped('equipment_id').filtered(lambda r:r.id not in categ_id.ids)
                if record.maintenance_frequency_ids > 0:
                    for rec in categ_id:
                        if record.is_odometer_m_plan:
                            rec.frequency_odoometer_ids = [(0, 0, {'is_odometer_m_plan': record.id, 'floorodoo_value': 0.0})]
                        elif record.is_hourmeter_m_plan:
                            rec.frequency_hourmeter_ids = [(0, 0, {'is_hourmeter_m_plan': record.id, 'floorhour_value': 0.0})]
                if record.maintenance_threshold_ids:
                    for rec in categ_id:
                        if record.is_odometer_m_plan:
                            rec.threshold_odoometer_ids = [(0, 0, {'is_odometer': record.id, 'last_threshold': 0.0})]
                        elif record.is_hourmeter_m_plan:
                            rec.threshold_hourmeter_ids = [(0, 0, {'is_hourmeter': record.id, 'last_threshold': 0.0})]
            record.write({'state':'active'})

    def action_move_cancel(self):
        self.write({'state':'cancel'})

    def workorder_link(self):
        self.ensure_one()
        view_form_id = self.env.ref('equip3_asset_fms_operation.maintenance_work_order_view_form').id
        view_list_id = self.env.ref('equip3_asset_fms_operation.maintenance_work_order_view_tree').id
        action = {
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', self.workorder_ids.ids)],
            'view_mode': 'kanban,form',
            'name': _('Work Order'),
            'context': {'default_maintenance_plan_id': self.id},
            'res_model': 'maintenance.work.order',
        }
        if len(self.workorder_ids) == 1:
            action.update({'views': [(view_form_id, 'form')], 'res_id': self.workorder_ids.id})
        else:
            action['views'] = [(view_list_id, 'list'), (view_form_id, 'form')]
        return action

    def unlink(self):
        for record in self:
            request_line_ids = self.env['request.line'].search(['|', ('is_odometer_m_plan', '=', record.id), ('is_hourmeter_m_plan', '=', record.id)])
            if request_line_ids:
                request_line_ids.unlink()
            threshold_line_ids = self.env['threshold.line'].search(['|', ('is_odometer', '=', record.id), ('is_hourmeter', '=', record.id)])
            if threshold_line_ids:
                threshold_line_ids.unlink()
        return super(MaintenancePlan, self).unlink()

    @api.model
    def default_get(self, fields_list):
        defaults = super(MaintenancePlan, self).default_get(fields_list)
        if self._context.get('categ_id'):
            defaults['maintenance_category_ids'] = [(6, 0, [self._context.get('categ_id')])]
        return defaults

    @api.model
    def create(self, vals):
        res = super(MaintenancePlan, self).create(vals)
        self.create_workorder()
        return res

    def write(self, vals):
        res = super(MaintenancePlan, self).write(vals)
        if vals.get('state') == 'active':
            if self.start_date <= date.today():
                self.write({'next_wo_date': date.today()})
                self.create_workorder()
        return res

    def create_workorder(self):
        asset_lst = []
        plan_data = self.env['maintenance.plan'].search([('is_preventive_m_plan', '=', True), ('start_date', '!=', False), ('end_date', '!=', False), ('frequency_interval_number', '!=', False)])
        startdate = self.start_date
        enddate = self.end_date
        latest_WO = self.wo_count
        while startdate <= enddate:
            plan = self
            work_order = self.env['maintenance.work.order'].search(
                [('maintenance_plan_id', '=', plan.id), ('startdate', '=', startdate)])
            if plan.state == 'active':
                if latest_WO == 0 :
                    if plan.start_date <= date.today() or date.today() <= plan.end_date:
                        work_order_id = self.env['maintenance.work.order'].create({
                            'partner_id': plan.partner_id.id,
                            'facility': plan.facility_area.id,
                            'user_id': plan.user_id.id,
                            'maintenanceteam': plan.maintenance_team_id.id,
                            'maintenanceassign': plan.m_assignation_type.id,
                            'branch': plan.branch_id.id,
                            'company_id': plan.company_id.id,
                            'remarks': plan.remarks,
                            'maintenance_plan_id': plan.id,
                            'ref': plan.name,
                            'startdate': startdate,
                            'enddate': enddate,
                        })
                        for material in plan.maintenance_materials_list_ids:
                            work_order_id.maintenance_materials_list_ids = [(0, 0, {
                                'product_id': material.product_id.id,
                                'product_uom_qty': material.product_uom_qty,
                                'uom_id': material.uom_id.id,
                                'notes': material.notes,
                            })]
                        for tool in plan.tools_materials_list_ids:
                            work_order_id.tools_materials_list_ids = [(0, 0, {
                                'product_id': tool.product_id.id,
                                'product_uom_qty': tool.product_uom_qty,
                                'uom_id': tool.uom_id.id,
                                'notes': tool.notes,
                            })]
                        for task_check in plan.task_check_list_ids:
                            work_order_id.task_check_list_ids = [(0, 0, {
                                'equipment_id': task_check.equipment_id.id,
                                'task': task_check.task,
                            })]
                        for asset in plan.maintenance_category_ids:
                            asset_lst = asset.equipment_ids.ids
                            for eq_id in asset_lst:
                                work_order_id.task_check_list_ids = [(0, 0, {
                                    'equipment_id': eq_id,
                                })]
                        if work_order_id:
                            latest_WO = date.today()
                    else :
                        break
                elif latest_WO >= 1 :
                    if plan.start_date <= date.today() or date.today() <= plan.end_date :
                        if date.today() == latest_WO + plan.frequency_interval_number:
                            work_order_id = self.env['maintenance.work.order'].create({
                            'partner_id': plan.partner_id.id,
                            'facility': plan.facility_area.id,
                            'user_id': plan.user_id.id,
                            'maintenanceteam': plan.maintenance_team_id.id,
                            'maintenanceassign': plan.m_assignation_type.id,
                            'branch': plan.branch_id.id,
                            'company_id': plan.company_id.id,
                            'remarks': plan.remarks,
                            'maintenance_plan_id': plan.id,
                            'ref': plan.name,
                            'startdate': startdate,
                            'enddate': enddate,
                        })
                        for material in plan.maintenance_materials_list_ids:
                            work_order_id.maintenance_materials_list_ids = [(0, 0, {
                                'product_id': material.product_id.id,
                                'product_uom_qty': material.product_uom_qty,
                                'uom_id': material.uom_id.id,
                                'notes': material.notes,
                            })]
                        for tool in plan.tools_materials_list_ids:
                            work_order_id.tools_materials_list_ids = [(0, 0, {
                                'product_id': tool.product_id.id,
                                'product_uom_qty': tool.product_uom_qty,
                                'uom_id': tool.uom_id.id,
                                'notes': tool.notes,
                            })]
                        for task_check in plan.task_check_list_ids:
                            work_order_id.task_check_list_ids = [(0, 0, {
                                'equipment_id': task_check.equipment_id.id,
                                'task': task_check.task,
                            })]
                        for asset in plan.maintenance_category_ids:
                            asset_lst = asset.equipment_ids.ids
                            for eq_id in asset_lst:
                                work_order_id.task_check_list_ids = [(0, 0, {
                                    'equipment_id': eq_id,
                                })]
                        if work_order_id:
                            latest_WO = date.today()
            startdate = date.today() + timedelta(days=plan.frequency_interval_number)
            break

    def add_threshold_action(self):
        view_id = self.env.ref('equip3_asset_fms_operation.create_multiple_threshold_view').id
        return {
            'type': 'ir.actions.act_window',
            'name': _('Add Mutiple Threshold'),
            'res_model': 'multiple.threshold.wizard',
            'target': 'new',
            'view_mode': 'form',
            'views': [[view_id, 'form']],
            'context': {}
        }

    @api.model
    def get_import_templates(self):
        return [{
            'label': _('Import Template for Maintenance Plan'),
            'template': '/equip3_asset_fms_operation/static/xls/maintenance_plan_template.xls'
        }]

MaintenancePlan()


class MaintenanceEquipmentCategory(models.Model):
    _inherit = 'maintenance.equipment.category'

    def name_get(self):
        result = []
        result_all = []
        maintenanceplan = self.env['maintenance.plan'].search([('id', '=', self._context.get('active_id'))]) 
        for rec in self:
            name = rec.name
            result_all += [(rec.id, name)]
            if rec.equipment_ids:
                equ_lst = rec.equipment_ids.mapped('vehicle_checkbox')
                if False not in equ_lst:
                    result += [(rec.id, name)]
            result = list(set(result))
            result_all = list(set(result_all))
        if self._context.get('is_odometer_m_plan'):
            return result
        else:
            return result_all
