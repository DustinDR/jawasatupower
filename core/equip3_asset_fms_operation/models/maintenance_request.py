from odoo import models, fields, api, _
from datetime import datetime,date


class MaintenanceRequest(models.Model):
    _inherit = 'maintenance.request'

    name = fields.Char('Maintenance Request')
    contact_number = fields.Char("Contact Number")
    facility_area = fields.Many2one ('maintenance.facilities.area', string='Facilities Area', required=True)
    facility = fields.Char('Facility')
    remarks = fields.Text('Remarks')
    reference = fields.Char(string='Reference')
    maintenance_team = fields.Many2one(comodel_name='maintenance.team', string='Maintenance Team')
    work_order_count = fields.Integer(compute='get_orders_count')
    note = fields.Text('Note')

    @api.onchange('facility_area')
    def onchange_facility_area(self):
        for rec in self:
            return {'domain': {'equipment_id': [('fac_area', '=', rec.facility_area.id)]}}

    def get_orders_count(self):
        for rec in self:
            order_count = self.env['maintenance.work.order'].search([('maintainence_request_id','=',rec.id)]).ids
            rec.work_order_count = len(order_count)

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('maintainance.request.sequence') or 'New'
        return super(MaintenanceRequest, self).create(vals)

    def create_work_order(self):
        view_id = self.env.ref('equip3_asset_fms_operation.create_work_order_view').id
        return {
            'type': 'ir.actions.act_window',
            'name': _('Create Work Order'),
            'res_model': 'work.order.wizard',
            'target': 'new',
            'view_mode': 'form',
            'views': [[view_id, 'form']],
            'context': {
                'default_asset_id': self.equipment_id.id,
                'default_facility_area':self.facility_area.id,
                'default_description':self.remarks,
                'request_note':self.note,
                'default_ref':self.name,
            }
        }

    def action_view_work_order(self):
        work_orders = self.env['maintenance.work.order'].search([('maintainence_request_id','=',self.id)]).ids
        action = {
            'name': _('Maintenance Work Orders'),
            'type': 'ir.actions.act_window',
            'res_model': 'maintenance.work.order',
            'target': 'current',
        }
        if len(work_orders) == 1:
            wk_order = work_orders[0]
            action['res_id'] = wk_order
            action['view_mode'] = 'form'
            form_view = [(self.env.ref('equip3_asset_fms_operation.maintenance_work_order_view_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [(state, view) for state, view in action['views'] if view != 'form']
            else:
                action['views'] = form_view
        else:
            action['view_mode'] = 'tree,form'
            action['domain'] = [('id', 'in', work_orders)]
        return action

    @api.model
    def get_import_templates(self):
        return [{
            'label': _('Import Template for Maintenance Request '),
            'template': '/equip3_asset_fms_operation/static/xls/request_template.xls'
        }]


MaintenanceRequest()

class MaintenanceWorkOrder(models.Model):
    _inherit = 'maintenance.work.order'

    maintainence_request_id = fields.Many2one('maintenance.request')

    def state_done(self):
        self.write({'state_id': 'done'})

        self.update_equipment_state()
        
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        Move = self.env['stock.move']
        move = self.env['stock.move']
        for repair in self:
            moves = self.env['stock.move']
            for operation in repair.maintenance_materials_list_ids:
                move = Move.create({
                    'name': repair.name,
                    'product_id': operation.product_id.id,
                    'product_uom_qty': operation.product_uom_qty,
                    'product_uom': operation.uom_id.id,
                    'partner_id': repair.partner_id.id,
                    'location_id': operation.location_id.id,
                    'location_dest_id': operation.location_dest_id.id,
                    'origin': repair.name,
                    'company_id': repair.company_id.id,
                })
                product_qty = move.product_uom._compute_quantity(
                    operation.product_uom_qty, move.product_id.uom_id, rounding_method='HALF-UP')
                available_quantity = self.env['stock.quant']._get_available_quantity(
                    move.product_id,
                    move.location_id,
                    strict=False,
                )
                move._update_reserved_quantity(
                    product_qty,
                    available_quantity,
                    move.location_id,
                    strict=False,
                )
                move._set_quantity_done(operation.product_uom_qty)
                moves |= move
                operation.write({'move_id': move.id})
            consumed_lines = moves.mapped('move_line_ids')
            produced_lines = move.move_line_ids
            moves |= move
            moves._action_done()
            produced_lines.write({'consume_line_ids': [(6, 0, consumed_lines.ids)]})

MaintenanceWorkOrder()

class MaintenanceStage(models.Model):
    _inherit = 'maintenance.stage'
