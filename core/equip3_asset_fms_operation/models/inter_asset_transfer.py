from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import ValidationError
from odoo.exceptions import UserError

class InterAssetTransfer(models.Model):
    _name = 'inter.asset.transfer'
    _description = 'Internal Asset Transfer'
    
    @api.returns('self')
    def _default_stage(self):
        return self.env['inter.asset.transfer.stage'].search([], limit=1)
    
    name = fields.Char(string='Name')
    source = fields.Many2one(comodel_name='maintenance.facilities.area', string='Source Location')
    schedule = fields.Date(string='Start Date',default=datetime.today().date())
    description = fields.Text(string='Description')
    destloc = fields.Many2one(comodel_name='maintenance.facilities.area', string='Destination Location')
    created = fields.Char(string='Create By')
    created_date = fields.Date(string='Create Date', readonly=True, default=datetime.today().date())
    asset_ids = fields.One2many(comodel_name='inter.asset', inverse_name='asset_line', string='Asset')
    user_id = fields.Many2one('res.users', 'Created By', required=True, readonly=True, default=lambda self: self.env.user)
    company_id = fields.Many2one("res.company", "Company", default=lambda self: self.env.user.company_id)
    partner_id = fields.Many2one('res.partner', string='Customer')
    stage_id = fields.Many2one('inter.asset.transfer.stage', string='Stage', ondelete='restrict', tracking=True,
                               group_expand='_read_group_stage_ids', default=_default_stage, copy=False)
    state = fields.Selection(
        [('draft', 'Draft'),
         ('in_progress', 'In Progress'),
         ('done', 'Done'),
         ('cancel', 'Cancelled')], string="State", default='draft')
    
    def action_done(self):
        for record in self:
            for line in record.asset_ids:
                line.asset_id.fac_area = line.dest.id            
            record.write({'state': 'done'})

    def action_cancel(self):
        for record in self:
            record.write({'state': 'cancel'})

    def action_confirm(self):
        for record in self:
            record.write({'state': 'in_progress'})

    def write(self, vals):
        if vals.get('stage_id'):
            stage_id = self.env['inter.asset.transfer.stage'].browse(vals.get('stage_id'))
            if not stage_id.custom_user_ids:
                return super(InterAssetTransfer, self).write(vals)
            if self.env.user.id in stage_id.custom_user_ids.ids:
                return super(InterAssetTransfer, self).write(vals)
            else:
                raise UserError(_("You can not move internal asset transfer to this stage. Please contact your manager."))
            return super(InterAssetTransfer, self).write(vals)
        return super(InterAssetTransfer, self).write(vals)
       
    
    @api.constrains('source', 'destloc')
    def _check_source(self):
        for record in self :
            same = record.source == record.destloc
            if same:
                raise ValidationError("Source Location must be diferent with Destination Location")
  
    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code(
                'inter.asset.transfer.sequence') or 'New'
        return super(InterAssetTransfer, self).create(vals)

    @api.model
    def get_import_templates(self):
        return [{
            'label': _('Import Template for Internal Asset Transfer '),
            'template': '/equip3_asset_fms_operation/static/xls/internal_asset_transfer_template.xls'
        }]
    
class InterAsset(models.Model):
    _name = 'inter.asset'
    
    no_asset_sequence = fields.Integer(string='NO', default=1)
    asset_id = fields.Many2one(comodel_name='maintenance.equipment', string='Asset')
    asset_line = fields.Many2one(comodel_name='inter.asset.transfer', string='Asset')
    #qty = fields.Integer(string='Qty')
    receive = fields.Char(string='Received Quantity')
    source = fields.Many2one(comodel_name='maintenance.facilities.area', string='Source Location', related='asset_line.source')
    dest = fields.Many2one(comodel_name='maintenance.facilities.area', string='Destination Location', related='asset_line.destloc')
    
    
    @api.model
    def default_get(self, fields):
        res = super(InterAsset, self).default_get(fields)
        if self._context:
            context_keys = self._context.keys()
            next_sequence = 1
            if 'asset_ids' in context_keys:
               if len(self._context.get('asset_ids')) > 0:
                    next_sequence = len(self._context.get('asset_ids')) + 1
            res.update({'no_asset_sequence': next_sequence})
        return res
    
class InterAssetTransferStage(models.Model):
    _name = 'inter.asset.transfer.stage'
    _description = 'Inter Asset Transfer Stage'
    _order = 'sequence, id'

    name = fields.Char('Name', required=True, translate=True)
    sequence = fields.Integer('Sequence', default=20)
    fold = fields.Boolean('Folded in Maintenance Pipe')
    done = fields.Boolean('InterAsset Done')
    custom_user_ids = fields.Many2many('res.users', string='Allow Users', help="Selected users can move maintenance repairorder into this stage.")
    custom_mail_template_id = fields.Many2one('mail.template', string='Email Template', domain=[('model', '=', 'inter.asset.transfer')])