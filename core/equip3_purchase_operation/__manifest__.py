# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Equip3 Purchase Operation',
    'version': '1.1.47',
    'summary': 'Manage your Purchase Requests and Purchase Orders Activities',
    'description': """
    This module manages these features :
    1. Purchase Request
    2. Purchase Request Line
    3. Purchase Request Approving Matrix
    4. RFQ
    5. RFQ Approving Matrix
    6. Purchase Order
    7. Invoicing
    8. Shipment
    """,
    'depends': ['purchase_stock', 'purchase_request', 
                'purchase_last_price_info', 'product',
                'sh_all_in_one_purchase_tools', 
                'purchase_requisition', 
                'sh_sale_credit_limit',
                'bi_sale_purchase_discount_with_tax',
                'dev_purchase_team',
                'sh_po_tender_management',
                'sh_po_status',
                'sh_rfq_portal',
                'equip3_purchase_masterdata',
                'branch',
                'eq_po_multi_warehouse',
                'general_template',
                'stock','mail',
                'equip3_purchase_accessright_setting',
                'sh_purchase_revisions', 
                'app_purchase_superbar',
                'ss_whatsapp_connector',
                ],
    'category': 'Purchase/Purchase',
    'data': [
        'security/purchase_request_group.xml',
        'security/ir.model.access.csv',
        'data/mail_purchase_order_template.xml',
        'data/mail_purchase_request_template.xml',
        'data/ir_cron_data.xml',
        'data/mail_template_data.xml',
        'data/rfq_sequence.xml',
        'wizard/approval_reject_views.xml',
        'wizard/approval_request_views.xml',
        'wizard/product_bundle_views_wizard.xml',
        'report/report_purchase.xml',
        'report/purchase_template.xml',
        'report/report_quotation_template.xml',
        'report/report_purchase_template.xml',
        'views/general_config_settings.xml',
        'views/purchase_order_views.xml',
        'views/product_template_views.xml',
        'views/res_partner_views.xml',
        'views/purchase_operation.xml',
        "views/pr_exclusive_templates.xml",
        "views/res_company_views.xml",
        "views/purchase_custom_checklist_template.xml",
        'wizard/vendor_purchase_limit_views.xml',
        'views/approval_matrix_purchase_order.xml',
        'views/approval_matrix_purchase_request.xml',
        'views/term_and_condition_view.xml',
        'views/mail_compose_message_views.xml',
        'views/assets.xml',
        'wizard/multiple_rfq_views.xml',
    ],
    'qweb': [
        "static/src/xml/purchase_dashboard.xml",
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'post_init_hook': 'assign_purchase_request_template',
}
