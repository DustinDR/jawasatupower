
from odoo import _, api, fields, models
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

class ApprovalReject(models.TransientModel):
    _name = "approval.reject"

    reason = fields.Text(string="Reason", required=True)

    def action_reject(self):
        purchase_order_id = self.env['purchase.order'].browse(self._context.get('active_ids'))
        user = self.env.user
        approving_matrix_line = sorted(purchase_order_id.approved_matrix_ids.filtered(lambda r:not r.approved), key=lambda r:r.sequence)
        if approving_matrix_line:
            matrix_line = approving_matrix_line[0]
            name = matrix_line.state_char or ''
            if name != '':
                name += "\n • %s: Rejected" % (user.name)
            else:
                name += "• %s: Rejected" % (user.name)
            matrix_line.write({'state_char': name, 'time_stamp': datetime.now(), 'feedback': self.reason})
        purchase_order_id.write({'state' : 'reject'})
