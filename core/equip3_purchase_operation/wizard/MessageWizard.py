# -*- coding: utf-8 -*-
import random
import pytz
import base64
import io
from datetime import datetime, time, timedelta
from pytz import timezone, UTC
from odoo.exceptions import ValidationError
from odoo.tools import plaintext2html
from odoo import api, fields, models, _, SUPERUSER_ID
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from PyPDF2 import  PdfFileReader, PdfFileWriter


class WhatsappSendMessage(models.TransientModel):
    _inherit = 'whatsapp.message.wizard'

    attachment_ids = fields.Many2many(
        'ir.attachment', 'whatsapp_message_compose_ir_attachments_rel',
        'wizard_id', 'attachment_id', 'Attachments')

    @api.model
    def default_get(self, default_fields):
        res = super(WhatsappSendMessage, self).default_get(default_fields)
        context = dict(self.env.context) or {}
        if context.get('active_model') == "purchase.order":
            order_id = self.env['purchase.order'].browse(context.get('active_ids'))
            ir_model_data = self.env['ir.model.data']
            if order_id.state != 'purchase':
                template_id = ir_model_data.get_object_reference('purchase', 'email_template_edi_purchase')[1]
            else:
                template_id = ir_model_data.get_object_reference('purchase', 'email_template_edi_purchase_done')[1]
            template_values = self.env['mail.template'].browse(template_id).generate_email(order_id.ids, ['attachment_ids'])
            attachment_ids = []
            Attachment = self.env['ir.attachment']
            for attach_fname, attach_datas in template_values[order_id.id].pop('attachments', []):
                data_attach = {
                    'name': attach_fname,
                    'datas': attach_datas,
                    'expiry_date': datetime.now(),
                    'res_model': 'purchase.order',
                    'res_id': order_id.id,
                    'type': 'binary',  # override default_type from context, possibly meant for another model!
                }
                attachment_id = Attachment.create(data_attach)
                attachment_id.generate_access_token()
                attachment_ids.append(attachment_id.id)
            res['attachment_ids'] = [(6, 0, attachment_ids)]
        return res

    def send_message(self):
        context = dict(self.env.context) or {}
        if context.get('active_model') == "purchase.order":
            order_id = self.env['purchase.order'].browse(context.get('active_ids'))
            if order_id.state != 'purchase':
                order_id.write({'state': 'sent'})
        return super(WhatsappSendMessage, self).send_message()
