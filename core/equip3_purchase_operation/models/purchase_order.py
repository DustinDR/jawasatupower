# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from concurrent.futures import process
from odoo import tools
from odoo import api, fields, models, SUPERUSER_ID, tools, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime, timedelta, date
from odoo.http import request
from dateutil.relativedelta import relativedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import pytz
from pytz import timezone, UTC


_STATES = [
    ("draft", "Draft"),
    ("to_approve", "Waiting For Approval"),
    ("approved", "Purchase Request Approved"),
    ("purchase_request", "Purchase Request"),
    ("rejected", "Rejected"),
    ("done", "Done"),
]

class PurchaseRequest(models.Model):
    _inherit = 'purchase.request'

    is_goods_orders = fields.Boolean(string="Goods Orders", default=False)
    approval_matrix_id = fields.Many2one('approval.matrix.purchase.request',string="Approval Matrix", compute="_compute_approval_matrix_request", store=True)
    is_approval_matrix_request = fields.Boolean(compute="_compute_is_approval_matrix_request", string="Approving Matrix")
    price_total = fields.Float(string="Price Total", compute="_compute_price_total")
    approved_matrix_ids = fields.One2many('approval.matrix.purchase.request.line', 'request_id', compute="_compute_approving_matrix_lines", store=True, string="Approved Matrix")
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button', store=False)
    approval_matrix_line_id = fields.Many2one('approval.matrix.purchase.request.line', string='Purchase Request Approval Matrix Line', compute='_get_approve_button', store=False)

    analytic_account_group_ids = fields.Many2many('account.analytic.tag', string='Analytic Group')
    request_date = fields.Date(string="Expected Date")
    pr_request_date = fields.Date(string="Request Date", readonly=True)
    is_single_request_date = fields.Boolean(string="Single Request Date")
    destination_warehouse = fields.Many2one('stock.warehouse', string="Destination")
    is_single_delivery_destination = fields.Boolean(string="Single Delivery Destination")
    analytic_accounting = fields.Boolean("Analyic Account", compute="get_analytic_accounting", store=True)
    partner_id = fields.Many2one(related="requested_by.partner_id", string='Partner')
    report_template_id = fields.Many2one('ir.actions.report', string="Purchase Request Template",
                                         help="Please select Template report for Purchase Request", domain=[('model', '=', 'purchase.request')])

    @api.onchange('analytic_account_group_ids', 'company_id')
    def set_account_tag(self):
        for res in self:
            for line in res.line_ids:
                line.analytic_account_group_ids = res.analytic_account_group_ids

    def _reset_sequence(self):
        for rec in self:
            current_sequence = 1
            for line in rec.line_ids:
                line.sequence = current_sequence
                current_sequence += 1

    def button_confirm_pr(self):
        for record in self:
            record.write({'state': 'purchase_request'})

    def button_to_approve(self):
        for record in self:
            approver = False
            for line in record.line_ids:
                if line.product_qty <= 0 :
                    raise ValidationError("Quantity should be greater then 0!")
            data = []
            template_id = self.env.ref('equip3_purchase_operation.email_template_purchase_request')
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = base_url + '/web#id=' + str(record.id) + '&view_type=form&model=purchase.order'
            if record.approved_matrix_ids and len(record.approved_matrix_ids[0].user_ids) > 1:
                for approved_matrix_id in record.approved_matrix_ids[0].user_ids:
                    approver = approved_matrix_id
                    ctx = {
                        'email_from' : self.env.user.company_id.email,
                        'email_to' : approver.partner_id.email,
                        'approver_name' : approver.name,
                        'requested_by' : self.env.user.name,
                        'product_lines' : data,
                        'url' : url,
                    }
                    template_id.with_context(ctx).send_mail(record.id, True)
            else:
                approver = record.approved_matrix_ids[0].user_ids[0]
                ctx = {
                    'email_from': self.env.user.company_id.email,
                    'email_to': approver.partner_id.email,
                    'approver_name': approver.name,
                    'requested_by': self.env.user.name,
                    'product_lines': data,
                    'url': url,
                }
                template_id.with_context(ctx).send_mail(record.id, True)
        return super(PurchaseRequest, self).button_to_approve()

    @api.model
    def default_get(self, fields):
        res = super(PurchaseRequest, self).default_get(fields)
        new_date = datetime.now().date() + timedelta(days=14)
        res['request_date'] = new_date
        res['pr_request_date'] = datetime.now().date()
        if 'is_goods_orders' in res:
            if self.env['ir.config_parameter'].sudo().get_param('is_good_services_order') == 'True':
                if 'is_goods_orders' in res:
                    if res['is_goods_orders']:
                        expiry_date = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.pr_expiry_date_goods') or 0
                    else:
                        expiry_date = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.pr_expiry_date_services') or 0
            else:
                expiry_date = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.pr_expiry_date') or 0
            res.update({
                'expiry_date': datetime.now() + timedelta(days=int(expiry_date))
            })
        analytic_priority_ids = self.env['analytic.priority'].search([], order="priority")
        for analytic_priority in analytic_priority_ids:
            if analytic_priority.object_id == 'user' and self.env.user.analytic_tag_ids:
                res.update({
                    'analytic_account_group_ids': [(6, 0, self.env.user.analytic_tag_ids.ids)]
                }) 
                break
            elif analytic_priority.object_id == 'branch' and self.env.user.branch_id.analytic_tag_ids:
                res.update({
                    'analytic_account_group_ids': [(6, 0, self.env.user.branch_id.analytic_tag_ids.ids)]
                })
                break
        return res
        
    @api.depends('company_id')
    def get_analytic_accounting(self):
        for res in self:
            res.analytic_accounting = self.user_has_groups('analytic.group_analytic_accounting')

    @api.onchange('request_date', 'is_single_request_date')
    def onchange_request_date(self):
        for record in self:
            for line in record.line_ids:
                if record.is_single_request_date and record.request_date:
                    line.date_required = record.request_date

    def _get_street(self, partner):
        self.ensure_one()
        res = {}
        address = ''
        if partner.street:
            address = "%s" % (partner.street)
        if partner.street2:
            address += ", %s" % (partner.street2)
        # reload(sys)
        html_text = str(tools.plaintext2html(address, container_tag=True))
        data = html_text.split('p>')
        if data:
            return data[1][:-2]
        return False

    def _get_address_details(self, partner):
        self.ensure_one()
        res = {}
        address = ''
        if partner.city:
            address = "%s" % (partner.city)
        if partner.state_id.name:
            address += ", %s" % (partner.state_id.name)
        if partner.zip:
            address += ", %s" % (partner.zip)
        if partner.country_id.name:
            address += ", %s" % (partner.country_id.name)
        # reload(sys)
        html_text = str(tools.plaintext2html(address, container_tag=True))
        data = html_text.split('p>')
        if data:
            return data[1][:-2]
        return False

    @api.onchange('destination_warehouse')
    def _onchange_destination_warehouse(self):
        for res in self:
            for line in res.line_ids:
                if res.is_single_delivery_destination:
                    line.dest_loc_id = res.destination_warehouse.id

    @api.onchange('is_single_delivery_destination')
    def onchange_destination_warehouse(self):
        for record in self:
            if record.is_single_delivery_destination:
                stock_warehouse = record.env['stock.warehouse'].search([], order="id", limit=1)
                record.destination_warehouse = stock_warehouse[0]

    def _get_approve_button(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False


    @api.depends('approval_matrix_id')
    def _compute_approving_matrix_lines(self):
        data = [(5, 0, 0)]
        for record in self:
            counter = 1
            record.approved_matrix_ids = []
            for line in record.approval_matrix_id.approval_matrix_purchase_request_line_ids:
                data.append((0, 0, {
                    'sequence' : counter,
                    'user_ids' : [(6, 0, line.user_ids.ids)],
                    'minimum_approver' : line.minimum_approver,
                }))
                counter += 1
            record.approved_matrix_ids = data

    @api.depends('line_ids', 'line_ids.price_total')
    def _compute_price_total(self):
        for record in self:
            record.price_total = sum(record.line_ids.mapped('price_total'))

    def _compute_is_approval_matrix_request(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        approval =  IrConfigParam.get_param('is_purchase_request_approval_matrix')
        for record in self:
            record.is_approval_matrix_request = approval

    @api.onchange('requested_by')
    def onchange_purchase_name(self):
        self._compute_is_approval_matrix_request()

    @api.depends('branch_id', 'company_id')
    def _compute_approval_matrix_request(self):
        for record in self:
            record.approval_matrix_id = False
            if record.is_approval_matrix_request:
                approval_matrix_id = self.env['approval.matrix.purchase.request'].search([
                            ('branch_id', '=', record.branch_id.id), ('company_id', '=', record.company_id.id)], limit=1)
                record.approval_matrix_id = approval_matrix_id and approval_matrix_id.id or False

    def action_request_approval(self):
        pass

    def action_approve(self):
        pass

    def action_reject(self):
        return {
                'type': 'ir.actions.act_window',
                'name': 'Rejected Reason',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'approval.request.reject',
                'target': 'new',
            }

    def action_confirm_purchase_request(self):
        for record in self:
            for line in record.line_ids:
                if line.product_qty <= 0:
                    raise ValidationError("Quantity should be greater then 0!")
            if not record.line_ids:
                raise ValidationError("Please Enter Lines Data!")
            else:
                record.write({'state': 'approved', 'purchase_req_state': 'pending'})

    @api.model
    def _company_get(self):
        return self.env["res.company"].browse(self.env.company.id)

    @api.model
    def _get_default_requested_by(self):
        return self.env["res.users"].browse(self.env.uid)

    # @api.model
    # def _default_picking_type(self):
    #     type_obj = self.env["stock.picking.type"]
    #     company_id = self.env.context.get("company_id") or self.env.company.id
    #     types = type_obj.search(
    #         [("code", "=", "incoming"), ("warehouse_id.company_id", "=", company_id)]
    #     )
    #     if not types:
    #         types = type_obj.search(
    #             [("code", "=", "incoming"), ("warehouse_id", "=", False)]
    #         )
    #     return types[:1]


    name = fields.Char(
        string="Request Reference",
        required=False,
        default=False,
        tracking=True,
    )
    state = fields.Selection(
        selection=_STATES,
        string="Status",
        index=True,
        required=True,
        copy=False,
        default="draft",
        tracking=True,
    )
    pr_state = fields.Selection(related='state')
    purchase_req_state = fields.Selection([('pending', 'Pending'), ('in_progress', 'In Progress'), ('done', 'Done'), ('close', 'Closed'), ('cancel', 'Cancelled')], tracking=True)
    purchase_req_state_1 = fields.Selection(related='purchase_req_state')
    purchase_req_state_2 = fields.Selection(related='purchase_req_state')
    origin = fields.Char(string="Source Document", tracking=True)
    is_goods_orders = fields.Boolean(string="Goods Orders", default=False)
    date_start = fields.Date(
        string="Creation date",
        help="Date when the user initiated the request.",
        tracking=True,
        default=fields.Date.context_today,
    )
    requested_by = fields.Many2one(
        comodel_name="res.users",
        string="Requested by",
        required=True,
        copy=False,
        tracking=True,
        default=_get_default_requested_by,
        index=True,
    )
    assigned_to = fields.Many2one(
        comodel_name="res.users",
        string="Approver",
        tracking=True,
        domain=lambda self: [
            (
                "groups_id",
                "in",
                self.env.ref("purchase_request.group_purchase_request_manager").id,
            )
        ],
        index=True,
    )
    description = fields.Text(string="Description", tracking=True)
    company_id = fields.Many2one(
        comodel_name="res.company",
        string="Company",
        required=True,
        default=_company_get,
        tracking=True,
    )
    picking_type_id = fields.Many2one(
        comodel_name="stock.picking.type",
        string="Picking Type",
        required=True,
        tracking=True,
    )
    group_id = fields.Many2one(
        comodel_name="procurement.group",
        string="Procurement Group",
        copy=False,
        tracking=True,
        index=True,
    )
    expiry_date = fields.Date('Expiry Date', tracking=True)
    branch_id = fields.Many2one('res.branch', domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]", string="Branch", tracking=True)
    days_left = fields.Integer("Days Left", tracking=True)

    def button_draft(self):
        res = super(PurchaseRequest, self).button_draft()
        for record in self:
            record.write({'purchase_req_state': 'pending'})
        return res
    
    def auto_cancel_pr(self):
        pr = self.env['purchase.request'].search(['|',('state', '!=', 'done'),('purchase_req_state', '!=', 'done')])
        for res in pr:
            if str(res.expiry_date) == datetime.strftime(datetime.now(), tools.DEFAULT_SERVER_DATE_FORMAT):
               if not res.purchase_count:
                   res.update({
                       'purchase_req_state': "cancel"
                   })
               else:
                   purchase_order_ids = res.mapped("line_ids.purchase_lines.order_id")
                   for order in purchase_order_ids:
                       if res.purchase_req_state == 'in_progress' and order.state == 'cancel' or not order.active:
                            res.write({'purchase_req_state': 'cancel'})
                
    def send_email(self):
        template_before = self.env.ref('equip3_purchase_operation.email_template_pr_expiry_reminder')
        template_after = self.env.ref('equip3_purchase_operation.email_template_pr_expiry_reminder_after')
        notif = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.pr_expiry_notification')
        on_date = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.pr_on_date_notify')
        before_exp = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.pr_enter_before_first_notify') or 3
        after_exp = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.pr_enter_after_first_notify') or 1
        pr = self.env['purchase.request'].search(['|',('state', 'not in', ('approved', 'done', 'rejected')),('purchase_req_state', '!=', 'done')])
        
        if notif:
            for res in pr:
                if res.expiry_date:
                    if str(res.expiry_date) == datetime.strftime(datetime.now() + timedelta(days=int(before_exp)), tools.DEFAULT_SERVER_DATE_FORMAT):
                        # Before Expiry Date
                        res.days_left = int(before_exp)
                        template_before.send_mail(
                            res.id, force_send=True)
                    elif str(res.expiry_date) == datetime.strftime(datetime.now() - timedelta(days=int(after_exp)), tools.DEFAULT_SERVER_DATE_FORMAT):
                        # After Expiry Date
                        template_after.send_mail(
                            res.id, force_send=True)
                
                        
    def get_full_url(self):
        for res in self:
            base_url = request.env['ir.config_parameter'].get_param('web.base.url')
            base_url += '/web#id=%d&view_type=form&model=%s' % (res.id, res._name)
            return base_url

    @api.model
    def create(self, vals):
        if self.env['ir.config_parameter'].sudo().get_param('is_good_services_order') == 'True':
            if vals.get('is_goods_orders'):
                vals['name'] = self.env['ir.sequence'].next_by_code('purchase.req.seqs.goods')
            else:
                vals['name'] = self.env['ir.sequence'].next_by_code('purchase.req.seqs.services')
        else:
            vals['name'] = self.env['ir.sequence'].next_by_code('purchase.req.seqs')
        # exp_date = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.pr_expiry_date') or 30
        # vals['expiry_date'] = datetime.now() + timedelta(days=int(exp_date))
        return super(PurchaseRequest, self).create(vals)

    def button_approved(self):
        for record in self:
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = base_url + '/web#id='+ str(record.id) + '&view_type=form&model=purchase.request'
            data = []
            template_id = self.env.ref('equip3_purchase_operation.email_template_purchase_request_approve')
            user = self.env.user
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_ids.ids and \
                    user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    if name != '':
                        name += "\n • %s: Approved" % (self.env.user.name)
                    else:
                        name += "• %s: Approved" % (self.env.user.name)

                    approval_matrix_line_id.write({ 
                        'last_approved': self.env.user.id, 'state_char': name, 
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
                        next_approval_matrix_line_id = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
                        if next_approval_matrix_line_id and len(next_approval_matrix_line_id[0].user_ids) > 1:
                            for approving_matrix_line_user in next_approval_matrix_line_id[0].user_ids:
                                ctx = {
                                    'email_from': self.env.user.company_id.email,
                                    'email_to': approving_matrix_line_user.partner_id.email,
                                    'user_name': approving_matrix_line_user.name,
                                    'approver_name': ','.join(approval_matrix_line_id.user_ids.mapped('name')),
                                    'url': url,
                                    'product_lines': data,
                                }
                                template_id.sudo().with_context(ctx).send_mail(record.id, True)
                        else:
                            if next_approval_matrix_line_id and next_approval_matrix_line_id[0].user_ids:
                                ctx = {
                                    'email_from': self.env.user.company_id.email,
                                    'email_to': next_approval_matrix_line_id[0].user_ids[0].partner_id.email,
                                    'user_name': next_approval_matrix_line_id[0].user_ids[0].name,
                                    'approver_name': ','.join(approval_matrix_line_id.user_ids.mapped('name')),
                                    'url': url,
                                    'product_lines': data,
                                }
                                template_id.sudo().with_context(ctx).send_mail(record.id, True)
                        
            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r:r.approved)):
                record.write({'purchase_req_state': 'pending', 'state': 'approved'})

    def button_done_pr(self):
        for record in self:
            if record.purchase_count <= 0:
                raise ValidationError(_("Please create RFQ"))
            else:
                record.write({'purchase_req_state': 'done'})

    def button_close_pr(self):
        for record in self:
            record.write({'purchase_req_state': 'close'})

    def button_cancel_pr(self):
        for record in self:
            purchase_order_ids = record.mapped("line_ids.purchase_lines.order_id")
            for order in purchase_order_ids:
                if order.state in ('purchase', 'done'):
                    raise ValidationError(_("Purchase Request cannot be cancelled. Purchase Order to vendor has been created for this Purchase Request."))
                if order.state != 'cancel':
                    raise ValidationError(_("There is Active RFQ. If you want to cancel Purchase Request please cancel the RFQ."))
            record.write({'purchase_req_state': 'cancel'})

class MailMessage(models.Model):
    _inherit = 'mail.message'

    @api.model
    def create(self, vals):
        if vals.get('model') and \
            vals.get('model') == 'purchase.request' and vals.get('tracking_value_ids'):
            purchase_req_1 = self.env['ir.model.fields']._get('purchase.request', 'purchase_req_state_1').id
            purchase_req_2 = self.env['ir.model.fields']._get('purchase.request', 'purchase_req_state_2').id
            pr_state = self.env['ir.model.fields']._get('purchase.request', 'pr_state').id
            vals['tracking_value_ids'] = [rec for rec in vals.get('tracking_value_ids') if 
                                        rec[2].get('field') not in (purchase_req_1, purchase_req_2, pr_state)]
        if vals.get('model') and \
            vals.get('model') == 'purchase.order' and vals.get('tracking_value_ids'):
            state1 = self.env['ir.model.fields']._get('purchase.order', 'state1').id
            po_state = self.env['ir.model.fields']._get('purchase.order', 'po_state').id
            vals['tracking_value_ids'] = [rec for rec in vals.get('tracking_value_ids') if 
                                        rec[2].get('field') not in (state1, po_state)]
        return super(MailMessage, self).create(vals)

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'
    
    po_expiry_date = fields.Datetime('PO Expiry Date', tracking=True)
    po = fields.Boolean('PO')
    exp_po = fields.Boolean('EXP PO')
    sent = fields.Boolean(string='Sent')
    is_goods_orders = fields.Boolean(string="Goods Orders", default=False)
    is_hold_purchase_order = fields.Boolean(compute="_compute_hold_purchase_order", string="Hold Purchase Order", store=False)
    approval_matrix_id = fields.Many2one('approval.matrix.purchase.order', compute="_compute_approval_matrix_id", string="Approval Matrix", store=True)
    is_approval_matrix = fields.Boolean(compute="_compute_approval_matrix", string="Approving Matrix")
    state = fields.Selection(selection_add=[
        ('waiting_for_approve', 'Waiting For Approval'),
        ('rfq_approved', 'RFQ Approved'),
        ('purchase', 'Purchase Order'),
        ('reject', 'Rejected'),
        ('done', 'Locked'),
        ('on_hold', 'On Hold'),
        ('cancel',)
        ])
    state1 = fields.Selection(related="state")
    po_state = fields.Selection(related="state")
    approved_matrix_ids = fields.One2many('approval.matrix.purchase.order.line', 'order_id', compute="_compute_approving_matrix_lines", store=True, string="Approved Matrix")
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button', store=False)
    approval_matrix_line_id = fields.Many2one('approval.matrix.purchase.order.line', string='Purchase Order Approval Matrix Line', compute='_get_approve_button', store=False)
    analytic_account_group_ids = fields.Many2many('account.analytic.tag', string='Analytic Group')
    company_id = fields.Many2one(readonly=True)
    branch_id = fields.Many2one(domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")
    destination_location_id = fields.Many2one('stock.location', string='Destination Location')
    is_delivery_receipt = fields.Boolean(string="Delivery With Receipt Date")
    date_planned = fields.Datetime(inverse='_inverse_date_planned')
    destination_warehouse_id = fields.Many2one('stock.warehouse', string='Destination', copy=True)
    discount_type = fields.Selection(string='Discount Type')
    is_single_delivery_destination = fields.Boolean(string="Single Delivery Destination")
    analytic_accounting = fields.Boolean("Analyic Account", compute="get_analytic_accounting", store=True)
    is_revision_po = fields.Boolean(string="Revison PO")
    is_revision_created = fields.Boolean(string='Revison Created', copy=False)
    revision_order_id = fields.Many2one('purchase.order', string='Revison Order')
    custom_checklist_template_ids = fields.Many2many(
        'purchase.custom.checklist.template', 'purchase_checklist_rel', 'order_id', 'checlist_id')
    readonly_price = fields.Boolean("Readonly Price", compute='compute_retail')
    timezone_date = fields.Char(compute="_compute_timezone_date", store=True, string='Date')

    @api.depends('state')
    def _compute_timezone_date(self):
        for record in self:
            record.timezone_date = ""
            if record.state == 'purchase':
                timezone_date = record.date_approve
            else:
                timezone_date = record.create_date
            if timezone_date:
                deadline_timezone_date = pytz.timezone(self.env.user.tz)
                time_zone_date = timezone_date.replace(tzinfo=pytz.utc)
                time_zone_date = time_zone_date.astimezone(deadline_timezone_date).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                record.timezone_date = time_zone_date

    @api.model
    def retrieve_dashboard(self):
        res = super(PurchaseOrder, self).retrieve_dashboard()
        po = self.env['purchase.order']
        context = dict(self.env.context) or {}
        one_week_ago = fields.Datetime.to_string(fields.Datetime.now() - relativedelta(days=7))
        query = """SELECT AVG(COALESCE(po.amount_total / NULLIF(po.currency_rate, 0), po.amount_total)),
                          AVG(extract(epoch from age(po.date_approve,po.create_date)/(24*60*60)::decimal(16,2))),
                          SUM(CASE WHEN po.date_approve >= %s THEN COALESCE(po.amount_total / NULLIF(po.currency_rate, 0), po.amount_total) ELSE 0 END),
                          MIN(curr.decimal_places)
                   FROM purchase_order po
                   JOIN res_company comp ON (po.company_id = comp.id)
                   JOIN res_currency curr ON (comp.currency_id = curr.id)
                   WHERE po.state in ('purchase', 'done')
                    AND po.is_goods_orders = %s
                    AND po.company_id = %s
                """
        if context.get('goods_order'):
            res['all_to_send'] = po.search_count([('state', '=', 'draft'), ('is_goods_orders', '=', True)])
            res['my_to_send'] = po.search_count([('state', '=', 'draft'), ('user_id', '=', self.env.uid), ('is_goods_orders', '=', True)])
            res['all_waiting'] = po.search_count([('state', 'in', ('waiting_for_approve', 'rfq_approved')), ('date_order', '>=', fields.Datetime.now()), ('is_goods_orders', '=', True)])
            res['my_waiting'] = po.search_count([('state', 'in', ('waiting_for_approve', 'rfq_approved')), ('date_order', '>=', fields.Datetime.now()), ('user_id', '=', self.env.uid), ('is_goods_orders', '=', True)])
            res['all_late'] = po.search_count([('state', 'in', ['draft', 'sent', 'to approve']), ('date_order', '<', fields.Datetime.now()), ('is_goods_orders', '=', True)])
            res['my_late'] = po.search_count([('state', 'in', ['draft', 'sent', 'to approve']), ('date_order', '<', fields.Datetime.now()), ('user_id', '=', self.env.uid), ('is_goods_orders', '=', True)])
            self._cr.execute(query, (one_week_ago, True, self.env.company.id))
        elif context.get('services_good'):
            res['all_to_send'] = po.search_count([('state', '=', 'draft'), ('is_goods_orders', '=', False)])
            res['my_to_send'] = po.search_count([('state', '=', 'draft'), ('user_id', '=', self.env.uid), ('is_goods_orders', '=', False)])
            res['all_waiting'] = po.search_count([('state', 'in', ('waiting_for_approve', 'rfq_approved')), ('date_order', '>=', fields.Datetime.now()), ('is_goods_orders', '=', False)])
            res['my_waiting'] = po.search_count([('state', 'in', ('waiting_for_approve', 'rfq_approved')), ('date_order', '>=', fields.Datetime.now()), ('user_id', '=', self.env.uid), ('is_goods_orders', '=', False)])
            res['all_late'] = po.search_count([('state', 'in', ['draft', 'sent', 'to approve']), ('date_order', '<', fields.Datetime.now()), ('is_goods_orders', '=', False)])
            res['my_late'] = po.search_count([('state', 'in', ['draft', 'sent', 'to approve']), ('date_order', '<', fields.Datetime.now()), ('user_id', '=', self.env.uid), ('is_goods_orders', '=', False)])
        else:
            res['all_waiting'] = po.search_count([('state', 'in', ('waiting_for_approve', 'rfq_approved')), ('date_order', '>=', fields.Datetime.now())])
            res['my_waiting'] = po.search_count([('state', 'in', ('waiting_for_approve', 'rfq_approved')), ('date_order', '>=', fields.Datetime.now()), ('user_id', '=', self.env.uid)])
        if not context.get('goods_order'):
            self._cr.execute(query, (one_week_ago, False, self.env.company.id))
        values = self.env.cr.fetchone()
        res['all_avg_order_value'] = round(values[0] or 0, values[3])
        res['all_avg_days_to_purchase'] = round(values[1] or 0, 2)
        res['all_total_last_7_days'] = round(values[2] or 0, values[3])
        order_value = res['all_avg_order_value']
        res['all_avg_order_value'] = f'{order_value:,}'
        last_7_days = res['all_total_last_7_days']
        res['all_total_last_7_days'] = f'{last_7_days:,}'
        return res

    @api.depends('name')
    def compute_retail(self):
        for res in self:
            if self.env['ir.config_parameter'].sudo().get_param('retail') == 'True':
                if not self.dp:
                    res.readonly_price = True
                else:
                    res.readonly_price = False
            else:
                res.readonly_price = False

    def action_quotation_send_wp(self):
        res = super(PurchaseOrder, self).action_quotation_send_wp()
        res['context'].update({'hide_send_button' : True})
        return res

    def sh_quotation_revision(self, default=None):
        if self:
            self.ensure_one()
            self.is_revision_created = True
            if default is None:
                default = {}
            if self.is_revision_po:
                po_count = self.search([("revision_order_id", '=', self.revision_order_id.id), ('is_revision_po', '=', True)])
                split_name = self.name.split('/')
                if split_name[-1].startswith('R'):
                    split_name[-1] = 'R%d' % (len(po_count) + 1)
                else:
                    split_name.append('R%d' % (len(po_count) + 1))
                name = '/'.join(split_name)
            else:
                po_count = self.search([("revision_order_id", '=', self.id), ('is_revision_po', '=', True)])
                name = _('%s/R%d') % (self.name, len(po_count) + 1)
            if 'name' not in default:
                default['state'] = 'draft'
                default['origin'] = self.name
                default['sh_purchase_order_id'] = self.id
                default['is_revision_po'] = True
                default['po'] = False
                default['order_line'] = False
                if self.is_revision_po:
                    default['revision_order_id'] = self.revision_order_id.id
                else:
                    default['revision_order_id'] = self.id
                default['is_revision_created'] = False
                self.sh_po_number += 1
            default['is_goods_orders'] = self.is_goods_orders
            new_purchase_id = self.copy(default=default)
            for line in self.order_line:
                line.copy({
                    'order_id': new_purchase_id.id,
                    'destination_warehouse_id': line.destination_warehouse_id.id
                })
            if name.startswith('RFQ'):
                new_purchase_id.name = name
            if self.is_revision_po:
                new_purchase_id.sh_revision_po_id = [(6, 0, self.revision_order_id.ids + po_count.ids)]
            else:
                new_purchase_id.sh_revision_po_id = [(6, 0, self.ids)]
        return self.open_quality_check()

    @api.model
    def action_purchase_order_menu(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        is_good_services_order = IrConfigParam.get_param('is_good_services_order', False)
        if is_good_services_order:
            self.env.ref("equip3_purchase_operation.menu_purchase_services_goods").active = True
            self.env.ref("purchase.menu_procurement_management").active = True
            self.env.ref("equip3_purchase_operation.menu_orders").active = False
        else:
            self.env.ref("equip3_purchase_operation.menu_purchase_services_goods").active = False
            self.env.ref("purchase.menu_procurement_management").active = False
            self.env.ref("equip3_purchase_operation.menu_orders").active = True

    @api.depends('company_id')
    def get_analytic_accounting(self):
        for res in self:
            res.analytic_accounting = self.user_has_groups('analytic.group_analytic_accounting')
    
    @api.onchange('destination_warehouse_id')
    def _onchange_destination_warehouse(self):
        for res in self:
            for line in res.order_line:
                if res.is_single_delivery_destination:
                    line.destination_warehouse_id = res.destination_warehouse_id.id

    @api.onchange('is_single_delivery_destination')
    def _onchange_is_single_delivery_destination(self):
        for record in self:
            if record.is_single_delivery_destination:
                stock_warehouse = record.env['stock.warehouse'].search([], order="id", limit=1)
                record.destination_warehouse_id = stock_warehouse[0]
            if record.is_single_delivery_destination and record.destination_warehouse_id:
                for line in record.order_line:
                    line.picking_type_id = record.destination_warehouse_id.in_type_id.id

    @api.depends('order_line', 'order_line.date_planned')
    def _compute_date_planned(self):
        """ date_planned = the earliest date_planned across all order lines. """
        for order in self:
            dates_list = order.order_line.filtered(lambda x: not x.display_type and x.date_planned).mapped('date_planned')
            if dates_list:
                order.date_planned = fields.Datetime.to_string(max(dates_list))
            else:
                order.date_planned = False

    def _inverse_date_planned(self):
        for record in self:
            record.date_planned = record.date_planned

    def _create_picking(self):
        StockPicking = self.env['stock.picking']
        for order in self:
            temp_data = []
            final_data = []
            for line in order.order_line:
                if {'date_planned': line.date_planned, 'warehouse_id': line.destination_warehouse_id.id} in temp_data:
                    filter_lines = list(filter(lambda r:r.get('date_planned') == line.date_planned and r.get('warehouse_id') == line.destination_warehouse_id.id, final_data))
                    if filter_lines:
                        filter_lines[0]['lines'].append(line)
                else:
                    temp_data.append({
                        'date_planned': line.date_planned,
                        'warehouse_id': line.destination_warehouse_id.id
                    })
                    final_data.append({
                        'date_planned': line.date_planned,
                        'warehouse_id': line.destination_warehouse_id.id,
                        'lines': [line]
                    })
            for line_data in final_data:
                if any(product.type in ['product', 'consu'] for product in order.order_line.product_id):
                    order = order.with_company(order.company_id)
                    pickings = order.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
                    res = order._prepare_picking()
                    warehouse_id = self.env['stock.warehouse'].browse([line_data.get('warehouse_id')])
                    picking_type_id = self.env['stock.picking.type'].search([('warehouse_id', '=', warehouse_id.id), ('code', '=', 'incoming')], limit=1)
                    if picking_type_id:
                        res.update({
                            'picking_type_id': picking_type_id.id,
                            'location_dest_id': picking_type_id.default_location_dest_id.id,
                            'date': line_data.get('date_planned'),
                        })
                    picking = StockPicking.with_user(SUPERUSER_ID).create(res)
                    lines = self.env['purchase.order.line']
                    for new_line in line_data.get('lines'):
                        lines += new_line
                    moves = lines._create_stock_moves(picking)
                    moves = moves.filtered(lambda x: x.state not in ('done', 'cancel'))._action_confirm()
                    seq = 0
                    for move in sorted(moves, key=lambda move: move.date):
                        seq += 5
                        move.sequence = seq
                    moves._action_assign()
                    picking.message_post_with_view('mail.message_origin_link',
                        values={'self': picking, 'origin': order},
                        subtype_id=self.env.ref('mail.mt_note').id)
        return True

    @api.onchange('date_planned', 'is_delivery_receipt')
    def onchange_date_planned(self):
        if self.date_planned and self.is_delivery_receipt:
            self.order_line.date_planned = self.date_planned

    def action_apply(self):
        for record in self:
            record.order_line.write({'destination_warehouse_id' : record.destination_warehouse_id})

    def _compute_approval_matrix(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        approval =  IrConfigParam.get_param('is_purchase_order_approval_matrix')
        for record in self:
            record.is_approval_matrix = approval

    def _get_approve_button(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False


    @api.depends('approval_matrix_id')
    def _compute_approving_matrix_lines(self):
        data = [(5, 0, 0)]
        for record in self:
            counter = 1
            record.approved_matrix_ids = []
            for line in record.approval_matrix_id.approval_matrix_purchase_order_line_ids:
                data.append((0, 0, {
                    'sequence' : counter,
                    'user_ids' : [(6, 0, line.user_ids.ids)],
                    'minimum_approver' : line.minimum_approver,
                }))
                counter += 1
            record.approved_matrix_ids = data

    @api.onchange('name')
    def onchange_purchase_name(self):
        self._compute_approval_matrix()
        self._compute_approval_matrix_id()

    @api.onchange('name')
    def onchange_checklist_template(self):
        if self.env['ir.config_parameter'].sudo().get_param('is_good_services_order'):
            if self.is_goods_orders:
                # ids = self.env['purchase.custom.checklist.template'].search([('order', '=', 'goods')]).ids
                b = {'domain': {'custom_checklist_template_ids': [('order', '=', 'goods')]}}
            else:
                # ids = self.env['purchase.custom.checklist.template'].search([('order', '=', 'services')]).ids
                b = {'domain': {'custom_checklist_template_ids': [('order', '=', 'services')]}}
        else:
            b = {'domain': {'custom_checklist_template_ids': []}}
        return b

    @api.onchange('name')
    def onchange_partner(self):
        b = {}
        if self.env['ir.config_parameter'].sudo().get_param('is_vendor_approval_matrix'):
            b = {'domain': {'partner_id': [('state', '=', 'approved')]}}
        return b

    
    @api.depends('amount_untaxed', 'branch_id')
    def _compute_approval_matrix_id(self):
        for record in self:
            record.approval_matrix_id = False
            if record.is_approval_matrix:
                approval_matrix_id = self.env['approval.matrix.purchase.order'].search([
                            ('minimum_amt', '<=', record.amount_untaxed), 
                            ('maximum_amt', '>=', record.amount_untaxed),
                            ('branch_id', '=', record.branch_id.id),
                            ('company_id', '=', record.company_id.id)], limit=1)
                record.approval_matrix_id = approval_matrix_id and approval_matrix_id.id or False


    def action_request_approval(self):
        for record in self:
            approver = False
            for line in record.order_line:
                if line.price_unit <= 0.0:
                    raise UserError(_('You cannot confirm the purchase order without price.'))
            data = []
            template_id = self.env.ref('equip3_purchase_operation.email_template_purchase_order')
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = base_url + '/web#id=' + str(record.id) + '&view_type=form&model=purchase.order'
            if record.approved_matrix_ids and len(record.approved_matrix_ids[0].user_ids) > 1:
                for approved_matrix_id in record.approved_matrix_ids[0].user_ids:
                    approver = approved_matrix_id
                    ctx = {
                        'email_from' : self.env.user.company_id.email,
                        'email_to' : approver.partner_id.email,
                        'approver_name' : approver.name,
                        'requested_by' : self.env.user.name,
                        'product_lines' : data,
                        'url' : url,
                    }
                    template_id.with_context(ctx).send_mail(record.id, True)
            else:
                approver = record.approved_matrix_ids[0].user_ids[0]
                ctx = {
                    'email_from': self.env.user.company_id.email,
                    'email_to': approver.partner_id.email,
                    'approver_name': approver.name,
                    'requested_by': self.env.user.name,
                    'product_lines': data,
                    'url': url,
                }
                template_id.with_context(ctx).send_mail(record.id, True)
            record.write({'state' : 'waiting_for_approve'})
        

    def action_approve(self):
        for record in self:
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = base_url + '/web#id='+ str(record.id) + '&view_type=form&model=purchase.order'
            data = []
            template_id = self.env.ref('equip3_purchase_operation.email_template_purchase_order_approve')
            user = self.env.user
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_ids.ids and \
                    user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    if name != '':
                        name += "\n • %s: Approved" % (self.env.user.name)
                    else:
                        name += "• %s: Approved" % (self.env.user.name)

                    approval_matrix_line_id.write({ 
                        'last_approved': self.env.user.id, 'state_char': name, 
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
                        next_approval_matrix_line_id = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
                        if next_approval_matrix_line_id and len(next_approval_matrix_line_id[0].user_ids) > 1:
                            for approving_matrix_line_user in next_approval_matrix_line_id[0].user_ids:
                                ctx = {
                                    'email_from': self.env.user.company_id.email,
                                    'email_to': approving_matrix_line_user.partner_id.email,
                                    'user_name': approving_matrix_line_user.name,
                                    'approver_name': ','.join(approval_matrix_line_id.user_ids.mapped('name')),
                                    'url': url,
                                    'product_lines': data,
                                }
                                template_id.sudo().with_context(ctx).send_mail(record.id, True)
                        else:
                            if next_approval_matrix_line_id and next_approval_matrix_line_id[0].user_ids:
                                ctx = {
                                    'email_from': self.env.user.company_id.email,
                                    'email_to': next_approval_matrix_line_id[0].user_ids[0].partner_id.email,
                                    'user_name': next_approval_matrix_line_id[0].user_ids[0].name,
                                    'approver_name': ','.join(approval_matrix_line_id.user_ids.mapped('name')),
                                    'url': url,
                                    'product_lines': data,
                                }
                                template_id.sudo().with_context(ctx).send_mail(record.id, True)
            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r:r.approved)):
                record.write({'state': 'rfq_approved'})

    def action_reject(self):
        return {
                'type': 'ir.actions.act_window',
                'name': 'Rejected Reason',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'approval.reject',
                'target': 'new',
            }

    def _compute_hold_purchase_order(self):
        for record in self:
            if self.env.user.has_group('equip3_purchase_operation.group_purchase_order_partner_credit_limit'):
                record.is_hold_purchase_order = True
            else:
                record.is_hold_purchase_order = False

    @api.onchange('partner_id', 'company_id')
    def onchange_partner_id(self):
        res = super(PurchaseOrder, self).onchange_partner_id()
        self._compute_hold_purchase_order()
        return res

    def button_onhold_confirm(self):
        self.state = 'sent'
        self.with_context(order_confirm=True).button_confirm()

    def button_confirm(self):
        context = dict(self.env.context) or {}
        context.update({'active_id': self.id, 'active_ids': self.ids})
        for record in self:
            if record.is_revision_po and record.origin.startswith('PO'):
                po_count = self.search_count([("revision_order_id", '=', self.revision_order_id.id), ('is_revision_po', '=', True)])
                split_name = self.origin.split('/')
                if split_name[-1].startswith('R'):
                    split_name[-1] = 'R%d' % (po_count)
                    name = '/'.join(split_name)
                else:
                    name = _('%s/R%d') % (self.origin, (po_count))
                record.name = name
            if self.env.user.has_group('equip3_purchase_operation.group_purchase_order_partner_credit_limit') and \
                record.amount_total > record.partner_id.vendor_available_purchase_limit and \
                not context.get('order_confirm') and \
                record.partner_id.is_set_vendor_purchase_limit:
                 return {
                    'name': 'Vendor Purchase Limit',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'purchase.order.partner.credit',
                    'view_id': self.env.ref('equip3_purchase_operation.purchase_order_partner_credit_limit_form').id,
                    'type': 'ir.actions.act_window',
                    'target': 'new',
                    'context' : context
                }
            else:
                record.state = 'draft'
                if self.env.user.has_group('equip3_purchase_operation.group_purchase_order_partner_credit_limit'):
                    record.partner_id.vendor_available_purchase_limit -=  record.amount_total
        res = super(PurchaseOrder, self).button_confirm()
        return res

    @api.model
    def default_get(self, fields):
        res = super(PurchaseOrder, self).default_get(fields)
        context = dict(self.env.context) or {}
        if 'is_goods_orders' in res or 'default_is_goods_orders' in context:
            if self.env['ir.config_parameter'].sudo().get_param('is_good_services_order') == 'True':
                if 'is_goods_orders' in res or 'default_is_goods_orders' in context:
                    if res.get('is_goods_orders') or context.get('default_is_goods_orders'):
                        rfq_exp_date = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.rfq_exp_date_goods') or 0
                    else:
                        rfq_exp_date = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.rfq_exp_date_services') or 0
            else:
                rfq_exp_date = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.rfq_exp_date') or 0
            res.update({
                'date_order': datetime.now() + timedelta(days=int(rfq_exp_date))
            })
        analytic_priority_ids = self.env['analytic.priority'].search([], order="priority")
        for analytic_priority in analytic_priority_ids:
            if analytic_priority.object_id == 'user' and self.env.user.analytic_tag_ids:
                res.update({
                    'analytic_account_group_ids': [(6, 0, self.env.user.analytic_tag_ids.ids)]
                }) 
                break
            elif analytic_priority.object_id == 'branch' and self.env.user.branch_id.analytic_tag_ids:
                res.update({
                    'analytic_account_group_ids': [(6, 0, self.env.user.branch_id.analytic_tag_ids.ids)]
                })
                break
        return res

    def auto_cancel_rfq(self):
        rfq = self.env['purchase.order'].search([('state', 'in', ('draft','sent','to_approve'))])
        for res in rfq:
            if datetime.strftime(res.date_order, tools.DEFAULT_SERVER_DATE_FORMAT) == datetime.strftime(datetime.now(), tools.DEFAULT_SERVER_DATE_FORMAT):
                res.write({'state': 'cancel'})

    def auto_cancel_po(self):
        po = self.env['purchase.order'].search([('state', 'in', ('purchase', 'done')),('po', '=', True)])
        for res in po:
            picking = self.env['stock.picking'].search([('purchase_id', '=', res.id),('state', '=', 'done')])
            account = self.env['account.move'].search([('id', 'in', res.invoice_ids.ids),('state', '=', 'posted')])
            if not picking and not account and res.po_expiry_date and datetime.strftime(res.po_expiry_date, tools.DEFAULT_SERVER_DATE_FORMAT) == datetime.strftime(datetime.now(), tools.DEFAULT_SERVER_DATE_FORMAT):
                res.write({'state': 'cancel'})

    @api.model
    def create(self, vals):
        if not vals.get('is_revision_po') or (vals.get('is_revision_po') and vals.get('origin').startswith('PO')):
            if self.env['ir.config_parameter'].sudo().get_param('is_good_services_order') == 'True':
                if vals.get('is_goods_orders'):
                    vals['name'] = self.env['ir.sequence'].next_by_code('purchase.order.seqs.rfq.goods')
                else:
                    vals['name'] = self.env['ir.sequence'].next_by_code('purchase.order.seqs.rfq.services')
            else:
                vals['name'] = self.env['ir.sequence'].next_by_code('purchase.order.seqs.rfq')
        return super(PurchaseOrder, self).create(vals)
    
    def write(self, vals):
        if 'state' in vals:
            if vals['state'] not in ('draft', 'sent', 'to_approve'):
                if vals['state'] == 'purchase':
                    if not self.exp_po or (self.is_revision_po and self.origin.startswith('RFQ')):
                        if self.env['ir.config_parameter'].sudo().get_param('is_good_services_order') == 'True':
                            if self.is_goods_orders:
                                vals['name'] = self.env['ir.sequence'].next_by_code('purchase.order.seqs.goods')
                            else:
                                vals['name'] = self.env['ir.sequence'].next_by_code('purchase.order.seqs.services')
                        else:
                            vals['name'] = self.env['ir.sequence'].next_by_code('purchase.order.seqs')
                        vals['exp_po'] = True
                    if self.env['ir.config_parameter'].sudo().get_param('is_good_services_order') == 'True':
                        if self.is_goods_orders:
                            po_exp_date = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.po_exp_date_goods') or 0
                        else:
                            po_exp_date = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.po_exp_date_services') or 0
                    else:
                        po_exp_date = self.env['ir.config_parameter'].get_param('equip3_purchase_operation.po_exp_date') or 0
                    vals['po_expiry_date'] = datetime.now() + timedelta(days=int(po_exp_date))
                    vals['po'] = True
            else:
                if vals['state'] == 'draft':
                    vals['po'] = False
        return super(PurchaseOrder, self).write(vals)

    def _reset_sequence(self):
        for rec in self:
            current_sequence = 1
            for line in rec.order_line:
                line.sequence = current_sequence
                current_sequence += 1

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    @api.constrains('product_qty')
    def _check_min_qty(self):
        for record in self:
            if record.product_qty < 1:
                raise ValidationError("Minimal Qty should be greater then zero.")

    @api.model
    def _default_domain(self):
        context = dict(self.env.context) or {}
        if context.get('goods_order'):
            return [('type', 'in', ('consu','product'))]
        elif context.get('services_good'):
            return [('type', '=', 'service')]

    @api.depends('company_id', 'destination_warehouse_id', 'product_id')
    def compute_destination(self):
        for res in self:
            if res.order_id.is_single_delivery_destination:
                res.destination_warehouse_id = res.order_id.destination_warehouse_id
            else:
                if not res.destination_warehouse_id:
                    res.destination_warehouse_id = False

    @api.depends('company_id', 'date_planned', 'product_id')
    def compute_receipt(self):
        for res in self:
            if res.order_id.is_delivery_receipt:
                res.date_planned = res.order_id.date_planned
            else:
                if not res.date_planned:
                    res.date_planned = False

    last_purchased_price = fields.Float(string="Last Purchased Price",compute='_compute_calculate_last_price', store=True)
    last_customer_purchase_price = fields.Float(string="Last Purchase Price Of Vendor", compute='_compute_calculate_last_price_vendor', store=True)
    product_template_id = fields.Many2one(domain=_default_domain)
    is_goods_orders = fields.Boolean(string="Goods Orders", compute='_compute_is_good_orders', store=True)
    destination_location_id = fields.Many2one('stock.location', string='Destination Location')
    destination_warehouse_id = fields.Many2one('stock.warehouse', string='Destination', compute="compute_destination", store=True)
    date_planned = fields.Datetime(string='Delivery Date', index=True,
                                   help="Delivery date expected from vendor. This date respectively defaults to vendor pricelist lead time then today's date.", compute="compute_receipt", store=True)
    delivery_ref = fields.Char(string="Delivery Reference", compute="_compute_delivery_ref")
    vendor_bills_ref = fields.Char(string="Vendor Bills Reference", compute="_compute_vendor_bills_ref")
    request_line_id = fields.Many2one('purchase.request.line', string="Purchase Request Line")
    readonly_price = fields.Boolean("Readonly Price", related='order_id.readonly_price')
    sequence = fields.Char(string='Sequence')
    sequence2 = fields.Char(string='No')


    @api.model
    def default_get(self, fields):
        res = super(PurchaseOrderLine, self).default_get(fields)
        if self._context:
            context_keys = self._context.keys()
            next_sequence = 1
            if 'order_line' in context_keys:
                if len(self._context.get('order_line')) > 0:
                    next_sequence = len(self._context.get('order_line')) + 1
            res.update({'sequence2': next_sequence})
        return res
    
    def unlink(self):
        approval = self.order_id
        res = super(PurchaseOrderLine, self).unlink()
        approval._reset_sequence()
        return res

    @api.onchange('sequence')
    def set_sequence_line(self):
        for record in self:
            record.order_id._reset_sequence()

    @api.model
    def create(self, vals):
        res = super(PurchaseOrderLine, self).create(vals)
        res.order_id._reset_sequence()
        return res

    def _compute_vendor_bills_ref(self):
        for rec in self:
            rec.vendor_bills_ref = ''
            if rec.invoice_lines and rec.invoice_lines.filtered(lambda r:r.ref):
                name = ",".join(rec.invoice_lines.filtered(lambda r:r.ref).mapped('ref'))
                rec.vendor_bills_ref = name

    def _compute_delivery_ref(self):
        for rec in self:
            rec.delivery_ref = ""
            if rec.move_ids and rec.move_ids.filtered(lambda r:r.reference):
                name = ",".join(rec.move_ids.filtered(lambda r:r.reference).mapped('reference'))
                rec.delivery_ref = name

    @api.depends('order_id', 'order_id.is_goods_orders')
    def _compute_is_good_orders(self):
        for record in self:
            record.is_goods_orders = record.order_id.is_goods_orders

    @api.onchange('date_planned')
    def onchange_date_planned_line(self):
        if self.date_planned and not self.order_id.is_delivery_receipt:
            dates_list = self.order_id.order_line.filtered(lambda x: not x.display_type and x.date_planned).mapped('date_planned')
            if dates_list:
                self.order_id.date_planned = fields.Datetime.to_string(max(dates_list))
        elif self.date_planned and self.order_id.is_delivery_receipt:
            self.order_id.onchange_date_planned()

    def _prepare_stock_move_vals(self, picking, price_unit, product_uom_qty, product_uom):
        res = super(PurchaseOrderLine, self)._prepare_stock_move_vals(picking=picking, price_unit=price_unit, product_uom_qty=product_uom_qty, product_uom=product_uom)
        product = self.product_id.with_context(lang=self.order_id.dest_address_id.lang or self.env.user.lang)
        description_picking = product._get_description(picking.picking_type_id)
        res.update({
            'picking_type_id': picking.picking_type_id.id,
            'warehouse_id': self.destination_warehouse_id.id,
            'route_ids': picking.picking_type_id.warehouse_id and [(6, 0, [x.id for x in picking.picking_type_id.warehouse_id.route_ids])] or [],
            'location_dest_id': picking.picking_type_id.default_location_dest_id.id,
        })
        self.picking_type_id = picking.picking_type_id.id
        return res

    @api.depends('product_id')
    def _compute_calculate_last_price(self):
        for record in self:
            if isinstance(record.id, models.NewId):
                purchase_order_line_id = self.search([('state','in',('purchase','done')),('product_id', '=', record.product_id.id)], limit=1, order="id desc")
            else:
                purchase_order_line_id = self.search([('state','in',('purchase','done')),('product_id', '=', record.product_id.id), ('id', '!=', record.id)], limit=1, order="id desc")
            record.last_purchased_price = purchase_order_line_id.price_unit

    @api.depends('product_id', 'order_id', 'order_id.partner_id')
    def _compute_calculate_last_price_vendor(self):
        for record in self:
            if isinstance(record.id, models.NewId):
                purchase_order_line_id = self.search([('state','in',('purchase','done')),('product_id', '=', record.product_id.id), ('order_id.partner_id', '=', record.order_id.partner_id.id)], limit=1, order="id desc")
            else:
                purchase_order_line_id = self.search([('state','in',('purchase','done')),('product_id', '=', record.product_id.id), ('order_id.partner_id', '=', record.order_id.partner_id.id), ('id', '!=', record.id)], limit=1, order="id desc")
            record.last_customer_purchase_price = purchase_order_line_id.price_unit

    @api.onchange('product_qty', 'product_uom')
    def _onchange_quantity(self):
        if self.product_id and not self.product_uom:
            self.product_uom = self.product_id.uom_po_id.id
        res = super(PurchaseOrderLine, self)._onchange_quantity()
        if self.product_id:
            params = {'order_id': self.order_id}
            seller = self.product_id._select_seller(
                partner_id=self.partner_id,
                quantity=self.product_qty,
                date=self.order_id.date_order and self.order_id.date_order.date(),
                uom_id=self.product_uom,
                params=params)
            if seller and self.is_vendor_pricelist_line and seller.state1 != 'approved':
                self.price_unit = 0
        if self.request_line_id:
            IrConfigParam = self.env['ir.config_parameter'].sudo()
            pr_qty_limit =  IrConfigParam.get_param('pr_qty_limit', "no_limit")
            if pr_qty_limit == 'percent':
                max_percentage = IrConfigParam.get_param('max_percentage', 0)
                pr_line_qty = (self.request_line_id.product_qty * int(max_percentage)) / 100
                max_qty = pr_line_qty + self.request_line_id.product_qty
                if self.product_qty > max_qty:
                    warning_mess = {
                        'title': _('Quantity Warning!'),
                        'message' : _('Quantity Can Not Be Greater Then Purchase Request Quantity.'),
                    }
                    return {'warning': warning_mess, 'value': {'product_qty': self.request_line_id.product_qty}}
            elif pr_qty_limit == 'fix' and self.product_qty > self.request_line_id.product_qty:
                warning_mess = {
                    'title': _('Quantity Warning!'),
                    'message' : _('Quantity Can Not Be Greater Then Purchase Request Quantity.'),
                }
                return {'warning': warning_mess, 'value': {'product_qty': self.request_line_id.product_qty}}
        return res

class PurchaseRequestLine(models.Model):
    _inherit ='purchase.request.line'


    sequence = fields.Char(string='Sequence')
    sequence2 = fields.Char(string='No')
    remaning_qty = fields.Float(compute="_compute_remaning_qty", store=True, string="Remaining Qty")
    purchase_req_budget = fields.Float(string='Purchase Request Budget', readonly=True)
    rem_budget = fields.Float(string='Remaining Budget', readonly=True)

    @api.depends('product_qty', 'purchased_qty')
    def _compute_remaning_qty(self):
        for record in self:
            record.remaning_qty = record.product_qty - record.purchased_qty

    @api.model
    def default_get(self, fields):
        res = super(PurchaseRequestLine, self).default_get(fields)
        if self._context:
            context_keys = self._context.keys()
            next_sequence = 1
            if 'line_ids' in context_keys:
                if len(self._context.get('line_ids')) > 0:
                    next_sequence = len(self._context.get('line_ids')) + 1
            res.update({'sequence2': next_sequence})
        return res
    
    def unlink(self):
        approval = self.request_id
        res = super(PurchaseRequestLine, self).unlink()
        approval._reset_sequence()
        return res

    @api.onchange('sequence')
    def set_sequence_line(self):
        for record in self:
            record.request_id._reset_sequence()

    @api.model
    def create(self, vals):
        res = super(PurchaseRequestLine, self).create(vals)
        res.request_id._reset_sequence()
        return res

    @api.model
    def _default_domain(self):
        context = dict(self.env.context) or {}
        setting = self.env['ir.config_parameter'].sudo().get_param('is_good_services_order')
        if setting:
            if context.get('goods_order'):
                return [('type', 'in', ('consu','product'))]
            elif context.get('services_good'):
                return [('type', '=', 'service')]
        else:
            return []

    @api.depends('company_id', 'dest_loc_id', 'product_id')
    def compute_destination(self):
        for res in self:
            if res.request_id.is_single_delivery_destination:
                res.dest_loc_id = res.request_id.destination_warehouse
            else:
                if not res.dest_loc_id:
                    res.dest_loc_id = False

    @api.depends('company_id', 'date_required', 'product_id')
    def compute_receipt(self):
        for res in self:
            if res.request_id.is_single_request_date:
                res.date_required = res.request_id.request_date
            else:
                if not res.date_required:
                    res.date_required = False

    product_id = fields.Many2one(domain=_default_domain, required=True)
    is_goods_orders = fields.Boolean(string="Goods Orders", default=False, related='request_id.is_goods_orders', store=True)
    price_total = fields.Float(string="Price Total", compute="_compute_price_total")
    dest_loc_id = fields.Many2one('stock.warehouse', string="Destination", compute="compute_destination", store=True)
    picking_type_dest = fields.Many2one('stock.picking.type', string="Picking Type")
    current_qty = fields.Float(string="Current Qty in Warehouse", compute="_compute_current_qty", store=True)
    analytic_account_group_ids = fields.Many2many('account.analytic.tag', string="Analytic Account Group")
    purchase_order = fields.Char(string="Purchase Order", compute="_compute_purchase_order", store=False)
    date_required = fields.Date(
        string="Expected Date",
        required=True,
        track_visibility="onchange",
        default=fields.Date.context_today,
        compute="compute_receipt",
        store=True
    )
    product_uom_id = fields.Many2one(
        domain="[('category_id', '=', product_uom_category_id)]"
    )
    product_uom_category_id = fields.Many2one(related='product_id.uom_id.category_id')

    def _compute_purchase_order(self):
        for record in self:
            name = ",".join(record.purchase_lines.mapped('order_id.name'))
            record.purchase_order = name

    @api.onchange('dest_loc_id')
    def _compute_picking_type(self):
        for res in self:
            if res.dest_loc_id:
                if res.dest_loc_id.pick_type_id:
                    res.picking_type_dest = res.dest_loc_id.pick_type_id
                else:
                    raise ValidationError("Picking type for destination location does not exist.")

    @api.depends('product_id', 'dest_loc_id')
    def _compute_current_qty(self):
        for record in self:
            location_ids = []
            record.current_qty = 0
            if record.product_id and record.dest_loc_id:
                location_obj = self.env['stock.location']
                store_location_id = record.dest_loc_id.view_location_id.id
                addtional_ids = location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')])
                for location in addtional_ids:
                    if location.location_id.id not in addtional_ids.ids:
                        location_ids.append(location.id)
                child_location_ids = self.env['stock.location'].search([('id', 'child_of', location_ids), ('id', 'not in', location_ids)]).ids
                final_location = child_location_ids + location_ids
                stock_quant_ids = self.env['stock.quant'].search([('location_id', 'in', final_location), ('product_id', '=', record.product_id.id)])
                record.current_qty = sum(stock_quant_ids.mapped('quantity'))

    @api.depends('product_qty', 'estimated_cost')
    def _compute_price_total(self):
        for record in self:
            amount = record.product_qty * record.estimated_cost
            record.price_total = amount

    @api.onchange("product_id")
    def onchange_product_id(self):
        res = super(PurchaseRequestLine,self).onchange_product_id()
        if self.product_id:
            self.product_uom_id = self.product_id.uom_po_id.id 
        return res

    @api.model
    def _calc_new_qty(self, request_line, po_line=None, new_pr_line=False):
        context = dict(self.env.context) or {}
        if po_line is not None and po_line.purchase_request_lines:
            return request_line.product_qty
        else:
            return super(PurchaseRequestLine,self)._calc_new_qty(request_line=request_line, po_line=po_line, new_pr_line=new_pr_line)


class PurchaseRequestLineMakePurchaseOrder(models.TransientModel):
    _inherit = "purchase.request.line.make.purchase.order"

    @api.onchange('supplier_id')
    def onchange_partner(self):
        b = {}
        if self.env['ir.config_parameter'].sudo().get_param('is_vendor_approval_matrix'):
            b = {'domain': {'supplier_id': [('state', '=', 'approved'), ("is_company", "=", True)]}}
        return b

    @api.model
    def _prepare_purchase_order_line(self, po, item):
        res = super(PurchaseRequestLineMakePurchaseOrder, self)._prepare_purchase_order_line(po, item)
        context = dict(self.env.context) or {}
        if context.get('goods_order'):
            res['is_goods_orders'] = True
        res['destination_warehouse_id'] = item.line_id.dest_loc_id.id
        res['picking_type_id'] = item.line_id.picking_type_dest.id
        res['request_line_id'] = item.line_id.id
        res['analytic_tag_ids'] = [(6, 0, item.line_id.analytic_account_group_ids.ids)]
        return res

    @api.model
    def _prepare_purchase_order(self, picking_type, group_id, company, origin):
        res = super(PurchaseRequestLineMakePurchaseOrder, self)._prepare_purchase_order(picking_type, group_id, company, origin)
        context = dict(self.env.context) or {}
        active_model = context.get('active_model')
        if active_model == 'purchase.request':
            request_id = self.env[active_model].browse(context.get('active_ids'))
            res['origin'] = request_id.name
        elif active_model == 'purchase.request.line':
            request_line_ids = self.env[active_model].browse(context.get('active_ids'))
            res['origin'] = ','.join(request_line_ids.mapped('request_id.name'))
        if context.get('goods_order'):
            res['is_goods_orders'] = True
        branch_id = False
        if request_id.branch_id:
            branch_id = request_id.branch_id.id
        res['branch_id'] = branch_id
        return res

    @api.model
    def _get_order_line_search_domain(self, order, item):
        res = super(PurchaseRequestLineMakePurchaseOrder, self)._get_order_line_search_domain(order, item)
        res.append(
            ("destination_warehouse_id", "=", item.line_id.dest_loc_id.id or False)
        )
        return res

class PurchaseRequestLineMakePurchaseOrderItem(models.TransientModel):
    _inherit = "purchase.request.line.make.purchase.order.item"

    destination_location_id = fields.Many2one('stock.location', string='Destination Location')
    rem_qty = fields.Float(compute='_compute_rem_qty', string='Remaining Qty', default=0.0)
    
    @api.depends('rem_qty')
    def _compute_rem_qty(self):
        for record in self:
            record.rem_qty = record.line_id.remaning_qty

class PurchaseAgreement(models.Model):
    _inherit = "purchase.agreement"

    def action_new_quotation(self):
        res = super(PurchaseAgreement, self).action_new_quotation()
        res['context'].update({'default_origin': self.name})
        return res


class SplitRFQWizard(models.TransientModel):
    _inherit = 'sh.split.rfq.wizard'

    @api.onchange('split_by')
    def onchange_split_by(self):
        context = dict(self.env.context) or {}
        active_model = context.get('active_model')
        if active_model == 'purchase.order':
            purchase_id = self.env[active_model].browse(context.get('active_ids'))
            return {'domain': {'purchase_order_id': [('id', '!=', purchase_id.id)]}}
class InheritPurchaseRequestLineMakePuchaseOrder(models.TransientModel):
    _inherit = 'purchase.request.line.make.purchase.order'

    @api.model
    def _prepare_item(self, line):
        return {
            "line_id": line.id,
            "request_id": line.request_id.id,
            "product_id": line.product_id.id,
            "name": line.name or line.product_id.name,
            "product_qty": line.remaning_qty,
            "product_uom_id": line.product_uom_id.id,
        }
    
    def mod_make_purchase_order(self):
        res = []
        purchase_obj = self.env["purchase.order"]
        po_line_obj = self.env["purchase.order.line"]
        pr_line_obj = self.env["purchase.request.line"]
        purchase = False

        for item in self.item_ids:
            line = item.line_id
            if item.product_qty <= 0.0:
                raise UserError(_("Enter a positive quantity."))
            if self.purchase_order_id:
                purchase = self.purchase_order_id
            if not purchase:
                po_data = self._prepare_purchase_order(
                    line.request_id.picking_type_id,
                    line.request_id.group_id,
                    line.company_id,
                    line.origin,
                )
                purchase = purchase_obj.create(po_data)

            # Look for any other PO line in the selected PO with same
            # product and UoM to sum quantities instead of creating a new
            # po line
            domain = self._get_order_line_search_domain(purchase, item)
            available_po_lines = po_line_obj.search(domain)
            new_pr_line = True
            # If Unit of Measure is not set, update from wizard.
            if not line.product_uom_id:
                line.product_uom_id = item.product_uom_id
            # Allocation UoM has to be the same as PR line UoM
            alloc_uom = line.product_uom_id
            wizard_uom = item.product_uom_id
            if available_po_lines and not item.keep_description:
                new_pr_line = False
                po_line = available_po_lines[0]
                po_line.purchase_request_lines = [(4, line.id)]
                po_line.move_dest_ids |= line.move_dest_ids
                po_line_product_uom_qty = po_line.product_uom._compute_quantity(
                    po_line.product_uom_qty, alloc_uom
                )
                wizard_product_uom_qty = wizard_uom._compute_quantity(
                    item.product_qty, alloc_uom
                )
                all_qty = min(po_line_product_uom_qty, wizard_product_uom_qty)
                self.create_allocation(po_line, line, all_qty, alloc_uom)
            else:
                po_line_data = self._prepare_purchase_order_line(purchase, item)
                if item.keep_description:
                    po_line_data["name"] = item.name
                po_line = po_line_obj.create(po_line_data)
                po_line_product_uom_qty = po_line.product_uom._compute_quantity(
                    po_line.product_uom_qty, alloc_uom
                )
                wizard_product_uom_qty = wizard_uom._compute_quantity(
                    item.product_qty, alloc_uom
                )
                all_qty = min(po_line_product_uom_qty, wizard_product_uom_qty)
                self.create_allocation(po_line, line, all_qty, alloc_uom)
            # TODO: Check propagate_uom compatibility not picked:
            new_qty = pr_line_obj._calc_new_qty(
                line, po_line=po_line, new_pr_line=new_pr_line
            )
            po_line.product_qty = all_qty
            po_line._onchange_quantity()
            # The onchange quantity is altering the scheduled date of the PO
            # lines. We do not want that:
            date_required = item.line_id.date_required
            po_line.date_planned = datetime(
                date_required.year, date_required.month, date_required.day
            )
            res.append(purchase.id)
            
            computerem = item.rem_qty - item.product_qty
            line.write({'remaning_qty': computerem})  

        return {
            "domain": [("id", "in", res)],
            "name": _("RFQ"),
            "view_mode": "tree,form",
            "res_model": "purchase.order",
            "view_id": False,
            "context": False,
            "type": "ir.actions.act_window",
        } 
