# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models, SUPERUSER_ID, tools, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime, timedelta
from odoo.http import request


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    # def write(self, vals):
    #     res = super(PurchaseOrder, self).write(vals)
    #     for line in self.order_line:
    #         if self.analytic_account_group_ids != line.analytic_tag_ids:
    #             line.update({
    #                 'analytic_tag_ids': [(6, 0, self.analytic_account_group_ids.ids)],
    #             })
    #     return res

    term_condition = fields.Many2one('term.condition', string="Terms and Conditions: ")
    term_condition_box = fields.Html("Terms and Conditions")
    bo_count = fields.Integer(
        string="Blanket Order",
        compute="_compute_purchase_bo_count",
        readonly=True,
    )
    multilevel_disc = fields.Boolean(string="Multi Level Discount", compute="_compute_multilevel_disc")
    multi_discount = fields.Char('Multi Discount')

    def get_disocunt(self,percentage,amount):
        new_amount = (percentage * amount)/100
        return (amount - new_amount)

    @api.onchange('multi_discount')
    def _onchange_multi_discount(self):
        if self.multi_discount:
            amount = 100
            splited_discounts = self.multi_discount.split("+")
            for disocunt in splited_discounts:
                try:
                    amount = self.get_disocunt(float(disocunt),amount)
                except ValueError:
                    raise ValidationError("Please Enter Valid Multi Discount")
            self.discount_amount = 100 - amount
        else:
            self.discount_amount = 0

    @api.onchange('discount_type', 'discount_amount', 'multi_discount', 'discount_method')
    def _set_discount_line(self):
        for res in self:
            if res.discount_amount:
                for line in res.order_line:
                    line.discount_amount = res.discount_amount
                    line.discount_method = res.discount_method
                    if res.multilevel_disc:
                        line.multi_discount = res.multi_discount

    @api.depends("state")
    def _compute_multilevel_disc(self):
        for res in self:
            res.multilevel_disc = self.env['ir.config_parameter'].sudo().get_param('multilevel_disc')


    @api.depends("order_line")
    def _compute_purchase_bo_count(self):
        for record in self:
            purchase_bo = self.env['purchase.requisition'].search_count([('purchase_id', '=', record.id)])
            record.bo_count = purchase_bo

    def action_view_purchase_bo(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Blanket Order',
            'view_mode': 'tree,form',
            'res_model': 'purchase.requisition',
            'domain' : [('purchase_id','=',self.id)],
            'target': 'current'
        }

    # @api.depends('term_condition')
    # def _compute_term_condition_box(self):
    #     for res in self:
    #         if res.term_condition:
    #             res.term_condition_box = res.term_condition.term_condition
    #             res.notes = res.term_condition.term_condition
    #         else:
    #             res.term_condition_box = False
    #             res.notes = False

    @api.onchange('term_condition')
    def _set_term_condition_box(self):
        for res in self:
            if res.term_condition:
                res.term_condition_box = res.term_condition.term_condition
                res.notes = res.term_condition.term_condition
            else:
                res.term_condition_box = False
                res.notes = False

    @api.onchange('term_condition_box')
    def _set_notes(self):
        for res in self:
            res.notes = res.term_condition_box


    @api.depends("order_line.qty_received")
    def _compute_shipment(self):
        res = super(PurchaseOrder, self)._compute_shipment()
        context = dict(self.env.context) or {}
        for record in self:
            record.sh_hidden_compute_field = False
        return res


    @api.onchange('analytic_account_group_ids', 'is_single_delivery_destination', 'date_planned', 'is_delivery_receipt', 'discount_type')
    def set_analytic_group(self):
        for res in self:
            for line in res.order_line:
                line.update({
                    'analytic_tag_ids': [(6, 0, res.analytic_account_group_ids.ids)],
                })

    @api.onchange('discount_type', 'discount_method', 'discount_amount')
    def set_disc(self):
        for res in self:
            if res.discount_type == 'global':
                for line in res.order_line:
                    line.update({
                        'discount_method': res.discount_method,
                        'discount_amount': res.discount_amount
                    })

    def print_quotation(self):
        return self.env.ref('general_template.report_purchase_exclusive').report_action(self)
    
    @api.model
    def _prepare_picking(self):
        res = super(PurchaseOrder, self)._prepare_picking()
        res.update({
                'analytic_account_group_ids': [(6, 0, self.analytic_account_group_ids.ids)],
            })
        return res

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'


    state_delivery = fields.Selection([
        ('nothing', 'Nothing'),
        ('draft', 'New'), ('cancel', 'Cancelled'),
        ('waiting', 'Waiting Another Move'),
        ('confirmed', 'Waiting Availability'),
        ('partially_available', 'Partially Available'),
        ('assigned', 'Available'),
        ('done', 'Done')], string='Status Delivery',
        copy=False, default='nothing', index=True, readonly=True,
        help="* New: When the stock move is created and not yet confirmed.\n"
             "* Waiting Another Move: This state can be seen when a move is waiting for another one, for example in a chained flow.\n"
             "* Waiting Availability: This state is reached when the procurement resolution is not straight forward. It may need the scheduler to run, a component to be manufactured...\n"
             "* Available: When products are reserved, it is set to \'Available\'.\n"
             "* Done: When the shipment is processed, the state is \'Done\'.")

    state_inv = fields.Selection(selection=[
        ('nothing', 'Nothing'),
        ('draft', 'Draft'),
        ('posted', 'Posted'),
        ('cancel', 'Cancelled'),
    ], string='Status Invoice', default='nothing')

    multi_discount = fields.Char('Multi Discount')

    def get_disocunt(self,percentage,amount):
        new_amount = (percentage * amount)/100
        return (amount - new_amount)

    @api.onchange('multi_discount')
    def _onchange_multi_discount(self):
        if self.multi_discount:
            amount = 100
            splited_discounts = self.multi_discount.split("+")
            for disocunt in splited_discounts:
                try:
                    amount = self.get_disocunt(float(disocunt),amount)
                except ValueError:
                    raise ValidationError("Please Enter Valid Multi Discount")
            self.discount_amount = 100 - amount
        else:
            self.discount_amount = 0

    @api.onchange('product_qty')
    def set_default_multi_disc(self):
        for res in self:
            if res.order_id.discount_type == 'global':
                res.discount_method = res.order_id.discount_method
                res.discount_amount = res.order_id.discount_amount
                if res.order_id.multilevel_disc:
                    res.multi_discount = res.order_id.multi_discount

    @api.constrains('qty_received')
    def set_state_delivery(self):
        for res in self:
            state = ''
            for move in res.move_ids:
                state = move.state
            if state:
                res.state_delivery = state

    @api.constrains('qty_invoiced')
    def set_state_inv(self):
        for res in self:
            state = ''
            # for inv in res.invoice_lines:
            #     state = inv.state
            if state:
                res.state_inv = state

    @api.onchange('destination_warehouse_id')
    def set_dest(self):
        for res in self:
            res.picking_type_id = False
            if res.order_id.is_single_delivery_destination:
                res.destination_warehouse_id = res.order_id.destination_warehouse_id
            else:
                if not res.destination_warehouse_id:
                    res.destination_warehouse_id = False
            if res.destination_warehouse_id:
                res.picking_type_id = res.destination_warehouse_id.in_type_id.id

    @api.onchange('date_planned')
    def set_date(self):
        for res in self:
            if res.order_id.is_delivery_receipt:
                res.date_planned = res.order_id.date_planned
            else:
                if not res.date_planned:
                    res.date_planned = False

class PurchaseRequisition(models.Model):
    _inherit = 'purchase.requisition'

    purchase_id = fields.Many2one('purchase.order', string="Purchase Order")

class PurchaseRequestLine(models.Model):
    _inherit ='purchase.request.line'

    @api.onchange('dest_loc_id')
    def set_destination(self):
        for res in self:
            if res.request_id.is_single_delivery_destination:
                res.dest_loc_id = res.request_id.destination_warehouse
            else:
                if not res.dest_loc_id:
                    res.dest_loc_id = False

    @api.onchange('date_required')
    def set_receipt(self):
        for res in self:
            if res.request_id.is_single_request_date:
                res.date_required = res.request_id.request_date
            else:
                if not res.date_required:
                    res.date_required = False