# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import models
from . import wizard

from odoo import api, SUPERUSER_ID

def assign_purchase_request_template(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    env['res.company']._assign_purchase_request_template()
