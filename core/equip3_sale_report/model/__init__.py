
from . import rfm_segment
from . import sale_report_margin
from . import sale_detail_report
from . import sale_report_by_saleperson
from . import sale_invoice_summary
from . import sale_invoice_payment
from . import customer_sales_analysis
from . import top_customer_wizard