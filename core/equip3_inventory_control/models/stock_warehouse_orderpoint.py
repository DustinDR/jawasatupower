
from odoo import SUPERUSER_ID, _, api, fields, models, registry
from collections import defaultdict
from odoo.exceptions import UserError , ValidationError
from datetime import datetime, date, timedelta
from odoo.tools import float_compare, frozendict, split_every
from dateutil import relativedelta
from itertools import groupby
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    name = fields.Char(required=True)

class StockWarehouseOrderpoint(models.Model):
    _inherit = "material.request"

    @api.onchange('branch_id')
    def _onchange_analytic_account_group_ids(self):
        if self.branch_id:
            self.analytic_account_group_ids = [(6, 0, self.branch_id.analytic_tag_ids.ids)]
        else:
            self.analytic_account_group_ids = [(6, 0, list())]


class StockWarehouseOrderpoint(models.Model):
    _name = "stock.warehouse.orderpoint"
    _inherit = ['portal.mixin', 'mail.thread', 'stock.warehouse.orderpoint', 'mail.activity.mixin', 'utm.mixin']

    location_id = fields.Many2one(required=False)
    name = fields.Char(default='New')
    source_location_id = fields.Many2one('stock.location', string="Source Location", tracking=True)
    action_to_take = fields.Selection([
                    ('no_action', 'No Action'),
                    ('create_pr', 'Create Purchase Request'),
                    ('create_rfq', 'Create Request For Quotation'),
                    ('create_itr', 'Create Internal Transfer Request'),
                    ('create_mr', 'Create Material Request'),
                    ('create_mo', 'Create Manufacturing Order'),
                    ('create_mp', 'Create Manufacturing Plan'),
    ], tracking=True, default='no_action')
    trigger = fields.Selection(default='manual', tracking=True)
    start_date = fields.Selection([
                    ('1', '1'),('2', '2'),('3', '3'),('4', '4'),('5', '5'),('6', '6'),('7', '7'),('8', '8'),('9', '9'),('10', '10'),
                    ('11', '11'),('12', '12'),('13', '13'),('14', '14'),('15', '15'),('16', '16'),('17', '17'),('18', '18'),('19', '19'),('20', '20'),
                    ('21', '21'),('22', '22'),('23', '23'),('24', '24'),('25', '25'),('26', '26'),('27', '27'),('28', '28'),('29', '29'),('30', '30'),
                    ('31','31')],string='Start Date', tracking=True)
    start_month = fields.Selection([
                    ('1','January'),('2','February'),('3','March'),('4','April'),('5','May'),('6','June'),
                    ('7','July'),('8','August'),('9','September'),('10','October'),('11','November'),('12','December')],string='Start Month', tracking=True)
    end_date = fields.Selection([
                    ('1', '1'),('2', '2'),('3', '3'),('4', '4'),('5', '5'),('6', '6'),('7', '7'),('8', '8'),('9', '9'),('10', '10'),
                    ('11', '11'),('12', '12'),('13', '13'),('14', '14'),('15', '15'),('16', '16'),('17', '17'),('18', '18'),('19', '19'),('20', '20'),
                    ('21', '21'),('22', '22'),('23', '23'),('24', '24'),('25', '25'),('26', '26'),('27', '27'),('28', '28'),('29', '29'),('30', '30'),
                    ('31','31')],string='End Date', tracking=True)
    end_month = fields.Selection([
                    ('1','January'),('2','February'),('3','March'),('4','April'),('5','May'),('6','June'),
                    ('7','July'),('8','August'),('9','September'),('10','October'),('11','November'),('12','December')],string='End Month', tracking=True)

    is_replenish_document_created = fields.Boolean(string="Replenish Document Created?", tracking=True)
    purchase_request_id = fields.Many2one('purchase.request', string="Purchase Request Reference", tracking=True)
    purchase_order_id = fields.Many2one('purchase.order', string="Request for Quotation Reference", tracking=True)
    material_request_id = fields.Many2one('material.request', string="Material Request Reference", tracking=True)
    internal_transfer_id = fields.Many2one('internal.transfer', string="Internal Transfer Request Reference", tracking=True)
    replenish_document_status = fields.Char(string="Replenish Document Status", tracking=True)
    periods = fields.Char(string="Active On Period", tracking=True)
    run_rate_days = fields.Integer(string="Run Rate Days", tracking=True)
    safety_stock = fields.Float(string="Safety Stock", tracking=True)
    safety_stock_select = fields.Selection([('fix_qty', 'Fixed Quantity'), ('percentage', 'Percentage')], default="fix_qty", tracking=True, string="Safety Stock")
    stock_days = fields.Float(string="Stock Days", tracking=True)
    responsible_id = fields.Many2many('res.users', 'res_user_order_point_rel', 'user_id', 'order_point_id', string="Responsible", tracking=True)
    branch_id = fields.Many2one('res.branch', string="Branch", related="warehouse_id.branch_id", tracking=True)

    average_quantity_last_year = fields.Float(string="Average Quantity Last Year", compute='_compute_average_quantity_last_year', store=False)
    lead_days_last_year = fields.Float(string="Lead Days Last Year", compute='_compute_average_quantity_last_year', store=False)
    average_quantity_run_rate_days = fields.Float(string="Average Quantity Run Rate Days", compute="_compute_average_quantity_run_rate_days", store=False)
    lead_days_run_rate_days = fields.Float(string="Lead Days Run Rate Days", compute="_compute_average_quantity_run_rate_days", store=False)
    run_rate_type = fields.Selection([
                                    ('get_last_year', 'Last Year Period'),
                                    ('get_past_days_data', 'Current Period'),
                                    ], default='get_last_year')

    company_id = fields.Many2one(default=lambda self:self.env.company.id, readonly=True)
    warehouse_id = fields.Many2one(domain="[('company_id', '=', company_id)]")
    filter_warehouse_id = fields.Many2many('stock.location', compute="_compute_warehouse", string="Warehouse")

    is_minimum_quantity = fields.Boolean(string="Minimum Quantity Based On Past Stock Demand")
    is_safety_stock_quantity = fields.Boolean(string="Safety Stock Quantity For Replenishment")
    select_period = fields.Char(string='Choose Period', default='active')
    notification_user_ids = fields.Many2many('res.users', string="Notification send to", related="warehouse_id.responsible_users")
    pr_status = fields.Selection(related="purchase_request_id.state", string="Purchase Request Status")
    rfq_status = fields.Selection(related="purchase_order_id.state", string="Request for Quotation Status")
    itr_status = fields.Selection(related="internal_transfer_id.state", string="Internal Transfer Request Status")
    mr_status = fields.Selection(related="material_request_id.status", string="Material Request Status")
    from_days_before = fields.Date(string="From Days Before")
    run_rate_qty = fields.Float(string="Run Rate Quantity")
    lead_days = fields.Float(string="Lead Days")

    period_date = fields.Selection([
                    ('1', '1'),('2', '2'),('3', '3'),('4', '4'),('5', '5'),('6', '6'),('7', '7'),('8', '8'),('9', '9'),('10', '10'),
                    ('11', '11'),('12', '12'),('13', '13'),('14', '14'),('15', '15'),('16', '16'),('17', '17'),('18', '18'),('19', '19'),('20', '20'),
                    ('21', '21'),('22', '22'),('23', '23'),('24', '24'),('25', '25'),('26', '26'),('27', '27'),('28', '28'),('29', '29'),('30', '30'),
                    ('31','31')],string='Period Date', tracking=True)
    period_month = fields.Selection([
                    ('1','January'),('2','February'),('3','March'),('4','April'),('5','May'),('6','June'),
                    ('7','July'),('8','August'),('9','September'),('10','October'),('11','November'),('12','December')],string='Period Month', tracking=True)
    run_rate_period_before = fields.Float(string="Run Rate Period Before", tracking=True)
    run_rate_period_after = fields.Float(string="Run Rate Period After", tracking=True)



    @api.onchange('warehouse_id')
    def _onchange_warehouse_id(self):
        self.location_id = False

    def _get_orderpoint_action(self):
        """Create manual orderpoints for missing product in each warehouses. It also removes
        orderpoints that have been replenish. In order to do it:
        - It uses the report.stock.quantity to find missing quantity per product/warehouse
        - It checks if orderpoint already exist to refill this location.
        - It checks if it exists other sources (e.g RFQ) tha refill the warehouse.
        - It creates the orderpoints for missing quantity that were not refill by an upper option.

        return replenish report ir.actions.act_window
        """
        action = self.env["ir.actions.actions"]._for_xml_id("stock.action_orderpoint_replenish")
        action['context'] = self.env.context
        # Search also with archived ones to avoid to trigger product_location_check SQL constraints later
        # It means that when there will be a archived orderpoint on a location + product, the replenishment
        # report won't take in account this location + product and it won't create any manual orderpoint
        # In master: the active field should be remove
        orderpoints = self.env['stock.warehouse.orderpoint'].with_context(active_test=False).search([])
        # Remove previous automatically created orderpoint that has been refilled.
        to_remove = orderpoints.filtered(lambda o: o.create_uid.id == SUPERUSER_ID and o.qty_to_order <= 0.0 and o.trigger == 'manual')
        to_remove.unlink()
        orderpoints = orderpoints - to_remove
        to_refill = defaultdict(float)
        all_product_ids = []
        all_warehouse_ids = []
        qty_by_product_warehouse = self.env['report.stock.quantity'].read_group(
            [('date', '=', fields.date.today()), ('state', '=', 'forecast')],
            ['product_id', 'product_qty', 'warehouse_id'],
            ['product_id', 'warehouse_id'], lazy=False)
        for group in qty_by_product_warehouse:
            warehouse_id = group.get('warehouse_id') and group['warehouse_id'][0]
            if group['product_qty'] >= 0.0 or not warehouse_id:
                continue
            all_product_ids.append(group['product_id'][0])
            all_warehouse_ids.append(warehouse_id)
            to_refill[(group['product_id'][0], warehouse_id)] = group['product_qty']
        if not to_refill:
            return action

        # Recompute the forecasted quantity for missing product today but at this time
        # with their real lead days.
        key_to_remove = []

        # group product by lead_days and warehouse in order to read virtual_available
        # in batch
        pwh_per_day = defaultdict(list)
        for (product, warehouse), quantity in to_refill.items():
            product = self.env['product.product'].browse(product).with_prefetch(all_product_ids)
            warehouse = self.env['stock.warehouse'].browse(warehouse).with_prefetch(all_warehouse_ids)
            rules = product._get_rules_from_location(warehouse.lot_stock_id)
            lead_days = rules.with_context(bypass_delay_description=True)._get_lead_days(product)[0]
            pwh_per_day[(lead_days, warehouse)].append(product.id)
        for (days, warehouse), p_ids in pwh_per_day.items():
            products = self.env['product.product'].browse(p_ids)
            qties = products.with_context(
                warehouse=warehouse.id,
                to_date=fields.datetime.now() + relativedelta.relativedelta(days=days)
            ).read(['virtual_available'])
            for qty in qties:
                if float_compare(qty['virtual_available'], 0, precision_rounding=product.uom_id.rounding) >= 0:
                    key_to_remove.append((qty['id'], warehouse.id))
                else:
                    to_refill[(qty['id'], warehouse.id)] = qty['virtual_available']

        for key in key_to_remove:
            del to_refill[key]
        if not to_refill:
            return action

        # Remove incoming quantity from other origin than moves (e.g RFQ)
        product_ids, warehouse_ids = zip(*to_refill)
        dummy, qty_by_product_wh = self.env['product.product'].browse(product_ids)._get_quantity_in_progress(warehouse_ids=warehouse_ids)
        rounding = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        # Group orderpoint by product-warehouse
        orderpoint_by_product_warehouse = self.env['stock.warehouse.orderpoint'].read_group(
            [('id', 'in', orderpoints.ids)],
            ['product_id', 'warehouse_id', 'qty_to_order:sum'],
            ['product_id', 'warehouse_id'], lazy=False)
        orderpoint_by_product_warehouse = {
            (record.get('product_id')[0], record.get('warehouse_id')[0]): record.get('qty_to_order')
            for record in orderpoint_by_product_warehouse
        }
        for (product, warehouse), product_qty in to_refill.items():
            qty_in_progress = qty_by_product_wh.get((product, warehouse)) or 0.0
            qty_in_progress += orderpoint_by_product_warehouse.get((product, warehouse), 0.0)
            # Add qty to order for other orderpoint under this warehouse.
            if not qty_in_progress:
                continue
            to_refill[(product, warehouse)] = product_qty + qty_in_progress
        to_refill = {k: v for k, v in to_refill.items() if float_compare(
            v, 0.0, precision_digits=rounding) < 0.0}

        lot_stock_id_by_warehouse = self.env['stock.warehouse'].search_read([
            ('id', 'in', [g[1] for g in to_refill.keys()])
        ], ['lot_stock_id'])
        lot_stock_id_by_warehouse = {w['id']: w['lot_stock_id'][0] for w in lot_stock_id_by_warehouse}

        # With archived ones to avoid `product_location_check` SQL constraints
        orderpoint_by_product_location = self.env['stock.warehouse.orderpoint'].with_context(active_test=False).read_group(
            [('id', 'in', orderpoints.ids)],
            ['product_id', 'location_id', 'ids:array_agg(id)'],
            ['product_id', 'location_id'], lazy=False)

        orderpoint_by_product_location_dict = {}
        for record in orderpoint_by_product_location:
            if record.get('location_id'):
                orderpoint_by_product_location_dict.update({
                    (record.get('product_id')[0], record.get('location_id')[0]): record.get('ids')[0]
                })
            else:
                orderpoint_by_product_location_dict.update({
                    (record.get('product_id')[0]): record.get('ids')[0]
                })
        orderpoint_by_product_location = orderpoint_by_product_location_dict
        orderpoint_values_list = []
        for (product, warehouse), product_qty in to_refill.items():
            lot_stock_id = lot_stock_id_by_warehouse[warehouse]
            orderpoint_id = orderpoint_by_product_location.get((product, lot_stock_id))
            if orderpoint_id:
                self.env['stock.warehouse.orderpoint'].browse(orderpoint_id).qty_forecast += product_qty
            else:
                orderpoint_values = self.env['stock.warehouse.orderpoint']._get_orderpoint_values(product, lot_stock_id)
                orderpoint_values.update({
                    'name': _('Replenishment Report'),
                    'warehouse_id': warehouse,
                    'company_id': self.env['stock.warehouse'].browse(warehouse).company_id.id,
                })
                orderpoint_values_list.append(orderpoint_values)

        orderpoints = self.env['stock.warehouse.orderpoint'].with_user(SUPERUSER_ID).create(orderpoint_values_list)
        for orderpoint in orderpoints:
            orderpoint.route_id = orderpoint.product_id.route_ids[:1]
        orderpoints.filtered(lambda o: not o.route_id)._set_default_route_id()
        return action

    @api.onchange('product_id', 'location_id', 'run_rate_qty')
    def _onchange_days(self):
        if self.product_id and self.location_id and self.run_rate_qty > 0:
            stock_quant_ids = self.env['stock.quant'].search([('product_id', '=', self.product_id.id),
                                                              ('location_id', '=', self.location_id.id)
                                                              ])
            available_quantity = sum(stock_quant_ids.mapped('available_quantity'))
            qty = available_quantity / self.run_rate_qty
            self.stock_days = qty

    def set_active_period(self, vals):
        self.start_date = vals.get('start_date')
        self.start_month = vals.get('start_month')
        self.end_date = vals.get('end_date')
        self.end_month = vals.get('end_month')
        if self.start_date and self.start_month and self.end_date and self.end_month:
            start_month = dict(self.fields_get(
                    allfields=['start_month'])['start_month']['selection'])[self.start_month]
            end_month = dict(self.fields_get(
                    allfields=['end_month'])['end_month']['selection'])[self.end_month]
            self.periods = self.start_date + " " + start_month + " " +"-" + " " +self.end_date + " " + end_month
            return self.periods

    @api.onchange('start_date','start_month', 'end_date', 'end_month')
    def _onchange_date(self):
        if self.start_date and self.start_month and self.end_date and self.end_month:
            start_month = dict(self.fields_get(
                    allfields=['start_month'])['start_month']['selection'])[self.start_month]
            end_month = dict(self.fields_get(
                    allfields=['end_month'])['end_month']['selection'])[self.end_month]
            self.periods = self.start_date + " " + start_month + " " +"-" + " " +self.end_date + " " + end_month


    @api.depends('warehouse_id')
    def _compute_warehouse(self):
        for record in self:
            location_ids = []
            if record.warehouse_id:
                location_obj = record.env['stock.location']
                store_location_id = record.warehouse_id.view_location_id.id
                addtional_ids = location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')])
                for location in addtional_ids:
                    if location.location_id.id not in addtional_ids.ids:
                        location_ids.append(location.id)
                child_location_ids = record.env['stock.location'].search([('id', 'child_of', location_ids), ('id', 'not in', location_ids)]).ids
                final_location = child_location_ids + location_ids
                record.filter_warehouse_id = [(6, 0, final_location)]
            else:
                record.filter_warehouse_id = [(6, 0, [])]

    @api.depends('product_id', 'warehouse_id', 'run_rate_type',
                'location_id', 'run_rate_period_after',
                'run_rate_period_before')
    def _compute_average_quantity_last_year(self):
        for record in self:
            record.average_quantity_last_year = 0
            record.lead_days_last_year = 0
            if record.run_rate_type == 'get_last_year':
                today_date = datetime.today()
                start_date = today_date.day
                start_month = today_date.month
                year = today_date.year - 1
                if record.location_id:
                    location_ids = record.location_id.ids
                else:
                    location_ids = record.filter_warehouse_id.ids

                first_day = date(year, start_month, start_date)
                first_day_before = first_day - timedelta(days=record.run_rate_period_before + 1)
                first_day_after = first_day + timedelta(days=record.run_rate_period_after)
                move_id = self.env['stock.move'].search([('product_id', '=', record.product_id.id), 
                                                        ('date', '>=', first_day_before.strftime(DEFAULT_SERVER_DATETIME_FORMAT)), 
                                                        ('date', '<=', first_day_after.strftime('%Y-%m-%d 23:59:59')),
                                                        ('state', '=', 'done'),
                                                        ('location_id', 'in', location_ids),
                                                        ])
                lead_move_ids = self.env['stock.move'].search([('product_id', '=', record.product_id.id), 
                                                        ('date', '>=', first_day_before.strftime(DEFAULT_SERVER_DATETIME_FORMAT)), 
                                                        ('date', '<=', first_day_after.strftime('%Y-%m-%d 23:59:59')),
                                                        ('state', '=', 'done'),
                                                        ('location_dest_id', 'in', location_ids),
                                                        ('picking_type_code', '=', 'incoming')
                                                        ])
                process_time_days = sum(lead_move_ids.mapped('picking_id.transfer_log_activity_ids.process_days'))
                if len(lead_move_ids) > 0:
                    record.lead_days_last_year = process_time_days / len(lead_move_ids)
                qty = sum(move_id.mapped('quantity_done'))
                quantity = 0
                if start_month >= 1 and start_month:
                    day = (first_day_after - first_day_before).days
                    if day > 0:
                        quantity = qty / day
                record.average_quantity_last_year = quantity

    @api.depends('product_id', 'warehouse_id', 
                'run_rate_type', 'run_rate_days', 
                'run_rate_period_before',
                'location_id')
    def _compute_average_quantity_run_rate_days(self):
        for record in self:
            record.average_quantity_run_rate_days = 0
            record.lead_days_run_rate_days = 0
            if record.run_rate_type == 'get_past_days_data':
                today_date = datetime.today()
                start_date = today_date.day + 1
                if record.location_id:
                    location_ids = record.location_id.ids
                else:
                    location_ids = record.filter_warehouse_id.ids
                start_month = today_date.month
                date1 = date.today().replace(day=start_date, month=start_month)
                date2 = date1 - timedelta(days=record.run_rate_period_before + 1)
                print (">date2", date2)
                move_id = self.env['stock.move'].search([('product_id', '=', record.product_id.id),
                                                        ('date', '>=', date2.strftime(DEFAULT_SERVER_DATETIME_FORMAT)),
                                                        ('date', '<=', date1.strftime('%Y-%m-%d 23:59:59')),
                                                        ('location_id', 'in', location_ids),
                                                        ('state', '=', 'done'),
                                                        ])
                lead_move_ids = self.env['stock.move'].search([('product_id', '=', record.product_id.id), 
                                                        ('date', '>=', date2.strftime(DEFAULT_SERVER_DATETIME_FORMAT)), 
                                                        ('date', '<=', date1.strftime('%Y-%m-%d 23:59:59')),
                                                        ('state', '=', 'done'),
                                                        ('location_dest_id', 'in', location_ids),
                                                        ('picking_type_code', '=', 'incoming')
                                                        ])
                process_time_days = sum(lead_move_ids.mapped('picking_id.transfer_log_activity_ids.process_days'))
                if len(lead_move_ids) > 0:
                    record.lead_days_run_rate_days = process_time_days / len(lead_move_ids)
                qty = sum(move_id.mapped('quantity_done'))
                run_rate_days = (date1 - date2).days
                quantity = 0
                if run_rate_days >= 1:
                    quantity = qty / run_rate_days
                record.average_quantity_run_rate_days = quantity

    @api.onchange('run_rate_type',  
                'safety_stock',
                'safety_stock_select', 'is_minimum_quantity', 
                'average_quantity_last_year', 'average_quantity_run_rate_days',
                'lead_days_last_year', 'lead_days_run_rate_days')
    def _onchange_run_rate_type(self):
        if self.run_rate_type == 'get_last_year':
            self.run_rate_qty = self.average_quantity_last_year
            self.lead_days = self.lead_days_last_year
            if self.lead_days_last_year > 0:
                self.product_min_qty = self.run_rate_qty * self.lead_days_last_year
            else:
                self.product_min_qty = self.run_rate_qty * 1
        elif self.run_rate_type == 'get_past_days_data':
            self.run_rate_qty = self.average_quantity_run_rate_days
            self.lead_days = self.lead_days_run_rate_days
            if self.lead_days_run_rate_days > 0:
                self.product_min_qty = self.run_rate_qty * self.lead_days_run_rate_days
            else:
                self.product_min_qty = self.run_rate_qty * 1
        if self.safety_stock_select == 'fix_qty':
            self.run_rate_qty += self.safety_stock
        elif self.safety_stock_select == 'percentage' and self.run_rate_qty > 0:
            self.run_rate_qty += ((self.run_rate_qty * self.safety_stock) / 100)

    @api.model
    def _cron_stock_warehouse_orderpoint(self):
        stock_orderpoints = self.search([])
        orderpoint = []
        counter = 1
        partner_ids = []
        for stock_orderpoint in stock_orderpoints:
            if self.start_date and self.start_month:
                start_date = int(stock_orderpoint.start_date)
                start_month = int(stock_orderpoint.start_month)
            else:
                start_date = date.today().day
                start_month = date.today().month
            if self.end_date and self.end_month:
                end_date = int(stock_orderpoint.end_date)
                end_month = int(stock_orderpoint.end_month)
            else:
                end_date = date.today().day
                end_month = date.today().month
            today_date = datetime.today()
            for responsible_user in stock_orderpoint.responsible_id:
                partner_ids.append(responsible_user)

            date1 = today_date.replace(day=start_date, month=start_month)
            date2 = today_date.replace(day=end_date, month=end_month)
            if date1 <= today_date and date2 >= today_date and stock_orderpoint.location_id.id:
                minimum_quantity = stock_orderpoint.product_min_qty
                stock_quant_ids = self.env['stock.quant'].search([('product_id', '=', stock_orderpoint.product_id.id),
                                                                  ('location_id', '=', stock_orderpoint.location_id.id),
                                                                  ])
                available_quantity = sum(stock_quant_ids.mapped('available_quantity'))
                if available_quantity < minimum_quantity:
                    data = {
                        'counter' : counter,
                        'product_id' : stock_orderpoint.product_id.display_name,
                        'location_id' : stock_orderpoint.location_id.display_name,
                        'product_min_qty' : stock_orderpoint.product_min_qty,
                        'available_quantity' : available_quantity,
                        'json_lead_days_popover' : stock_orderpoint.qty_forecast,
                        }
                    counter += 1
                    orderpoint.append(data)
        if orderpoint:
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            action_stock_orderpoint = self.env.ref('stock.action_orderpoint_replenish')
            url = base_url + '/web#action='+ str(action_stock_orderpoint.id) + '&view_type=list&model=stock.warehouse.orderpoint'
            login_url = base_url + '/web/login'
            template_id = self.env.ref('equip3_inventory_control.email_template_stock_warehouse_orderpoint')
            datas = list(set(partner_ids))
            for user in datas:
                email = user.partner_id.email
                user_name = user.name
                ctx = {
                    'user_name': user_name,
                    'email_from' : self.env.user.company_id.email,
                    'email_to' : email,
                    'url' : url,
                    'login_url' : login_url,
                    'data' : orderpoint
                    }
                if stock_orderpoints:
                    template_id.with_context(ctx).send_mail(stock_orderpoints[0].id, True)


    @api.model
    def create(self, vals):
        seq1 = self.env.ref('equip3_inventory_control.stock_warehouse_orderpoint_sequence').sudo()
        record_id = self.search([], limit=1, order='id desc')
        check_today = False
        if record_id and record_id.create_date.date() == date.today():
            check_today = True
        if check_today == True:
            seq = self.env['ir.sequence'].next_by_code('sequence.stock.warehouse.orderpoint')
            vals['name'] = seq
        else:
            seq1.sudo().write({'number_next_actual': 1})
            seq = self.env['ir.sequence'].next_by_code('sequence.stock.warehouse.orderpoint')
            vals['name'] = seq
        # if self.product_id:
        if vals['product_max_qty'] == 0 and vals['product_min_qty'] == 0:
            vals['product_max_qty'] = 1
        elif vals['product_min_qty'] > vals['product_max_qty']:
            vals['product_min_qty'] = 0
        elif vals['product_max_qty'] <= vals['product_min_qty']:
            print('qty1', self.name)
            raise ValidationError('The Maximum Quantity cannot be below or equal to Minimum Quantity')
        return super(StockWarehouseOrderpoint, self).create(vals)


    @api.onchange('product_min_qty','product_max_qty')
    def min_qty_validation(self):
        if self.product_max_qty == 0 and self.product_min_qty == 0:
            self.product_max_qty = 1
        if self.product_id:
            if self.product_max_qty <= self.product_min_qty:
                print('qty', self.name)
                raise ValidationError('The Maximum Quantity cannot be below or equal to Minimum Quantity')

    @api.model
    def action_replenish_orderpoint(self, active_ids):
        records = self.browse(active_ids)
        action_to_take = list(set(records.mapped('action_to_take')))
        location_ids = list(set(records.mapped('location_id')))
        warehouse_ids = list(set(records.mapped('warehouse_id')))
        vendor_ids = list(set(records.mapped('supplier_id')))
        if len(action_to_take) > 1:
            raise ValidationError('Cannot merge more then one actions!') 
        vals = {}
        message = "The following record(s) already on progress of Replenishment:\n"
        is_replenish_document_created = False
        for rec in records:
            if rec.is_replenish_document_created:
                is_replenish_document_created = True
                message += ' - ' + rec.name + ' - ' + rec.product_id.display_name + ' - ' + rec.warehouse_id.name + '\n'
        if is_replenish_document_created:
            raise ValidationError (('%s') % message)
        if action_to_take and action_to_take[0] == 'create_mr':
            for warehouse in warehouse_ids:
                lines = records.filtered(lambda a: a.warehouse_id.id == warehouse.id)
                product_line_data = []
                temp_list = []
                line_list_vals = []
                name = ",".join(lines.mapped('name'))
                for line in lines:
                    if line.product_id.id in temp_list:
                        filter_list = list(filter(lambda r: r.get('product') == line.product_id.id, line_list_vals))
                        if filter_list:
                            filter_list[0]['quantity'].append(line.qty_to_order)
                    else:
                        temp_list.append(line.product_id.id)
                        line_list_vals.append({
                                'product' : line.product_id.id,
                                'description' : line.product_id.name,
                                'quantity' : [line.qty_to_order],
                                'product_unit_measure' : line.product_id.uom_id.id,
                                'request_date' : date.today(),
                                'destination_warehouse_id': warehouse.id,
                            }
                        ) 
                for product_line in line_list_vals:
                    product_line['quantity'] = sum(product_line['quantity'])
                    product_line_data.append((0, 0, product_line))
                vals={
                    'requested_by' : self.env.user.id,
                    'schedule_date' : date.today(),
                    'source_document' : name,
                    'product_line' : product_line_data,
                    'destination_warehouse_id' : warehouse.id,
                }
                material_request = self.env['material.request'].create(vals)
                material_request._onchange_analytic_account_group_ids()

                lines.write({
                    'trigger': 'manual', 
                    'is_replenish_document_created': True, 
                    'material_request_id' : material_request.id, 
                    'replenish_document_status': 'Draft',
                    'snoozed_until': date.today() + timedelta(days=1),
                })
        elif action_to_take and action_to_take[0] == 'create_pr':
            product_line_data = []
            temp_list = []
            line_list_vals = []
            name = ",".join(records.mapped('name'))
            for warehouse in warehouse_ids:
                lines = records.filtered(lambda a: a.warehouse_id.id == warehouse.id)
                for line in lines:
                    if {'product_id': line.product_id.id, 'dest_loc_id': line.warehouse_id.id} in temp_list:
                        filter_list = list(filter(lambda r: r.get('product_id') == line.product_id.id and r.get('dest_loc_id') == line.warehouse_id.id,  line_list_vals))
                        if filter_list:
                            filter_list[0]['product_qty'].append(line.qty_to_order)
                    else:
                        temp_list.append({'product_id': line.product_id.id, 'dest_loc_id': line.warehouse_id.id})
                        line_list_vals.append({
                            'product_id' : line.product_id.id,
                            'name' : line.product_id.name,
                            'product_qty' : [line.qty_to_order],
                            'product_uom_id' : line.product_id.uom_id.id,
                            'dest_loc_id': line.warehouse_id.id,
                        })
            for final_line in line_list_vals:
                final_line['product_qty'] = sum(final_line['product_qty'])
                product_line_data.append((0, 0, final_line))
            vals = {
                'requested_by' : self.env.user.id,
                'origin' : name,
                'is_goods_orders' : True,
                'line_ids' : product_line_data,
            }
            purchase_request = self.env['purchase.request'].create(vals)
            records.write({
                'trigger': 'manual',
                'is_replenish_document_created': True,
                'purchase_request_id' : purchase_request.id,
                'replenish_document_status': 'Draft',
                'snoozed_until': date.today() + timedelta(days=1),
            })
        elif action_to_take and action_to_take[0] == 'create_rfq':
            temp_list = []
            line_list_vals = []
            for record in records:
                if {'warehouse_id': record.warehouse_id.id, 'vendor_id': record.supplier_id.name.id} in temp_list:
                    filter_line = list(filter(lambda r:r.get('warehouse_id') == record.warehouse_id.id and r.get('vendor_id') == record.supplier_id.name.id, line_list_vals))
                    if filter_line:
                        filter_line[0]['lines'].append(record)
                else:
                    temp_list.append({'warehouse_id': record.warehouse_id.id, 'vendor_id': record.supplier_id.name.id})
                    line_list_vals.append({
                        'warehouse_id': record.warehouse_id.id, 
                        'vendor_id': record.supplier_id.name.id,
                        'lines': [record]
                    })
            final_lines = []
            name = ",".join([order_point.name for order_point in records])
            for rfq_line in line_list_vals:
                product_line_data = []
                temp_lines = []
                for line in rfq_line.get('lines'):
                    if line.product_id.id in temp_lines:
                        filter_list = list(filter(lambda r: r.get('product_id') == line.product_id.id, final_lines))
                        if filter_list:
                            filter_list[0]['product_qty'].append(line.qty_to_order)
                    else:
                        temp_lines.append(line.product_id.id)
                        final_lines.append({
                                'product_id' : line.product_id.id,
                                'name' : line.product_id.name,
                                'price_unit': 1.0,
                                'display_type': False,
                                'date_planned': datetime.today(),
                                'product_qty' : [line.qty_to_order],
                                'product_uom' : line.product_id.uom_id.id,
                                'destination_warehouse_id' : line.warehouse_id.id,
                            })
            for product_line in final_lines:
                product_line['product_qty'] = sum(product_line['product_qty'])
                product_line_data.append((0, 0, product_line))
            warehouse_id = self.env['stock.warehouse'].search([('company_id','=',self.env.company.id)], limit=1)
            vals = {
                'partner_id' : rfq_line.get('vendor_id'),
                'is_goods_orders' : True,
                'order_line' : product_line_data,
                'picking_type_id': warehouse_id.int_type_id.id,
                'origin' : name,
            }
            purchase_order = self.env['purchase.order'].create(vals)
            for line in records:
                line.write({
                    'trigger': 'manual',
                    'is_replenish_document_created': True,
                    'purchase_order_id' : purchase_order.id,
                    'replenish_document_status': 'Draft',
                    'snoozed_until': date.today() + timedelta(days=1),
                    })
        elif action_to_take and action_to_take[0] == 'create_itr':
            temp_list = []
            line_list_vals = []
            for record in records:
                if {'destination_location_id': record.location_id.id, 'source_location_id': record.source_location_id.id} in temp_list:
                    filter_line = list(filter(lambda r:r.get('destination_location_id') == record.location_id.id and r.get('source_location_id') == record.source_location_id.id, line_list_vals))
                    if filter_line:
                        filter_line[0]['lines'].append(record)
                else:
                    temp_list.append({'destination_location_id': record.location_id.id, 'source_location_id': record.source_location_id.id})
                    line_list_vals.append({
                        'destination_location_id': record.location_id.id,
                        'source_location_id': record.source_location_id.id,
                        'lines': [record]
                    })
            for itr_line in line_list_vals:
                product_line_data = []
                temp_lines = []
                counter = 1
                final_lines = []
                name = ",".join([order_point.name for order_point in itr_line.get('lines')])
                for line in itr_line.get('lines'):
                    if line.product_id.id in temp_lines:
                        filter_list = list(filter(lambda r: r.get('product_id') == line.product_id.id, final_lines))
                        if filter_list:
                            filter_list[0]['qty'].append(line.qty_to_order)
                    else:
                        temp_lines.append(line.product_id.id)
                        final_lines.append({
                                'sequence': counter,
                                'product_id' : line.product_id.id,
                                'description' : line.product_id.display_name,
                                'qty' : [line.qty_to_order],
                                'scheduled_date' : date.today(),
                                'uom' : line.product_id.uom_id.id,
                                'source_location_id': itr_line.get('source_location_id'),
                                'destination_location_id': itr_line.get('destination_location_id'),
                            })
                for product_line in final_lines:
                    product_line['qty'] = sum(product_line['qty'])
                    product_line_data.append((0, 0, product_line))
                vals={
                    'source_location_id' : itr_line.get('source_location_id'),
                    'destination_location_id' : itr_line.get('destination_location_id'),
                    'scheduled_date' : date.today(),
                    'source_document' : name,
                    'product_line_ids' : product_line_data,
                }
                internal_transfer = self.env['internal.transfer'].create(vals)
                print('internal_transfer',internal_transfer)
                internal_transfer.write({'source_warehouse_id': record.source_location_id.warehouse_id.id, 'destination_warehouse_id': record.location_id.warehouse_id.id})
                for line in itr_line.get('lines'):
                    line.write({
                        'trigger': 'manual', 
                        'is_replenish_document_created': True,
                        'internal_transfer_id' : internal_transfer.id,
                        'replenish_document_status': 'Draft',
                        'snoozed_until': date.today() + timedelta(days=1),
                    })
                internal_transfer.onchange_source_loction_id()
                internal_transfer.onchange_dest_loction_id()
        elif action_to_take and action_to_take[0] in ('create_mo', 'create_mp'):
            product_line_data = []
            temp_list = []
            line_list_vals = []
            name = ",".join(records.mapped('name'))
            for warehouse in warehouse_ids:
                lines = records.filtered(lambda a: a.warehouse_id.id == warehouse.id)
                for line in lines:
                    if {'product_id': line.product_id.id, 'warehouse_id': line.warehouse_id.id} in temp_list:
                        filter_list = list(filter(lambda r: r.get('product_id').id == line.product_id.id and r.get('warehouse_id') == line.warehouse_id.id,  line_list_vals))
                        if filter_list:
                            filter_list[0]['product_uom_qty'].append(line.qty_to_order)
                    else:
                        temp_list.append({'product_id': line.product_id.id, 'warehouse_id': line.warehouse_id.id})
                        line_list_vals.append({
                            'product_id' : line.product_id,
                            'name' : line.product_id.name,
                            'product_uom_qty' : [line.qty_to_order],
                            'product_uom' : line.product_id.uom_id.id,
                            'warehouse_id': line.warehouse_id.id,
                            'company_id': line.company_id.id,
                            'picking_type_id': line.warehouse_id.manu_type_id.id,
                            'location_src_id': line.warehouse_id.lot_stock_id.id,
                            'location_dest_id': line.warehouse_id.lot_stock_id.id,
                        })
            for final_line in line_list_vals:
                final_line['product_uom_qty'] = sum(final_line['product_uom_qty'])
                bom_id = self.env['mrp.bom'].search(['|', ('product_id', '=', final_line.get('product_id').id), 
                                                          ('product_tmpl_id', '=', final_line.get('product_id').product_tmpl_id.id)], limit=1)
                vals = {
                    'product_id': final_line.get('product_id').id,
                    'product_qty': final_line.get('product_uom_qty'),
                    'product_uom_id': final_line.get('product_uom'),
                    'date_planned_start': date.today(),
                    'company_id': final_line.get('company_id'),
                    'picking_type_id': final_line.get('picking_type_id'),
                    'location_src_id': final_line.get('location_src_id'),
                    'location_dest_id': final_line.get('location_dest_id'),
                    'bom_id' : bom_id.id,
                }
                manufacturing_order_id = self.env['mrp.production'].create(vals)
                manufacturing_order_id._onchange_bom_id()
                self.env['stock.move'].create(manufacturing_order_id.with_context({'replenish': True})._get_moves_raw_values())
                self.env['stock.move'].create(manufacturing_order_id.with_context({'replenish': True})._get_moves_finished_values())
                manufacturing_order_id._onchange_location()
                manufacturing_order_id._onchange_location_dest()
                manufacturing_order_id._onchange_workorder_ids()
                manufacturing_order_id.product_qty = final_line.get('product_uom_qty')
                manufacturing_order_id.with_context({'replenish': True})._onchange_move_raw()
                manufacturing_order_id.with_context({'replenish': True})._onchange_move_finished()
                if action_to_take and action_to_take[0] == 'create_mp':
                    vals = {
                        'analytic_tag_ids': [(6, 0, self.env.user.analytic_tag_ids.ids)],
                    }
                    mrp_plan_id = self.env['mrp.plan'].create(vals)
                    manufacturing_order_id.write({'mrp_plan_id': mrp_plan_id.id})
            # records.write({
            #     'trigger': 'manual',
            #     'is_replenish_document_created': True,
            #     'replenish_document_status': 'Draft',
            #     'snoozed_until': date.today() + timedelta(days=1),
            # })

        for rec in records:
            for user in rec.responsible_id:
                ctx = {}
                record_ref = ''
                subject = ''
                base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
                orderpoint_ref = ''
                url = ''
                template_id = self.env.ref('equip3_inventory_control.email_template_stock_warehouse_orderpoint_for_all')
                
                if rec.action_to_take == 'create_mr':
                    record_ref = 'Material Request'
                    orderpoint_ref = rec.material_request_id.name
                    url = base_url + '/web#id='+ str(rec.material_request_id.id) + '&view_type=form&model=material.request'
                elif rec.action_to_take == 'create_pr':
                    record_ref = 'Purchase Request'
                    orderpoint_ref = rec.purchase_request_id.name
                    url = base_url + '/web#id='+ str(rec.purchase_request_id.id) + '&view_type=form&model=purchase.request'
                elif rec.action_to_take == 'create_rfq':
                    record_ref = 'Request for Quotation'
                    orderpoint_ref = rec.purchase_order_id.name
                    url = base_url + '/web#id='+ str(rec.purchase_order_id.id) + '&view_type=form&model=purchase.order'
                elif rec.action_to_take == 'create_itr':
                    record_ref = 'Internal Transfer Request'
                    orderpoint_ref = rec.internal_transfer_id.name
                    url = base_url + '/web#id='+ str(rec.internal_transfer_id.id) + '&view_type=form&model=internal.transfer'
                subject =  record_ref + ' To ' + rec.product_id.display_name
                ctx.update({
                    'email_from': self.env.user.company_id.email,
                    'email_to': user.partner_id.email,
                    'subject': subject,
                    'user_name': user.name,
                    'orderpoint_ref': orderpoint_ref,
                    'login_url': url,
                    'ref': record_ref,
                })
                template_id.with_context(ctx).send_mail(rec.id, True)

    
    @api.model
    def default_get(self, fields):
        res = super(StockWarehouseOrderpoint, self).default_get(fields)
        context = dict(self.env.context) or {}
        res['responsible_id'] = [(6, 0, self.env.user.ids)]
        if context.get('search_default_trigger'):
            res['trigger'] = 'auto'
        return res
