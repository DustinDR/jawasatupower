# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import stock_inventory
from . import usage_type
from . import stock_scrap_request
from . import res_config_settings
from . import stock_warehouse_orderpoint
from . import product_category
from . import mrp_production
from . import stock_picking
from . import untaken_stock