# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime, date, timedelta
from odoo import _, api, fields, models
from odoo.addons.base.models.ir_model import MODULE_UNINSTALL_FLAG
from odoo.exceptions import UserError, ValidationError
from odoo.osv import expression
from odoo.tools import float_compare, float_is_zero
import pytz
from pytz import timezone, UTC
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

class StockLocation(models.Model):
    _inherit = 'stock.location'

    location_display_name = fields.Char(string='Location Display Name', compute='_compute_location_display_name', store=True)

    @api.depends('display_name')
    def _compute_location_display_name(self):
        for record in self:
            record.location_display_name = record.display_name

class Product(models.Model):
    _inherit = 'product.product'

    product_display_name = fields.Char(string='Product Display Name', compute='_compute_product_display_name', store=True)

    @api.depends('name', 'default_code')
    def _compute_product_display_name(self):
        for record in self:
            if isinstance(record.id, models.NewId):
                record.product_display_name = ''
            else:
                record.product_display_name = record.display_name or ''

class StockInventory(models.Model):
    _inherit = 'stock.inventory'
    _description = "Inventory Adjustment"

    warehouse_id = fields.Many2one('stock.warehouse', string="Warehouse", tracking=True, required=True)
    user_id = fields.Many2one('res.users', string="Responsible", tracking=True)
    inventoried_product = fields.Selection([('all_product', 'All Products'), ('specific_product', 'Specific Products'), ('specific_category', 'Specific Categories')], string="Inventoried Product", default='all_product', tracking=True)
    is_adj_value = fields.Boolean(string="Input Unit Price")
    is_counted_qty = fields.Boolean(string="Counted Quantities", compute='_compute_counted_qty', store=False, readonly=True)
    adjustment_account_id = fields.Many2one('account.account', string="Adjustment Account")
    filtered_adjustment_account_id = fields.Many2many('account.account', string="Filter Adjustment Account", compute="_compute_filtered_adjustment_account_id")
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Groups', default=lambda self: self.env.user.analytic_tag_ids.ids)
    is_analytic_mandatory = fields.Boolean(compute='compute_is_analytic_mandatory')
    is_analytic_readonly_dup = fields.Boolean(compute="compute_is_analytic_readonly", default=True)
    state = fields.Selection(string='Status', selection=[
        ('draft', 'Draft'),
        ('cancel', 'Cancelled'),
        ('confirm', 'In Progress'),
        ('completed', 'Completed'),
        ('to_approve', 'Waiting for Approval'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('done', 'Validated')],
         copy=False, index=True, readonly=True, tracking=True,
         default='draft')
    inv_state = fields.Selection(related='state', tracking=False)
    inv_state_1 = fields.Selection(related='state', tracking=False)
    branch_id = fields.Many2one('res.branch', string="Branch", related='warehouse_id.branch_id', tracking=True)
    approval_matrix_id = fields.Many2one('stock.inventory.approval.matrix', string="Approval Matrix", compute='_get_approval_matrix')
    is_stock_count_approval = fields.Boolean(string="Stock Count Approval", compute='_get_approve_button_from_config')
    approved_matrix_ids = fields.One2many('stock.inventory.approval.matrix.line', 'st_inv_id', compute="_compute_approving_matrix_lines_inv", store=True, string="Approved Matrix")
    approval_matrix_line_id = fields.Many2one('stock.inventory.approval.matrix.line', string='Material Approval Matrix Line', compute='_get_approve_button', store=False)
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button', store=False)
    inv_ref = fields.Char(default="New", readonly=True)
    product_categories = fields.Many2many('product.category', 'category_id', 'p_id', string="Product Category", tracking=True)
    description = fields.Text(string="Description", tracking=True)
    location_ids = fields.Many2many(
        'stock.location', string='Locations',
        readonly=True, check_company=True,
        states={'draft': [('readonly', False)]}, required=True,
        domain="[('company_id', '=', company_id), ('usage', 'in', ['internal', 'transit'])]")
    is_mbs_on_stock_count_and_inventory_adjustment = fields.Boolean(string="Stock Count and Inventory Adjustment", compute='_compute_stock_count_and_inventory_adjustment')

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        context = dict(self.env.context) or {}
        is_stock_count_approval = self.env['ir.config_parameter'].sudo().get_param('is_stock_count_approval')
        domain = domain or []
        if 'is_inv_adj_acc' in context:
            if is_stock_count_approval:
                domain.extend([('state', 'in', ('approved', 'done'))])
            else:
                domain.extend([('state', 'in', ('completed', 'done'))])
        return super(StockInventory, self).search_read(domain=domain, fields=fields, offset=offset, limit=limit, order=order)

    def _get_approve_button(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False

    def _get_approve_button_from_config(self):
        is_stock_count_approval = self.env['ir.config_parameter'].sudo().get_param('is_stock_count_approval')
        for record in self:
            record.is_stock_count_approval = is_stock_count_approval
    
    def _compute_stock_count_and_inventory_adjustment(self):
        is_mbs_on_stock_count_and_inventory_adjustment = self.env['ir.config_parameter'].sudo().get_param('is_mbs_on_stock_count_and_inventory_adjustment', False)
        for record in self:
            record.is_mbs_on_stock_count_and_inventory_adjustment = is_mbs_on_stock_count_and_inventory_adjustment

    @api.depends('approval_matrix_id')
    def _compute_approving_matrix_lines_inv(self):
        data = [(5, 0, 0)]
        for record in self:
            counter = 1
            record.approved_matrix_ids = []
            for line in record.approval_matrix_id.si_approval_matrix_line_ids:
                data.append((0, 0, {
                    'sequence' : counter,
                    'user_ids' : [(6, 0, line.user_ids.ids)],
                    'minimum_approver' : line.minimum_approver,
                }))
                counter += 1
            record.approved_matrix_ids = data

    @api.depends('warehouse_id')
    def _get_approval_matrix(self):
        for record in self:
            matrix_id = self.env['stock.inventory.approval.matrix'].search([('warehouse_id', '=', record.warehouse_id.id)], limit=1)
            record.approval_matrix_id = matrix_id

    @api.onchange('warehouse_id')
    def _onchage_warhouse_branch(self):
        self._get_approve_button_from_config()
        self._get_approve_button()
        self._compute_stock_count_and_inventory_adjustment()

    def inv_request_for_approving(self):
        for record in self:
            record.write({'state': 'to_approve'})

    def inv_approving(self):
        for record in self:
            user = self.env.user
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_ids.ids and \
                    user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    utc_datetime = datetime.now()
                    local_timezone = pytz.timezone(self.env.user.tz)
                    local_datetime = utc_datetime.replace(tzinfo=pytz.utc)
                    local_datetime = local_datetime.astimezone(local_timezone).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                    if name != '':
                        name += "\n • %s: Approved - %s" % (self.env.user.name, local_datetime)
                    else:
                        name += "• %s: Approved - %s" % (self.env.user.name, local_datetime)

                    approval_matrix_line_id.write({ 
                        'last_approved': self.env.user.id, 'state_char': name, 
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
                        # next_approval_matrix_line_id = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
                        # if next_approval_matrix_line_id and len(next_approval_matrix_line_id[0].approver) > 1:
                        #     pass
            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r:r.approved)):
                record.write({'state': 'approved'})

    def inv_reject(self):
        for record in self:
            return {
                    'type': 'ir.actions.act_window',
                    'name': 'Reject Stock Inventory Matrix',
                    'res_model': 'stock.inventory.request.matrix.reject',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'new',
                }
    def set_to_draft_inv(self):
        for record in self:
            record.write({'state': 'draft'})

    @api.depends('is_analytic_readonly_dup')
    def compute_is_analytic_mandatory(self):
        user = self.env.user
        for record in self:
            if user.has_group('analytic.group_analytic_tags'):
                record.is_analytic_mandatory = True
            else:
                record.is_analytic_mandatory = False

    def compute_is_analytic_readonly(self):
        for record in self:
            group_allow_validate_inventory_adjustment = self.env['ir.config_parameter'].sudo().get_param('is_inventory_adjustment_with_value', False)
            if not group_allow_validate_inventory_adjustment:
                record.is_analytic_readonly_dup = False
            else:
                record.is_analytic_readonly_dup = True

    def _compute_filtered_adjustment_account_id(self):
        for record in self:
            current_assets = record.env.ref('account.data_account_type_current_assets')
            filter_adj_acc_id = record.env['account.account'].search([('user_type_id', '=', current_assets.id)])
            record.filtered_adjustment_account_id = [(6, 0, filter_adj_acc_id.ids)]
  
    def _compute_counted_qty(self):
        group_allow_see_onhand_difference = self.env.user.has_group('equip3_inventory_accessright_setting.group_allow_see_onhand_difference')
        for rec in self:
            if not group_allow_see_onhand_difference:
                rec.is_counted_qty = True
            else:
                rec.is_counted_qty = False

    @api.model
    def default_get(self, fields):
        res = super(StockInventory, self).default_get(fields)
        group_allow_see_onhand_difference = self.env.user.has_group('equip3_inventory_accessright_setting.group_allow_see_onhand_difference')
        if not group_allow_see_onhand_difference:
            res['prefill_counted_quantity'] = 'zero'
        return res

    @api.onchange('adjustment_account_id')
    def _onchange_adjustment_account_id(self):
        for record in self.line_ids:
            record.adjustment_account_id = self.adjustment_account_id.id

    # def _set_domain_for_product_categ(self):
    #     if self.inventoried_product == 'specific_category':
    #         print('pc')
    #         product_categ_list = []
    #         categ_rec = self.env['product.category'].search([])
    #         for categ in categ_rec:
    #             if categ.id not in product_categ_list:
    #                 product_ids = self.env['product.product'].search([('categ_id', '=', categ.id)])
    #                 if product_ids:
    #                     product_categ_list.append(categ.id)
    #         print('pcid',product_categ_list)
    #         res = {}
    #         if product_categ_list:
    #             res['domain'] = {'product_categories': [('id', 'in', product_categ_list)]}
    #         return res

    @api.model
    def create(self, vals):
        print('ccccc')
        seq1 = self.env.ref('equip3_inventory_control.inventory_adjust_sequence')
        seq2 = self.env.ref('equip3_inventory_control.inventory_adjust_value_sequence')
        record_id = self.search([], limit=1, order='id desc')
        check_today = False
        if record_id and record_id.create_date.date() == date.today():
            check_today = True
        
        if not check_today:
            if vals.get('is_adj_value') == False:
                seq1.number_next_actual = 1
            else:
                seq2.number_next_actual = 1
        res = super(StockInventory, self).create(vals)
        if res.is_adj_value:
            res.inv_ref = self.env['ir.sequence'].next_by_code('inv.adj.value.seq')
        else:
            res.inv_ref = self.env['ir.sequence'].next_by_code('inv.adj.seq')
        return res

    @api.constrains('location_ids', 'state')
    def _check_location_ids(self):
        for rec in self:
            for location_id in rec.location_ids:
                inv_adj_ids = self.search([('location_ids', 'in', location_id.ids),('is_adj_value','=',rec.is_adj_value),('state', '=', 'confirm'), ('id', '!=', rec.id)])
                if len(inv_adj_ids) >= 1 and rec.state != 'draft':
                    message = ''
                    for inv_adj_id in inv_adj_ids:
                        message += " - %s on %s \n" % (location_id.display_name, inv_adj_id.inv_ref)
                    raise ValidationError(_('An Inventory Adjustment has been conducted on the following location(s): \n%s' % (message)))

    @api.onchange('warehouse_id')
    def set_domain_for_location_ids(self):
        self._compute_counted_qty()
        location_ids = []
        final_location = []
        if self.warehouse_id:
            location_obj = self.env['stock.location']
            store_location_id = self.warehouse_id.view_location_id.id
            addtional_ids = location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')])
            for location in addtional_ids:
                if location.location_id.id not in addtional_ids.ids:
                    location_ids.append(location.id)
            child_location_ids = self.env['stock.location'].search([('id', 'child_of', location_ids), ('id', 'not in', location_ids)]).ids
            final_location = child_location_ids + location_ids
        res = {}
        res['domain'] = {'location_ids': [('id', 'in', final_location)]}
        return res

    @api.onchange('inventoried_product')
    def _onchange_inventoried_prduct(self):
        if self.inventoried_product == 'all_product':
            product_ids = self.env['product.product'].search([])
            self.product_ids = [(6,0, product_ids.ids)]
        else:
            self.product_ids = [(6,0, [])]
        self.product_categories = False

    @api.onchange('product_categories')
    def _onchange_product_categories_prduct(self):
        if self.product_categories:
            if self.inventoried_product == 'specific_category':
                # if self.product_ids:
                #     self.product_ids = [(6,0, [])]
                product_list = []
                product_ids = self.env['product.product'].search([('categ_id', 'in', self.product_categories.ids)])
                for prod in product_ids:
                    product_list.append(prod.id)
                print('pl', product_list)
                if self.location_ids:
                    stock_quant = self.env['stock.quant'].search([('location_id', 'in', self.location_ids.ids)])
                    prod_dup = []
                    print('r2')
                    for quant in stock_quant:
                        if quant.product_id.id not in prod_dup:
                            if quant.product_id.id in product_list:
                                print('l1', quant.product_id.id)
                                self.product_ids = [(4,quant.product_id.id)]
                                prod_dup.append(quant.product_id.id)

    #delete in product-category is not changing products(fix)
    def write(self, vals):
        res = super(StockInventory, self).write(vals)
        print('aaaaaaaaaaa')
        context = dict(self.env.context) or {}
        categ_list = []
        for categ in self.product_categories:
            print('cn',categ.name)
            categ_list.append(categ.id)
        remove_product_ids = []
        if self.inventoried_product == 'specific_category':
            for product in self.product_ids:
                print('chk1')
                if product.categ_id.id not in categ_list:
                    remove_product_ids.append(product.id)

        # if not context.get('remove_product_ids'):
        #     print('chk2')
        #     self.with_context(remove_product_ids=True).write({'product_ids': [(3, product_id) for product_id in remove_product_ids]})

        stock_quant = self.env['stock.quant'].search([('location_id', 'in', self.location_ids.ids)])
        products_list = []
        for categ in categ_list:
                for quant in stock_quant:
                    if categ == quant.product_id.categ_id.id:
                        products_list.append(quant.product_id.id)
        for product in self.product_ids:
            if product.id in products_list:
                print('chk3')
                products_list.remove(product.id)

        if not context.get('add_product_ids'):
            self.with_context(add_product_ids=True).write({'product_ids': [(4,product) for product in products_list]})
        return res

    def action_complete(self):
        self.write({'state': 'completed'})
        return True

    def action_open_inventory_lines(self):
        self.ensure_one()
        action = {
            'type': 'ir.actions.act_window',
            'view_mode': 'tree',
            'name': _('Inventory Lines'),
            'res_model': 'stock.inventory.line',
        }
        context = dict(self.env.context) or {}
        context.update({
            'default_is_editable': True,
            'default_inventory_id': self.id,
            'default_company_id': self.company_id.id,
            'is_inv_adj_acc': context.get('is_inv_adj_acc') if context.get('is_inv_adj_acc') else True
        })
        # Define domains and context
        domain = [
            ('inventory_id', '=', self.id),
            ('location_id.usage', 'in', ['internal', 'transit'])
        ]
        if self.location_ids:
            context['default_location_id'] = self.location_ids[0].id
            if len(self.location_ids) == 1:
                if not self.location_ids[0].child_ids:
                    context['readonly_location_id'] = True

        if self.product_ids:
            action['view_id'] = self.env.ref('stock.stock_inventory_line_tree_no_product_create').id
            if len(self.product_ids) == 1:
                context['default_product_id'] = self.product_ids[0].id
        else:
            action['view_id'] = self.env.ref('stock.stock_inventory_line_tree').id

        if self.product_categories:
            # print('ccc')
            # for product in self.product_ids:
            #     self.product_ids.write({'id': [(3, product.id)]})
            self.line_ids.search([]).unlink()
            quants_groups = self._get_quantities()
            vals = []
            for (product_id, location_id, lot_id, package_id, owner_id), quantity in quants_groups.items():
                line_values = {
                    'inventory_id': self.id,
                    'product_qty': 0 if self.prefill_counted_quantity == "zero" else quantity,
                    'theoretical_qty': quantity,
                    'prod_lot_id': lot_id,
                    'partner_id': owner_id,
                    'product_id': product_id,
                    'location_id': location_id,
                    'package_id': package_id
                }
                line_values['product_uom_id'] = self.env['product.product'].browse(product_id).uom_id.id
                vals.append(line_values)
            self.line_ids.create(vals)
            action['view_id'] = self.env.ref('stock.stock_inventory_line_tree_no_product_create').id

        if self.state in ('completed', 'approved', 'done'):
            action['view_id'] = self.env.ref('equip3_inventory_control.stock_inventory_line_tree_no_product_create_account_adj').id

        if self.line_ids and self._context.get('sequence_change'):
            counter = 1
            for line in self.line_ids:
                line.write({'sequence': counter})
                counter += 1

        action['context'] = context
        action['domain'] = domain
        return action

    def _get_quantities(self):
        """Return quantities group by product_id, location_id, lot_id, package_id and owner_id

        :return: a dict with keys as tuple of group by and quantity as value
        :rtype: dict
        """
        self.ensure_one()
        if self.location_ids:
            domain_loc = [('id', 'child_of', self.location_ids.ids)]
        else:
            domain_loc = [('company_id', '=', self.company_id.id), ('usage', 'in', ['internal', 'transit'])]
        locations_ids = [l['id'] for l in self.env['stock.location'].search_read(domain_loc, ['id'])]

        domain = [('company_id', '=', self.company_id.id),
                  ('quantity', '!=', '0'),
                  ('location_id', 'in', locations_ids)]
        if self.prefill_counted_quantity == 'zero':
            domain.append(('product_id.active', '=', True))


        # if self.inventoried_product == 'specific_category':
            # print('test_chk')
            # product_ids = self.env['product.product'].search([('categ_id', 'in', self.product_categories.ids)])
            # self.product_ids = [(6,0, product_ids.ids)]
        # print('sp', self.product_ids)
        if self.product_ids:
            print('True')
            domain = expression.AND([domain, [('product_id', 'in', self.product_ids.ids)]])

        fields = ['product_id', 'location_id', 'lot_id', 'package_id', 'owner_id', 'quantity:sum']
        group_by = ['product_id', 'location_id', 'lot_id', 'package_id', 'owner_id']

        quants = self.env['stock.quant'].read_group(domain, fields, group_by, lazy=False)
        return {(
                    quant['product_id'] and quant['product_id'][0] or False,
                    quant['location_id'] and quant['location_id'][0] or False,
                    quant['lot_id'] and quant['lot_id'][0] or False,
                    quant['package_id'] and quant['package_id'][0] or False,
                    quant['owner_id'] and quant['owner_id'][0] or False):
                    quant['quantity'] for quant in quants
                }


    def action_validate(self):
        if not self.exists():
            return
        self.ensure_one()
        if not self.user_has_groups('stock.group_stock_manager'):
            raise UserError(_("Only a stock manager can validate an inventory adjustment."))
        context = dict(self.env.context) or {}
        context.update({
            'adjustment_account_id': self.adjustment_account_id and self.adjustment_account_id.id or False
        })
        self = self.with_context(context)
        if self.state not in ('completed', 'approved'):
            raise UserError(_(
                "You can't validate the inventory '%s', maybe this inventory "
                "has been already validated or isn't ready.", self.name))
        inventory_lines = self.line_ids.filtered(lambda l: l.product_id.tracking in ['lot', 'serial'] and not l.prod_lot_id and l.theoretical_qty != l.product_qty)
        lines = self.line_ids.filtered(lambda l: float_compare(l.product_qty, 1, precision_rounding=l.product_uom_id.rounding) > 0 and l.product_id.tracking == 'serial' and l.prod_lot_id)
        if inventory_lines and not lines:
            wiz_lines = [(0, 0, {'product_id': product.id, 'tracking': product.tracking}) for product in inventory_lines.mapped('product_id')]
            wiz = self.env['stock.track.confirmation'].create({'inventory_id': self.id, 'tracking_line_ids': wiz_lines})
            return {
                'name': _('Tracked Products in Inventory Adjustment'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'views': [(False, 'form')],
                'res_model': 'stock.track.confirmation',
                'target': 'new',
                'res_id': wiz.id,
            }
        self._action_done()
        self.line_ids._check_company()
        self._check_company()
        return True

    def action_wizard_save(self):
        return True


class InventoryLine(models.Model):
    _inherit = "stock.inventory.line"

    location_id = fields.Many2one('stock.location', 'Location', required=False)
    sequence = fields.Char(string="No")
    adjustment_account_id = fields.Many2one('account.account', related='inventory_id.adjustment_account_id', string="Adjustment Account")
    filtered_adjustment_account_id = fields.Many2many('account.account', string="Filter Adjustment Account", compute="_compute_adjustment_account_id")
    is_adj_val = fields.Boolean(related="inventory_id.is_adj_value", string="Value")
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Groups', related="inventory_id.analytic_tag_ids", readonly='1')

    def _compute_adjustment_account_id(self):
        for record in self:
            current_assets = record.env.ref('account.data_account_type_current_assets')
            filter_adj_acc_id = record.env['account.account'].search([('user_type_id', '=', current_assets.id)])
            record.filtered_adjustment_account_id = [(6, 0, filter_adj_acc_id.ids)]
            
    @api.onchange('inventory_id')
    def _onchange_inventory_id(self):
        self.sequence = len(self.inventory_id.line_ids) + 1

    @api.constrains('product_id')
    def _check_product_id(self):
        """ As no quants are created for consumable products, it should not be possible do adjust
        their quantity.
        """
        return True
        # for line in self:
        #     if line.product_id.type != 'product':
        #         raise ValidationError(_("You can only adjust storable products.") + '\n\n%s -> %s' % (line.product_id.display_name, line.product_id.type))


class StockMoveLine(models.Model):
    _inherit = "stock.move.line"

    scrap_sale_price = fields.Float(string="Sale Price")

class StockMove(models.Model):
    _inherit = "stock.move"

    def _account_entry_move(self, qty, description, svl_id, cost):
        context = dict(self.env.context) or {}
        if context.get('is_scrap') or context.get('default_is_product_usage'):
            pass
        else:
            return super(StockMove, self)._account_entry_move(qty, description, svl_id, cost)

    def _get_src_account(self, accounts_data):
        context = dict(self.env.context) or {}
        if 'is_inv_adj_acc' in context and context.get('adjustment_account_id'):
            return context.get('adjustment_account_id')
        return self.location_id.valuation_out_account_id.id or accounts_data['stock_input'].id

    def _get_dest_account(self, accounts_data):
        context = dict(self.env.context) or {}
        if 'is_inv_adj_acc' in context and context.get('adjustment_account_id'):
            return context.get('adjustment_account_id')
        return self.location_dest_id.valuation_in_account_id.id or accounts_data['stock_output'].id

    def _generate_valuation_lines_data(self, partner_id, qty, debit_value, credit_value, debit_account_id, credit_account_id, description):
        res = super(StockMove, self)._generate_valuation_lines_data(partner_id=partner_id, qty=qty, debit_value=debit_value, credit_value=credit_value, debit_account_id=debit_account_id, credit_account_id=credit_account_id, description=description)
        if self.inventory_id and self.inventory_id.analytic_tag_ids:
            res['credit_line_vals']['analytic_tag_ids'] = [(6, 0, self.inventory_id.analytic_tag_ids.ids)]
            res['debit_line_vals']['analytic_tag_ids'] = [(6, 0, self.inventory_id.analytic_tag_ids.ids)]
        return res
