# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError
from datetime import datetime, date, timedelta

class MrpProductionInherit(models.Model):
    _inherit = 'mrp.production'

    
    def _get_move_raw_values(self, product_id, product_uom_qty, product_uom, operation_id=False, bom_line=False):
        res = super(MrpProductionInherit, self)._get_move_raw_values(product_id, product_uom_qty, product_uom, operation_id, bom_line)
        context = dict(self.env.context) or {}
        if context.get('replenish'):
            res['location_id'] = self.location_src_id.id
        return res