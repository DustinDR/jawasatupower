
from odoo import models, fields, api, _


class ProductCategory(models.Model):
    _inherit = 'product.category'

    stock_scrap_account = fields.Many2one(
        'account.account', domain=[('user_type_id.name','ilike','Expenses')], string="Stock Scrap Account") 
