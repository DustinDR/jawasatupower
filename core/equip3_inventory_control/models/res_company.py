# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class ResCompany(models.Model):
    _inherit = "res.company"

    pu_barcode_mobile_type = fields.Selection([
        ('int_ref', 'Internal Reference'),
        ('barcode', 'Barcode'),
        ('sh_qr_code', 'QR code'),
        ('all', 'All')
    ], default='barcode', string='Product Scan Options In Mobile (Product Usage)', translate=True)

    pu_bm_is_cont_scan = fields.Boolean(
        string='Continuously Scan? (Product Usage)')

    pu_bm_is_notify_on_success = fields.Boolean(
        string='Notification On Product Succeed? (Product Usage)')

    pu_bm_is_notify_on_fail = fields.Boolean(
        string='Notification On Product Failed? (Product Usage)')

    pu_bm_is_sound_on_success = fields.Boolean(
        string='Play Sound On Product Succeed? (Product Usage)')

    pu_bm_is_sound_on_fail = fields.Boolean(
        string='Play Sound On Product Failed? (Product Usage)')

    pu_bm_is_add_product = fields.Boolean(
        string="Is add new product in Product Usage? (Product Usage)")
