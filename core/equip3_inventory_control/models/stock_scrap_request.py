
from odoo import _, api, fields, models
from odoo.exceptions import UserError
from datetime import datetime, timedelta, date
import pytz
from pytz import timezone, UTC
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT


class StockScrapRequest(models.Model):
    _name = 'stock.scrap.request'
    _description = 'Stock Scrap Request'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']

    @api.model
    def default_pu_bm_is_cont_scan(self):
        return self.env.company.pu_bm_is_cont_scan

    pu_barcode_mobile = fields.Char(string="Mobile Barcode")
    pu_bm_is_cont_scan = fields.Char(
        string='Continuously Scan?', default=default_pu_bm_is_cont_scan, readonly=True)
    pu_barcode_mobile_type = fields.Selection([
        ('int_ref', 'Internal Reference'),
        ('barcode', 'Barcode'),
        ('sh_qr_code', 'QR code'),
        ('all', 'All')
    ], default='barcode', string='Product Scan Options In Mobile (Product Usage)', translate=True, compute='_compute_barcode_scan')
    pu_bm_is_cont_scan = fields.Boolean(
        string='Continuously Scan? (Product Usage)', compute='_compute_barcode_scan')
    pu_bm_is_notify_on_success = fields.Boolean(
        string='Notification On Product Succeed? (Product Usage)', compute='_compute_barcode_scan')
    pu_bm_is_notify_on_fail = fields.Boolean(
        string='Notification On Product Failed? (Product Usage)', compute='_compute_barcode_scan')
    pu_bm_is_sound_on_success = fields.Boolean(
        string='Play Sound On Product Succeed? (Product Usage)', compute='_compute_barcode_scan')
    pu_bm_is_sound_on_fail = fields.Boolean(
        string='Play Sound On Product Failed? (Product Usage)', compute='_compute_barcode_scan')
    pu_bm_is_add_product = fields.Boolean(
        string="Is add new product in Product Usage? (Product Usage)", compute='_compute_barcode_scan')
    is_mbs_on_product_usage = fields.Boolean(
        string="Product Usage", compute='_compute_barcode_scan') 

    def _compute_barcode_scan(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        for record in self:
            record.pu_barcode_mobile_type = IrConfigParam.get_param('pu_barcode_mobile_type', 'barcode')
            record.pu_bm_is_cont_scan = IrConfigParam.get_param('pu_bm_is_cont_scan', False)
            record.pu_bm_is_notify_on_success = IrConfigParam.get_param('pu_bm_is_notify_on_success', False)
            record.pu_bm_is_notify_on_fail = IrConfigParam.get_param('pu_bm_is_notify_on_fail', False)
            record.pu_bm_is_sound_on_success = IrConfigParam.get_param('pu_bm_is_sound_on_success', False)
            record.pu_bm_is_sound_on_fail = IrConfigParam.get_param('pu_bm_is_sound_on_fail', False)
            record.pu_bm_is_add_product = IrConfigParam.get_param('pu_bm_is_add_product', False)
            record.is_mbs_on_product_usage = IrConfigParam.get_param('is_mbs_on_product_usage', False)

    company_id = fields.Many2one('res.company', string='Company',tracking=True, readonly=True, default=lambda self: self.env.user.company_id)
    branch_id = fields.Many2one('res.branch', string='Branch',tracking=True, domain="[('company_id', '=', company_id)]", related="warehouse_id.branch_id")
    accounting = fields.Boolean(string="Acccounting", related='company_id.accounting', readonly=False)
    name = fields.Char(string="Reference", readonly=True, tracking=True)
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', tracking=True, required=True)
    state = fields.Selection([('draft', 'Draft'),
                                    ('confirmed', 'Confirmed'),
                                    ('to_approve', 'Waiting for Approval'),
                                    ('approved', 'Approved'),
                                    ('rejected', 'Rejected'),
                                    ('validated', 'Validated'),
                                    ('cancel', 'Cancelled')
                                    ], string='Status', tracking=True, default='draft')
    scrap_ids = fields.One2many('stock.scrap', 'scrap_id', tracking=True)
    scrap_request_name = fields.Char(string='Name', required=True, tracking=True)
    expire_date = fields.Datetime(string="Expiry Date")
    scrap_type = fields.Many2one('usage.type', string="Scrap Type")
    is_product_usage = fields.Boolean(string="Product Usage")
    payment_method_id = fields.Many2one('account.journal', string="Payment Method", tracking=True)
    is_payment_method = fields.Boolean(string="Payment Usage", compute="_compute_payement_method")
    schedule_date = fields.Datetime(string='Scheduled Date', tracking=True)
    responsible_id = fields.Many2one('res.users', string="Responsible", tracking=True)
    analytic_tag_ids = fields.Many2many("account.analytic.tag",
                                        "scrap_request_analytic_tag_rel",
                                        "stock_scrap_request",
                                        "account_analytic_tag_id",
                                        "Analytic Groups",
                                        default=lambda self: self.env.user.analytic_tag_ids.ids)
    is_group_analytic_tags = fields.Boolean("Is Analytic Group", 
                                            compute="_compute_is_group_analytic_tags",
                                            default=lambda self: bool(self.env['ir.config_parameter'].sudo().get_param('group_analytic_tags', False)))
    scrap_state = fields.Selection(related='state', tracking=False)
    scrap_state_1 = fields.Selection(related='state', tracking=False)
    is_product_usage_approval = fields.Boolean(string="Product Usage Approval", compute='_get_usage_approve_button_from_config')
    approval_matrix_id = fields.Many2one('stock.scrap.approval.matrix', string="Approval Matrix", compute='_get_approval_matrix')
    approved_matrix_ids = fields.One2many('stock.scrap.approval.matrix.line', 'scap_request_id', compute="_compute_approving_matrix_lines_scrap", store=True, string="Approved Matrix")
    approval_matrix_line_id = fields.Many2one('stock.scrap.approval.matrix.line', string='Material Approval Matrix Line', compute='_get_approve_button', store=False)
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button', store=False)

    @api.onchange('warehouse_id')
    def _onchange_warehouse(self):
        print('chk----')
        for line in self.scrap_ids:
            if self.warehouse_id.id != line.location_id.warehouse_id.id:
                print('cht')
                if self.state != 'validated':
                    self.write({'scrap_ids': [(3, line.id)]})


    @api.onchange('pu_barcode_mobile')
    def _onchange_pu_barcode_mobile(self):

        if self.pu_barcode_mobile in ['', "", False, None]:
            return

        CODE_SOUND_SUCCESS = ""
        CODE_SOUND_FAIL = ""
        if self.pu_bm_is_sound_on_success:
            CODE_SOUND_SUCCESS = "SH_BARCODE_MOBILE_SUCCESS_"

        if self.pu_bm_is_sound_on_fail:
            CODE_SOUND_FAIL = "SH_BARCODE_MOBILE_FAIL_"

        if self and self.state != 'draft':
            selections = self.fields_get()['state']['selection']
            value = next((v[1] for v in selections if v[0]
                          == self.state), self.state)
            if self.pu_bm_is_notify_on_fail:
                message = _(CODE_SOUND_FAIL +
                            'You can not scan item in %s state.') % (value)
                self.env['bus.bus'].sendone(
                    (self._cr.dbname, 'res.partner', self.env.user.partner_id.id),
                    {'type': 'simple_notification', 'title': _('Failed'), 'message': message, 'sticky': False, 'warning': True})
            return
        elif self:
            search_pul = False
            domain = []

            if self.pu_barcode_mobile_type == 'barcode':
                search_pul = self.scrap_ids.filtered(
                    lambda ml: ml.product_id.barcode == self.pu_barcode_mobile)
                domain = [("barcode", "=", self.pu_barcode_mobile)]

            elif self.pu_barcode_mobile_type == 'int_ref':
                search_pul = self.scrap_ids.filtered(
                    lambda ml: ml.product_id.default_code == self.pu_barcode_mobile)
                domain = [("default_code", "=", self.pu_barcode_mobile)]

            elif self.pu_barcode_mobile_type == 'sh_qr_code':
                search_pul = self.scrap_ids.filtered(
                    lambda ml: ml.product_id.sh_qr_code == self.pu_barcode_mobile)
                domain = [("sh_qr_code", "=", self.pu_barcode_mobile)]

            elif self.pu_barcode_mobile_type == 'all':
                search_pul = self.scrap_ids.filtered(
                    lambda ml: ml.product_id.barcode == self.pu_barcode_mobile or ml.product_id.default_code == self.pu_barcode_mobile)
                domain = ["|", "|",
                          ("default_code", "=", self.pu_barcode_mobile),
                          ("barcode", "=", self.pu_barcode_mobile),
                          ("sh_qr_code", "=", self.pu_barcode_mobile),
                          ]

            if search_pul:
                for line in search_pul:
                    line.write({'scrap_qty': line.scrap_qty + 1})
                    if self.pu_bm_is_notify_on_success:
                        message = _(CODE_SOUND_SUCCESS + 'Product: %s Qty: %s') % (
                            line.product_id.name, line.scrap_qty)
                        self.env['bus.bus'].sendone(
                            (self._cr.dbname, 'res.partner',
                             self.env.user.partner_id.id),
                            {'type': 'simple_notification', 'title': _('Succeed'), 'message': message, 'sticky': False, 'warning': False})
            elif self.state == 'draft':
                if self.pu_bm_is_add_product:

                    search_product = self.env["product.product"].search(
                        domain, limit=1)
                    addtional_ids = False
                    if self.warehouse_id:
                        location_ids = []
                        location_obj = self.env['stock.location']
                        store_location_id = self.warehouse_id.view_location_id.id
                        addtional_ids = location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')], order='id')
                    if search_product:
                        product_vals = {
                                        "name": search_product.name,
                                        "product_id": search_product.id,
                                        "product_uom_id": search_product.uom_id.id,
                                        "scrap_qty": 1,
                                        "location_id": addtional_ids and addtional_ids[0].id or False,
                                    }
                        self.scrap_ids = [(0, 0, product_vals)]
                        if self.pu_bm_is_notify_on_success:
                            message = _(CODE_SOUND_SUCCESS + 'Product: %s Qty: %s') % (
                                search_product.name, 1)
                            self.env['bus.bus'].sendone(
                                (self._cr.dbname, 'res.partner',
                                 self.env.user.partner_id.id),
                                {'type': 'simple_notification', 'title': _('Succeed'), 'message': message, 'sticky': False, 'warning': False})
                        return

                    else:
                        if self.pu_bm_is_notify_on_fail:
                            message = _(
                                CODE_SOUND_FAIL + 'Scanned Internal Reference/Barcode not exist in any product!')
                            self.env['bus.bus'].sendone(
                                (self._cr.dbname, 'res.partner',
                                 self.env.user.partner_id.id),
                                {'type': 'simple_notification', 'title': _('Failed'), 'message': message, 'sticky': False, 'warning': True})

                        return

                else:
                    if self.pu_bm_is_notify_on_fail:
                        message = _(
                            CODE_SOUND_FAIL + 'Scanned Internal Reference/Barcode not exist in any product!')
                        self.env['bus.bus'].sendone(
                            (self._cr.dbname, 'res.partner',
                             self.env.user.partner_id.id),
                            {'type': 'simple_notification', 'title': _('Failed'), 'message': message, 'sticky': False, 'warning': True})

                    return

            else:
                if self.pu_bm_is_notify_on_fail:
                    message = _(
                        CODE_SOUND_FAIL + 'Scanned Internal Reference/Barcode not exist in any product!')
                    self.env['bus.bus'].sendone(
                        (self._cr.dbname, 'res.partner',
                         self.env.user.partner_id.id),
                        {'type': 'simple_notification', 'title': _('Failed'), 'message': message, 'sticky': False, 'warning': True})

                return
        else:
            # failed message here
            if self.pu_bm_is_notify_on_fail:
                message = _(
                    CODE_SOUND_FAIL + 'Scanned Internal Reference/Barcode not exist in any product!')
                self.env['bus.bus'].sendone(
                    (self._cr.dbname, 'res.partner', self.env.user.partner_id.id),
                    {'type': 'simple_notification', 'title': _('Failed'), 'message': message, 'sticky': False, 'warning': True})

            return

    def _get_usage_approve_button_from_config(self):
        is_product_usage_approval = self.env['ir.config_parameter'].sudo().get_param('is_product_usage_approval')
        for record in self:
            record.is_product_usage_approval = is_product_usage_approval

    def _get_approve_button(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False

    @api.depends('warehouse_id')
    def _get_approval_matrix(self):
        for record in self:
            matrix_id = self.env['stock.scrap.approval.matrix'].search([('warehouse_id', '=', record.warehouse_id.id)], limit=1)
            record.approval_matrix_id = matrix_id and matrix_id.id or False

    @api.depends('approval_matrix_id')
    def _compute_approving_matrix_lines_scrap(self):
        data = [(5, 0, 0)]
        for record in self:
            counter = 1
            record.approved_matrix_ids = []
            for line in record.approval_matrix_id.sc_approval_matrix_line_ids:
                data.append((0, 0, {
                    'sequence' : counter,
                    'user_ids' : [(6, 0, line.user_ids.ids)],
                    'minimum_approver' : line.minimum_approver,
                }))
                counter += 1
            record.approved_matrix_ids = data

    @api.onchange('warehouse_id')
    def _onchange_warehouse_id_boolean(self):
        self._get_usage_approve_button_from_config()
        self._compute_barcode_scan()

    def scrap_request_for_approving(self):
        for record in self:
            record.write({'state': 'to_approve'})

    def scrap_approving(self):
        for record in self:
            user = self.env.user
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_ids.ids and \
                    user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    utc_datetime = datetime.now()
                    local_timezone = pytz.timezone(self.env.user.tz)
                    local_datetime = utc_datetime.replace(tzinfo=pytz.utc)
                    local_datetime = local_datetime.astimezone(local_timezone).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                    if name != '':
                        name += "\n • %s: Approved - %s" % (self.env.user.name, local_datetime)
                    else:
                        name += "• %s: Approved - %s" % (self.env.user.name, local_datetime)

                    approval_matrix_line_id.write({ 
                        'last_approved': self.env.user.id, 'state_char': name, 
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
                        # next_approval_matrix_line_id = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
                        # if next_approval_matrix_line_id and len(next_approval_matrix_line_id[0].approver) > 1:
                        #     pass
            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r:r.approved)):
                record.write({'state': 'approved'})

    def scrap_reject(self):
        for record in self:
            return {
                    'type': 'ir.actions.act_window',
                    'name': 'Reject Product Usage Matrix',
                    'res_model': 'stock.scrap.request.matrix.reject',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'new',
                }
    
    def set_to_draft_scp(self):
        for record in self:
            record.write({'state': 'draft'})

    @api.depends('scrap_type.usage_type', 
                'scrap_type.income_account_id', 
                'scrap_type', 'state',
                'scrap_ids', 'scrap_ids.sale_price')
    def _compute_payement_method(self):
        for record in self:
            if record.scrap_type.usage_type == 'scrap' and \
               record.scrap_type.income_account_id and \
               record.scrap_ids and \
               record.accounting and \
               all(line.sale_price > 0 for line in record.scrap_ids):
                record.is_payment_method = True
            else:
                record.is_payment_method = False

    def action_request_confirm(self):
        for record in self:
            record.write({'state': 'confirmed'})
            # for line in record.scrap_ids:
            #     line.action_validate()
            #     line.move_id.reference = record.name
            #     for move_line_id in line.move_id.move_line_ids:
            #         move_line_id.write({
            #             'reference': record.name,
            #             'scrap_sale_price': line.sale_price,
            #         })

    def action_request_validated(self):
        for record in self:
            context = dict(self.env.context) or {}

            analytic_tag_ids = bool(self.env['ir.config_parameter'].sudo().get_param('group_analytic_tags', False)) and \
                               [(6, 0, record.analytic_tag_ids.ids)] or \
                               [(6, 0, [])]

            for scrap_id in record.scrap_ids:
                scrap_id.action_validate()
                scrap_id.move_id.reference = record.name
                for move_line_id in scrap_id.move_id.move_line_ids:
                    move_line_id.write({
                        'reference': record.name,
                        'scrap_sale_price': scrap_id.sale_price,
                    })
                for move_line in scrap_id.move_id.move_line_ids:
                    # move_line.write({
                    #     'reference': record.name,
                    #     'scrap_sale_price': scrap_id.sale_price,
                    # })
                    value = sum(scrap_id.move_id.stock_valuation_layer_ids.mapped('unit_cost')) * move_line.qty_done
                    scrap_journal_id = self.env['account.journal'].search([('name', 'ilike', 'Miscellaneous'), ('type', '=', 'general'), ('company_id', '=', record.company_id.id)], limit=1)
                    if not record.accounting and value > 0:
                        credit_vals = {
                            'name' : move_line.origin,
                            'debit' : 0,
                            'credit' : value,
                            'date' : datetime.today(),
                            'account_id' : scrap_id.product_id.categ_id.property_stock_valuation_account_id.id,
                            'analytic_tag_ids': analytic_tag_ids
                        }
                        debit_vals = {
                            'name' : move_line.origin,
                            'credit' : 0,
                            'debit' : value,
                            'date' : datetime.today(),
                            'account_id' : scrap_id.product_id.categ_id.stock_scrap_account.id,
                            'analytic_tag_ids': analytic_tag_ids
                        }
                        vals = {
                            'ref' : move_line.reference,
                            'date' : datetime.today(),
                            'journal_id' : scrap_journal_id.id,
                            'move_type': 'entry',
                            'branch_id' : record.branch_id.id,
                            'line_ids' : [(0, 0, credit_vals), (0, 0, debit_vals)]
                        }
                        move_id = self.env['account.move'].create(vals)
                        move_id.action_post()
                    elif record.accounting and value > 0:
                        if move_line.location_dest_id.usage != 'internal':
                            journal_id = record.payment_method_id
                            if record.scrap_type.usage_type == "usage" or \
                                (record.scrap_type.usage_type == "scrap" and \
                                scrap_id.sale_price == 0):
                                credit_vals = {
                                    'name' : move_line.origin,
                                    'debit' : 0,
                                    'credit' : value,
                                    'date' : datetime.today(),
                                    'account_id' : scrap_id.product_id.categ_id.property_stock_valuation_account_id.id,
                                    'analytic_tag_ids': analytic_tag_ids
                                }
                                debit_vals = {
                                    'name' : move_line.origin,
                                    'credit' : 0,
                                    'debit' : value,
                                    'date' : datetime.today(),
                                    'account_id' : record.scrap_type.account_id.id,
                                    'analytic_tag_ids': analytic_tag_ids
                                }
                                vals = {
                                    'ref' : move_line.reference,
                                    'date' : datetime.today(),
                                    'journal_id' : scrap_journal_id.id,
                                    'move_type': 'entry',
                                    'branch_id' : record.branch_id.id,
                                    'line_ids' : [(0, 0, credit_vals), (0, 0, debit_vals)]
                                }
                                move_id = self.env['account.move'].create(vals)
                                move_id.action_post()
                            if record.scrap_type.usage_type == "scrap" and \
                                scrap_id.sale_price > 0 and record.payment_method_id:
                                credit_vals = {
                                    'name' : move_line.origin,
                                    'debit' : 0,
                                    'credit' : value,
                                    'date' : datetime.today(),
                                    'account_id' : scrap_id.product_id.categ_id.property_stock_valuation_account_id.id,
                                    'analytic_tag_ids': analytic_tag_ids
                                }
                                debit_vals = {
                                    'name' : move_line.origin,
                                    'credit' : 0,
                                    'debit' : value,
                                    'date' : datetime.today(),
                                    'account_id' : record.scrap_type.account_id.id,
                                    'analytic_tag_ids': analytic_tag_ids
                                }
                                vals = {
                                    'ref' : move_line.reference,
                                    'date' : datetime.today(),
                                    'journal_id' : scrap_journal_id.id,
                                    'move_type': 'entry',
                                    'branch_id' : record.branch_id.id,
                                    'line_ids' : [(0, 0, credit_vals), (0, 0, debit_vals)]
                                }
                                move_id = self.env['account.move'].create(vals)
                                move_id.action_post()
                                credit_vals = {
                                    'name' : move_line.origin,
                                    'debit' : 0,
                                    'credit' : move_line.scrap_sale_price,
                                    'date' : datetime.today(),
                                    'account_id' : record.scrap_type.income_account_id.id,
                                    'analytic_tag_ids': analytic_tag_ids
                                }
                                debit_vals = {
                                    'name' : move_line.origin,
                                    'credit' : 0,
                                    'debit' : move_line.scrap_sale_price,
                                    'date' : datetime.today(),
                                    'account_id' : journal_id.payment_debit_account_id.id,
                                    'analytic_tag_ids': analytic_tag_ids
                                }
                                vals = {
                                    'ref' : move_line.reference,
                                    'date' : datetime.today(),
                                    'journal_id' : journal_id.id,
                                    'move_type': 'entry',
                                    'branch_id' : record.branch_id.id,
                                    'line_ids' : [(0, 0, credit_vals), (0, 0, debit_vals)]
                                }
                                move_id = self.env['account.move'].create(vals)
                                move_id.action_post()

            record.write({'state': 'validated'})

    def action_cancel(self):
        for record in self:
            if record.state == 'validated':
                scrap_name = record.scrap_ids.mapped('name')
                journal_entries_ids = self.env['account.move.line'].search([("name", 'in', scrap_name)]).mapped('move_id')
                journal_entries_ids.button_draft()
                journal_entries_ids.button_cancel()
                for scrap_id in record.scrap_ids:
                    new_move_id = scrap_id.move_id.copy({
                        'location_id': scrap_id.move_id.location_dest_id.id, 
                        'location_dest_id': scrap_id.move_id.location_id.id, 
                        'product_uom_qty': scrap_id.move_id.product_uom_qty,
                        'state': 'done'})
                    for move_line_id in scrap_id.move_id.move_line_ids:
                        new_move_line_id = move_line_id.copy({
                            'move_id': new_move_id.id,
                            'state': 'done',
                            'qty_done': move_line_id.qty_done,
                            'location_id': move_line_id.location_dest_id.id, 
                            'location_dest_id': move_line_id.location_id.id
                        })
            record.write({'state': 'cancel'})

    @api.model
    def _cron_auto_cancel_product_usage(self):
        today = datetime.now()
        product_usage_ids = self.search([('state', '=', 'confirmed'), ('expire_date', '<', today)])
        for record in product_usage_ids:
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = base_url + '/web#id='+ str(record.id) + '&view_type=form&model=stock.scrap.request'
            template_id = self.env.ref('equip3_inventory_control.email_template_product_usage_scrap_auto_cancel')
            ctx = {
                    'email_from': self.env.user.company_id.email,
                    'url': url,
                    'is_lot': record.scrap_ids.filtered(lambda r:r.lot_id),
                    'is_package': record.scrap_ids.filtered(lambda r:r.package_id),
                    'is_owner': record.scrap_ids.filtered(lambda r:r.owner_id),
                }
            template_id.with_context(ctx).send_mail(record.id, True)
            template_id = self.env["ir.model.data"].xmlid_to_object(
                "equip3_inventory_control.email_template_product_usage_scrap_auto_cancel"
            )

            body_html = self.env['mail.render.mixin'].with_context(ctx)._render_template(
                        template_id.body_html, 'stock.scrap.request', record.ids, post_process=True)[record.id]
            message_id = (
                self.env["mail.message"]
                .sudo()
                .create(
                    {
                        "subject": "Product Usage Expire",
                        "body": body_html,
                        "model": "stock.scrap.request",
                        "res_id": record.id,
                        "message_type": "notification",
                        "partner_ids": [
                            (
                                6,
                                0,
                                record.responsible_id.partner_id.ids,
                            )
                        ],
                    }
                )
            )
            notif_create_values = {
                "mail_message_id": message_id.id,
                "res_partner_id": record.responsible_id.partner_id.id,
                "notification_type": "inbox",
                "notification_status": "sent",
            }
            self.env["mail.notification"].sudo().create(notif_create_values)
            record.write({'state': 'cancel'})

    @api.onchange('schedule_date')
    def onchange_schedule_date(self):
        if self.schedule_date:
            IrConfigParam = self.env['ir.config_parameter'].sudo()
            expiry_days_select = IrConfigParam.get_param('expiry_days_select', 'before')
            product_expiry = IrConfigParam.get_param('product_expiry', 0)
            if expiry_days_select == 'before':
                self.expire_date = self.schedule_date - timedelta(days=int(product_expiry))
            else:
                self.expire_date = self.schedule_date + timedelta(days=int(product_expiry))

    @api.model
    def create(self, vals):
        if vals.get('is_product_usage'):
            vals['name'] = self.env['ir.sequence'].next_by_code('stock.product.usage') 
        else:
            vals['name'] = self.env['ir.sequence'].next_by_code('stock.scrap.request')
        res = super(StockScrapRequest, self).create(vals)
        return res

    @api.model
    def default_get(self, fields):
        res = super(StockScrapRequest, self).default_get(fields)
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        scrap_expiry = int(IrConfigParam.get_param('product_expiry', 0))
        res['expire_date'] = datetime.now() + timedelta(days=scrap_expiry)
        return res

    def _compute_is_group_analytic_tags(self):
        for rec in self:
            rec.is_group_analytic_tags = bool(self.env['ir.config_parameter'].sudo().get_param('group_analytic_tags', False))


class StockScrap(models.Model):
    _inherit = 'stock.scrap'

    product_uom_id = fields.Many2one(domain="[]")
    scrap_id = fields.Many2one('stock.scrap.request', string="Scrap")
    filter_location_ids = fields.Many2many('stock.location', compute='_get_stock_locations', store=False)
    filter_product_ids = fields.Many2many('product.product', compute='_get_product_by_locations', store=False)
    location_id = fields.Many2one('stock.location', default=False)
    sale_price = fields.Float(string="Sale Price")
    product_usage_state = fields.Selection(related="scrap_id.state")
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Groups', related="scrap_id.analytic_tag_ids")
    filter_lot_ids = fields.Many2many('stock.production.lot', compute='_get_lot_serial', store=False)
    filter_package_ids = fields.Many2many('stock.quant.package', compute='_get_lot_serial', store=False)
    filter_owner_ids = fields.Many2many('res.partner', compute='_get_lot_serial', store=False)
    scrap_qty = fields.Float(default=0)
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', tracking=True, related="scrap_id.warehouse_id")


    @api.onchange('location_id')
    def _onchange_location(self):
        return {'domain': {'location_id': [('warehouse_id', '=', self.scrap_id.warehouse_id.id)]}}

    @api.onchange('product_id')
    def onchange_product_id(self):
        self.product_uom_id = False
        if self.product_id:
            self.product_uom_id = self.product_id.uom_id.id
        self._get_stock_locations()
        self._get_product_by_locations()

    @api.onchange('company_id')
    def _onchange_company_id(self):
        res = super(StockScrap, self)._onchange_company_id()
        if self.scrap_id.warehouse_id:
            location_ids = []
            location_obj = self.env['stock.location']
            store_location_id = self.scrap_id.warehouse_id.view_location_id.id
            addtional_ids = location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')], order='id')
            if addtional_ids:
                self.location_id = addtional_ids[0].id
        return res

    def _get_stock_locations(self):
        for record in self:
            location_ids = []
            location_obj = self.env['stock.location']
            store_location_id = record.scrap_id.warehouse_id.view_location_id.id
            addtional_ids = location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')], order='id')
            for location in addtional_ids:
                if location.location_id.id not in addtional_ids.ids:
                    location_ids.append(location.id)
            record.filter_location_ids = [(6, 0, location_ids)]
            # if not self.scrap_id.warehouse_id:
            #     record.location_id = False
            if not self.product_id:
                record.location_id = False
            # if self.location_id.id not in location_ids:
            #     record.location_id = False

    @api.depends('location_id', 'product_id')
    def _get_lot_serial(self):
        for record in self:
            quant_ids = self.env['stock.quant'].search([('product_id', '=', record.product_id.id), ('location_id', '=', record.location_id.id)])
            lot_serial = quant_ids.mapped('lot_id').ids
            package_id = quant_ids.mapped('package_id').ids
            owner_id = quant_ids.mapped('owner_id').ids
            record.filter_lot_ids = [(6, 0, lot_serial)]
            record.filter_package_ids = [(6, 0, package_id)]
            record.filter_owner_ids = [(6, 0, owner_id)]

    @api.depends('location_id')
    def _get_product_by_locations(self):
        for record in self:
            product_ids = self.env['stock.quant'].search([('location_id', '=', record.location_id.id)]).mapped('product_id')
            record.filter_product_ids = [(6, 0, product_ids.ids)]

class StockScrapApprovalMatrixLine(models.Model):
    _inherit = "stock.scrap.approval.matrix.line"

    scap_request_id = fields.Many2one('stock.scrap.request', string="Product Usage")
