# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
from odoo import _, api, fields, models
from datetime import date, timedelta
from json import dumps
from odoo.exceptions import UserError, ValidationError

import json
from datetime import datetime, timedelta
from pytz import timezone


class HrExpense(models.Model):
    _inherit = 'hr.expense'

    def unlink(self):
        for expense in self:
            if expense.state in ['done', 'approved']:
                raise UserError(_('You cannot delete a Expense Report to Approve.'))
        return super(HrExpense, self).unlink()


class HrExpenseSheet(models.Model):
    _inherit = 'hr.expense.sheet'

    def unlink(self):
        for expense in self:
            if expense.state in ['done', 'submit', 'approve']:
                raise UserError(_('You cannot delete a Expense.'))
        return super(HrExpenseSheet, self).unlink()

    @api.model
    def create(self, vals):
        sequence_no = self.env['ir.sequence'].next_by_code('hr.expense.sheet')
        vals.update({'seq_name': sequence_no})
        result = super(HrExpenseSheet, self).create(vals)
        return result

    def name_get(self):
        return [(exp.id, exp.seq_name) for exp in self]

    seq_name = fields.Char('Name', default='New', copy=False)
    name = fields.Char('Expense Summary', required=True, tracking=True)
    expense_advance = fields.Boolean(string='Expense Advance', default=False)
    cash_advance_number_ids = fields.Many2many('vendor.deposit', string='Cash Advance Number',
                                               domain=lambda
                                                   self: "[('employee_id', '=', employee_id), ('state', '=', 'post'), ('is_cash_advance', '=', 'True')]")
    expense_payment_widget = fields.Text(string="Expense Payment Widget", compute="_get_payment_info_JSON")
    cash_advance_amount = fields.Monetary(string="Cash Advance Amount", tracking=True)
    exp_difference = fields.Integer('Total difference')
    is_hide = fields.Boolean(string='Reconcile Button Visible', default=False)
    line_ids = fields.Many2many('account.move.line', string='Account Line', compute='_compute_acc_line')
    register_button_hide = fields.Boolean(string='Register Button Visible', default=False,
                                          compute='_compute_register_button_hide')
    expense_approver_user_ids = fields.One2many('expense.approver.user', 'expense_id', string='Approver')
    approvers_ids = fields.Many2many('res.users', 'exp_approvers_rel', string='Approvers List')
    approved_user_ids = fields.Many2many('res.users', string='Approved by User')
    is_approver = fields.Boolean(string="Is Approver", compute='_compute_can_approve')
    approved_user_text = fields.Text(string="Approved User", tracking=True)
    approved_user = fields.Text(string="Approved User", tracking=True)
    job_id = fields.Many2one('hr.job', related='employee_id.job_id')
    department_id = fields.Many2one('hr.department', related='employee_id.department_id')
    amount_sum = fields.Float("Sum of total from line item", )
    feedback_parent = fields.Text(string='Parent Feedback')
    expense_cycle = fields.Char(string='Expense Cycle')
    reimbursement_date = fields.Date(string='Reimbursement Date')
    expense_reminder_before_days = fields.Integer(copute='compute_reminder_details')
    expense_choose_color_reminder = fields.Selection(
        [('red', 'Red'), ('green', 'Green'), ('blue', 'Blue'), ('purple', 'Purple'), ('yellow', 'Yellow'),
         ('white', 'White')],
        compute='compute_reminder_details')
    is_self_service = fields.Boolean(compute='_compute_is_self_service')
    is_read_only_manager = fields.Boolean(compute='_compute_is_read_only_manager')
    employee_domain_ids = fields.Many2many('hr.employee',compute='_compute_employee_domain')
    
    
    @api.depends('employee_id')
    def _compute_employee_domain(self):
        for record in self:
            if self.env.user.has_group('equip3_hr_employee_access_right_setting.group_team_approver') and not self.env.user.has_group('hr_expense.group_hr_expense_user'):
                my_employee = self.env['hr.employee'].sudo().search([('user_id','=',self.env.user.id)])
                employee_ids = []
                if my_employee:
                    for child_record in my_employee.child_ids:
                        employee_ids.append(child_record.id)
                        child_record._get_amployee_hierarchy(employee_ids,child_record.child_ids,my_employee.id)
                record.employee_domain_ids = employee_ids
            else:
                employee = self.env['hr.employee'].sudo().search([])
                employee_ids = []
                if employee:
                    for record_employee in employee:
                        employee_ids.append(record_employee.id)
                record.employee_domain_ids = employee_ids
            
    
    @api.depends('employee_id')
    def _compute_is_self_service(self):
        for record in self:
            if  self.env.user.has_group('hr_expense.group_hr_expense_team_approver') and not self.env.user.has_group('equip3_hr_employee_access_right_setting.group_team_approver'):
                record.is_self_service =  True
            else:
                record.is_self_service =  False
                
    @api.depends('employee_id')
    def _compute_is_read_only_manager(self):
        for record in self:
            if  self.env.user.has_group('hr_expense.group_hr_expense_team_approver') and not self.env.user.has_group('hr_expense.group_hr_expense_user') or self.env.user.has_group('equip3_hr_employee_access_right_setting.group_team_approver') and not self.env.user.has_group('hr_expense.group_hr_expense_user'):
                record.is_read_only_manager =  True
            else:
                record.is_read_only_manager =  False
            

    @api.onchange('expense_line_ids', 'state')
    def onchange_amount_sum(self):
        for rec in self:
            total_amt_sum = 0
            for line in rec.expense_line_ids:
                total_amt_sum += line.total_amount
        rec.amount_sum = total_amt_sum
        self.onchange_approver_user()
        self.get_expense_date()

    @api.onchange('employee_id', 'expense_line_ids', 'amount_sum')
    def onchange_approver_user(self):
        for expense in self:
            if expense.expense_approver_user_ids:
                remove = []
                for line in expense.expense_approver_user_ids:
                    remove.append((2, line.id))
                expense.expense_approver_user_ids = remove
            setting = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_expense_extend.expense_type_approval')
            if setting == 'employee_hierarchy':
                expense.expense_approver_user_ids = self.expense_emp_by_hierarchy(expense)
                # self.emp_hierarchy_approver()
                self.app_list_expense_emp_by_hierarchy()
            if setting == 'approval_matrix':
                self.expense_approval_by_matrix(expense)

    def expense_emp_by_hierarchy(self, expense):
        approval_ids = []
        seq = 1
        data = 0
        line = self.get_manager(expense, expense.employee_id, data, approval_ids, seq)
        return line

    def get_manager(self, expense, employee_manager, data, approval_ids, seq):
        setting_level = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_expense_extend.expense_level')
        if not setting_level:
            raise ValidationError("Level not set")
        if not employee_manager['parent_id']['user_id']:
            return approval_ids
        while data < int(setting_level):
            approval_ids.append(
                (0, 0, {'user_ids': [(4, employee_manager['parent_id']['user_id']['id'])]}))
            data += 1
            seq += 1
            if employee_manager['parent_id']['user_id']['id']:
                self.get_manager(expense, employee_manager['parent_id'], data, approval_ids, seq)
                break

        return approval_ids

    def app_list_expense_emp_by_hierarchy(self):
        for expense in self:
            app_list = []
            for line in expense.expense_approver_user_ids:
                app_list.append(line.user_ids.id)
            expense.approvers_ids = app_list

    def expense_approval_by_matrix(self, expense):
        app_list = []
        approval_matrix = self.env['hr.expense.approval.matrix'].search(
            [('apply_to', '=', 'by_employee'), ('maximum_amount', '>=', expense.amount_sum),
             ('minimum_amount', '<=', expense.amount_sum)])
        matrix = approval_matrix.filtered(lambda line: expense.employee_id.id in line.employee_ids.ids)
        if matrix:
            data_approvers = []
            for line in matrix[0].approval_matrix_ids:
                data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                              'user_ids': [(6, 0, line.approvers.ids)]}))
                for approvers in line.approvers:
                    app_list.append(approvers.id)
            expense.approvers_ids = app_list
            expense.expense_approver_user_ids = data_approvers

        if not matrix:
            data_approvers = []
            approval_matrix = self.env['hr.expense.approval.matrix'].search(
                [('apply_to', '=', 'by_job_position'), ('maximum_amount', '>=', expense.amount_sum),
                 ('minimum_amount', '<=', expense.amount_sum)])
            matrix = approval_matrix.filtered(lambda line: expense.job_id.id in line.job_ids.ids)
            if matrix:
                for line in matrix[0].approval_matrix_ids:
                    data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                                  'user_ids': [(6, 0, line.approvers.ids)]}))
                    for approvers in line.approvers:
                        app_list.append(approvers.id)
                expense.approvers_ids = app_list
                expense.expense_approver_user_ids = data_approvers
            if not matrix:
                data_approvers = []
                approval_matrix = self.env['hr.expense.approval.matrix'].search(
                    [('apply_to', '=', 'by_department'), ('maximum_amount', '>=', expense.amount_sum),
                     ('minimum_amount', '<=', expense.amount_sum)])
                matrix = approval_matrix.filtered(lambda line: expense.department_id.id in line.deparment_ids.ids)
                if matrix:
                    for line in matrix[0].approval_matrix_ids:
                        data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                                      'user_ids': [(6, 0, line.approvers.ids)]}))
                        for approvers in line.approvers:
                            app_list.append(approvers.id)
                    expense.approvers_ids = app_list
                    expense.expense_approver_user_ids = data_approvers

    @api.depends('state', 'employee_id', 'total_amount')
    def _compute_can_approve(self):
        for expense in self:
            if expense.approvers_ids:
                setting = self.env['ir.config_parameter'].sudo().get_param(
                    'equip3_hr_expense_extend.expense_type_approval')
                setting_level = self.env['ir.config_parameter'].sudo().get_param(
                    'equip3_hr_expense_extend.expense_level')
                app_level = int(setting_level)
                current_user = expense.env.user
                if setting == 'employee_hierarchy':
                    matrix_line = sorted(expense.expense_approver_user_ids.filtered(lambda r: r.is_approve == True))
                    app = len(matrix_line)
                    a = len(expense.expense_approver_user_ids)
                    if app < app_level and app < a:
                        if current_user in expense.expense_approver_user_ids[app].user_ids:
                            expense.is_approver = True
                        else:
                            expense.is_approver = False
                    else:
                        expense.is_approver = False
                elif setting == 'approval_matrix':
                    matrix_line = sorted(expense.expense_approver_user_ids.filtered(lambda r: r.is_approve == True))
                    app = len(matrix_line)
                    a = len(expense.expense_approver_user_ids)
                    if app < a:
                        for line in expense.expense_approver_user_ids[app]:
                            if current_user in line.user_ids:
                                expense.is_approver = True
                            else:
                                expense.is_approver = False
                    else:
                        expense.is_approver = False

                else:
                    expense.is_approver = False
            else:
                expense.is_approver = False

    def approve_expense_sheets(self):
        if not self.user_has_groups('hr_expense.group_hr_expense_team_approver'):
            raise UserError(_("Only Managers and HR Officers can approve expenses"))
        elif not self.user_has_groups('hr_expense.group_hr_expense_manager'):
            current_managers = self.employee_id.sudo().expense_manager_id | self.employee_id.sudo().parent_id.user_id | self.employee_id.sudo().department_id.manager_id.user_id

            if self.employee_id.user_id == self.env.user:
                raise UserError(_("You cannot approve your own expenses"))

            if not self.env.user in current_managers and not self.user_has_groups(
                    'hr_expense.group_hr_expense_user') and self.employee_id.expense_manager_id != self.env.user:
                raise UserError(_("You can only approve your department expenses"))

        responsible_id = self.user_id.id or self.env.user.id
        notification = {
            'type': 'ir.actions.client',
            'tag': 'display_notification',
            'params': {
                'title': _('There are no expense reports to approve.'),
                'type': 'warning',
                'sticky': False,  # True/False will display for few seconds if false
            },
        }
        sheet_to_approve = self.filtered(lambda s: s.state in ['submit', 'draft'])
        if sheet_to_approve:
            notification['params'].update({
                'title': _('The expense reports were successfully approved.'),
                'type': 'success',
                'next': {'type': 'ir.actions.act_window_close'},
            })
            # sheet_to_approve.write({'state': 'approve', 'user_id': responsible_id})
            self.approve_expense_sheets_by_list()
        self.activity_update()
        return notification

    def approve_expense_sheets_by_list(self):
        for record in self:
            current_user = self.env.uid
            setting = self.env['ir.config_parameter'].sudo().get_param(
                'equip3_hr_expense_extend.expense_type_approval')
            now = datetime.now(timezone(self.env.user.tz))
            dateformat = f"{now.day}/{now.month}/{now.year} {now.hour}:{now.minute}:{now.second}"
            if setting == 'employee_hierarchy':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for user in record.expense_approver_user_ids:
                            if current_user == user.user_ids.id:
                                user.is_approve = True
                                user.timestamp = fields.Datetime.now()
                                user.approver_state = 'approved'
                                string_approval = []
                                if user.approval_status:
                                    string_approval.append(f"{self.env.user.name}:Approved")
                                    user.approval_status = "\n".join(string_approval)
                                    string_timestammp = [user.approved_time]
                                    string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                    user.approved_time = "\n".join(string_timestammp)
                                    if record.feedback_parent:
                                        feedback_list = [user.feedback,
                                                         f"{self.env.user.name}:{record.feedback_parent}"]
                                        final_feedback = "\n".join(feedback_list)
                                        user.feedback = f"{final_feedback}"
                                    elif user.feedback and not record.feedback_parent:
                                        user.feedback = user.feedback
                                    else:
                                        user.feedback = ""
                                else:
                                    user.approval_status = f"{self.env.user.name}:Approved"
                                    user.approved_time = f"{self.env.user.name}:{dateformat}"
                                    if record.feedback_parent:
                                        user.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                    else:
                                        user.feedback = ""
                                record.approved_user_ids = [(4, current_user)]
                        matrix_line = sorted(record.expense_approver_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            record.write({'state': 'approve', 'user_id': self.user_id.id or self.env.user.id})
                            record.approved_mail()
                        else:
                            record.approved_user = self.env.user.name + ' ' + 'has been approved the Request!'
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved'
                    ))
            elif setting == 'approval_matrix':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for line in record.expense_approver_user_ids:
                            for user in line.user_ids:
                                if current_user == user.user_ids.id:
                                    line.timestamp = fields.Datetime.now()
                                    record.approved_user_ids = [(4, current_user)]
                                    var = len(line.approved_employee_ids) + 1
                                    if line.minimum_approver <= var:
                                        line.approver_state = 'approved'
                                        string_approval = []
                                        string_approval.append(line.approval_status)
                                        if line.approval_status:
                                            string_approval.append(f"{self.env.user.name}:Approved")
                                            line.approval_status = "\n".join(string_approval)
                                            string_timestammp = [line.approved_time]
                                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                            line.approved_time = "\n".join(string_timestammp)
                                            if record.feedback_parent:
                                                feedback_list = [line.feedback,
                                                                 f"{self.env.user.name}:{record.feedback_parent}"]
                                                final_feedback = "\n".join(feedback_list)
                                                line.feedback = f"{final_feedback}"
                                            elif line.feedback and not record.feedback_parent:
                                                line.feedback = line.feedback
                                            else:
                                                line.feedback = ""
                                        else:
                                            line.approval_status = f"{self.env.user.name}:Approved"
                                            line.approved_time = f"{self.env.user.name}:{dateformat}"
                                            if record.feedback_parent:
                                                line.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                            else:
                                                line.feedback = ""
                                        line.is_approve = True
                                    else:
                                        line.approver_state = 'pending'
                                        if line.approval_status:
                                            string_approval.append(f"{self.env.user.name}:Approved")
                                            line.approval_status = "\n".join(string_approval)
                                            string_timestammp = [line.approved_time]
                                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                            line.approved_time = "\n".join(string_timestammp)
                                            if record.feedback_parent:
                                                feedback_list = [line.feedback,
                                                                 f"{self.env.user.name}:{record.feedback_parent}"]
                                                final_feedback = "\n".join(feedback_list)
                                                line.feedback = f"{final_feedback}"
                                            elif line.feedback and not record.feedback_parent:
                                                line.feedback = line.feedback
                                            else:
                                                line.feedback = ""
                                        else:
                                            line.approval_status = f"{self.env.user.name}:Approved"
                                            line.approved_time = f"{self.env.user.name}:{dateformat}"
                                            if record.feedback_parent:
                                                line.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                            else:
                                                line.feedback = ""
                                    line.approved_employee_ids = [(4, current_user)]

                        matrix_line = sorted(record.expense_approver_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Request!'
                            record.write({'state': 'approve', 'user_id': self.user_id.id or self.env.user.id})
                            record.approved_mail()
                        else:
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Request!'
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved!'
                    ))
            else:
                raise ValidationError(_(
                    'Already approved!'
                ))

    def refuse_sheet(self, reason):
        if not self.user_has_groups('hr_expense.group_hr_expense_team_approver'):
            raise UserError(_("Only Managers and HR Officers can approve expenses"))
        elif not self.user_has_groups('hr_expense.group_hr_expense_manager'):
            current_managers = self.employee_id.sudo().expense_manager_id | self.employee_id.sudo().parent_id.user_id | self.employee_id.sudo().department_id.manager_id.user_id

            if self.employee_id.sudo().user_id == self.env.user:
                raise UserError(_("You cannot refuse your own expenses"))

            if not self.env.user in current_managers and not self.user_has_groups(
                    'hr_expense.group_hr_expense_user') and self.employee_id.sudo().expense_manager_id != self.env.user:
                raise UserError(_("You can only refuse your department expenses"))

        self.expense_refuse_sheet()
        for sheet in self:
            sheet.message_post_with_view('hr_expense.hr_expense_template_refuse_reason',
                                         values={'reason': reason, 'is_sheet': True, 'name': sheet.name})
        self.activity_update()

    def expense_refuse_sheet(self):
        for record in self:
            for user in record.expense_approver_user_ids:
                for check_user in user.user_ids:
                    now = datetime.now(timezone(self.env.user.tz))
                    dateformat = f"{now.day}/{now.month}/{now.year} {now.hour}:{now.minute}:{now.second}"
                    if self.env.uid == check_user.id:
                        user.timestamp = fields.Datetime.now()
                        user.approver_state = 'refuse'
                        string_approval = []
                        string_approval.append(user.approval_status)
                        if user.approval_status:
                            string_approval.append(f"{self.env.user.name}:Refused")
                            user.approval_status = "\n".join(string_approval)
                            string_timestammp = [user.approved_time]
                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                            user.approved_time = "\n".join(string_timestammp)
                        else:
                            user.approval_status = f"{self.env.user.name}:Refused"
                            user.approved_time = f"{self.env.user.name}:{dateformat}"
            record.approved_user = self.env.user.name + ' ' + 'has been Rejected!'
            record.write({'state': 'cancel'})
            record.reject_mail()

    def _compute_register_button_hide(self):
        for rec in self:
            if rec.is_hide and rec.amount_residual != 0:
                rec.register_button_hide = True
            else:
                rec.register_button_hide = False

    def get_ca_remaining_amount(self):
        for rec in self:
            total_amt = 0
            for line in rec.cash_advance_number_ids:
                total_amt += line.remaining_amount
        rec.cash_advance_amount = total_amt

    def action_submit_sheet(self):
        for rec in self:
            if len(rec.expense_line_ids) == 0:
                raise ValidationError("Can’t submit new expense because the expense line has not been filled")
            rec.exp_difference = rec.total_amount
            rec.write({'state': 'submit'})
            rec.onchange_amount_sum()
            rec.activity_update()
            rec.approver_mail()

    def action_sheet_move_create(self):
        """Inheriting Post journal entries."""
        super(HrExpenseSheet, self).action_sheet_move_create()
        return self.reconcile_cash_advance()

    def reconcile_cash_advance(self):
        for rec in self:
            rec.is_hide = True
            for cash in rec.cash_advance_number_ids:
                cash.expense_ids = [(4, rec.id)]
                catch_before_update = cash.remaining_amount
                if rec.exp_difference > 0:
                    rec.exp_difference = (cash.remaining_amount - rec.exp_difference)
                    if rec.exp_difference <= 0:
                        cash.update({'remaining_amount': 0,
                                     'state': 'reconciled'})
                    else:
                        cash.update({'remaining_amount': rec.exp_difference,
                                     })
                elif rec.exp_difference < 0:
                    if rec.exp_difference < 0:
                        rec.exp_difference = (cash.remaining_amount + rec.exp_difference)
                        if rec.exp_difference <= 0:
                            cash.update({'remaining_amount': 0,
                                         'state': 'reconciled'})
                        else:
                            cash.update({'remaining_amount': rec.exp_difference,
                                         })
                # second condition: already cash advance is reconciled and made remaining amount != 0 in cash advance
                pay_2 = (catch_before_update - cash.remaining_amount)
                if pay_2 != 0 and cash.is_reconciled:
                    payments = self.env['account.payment.register'].with_context(active_model='account.move',
                                                                                 active_ids=self.account_move_id.ids).create(
                        {
                            'amount': pay_2,
                            'group_payment': True,
                            'payment_difference_handling': 'open',
                            'currency_id': rec.currency_id.id,
                            'payment_method_id': 2,
                        })._create_payments()
                    cash.update({'account_move_ids': [(4, payments.move_id.id)],
                                 })
                # Third condition: already cash advance is reconciled and made remaining amount = 0 in cash advance
                if pay_2 == 0 and cash.is_reconciled:
                    payments = self.env['account.payment.register'].with_context(active_model='account.move',
                                                                                 active_ids=self.account_move_id.ids).create(
                        {
                            'amount': catch_before_update,
                            'group_payment': True,
                            'payment_difference_handling': 'open',
                            'currency_id': rec.currency_id.id,
                            'payment_method_id': 2,
                        })._create_payments()
                    cash.update({'account_move_ids': [(4, payments.move_id.id)],
                                 })

                # first condition: new cash advance without reconciled
                pay = (cash.amount - cash.remaining_amount)
                if pay != 0 and not cash.is_reconciled:
                    payments = self.env['account.payment.register'].with_context(active_model='account.move',
                                                                                 active_ids=self.account_move_id.ids).create(
                        {
                            'amount': pay,
                            'group_payment': True,
                            'payment_difference_handling': 'open',
                            'currency_id': rec.currency_id.id,
                            'payment_method_id': 2,
                        })._create_payments()
                    cash.update({'account_move_ids': [(4, payments.move_id.id)],
                                 'is_reconciled': 'True',
                                 })

    def _compute_acc_line(self):
        for rec in self:
            if rec.account_move_id and rec.account_move_id.line_ids:
                for acc in rec.account_move_id.line_ids:
                    rec.line_ids = acc._reconciled_lines()
            else:
                rec.line_ids = False

    def _get_payment_info_JSON(self):
        self.expense_payment_widget = json.dumps(False)
        if self.account_move_id and self.line_ids:
            info = {'title': _('Payment'), 'content': []}
            for acc_move_line in self.line_ids:
                for payment in self.env['account.payment'].search([('move_id', '=', acc_move_line.move_id.id)]):
                    payment_ref = payment.move_id.name
                    if payment.move_id.ref:
                        payment_ref += ' (' + payment.move_id.ref + ')'
                    info['content'].append({
                        'name': payment.name,
                        'journal_name': payment.journal_id.name,
                        'amount': payment.amount,
                        'currency': payment.currency_id.symbol,
                        'digits': [69, payment.currency_id.decimal_places],
                        'position': payment.currency_id.position,
                        'date': str(payment.date),
                        'account_payment_id': payment.id,
                        'move_id': payment.move_id.id,
                        'ref': payment_ref,
                    })
            self.expense_payment_widget = json.dumps(info)

    def wizard_approve(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'hr.expense.sheet.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'name': "Confirmation Message",
            'target': 'new',
        }

    def get_expense_date(self):
        for record in self:
            # hr_expense_rec = self.env['hr.expense'].search([('sheet_id', '=', record.id)], order="id desc", limit=1)
            if record.create_date:
                expense_date = record.create_date
                expense_year = expense_date.strftime("%Y")
                expense_month_year = expense_date.strftime("%m/%Y")
                hr_expense_cycle_rec = self.env['hr.expense.cycle'].search([('hr_year_id.name', '=', expense_year)],
                                                                           limit=1)
                if hr_expense_cycle_rec:
                    hr_expense_cycle_line_rec = self.env['hr.expense.cycle.line'].search(
                        [('year_id', '=', hr_expense_cycle_rec.id), ('code', '=', expense_month_year)], limit=1)
                    if hr_expense_cycle_line_rec:
                        record.expense_cycle = hr_expense_cycle_line_rec.code
                        record.reimbursement_date = hr_expense_cycle_line_rec.reimbursement_date
            else:
                record.expense_cycle = False
                record.reimbursement_date = False

    def compute_reminder_details(self):
        for rec in self:
            rec.expense_reminder_before_days = self.env['ir.config_parameter'].sudo().get_param(
                'equip3_hr_expense_extend.expense_reminder_before_days')
            bg_color = self.env['ir.config_parameter'].sudo().get_param(
                'equip3_hr_expense_extend.expense_choose_color_reminder')
            if rec.reimbursement_date and rec.state in ('approve', 'post'):
                rei_date = datetime.strptime(str(rec.reimbursement_date), '%Y-%m-%d')
                rei_before_days = timedelta(rec.expense_reminder_before_days)
                a = rei_date - datetime.now()
                b = timedelta(days=-1)
                if b <= a and a <= rei_before_days:
                    rec.expense_choose_color_reminder = bg_color
                else:
                    rec.expense_choose_color_reminder = 'white'
            else:
                rec.expense_choose_color_reminder = 'white'

    def get_url(self, obj):
        url = ''
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        menu_id = self.env['ir.model.data'].get_object_reference(
            'hr_expense', 'menu_hr_expense_sheet_all_to_approve')[1]
        action_id = self.env['ir.model.data'].get_object_reference(
            'hr_expense', 'action_hr_expense_sheet_all_to_approve')[1]
        url = base_url + "/web?db=" + str(self._cr.dbname) + "#id=" + str(
            obj.id) + "&view_type=form&model=hr.expense.sheet&menu_id=" + str(
            menu_id) + "&action=" + str(action_id)
        return url

    def approver_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.expense_approver_user_ids:
                matrix_line = sorted(rec.expense_approver_user_ids.filtered(lambda r: r.is_approve == True))
                approver = rec.expense_approver_user_ids[len(matrix_line)]
                for user in approver.user_ids:
                    try:
                        template_id = ir_model_data.get_object_reference(
                            'equip3_hr_expense_extend',
                            'email_template_application_for_expense_approval')[1]
                    except ValueError:
                        template_id = False
                    ctx = self._context.copy()
                    url = self.get_url(self)
                    ctx.update({
                        'email_from': self.env.user.email,
                        'email_to': user.email,
                        'url': url,
                        'approver_name': user.name,
                        'emp_name': self.employee_id.name,
                    })
                    self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,force_send=True)
                break

    def approved_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.expense_approver_user_ids:
                try:
                    template_id = ir_model_data.get_object_reference(
                        'equip3_hr_expense_extend',
                        'email_template_approval_expense_request')[1]
                except ValueError:
                    template_id = False
                ctx = self._context.copy()
                url = self.get_url(self)
                ctx.update({
                    'email_from': self.env.user.email,
                    'email_to': self.employee_id.user_id.email,
                    'url': url,
                    'emp_name': self.employee_id.name,
                })
                self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id, force_send=True)
            break

    def reject_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.expense_approver_user_ids:
                try:
                    template_id = ir_model_data.get_object_reference(
                        'equip3_hr_expense_extend',
                        'email_template_rejection_of_expense_request')[1]
                except ValueError:
                    template_id = False
                ctx = self._context.copy()
                url = self.get_url(self)
                ctx.update({
                    'email_from': self.env.user.email,
                    'email_to': self.employee_id.user_id.email,
                    'url': url,
                    'emp_name': self.employee_id.name,
                })
                self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(rec.id, force_send=True)
            break


class VendorDepositHrCashAdvance(models.Model):
    _inherit = 'vendor.deposit'

    expense_count = fields.Integer(compute='compute_count')
    reconcile_count = fields.Integer()
    expense_ids = fields.Many2many('hr.expense.sheet', string='Expenses')
    account_move_ids = fields.Many2many('account.move', string='Journal Entrys')
    is_reconciled = fields.Boolean(string='Is Reconciled', default=False)
    communication = fields.Char(string="Remarks", tracking=True)

    def compute_count(self):
        for record in self:
            record.expense_count = self.env['hr.expense.sheet'].search_count(
                [('id', 'in', self.expense_ids.ids)])
            record.reconcile_count = self.env['account.move'].search_count(
                [('id', 'in', self.account_move_ids.ids)])

    def get_expense(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Expense',
            'view_mode': 'tree,form',
            'res_model': 'hr.expense.sheet',
            'domain': [('id', 'in', self.expense_ids.ids)],
            'context': "{'create': False}"
        }

    def get_reconcile(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Reconcile',
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'domain': [('id', 'in', self.account_move_ids.ids)],
            'context': "{'create': False}"
        }


class ExpenseApproverUser(models.Model):
    _name = 'expense.approver.user'

    expense_id = fields.Many2one('hr.expense.sheet', string="Expense Id")
    name = fields.Integer('Sequence', compute="fetch_sl_no")
    user_ids = fields.Many2many('res.users', string="Approvers")
    approved_employee_ids = fields.Many2many('res.users', 'exp_user_ids', string="Approved user")
    minimum_approver = fields.Integer(string="Minimum Approver", default=1)
    timestamp = fields.Datetime(string="Timestamp")
    approved_time = fields.Text(string="Timestamp")
    feedback = fields.Text()
    approver_state = fields.Selection([('draft', 'Draft'), ('pending', 'Pending'), ('approved', 'Approved'),
                                       ('refuse', 'Refused')], default='draft', string="Approval Status")
    approval_status = fields.Text()
    is_approve = fields.Boolean(string="Is Approve", default=False)

    @api.depends('expense_id')
    def fetch_sl_no(self):
        sl = 0
        for line in self.expense_id.expense_approver_user_ids:
            sl = sl + 1
            line.name = sl
