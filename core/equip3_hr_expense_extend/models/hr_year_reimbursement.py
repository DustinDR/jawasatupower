# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
from odoo import _, api, fields, models
from datetime import timedelta
from odoo.exceptions import ValidationError
from lxml import etree

class HrExpenseCycle(models.Model):
    _name = 'hr.expense.cycle'
    _description = 'Hr Expense Cycle'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    hr_year_id = fields.Many2one("hr.years", string="Hr Year", domain="[('status', '=', 'open')]")
    year_ids = fields.One2many('hr.expense.cycle.line', 'year_id', string='Expense Cycle Line')
    reimbursement_date_after = fields.Integer("Reimbursement Date after")
    is_confirm = fields.Boolean("Is Confirmed", default=False)
    
    
    @api.model
    def fields_view_get(self, view_id=None, view_type=None,
                        toolbar=True, submenu=True):
        res = super(HrExpenseCycle, self).fields_view_get(
            view_id=view_id, view_type=view_type,toolbar=toolbar,submenu=submenu)
        
        if  self.env.user.has_group('hr_expense.group_hr_expense_user'):
            root = etree.fromstring(res['arch'])
            root.set('create', 'true')
            root.set('edit', 'true')
            root.set('delete', 'true')
            res['arch'] = etree.tostring(root)
        else:
            root = etree.fromstring(res['arch'])
            root.set('create', 'false')
            root.set('edit', 'false')
            root.set('delete', 'false')
            res['arch'] = etree.tostring(root)
            
        return res

    def expense_confirm(self):
        expense_line = []
        for rec in self:
            if rec.hr_year_id:
                rec.is_confirm = True
                rec.year_ids.unlink()
                for hr_rec_line in rec.hr_year_id.year_ids:
                    expense_line.append((0, 0, {'year_id': self.id,
                                                'code': hr_rec_line.code,
                                                'cycle_start': hr_rec_line.start_period,
                                                'cycle_end': hr_rec_line.end_period,
                                                'reimbursement_date': hr_rec_line.end_period + timedelta(
                                                    days=rec.reimbursement_date_after)}))
                rec.year_ids = expense_line

    def name_get(self):
        return [(record.id, "%s" % (record.hr_year_id.name)) for record in self]

    _sql_constraints = [
        ('unique_hr_year_id', 'unique(hr_year_id)', 'Hr Year already taken by another Period'),
    ]


class HrExpenseCycleLine(models.Model):
    _name = 'hr.expense.cycle.line'

    year_id = fields.Many2one('hr.expense.cycle', string='Hr Year')
    code = fields.Char("Cycle Code")
    cycle_start = fields.Date("Cycle Start")
    cycle_end = fields.Date("Cycle End")
    reimbursement_date = fields.Date("Reimbursement Date")


class AccountPaymentRegister(models.TransientModel):
    _inherit = 'account.payment.register'
    _description = 'Register Payment'

    @api.depends('partner_id')
    def _compute_partner_bank_id(self):
        ''' The default partner_bank_id will be the first available on the partner. '''
        for wizard in self:
            wizard.get_bank_details()
            available_partner_bank_accounts = wizard.partner_id.bank_ids.filtered(lambda x: x.company_id in (False, wizard.company_id))
            if available_partner_bank_accounts:
                wizard.partner_bank_id = available_partner_bank_accounts[0]._origin
            else:
                wizard.partner_bank_id = False


    def get_bank_details(self):
        for pay in self:
            if pay.partner_id.bank_ids:
                pay.partner_id.bank_ids = False
            exp_user_id = self.env['res.users'].search([('partner_id', '=', self.partner_id.id)], limit=1)
            if exp_user_id:
                exp_emp_id = self.env['hr.employee'].search([('user_id', '=', exp_user_id.id)], limit=1)
                if exp_emp_id:
                    exp_acc_rec = self.env['bank.account'].search([('employee_id', '=', exp_emp_id.id), ('is_used', '=', True)],
                                                                  limit=1)
                    if exp_acc_rec:
                        self.env['res.partner.bank'].create({
                            'bank_id': exp_acc_rec.name.id,
                            'acc_number': exp_acc_rec.acc_number,
                            'partner_id': self.partner_id.id,
                            'company_id': self.company_id.id,
                        })

                    exp_acc_rec_false = self.env['bank.account'].search(
                        [('employee_id', '=', exp_emp_id.id), ('is_used', '=', False)])
                    if exp_acc_rec_false:
                        for acc_false in exp_acc_rec_false:
                            self.env['res.partner.bank'].create({
                                'bank_id': acc_false.name.id,
                                'acc_number': acc_false.acc_number,
                                'partner_id': self.partner_id.id,
                                'company_id': self.company_id.id,
                            })