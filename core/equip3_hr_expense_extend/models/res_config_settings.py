from odoo import api, fields, models


class ExpenseResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    expense_type_approval = fields.Selection(
        [('employee_hierarchy', 'By Employee Hierarchy'), ('approval_matrix', 'By Approval Matrix')], default='employee_hierarchy',
        config_parameter='equip3_hr_expense_extend.expense_type_approval')
    expense_level = fields.Integer(config_parameter='equip3_hr_expense_extend.expense_level', default=1)
    expense_reminder_before_days = fields.Integer(config_parameter='equip3_hr_expense_extend.expense_reminder_before_days')
    expense_choose_color_reminder = fields.Selection(
        [('red', 'Red'), ('green', 'Green'), ('blue', 'Blue'), ('purple', 'Purple'), ('yellow', 'Yellow'), ('white', 'White')],
        config_parameter='equip3_hr_expense_extend.expense_choose_color_reminder')

    @api.onchange("expense_level")
    def _onchange_expense_level(self):
        if self.expense_level < 1:
            self.expense_level = 1