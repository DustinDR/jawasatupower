from odoo import fields, models, api, _
from odoo.exceptions import ValidationError
from datetime import datetime
import pytz
from odoo.tools.safe_eval import safe_eval

class HrOvertimeActualConvertLeave(models.TransientModel):
    _name = 'hr.overtime.actual.convert.leave'

    overtime_id = fields.Many2one('hr.overtime.actual')
    state = fields.Char(related='overtime_id.overtime_wizard_state')
    leave_type = fields.Many2one('hr.leave.type', string="Leave Type", domain=[('leave_method', 'in', ['none']),('overtime_extra_leave', '=', True)])
    durations = fields.Float('Durations')
    effective_date = fields.Date('Effective Date', default=fields.Date.context_today)

    # @api.onchange('leave_type')
    # def onchange_leave_type(self):
    #     for res in self:
    #         if res.leave_type and res.leave_type.overtime_extra_leave == False:
    #             raise ValidationError(_("Selected leave types not allow to overtime extra leave"))

    @api.onchange('overtime_id', 'leave_type')
    def onchange_overtime_id(self):
        for res in self:
            if res.leave_type and res.overtime_id:
                res.durations = 0.0
                formula = res.leave_type.formula
                if res.leave_type.total_actual_hours:
                    localdict = {"actual_hours": res.overtime_id.total_actual_hours,"duration": 0.0}
                    safe_eval(formula, localdict, mode='exec', nocopy=True)
                    res.durations = localdict['duration']
                else:
                    total_duration = 0.0
                    for line in res.overtime_id.actual_line_ids:
                        localdict = {"actual_hours": line.actual_hours,"duration": 0.0}
                        safe_eval(formula, localdict, mode='exec', nocopy=True)
                        total_duration += localdict['duration']
                    res.durations = total_duration

    def save(self):
        line_date = []
        if self.leave_type:
            if self.leave_type.allocation_type != 'fixed_allocation':
                if not self.leave_type.responsible_id:
                    raise ValidationError(f"Responsible not set in leave types {self.leave_type.name}")
                line_data = [(0, 0, {'user_ids': [(4, self.leave_type.responsible_id.id)]})]
            elif self.leave_type.allocation_type == 'fixed_allocation':
                if self.leave_type.allocation_validation_type == 'hr':
                    if not self.leave_type.responsible_id:
                        raise ValidationError(f"Responsible not set in leave types {self.leave_type.name}")
                    line_data = [(0, 0, {'user_ids': [(4, self.leave_type.responsible_id.id)]})]
                if self.leave_type.allocation_validation_type == 'manager':
                    if not self.overtime_id.employee_id.parent_id:
                        raise ValidationError(f"Manager not set in Employee {self.overtime_id.employee_id.name}")
                    if not self.overtime_id.employee_id.parent_id.user_id:
                        raise ValidationError(f"User not set in Employee {self.overtime_id.employee_id.parent_id.name}")
                    line_data = [(0, 0, {'user_ids': [(4, self.overtime_id.employee_id.parent_id.user_id.id)]})]
                if self.leave_type.allocation_validation_type == 'both':
                    if not self.overtime_id.employee_id.parent_id:
                        raise ValidationError(f"Manager not set in Employee {self.overtime_id.employee_id.name}")
                    if not self.overtime_id.employee_id.parent_id.user_id:
                        raise ValidationError(f"User not set in Employee {self.overtime_id.employee_id.parent_id.name}")
                    if not self.leave_type.responsible_id:
                        raise ValidationError(f"Responsible not set in leave types {self.leave_type.name}")
                    line_data = [(0, 0, {'user_ids': [(4, self.overtime_id.employee_id.parent_id.user_id.id)]})]
                    line_data.append((0, 0, {'user_ids': [(4, self.leave_type.responsible_id.id)]}))
        leave_allocation = self.env['hr.leave.allocation'].create({
                                'name': self.leave_type.name + ' Allocation',
                                'employee_id': self.overtime_id.employee_id.id,
                                'department_id': self.overtime_id.employee_id.department_id.id,
                                'holiday_status_id': self.leave_type.id,
                                'allocation_type_by': 'overtime',
                                'overtime_id': self.overtime_id.id,
                                'number_of_days_display': self.durations,
                                'effective_date': self.effective_date,
                                'state': 'draft',
                                'approver_user_ids': line_data
                            })
