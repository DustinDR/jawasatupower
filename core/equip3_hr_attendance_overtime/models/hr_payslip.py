# -*- coding: utf-8 -*-
from odoo import models, fields, api, tools, _

class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    @api.model
    def create(self, vals):
        payslip = super(HrPayslip, self).create(vals)
        act_ovt_obj = self.env['hr.overtime.actual'].search(
            [('employee_id', '=', payslip.employee_id.id), ('state', '=', 'approved'), ('applied_to','=','payslip')])
        ovt_obj = self.env['hr.overtime.actual.line'].search(
            [('employee_id', '=', payslip.employee_id.id), ('state', '=', 'approved'), ('actual_id','in',act_ovt_obj.ids)])
        total_overtime_day = 0.0
        total_overtime_hour = 0.0
        total_overtime_amount = 0.0
        total_meal_allowance = 0.0
        if ovt_obj:
            for ovt in ovt_obj:
                if payslip.date_from <= ovt.date <= payslip.date_to:
                    total_overtime_day += 1
                    total_overtime_hour += ovt.actual_hours
                    total_overtime_amount += ovt.amount
                    total_meal_allowance += ovt.meal_allowance

            for rec in payslip.input_line_ids:
                if rec.code == 'OVT':
                    rec.amount = total_overtime_amount
                if rec.code == 'OVT_MEAL':
                    rec.amount = total_meal_allowance

            for rec in payslip.worked_days_line_ids:
                if rec.code == 'OVERTIME':
                    rec.number_of_days = total_overtime_day
                    rec.number_of_hours = total_overtime_hour
        return payslip

    @api.onchange('employee_id', 'date_from', 'date_to')
    def onchange_employee(self):
        res = super(HrPayslip, self).onchange_employee()
        if (not self.employee_id) or (not self.date_from) or (not self.date_to):
            return

        employee = self.employee_id
        date_from = self.date_from
        date_to = self.date_to
        contract_ids = []

        if not self.env.context.get('contract') or not self.contract_id:
            contract_ids = self.get_contract(employee, date_from, date_to)
            if not contract_ids:
                return
            self.contract_id = self.env['hr.contract'].browse(contract_ids[0])

        contracts = self.env['hr.contract'].browse(contract_ids)

        act_ovt_obj = self.env['hr.overtime.actual'].search(
            [('employee_id', '=', payslip.employee_id.id), ('state', '=', 'approved'), ('applied_to','=','payslip')])
        ovt_obj = self.env['hr.overtime.actual.line'].search(
            [('employee_id', '=', payslip.employee_id.id), ('state', '=', 'approved'), ('actual_id','in',act_ovt_obj.ids)])

        total_overtime_day = 0.0
        total_overtime_hour = 0.0
        total_overtime_amount = 0.0
        total_meal_allowance = 0.0

        if ovt_obj:
            for ovt in ovt_obj:
                if self.date_from <= ovt.date <= self.date_to:
                    total_overtime_day += 1
                    total_overtime_hour += ovt.actual_hours
                    total_overtime_amount += ovt.amount
                    total_meal_allowance += ovt.meal_allowance

            other_input_entries = []
            for contract in contracts:
                input_data = {
                    'name': 'Overtime',
                    'code': 'OVT',
                    'amount': total_overtime_amount,
                    'contract_id': contract.id,
                }
                other_input_entries += [input_data]

                input_meal = {
                    'name': 'Overtime Meal',
                    'code': 'OVT_MEAL',
                    'amount': total_meal_allowance,
                    'contract_id': contract.id,
                }
                other_input_entries += [input_meal]

            input_lines = self.input_line_ids.browse([])
            for r in other_input_entries:
                input_lines += input_lines.new(r)
            self.input_line_ids = input_lines

            for rec in self.worked_days_line_ids:
                if rec.code == 'OVERTIME':
                    rec.number_of_days = total_overtime_day
                    rec.number_of_hours = total_overtime_hour

        return res