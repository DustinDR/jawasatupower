# -*- coding: utf-8 -*-

from . import overtime_rules
from . import resource_calendar
from . import hr_overtime_approval_matrix
from . import hr_overtime_request
from . import hr_overtime_actual
from . import hr_payslip
from . import res_config_settings