# -*- coding: utf-8 -*-
import babel
from odoo import models, fields, api, tools, _
from datetime import datetime, timedelta, time
from odoo.exceptions import ValidationError
import pytz
import math
import requests
headers = {'content-type': 'application/json'}

class Equip3HrOvertimeActual(models.Model):
    _name = 'hr.overtime.actual'
    _description = 'HR Overtime Actual'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'create_date desc'

    @api.returns('self')
    def _get_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1) or False

    name = fields.Char('Number', track_visibility='onchange')
    employee_id = fields.Many2one('hr.employee', string='Employee', default=_get_employee, required=True)
    actual_based_on = fields.Selection([('overtime_request', 'Overtime Request'),
                                        ('without_overtime_request', 'Without Overtime Request')],
                                       default='', required=True, string='Actual Based on')
    overtime_request = fields.Many2one('hr.overtime.request', string='Overtime Request', domain="[('employee_id', '=', employee_id),('state','=','approved')]")
    period_start = fields.Date('Period Start', required=True)
    period_end = fields.Date('Period End', required=True)
    description = fields.Text('Description', required=True)
    total_actual_hours = fields.Float('Actual Hours', store=True, readonly=True, compute='_get_total_actual_hours')
    total_coefficient_hours = fields.Float('Coefficient Hours', store=True, readonly=True, compute='_get_total_coefficient_hours')
    total_overtime_amount = fields.Float('Overtime Amount', store=True, readonly=True, compute='_get_total_overtime_amount')
    company_id = fields.Many2one("res.company", string="Company", default=lambda self: self.env.company,
                                 track_visibility='onchange')
    state = fields.Selection([('draft', 'Draft'), ('to_approve', 'To Approve'), ('approved', 'Approved'),
                              ('convert_as_leave', 'Convert as Leave'),('rejected', 'Rejected')], default='draft', string='Status')
    is_calculated = fields.Boolean('is Calculated')
    is_admin_user = fields.Boolean('Admin User', compute='get_user')
    actual_line_ids = fields.One2many('hr.overtime.actual.line', 'actual_id')
    actual_approval_line_ids = fields.One2many('hr.overtime.actual.approval.line', 'actual_id')
    actual_attendance_line_ids = fields.One2many('hr.overtime.actual.attendance.line', 'actual_id')
    is_hide_reject = fields.Boolean(default=True, compute='_get_is_hide')
    is_hide_approve = fields.Boolean(default=True, compute='_get_is_hide')
    user_approval_ids = fields.Many2many('res.users', compute="_is_hide_approve")
    overtime_wizard_state = fields.Char('OverTime Wizard State')
    applied_to = fields.Selection([('payslip', 'Payslip'), ('extra_leave', 'Extra Leave')], default='payslip', string='Applied To', required=True)
    
    
    def custom_menu(self):
        views = [(self.env.ref('equip3_hr_attendance_overtime.hr_overtime_actual_tree_view').id, 'tree'),
                        (self.env.ref('equip3_hr_attendance_overtime.hr_overtime_actual_form_view').id, 'form')]
        if  self.env.user.has_group('equip3_hr_attendance_overtime.group_overtime_self_service') and not self.env.user.has_group('equip3_hr_attendance_overtime.group_overtime_team_approver'):
            return {
                'type': 'ir.actions.act_window',
                'name': 'Actual Overtimes',
                'res_model': 'hr.overtime.actual',
                'view_mode': 'tree,form',
                'views':views,
                'domain': [('employee_id.user_id', '=', self.env.user.id)],
                'context':{},
                'help':"""<p class="oe_view_nocontent_create">
                    Click Create to add new Actual Overtimes.
                </p>"""
        }
        else:
            return {
                'type': 'ir.actions.act_window',
                'name': 'Actual Overtimes',
                'res_model': 'hr.overtime.actual',
                'view_mode': 'tree,form',
                'views':views,
                'domain': [],
                'context':{},
                'help':"""<p class="oe_view_nocontent_create">
                    Click Create to add new Actual Overtimes.
                </p>"""
        }

    def round_up(self, n, decimals=0):
        multiplier = 10 ** decimals
        return math.ceil(n * multiplier) / multiplier

    def round_down(self, n, decimals=0):
        multiplier = 10 ** decimals
        return math.floor(n * multiplier) / multiplier

    @api.model
    def create(self, vals):
        sequence = self.env['ir.sequence'].next_by_code('hr.overtime.actual')
        vals.update({'name': sequence})
        return super(Equip3HrOvertimeActual, self).create(vals)

    @api.depends('actual_approval_line_ids')
    def _is_hide_approve(self):
        for record in self:
            if record.actual_approval_line_ids:
                sequence = [data.sequence for data in record.actual_approval_line_ids.filtered(
                    lambda line: len(line.approver_confirm.ids) != line.minimum_approver)]
                if sequence:
                    minimum_sequence = min(sequence)
                    approve_user = record.actual_approval_line_ids.filtered(lambda
                                                                                 line: self.env.user.id in line.approver_id.ids and self.env.user.id not in line.approver_confirm.ids and line.sequence == minimum_sequence)
                    if approve_user:
                        record.user_approval_ids = [(6, 0, [self.env.user.id])]
                    else:
                        record.user_approval_ids = False
                else:
                    record.user_approval_ids = False
            else:
                record.user_approval_ids = False

    @api.depends('user_approval_ids')
    def _get_is_hide(self):
        for record in self:
            if not record.user_approval_ids:
                record.is_hide_approve = True
                record.is_hide_reject = True
            else:
                record.is_hide_approve = False
                record.is_hide_reject = False

    @api.depends('employee_id')
    def get_user(self):
        if self.env.uid == 2:
            self.is_admin_user = True
        else:
            self.is_admin_user = False

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        for record in self:
            setting = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_overtime.approval_method')
            if record.employee_id:
                if record.actual_approval_line_ids:
                    remove = []
                    for line in record.actual_approval_line_ids:
                        remove.append((2, line.id))
                    record.actual_approval_line_ids = remove
                if setting == 'employee_hierarchy':
                    record.actual_approval_line_ids = self.approval_by_hierarchy(record)
                else:
                    self.approval_by_matrix(record)

    def approval_by_matrix(self, record):
        approval_matrix = self.env['hr.overtime.approval.matrix'].search([('apply_to', '=', 'by_employee')])
        matrix = approval_matrix.filtered(lambda line: record.employee_id.id in line.employee_ids.ids)

        if matrix:
            data_approvers = []
            for line in matrix[0].approval_matrix_ids:
                data_approvers.append((0, 0, {'sequence': line.sequence, 'minimum_approver': line.minimum_approver,
                                              'approver_id': [(6, 0, line.approvers.ids)]}))
            record.actual_approval_line_ids = data_approvers
        if not matrix:
            data_approvers = []
            approval_matrix = self.env['hr.overtime.approval.matrix'].search(
                [('apply_to', '=', 'by_job_position')])
            matrix = approval_matrix.filtered(lambda line: record.employee_id.job_id.id in line.job_ids.ids)
            if matrix:
                for line in matrix[0].approval_matrix_ids:
                    data_approvers.append((0, 0, {'sequence': line.sequence, 'minimum_approver': line.minimum_approver,
                                                  'approver_id': [(6, 0, line.approvers.ids)]}))
                record.actual_approval_line_ids = data_approvers
            if not matrix:
                data_approvers = []
                approval_matrix = self.env['hr.overtime.approval.matrix'].search(
                    [('apply_to', '=', 'by_department')])
                matrix = approval_matrix.filtered(lambda line: record.employee_id.department_id.id in line.deparment_ids.ids)
                if matrix:
                    for line in matrix[0].approval_matrix_ids:
                        data_approvers.append((0, 0,
                                               {'sequence': line.sequence, 'minimum_approver': line.minimum_approver,
                                                'approver_id': [(6, 0, line.approvers.ids)]}))
                    record.actual_approval_line_ids = data_approvers

    def approval_by_hierarchy(self,record):
        approval_ids = []
        seq = 1
        data = 0
        line = self.get_manager(record,record.employee_id,data,approval_ids,seq)
        return line

    def get_manager(self, record, employee_manager, data, approval_ids, seq):
        setting_level = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_overtime.approval_levels')
        if not setting_level:
            raise ValidationError("level not set")
        if not employee_manager['parent_id']['user_id']:
            return approval_ids
        while data < int(setting_level):
            approval_ids.append(
                (0, 0, {'sequence': seq, 'approver_id': [(4, employee_manager['parent_id']['user_id']['id'])]}))
            data += 1
            seq += 1
            if employee_manager['parent_id']['user_id']['id']:
                self.get_manager(record, employee_manager['parent_id'], data, approval_ids, seq)
                break

        return approval_ids

    @api.onchange('actual_based_on')
    def _onchange_actual_based_on(self):
        if self.actual_based_on:
            self.overtime_request = False
            self.period_start = False
            self.period_end = False
            remove = []
            for line in self.actual_line_ids:
                remove.append((2, line.id))
            self.actual_line_ids = remove

            att_remove = []
            for line in self.actual_attendance_line_ids:
                att_remove.append((2, line.id))
            self.actual_attendance_line_ids = att_remove

    @api.onchange('overtime_request')
    def _onchange_overtime_request(self):
        request = self.env['hr.overtime.request'].search([('id', '=', self.overtime_request.id)], limit=1)
        self.period_start = request.period_start
        self.period_end = request.period_end

        if self.period_start and self.period_end:
            user_tz = self.employee_id.tz or pytz.utc or 'UTC'
            local = pytz.timezone(user_tz)
            date_min = datetime.combine(fields.Date.from_string(self.period_start), time.min)
            date_max = datetime.combine(fields.Date.from_string(self.period_end), time.max)
            date_start = local.localize(date_min).astimezone(pytz.UTC).replace(tzinfo=None)
            date_end = local.localize(date_max).astimezone(pytz.UTC).replace(tzinfo=None)

            attendances = self.env['hr.attendance'].search([('employee_id', '=', self.employee_id.id),
                                                            ('check_in', '>=', date_start),
                                                            ('check_in', '<=', date_end)], order='check_in')
            if attendances:
                attendance_line = []
                for att in attendances:
                    input_data = {
                        'actual_id': self.id,
                        'attendance_id': att.id,
                        'employee_id': att.employee_id.id,
                        'check_in': att.check_in,
                        'check_out': att.check_out,
                        'worked_hours': att.worked_hours,
                        'attendance_status': att.attendance_status
                    }
                    attendance_line += [input_data]

                attendance_lines = self.actual_attendance_line_ids.browse([])
                for r in attendance_line:
                    attendance_lines += attendance_lines.new(r)
                self.actual_attendance_line_ids = attendance_lines
            else:
                att_remove = []
                for line in self.actual_attendance_line_ids:
                    att_remove.append((2, line.id))
                self.actual_attendance_line_ids = att_remove

            request_line = self.env['hr.overtime.request.line'].search(
                [('request_id', '=', self.overtime_request.id)], order='date')

            if request_line:
                actual_line = []
                for rec in request_line:
                    dates_min = datetime.combine(fields.Date.from_string(rec.date), time.min)
                    dates_max = datetime.combine(fields.Date.from_string(rec.date), time.max)
                    dates_start = local.localize(dates_min).astimezone(pytz.UTC).replace(tzinfo=None)
                    dates_end = local.localize(dates_max).astimezone(pytz.UTC).replace(tzinfo=None)
                    att_obj = self.env['hr.attendance'].search([('employee_id', '=', rec.employee_id.id),
                                                                ('check_in', '>=', dates_start),
                                                                ('check_in', '<=', dates_end)],
                                                               limit=1, order='check_in')
                    start_times = 0.0
                    end_times = 0.0
                    if att_obj:
                        check_ins = pytz.UTC.localize(att_obj.check_in).astimezone(local)
                        check_outs = pytz.UTC.localize(att_obj.check_out).astimezone(local)
                        checkout_times = check_outs.time()
                        if rec.date == check_ins.date():
                            start_times = att_obj.hour_to
                            end_times = checkout_times.hour + checkout_times.minute / 60

                    input_data = {
                        'employee_id': rec.employee_id.id,
                        'overtime_reason': rec.overtime_reason,
                        'date': rec.date,
                        'start_time': rec.start_time,
                        'end_time': rec.end_time,
                        'hours': rec.number_of_hours,
                        'actual_start_time': rec.start_time,
                        'actual_end_time': rec.end_time,
                        'actual_id': self.id,
                    }
                    actual_line += [input_data]

                actual_lines = self.actual_line_ids.browse([])
                for r in actual_line:
                    actual_lines += actual_lines.new(r)
                self.actual_line_ids = actual_lines
            else:
                req_remove = []
                for line in self.actual_line_ids:
                    req_remove.append((2, line.id))
                self.actual_line_ids = req_remove

    @api.onchange('employee_id', 'period_start', 'period_end', 'actual_based_on')
    def _onchange_period(self):
        if (not self.employee_id) or (not self.period_start) or (not self.period_end) or (not self.actual_based_on):
            return

        employee = self.employee_id
        period_start = self.period_start
        period_end = self.period_end
        delta = period_end - period_start
        days = [period_start + timedelta(days=i) for i in range(delta.days + 1)]

        ttyme = datetime.combine(fields.Date.from_string(period_start), time.min)
        ttyme_end = datetime.combine(fields.Date.from_string(period_end), time.min)
        locale = self.env.context.get('lang') or 'en_US'
        self.description = _('Actual Overtime for period: %s to %s') % (
            tools.ustr(babel.dates.format_date(date=ttyme, format='dd MMM YYYY', locale=locale)),
            tools.ustr(babel.dates.format_date(date=ttyme_end, format='dd MMM YYYY', locale=locale)))

        user_tz = self.employee_id.tz or pytz.utc or 'UTC'
        local = pytz.timezone(user_tz)
        date_min = datetime.combine(fields.Date.from_string(period_start), time.min)
        date_max = datetime.combine(fields.Date.from_string(period_end), time.max)
        date_start = local.localize(date_min).astimezone(pytz.UTC).replace(tzinfo=None)
        date_end = local.localize(date_max).astimezone(pytz.UTC).replace(tzinfo=None)

        attendances = self.env['hr.attendance'].search([('employee_id', '=', employee.id),
                                                        ('check_in', '>=', date_start),
                                                        ('check_in', '<=', date_end)], order='check_in')
        if attendances:
            attendance_line = []
            for att in attendances:
                input_data = {
                    'actual_id': self.id,
                    'attendance_id': att.id,
                    'employee_id': att.employee_id.id,
                    'check_in': att.check_in,
                    'check_out': att.check_out,
                    'worked_hours': att.worked_hours,
                    'attendance_status': att.attendance_status
                }
                attendance_line += [input_data]

            attendance_lines = self.actual_attendance_line_ids.browse([])
            for r in attendance_line:
                attendance_lines += attendance_lines.new(r)
            self.actual_attendance_line_ids = attendance_lines

        if self.actual_based_on == 'without_overtime_request':
            actual_line = []
            for date in days:
                dates_min = datetime.combine(fields.Date.from_string(date), time.min)
                dates_max = datetime.combine(fields.Date.from_string(date), time.max)
                dates_start = local.localize(dates_min).astimezone(pytz.UTC).replace(tzinfo=None)
                dates_end = local.localize(dates_max).astimezone(pytz.UTC).replace(tzinfo=None)
                att_obj = self.env['hr.attendance'].search([('employee_id', '=', employee.id),
                                                            ('check_in', '>=', dates_start),
                                                            ('check_in', '<=', dates_end)],
                                                           limit=1, order='check_in')
                start_times = 0.0
                end_times = 0.0
                if att_obj:
                    check_ins = pytz.UTC.localize(att_obj.check_in).astimezone(local)
                    check_outs = pytz.UTC.localize(att_obj.check_out).astimezone(local)
                    checkout_times = check_outs.time()
                    if date == check_ins.date():
                        start_times = att_obj.hour_to
                        end_times = checkout_times.hour + checkout_times.minute / 60

                input_data = {
                    'employee_id': self.employee_id.id,
                    'date': date,
                    'actual_start_time': start_times,
                    'actual_end_time': end_times,
                    'actual_id': self.id,
                }
                actual_line += [input_data]

            actual_lines = self.actual_line_ids.browse([])
            for r in actual_line:
                actual_lines += actual_lines.new(r)
            self.actual_line_ids = actual_lines

    @api.depends('actual_line_ids.actual_hours')
    def _get_total_actual_hours(self):
        for rec in self:
            total_actual_hours = 0.0
            for line in rec.actual_line_ids:
                total_actual_hours += line.actual_hours
            rec.update({
                'total_actual_hours': total_actual_hours,
            })

    @api.depends('actual_line_ids.coefficient_hours')
    def _get_total_coefficient_hours(self):
        for rec in self:
            total_coefficient_hours = 0.0
            for line in rec.actual_line_ids:
                total_coefficient_hours += line.coefficient_hours
            rec.update({
                'total_coefficient_hours': total_coefficient_hours,
            })

    @api.depends('actual_line_ids.amount')
    def _get_total_overtime_amount(self):
        for rec in self:
            total_overtime_amount = 0.0
            for line in rec.actual_line_ids:
                total_overtime_amount += line.amount
            rec.update({
                'total_overtime_amount': total_overtime_amount,
            })

    def confirm(self):
        for rec in self:
            if rec.is_calculated  == False:
                raise ValidationError(_("""Please calculate overtime first!"""))
            else:
                if not rec.actual_approval_line_ids:
                    rec.state = "approved"
                    rec.message_post(body=_('Status: Draft -> Approved'))
                else:
                    rec.state = "to_approve"
                    rec.message_post(body=_('Status: Draft -> To Approve'))

                for line in rec.actual_line_ids:
                    line.state = rec.state
            rec.approver_mail()
            rec.approver_wa_template()

    def approve(self):
        self.update({
            'overtime_wizard_state': 'approved',
        })
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'hr.overtime.actual.approval.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'name':"Confirmation Message",
            'target': 'new',
            'context':{'default_overtime_id':self.id},
        }

    def reject(self):
        self.update({
            'overtime_wizard_state': 'rejected',
        })
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'hr.overtime.actual.approval.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'name':"Confirmation Message",
            'target': 'new',
            'context':{'default_overtime_id':self.id},
        }

    def convert_to_leave(self):
        self.update({
            'overtime_wizard_state': 'convert_as_leave',
        })
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'hr.overtime.actual.convert.leave',
            'view_type': 'form',
            'view_mode': 'form',
            'name':"Convert to Leave",
            'target': 'new',
            'context':{'default_overtime_id':self.id},
        }

    @api.constrains('actual_line_ids')
    def _check_overtime_reason(self):
        for rec in self:
            for line in rec.actual_line_ids:
                if not line.overtime_reason:
                    raise ValidationError(_("""You must filled overtime reason."""))

    @api.model
    def get_contract(self, employee, date_from, date_to):
        # a contract is valid if it ends between the given dates
        clause_1 = ['&', ('date_end', '<=', date_to), ('date_end', '>=', date_from)]
        # OR if it starts between the given dates
        clause_2 = ['&', ('date_start', '<=', date_to), ('date_start', '>=', date_from)]
        # OR if it starts before the date_from and finish after the date_end (or never finish)
        clause_3 = ['&', ('date_start', '<=', date_from), '|', ('date_end', '=', False), ('date_end', '>=', date_to)]
        clause_final = [('employee_id', '=', employee.id), ('state', '=', 'open'), '|',
                        '|'] + clause_1 + clause_2 + clause_3
        return self.env['hr.contract'].search(clause_final).ids

    def calculate_overtime(self):
        for ovt in self:
            overtime_rounding = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_overtime.overtime_rounding')
            overtime_rounding_type = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_overtime.overtime_rounding_type')
            overtime_rounding_digit = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_overtime.overtime_rounding_digit')
            act_ovt_line = ovt.actual_line_ids
            for act in act_ovt_line:
                user_tz = act.employee_id.tz or pytz.utc
                local = pytz.timezone(user_tz)
                working_time = act.employee_id.resource_calendar_id
                week_working_day = working_time.week_working_day
                working_schedule_cal = self.env['employee.working.schedule.calendar'].search([('employee_id', '=', ovt.employee_id.id)])
                shortday_schedule_cal = self.env['employee.working.schedule.calendar'].search([('is_holiday', '=', True),
                                                                                               ('dayofweek', '=', 4)])

                day_dict = []
                start_ovt_time = 0.0
                for cal in working_schedule_cal:
                    checkin = datetime.strftime(cal.checkin.astimezone(local), '%Y-%m-%d %H:%M:%S')
                    checkin = datetime.strptime(str(checkin), '%Y-%m-%d %H:%M:%S').date()
                    if checkin == act.date:
                        start_ovt_time = cal.hour_to
                    day_dict += [checkin]

                shortday_dict = []
                for shortcal in shortday_schedule_cal:
                    checkin = datetime.strftime(shortcal.checkin.astimezone(local), '%Y-%m-%d %H:%M:%S')
                    checkin = datetime.strptime(str(checkin), '%Y-%m-%d %H:%M:%S').date()
                    shortday_dict += [checkin]

                wt_overtime_rule = working_time.overtime_rules_id
                day_name = act.date.strftime("%A")

                contract_ids = act.employee_id.contract_id.ids or \
                               self.get_contract(act.employee_id, act.date, act.date)

                contracts = self.env['hr.contract'].browse(contract_ids)
                slip_lines = self._get_payslip_lines(contract_ids, contracts[0].id)
                salary_amounts = 0.0
                for rec in slip_lines:
                    if rec['amount_select'] == 'fix':
                        amount = rec['amount_fix']
                    elif rec['amount_select'] == 'percentage':
                        amount = rec['amount'] * rec['rate'] / 100
                    else:
                        amount = rec['amount']
                    salary_amounts += amount

                works_day = wt_overtime_rule.works_day
                last_works_day_line = works_day.search([], order='id desc', limit=1).id
                off_days_working = wt_overtime_rule.off_days_working
                last_off_days_working_line = off_days_working.search([], order='id desc', limit=1).id
                off_days_working_per_week = wt_overtime_rule.off_days_working_per_week
                last_off_days_working_per_week_line = off_days_working_per_week.search([], order='id desc', limit=1).id
                off_days_public_holiday = wt_overtime_rule.off_days_public_holiday
                last_off_days_public_holiday_line = off_days_public_holiday.search([], order='id desc', limit=1).id

                #overtime rules meal allowance
                meal_alw_works_day = wt_overtime_rule.meal_allowance_work_days
                meal_alw_offtime_five_day = wt_overtime_rule.meal_allowance_offtime_five_days
                meal_alw_offtime_six_day = wt_overtime_rule.meal_allowance_offtime_six_days
                meal_allowance_off_public_holiday = wt_overtime_rule.meal_allowance_off_public_holiday

                total_coefficient = 0.0
                amounts = 0.0
                counter = 1
                hours = act.actual_hours
                alw_amount = 0.0

                # if act.date in day_dict and act.actual_start_time >= start_ovt_time:
                if act.date in shortday_dict:
                    for rec in off_days_public_holiday:
                        if rec.hour < hours:
                            while counter <= rec.hour:
                                total_coefficient += rec.values
                                if rec.fix_amount:
                                    amounts += rec.amount
                                hours -= 1
                                counter += 1
                        elif counter <= rec.hour:
                            while counter <= rec.hour and counter <= act.actual_hours:
                                total_coefficient += rec.values
                                if rec.fix_amount:
                                    amounts += rec.amount
                                hours -= 1
                                counter += 1
                            if rec.id == last_off_days_public_holiday_line:
                                if act.actual_hours > rec.hour:
                                    total_coefficient += rec.values
                                    if rec.fix_amount:
                                        amounts += rec.amount
                    for meal in meal_allowance_off_public_holiday:
                        if act.actual_hours >= meal.minimum_hours:
                            alw_amount = meal.meal_allowance
                elif act.date in day_dict:
                    for rec in works_day:
                        if rec.hour < hours:
                            while counter <= rec.hour:
                                total_coefficient += rec.values
                                if rec.fix_amount:
                                    amounts += rec.amount
                                hours -= 1
                                counter += 1
                        elif counter <= rec.hour:
                            while counter <= rec.hour and counter <= act.actual_hours:
                                total_coefficient += rec.values
                                if rec.fix_amount:
                                    amounts += rec.amount
                                hours -= 1
                                counter += 1
                            if rec.id == last_works_day_line:
                                if act.actual_hours > rec.hour:
                                    total_coefficient += rec.values
                                    if rec.fix_amount:
                                        amounts += rec.amount
                    for meal in meal_alw_works_day:
                        if act.actual_hours >= meal.minimum_hours:
                            alw_amount = meal.meal_allowance
                elif act.date not in day_dict and week_working_day == 5:
                    for rec in off_days_working:
                        if rec.hour < hours:
                            while counter <= rec.hour:
                                total_coefficient += rec.values
                                if rec.fix_amount:
                                    amounts += rec.amount
                                hours -= 1
                                counter += 1
                        elif counter <= rec.hour:
                            while counter <= rec.hour and counter <= act.actual_hours:
                                total_coefficient += rec.values
                                if rec.fix_amount:
                                    amounts += rec.amount
                                hours -= 1
                                counter += 1
                            if rec.id == last_off_days_working_line:
                                if act.actual_hours > rec.hour:
                                    total_coefficient += rec.values
                                    if rec.fix_amount:
                                        amounts += rec.amount
                    for meal in meal_alw_offtime_five_day:
                        if act.actual_hours >= meal.minimum_hours:
                            alw_amount = meal.meal_allowance
                elif act.date not in day_dict and week_working_day == 6:
                    for rec in off_days_working_per_week:
                        if rec.hour < hours:
                            while counter <= rec.hour:
                                total_coefficient += rec.values
                                if rec.fix_amount:
                                    amounts += rec.amount
                                hours -= 1
                                counter += 1
                        elif counter <= rec.hour:
                            while counter <= rec.hour and counter <= act.actual_hours:
                                total_coefficient += rec.values
                                if rec.fix_amount:
                                    amounts += rec.amount
                                hours -= 1
                                counter += 1
                            if rec.id == last_off_days_working_per_week_line:
                                if act.actual_hours > rec.hour:
                                    total_coefficient += rec.values
                                    if rec.fix_amount:
                                        amounts += rec.amount
                    for meal in meal_alw_offtime_six_day:
                        if act.actual_hours >= meal.minimum_hours:
                            alw_amount = meal.meal_allowance
                total_amount = ((total_coefficient * salary_amounts) / 173) + amounts
                if overtime_rounding:
                    if overtime_rounding_type == 'round':
                        total_amount = round(total_amount,-abs(int(overtime_rounding_digit)))
                    if overtime_rounding_type == 'round_up':
                        total_amount = self.round_up(total_amount,-abs(int(overtime_rounding_digit)))
                    elif overtime_rounding_type == 'round_down':
                        total_amount = self.round_down(total_amount,-abs(int(overtime_rounding_digit)))
                act.update({
                    'coefficient_hours': total_coefficient,
                    'amount': total_amount,
                    'meal_allowance': alw_amount
                })
            ovt.update({
                'is_calculated': True
            })

    @api.model
    def _get_payslip_lines(self, contract_ids, contract_id):

        def _sum_salary_rule_category(localdict, category, amount):
            if category.parent_id:
                localdict = _sum_salary_rule_category(localdict, category.parent_id, amount)
            localdict['categories'].dict[category.code] = category.code in localdict['categories'].dict and \
                                                          localdict['categories'].dict[category.code] + amount or amount
            return localdict

        class BrowsableObject(object):
            def __init__(self, employee_id, dict, env):
                self.employee_id = employee_id
                self.dict = dict
                self.env = env

            def __getattr__(self, attr):
                return attr in self.dict and self.dict.__getitem__(attr) or 0.0

        # we keep a dict with the result because a value can be overwritten by another rule with the same code
        result_dict = {}
        rules_dict = {}
        blacklist = []
        contract_obj = self.env['hr.contract'].browse(contract_id)

        categories = BrowsableObject(contract_obj.employee_id.id, {}, self.env)
        rules = BrowsableObject(contract_obj.employee_id.id, rules_dict, self.env)
        payslips = self.env['hr.payslip'].browse()
        inputs = self.env['hr.payslip.input'].browse()
        worked_days = self.env['hr.payslip.worked_days'].browse()

        baselocaldict = {'categories': categories, 'rules': rules, 'payslip': payslips, 'worked_days': worked_days,
                         'inputs': inputs}
        # get the ids of the structures on the contracts and their parent id as well
        contracts = self.env['hr.contract'].browse(contract_ids)
        if len(contracts) == 1 and contract_obj.struct_id:
            structure_ids = list(set(contract_obj.struct_id._get_parent_structure().ids))
        else:
            structure_ids = contracts.get_all_structures()
        # get the rules of the structure and thier children
        rule_ids = self.env['hr.payroll.structure'].browse(structure_ids).get_all_rules()
        # run the rules by sequence
        sorted_rule_ids = [id for id, sequence in sorted(rule_ids, key=lambda x: x[1])]
        sorted_rules = self.env['hr.salary.rule'].browse(sorted_rule_ids)
        sorted_rules = sorted_rules.filtered(lambda line: line.apply_to_overtime_calculation == True)

        for contract in contracts:
            employee = contract.employee_id
            localdict = dict(baselocaldict, employee=employee, contract=contract)
            for rule in sorted_rules:
                key = rule.code + '-' + str(contract.id)
                localdict['result'] = None
                localdict['result_qty'] = 1.0
                localdict['result_rate'] = 100
                # check if the rule can be applied
                if rule._satisfy_condition(localdict) and rule.id not in blacklist:
                    # compute the amount of the rule
                    try:
                        amount, qty, rate = rule._compute_rule(localdict)
                    except:
                        amount = 0.0
                        qty = 1.0
                        rate = 100
                    # check if there is already a rule computed with that code
                    previous_amount = rule.code in localdict and localdict[rule.code] or 0.0
                    # set/overwrite the amount computed for this rule in the localdict
                    tot_rule = amount * qty * rate / 100.0
                    localdict[rule.code] = tot_rule
                    rules_dict[rule.code] = rule
                    # sum the amount for its salary category
                    localdict = _sum_salary_rule_category(localdict, rule.category_id, tot_rule - previous_amount)
                    # create/overwrite the rule in the temporary results
                    result_dict[key] = {
                        'salary_rule_id': rule.id,
                        'contract_id': contract.id,
                        'name': rule.name,
                        'code': rule.code,
                        'category_id': rule.category_id.id,
                        'sequence': rule.sequence,
                        'apply_to_overtime_calculation': rule.apply_to_overtime_calculation,
                        'amount_select': rule.amount_select,
                        'amount_fix': rule.amount_fix,
                        'amount_percentage': rule.amount_percentage,
                        'amount': amount,
                        'employee_id': contract.employee_id.id,
                        'quantity': qty,
                        'rate': rate,
                    }
                else:
                    # blacklist this rule and its children
                    blacklist += [id for id, seq in rule._recursive_search_of_rules()]

        return list(result_dict.values())

    def get_url(self, obj):
        url = ''
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        menu_id = self.env['ir.model.data'].get_object_reference(
            'equip3_hr_attendance_overtime', 'hr_actual_overtime_approval_menu')[1]
        action_id = self.env['ir.model.data'].get_object_reference(
            'equip3_hr_attendance_overtime', 'hr_overtime_actual_approval_act_window')[1]
        url = base_url + "/web?db=" + str(self._cr.dbname) + "#id=" + str(
            obj.id) + "&view_type=form&model=hr.overtime.actualt&menu_id=" + str(
            menu_id) + "&action=" + str(action_id)
        return url

    def approver_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.actual_approval_line_ids:
                matrix_line = sorted(rec.actual_approval_line_ids.filtered(lambda r: r.is_approve == True))
                approver = rec.actual_approval_line_ids[len(matrix_line)]
                for user in approver.approver_id:
                    try:
                        template_id = ir_model_data.get_object_reference(
                            'equip3_hr_attendance_overtime',
                            'email_template_approver_of_actual_approval')[1]
                    except ValueError:
                        template_id = False
                    ctx = self._context.copy()
                    url = self.get_url(self)
                    ctx.update({
                        'email_from': self.env.user.email,
                        'email_to': user.email,
                        'url': url,
                        'approver_name': user.name,
                        'emp_name': self.employee_id.name,
                    })
                    if self.period_start:
                        ctx.update(
                            {'date_from': fields.Datetime.from_string(self.period_start).strftime('%d/%m/%Y')})
                    if self.period_end:
                        ctx.update(
                            {'date_to': fields.Datetime.from_string(self.period_end).strftime('%d/%m/%Y')})
                    self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                                  force_send=True)
                break

    def approved_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.actual_approval_line_ids:
                try:
                    template_id = ir_model_data.get_object_reference(
                        'equip3_hr_attendance_overtime',
                        'email_template_approved_actual_overtime')[1]
                except ValueError:
                    template_id = False
                ctx = self._context.copy()
                url = self.get_url(self)
                ctx.update({
                    'email_from': self.env.user.email,
                    'email_to': self.employee_id.user_id.email,
                    'url': url,
                    'emp_name': self.employee_id.name,
                })
                if self.period_start:
                    ctx.update(
                        {'date_from': fields.Datetime.from_string(self.period_start).strftime('%d/%m/%Y')})
                if self.period_end:
                    ctx.update(
                        {'date_to': fields.Datetime.from_string(self.period_end).strftime('%d/%m/%Y')})
                self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                          force_send=True)
            break

    def reject_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.actual_approval_line_ids:
                try:
                    template_id = ir_model_data.get_object_reference(
                        'equip3_hr_attendance_overtime',
                        'email_template_rejection_of_actual_overtime_req')[1]
                except ValueError:
                    template_id = False
                ctx = self._context.copy()
                url = self.get_url(self)
                ctx.update({
                    'email_from': self.env.user.email,
                    'email_to': self.employee_id.user_id.email,
                    'url': url,
                    'emp_name': self.employee_id.name,
                })
                if self.period_start:
                    ctx.update(
                        {'date_from': fields.Datetime.from_string(self.period_start).strftime('%d/%m/%Y')})
                if self.period_end:
                    ctx.update(
                        {'date_to': fields.Datetime.from_string(self.period_end).strftime('%d/%m/%Y')})
                self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(rec.id,
                                                                                          force_send=True)
            break

    def approver_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_overtime.send_by_wa_overtimes')
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        url = self.get_url(self)
        if send_by_wa:
            template = self.env.ref('equip3_hr_attendance_overtime.actual_overtime_approver_wa_template')
            if template:
                if self.actual_approval_line_ids:
                    matrix_line = sorted(self.actual_approval_line_ids.filtered(lambda r: r.is_approve == True))
                    approver = self.actual_approval_line_ids[len(matrix_line)]
                    for user in approver.approver_id:
                        string_test = str(template.message)
                        if "${employee_name}" in string_test:
                            string_test = string_test.replace("${employee_name}", self.employee_id.name)
                        if "${name}" in string_test:
                            string_test = string_test.replace("${name}", self.name)
                        if "${start_date}" in string_test:
                            string_test = string_test.replace("${start_date}", fields.Datetime.from_string(
                                self.period_start).strftime('%d/%m/%Y'))
                        if "${end_date}" in string_test:
                            string_test = string_test.replace("${end_date}", fields.Datetime.from_string(
                                self.period_end).strftime('%d/%m/%Y'))
                        if "${approver_name}" in string_test:
                            string_test = string_test.replace("${approver_name}", user.name)
                        if "${br}" in string_test:
                            string_test = string_test.replace("${br}", f"\n")
                        if "${url}" in string_test:
                            string_test = string_test.replace("${url}", url)
                        phone_num = str(user.mobile_phone)
                        if "+" in phone_num:
                            phone_num = int(phone_num.replace("+", ""))
                        param = {'body': string_test, 'phone': phone_num}
                        domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                        token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                        try:
                            request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                        except ConnectionError:
                            raise ValidationError("Not connect to API Chat Server. Limit reached or not active")

    def approved_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_overtime.send_by_wa_overtimes')
        if send_by_wa:
            template = self.env.ref('equip3_hr_attendance_overtime.actual_overtime_approved_wa_template')
            url = self.get_url(self)
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            if template:
                if self.actual_approval_line_ids:
                    string_test = str(template.message)
                    if "${employee_name}" in string_test:
                        string_test = string_test.replace("${employee_name}", self.employee_id.name)
                    if "${name}" in string_test:
                        string_test = string_test.replace("${name}", self.name)
                    if "${start_date}" in string_test:
                        string_test = string_test.replace("${start_date}", fields.Datetime.from_string(
                            self.period_start).strftime('%d/%m/%Y'))
                    if "${end_date}" in string_test:
                        string_test = string_test.replace("${end_date}", fields.Datetime.from_string(
                            self.period_end).strftime('%d/%m/%Y'))
                    if "${br}" in string_test:
                        string_test = string_test.replace("${br}", f"\n")
                    phone_num = str(self.employee_id.mobile_phone)
                    if "+" in phone_num:
                        phone_num = int(phone_num.replace("+", ""))
                    if "${url}" in string_test:
                        string_test = string_test.replace("${url}", url)
                    param = {'body': string_test, 'phone': phone_num}
                    domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                    token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                    try:
                        request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,
                                                       headers=headers, verify=True)
                    except ConnectionError:
                        raise ValidationError("Not connect to API Chat Server. Limit reached or not active")

    def rejected_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_overtime.send_by_wa_overtimes')
        if send_by_wa:
            template = self.env.ref('equip3_hr_attendance_overtime.actual_overtime_rejected_wa_template')
            url = self.get_url(self)
            if template:
                if self.actual_approval_line_ids:
                    string_test = str(template.message)
                    if "${employee_name}" in string_test:
                        string_test = string_test.replace("${employee_name}", self.employee_id.name)
                    if "${name}" in string_test:
                        string_test = string_test.replace("${name}", self.name)
                    if "${start_date}" in string_test:
                        string_test = string_test.replace("${start_date}", fields.Datetime.from_string(
                            self.period_start).strftime('%d/%m/%Y'))
                    if "${end_date}" in string_test:
                        string_test = string_test.replace("${end_date}", fields.Datetime.from_string(
                            self.period_end).strftime('%d/%m/%Y'))
                    if "${br}" in string_test:
                        string_test = string_test.replace("${br}", f"\n")
                    phone_num = str(self.employee_id.mobile_phone)
                    if "+" in phone_num:
                        phone_num = int(phone_num.replace("+", ""))
                    param = {'body': string_test, 'phone': phone_num}
                    domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                    token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                    try:
                        request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,
                                                       headers=headers, verify=True)
                    except ConnectionError:
                        raise ValidationError("Not connect to API Chat Server. Limit reached or not active")

class Equip3HrOvertimeActualLine(models.Model):
    _name = 'hr.overtime.actual.line'
    _order = 'create_date desc'

    @api.returns('self')
    def _get_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1) or False

    actual_id = fields.Many2one('hr.overtime.actual')
    employee_id = fields.Many2one('hr.employee', string='Employee', default=_get_employee, required=True)
    overtime_reason = fields.Char('Overtime Reason')
    date = fields.Date('Date')
    name_of_day = fields.Char('Name of Day', store=True, readonly=True, compute='_get_name_of_day')
    start_time = fields.Float('Start Time', readonly=True)
    end_time = fields.Float('End Time', readonly=True)
    hours = fields.Float('Hours', readonly=True)
    actual_start_time = fields.Float('Actual Start Time', required=True)
    actual_end_time = fields.Float('Actual End Time', required=True)
    break_time = fields.Float('Break Time', store=True, readonly=True, compute='_compute_break_time')
    actual_hours = fields.Float('Actual Hours', store=True, readonly=True, compute='_compute_actual_hours')
    coefficient_hours = fields.Float('Coefficient Hours')
    amount = fields.Float('Overtime Fee')
    meal_allowance = fields.Float('Meal Allowance')
    state = fields.Selection([('draft', 'Draft'), ('to_approve', 'To Approve'), ('approved', 'Approved'),
                              ('rejected', 'Rejected')], default='draft', string='Status')
    def custom_menu(self):
        views = [(self.env.ref('equip3_hr_attendance_overtime.hr_overtime_actual_lines_tree_view').id, 'tree')]
        if  self.env.user.has_group('equip3_hr_attendance_overtime.group_overtime_self_service') and not self.env.user.has_group('equip3_hr_attendance_overtime.group_overtime_team_approver'):
            return {
                'type': 'ir.actions.act_window',
                'name': 'Actual Overtimes Lines',
                'res_model': 'hr.overtime.actual.line',
                'view_mode': 'tree',
                'views':views,
                'domain': [('employee_id.user_id', '=', self.env.user.id)],
                'context':{},
                'help':"""<p class="oe_view_nocontent_create">
                    Click Create to add new Actual Overtimes Lines.
                </p>"""
        }
        else:
            return {
                'type': 'ir.actions.act_window',
                'name': 'Actual Overtimes Lines',
                'res_model': 'hr.overtime.actual.line',
                'view_mode': 'tree',
                'views':views,
                'domain': [],
                'context':{},
                'help':"""<p class="oe_view_nocontent_create">
                    Click Create to add new Actual Overtimes Lines.
                </p>"""
        }
            
    @api.model
    def create(self, vals):
        if self.search([('date', '=', vals.get('date')), ('id', '!=', vals.get('id')), ('employee_id', '=', vals.get('employee_id')),
                        ('actual_id', '!=', vals.get('actual_id')), ('state', 'not in', ['draft','rejected'])]):
            raise ValidationError(_('Date %s has been actualized!') % (
                vals.get('date')))
        elif self.search([('date', '=', vals.get('date')), ('id', '!=', vals.get('id')), ('employee_id', '=', vals.get('employee_id')),
                          ('actual_id', '=', vals.get('actual_id'))]):
            raise ValidationError(_('Duplicate date for %s !') % (
                vals.get('date')))
        return super(Equip3HrOvertimeActualLine, self).create(vals)

    def write(self, vals):
        res = super(Equip3HrOvertimeActualLine, self).write(vals)
        for rec in self:
            if self.search([('date', '=', rec.date), ('id', '!=', rec.id), ('actual_id', '!=', rec.actual_id.id), ('employee_id', '=', rec.employee_id.id),
                            ('state', 'not in', ['draft','rejected'])]):
                raise ValidationError(_('Date %s has been actualized!') % (rec.date))
            elif self.search([('date', '=', rec.date), ('id', '!=', rec.id), ('actual_id', '=', rec.actual_id.id),
                              ('employee_id', '=', rec.employee_id.id),]):
                raise ValidationError(_('Duplicate date for %s !') % (rec.date))
        return res

    @api.depends('actual_start_time', 'actual_end_time')
    def _compute_break_time(self):
        for rec in self:
            user_tz = rec.employee_id.tz or pytz.utc
            local = pytz.timezone(user_tz)
            working_time = rec.employee_id.resource_calendar_id
            wt_overtime_rule = working_time.overtime_rules_id
            week_working_day = working_time.week_working_day
            working_schedule_cal = self.env['employee.working.schedule.calendar'].search(
                [('employee_id', '=', rec.employee_id.id)])
            shortday_schedule_cal = self.env['employee.working.schedule.calendar'].search([('is_holiday', '=', True),
                                                                                           ('dayofweek', '=', 4)])

            day_dict = []
            start_ovt_time = 0.0
            for cal in working_schedule_cal:
                checkin = datetime.strftime(cal.checkin.astimezone(local), '%Y-%m-%d %H:%M:%S')
                checkin = datetime.strptime(str(checkin), '%Y-%m-%d %H:%M:%S').date()
                if checkin == rec.date:
                    start_ovt_time = cal.hour_to
                day_dict += [checkin]

            shortday_dict = []
            for shortcal in shortday_schedule_cal:
                checkin = datetime.strftime(shortcal.checkin.astimezone(local), '%Y-%m-%d %H:%M:%S')
                checkin = datetime.strptime(str(checkin), '%Y-%m-%d %H:%M:%S').date()
                shortday_dict += [checkin]

            # rule break time
            break_time_work_days = wt_overtime_rule.break_time_work_days
            break_time_offtime_five_days = wt_overtime_rule.break_time_offtime_five_days
            break_time_offtime_six_days = wt_overtime_rule.break_time_offtime_six_days
            break_time_off_public_holiday = wt_overtime_rule.break_time_off_public_holiday

            break_time = 0.0

            if rec.date in shortday_dict:
                for breaks in break_time_off_public_holiday:
                    if (rec.actual_end_time - rec.actual_start_time) >= breaks.minimum_hours:
                        break_time = breaks.break_time
            elif rec.date in day_dict:
                for breaks in break_time_work_days:
                    if (rec.actual_end_time - rec.actual_start_time) >= breaks.minimum_hours:
                        break_time = breaks.break_time
            elif rec.date not in day_dict and week_working_day == 5:
                for breaks in break_time_offtime_five_days:
                    if (rec.actual_end_time - rec.actual_start_time) >= breaks.minimum_hours:
                        break_time = breaks.break_time
            elif rec.date not in day_dict and week_working_day == 6:
                for breaks in break_time_offtime_six_days:
                    if (rec.actual_end_time - rec.actual_start_time) >= breaks.minimum_hours:
                        break_time = breaks.break_time

            rec.update({
                'break_time': break_time
            })

    @api.depends('actual_start_time', 'actual_end_time')
    def _compute_actual_hours(self):
        for rec in self:
            user_tz = rec.employee_id.tz or pytz.utc
            local = pytz.timezone(user_tz)
            working_time = rec.employee_id.resource_calendar_id
            wt_overtime_rule = working_time.overtime_rules_id
            week_working_day = working_time.week_working_day
            working_schedule_cal = self.env['employee.working.schedule.calendar'].search(
                [('employee_id', '=', rec.employee_id.id)])
            shortday_schedule_cal = self.env['employee.working.schedule.calendar'].search([('is_holiday', '=', True),
                                                                                           ('dayofweek', '=', 4)])

            day_dict = []
            start_ovt_time = 0.0
            for cal in working_schedule_cal:
                checkin = datetime.strftime(cal.checkin.astimezone(local), '%Y-%m-%d %H:%M:%S')
                checkin = datetime.strptime(str(checkin), '%Y-%m-%d %H:%M:%S').date()
                if checkin == rec.date:
                    start_ovt_time = cal.hour_to
                day_dict += [checkin]

            shortday_dict = []
            for shortcal in shortday_schedule_cal:
                checkin = datetime.strftime(shortcal.checkin.astimezone(local), '%Y-%m-%d %H:%M:%S')
                checkin = datetime.strptime(str(checkin), '%Y-%m-%d %H:%M:%S').date()
                shortday_dict += [checkin]

            works_day = wt_overtime_rule.works_day
            last_works_day_line = works_day.search([], order='id desc', limit=1)
            off_days_working = wt_overtime_rule.off_days_working
            last_off_days_working_line = off_days_working.search([], order='id desc', limit=1)
            off_days_working_per_week = wt_overtime_rule.off_days_working_per_week
            last_off_days_working_per_week_line = off_days_working_per_week.search([], order='id desc', limit=1)
            off_days_public_holiday = wt_overtime_rule.off_days_public_holiday
            last_off_days_public_holiday_line = off_days_public_holiday.search([], order='id desc', limit=1)

            # if rec.actual_start_time and rec.actual_end_time:
            actual_hours = rec.actual_end_time - rec.actual_start_time
            actual_mins = actual_hours * 60
            actual_hour, actual_min = divmod(actual_mins, 60)
            if actual_mins >= wt_overtime_rule.minimum_time:
                if actual_min >= wt_overtime_rule.overtime_rounding:
                    if rec.date in shortday_dict:
                        if (actual_hour + 1.0) > last_off_days_public_holiday_line.hour:
                            rec.update({
                                'actual_hours': last_off_days_public_holiday_line.hour - rec.break_time
                            })
                        else:
                            rec.update({
                                'actual_hours': (actual_hour + 1.0) - rec.break_time
                            })
                    elif rec.date in day_dict:
                        if (actual_hour + 1.0) > last_works_day_line.hour:
                            rec.update({
                                'actual_hours': last_works_day_line.hour - rec.break_time
                            })
                        else:
                            rec.update({
                                'actual_hours': (actual_hour + 1.0) - rec.break_time
                            })
                    elif rec.date not in day_dict and week_working_day == 5:
                        if (actual_hour + 1.0) > last_off_days_working_line.hour:
                            rec.update({
                                'actual_hours': last_off_days_working_line.hour - rec.break_time
                            })
                        else:
                            rec.update({
                                'actual_hours': (actual_hour + 1.0) - rec.break_time
                            })
                    elif rec.date not in day_dict and week_working_day == 6:
                        if (actual_hour + 1.0) > last_off_days_working_per_week_line.hour:
                            rec.update({
                                'actual_hours': last_off_days_working_per_week_line.hour - rec.break_time
                            })
                        else:
                            rec.update({
                                'actual_hours': (actual_hour + 1.0) - rec.break_time
                            })
                else:
                    if rec.date in shortday_dict:
                        if actual_hour > last_off_days_public_holiday_line.hour:
                            rec.update({
                                'actual_hours': last_off_days_public_holiday_line.hour - rec.break_time
                            })
                        else:
                            rec.update({
                                'actual_hours': actual_hour - rec.break_time
                            })
                    elif rec.date in day_dict:
                        if actual_hour > last_works_day_line.hour:
                            rec.update({
                                'actual_hours': last_works_day_line.hour - rec.break_time
                            })
                        else:
                            rec.update({
                                'actual_hours': actual_hour - rec.break_time
                            })
                    elif rec.date not in day_dict and week_working_day == 5:
                        if actual_hour > last_off_days_working_line.hour:
                            rec.update({
                                'actual_hours': last_off_days_working_line.hour - rec.break_time
                            })
                        else:
                            rec.update({
                                'actual_hours': actual_hour - rec.break_time
                            })
                    elif rec.date not in day_dict and week_working_day == 6:
                        if actual_hour > last_off_days_working_per_week_line.hour:
                            rec.update({
                                'actual_hours': last_off_days_working_per_week_line.hour - rec.break_time
                            })
                        else:
                            rec.update({
                                'actual_hours': actual_hour - rec.break_time
                            })
            else:
                rec.update({
                    'actual_hours': 0.0
                })

    @api.depends('date')
    def _get_name_of_day(self):
        for rec in self:
            if rec.date:
                name_day = rec.date.strftime("%A")
                rec.update({
                    'name_of_day': name_day
                })

class Equip3HrOvertimeActualApprovalLine(models.Model):
    _name = 'hr.overtime.actual.approval.line'

    actual_id = fields.Many2one('hr.overtime.actual')
    sequence = fields.Integer('Sequence')
    approver_id = fields.Many2many('res.users', string="Approvers")
    approver_confirm = fields.Many2many('res.users', 'overtime_actual_line_user_approve_ids', 'user_id', string="Approvers confirm")
    approval_status = fields.Char('Approval Status')
    timestamp = fields.Text('Timestamp')
    feedback = fields.Text('Feedback')
    minimum_approver = fields.Integer(default=1)
    is_approve = fields.Boolean(string="Is Approve", default=False)

class Equip3HrOvertimeActualAttendanceLine(models.Model):
    _name = 'hr.overtime.actual.attendance.line'

    actual_id = fields.Many2one('hr.overtime.actual')
    attendance_id = fields.Many2one('hr.attendance')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    check_in = fields.Datetime('Check In')
    check_out = fields.Datetime('Check Out')
    worked_hours = fields.Float(string='Worked Hours')
    attendance_status = fields.Selection([('present', 'Present'), ('absent', 'Absent'), ('leave', 'Leave')],
                                         string='Attendance Status')

