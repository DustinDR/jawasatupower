# -*- coding: utf-8 -*-
import babel
from odoo import models, fields, api, tools, _
from datetime import datetime, timedelta, time
from odoo.exceptions import ValidationError
import requests
headers = {'content-type': 'application/json'}

class Equip3HrOvertimeRequest(models.Model):
    _name = 'hr.overtime.request'
    _description = 'HR Overtime Request'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'create_date desc'

    def name_get(self):
        result = []
        for rec in self:
            ttyme_start = datetime.combine(fields.Date.from_string(rec.period_start), time.min)
            ttyme_end = datetime.combine(fields.Date.from_string(rec.period_end), time.min)
            locale = self.env.context.get('lang') or 'en_US'
            period_start = tools.ustr(babel.dates.format_date(date=ttyme_start, format='dd MMM YYYY', locale=locale))
            period_end = tools.ustr(babel.dates.format_date(date=ttyme_end, format='dd MMM YYYY', locale=locale))
            name = rec.name + ' - ' + period_start + ' to ' + period_end
            result.append((rec.id, name))
        return result

    @api.returns('self')
    def _get_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1) or False

    name = fields.Char('Number')
    request_type = fields.Selection([('by_employee', 'By Employee'),
                                     ('by_manager', 'By Manager')], default='', string='Request Type')
    employee_id = fields.Many2one('hr.employee', string='Employee', default=_get_employee, required=True)
    employee_ids = fields.Many2many('hr.employee', string='Employees', domain="[('parent_id', '=', employee_id)]")
    description = fields.Text('Description', required=True)
    period_start = fields.Date('Period Start', required=True)
    period_end = fields.Date('Period End', required=True)
    total_hours = fields.Float('Total Hours', store=True, readonly=True, compute='_get_total_hours')
    company_id = fields.Many2one("res.company", string="Company", default=lambda self: self.env.company,
                                 track_visibility='onchange')
    state = fields.Selection([('draft', 'Draft'), ('to_approve', 'To Approve'), ('approved', 'Approved'),
                              ('rejected', 'Rejected')], default='draft', string='Status')
    request_line_ids = fields.One2many('hr.overtime.request.line', 'request_id')
    request_approval_line_ids = fields.One2many('hr.overtime.request.approval.line', 'request_id')
    is_hide_reject = fields.Boolean(default=True, compute='_get_is_hide')
    is_hide_approve = fields.Boolean(default=True, compute='_get_is_hide')
    user_approval_ids = fields.Many2many('res.users', compute="_is_hide_approve")
    overtime_wizard_state = fields.Char('OverTime Wizard State')

    @api.model
    def create(self, vals):
        sequence = self.env['ir.sequence'].next_by_code('hr.overtime.request')
        vals.update({'name': sequence})
        return super(Equip3HrOvertimeRequest, self).create(vals)

    @api.depends('request_approval_line_ids')
    def _is_hide_approve(self):
        for record in self:
            if record.request_approval_line_ids:
                sequence = [data.sequence for data in record.request_approval_line_ids.filtered(
                    lambda line: len(line.approver_confirm.ids) != line.minimum_approver)]
                if sequence:
                    minimum_sequence = min(sequence)
                    approve_user = record.request_approval_line_ids.filtered(lambda
                                                                           line: self.env.user.id in line.approver_id.ids and self.env.user.id not in line.approver_confirm.ids and line.sequence == minimum_sequence)
                    if approve_user:
                        record.user_approval_ids = [(6, 0, [self.env.user.id])]
                    else:
                        record.user_approval_ids = False
                else:
                    record.user_approval_ids = False
            else:
                record.user_approval_ids = False

    @api.depends('user_approval_ids')
    def _get_is_hide(self):
        for record in self:
            if not record.user_approval_ids:
                record.is_hide_approve = True
                record.is_hide_reject = True
            else:
                record.is_hide_approve = False
                record.is_hide_reject = False

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        for record in self:
            setting = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_overtime.approval_method')
            if record.request_type == 'by_employee':
                if record.employee_id:
                    record.employee_ids = [(4, record.employee_id.id)]
                    if record.request_approval_line_ids:
                        remove = []
                        for line in record.request_approval_line_ids:
                            remove.append((2, line.id))
                        record.request_approval_line_ids = remove
                    if setting == 'employee_hierarchy':
                        record.request_approval_line_ids = self.approval_by_hierarchy(record)
                    else:
                        self.approval_by_matrix(record)

            elif record.request_type == 'by_manager':
                if record.request_approval_line_ids:
                    remove = []
                    for line in record.request_approval_line_ids:
                        remove.append((2, line.id))
                    record.request_approval_line_ids = remove
                if setting == 'employee_hierarchy':
                    record.request_approval_line_ids = self.approval_by_hierarchy(record)
                else:
                    self.approval_by_matrix(record)

    def approval_by_matrix(self, record):
        approval_matrix = self.env['hr.overtime.approval.matrix'].search([('apply_to', '=', 'by_employee')])
        matrix = approval_matrix.filtered(lambda line: record.employee_id.id in line.employee_ids.ids)

        if matrix:
            data_approvers = []
            for line in matrix[0].approval_matrix_ids:
                data_approvers.append((0, 0, {'sequence': line.sequence, 'minimum_approver': line.minimum_approver,
                                              'approver_id': [(6, 0, line.approvers.ids)]}))
            record.request_approval_line_ids = data_approvers
        if not matrix:
            data_approvers = []
            approval_matrix = self.env['hr.overtime.approval.matrix'].search(
                [('apply_to', '=', 'by_job_position')])
            matrix = approval_matrix.filtered(lambda line: record.employee_id.job_id.id in line.job_ids.ids)
            if matrix:
                for line in matrix[0].approval_matrix_ids:
                    data_approvers.append((0, 0, {'sequence': line.sequence, 'minimum_approver': line.minimum_approver,
                                                  'approver_id': [(6, 0, line.approvers.ids)]}))
                record.request_approval_line_ids = data_approvers
            if not matrix:
                data_approvers = []
                approval_matrix = self.env['hr.overtime.approval.matrix'].search(
                    [('apply_to', '=', 'by_department')])
                matrix = approval_matrix.filtered(lambda line: record.employee_id.department_id.id in line.deparment_ids.ids)
                if matrix:
                    for line in matrix[0].approval_matrix_ids:
                        data_approvers.append((0, 0,
                                               {'sequence': line.sequence, 'minimum_approver': line.minimum_approver,
                                                'approver_id': [(6, 0, line.approvers.ids)]}))
                    record.request_approval_line_ids = data_approvers

    def approval_by_hierarchy(self,record):
        approval_ids = []
        seq = 1
        data = 0
        line = self.get_manager(record,record.employee_id,data,approval_ids,seq)
        return line

    def get_manager(self, record, employee_manager, data, approval_ids, seq):
        setting_level = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_overtime.approval_levels')
        if not setting_level:
            raise ValidationError("level not set")
        if not employee_manager['parent_id']['user_id']:
            return approval_ids
        while data < int(setting_level):
            approval_ids.append(
                (0, 0, {'sequence': seq, 'approver_id': [(4, employee_manager['parent_id']['user_id']['id'])]}))
            data += 1
            seq += 1
            if employee_manager['parent_id']['user_id']['id']:
                self.get_manager(record, employee_manager['parent_id'], data, approval_ids, seq)
                break

        return approval_ids

    @api.onchange('request_type', 'period_start', 'period_end', 'employee_ids')
    def onchange_period(self):
        if self.request_type == 'by_employee':
            if (not self.employee_id) or (not self.period_start) or (not self.period_end) or (not self.request_type):
                return
        elif self.request_type == 'by_manager':
            if (not self.period_start) or (not self.period_end) or (not self.request_type):
                return

        period_start = self.period_start
        period_end = self.period_end
        delta = period_end - period_start
        days = [period_start + timedelta(days=i) for i in range(delta.days + 1)]

        ttyme = datetime.combine(fields.Date.from_string(period_start), time.min)
        ttyme_end = datetime.combine(fields.Date.from_string(period_end), time.min)
        locale = self.env.context.get('lang') or 'en_US'
        self.description = _('Overtime request for period: %s to %s') % (
            tools.ustr(babel.dates.format_date(date=ttyme, format='dd MMM YYYY', locale=locale)), tools.ustr(babel.dates.format_date(date=ttyme_end, format='dd MMM YYYY', locale=locale)))

        request_line = []
        for date in days:
            if self.request_type == 'by_employee':
                input_data = {
                    'employee_id': self.employee_id.id,
                    'request_type': self.request_type,
                    'date': date,
                    'request_id': self.id,
                }
                request_line += [input_data]
            elif self.request_type == 'by_manager':
                for emp in self.employee_ids.ids:
                    input_data = {
                        'employee_id': emp,
                        'request_type': self.request_type,
                        'date': date,
                        'request_id': self.id,
                    }
                    request_line += [input_data]

        request_lines = self.request_line_ids.browse([])
        for r in request_line:
            request_lines += request_lines.new(r)
        self.request_line_ids = request_lines

    def confirm(self):
        for rec in self:
            if not rec.request_approval_line_ids:
                rec.state = "approved"
                rec.message_post(body=_('Periods Status: Draft -> Approved'))
            else:
                rec.state = "to_approve"
                rec.message_post(body=_('Periods Status: Draft -> To Approve'))

            for line in rec.request_line_ids:
                line.state = rec.state
            rec.approver_mail()
            rec.approver_wa_template()

    def approve(self):
        self.update({
            'overtime_wizard_state': 'approved',
        })
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'hr.overtime.approval.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'name':"Confirmation Message",
            'target': 'new',
            'context':{'default_overtime_id':self.id},
        }

    def reject(self):
        self.update({
            'overtime_wizard_state': 'rejected',
        })
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'hr.overtime.approval.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'name':"Confirmation Message",
            'target': 'new',
            'context':{'default_overtime_id':self.id},
        }

    @api.depends('request_line_ids.number_of_hours')
    def _get_total_hours(self):
        for rec in self:
            total_hours = 0.0
            for line in rec.request_line_ids:
                total_hours += line.number_of_hours
            rec.update({
                'total_hours': total_hours,
            })

    @api.constrains('request_line_ids')
    def _check_overtime_reason(self):
        for rec in self:
            for line in rec.request_line_ids:
                if not line.overtime_reason:
                    raise ValidationError(_("""You must filled overtime reason."""))

    def get_url(self, obj):
        url = ''
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        menu_id = self.env['ir.model.data'].get_object_reference(
            'equip3_hr_attendance_overtime', 'overtime_requests_approval_menu')[1]
        action_id = self.env['ir.model.data'].get_object_reference(
            'equip3_hr_attendance_overtime', 'overtime_requests_approval_act_window')[1]
        url = base_url + "/web?db=" + str(self._cr.dbname) + "#id=" + str(
            obj.id) + "&view_type=form&model=hr.overtime.request&menu_id=" + str(
            menu_id) + "&action=" + str(action_id)
        return url

    def approver_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.request_approval_line_ids:
                matrix_line = sorted(rec.request_approval_line_ids.filtered(lambda r: r.is_approve == True))
                approver = rec.request_approval_line_ids[len(matrix_line)]
                for user in approver.approver_id:
                    try:
                        template_id = ir_model_data.get_object_reference(
                            'equip3_hr_attendance_overtime',
                            'email_template_approver_of_overtime_req')[1]
                    except ValueError:
                        template_id = False
                    ctx = self._context.copy()
                    url = self.get_url(self)
                    ctx.update({
                        'email_from': self.env.user.email,
                        'email_to': user.email,
                        'url': url,
                        'approver_name': user.name,
                        'emp_name': self.employee_id.name,
                    })
                    if self.period_start:
                        ctx.update(
                            {'date_from': fields.Datetime.from_string(self.period_start).strftime('%d/%m/%Y')})
                    if self.period_end:
                        ctx.update(
                            {'date_to': fields.Datetime.from_string(self.period_end).strftime('%d/%m/%Y')})
                    self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                                  force_send=True)
                break

    def approved_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.request_approval_line_ids:
                try:
                    template_id = ir_model_data.get_object_reference(
                        'equip3_hr_attendance_overtime',
                        'email_template_approved_overtime')[1]
                except ValueError:
                    template_id = False
                ctx = self._context.copy()
                url = self.get_url(self)
                ctx.update({
                    'email_from': self.env.user.email,
                    'email_to': self.employee_id.user_id.email,
                    'url': url,
                    'emp_name': self.employee_id.name,
                })
                if self.period_start:
                    ctx.update(
                        {'date_from': fields.Datetime.from_string(self.period_start).strftime('%d/%m/%Y')})
                if self.period_end:
                    ctx.update(
                        {'date_to': fields.Datetime.from_string(self.period_end).strftime('%d/%m/%Y')})
                self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                          force_send=True)
            break

    def reject_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.request_approval_line_ids:
                try:
                    template_id = ir_model_data.get_object_reference(
                        'equip3_hr_attendance_overtime',
                        'email_template_rejection_of_overtime_req')[1]
                except ValueError:
                    template_id = False
                ctx = self._context.copy()
                url = self.get_url(self)
                ctx.update({
                    'email_from': self.env.user.email,
                    'email_to': self.employee_id.user_id.email,
                    'url': url,
                    'emp_name': self.employee_id.name,
                })
                if self.period_start:
                    ctx.update(
                        {'date_from': fields.Datetime.from_string(self.period_start).strftime('%d/%m/%Y')})
                if self.period_end:
                    ctx.update(
                        {'date_to': fields.Datetime.from_string(self.period_end).strftime('%d/%m/%Y')})
                self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(rec.id,
                                                                                          force_send=True)
            break

    def approver_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_overtime.send_by_wa_overtimes')
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        url = self.get_url(self)
        if send_by_wa:
            template = self.env.ref('equip3_hr_attendance_overtime.overtime_approver_wa_template')
            if template:
                if self.request_approval_line_ids:
                    matrix_line = sorted(self.request_approval_line_ids.filtered(lambda r: r.is_approve == True))
                    approver = self.request_approval_line_ids[len(matrix_line)]
                    for user in approver.approver_id:
                        string_test = str(template.message)
                        if "${employee_name}" in string_test:
                            string_test = string_test.replace("${employee_name}", self.employee_id.name)
                        if "${name}" in string_test:
                            string_test = string_test.replace("${name}", self.name)
                        if "${start_date}" in string_test:
                            string_test = string_test.replace("${start_date}", fields.Datetime.from_string(
                                self.period_start).strftime('%d/%m/%Y'))
                        if "${end_date}" in string_test:
                            string_test = string_test.replace("${end_date}", fields.Datetime.from_string(
                                self.period_end).strftime('%d/%m/%Y'))
                        if "${approver_name}" in string_test:
                            string_test = string_test.replace("${approver_name}", user.name)
                        if "${br}" in string_test:
                            string_test = string_test.replace("${br}", f"\n")
                        if "${url}" in string_test:
                            string_test = string_test.replace("${url}", url)
                        phone_num = str(user.mobile_phone)
                        if "+" in phone_num:
                            phone_num = int(phone_num.replace("+", ""))
                        param = {'body': string_test, 'phone': phone_num}
                        domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                        token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                        try:
                            request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                        except ConnectionError:
                            raise ValidationError("Not connect to API Chat Server. Limit reached or not active")

    def approved_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_overtime.send_by_wa_overtimes')
        if send_by_wa:
            template = self.env.ref('equip3_hr_attendance_overtime.overtime_approved_wa_template')
            url = self.get_url(self)
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            if template:
                if self.request_approval_line_ids:
                    string_test = str(template.message)
                    if "${employee_name}" in string_test:
                        string_test = string_test.replace("${employee_name}", self.employee_id.name)
                    if "${name}" in string_test:
                        string_test = string_test.replace("${name}", self.name)
                    if "${start_date}" in string_test:
                        string_test = string_test.replace("${start_date}", fields.Datetime.from_string(
                            self.period_start).strftime('%d/%m/%Y'))
                    if "${end_date}" in string_test:
                        string_test = string_test.replace("${end_date}", fields.Datetime.from_string(
                            self.period_end).strftime('%d/%m/%Y'))
                    if "${br}" in string_test:
                        string_test = string_test.replace("${br}", f"\n")
                    phone_num = str(self.employee_id.mobile_phone)
                    if "+" in phone_num:
                        phone_num = int(phone_num.replace("+", ""))
                    if "${url}" in string_test:
                        string_test = string_test.replace("${url}", url)
                    param = {'body': string_test, 'phone': phone_num}
                    domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                    token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                    try:
                        request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,
                                                       headers=headers, verify=True)
                    except ConnectionError:
                        raise ValidationError("Not connect to API Chat Server. Limit reached or not active")

    def rejected_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_overtime.send_by_wa_overtimes')
        if send_by_wa:
            template = self.env.ref('equip3_hr_attendance_overtime.overtime_rejected_wa_template')
            url = self.get_url(self)
            if template:
                if self.request_approval_line_ids:
                    string_test = str(template.message)
                    if "${employee_name}" in string_test:
                        string_test = string_test.replace("${employee_name}", self.employee_id.name)
                    if "${name}" in string_test:
                        string_test = string_test.replace("${name}", self.name)
                    if "${start_date}" in string_test:
                        string_test = string_test.replace("${start_date}", fields.Datetime.from_string(
                            self.period_start).strftime('%d/%m/%Y'))
                    if "${end_date}" in string_test:
                        string_test = string_test.replace("${end_date}", fields.Datetime.from_string(
                            self.period_end).strftime('%d/%m/%Y'))
                    if "${br}" in string_test:
                        string_test = string_test.replace("${br}", f"\n")
                    phone_num = str(self.employee_id.mobile_phone)
                    if "+" in phone_num:
                        phone_num = int(phone_num.replace("+", ""))
                    param = {'body': string_test, 'phone': phone_num}
                    domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                    token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                    try:
                        request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,
                                                       headers=headers, verify=True)
                    except ConnectionError:
                        raise ValidationError("Not connect to API Chat Server. Limit reached or not active")

class Equip3HrOvertimeRequestLine(models.Model):
    _name = 'hr.overtime.request.line'
    _order = 'create_date desc'

    @api.returns('self')
    def _get_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1) or False

    @api.returns('self')
    def _get_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1) or False

    request_id = fields.Many2one('hr.overtime.request')
    request_type = fields.Selection([('by_employee', 'By Employee'),
                                     ('by_manager', 'By Manager')], default='', string='Request Type')
    employee_id = fields.Many2one('hr.employee', string='Employee', default=_get_employee, required=True)
    overtime_reason = fields.Char('Overtime Reason')
    date = fields.Date('Date', readonly=True)
    name_of_day = fields.Char('Name of Day', store=True, readonly=True, compute='_get_name_of_day')
    start_time = fields.Float('Start Time', required=True)
    end_time = fields.Float('End Time', required=True)
    number_of_hours = fields.Float('Number of Hours', store=True, readonly=True, compute='_compute_number_of_hours')
    state = fields.Selection([('draft', 'Draft'), ('to_approve', 'To Approve'), ('approved', 'Approved'),
                              ('rejected', 'Rejected')], default='draft', string='Status')

    @api.depends('start_time', 'end_time')
    def _compute_number_of_hours(self):
        for rec in self:
            if rec.start_time and rec.end_time:
                number_of_hours = rec.end_time - rec.start_time
                rec.update({
                    'number_of_hours': number_of_hours
                })

    @api.depends('date')
    def _get_name_of_day(self):
        for rec in self:
            if rec.date:
                name_day = rec.date.strftime("%A")
                rec.update({
                    'name_of_day': name_day
                })

class Equip3HrOvertimeRequestApprovalLine(models.Model):
    _name = 'hr.overtime.request.approval.line'

    request_id = fields.Many2one('hr.overtime.request')
    sequence = fields.Integer('Sequence')
    approver_id = fields.Many2many('res.users', string="Approvers")
    approver_confirm = fields.Many2many('res.users', 'overtime_line_user_approve_ids', 'user_id', string="Approvers confirm")
    approval_status = fields.Text('Approval Status')
    timestamp = fields.Text('Timestamp')
    feedback = fields.Text('Feedback')
    minimum_approver = fields.Integer(default=1)
    is_approve = fields.Boolean(string="Is Approve", default=False)
