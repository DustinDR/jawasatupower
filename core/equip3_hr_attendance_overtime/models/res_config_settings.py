# -*- coding: utf-8 -*-
from odoo import models, fields, api, tools, _

class HrOvertimeResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    approval_method = fields.Selection(
        [('employee_hierarchy', 'By Employee Hierarchy'), ('approval_matrix', 'By Approval Matrix')],
        config_parameter='equip3_hr_attendance_overtime.approval_method', default='employee_hierarchy')
    approval_levels = fields.Integer(config_parameter='equip3_hr_attendance_overtime.approval_levels', default=1)

    overtime_rounding = fields.Boolean(config_parameter='equip3_hr_attendance_overtime.overtime_rounding', default=False)
    overtime_rounding_type = fields.Selection(
        [('round', 'Round'), ('round_up', 'Round-Up'), ('round_down', 'Round-Down')],
        config_parameter='equip3_hr_attendance_overtime.overtime_rounding_type', default='round')
    overtime_rounding_digit = fields.Integer(config_parameter='equip3_hr_attendance_overtime.overtime_rounding_digit')
    send_by_wa_overtimes = fields.Boolean(config_parameter='equip3_hr_attendance_overtime.send_by_wa_overtimes')
    send_by_mail_overtimes = fields.Boolean(config_parameter='equip3_hr_attendance_overtime.send_by_mail_overtimes',
                                           default=True)

    @api.onchange("approval_levels")
    def _onchange_approval_levels(self):
        if self.approval_levels < 1:
            self.approval_levels = 1