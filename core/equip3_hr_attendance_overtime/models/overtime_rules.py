from odoo import api,fields,models
from lxml import etree


class Equip3OvertimeRules(models.Model):
    _name = 'overtime.rules'
    _rec_name = 'name'
    name = fields.Char(required=1)
    code = fields.Char(required=1)
    minimum_time = fields.Integer(required=1)
    overtime_rounding = fields.Integer(required=1)
    company_id = fields.Many2one('res.company',default=lambda self:self.env.user.company_id.id)
    works_day = fields.One2many('overtime.work.days.line','overtime_id')
    off_days_working = fields.One2many('overtime.off.time.days','overtime_id')
    off_days_working_per_week = fields.One2many('overtime.off.time.days.week','overtime_id')
    off_days_public_holiday = fields.One2many('overtime.off.public.holiday','overtime_id')
    meal_allowance_work_days = fields.One2many('meal.allowance.work.days','overtime_rules_id')
    meal_allowance_offtime_five_days = fields.One2many('meal.allowance.offtime.five.days','overtime_rules_id')
    meal_allowance_offtime_six_days = fields.One2many('meal.allowance.offtime.six.days','overtime_rules_id')
    meal_allowance_off_public_holiday = fields.One2many('meal.allowance.off.public.holiday','overtime_rules_id')
    break_time_work_days = fields.One2many('break.time.work.days','overtime_rules_id')
    break_time_offtime_five_days = fields.One2many('break.time.offtime.five.days','overtime_rules_id')
    break_time_offtime_six_days = fields.One2many('break.time.offtime.six.days','overtime_rules_id')
    break_time_off_public_holiday = fields.One2many('break.time.off.public.holiday','overtime_rules_id')
    
    
    @api.model
    def fields_view_get(self, view_id=None, view_type=None,
                        toolbar=True, submenu=True):
        res = super(Equip3OvertimeRules, self).fields_view_get(
            view_id=view_id, view_type=view_type,toolbar=toolbar,submenu=submenu)
        if  self.env.user.has_group('equip3_hr_attendance_overtime.group_overtime_all_approver'):
            root = etree.fromstring(res['arch'])
            root.set('create', 'true')
            root.set('edit', 'true')
            root.set('delete', 'true')
            res['arch'] = etree.tostring(root)
        else:
            root = etree.fromstring(res['arch'])
            root.set('create', 'false')
            root.set('edit', 'false')
            root.set('delete', 'false')
            res['arch'] = etree.tostring(root)
            
        return res

class Equip3OvertimeWorkdays(models.Model):
    _name = 'overtime.work.days.line'
    hour = fields.Integer(string='Hours')
    values = fields.Float(string='Coefficient Hours')
    fix_amount = fields.Boolean()
    amount = fields.Float()
    overtime_id = fields.Many2one('overtime.rules')

    @api.onchange('fix_amount')
    def _onchange_fix_amount(self):
        for record in self:
            if record.fix_amount:
                record.values = 0
            if not record.fix_amount and record.amount > 0:
                record.amount=0

class OffDaysWorkingPerWeek(models.Model):
    _name = 'overtime.off.time.days'
    hour = fields.Integer(string='Hours')
    values = fields.Float(string='Coefficient Hours')
    fix_amount = fields.Boolean()
    amount = fields.Float()
    overtime_id = fields.Many2one('overtime.rules')


    @api.onchange('fix_amount')
    def _onchange_fix_amount(self):
        for record in self:
            if record.fix_amount:
                record.values = 0
            if  not record.fix_amount  and record.amount > 0:
                record.amount = 0



class OffDaysWorkingDaysPerweek(models.Model):
    _name = 'overtime.off.time.days.week'
    hour = fields.Integer(string='Hours')
    values = fields.Float(string='Coefficient Hours')
    fix_amount = fields.Boolean()
    amount = fields.Float()
    overtime_id = fields.Many2one('overtime.rules')



    @api.onchange('fix_amount')
    def _onchange_fix_amount(self):
        for record in self:
            if record.fix_amount:
                record.values = 0
            if not record.fix_amount and record.amount > 0:
                record.amount = 0



class PublicHolihay(models.Model):
    _name = 'overtime.off.public.holiday'
    hour = fields.Integer(string='Hours')
    values = fields.Float(string='Coefficient Hours')
    fix_amount = fields.Boolean()
    amount = fields.Float()
    overtime_id = fields.Many2one('overtime.rules')


    @api.onchange('fix_amount')
    def _onchange_fix_amount(self):
        for record in self:
            if record.fix_amount:
                record.values = 0
            if  record.fix_amount == False and record.amount > 0:
                record.amount = 0

class MealAllowanceWorkDays(models.Model):
    _name = 'meal.allowance.work.days'

    overtime_rules_id = fields.Many2one('overtime.rules')
    minimum_hours = fields.Integer(string='Minimum Hours')
    meal_allowance = fields.Float(string='Meal Allowance')

class MealAllowanceOfftimeFiveDays(models.Model):
    _name = 'meal.allowance.offtime.five.days'

    overtime_rules_id = fields.Many2one('overtime.rules')
    minimum_hours = fields.Integer(string='Minimum Hours')
    meal_allowance = fields.Float(string='Meal Allowance')

class MealAllowanceOfftimeSixDays(models.Model):
    _name = 'meal.allowance.offtime.six.days'

    overtime_rules_id = fields.Many2one('overtime.rules')
    minimum_hours = fields.Integer(string='Minimum Hours')
    meal_allowance = fields.Float(string='Meal Allowance')

class MealAllowanceOffPublicHoliday(models.Model):
    _name = 'meal.allowance.off.public.holiday'

    overtime_rules_id = fields.Many2one('overtime.rules')
    minimum_hours = fields.Integer(string='Minimum Hours')
    meal_allowance = fields.Float(string='Meal Allowance')

class BreakTimeWorkDays(models.Model):
    _name = 'break.time.work.days'

    overtime_rules_id = fields.Many2one('overtime.rules')
    minimum_hours = fields.Integer(string='Minimum Hours')
    break_time = fields.Float(string='Break Time')

class BreakTimeOfftimeFiveDays(models.Model):
    _name = 'break.time.offtime.five.days'

    overtime_rules_id = fields.Many2one('overtime.rules')
    minimum_hours = fields.Integer(string='Minimum Hours')
    break_time = fields.Float(string='Break Time')

class BreakTimeOfftimeSixDays(models.Model):
    _name = 'break.time.offtime.six.days'

    overtime_rules_id = fields.Many2one('overtime.rules')
    minimum_hours = fields.Integer(string='Minimum Hours')
    break_time = fields.Float(string='Break Time')

class BreakTimeOffPublicHoliday(models.Model):
    _name = 'break.time.off.public.holiday'

    overtime_rules_id = fields.Many2one('overtime.rules')
    minimum_hours = fields.Integer(string='Minimum Hours')
    break_time = fields.Float(string='Break Time')