{
    'name': 'Portal User Deliveryboy',
    'version': '1.1.2',
    'author': 'Pragmatic TechSoft Pvt Ltd.',
    'website': 'http://www.pragtech.co.in',
    'category': 'Website',
    'summary': '',
    'description': """ """,
    'depends': ['website', 'pragmatic_odoo_delivery_boy','pragmatic_configurable_order_stages'],
    'data': [
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
