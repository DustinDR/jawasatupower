# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Equip3 POS Report Menuitem',
    'author': 'Hashmicro',
    'version': '1.1.1',
    'summary': 'Manage your stock operation activities.',
    'depends': ['point_of_sale'],
    'category': 'Inventory/Inventory',
    'data': [
        'views/report_menu.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
