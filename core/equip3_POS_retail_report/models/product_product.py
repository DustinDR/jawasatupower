# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import _, api, fields, models

class Product(models.Model):
    _inherit = 'product.product'

    product_lots_ids = fields.One2many('stock.production.lot', 'product_id', string="Product Lots", tracking=True)
    