
from odoo import fields, models, api, _

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    is_valid_coupon = fields.Boolean(string="Valid Coupon", default=False)

    def _create_new_no_code_promo_reward_lines(self):
        res = super(SaleOrder, self)._create_new_no_code_promo_reward_lines()
        for record in self:
            if record.order_line.filtered(lambda r:r.is_reward_line):
                record.is_valid_coupon = True
        return res

    def _create_reward_line(self, program):
        res = super(SaleOrder, self)._create_reward_line(program)
        for record in self:
            record.is_valid_coupon = True
        return res

    def action_cancel_promo(self):
        self.order_line.filtered(lambda r:r.is_reward_line).unlink()
        self.write({'is_valid_coupon': False})
