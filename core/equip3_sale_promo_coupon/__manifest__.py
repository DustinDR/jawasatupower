# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Equip3 Sale Promo Coupon',
    'summary': """
        Manage your Sale Promotion and Coupon Programs""",
    'description': """
        This module manages these features :
        1. Promotion Programs
        2. Coupon Programs
    """,
    'author': "Hashmicro",
    'category': 'Sales',
    'version': '1.1.1',
    'depends': ['sale', 'coupon', 'sale_coupon'],
    'data': [
        'views/sale_order_line_views.xml'
    ],
    'installable': True,
}