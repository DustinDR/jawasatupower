About
============
Want to provide commission to salesman/salesperson for specific product & product categories? The "Sales Commission Based On Target "module allows you to pay sales commission based on the targets you set for each salesperson. In the target-based commission method, the salesman is provided with a range of targets after achieving those ranges the salesman entitled for the commission set up by the company. You can set the commission based on the product or product category. You can apply target on the amount as well as on a quantity basis. You can calculate the commission in 2 ways,

1) Based On Amount: That means commission calculate based on fixed amount.

2) Based On Percentage: Here the commission calculated based on the 2 factors,

A) Percentage On Sales Amount: The commission will be calculated based on the percentage of sales amount.
B) Percentage On Collection Amount: The commission will be calculated based on the percentage of the collection amount.

Installation
============
1) Copy module files to addon folder.
2) Restart odoo service (sudo service odoo-server restart).
3) Go to your odoo instance and open apps (make sure to activate debug mode).
4) click on update app list.
5) search module name and hit install button.

Any Problem with module?
=====================================
Please create your ticket here https://softhealer.com/support

Softhealer Technologies Doubt/Inquiry/Sales/Customization Team
=====================================
Skype: live:softhealertechnologies
What's app: +917984575681
E-Mail: support@softhealer.com
Website: https://softhealer.com
