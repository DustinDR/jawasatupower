# 模块赠送
在Odoo官方市场下载以下模块，然后给“英文五星好评”！
截图发送至 guohuadeng@hotmail.com，即可得到我们的回复邮件。
免费赠送以下三个模块，各价值 38.00 €

## 1. 彩色表格模块
可以按不同条件设置数据行为不同背景颜色，对于跟单十分好用。
如：10天内到期订单，显示红色背景，5天内到期显示黄色背景。
https://apps.odoo.com/apps/modules/13.0/app_web_tree_bgcolor/

## 2. 销售跟单模块.
彩色表格，不同时间，不同状态显示不同的标签或者背景色
https://apps.odoo.com/apps/modules/13.0/app_sale_date_alert/

## 3. 采购跟单模块. 
同上
https://apps.odoo.com/apps/modules/13.0/app_purchase_date_alert/

# 具体使用
每个模块都有使用说明，请自行查看。
注意点：
将模块1安装后，重启 odoo 服务。注意！一定要重启
模块2，3安装后，默认只有文字变色，如果要背景变色，将 views/*.xml文件更改。
将其中的 "decoration-**" 改为 "bg-**" 即可。例子如下
原
<attribute name="decoration-danger">sale_alert=='alert'</attribute>
变更后
<attribute name="bg-danger">sale_alert=='alert'</attribute>

# 模块下载地址为，一周后将失效
!! 注意，本模块为商业模块，故请自己商业使用，不要进行再次模块销售或者免费分享，谢谢！
https://pan.baidu.com/s/1bHyw25wPBxsrsoczIJ_YTw
提取码： nefc



