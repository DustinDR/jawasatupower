# -*- coding: utf-8 -*-
from odoo import fields, models, tools, api


class WizardChangeComponents(models.TransientModel):
    _name = 'mrp.change.component.wizard'
    _description = 'Change Components Wizard'

    production_id = fields.Many2one('mrp.production', string='Production')
    line_ids = fields.One2many('mrp.change.component.wizard.line', 'add_id')
    production_ids = fields.Many2many('mrp.production', string='Production IDs')

    def confirm(self):
        for line in self.line_ids:
            if line.move_id:
                line.move_id.sudo().write({
                    'product_id': line.product_id and line.product_id.id or False,
                    'product_uom_qty': line.product_qty,
                    'operation_id': line.operation_id and line.operation_id.id or False,
                    'raw_material_production_id': line.production_id and line.production_id.id or False
                })
            else:
                move_raw_values = line.production_id._get_move_raw_values(
                    product_id=line.product_id or False,
                    product_uom_qty=line.product_qty,
                    product_uom=line.product_uom or False,
                    operation_id=line.operation_id and line.operation_id.id or False
                )
                if 'location_id' in move_raw_values and not move_raw_values.get('location_id'):
                    move_raw_values['location_id'] = line.production_id.sudo().production_location_id and line.production_id.sudo().production_location_id.id or False
                if line.production_id.mrp_plan_id:
                    move_raw_values['mrp_plan_id'] = line.production_id.mrp_plan_id.id
                new_move = self.env['stock.move'].sudo().create(move_raw_values)
                new_move._action_confirm()
                if line.operation_id and line.bom_id and line.operation_id.id not in line.bom_id.operation_ids.ids:
                    line.bom_id.operation_ids = [(4, line.operation_id.id)]
            if line.operation_id and line.production_id.workorder_ids and line.operation_id.id not in line.production_id.workorder_ids.mapped('operation_id').ids:
                workorder_values = [(0, 0, {
                    'name': line.operation_id.name,
                    'mrp_plan_id': line.production_id.mrp_plan_id and line.production_id.mrp_plan_id.id or False,
                    'production_id': line.production_id.id,
                    'workcenter_id': line.operation_id.workcenter_id.id,
                    'product_uom_id': line.production_id.product_uom_id.id,
                    'operation_id': line.operation_id.id,
                    'state': 'pending',
                    'consumption': line.production_id.consumption,
                })]
                line.production_id.sudo().workorder_ids = workorder_values
            if line.production_id.sudo().state != 'confirmed':
                line.production_id.sudo()._onchange_move_raw()
                line.production_id.sudo()._onchange_move_finished()
            else:
                line.production_id.sudo().button_unplan()
                line.production_id.sudo().button_plan()

        production_orders = self.line_ids.mapped('production_id')
        if production_orders:
            for production in production_orders:
                if production.move_raw_ids:
                    for move in production.move_raw_ids:
                        workorders = production.workorder_ids.filtered(lambda w: move.operation_id and w.operation_id and move.operation_id.id == w.operation_id.id)
                        if workorders:
                            move.sudo().write({'workorder_id': workorders[0].id, 'mrp_workorder_component_id': workorders[0].id, 'production_id': False})
        return


class WizardChangeComponentLine(models.TransientModel):
    _name = 'mrp.change.component.wizard.line'
    _description = 'Change Components Wizard Line'

    add_id = fields.Many2one('mrp.change.component.wizard')
    product_id = fields.Many2one('product.product', string='Product', domain=[('type', 'in', ['product', 'consu'])])
    product_tmpl_id = fields.Many2one('product.template', 'Product', domain=[('type', 'in', ['product', 'consu'])])
    product_qty = fields.Float('To Consume', default=1)
    product_uom = fields.Many2one('uom.uom', 'Unit of Measure', related='product_id.uom_id')
    alternative_component_ids = fields.Many2many('product.product', string='Alternative Components')
    allowed_operation_ids = fields.Many2many('mrp.routing.workcenter', compute='_compute_allowed_operation_ids')
    operation_id = fields.Many2one(
        'mrp.routing.workcenter', 'Operation To Consume', check_company=True,
        domain="[('id', 'in', allowed_operation_ids)]")
    bom_id = fields.Many2one('mrp.bom', 'BoM')
    move_id = fields.Many2one('stock.move', string='Stock Move')
    production_id = fields.Many2one('mrp.production', string='MO #')
    add_production_ids = fields.Many2many(related='add_id.production_ids')

    @api.onchange('production_id')
    def onchange_production_id(self):
        if self.production_id and self.production_id.bom_id:
            self.bom_id = self.production_id.bom_id.id
        else:
            self.bom_id = False

    @api.depends('bom_id')
    def _compute_allowed_operation_ids(self):
        for move in self:
            # if move.bom_id:
            #     move.alternative_component_ids = move.bom_id.alternative_component_ids
            if (
                    not move.bom_id or not move.bom_id.operation_ids
            ):
                move.allowed_operation_ids = self.env['mrp.routing.workcenter']
            else:
                operation_domain = [
                    ('id', 'in', move.bom_id.operation_ids.ids),
                    '|',
                    ('company_id', '=', self.env.company.id),
                    ('company_id', '=', False)
                ]
                move.allowed_operation_ids = self.env['mrp.routing.workcenter'].search(operation_domain)



