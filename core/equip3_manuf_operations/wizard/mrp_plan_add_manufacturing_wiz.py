# -*- coding: utf-8 -*-
from odoo import fields, models, api


class WizardAddManuOrder(models.TransientModel):
    _name = 'mrp.production.wizard'
    _description = 'Add Manufacturing Order'

    plan_id = fields.Many2one('mrp.plan', string='Manufacturing Plan', required=True)
    line_ids = fields.One2many('mrp.production.wizard.line', 'add_id')

    def create_production(self, product_id, product_qty, child_ids):
        plan_id = self.plan_id

        values = {
            'mrp_plan_id': plan_id.id,
            'product_id': product_id.id,
            'product_qty': product_qty,
            'bom_id': product_id.bom_ids[0].id,
            'user_id': self.env.user.id,
            'product_uom_id': product_id.uom_id.id,
            'company_id': (plan_id.company_id or self.env.company).id,
            'date_planned_start': plan_id.date_planned_start,
            'date_planned_finished': plan_id.date_planned_finished,
            'child_ids': [(6, 0, child_ids.ids)]
        }

        if plan_id.analytic_tag_ids:
            values['analytic_tag_ids'] = [(6, 0, plan_id.analytic_tag_ids.ids)]
        else:
            analytic_priority = self.env['analytic.priority'].sudo().search([], limit=1, order='priority')
            if analytic_priority:
                if analytic_priority.object_id == 'user' and self.env.user.analytic_tag_ids:
                    values['analytic_tag_ids'] = [(6, 0, self.env.user.analytic_tag_ids.ids)]
                elif analytic_priority.object_id == 'branch' and self.env.user.branch_id and self.env.user.branch_id.analytic_tag_ids:
                    values['analytic_tag_ids'] = [(6, 0, self.env.user.branch_id.analytic_tag_ids.ids)]
                elif analytic_priority.object_id == 'product_category':
                    search_product_category = self.env['product.category'].sudo().search([('analytic_tag_ids', '!=', False)], limit=1)
                    if search_product_category:
                        values['analytic_tag_ids'] = [(6, 0, search_product_category.analytic_tag_ids.ids)]

        if plan_id.state == 'confirm':
            values['additional_mo'] = True

        production_id = self.env['mrp.production'].create(values)
        production_id.sudo().onchange_product_id()
        production_id.sudo().onchange_branch()
        production_id.sudo()._onchange_workorder_ids()
        production_id.sudo()._onchange_move_raw()
        production_id.sudo()._onchange_move_finished()
        production_id.sudo().onchange_workorder_ids()
        return production_id

    def create_recursive_production(self, line_ids=False, recursive=False):
        is_parent = False
        repeat = 1
        if not line_ids:
            line_ids = self.line_ids
            is_parent = True

        production_ids = self.env['mrp.production']
        for line in line_ids:

            if is_parent:
                recursive = line.premix
                repeat = line.no_of_mrp

            for i in range(repeat):
                product_id = line.product_id

                if not product_id.bom_ids:
                    continue

                bom_id = product_id.bom_ids[0]
                next_line_ids = bom_id.bom_line_ids

                child_ids = self.env['mrp.production']
                if any(p.bom_ids for p in next_line_ids.mapped('product_id')) and recursive:
                    child_ids = self.create_recursive_production(line_ids=next_line_ids, recursive=recursive)

                production_id = self.create_production(product_id, line.product_qty, child_ids)
                production_ids |= production_id

        return production_ids

    def confirm(self):
        self.create_recursive_production()
        self.plan_id.generate_mrp_order_ids()
        for i in self.plan_id.mrp_order_ids.bom_id.operation_ids:
            for work in self.plan_id.workorder_ids:
                if i.work_center == 'with_group':
                    if i.name == work.name:
                        for a in i.work_center_group.mrp_work_center_group_ids:
                            work.write({
                                'work_center_group_ids': [(4, a.work_center.id)],
                            })
                if i.work_center == 'without_group':
                    if i.name == work.name:
                        work_center = self.env['mrp.workcenter'].search([])
                        for workcenter in work_center:
                            work.write({
                                'work_center_group_ids': [(4, workcenter.id)]
                            })


class ManufacturingOrder(models.TransientModel):
    _name = 'mrp.production.wizard.line'
    _description = 'MRP Plan Production Wizard Line'

    add_id = fields.Many2one('mrp.production.wizard')
    product_id = fields.Many2one('product.product', 'Product', domain="""[
                ('bom_ids', '!=', False),
                ('bom_ids.branch', '=', branch)
                ]
                """)
    product_qty = fields.Float('Quantity', default=1, digits='Product Unit of Measure')
    product_uom = fields.Many2one('uom.uom', 'Unit of Measure')
    no_of_mrp = fields.Integer(string='No of Manufacturing Order', default=1)
    premix = fields.Boolean(string='Cascade to Child BOM', default=True)
    branch = fields.Many2one('res.branch', string='Branch')

    @api.model
    def default_get(self, fields):
        defaults = super(ManufacturingOrder, self).default_get(fields)
        mrp_plan = self.env["mrp.plan"].browse(self._context.get("active_id"))
        if mrp_plan and mrp_plan.branch:
            defaults['branch'] = mrp_plan.branch.id
        return defaults

    @api.onchange('product_id','product_uom')
    def set_product_uom(self):
        for rec in self:
            if rec.product_id:
                rec.product_uom = rec.product_id.uom_id and rec.product_id.uom_id.id or self.env.ref('uom.product_uom_unit').id

    @api.depends('product_id')
    def _compute_allowed_product_ids(self):
        for production in self:
            product_domain = [
                ('type', 'in', ['product', 'consu']),
            ]
            if production.bom_id:
                if production.bom_id.product_id:
                    product_domain += [('id', '=', production.bom_id.product_id.id)]
                else:
                    product_domain += [('id', 'in', production.bom_id.product_tmpl_id.product_variant_ids.ids)]
            production.allowed_product_ids = self.env['product.product'].search(product_domain)
