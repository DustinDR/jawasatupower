# -*- coding: utf-8 -*-
from odoo import fields, models, tools, api, _
from odoo.exceptions import UserError, Warning


class WizardPlanReserveMaterial(models.TransientModel):
    _name = 'mrp.plan.reserve.material.wizard'
    _description = 'MRP Plan Reserve Material Wizard'

    line_ids = fields.One2many('mrp.plan.reserve.material.wizard.line', 'add_id')

    def confirm(self, raise_error=True):
        if self.line_ids:
            not_available_products = []
            number = 1
            for move_line in self.line_ids:
                if move_line.move_id and (move_line.product_reserve_qty >= 0 and move_line.product_reserve_qty > move_line.plan_forecast_qty):
                    not_available_products.append(_('%s. %s on location %s', number, move_line.move_id.product_id.display_name, move_line.move_id.location_id.display_name))
                    number += 1

            if not_available_products and raise_error:
                product_info = '\n'.join(not_available_products)
                raise Warning(_('There is not enough stock for: \n%s', product_info))

            for line in self.line_ids:
                if line.move_id and line.product_reserve_qty > 0:
                    if line.product_reserve_qty == line.product_qty:
                        qty_to_reserve = 0
                    elif line.product_qty > line.product_reserve_qty:
                        qty_to_reserve = line.product_qty - line.product_reserve_qty
                    else:
                        qty_to_reserve = 0
                    line.move_id.reserved_availability = line.move_id.product_id.uom_id._compute_quantity(
                        qty_to_reserve, line.move_id.product_uom, rounding_method='HALF-UP')
                    line.move_id._action_assign()
        return


class WizardPlanReserveMaterialLine(models.TransientModel):
    _name = 'mrp.plan.reserve.material.wizard.line'
    _description = 'MRP Plan Reserve Material Wizard Line'

    @api.depends('add_id', 'add_id.line_ids', 'move_id', 'move_id.forecast_qty', 'product_id', 'sequence', 'product_reserve_qty')
    def _compute_plan_forecast_qty(self):
        for record in self:
            record.plan_forecast_qty = 0.0
            line_ids = record.add_id.line_ids
            previous_lines = line_ids.filtered(lambda l: l.sequence < record.sequence and l.product_id == record.product_id)
            already_reserved = sum(previous_lines.mapped('product_reserve_qty'))
            record.plan_forecast_qty = record.forecast_qty - already_reserved

    sequence = fields.Integer()
    add_id = fields.Many2one('mrp.plan.reserve.material.wizard')
    product_id = fields.Many2one('product.product', string='Product', domain=[('type', 'in', ['product', 'consu']),('bom_ids', '!=', False)])
    product_name = fields.Char(related='product_id.display_name', string='Product')
    product_qty = fields.Float('To Consume', default=1)
    product_qty_rel = fields.Float(related='move_id.product_uom_qty', string='To Consume')
    product_reserve_qty = fields.Float('To Reserve')
    move_id = fields.Many2one('stock.move', string='Stock Move')
    forecast_qty = fields.Float(related='move_id.forecast_qty', string='Available')
    production_id = fields.Many2one('mrp.production', string='Manufacturing Order')
    production_name = fields.Char(related='production_id.display_name', string='Manufacturing Order')
    workorder_id = fields.Many2one('mrp.workorder', string='Work Order')
    workorder_name = fields.Char(related='workorder_id.display_name', string='Work Order')
    plan_forecast_qty = fields.Float(compute=_compute_plan_forecast_qty)

    @api.onchange('product_reserve_qty')
    def onchange_product_reserve_qty(self):
        if self.product_reserve_qty > 0 and self.product_reserve_qty > self.plan_forecast_qty:
            raise UserError(_('There is not enough stock for product "%s" on location "%s"') % (self.product_id.display_name, self.move_id.location_id and self.move_id.location_id.display_name or ''))
        if self.product_reserve_qty > 0 and self.product_reserve_qty > self.product_qty:
            raise UserError(_("Quantity to Reserve couldn't be bigger than Total Quantity"))


