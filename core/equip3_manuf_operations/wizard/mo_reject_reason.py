# -*- coding: utf-8 -*-
from odoo import fields, models, tools, api


class WizardMOApprovalMatrixRejectReason(models.TransientModel):
    _name = 'mo.matrix.reject.reason.wizard'
    _description = 'MO Approval Matrix Reject Reason Wizard'

    reject_reason = fields.Text(string='Reject Reason')

    def confirm(self):
        mrp_production = self.env["mrp.production"].browse(self._context.get("active_id"))
        if mrp_production:
            mrp_production.action_reject(reject_reason=str(self.reject_reason))