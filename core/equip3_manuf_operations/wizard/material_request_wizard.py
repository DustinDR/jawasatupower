from odoo import fields, models, api, _


class MrpMaterialRequestWizard(models.TransientModel):
    _name = "material.request.wizard"
    _description = "Material Request"

    name = fields.Char(
        string="Material Request",
        readonly=True,
        required=True,
        copy=False,
        default="New",
    )
    requested_by = fields.Many2one(
        "res.users",
        string="Created By",
        default=lambda self: self.env.user,
        tracking=True,
    )
    branch = fields.Many2one(
        "res.branch",
        string="Branch",
        default=lambda self: self.env.user.branch_id,
        tracking=True,
    )
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        tracking=True,
        default=lambda self: self.env.company,
    )
    destination_warehouse_id = fields.Many2one('stock.warehouse', 'Destination Warehouse', required='1', tracking=True)
    destination_location = fields.Many2one(
        "stock.location",
        "Destination Location",
        check_company=True,
        required=True,
    )
    creation_date = fields.Datetime(
        "Create On",
        copy=False,
        default=lambda self: fields.Datetime.now(),
        index=True,
        readonly=True
    )
    create_uid = fields.Many2one(
        "res.users",
        string="Created By",
        default=lambda self: self.env.user,
        tracking=True,
        readonly=True
    )
    date_planned = fields.Datetime(
        "Scheduled Date",
        copy=False,
        default=lambda self: fields.Datetime.now(),
        help="Date at which you plan to start the production.",
        index=True,
        required=True,
        tracking=True,
    )
    date_expiry = fields.Datetime(
        "Expiry Date",
        copy=False,
        index=True,
        tracking=True,
    )
    description = fields.Text(string="Description")
    origin = fields.Char(string="Source Documant")

    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("approval", "Wating for Approval"),
            ("approved", "Approve"),
            ("reject", "Reject "),
            ("confirm", "Confirmed"),
            ("cancel", "Cancelled"),
            ("lock", "Done"),
        ],
        default="draft",
        tracking=True,
    )

    material_request_line_ids = fields.One2many(
        "material.request.line.wizard", "material_request_id", string="Products"
    )

    is_readonly_origin = fields.Boolean()

    def action_done(self):
        material_request = self.env['material.request'].create({
            'requested_by': self.requested_by.id,
            'branch_id': self.branch.id,
            'destination_warehouse_id': self.destination_warehouse_id.id,
            'destination_location_id': self.destination_location.id,
            'create_date': self.creation_date,
            'create_uid': self.create_uid,
            'schedule_date': self.date_planned,
            'description': self.description,
            'source_document': self.origin,
            'status': self.state,
            'product_line': [(0, 0, {
                'description': line.product_id and line.product_id.name or '',
                'product': line.product_id and line.product_id.id or False,
                'product_unit_measure': line.product_id and line.product_id.uom_id and line.product_id.uom_id.id or False,
                'quantity': line.qty,
                'request_date': self.creation_date,
                'destination_warehouse_id': self.destination_warehouse_id.id,
                'destination_location_id': line.product_id and line.product_id.with_company(
                    self.company_id).property_stock_production.id or False,
                'status': 'draft'
            }) for line in self.material_request_line_ids]
        })

        return {
            'type': 'ir.actions.act_window',
            'name': _('Material Request'),
            'res_model': 'material.request',
            'target': 'self',
            'view_mode': 'form',
            'res_id': material_request.id
        }


class MrpMaterialRequestLineWizard(models.TransientModel):
    _name = "material.request.line.wizard"
    _description = "Material Request Line"

    product_id = fields.Many2one(
        "product.product",
        string="Product",
        change_default=True,
    )
    description = fields.Text(string="Description")
    qty = fields.Float(string="Quantity", default=1.0)
    qty_done = fields.Float(string="Done Quantity", default=1.0)
    uom_id = fields.Many2one("uom.uom", string="Unit Of Measure")
    destination_warehouse_id = fields.Many2one('stock.warehouse', 'Destination Warehouse', required='1', tracking=True, 
    related='material_request_id.destination_warehouse_id',
    readonly=True,
    store=True
    )
    destination_location = fields.Many2one(
        "stock.location", "Destination Location", check_company=True
    )
    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("approval", "Wating for Approval"),
            ("approved", "Approve"),
            ("reject", "Reject "),
            ("confirm", "Confirmed"),
            ("cancel", "Cancelled"),
            ("lock", "Done"),
        ],
        default="draft",
        tracking=True,
    )
    material_request_id = fields.Many2one(
        "material.request.wizard", string="Material Request")
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        tracking=True,
        default=lambda self: self.env.company,
    )
