# -*- coding: utf-8 -*-
from odoo import fields, models, tools, api


class WizardMRPPlanApprovalMatrixRejectReason(models.TransientModel):
    _name = 'mrp.plan.matrix.reject.reason.wizard'
    _description = 'MRP Plan Approval Matrix Reject Reason Wizard'

    reject_reason = fields.Text(string='Reject Reason')

    def confirm(self):
        mrp_plan = self.env["mrp.plan"].browse(self._context.get("active_id"))
        if mrp_plan:
            mrp_plan.action_reject(reject_reason=str(self.reject_reason))