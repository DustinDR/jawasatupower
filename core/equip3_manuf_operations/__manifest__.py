# -*- coding: utf-8 -*-

{
    "name": "Equip 3 - Manufacturing Operations",
    "version": "1.1.49",
    "category": "Manufacturing",
    "summary": "Manufacturing Operations",
    "description": """
    i. Manufacturing Plan
    ii. Manufacturing Plan
    iii. Work Order
    iv. Auto Generate Internal Transfers on Manufacturing Plan/ Manufacturing Order/ Work Order
    v. Auto Generate Purchase Requeston Manufacturing Plan/ Manufacturing Order/ Work Order
    vi. Sales to Manufacturing
    vii. Sales to Manufacturing System and Email Notification
    viii. Approval Matrix on Manufacturing Plan/ Manufacturing Order
    """,
    "author": "HashMicro",
    "website": "www.hashmicro.com",
    "depends": [
        "web",
        "base",
        "mail",
        "mrp",
        "branch",
        "sale",
        "mrp_subcontracting",
        "equip3_manuf_accessright_settings",
        "equip3_manuf_masterdata",
        "equip3_sale_operation",
        "equip3_purchase_operation",
        "stock",
        "purchase_request",
        "sh_inventory_mrp_qc",
        "equip3_inventory_operation",
        "analytic",
        "ks_gantt_view_mrp",
        "app_mrp_superbar"
    ],
    "data": [
        "data/ir_sequence_data.xml",
        "data/on_upgrade.xml",
        "views/mrp_production_view.xml",
        "views/mrp_approval_matrix.xml",
        "views/mrp_plan.xml",
        "views/mrp_workorder_view.xml",
        "views/mrp_notification_views.xml",
        "views/mrp_views_menu.xml",
        "views/product_template_views.xml",
        "views/res_config_settings_views.xml",
        "views/stock_move_view.xml",
        'views/sale_order_views.xml',
        "wizard/mrp_plan_add_manufacturing_wiz.xml",
        "wizard/mrp_plan_reject_reason_wiz.xml",
        "wizard/mrp_plan_change_component_wiz.xml",
        "wizard/material_request_wizard_view.xml",
        "wizard/mo_reject_reason_wiz.xml",
        "wizard/mrp_plan_reserve_material_wiz.xml",
        "wizard/mp_done_confirm_wiz.xml",
        "security/ir.model.access.csv",
        "security/ir_rule.xml",
        "templates/sale_order_auto_mail_template.xml"
    ],
    "installable": True,
    "application": True,
    "auto_install": False
}
