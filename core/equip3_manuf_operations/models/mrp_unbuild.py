from odoo import models, fields


class MRPUnbuild(models.Model):
	_inherit = 'mrp.unbuild'

	product_qty = fields.Float(digits='Product Unit of Measure')
