from odoo import models, fields


class ResCompany(models.Model):
	_inherit = 'res.company'

	send_email_so_confirm = fields.Boolean('Send Email Notification when SO Confirm')
	send_system_so_confirm = fields.Boolean('Send System Notification when SO Confirm')
	send_email_user_ids = fields.Many2many('res.users', 'send_email_company_config_ids', string='Send Email Notification to')
	send_system_user_ids = fields.Many2many('res.users', 'send_system_company_config_ids', string='Send System Notification to')
	send_whatsapp_so_confirm = fields.Boolean('WhatsApp Notification', default=False)
	# connector_whatsapp_so_confirm = fields.Many2one('acrux.chat.connector', string='WhatsApp Connector')

	def write(self, vals):
		res = super(ResCompany, self).write(vals)
		manufacturing_plan_conf_menu = self.env.ref("equip3_manuf_operations.con_mrp_plan_menu")
		manufacturing_order_conf_menu = self.env.ref("equip3_manuf_operations.con_mrp_order_menu")
		for company in self:
			if not company == self.env.company:
				continue
			manufacturing_plan_conf_menu.active = company.manufacturing_plan_conf
			manufacturing_order_conf_menu.active = company.manufacturing_order_conf
		return res
