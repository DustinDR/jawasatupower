# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import UserError
from odoo.addons.stock.models.stock_move import PROCUREMENT_PRIORITIES
from datetime import date, datetime, timedelta
import pytz


class MrpPlan(models.Model):
    _name = 'mrp.plan'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'mrp.material.request']
    _description = 'Manufacturing Plan'
    _rec_name = 'plan_id'
    _order = 'priority desc, date_planned_start asc,id'

    @api.model
    def create(self, vals):
        vals['plan_id'] = self.env['ir.sequence'].next_by_code('mrp.plan')
        return super(MrpPlan, self).create(vals)

    def write(self, vals):
        res = super(MrpPlan, self).write(vals)
        for plan in self:
            production_ids = plan.mrp_order_ids.filtered(lambda p: not p.is_planned)
            if production_ids:
                production_ids.with_context(force_date=True).write({
                    'date_planned_start': plan.date_planned_start,
                    'date_planned_finished': plan.date_planned_finished
                })
        return res

    @api.model
    def _get_default_date_planned_finished(self):
        if self.env.context.get('default_date_planned_start'):
            return fields.Datetime.to_datetime(self.env.context.get('default_date_planned_start')) + timedelta(hours=1)
        return datetime.now() + timedelta(hours=1)

    @api.model
    def _get_default_date_planned_start(self):
        if self.env.context.get('default_date_deadline'):
            return fields.Datetime.to_datetime(self.env.context.get('default_date_deadline'))
        return datetime.now()

    @api.model
    def _get_default_analytic_tag_ids(self):
        user = self.env.user
        analytic_priority = self.env['analytic.priority'].sudo().search([], limit=1, order='priority')
        analytic_tag_ids = []
        if analytic_priority.object_id == 'user' and user.analytic_tag_ids:
            analytic_tag_ids = user.analytic_tag_ids.ids
        elif analytic_priority.object_id == 'branch' and user.branch_id and user.branch_id.analytic_tag_ids:
            analytic_tag_ids = user.branch_id.analytic_tag_ids.ids
        elif analytic_priority.object_id == 'product_category':
            product_category = self.env['product.category'].sudo().search([('analytic_tag_ids', '!=', False)], limit=1)
            analytic_tag_ids = product_category.analytic_tag_ids.ids
        return [(6, 0, analytic_tag_ids)]

    @api.depends('mrp_order_ids', 'mrp_order_ids.reservation_state')
    def _compute_reservation_state(self):
        for plan in self:
            plan.reservation_state = 'waiting'
            if all(order.reservation_state == 'assigned' for order in plan.mrp_order_ids):
                plan.reservation_state = 'available'

    @api.depends('mrp_order_ids', 'mrp_order_ids.is_planned')
    def _compute_is_planned(self):
        for record in self:
            record.is_planned = all(mo.is_planned for mo in record.mrp_order_ids)

    @api.depends('company_id')
    def _compute_allowed_branch(self):
        user_branch = self.env.user.branch_ids
        for record in self:
            allowed_branch = user_branch.filtered(lambda b: b.company_id == record.company_id)
            record.allowed_branch_ids = [(6, 0, allowed_branch.ids)]

    @api.depends('mrp_order_ids')
    def _compute_move_byproduct_ids(self):
        for plan in self:
            byproduct_move_ids = []
            if plan.mrp_order_ids:
                for order in plan.mrp_order_ids:
                    for stock_move in order.move_finished_ids.filtered(lambda m: m.product_id != order.product_id):
                        byproduct_move_ids.append(stock_move.id)
            plan.move_byproduct_ids = [(6, 0, byproduct_move_ids)]

    @api.depends('mo_stock_move_ids', 'mrp_order_ids.state', 'mo_stock_move_ids.product_uom_qty')
    def _compute_unreserve_visible(self):
        for plan in self:
            already_reserved = any(
                order.state not in ('done', 'cancel') for order in plan.mrp_order_ids) and plan.mapped(
                'mo_stock_move_ids.move_line_ids')
            any_quantity_done = any(m.quantity_done > 0 for m in plan.mo_stock_move_ids)

            plan.unreserve_visible = not any_quantity_done and already_reserved
            plan.reserve_visible = any(
                order.state in ('confirmed', 'progress', 'to_close') for order in plan.mrp_order_ids) and any(
                move.product_uom_qty and move.state in ['confirmed', 'partially_available'] for move in
                plan.mo_stock_move_ids)

    state = fields.Selection([
        ('draft', 'Draft'),
        ('approval', 'To be Approved '),
        ('approved', 'Approved'),
        ('reject', 'Reject '),
        ('confirm', 'Confirmed'),
        ('cancel', 'Cancelled'),
        ('progress', 'In Progress'),
        ('to_close', 'To Close'),
        ('done', 'Done')
    ], default='draft', tracking=True)
    name = fields.Char(string='Plan Name', tracking=True)
    plan_id = fields.Char(string='Manufacturing Plan', copy=False, tracking=True)
    priority = fields.Selection(
        PROCUREMENT_PRIORITIES, string='Priority', default='0', index=True, tracking=True)
    ppic_id = fields.Many2one('res.users', string='PPIC', tracking=True, readonly=True, states={'draft': [('readonly', False)]})
    allowed_branch_ids = fields.One2many('res.branch', compute=_compute_allowed_branch)
    branch = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id,
                             domain="[('id', 'in', allowed_branch_ids)]", readonly=True,
                             states={'draft': [('readonly', False)]}, tracking=True)
    company_id = fields.Many2one(
        'res.company', string='Company', tracking=True, default=lambda self: self.env.company)
    create_uid = fields.Many2one(
        'res.users', string='Created By', default=lambda self: self.env.user, tracking=True)
    mrp_order_ids = fields.One2many('mrp.production', 'mrp_plan_id', string='Manufacturing Order',
                                    tracking=True, readonly=True, states={'draft': [('readonly', False)]})
    mo_stock_move_ids = fields.One2many('stock.move', 'mrp_plan_id', string='Components',
                                        tracking=True, readonly=True, states={'draft': [('readonly', False)]})
    workorder_ids = fields.One2many(
        'mrp.workorder', 'mrp_plan_id', string='Work Orders')
    move_byproduct_ids = fields.One2many(
        'stock.move', compute='_compute_move_byproduct_ids', tracking=True)

    sale_order_id = fields.Many2one(
        'sale.order', string='Sale Order', readonly=True, copy=False)
    unreserve_visible = fields.Boolean('Allowed to Unreserve Production', compute='_compute_unreserve_visible',
        help='Technical field to check when we can unreserve')
    reserve_visible = fields.Boolean('Allowed to Reserve Production', compute='_compute_unreserve_visible',
        help='Technical field to check when we can reserve quantities')

    date_planned_start = fields.Datetime('Scheduled Date Start', required=True, default=_get_default_date_planned_start)
    date_planned_finished = fields.Datetime('Scheduled Date Finished', required=True, default=_get_default_date_planned_finished)
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytical Group', domain="[('company_id', '=', company_id)]", default=lambda self: self._get_default_analytic_tag_ids())
    reservation_state = fields.Selection(string='Reservation State', selection=[('waiting', 'Waiting for Material'), ('available', 'Material Available')], compute=_compute_reservation_state, store=True)
    is_planned = fields.Boolean(compute=_compute_is_planned)

    @api.onchange('date_planned_start')
    def _onchange_date_planned_start(self):
        if self.date_planned_start and not self.is_planned:
            self.date_planned_finished = self.date_planned_start + timedelta(hours=1)
            self.mrp_order_ids.write({
                'date_planned_start': self.date_planned_start,
                'date_planned_finished': self.date_planned_finished
            })

    def button_plan(self):
        self.mrp_order_ids.button_plan()
        self._recompute_scheduled_date()

    def button_unplan(self):
        for production_id in self.mrp_order_ids:
            production_id.button_unplan()

        now = fields.Datetime.now()
        self.date_planned_start = now
        self.date_planned_finished = now + timedelta(hours=1)

    def button_unreserve(self):
        for plan in self:
            if plan.mrp_order_ids:
                for order in plan.mrp_order_ids:
                    order.sudo().button_unreserve()

    def action_confirm(self):
        for plan in self:
            if plan.mrp_order_ids:
                plan.mrp_order_ids.action_confirm()
            plan.write({'state': 'confirm'})

    def action_cancel(self):
        self.write({'state': 'cancel'})

    def action_add(self):
        view_id = self.env.ref('equip3_manuf_operations.manu_order_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _('Add Manufacturing Order'),
            'res_model': 'mrp.production.wizard',
            'target': 'new',
            'view_mode': 'form',
            'views': [[view_id, 'form']],
            'context': {
                'default_plan_id': self.id,
                'default_branch': self.branch and self.branch.id or self.env.user.branch_id.id
            }
        }

    def action_check_availability(self):
        for plan in self:
            if plan.mrp_order_ids:
                if not self.env.company.mrp_plan_partial_availability:
                    product_message = ''
                    count = 1
                    for line in self.mo_stock_move_ids:
                        if line.forecast_qty <= 0 or line.forecast_qty < line.product_uom_qty:
                            if line.product_id and line.location_id:
                                if product_message == '':
                                    product_message = str(count) + _('. "%s" on location "%s"') % (
                                    line.product_id.display_name, line.location_id.display_name)
                                else:
                                    product_message = product_message + '\r\n' + str(count) + _(
                                        '. "%s" on location "%s"') % (
                                                      line.product_id.display_name, line.location_id.display_name)
                                count = count + 1
                    if product_message != '':
                        raise UserError(_('There is not enough stock for: \r\n%s') % product_message)
                    plan.mrp_order_ids.sudo().action_assign()
                else:
                    view_id = self.env.ref('equip3_manuf_operations.manu_plan_reserve_material_wiz_form').id
                    line_ids = []
                    for seq, line in enumerate(self.mo_stock_move_ids.filtered(lambda move: move.state not in ("done", "cancelled"))):
                        line_ids.append((0, 0, {
                            'sequence': seq,
                            'product_id': line.product_id and line.product_id.id or False,
                            'product_qty': line.product_uom_qty,
                            'product_reserve_qty': line.product_uom_qty,
                            'move_id': line.id,
                            'production_id': line.raw_material_production_id and line.raw_material_production_id.id or False,
                            'workorder_id': line.workorder_id and line.workorder_id.id or False
                        }))

                    return {
                        'type': 'ir.actions.act_window',
                        'name': _('Reserve Material'),
                        'res_model': 'mrp.plan.reserve.material.wizard',
                        'target': 'new',
                        'view_mode': 'form',
                        'views': [[view_id, 'form']],
                        'context': {
                            'default_line_ids': line_ids,
                        }
                    }

    def action_done(self):
        for plan in self:
            any_unfinished_wo = any(wo.state != 'done' for wo in plan.workorder_ids)
            if any_unfinished_wo:
                view_id = self.env.ref('equip3_manuf_operations.view_mp_done_confirm_wizard').id
                return {
                    'type': 'ir.actions.act_window',
                    'name': _('Confirm'),
                    'res_model': 'mp.done.confirm.wizard',
                    'target': 'new',
                    'view_mode': 'form',
                    'views': [[view_id, 'form']],
                    'context': {}
                }
            else:
                plan.mrp_order_ids.sudo().button_mark_done()
                plan.sudo().write({'state': 'done'})

    @api.model
    def delete_view_issue(self):
        try:
            mrp_plan_form_view = self.env.ref('equip3_manuf_operations.mrp_plan_form_view_inherit')
            if mrp_plan_form_view:
                mrp_plan_form_view.active = False
        except ValueError:
            pass

    def generate_mrp_order_ids(self):
        if self.mrp_order_ids:
            stock_move_ids = []
            for mrp_order_id in self.mrp_order_ids:
                for stock_move in mrp_order_id.move_raw_ids:
                    stock_move_ids.append(stock_move.id)
            self.write({'mo_stock_move_ids': [(6, 0, stock_move_ids)]})

    def _recompute_scheduled_date(self):
        self.ensure_one()
        date_planned_start = False
        date_planned_finished = False

        starts = self.mrp_order_ids.filtered(lambda w: w.date_planned_start)
        finished = self.mrp_order_ids.filtered(lambda w: w.date_planned_finished)
        sorted_starts = sorted(starts, key=lambda w: w.date_planned_start)
        sorted_finished = sorted(finished, key=lambda w: w.date_planned_finished)

        if sorted_starts:
            date_planned_start = sorted_starts[0].date_planned_start
            date_planned_finished = sorted_finished[-1].date_planned_finished

        self.write({
            'date_planned_start': date_planned_start,
            'date_planned_finished': date_planned_finished
        })

    # APPROVAL MATRIX
    # ==================================================================================================================
    @api.model
    def _get_default_is_matrix_on(self):
        return self.env.company.manufacturing_plan_conf

    @api.model
    def _get_default_approval_matrix(self, is_matrix_on=None, company=None, branch=None):
        if is_matrix_on is None:
            is_matrix_on = self._get_default_is_matrix_on()

        if is_matrix_on is False:
            return False

        if company is None:
            company = self.env.company
        if branch is None:
            branch = self.env.user.branch_id

        approval_matrix = self.env['mrp.approval.matrix'].sudo().search([
            ('company', '=', company.id),
            ('branch', '=', branch.id),
            ('matrix_type', '=', 'mp')
        ], limit=1)
        return approval_matrix.id

    @api.depends('approval_matrix')
    def _compute_approval_matrix_lines(self):
        lines = []
        for rec in self:
            rec.mrp_plan_approval_matrix_ids = False
            for app_matrix in rec.approval_matrix.mrp_approval_matrix_ids:
                lines.append((0, 0, {
                    'mrp_plan_approval_matrix_id': rec.approval_matrix.id,
                    'sequence': app_matrix.sequence,
                    'approve': [(6, 0, app_matrix.approve.ids)],
                    'minimum_approver': app_matrix.minimum_approver,
                }))
            rec.mrp_plan_approval_matrix_ids = lines

    def _compute_check_by_product(self):
        for plan in self:
            if len(plan.move_byproduct_ids) >= 1:
                plan.check_by_product = True
            else:
                plan.check_by_product = False

    def _compute_approved_his_doc(self):
        for rec in self:
            b = False
            for line in rec.mrp_plan_approval_matrix_ids:
                if self.env.user.id in line.approve.ids:
                    if line.state == 'approved':
                        b = True
                    if line.state == 'rejected':
                        b = True
            rec.approved_his_doc = b

    def _compute_user_is_approver(self):
        for rec in self:
            b = False
            next_approved = 0
            for line in rec.mrp_plan_approval_matrix_ids:
                if line.state == 'approved':
                    next_approved += 1
                if line.state == 'rejected':
                    next_approved += 1

            if len(rec.mrp_plan_approval_matrix_ids) != next_approved:
                if self.env.user.id in rec.mrp_plan_approval_matrix_ids[next_approved].approve.ids:
                    b = True
                if self.env.user.id in rec.mrp_plan_approval_matrix_ids.approve_by.ids:
                    b = False
            rec.user_is_approver = b

    is_matrix_on = fields.Boolean(default=_get_default_is_matrix_on)
    approved_his_doc = fields.Boolean(compute=_compute_approved_his_doc)
    user_is_approver = fields.Boolean(compute=_compute_user_is_approver)
    approval_matrix = fields.Many2one('mrp.approval.matrix', 'Approval Matrix', default=_get_default_approval_matrix)
    mrp_plan_approval_matrix_ids = fields.One2many(
        'mrp.plan.approval.matrix.line',
        'mrp_plan_approval_matrix_id',
        string='Approval Lines',
        compute=_compute_approval_matrix_lines,
        store=True)
    check_by_product = fields.Boolean(compute=_compute_check_by_product)
    state_no_approval_matrix = fields.Selection(related='state')

    @api.onchange('is_matrix_on', 'company_id', 'branch')
    def onchange_branch(self):
        self.approval_matrix = self._get_default_approval_matrix(self.is_matrix_on, self.company_id, self.branch)

    def check_approve_reject_state(self):
        for rec in self:
            approve_state = len(
                [line_approve for line_approve in rec.mrp_plan_approval_matrix_ids if line_approve.state == 'approved'])
            rejected_state = len(
                [line_reject for line_reject in rec.mrp_plan_approval_matrix_ids if line_reject.state == 'rejected'])
            if all([line.state for line in rec.mrp_plan_approval_matrix_ids]):
                if approve_state >= 2:
                    if approve_state >= rejected_state:
                        rec.write({'state': 'approved'})
                    else:
                        rec.write({'state': 'reject'})
                else:
                    if rejected_state < 1:
                        rec.write({'state': 'approved'})
                    else:
                        rec.write({'state': 'reject'})

    def action_approval(self):
        for rec in self:
            for line in rec.mrp_plan_approval_matrix_ids:
                # if self.env.user.id in line.approve.ids:
                line.requested_time = fields.Datetime.now()
        self.write({'state': 'approval'})

    def action_approve(self):
        tz = pytz.timezone(self.env.user.tz) or 'UTC'
        current_time = datetime.now(tz)
        for rec in self:
            for line in rec.mrp_plan_approval_matrix_ids:
                if self.env.user.id in line.approve.ids:
                    line.approve_by = [(4, self.env.user.id)]
                    if line.approval_matrix_status and line.approval_matrix_status != '':
                        line.approval_matrix_status = line.approval_matrix_status + '\r\nApproved By: ' + \
                            str(self.env.user.name) + ', ' + \
                            current_time.strftime('%m/%d/%Y %H:%M:%S')
                    else:
                        line.approval_matrix_status = 'Approved By: ' + str(
                            self.env.user.name) + ', ' + current_time.strftime('%m/%d/%Y %H:%M:%S')
                    if line.minimum_approver < 2:
                        line.state = 'approved'
                    if line.approve_by == line.approve:
                        line.state = 'approved'
                    #line.approved_time = fields.Datetime.now()
            if all([line.state == 'approved' for line in rec.mrp_plan_approval_matrix_ids]):
                rec.write({'state': 'approved'})
            # if line.minimum_approver >= 2 :
            #     if len(line.approve_by) >= line.minimum_approver:
            #         rec.write({'state': 'approved'})
            self.check_approve_reject_state()

    def action_reject(self, reject_reason=''):
        tz = pytz.timezone(self.env.user.tz) or 'UTC'
        current_time = datetime.now(tz)
        for rec in self:
            for line in rec.mrp_plan_approval_matrix_ids:
                reject_msg = ''
                if self.env.user.id in line.approve.ids:
                    line.state = 'rejected'
                    if line.approval_matrix_status and line.approval_matrix_status != '':
                        reject_msg = line.approval_matrix_status + '\r\nRejected By: ' + str(
                            self.env.user.name) + ', ' + current_time.strftime('%m/%d/%Y %H:%M:%S')
                    else:
                        reject_msg = 'Rejected By: ' + str(
                            self.env.user.name) + ', ' + current_time.strftime('%m/%d/%Y %H:%M:%S')

                    if reject_reason != '':
                        reject_msg = reject_msg + ' Reason: ' + reject_reason
                    line.approval_matrix_status = reject_msg
            if all([line.state == 'rejected' for line in rec.mrp_plan_approval_matrix_ids if not line.minimum_approver >= 1]):
                rec.write({'state': 'reject'})
            self.check_approve_reject_state()

    def action_reject_wiz(self):
        view_id = self.env.ref('equip3_manuf_operations.manu_plan_reject_reason_wiz').id
        return {
            'type': 'ir.actions.act_window',
            'name': _('Reject Reason'),
            'res_model': 'mrp.plan.matrix.reject.reason.wizard',
            'target': 'new',
            'view_mode': 'form',
            'views': [[view_id, 'form']],
        }

    # ASSIGNED LABORS
    # ==================================================================================================================
    @api.depends('labor_production_id')
    def _compute_labor_allowed_workorder_ids(self):
        for record in self:
            workorder_ids = record.labor_production_id.workorder_ids
            allowed_workorder_ids = workorder_ids.filtered(lambda w: w.state not in ('done', 'cancel'))
            record.labor_allowed_workorder_ids = [(6, 0, allowed_workorder_ids.ids)]

    @api.depends('labor_type', 'labor_production_id', 'labor_workorder_id', 'labor_group_id',
                 'labor_group_id.labor_ids', 'labor_ids')
    def _compute_labor_hide_assign_button(self):
        for record in self:
            err_message = record.check_action_assign_labor()
            record.labor_hide_assign_button = err_message is not False

    labor_allowed_workorder_ids = fields.One2many('mrp.workorder', compute=_compute_labor_allowed_workorder_ids)
    labor_type = fields.Selection(
        selection=[
            ('with_group', 'With Group'),
            ('without_group', 'Without Group')
        ],
        string='Labor Type', required=True, default='with_group', tracking=True)
    labor_production_id = fields.Many2one(
        'mrp.production', string='Labor Manufacturing Order', domain="[('id', 'in', mrp_order_ids)]", tracking=True)
    labor_workorder_id = fields.Many2one(
        'mrp.workorder', string='Labor Work Order', domain="[('id', 'in', labor_allowed_workorder_ids)]", tracking=True)
    labor_group_id = fields.Many2one('mrp.labor.group', string='Labor Group', tracking=True)
    labor_ids = fields.Many2many('hr.employee', string='Labor', domain="[('active', '=', True)]")
    labor_hide_assign_button = fields.Boolean(compute=_compute_labor_hide_assign_button)
    assign_ids = fields.One2many('mrp.labor.assign', 'plan_id', string='Assigned Labors')

    def reset_labor_form(self):
        self.ensure_one()
        return self.write({
            'labor_type': 'with_group',
            'labor_production_id': False,
            'labor_workorder_id': False,
            'labor_group_id': False,
            'labor_ids': [(5,)]
        })

    def check_action_assign_labor(self):
        self.ensure_one()
        err_message = False
        if not self.labor_production_id:
            err_message = _('Please select Manufacturing Order to assign!')
        if not self.labor_workorder_id:
            err_message = _('Please select Work Order to assign!')

        if self.labor_type == 'with_group':
            if not self.labor_group_id:
                err_message = _('Please select Labor Group to assign!')
            elif not self.labor_group_id.labor_ids:
                err_message = _('The selected labor group has no employees to assign!')
        else:
            if not self.labor_ids:
                err_message = _('Please select Labors to assign!')
        return err_message

    def action_assign_labor(self):
        assign_labor = self.env['mrp.labor.assign']
        for record in self:
            err_message = record.check_action_assign_labor()
            if err_message is not False:
                raise UserError(err_message)

            labor_ids = record.labor_ids
            if record.labor_type == 'with_group':
                labor_ids = record.labor_group_id.labor_ids

            for labor in labor_ids:
                assign_labor.create({
                    'plan_id': record.id,
                    'production_id': record.labor_production_id.id,
                    'workorder_id': record.labor_workorder_id.id,
                    'labor_id': labor.id
                })
            record.reset_labor_form()

    def action_remove_labor(self):
        assign_to_delete = self.assign_ids.filtered(lambda a: a.workorder_id.state in ('pending', 'ready', 'cancel'))
        assign_to_delete.unlink()
    
    @api.depends('state', 'mo_stock_move_ids.quantity_done', 'mo_stock_move_ids.product_uom_qty')
    def _compute_show_transfer_back_material_button(self):
        for rec in self:
            yes = False
            if rec.state == 'done' or rec.state == 'to_close' or rec.state == 'cancel':
                for line in rec.mo_stock_move_ids:
                    if line.product_uom_qty >= line.quantity_done:
                        yes = True
                    else:
                        yes = False
            rec.show_transfer_back_material_button = yes
    show_transfer_back_material_button = fields.Boolean(compute='_compute_show_transfer_back_material_button', string='Show Transfer Back Material Button', default=False)

    def transfer_back_material(self):
        lines = []
        for line in self.mo_stock_move_ids:
            if line.product_uom_qty > line.quantity_done:
                line_data = {
                    'name': line.name,
                    'product_id': line.product_id.id,
                    'product_uom_qty': line.product_uom_qty - line.quantity_done,
                    'product_uom': line.product_uom,
                    'location_id': line.location_id.id,
                }
                lines.append((0, 0, line_data))

        picking_id = self.env['stock.picking'].create({
            "picking_type_id": self.env['stock.picking.type'].search([('sequence_code', '=', 'MO')], limit=1).id,
            "location_id": self.env['stock.location'].search([], limit=1).id,
            "location_dest_id": self.env['stock.location'].search([], limit=1).id,
            "move_ids_without_package": lines,
            "origin": self.name
        })

        if picking_id:
            return {
            'name': 'Transfer Back Material',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'stock.picking',
            'view_id': self.env.ref('stock.view_picking_form').id,
            'res_id': picking_id.id,
            'type': 'ir.actions.act_window',
            'context': self.env.context,
            'target': 'new'
        }
        
    def _compute_show_trasfer_fn_goods_button(self):
        for rec in self:
            # check if all mo state is done or to_close
            yes = False
            for mo in rec.mrp_order_ids:
                if mo.state == 'done' or mo.state == 'to_close':
                    yes = True
            rec.show_transfer_fn_goods_button = yes
    
    show_transfer_fn_goods_button = fields.Boolean(compute='_compute_show_trasfer_fn_goods_button', string='Show Transfer FN Goods Button', default=False)
    
    def action_transfer_fn_goods(self):
        for rec in self:
            l_ids = self.env['mrp.production'].read_group([('mrp_plan_id', '=', rec.id), ('state', 'in', ('done', 'to_close'))], ['id', 'location_dest_id'], ['location_dest_id'])
            
            # raise UserError(_('There is not enough stock for: \r\n%s') % l_ids)
            
            # create picking
            for location in l_ids:
                lines = []
                mo__ids = self.env['mrp.production'].search([('mrp_plan_id', '=', rec.id), ('state', 'in', ('done', 'to_close')), ('location_dest_id', '=', location['location_dest_id'][0])])
                # ready picking lines 
                for line in mo__ids:
                    line_data = {
                        'name': line.product_id.name,
                        'product_id': line.product_id.id,
                        'product_uom_qty': line.qty_produced,
                        'product_uom': line.product_uom_id,
                        'location_id': location['location_dest_id'][0],
                    }
                    lines.append((0, 0, line_data))
                    
                # create picking
                picking_id = self.env['stock.picking'].create({
                    'picking_type_id': self.env['stock.picking.type'].search([('sequence_code', '=', 'MO')], limit=1).id,
                    'location_id': location['location_dest_id'][0],
                    'location_dest_id': location['location_dest_id'][0],
                    'move_ids_without_package': lines,
                    'mrp_plan_id': rec.id,
                    'origin': rec.name,
                })
                    
        # search all newly created picking
        picking_ids = self.env['stock.picking'].search([('mrp_plan_id', '=', self.id)])
        
        action = {
            'name': 'Transfer Finished Goods',
            'res_model': 'stock.picking',
            'type': 'ir.actions.act_window',
        }
        
        if len(picking_ids) == 1:
            action.update({
                'view_mode': 'form',
                'res_id': picking_ids[0].id,
                'target': 'new',
            })
        else:
            action.update({
                'view_mode': 'tree,form',
                'domain': [('mrp_plan_id', '=', self.id)],
                'target': 'current',
            })
        
        return action
    
    def _compute_transfer_fn_goods_count(self):
        for record in self:
            picking_ids = self.env['stock.picking'].search([('mrp_plan_id', '=', record.id)])
            record.transfer_fn_goods_count = len(picking_ids)
   
    transfer_fn_goods_count = fields.Integer(compute='_compute_transfer_fn_goods_count')
    
    def action_view_transfer_fn_goods(self):
        picking_ids = self.env['stock.picking'].search([('mrp_plan_id', '=', self.id)])
        
        action = {
            'name': 'Transfer Finished Goods',
            'type': 'ir.actions.act_window',
            'res_model': 'stock.picking',
            'target': 'current'
        }
        if len(picking_ids) == 1:
            action.update({
                'view_mode': 'form',
                'res_id': picking_ids[0].id
            })
        else:
            action.update({
                'view_mode': 'tree,form',
                'domain': [('mrp_plan_id', '=', self.id)]
            })

        return action