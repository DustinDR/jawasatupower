from odoo import models, fields


class ResConfigSettings(models.TransientModel):
	_inherit = 'res.config.settings'

	send_email_so_confirm = fields.Boolean('Email Notification', related='company_id.send_email_so_confirm', readonly=False)
	send_system_so_confirm = fields.Boolean('System Notification', related='company_id.send_system_so_confirm', readonly=False)
	send_email_user_ids = fields.Many2many('res.users', related='company_id.send_email_user_ids')
	send_system_user_ids = fields.Many2many('res.users', related='company_id.send_system_user_ids')
	send_whatsapp_so_confirm = fields.Boolean('WhatsApp Notification', related='company_id.send_whatsapp_so_confirm', readonly=False)
	connector_whatsapp_so_confirm = fields.Many2one('acrux.chat.connector', config_parameter='equip3_manuf_operations.connector_whatsapp_so_confirm')
