from odoo import models, fields, _
from datetime import date


class MRPMaterialRequest(models.AbstractModel):
    _name = 'mrp.material.request'
    _description = 'MRP Material Request'
    
    def _get_origin_moves(self):
        self.ensure_one()
        if self._name == 'mrp.plan':
            return self.plan_id
        elif self._name == 'mrp.workorder':
            return self.workorder_id
        if 'name' in self.env[self._name]._fields:
            return self.name
        return ''
    
    def _get_material_moves(self):
        self.ensure_one()
        if self._name == 'mrp.plan':
            return self.mo_stock_move_ids
        elif 'move_raw_ids' in self.env[self._name]._fields:
            return self.move_raw_ids
        return []

    def _get_location_moves(self):
        self.ensure_one()
        if self._name == 'mrp.plan':
            return self.mrp_order_ids[0].location_src_id
        elif self._name == 'mrp.production':
            return self.location_src_id
        elif self._name == 'mrp.workorder':
            return self.production_id.location_src_id
        return self.env['stock.location']
    
    def _get_hide_change_component_btn(self):
        self.ensure_one()
        if self._name in ('mrp.plan', 'mrp.production'):
            return any(wo.state in ['progress', 'done'] for wo in self.workorder_ids)
        return True

    def _compute_material_tab_technical_fields(self):
        material_request = self.env['material.request']
        stock_picking = self.env['stock.picking']
        purchase_request = self.env['purchase.request']
        for record in self:
            name = record._get_origin_moves()
            material_request_count = material_request.search_count([('source_document', '=', name)])
            transfer_request_count = stock_picking.search_count([('origin', '=', name)])
            purchase_request_count = purchase_request.search_count([('origin', '=', name)])
            hide_change_component_btn = record._get_hide_change_component_btn()
            record.write({
                'material_request_count': material_request_count,
                'transfer_request_count': transfer_request_count,
                'purchase_request_count': purchase_request_count,
                'hide_change_component_btn': hide_change_component_btn
            })

    material_request_count = fields.Integer(compute=_compute_material_tab_technical_fields)
    purchase_request_count = fields.Integer(compute=_compute_material_tab_technical_fields)
    transfer_request_count = fields.Integer(compute=_compute_material_tab_technical_fields)
    hide_change_component_btn = fields.Boolean(compute=_compute_material_tab_technical_fields)

    def action_transfer_request(self):
        origin = self._get_origin_moves()
        location = self._get_location_moves()
        material_moves = self._get_material_moves()
        
        lines = []
        for line in material_moves:
            availability = line.product_uom_qty - line.forecast_qty
            if availability > 0:
                line_data = {
                    'name': origin,
                    'product_id': line.product_id.id,
                    'product_uom_qty': line.product_uom_qty,
                    'product_uom': line.product_uom
                }
                lines.append((0, 0, line_data))

        picking_type_id = self.env['stock.picking.type'].search([
            ('sequence_code', '=', 'MO'),
            ('default_location_dest_id', '=', location.id)
        ], limit=1)

        picking_values = {
            'force_manuf_picking_type': True,
            'default_picking_type_id': picking_type_id.id,
            'default_move_ids_without_package': lines,
            'default_origin': origin,
            'default_is_readonly_origin': True
        }

        return {
            'name': _('Transfer Stock'),
            'view_mode': 'form',
            'res_model': 'stock.picking',
            'type': 'ir.actions.act_window',
            'context': picking_values,
            'target': 'new'
        }

    def action_purchase_request(self):
        origin = self._get_origin_moves()
        location = self._get_location_moves()
        material_moves = self._get_material_moves()

        stock_move_ids = material_moves.filtered(lambda l: l.forecast_availability <= 0)
        line_ids = [(0, 0, {
            'name': line.product_id and line.product_id.name or '',
            'product_id': line.product_id and line.product_id.id or False,
            'product_uom_id': line.product_id and line.product_id.uom_id and line.product_id.uom_id.id or False,
            'product_qty': line.product_uom_qty - line.forecast_qty,
            'date_required': date.today()
        }) for line in stock_move_ids]
        picking_type_id = self.env.ref('stock.picking_type_in').id

        purchase_request_values = {
            'default_picking_type_id': picking_type_id,
            'default_company_id': self.env.company.id,
            'default_line_ids': line_ids,
            'default_origin': origin,
            'default_is_goods_orders': True,
            'default_branch_id': self.branch.id,
            'default_destination_warehouse': location.get_warehouse().id,
            'default_request_date': fields.Datetime.now(),
            'default_is_readonly_origin': True
        }
        
        if self._name == 'mrp.production':
            purchase_request_values.update({
                'default_group_id': self.procurement_group_id.id,
            })

        return {
            'type': 'ir.actions.act_window',
            'name': _('Purchase Request'),
            'res_model': 'purchase.request',
            'target': 'new',
            'view_mode': 'form',
            'context': purchase_request_values
        }

    def action_material_request(self):
        origin = self._get_origin_moves()
        location = self._get_location_moves()
        material_moves = self._get_material_moves()

        stock_move_ids = material_moves.filtered(lambda l: l.product_uom_qty - l.forecast_qty > 0)
        material_request_line_ids = [(0, 0, {
            'description': line.product_id and line.product_id.name or '',
            'product_id': line.product_id and line.product_id.id or False,
            'uom_id': line.product_id and line.product_id.uom_id and line.product_id.uom_id.id or False,
            'qty': line.product_uom_qty - line.forecast_qty,
            'qty_done': 0.0,
            'destination_location': line.location_id.id or False,
            'state': 'draft'
        }) for line in stock_move_ids]

        material_request_values = {
            'default_material_request_line_ids': material_request_line_ids,
            'default_origin': origin,
            'default_destination_location': location.id,
            'default_destination_warehouse_id': location.get_warehouse().id,
            'default_is_readonly_origin': True
        }

        return {
            'type': 'ir.actions.act_window',
            'name': _('Material Request'),
            'res_model': 'material.request.wizard',
            'target': 'new',
            'view_mode': 'form',
            'context': material_request_values
        }

    def action_change_component(self):
        if self._name == 'mrp.workorder':
            return

        material_moves = self._get_material_moves()

        line_ids = []
        for line in material_moves:
            alternative_component_ids = False
            if line.raw_material_production_id and line.raw_material_production_id.bom_id and line.raw_material_production_id.bom_id:
                bom_lines = line.raw_material_production_id.bom_id.bom_line_ids.filtered(
                    lambda x: x.product_tmpl_id and x.product_tmpl_id.id == line.product_tmpl_id.id)
                if bom_lines:
                    alternative_component_ids = [(6, 0, bom_lines[0].alternative_component_ids.ids)]
            line_ids.append((0, 0, {
                'product_id': line.product_id and line.product_id.id or False,
                'product_tmpl_id': line.product_tmpl_id and line.product_tmpl_id.id or False,
                'product_qty': line.product_uom_qty,
                'bom_id': line.raw_material_production_id and line.raw_material_production_id.bom_id and line.raw_material_production_id.bom_id.id or False,
                'move_id': line.id,
                'alternative_component_ids': alternative_component_ids,
                'production_id': line.raw_material_production_id and line.raw_material_production_id.id or False,
                'operation_id': line.operation_id and line.operation_id.id or False
            }))

        change_component_values = {
            'default_line_ids': line_ids
        }
        
        if self._name == 'mrp.production':
            change_component_values.update({
                'default_production_id': self.id,
                'hide_mo_field': True,
            })
        elif self._name == 'mrp.plan':
            change_component_values.update({
                'default_production_ids': [(6, 0, self.mrp_order_ids.ids)],
            })

        return {
            'name': _('Change Material'),
            'type': 'ir.actions.act_window',
            'res_model': 'mrp.change.component.wizard',
            'target': 'new',
            'view_mode': 'form',
            'context': change_component_values
        }

    def action_view_model(self, action_name, action_model, action_view, action_domain):
        self.ensure_one()
        if not action_name or not action_model or not action_view or not action_domain:
            return
        result = self.env['ir.actions.actions']._for_xml_id(action_name)
        records = self.env[action_model].search(action_domain)
        if not records:
            return
        if len(records) > 1:
            result['domain'] = [('id', 'in', records.ids)]
        else:
            form_view = [(self.env.ref(action_view).id, 'form')]
            if 'views' in result:
                result['views'] = form_view + [(s, v) for s, v in result['views'] if v != 'form']
            else:
                result['views'] = form_view
            result['res_id'] = records.id
        result['context'] = str(dict(eval(result.get('context') or '{}', self._context), create=False))
        return result

    def action_view_transfer_request(self):
        return self.action_view_model(
            'stock.action_picking_tree_all',
            'stock.picking',
            'stock.view_picking_form',
            [('origin', '=', self._get_origin_moves())],
        )

    def action_view_purchase_request(self):
        return self.action_view_model(
            'purchase_request.purchase_request_form_action',
            'purchase.request',
            'purchase_request.view_purchase_request_form',
            [('origin', '=', self._get_origin_moves())],
        )

    def action_view_material_request(self):
        return self.action_view_model(
            'equip3_inventory_operation.material_request_action',
            'material.request',
            'equip3_inventory_operation.material_request_form_view',
            [('source_document', '=', self._get_origin_moves())],
        )
