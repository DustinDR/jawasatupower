

from ast import literal_eval
import pytz
from datetime import datetime
import logging
import sys
import requests
import json
from odoo.addons.acrux_chat.tools import TIMEOUT, log_request_error, get_image_from_url
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from odoo.http import request

_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
	_inherit = 'sale.order'

	@api.depends('mrp_order_ids')
	def _compute_mrp_orders(self):
		for record in self:
			record.mrp_order_count = len(record.mrp_order_ids)

	@api.depends('mrp_plan_ids')
	def _compute_mrp_plans(self):
		for record in self:
			record.mrp_plan_count = len(record.mrp_plan_ids)

	mrp_plan_ids = fields.One2many('mrp.plan', 'sale_order_id', string='Manufacturing Plan')
	mrp_order_ids = fields.One2many('mrp.production', 'sale_order_id', string='Manufacturing Orders')
	mrp_order_count = fields.Integer(compute='_compute_mrp_orders')
	mrp_plan_count = fields.Integer(compute='_compute_mrp_plans')

	def _check_for_manufacturing_plan_orders(self):

		mrp_order_ids = self.env['mrp.production']
		mrp_plan_ids = self.env['mrp.plan']

		company_id = self.company_id or self.env.company
		branch_id = self.env.user.branch_id

		for line in self.order_line:
			
			if not line.product_id.bom_ids:
				continue

			if line.product_id.manuf_auto_create == 'auto_mp':

				approval_matrix_id = self.env['mrp.approval.matrix'].sudo().search([
					('company', '=', company_id.id),
					('branch', '=', branch_id.id),
					('matrix_type', '=', 'mp')
				], limit=1)

				if not approval_matrix_id:
					raise ValidationError(_(
						'Please create approval matrix for manufacturing plan first!'
					))

				mrp_plan_id = self.env['mrp.plan'].create({
					'name': line.product_id.name + ' (' + self.name + ')',
					'sale_order_id': self.id,
					'approval_matrix': approval_matrix_id.id,
				})
				mrp_prod_wiz = self.env['mrp.production.wizard'].create({
					'plan_id': mrp_plan_id.id,
					'line_ids': [(0, 0, {
						'product_id': line.product_id.id,
						'product_qty': line.product_uom_qty,
						'product_uom': line.product_id.uom_id.id
					})]
				})
				mrp_prod_wiz.confirm()
				mrp_plan_ids += mrp_plan_id

			if line.product_id.manuf_auto_create == 'auto_mo':
				approval_matrix_id = self.env['mrp.approval.matrix'].sudo().search([
					('company', '=', company_id.id),
					('branch', '=', branch_id.id),
					('matrix_type', '=', 'mo')
				], limit=1)

				if not approval_matrix_id:
					raise ValidationError(_(
						'Please create approval matrix for manufacturing order first!'
					))

				bom_id = self.env['mrp.bom']._bom_find(
					product=line.product_id, 
					company_id=self.company_id.id, 
					bom_type='normal'
				)

				mrp_order_id = self.env['mrp.production'].sudo().create({
					'product_id': line.product_id.id,
					'product_uom_id': line.product_id.uom_id.id,
					'sale_order_id': self.id,
					'bom_id': bom_id.id,
					'product_qty': line.product_uom_qty,
					'approval_matrix': approval_matrix_id.id,
				})
				mrp_order_id._onchange_move_raw()
				mrp_order_id._onchange_move_finished()
				mrp_order_id._onchange_workorder_ids()
				mrp_order_ids += mrp_order_id

		if mrp_plan_ids:
			self.mrp_plan_ids = [(6, 0, mrp_plan_ids.ids)]

		if mrp_order_ids:
			self.mrp_order_ids = [(6, 0, mrp_order_ids.ids)]

	def get_mrp_button(self):
		table = '<table class="table table-borderless"><tbody>'
		for i in range(max([len(self.mrp_order_ids), len(self.mrp_plan_ids)])):
			table += '<tr>'
			try:
				mrp_order_id = self.mrp_order_ids[i]
				table += f'<td><a style="color:#fff;background-color:#875A7B; padding:8px 16px 8px 16px; border-radius:5px" href="/mail/view?model=mrp.production&amp;res_id={mrp_order_id.id}" data-oe-model="mrp.production" data-oe-id="{mrp_order_id.id}">View MO</a></td>'
			except IndexError:
				table += '<td/>'

			try:
				mrp_plan_id = self.mrp_plan_ids[i]
				table += f'<td><a style="color:#fff;background-color:#875A7B; padding:8px 16px 8px 16px; border-radius:5px" href="/mail/view?model=mrp.plan&amp;res_id={mrp_plan_id.id}" data-oe-model="mrp.plan" data-oe-id="{mrp_plan_id.id}">View MP</a></td>'
			except IndexError:
				table += '<td/>'
			table += '</tr>'
			
		table += '</tbody></table>'
		return table

	def get_local_date(self, date):
		user_tz = self.env.user.tz or pytz.utc
		local = pytz.timezone(user_tz)
		local_date = datetime.strftime(pytz.utc.localize(datetime.strptime(datetime.strftime(date, '%m/%d/%Y %H:%M:%S'),
			'%m/%d/%Y %H:%M:%S')).astimezone(local),"%m/%d/%Y %H:%M:%S"
		)
		return local_date

	def _send_email_notification(self):
		user_ids = self.env['mrp.notification'].search([('type', '=', 'email')]).mapped('receiver_ids').mapped('user_id')
		local_date = self.get_local_date(self.date_order)

		for user_id in user_ids:
			mail = self.env.ref('equip3_manuf_operations.sale_order_auto_mail_template')
			mail.partner_to = str(user_id.partner_id.id)
			mail.lang = user_id.partner_id.lang
			mail.body_html = f"""
			<p>Dear {user_id.name},</p>
			<p>Administrator has approved the Sales Order ({self.name}) on {local_date}</p>
			{self.get_mrp_button()}
			<p>Best Regards.</p>
			"""

			mail.send_mail(self.id, force_send=True)
			_logger.info(f'Mail has been sent to {user_id.name} on {local_date}')

	def _send_system_notification(self):
		user_ids = self.env['mrp.notification'].search([('type', '=', 'system')]).mapped('receiver_ids').mapped('user_id')
		local_date = self.get_local_date(self.date_order)

		for user_id in user_ids:
			mail_message_id = self.env['mail.message'].create({
				'email_from': self.env.user.email_formatted,
				'author_id': self.env.user.partner_id.id,
				'message_type': 'email',
				'is_internal': True,
				'body': f"""<p>Dear {user_id.partner_id.name},</p>
				<p>Administrator has approved the Sales Order ({self.name}) on {local_date}</p>
				{self.get_mrp_button()}
				<p style="margin:16px 0px 16px 0px">Best Regards.</p>
				"""
			})

			mail_notif = self.env['mail.notification'].create({
				'mail_message_id': mail_message_id.id,
				'notification_type': 'inbox',
				'res_partner_id': user_id.partner_id.id
			})

	def _send_whatsapp_notification(self):
		user_ids = self.env['mrp.notification'].search([('type', '=', 'whatsapp')]).mapped('receiver_ids').mapped('user_id')
		local_date = self.get_local_date(self.date_order)

		for user in user_ids:
			message = f"""
			Dear {user.name},
			{self.env.user.partner_id.name} has confirmed the Sales Order ({self.name}) on {local_date} which auto created the Manufacturing Plan/Manufacturing Order {self.get_mrp_plan_link(self.mrp_plan_ids.id)} / {self.get_mrp_link(self.mrp_order_ids.id)}
			Best Regards.
			"""
			if user.partner_id.mobile:
				phone_num = str(user.partner_id.mobile)
				if "+" in phone_num:
					phone_num =  phone_num.replace("+","")
				param = {'body': message, 'phone': phone_num}
				self.ca_request('post','sendMessage',param)
	def get_mrp_link(self, rec_id):
		base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
		return '%s/web#id=%s&model=mrp.production&view_type=form' % (base_url, rec_id)

	def get_mrp_plan_link(self, rec_id):
		base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
		if rec_id:
			return '%s/web#id=%s&model=mrp.plan&view_type=form' % (base_url, rec_id)
		else:
			return False
	
	def ca_get_endpoint(self, resource_path):
		api_key = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
		end_point = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
		return '%s/%s?token=%s' % (end_point.strip('/'), resource_path, api_key)
    
	def ca_request(self, req_type, path, param={}, timeout=False):
		def response_handle_error(req):
			error = False
			try:
				ret = req.json()
			except ValueError as _e:
				ret = {}
			err = ret.get('error', 'Send Error')
			message = ret.get('message', 'Send Error')
			if req.status_code == 401:
				error = err
			elif not 200 <= req.status_code <= 299:
				error = err or message
			if error:
				log_request_error([error, req_type, path, param], req)
				raise ValidationError(error)
			return ret
		self.ensure_one()
		result = {}
		timeout = timeout or TIMEOUT
		url = self.ca_get_endpoint(path)
		header = {'Accept': 'application/json'}
		req = False
		try:
			if req_type == 'post':
				data = json.dumps(param)
				header.update({'Content-Type': 'application/json'})
				w = len(data) / 20000
				timeout = (int(max(10, w)), 20)
				req = requests.post(url, data=data, headers=header, timeout=timeout, verify=True)
				result = response_handle_error(req)
		except requests.exceptions.SSLError as _err:
			log_request_error(['SSLError', req_type, path, param])
			raise UserError(_('Error! Could not connect to Chat-Api server. '
								'Please in the connector settings, set the '
								'parameter "Verify" to false by unchecking it and try again.'))
		except requests.exceptions.ConnectTimeout as _err:
			log_request_error(['ConnectTimeout', req_type, path, param])
			raise UserError(_('Timeout error. Try again...'))
		except (requests.exceptions.HTTPError,
				requests.exceptions.RequestException,
				requests.exceptions.ConnectionError) as _err:
			log_request_error(['requests', req_type, path, param])
			ex_type, _ex_value, _ex_traceback = sys.exc_info()
			raise UserError(_('Error! Could not connect to Chat-Api account.\n%s') % ex_type)
		self.print_result(req_type, url, result, param, req)
		return result

	def print_result(self, req_type, url, result, param, req):
		try:
			Host = request.httprequest.headers.get('Host')
			if Host.startswith('localhost'):
				print('status =', req and req.status_code or 'except request')
				print(request.httprequest.headers)
				print('%%%% => %s %s' % (req_type.upper(), url))
				if param:
					body = param.get('body', False)
					if body:
						param['body'] = body[0:100]
					data = json.dumps(param, indent=2, sort_keys=True)
					data = data.replace('\\"', "'")
					print(data)
				print('################ resultado')
				data = json.dumps(result, indent=2, sort_keys=True)
				print(data)
		except RuntimeError:
			pass
			

	def action_confirm(self):
		super(SaleOrder, self).action_confirm()

		if self.company_id.sales_to_manufacturing:
			self._check_for_manufacturing_plan_orders()

			if self.company_id.send_email_so_confirm:
				self._send_email_notification()

			if self.company_id.send_system_so_confirm:
				self._send_system_notification()
    
			# Send whatsapp notification
			if self.company_id.send_whatsapp_so_confirm:
				self._send_whatsapp_notification()
			
		return True

	def action_view_manufacturing(self):
		if self.mrp_order_ids or self.mrp_plan_ids:
			model = self.env.context.get('model')
			name = model == 'mrp.production' and 'Manufacturing Order' or "Manufacturing Plan"
			mrp_ids = model == 'mrp.production' and self.mrp_order_ids or self.mrp_plan_ids
			
			action = {
				'name': name,
				'type': 'ir.actions.act_window',
				'res_model': model,
				'target': 'current'
			}
			if len(mrp_ids) == 1:
				action.update({
					'view_mode': 'form',
					'res_id': mrp_ids[0].id
				})
			else:
				action.update({
					'view_mode': 'tree,form',
					'domain': [('sale_order_id', '=', self.id)]
				})

			return action