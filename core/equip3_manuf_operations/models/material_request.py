# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import UserError
from odoo.addons.stock.models.stock_move import PROCUREMENT_PRIORITIES
from datetime import date, datetime


class MrpMaterialRequestInherit(models.Model):
    _name = "material.request"
    _inherit = ["material.request"]


    mo_id = fields.Many2one("mrp.production", string="Order")
    wo_id = fields.Many2one("mrp.workorder", string="Work Order")
    plan_id = fields.Many2one("mrp.plan", string="Manufacturing Plan")