# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class MrpApprovalMatrix(models.Model):
    _name = 'mrp.approval.matrix'
    _description = 'MRP Approval Matrix'
    _inherit = 'mail.thread'

    MATRIX_TYPE = [
        ('mp', 'Manufacturing Plan'),
        ('mo', 'Manufacturing Order')
    ]

    name = fields.Char('Name', required=True, tracking=True,)
    company = fields.Many2one('res.company', 'Company', tracking=True, default=lambda self: self.env.company)
    branch = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id, tracking=True)
    mrp_approval_matrix_ids = fields.One2many('mrp.approval.matrix.line', 'mrp_approval_matrix_id',  string='Mrp Plans', tracking=True)
    matrix_type = fields.Selection(MATRIX_TYPE, string='Matrix Type', default='mp', tracking=True)

    @api.model
    def create(self, vals):
        if 'branch' in vals and vals.get('branch') != '':
            if 'matrix_type' in vals and vals.get('matrix_type') == 'mp' and self.env['mrp.approval.matrix'].sudo().search([('company', '=', self.env.company.id), ('branch', '=', int(vals.get('branch'))), ('matrix_type', '=', 'mp')], limit=1):
                raise UserError(
                    _("The branch already used for another Approval Matrix. Please choose another branch."))
            if 'matrix_type' in vals and vals.get('matrix_type') == 'mo' and self.env['mrp.approval.matrix'].sudo().search([('company', '=', self.env.company.id), ('branch', '=', int(vals.get('branch'))), ('matrix_type', '=', 'mo')], limit=1):
                raise UserError(
                    _("The branch already used for another Approval Matrix. Please choose another branch."))
        return super(MrpApprovalMatrix, self).create(vals)

    def write(self, values):
        if 'branch' in values and values.get('branch') != '':
            if 'matrix_type' in values and values.get('matrix_type') == 'mp' and self.env['mrp.approval.matrix'].sudo().search([('company', '=', self.env.company.id), ('branch', '=', int(values.get('branch'))), ('matrix_type', '=', 'mp')], limit=1):
                raise UserError(_("The branch already used for another Approval Matrix. Please choose another branch."))
            if 'matrix_type' in values and values.get('matrix_type') == 'mo' and self.env['mrp.approval.matrix'].sudo().search([('company', '=', self.env.company.id), ('branch', '=', int(values.get('branch'))), ('matrix_type', '=', 'mo')], limit=1):
                raise UserError(_("The branch already used for another Approval Matrix. Please choose another branch."))
        return super(MrpApprovalMatrix, self).write(values)


class MrpApprovalMatrixLine(models.Model):
    _name = 'mrp.approval.matrix.line'
    _description = 'MRP Approval Matrix Line'

    mrp_approval_matrix_id = fields.Many2one('mrp.approval.matrix', 'Mrp Approval Matrix')
    sequence = fields.Integer('Sequence', compute='compute_sequence')
    sequence_handle = fields.Integer('Sequence Handle')
    approve = fields.Many2many('res.users', string='Approver', required=True)
    minimum_approver = fields.Integer('Minimum Approver', default="1", required=True)
    approval_status = fields.Char('Approval Status')
    requested_time = fields.Datetime('Requested Time')
    approved_time = fields.Datetime('Approved Time ')

    def compute_sequence(self):
        for rec in self.mapped('mrp_approval_matrix_id'):
            count_seq = 1
            for line in rec.mrp_approval_matrix_ids:
                line.sequence = count_seq
                count_seq += 1


class MrpPlanApprovalMatrixLine(models.Model):
    _name = 'mrp.plan.approval.matrix.line'
    _description = 'MRP Plan Approval Matrix Line'

    mrp_plan_approval_matrix_id = fields.Many2one('mrp.plan', 'Mrp Plan')
    sequence_handle = fields.Integer('Sequence Handle')
    sequence = fields.Integer('Sequence')
    approve = fields.Many2many('res.users', 'mrp_user_approve_line_rel','mrp_plan_id', 'aproval_user_line_id' ,string='Approver', required=True)
    minimum_approver = fields.Integer('Minimum Approver', required=True)
    # approval_status = fields.Char('Approval Status')
    state = fields.Selection([
        ('approved', 'Approved'),
        ('rejected', 'Rejected')
    ], string='Approval Status')
    requested_time = fields.Datetime('Requested Time')
    approved_time = fields.Datetime('Approved Time ')
    # approve_by = fields.Many2one('res.users', 'Approver By')
    approve_by = fields.Many2many('res.users', 'mrp_approve_line_rel','mrp_plan_id', 'aproval_line_id', string='Approved By')
    approved_count = fields.Integer('Approved Count')
    approval_matrix_status = fields.Text(string='Approval Status')


class MOApprovalMatrixLine(models.Model):
    _name = 'mo.approval.matrix.line'
    _description = 'MO Approval Matrix Line'

    mo_approval_matrix_id = fields.Many2one('mrp.production', 'Manufacturing Order')
    sequence_handle = fields.Integer('Sequence Handle')
    sequence = fields.Integer('Sequence')
    approve = fields.Many2many('res.users', 'mo_user_approve_line_rel','mo_id', 'aproval_user_line_id' ,string='Approver', required=True)
    minimum_approver = fields.Integer('Minimum Approver', required=True)
    # approval_status = fields.Char('Approval Status')
    state = fields.Selection([
        ('approved', 'Approved'),
        ('rejected', 'Rejected')
    ], string='Approval Status')
    requested_time = fields.Datetime('Requested Time')
    approved_time = fields.Datetime('Approved Time ')
    # approve_by = fields.Many2one('res.users', 'Approver By')
    approve_by = fields.Many2many('res.users', 'mo_approve_line_rel','mo_id', 'aproval_line_id', string='Approved By')
    approved_count = fields.Integer('Approved Count')
    approval_matrix_status = fields.Text(string='Approval Status')







