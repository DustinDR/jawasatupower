import datetime

from odoo import models
from odoo.addons.resource.models.resource import make_aware, Intervals
from datetime import timedelta
from functools import partial
from odoo.tools.float_utils import float_compare


class MrpWorkcenter(models.Model):
    _inherit = 'mrp.workcenter'

    def _get_first_available_slot(self, start_datetime, duration):
        """Get the first available interval for the workcenter in `self`.

        The available interval is disjoinct with all other workorders planned on this workcenter, but
        can overlap the time-off of the related calendar (inverse of the working hours).
        Return the first available interval (start datetime, end datetime) or,
        if there is none before 700 days, a tuple error (False, 'error message').

        :param start_datetime: begin the search at this datetime
        :param duration: minutes needed to make the workorder (float)
        :rtype: tuple
        """
        self.ensure_one()
        self.resource_id.tz = self.env.user.tz
        start_datetime, revert = make_aware(start_datetime)

        get_available_intervals = partial(self.resource_calendar_id._work_intervals, domain=[('time_type', 'in', ['other', 'leave'])], resource=self.resource_id)
        get_workorder_intervals = partial(self.resource_calendar_id._leave_intervals, domain=[('time_type', '=', 'other')], resource=self.resource_id)

        remaining = duration
        start_interval = start_datetime
        delta = timedelta(days=14)

        for n in range(50):  # 50 * 14 = 700 days in advance (hardcoded)
            dt = start_datetime + delta * n
            available_intervals = get_available_intervals(dt, dt + delta)
            workorder_intervals = get_workorder_intervals(dt, dt + delta)
            for origin_start, origin_stop, dummy in available_intervals:
                if dummy.break_from and dummy.break_to:
                    bf_hours = int((dummy.break_from * 60) / 60)
                    bf_minutes = int((dummy.break_from * 60) - (bf_hours * 60))

                    bt_hours = int((dummy.break_to * 60) / 60)
                    bt_minutes = int((dummy.break_to * 60) - (bt_hours * 60))

                    break_from = origin_start.replace(hour=bf_hours, minute=bf_minutes, second=0, microsecond=0)
                    break_to = origin_start.replace(hour=bt_hours, minute=bt_minutes, second=0, microsecond=0)

                    if origin_start < break_from:
                        starts = [origin_start, break_to]
                        stops = [break_from, origin_stop]
                    else:
                        starts = [origin_start]
                        stops = [origin_stop]

                else:
                    starts = [origin_start]
                    stops = [origin_stop]

                for start, stop in zip(starts, stops):
                    interval_minutes = (stop - start).total_seconds() / 60
                    # If the remaining minutes has never decrease update start_interval
                    if remaining == duration:
                        start_interval = start
                    # If there is a overlap between the possible available interval and a others WO
                    if Intervals([(start_interval, start + timedelta(minutes=min(remaining, interval_minutes)), dummy)]) & workorder_intervals:
                        remaining = duration
                        start_interval = start
                    elif float_compare(interval_minutes, remaining, precision_digits=3) >= 0:
                        return revert(start_interval), revert(start + timedelta(minutes=remaining))
                    # Decrease a part of the remaining duration
                    remaining -= interval_minutes
        return False, 'Not available slot 700 days after the planned start'
