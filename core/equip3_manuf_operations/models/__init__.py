# -*- coding: utf-8 -*-

from . import mrp_approval_matrix
from . import mrp_material_request
from . import mrp_labor_assign
from . import stock_move
from . import mrp_production
from . import mrp_workorder
from . import mrp_plan
from . import product_template
from . import sale
from . import mrp_notification
from . import res_company
from . import res_config_settings
from . import mrp_unbuild
from . import stock_scrap
# from . import mrp_workcenter
from . import resource_calendar_leaves
