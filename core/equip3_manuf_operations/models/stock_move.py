# -*- coding: utf-8 -*-
from collections import defaultdict

from odoo import fields, models, api

class StockPicking(models.Model):
    _inherit = 'stock.picking'
    
    mrp_plan_id = fields.Many2one('mrp.plan', string='Manufacturing Plan')
    
    @api.constrains("mrp_plan_id",)
    def _update_origin(self):
        for rec in self:
            if rec.mrp_plan_id:
                # rec.origin = rec.mrp_plan_id.name
                rec.write({'origin': rec.mrp_plan_id.name})
    
class StockMove(models.Model):
    _inherit = 'stock.move'

    mrp_plan_id = fields.Many2one('mrp.plan', string='Manufacturing Plan')
    mrp_workorder_component_id = fields.Many2one('mrp.workorder', string='Manufacturing Consumption ID')
    mrp_workorder_byproduct_id = fields.Many2one('mrp.workorder', string='Manufacturing Byproduct ID')
    forecast_qty = fields.Float(string='Available', compute='_get_forecast_qty')
    production_byproduct_loc_id = fields.Many2one('stock.location', string='Produced in Location')
    operation_id = fields.Many2one(
        'mrp.routing.workcenter', 'Operation To Consume', check_company=True,
        domain="[('id', 'in', allowed_operation_ids)]", compute="_get_bom_operation_id", store=True, readonly=False)

    @api.depends('bom_line_id')
    def _get_bom_operation_id(self):
        for move in self:
            if move.bom_line_id and move.bom_line_id.operation_id:
                move.operation_id = move.bom_line_id.operation_id.id
            else:
                move.operation_id = False

    @api.depends('product_id')
    def _get_forecast_qty(self):
        for move in self:
            if move.product_id and move.location_id:
                quants = self.env['stock.quant'].sudo().search([('product_id', '=', move.product_id.id),('location_id', 'child_of', move.location_id.id)])
                if quants:
                    move.forecast_qty = sum(quants.mapped('available_quantity'))
                else:
                    move.forecast_qty = 0
                #move.forecast_qty = move.product_id.with_context(location=move.location_id.id).qty_available
            elif move.product_id and not move.location_id:
                move.forecast_qty = move.product_id.qty_available
            else:
                move.forecast_qty = 0

    @api.depends('product_id', 'picking_type_id', 'picking_id', 'reserved_availability', 'priority', 'state',
                 'product_uom_qty', 'location_id')
    def _compute_forecast_information(self):
        """ Compute forecasted information of the related product by warehouse."""
        self.forecast_availability = False
        self.forecast_expected_date = False

        not_product_moves = self.filtered(lambda move: move.product_id.type != 'product')
        for move in not_product_moves:
            move.forecast_availability = move.product_qty

        product_moves = (self - not_product_moves)
        warehouse_by_location = {loc: loc.get_warehouse() for loc in product_moves.location_id}

        outgoing_unreserved_moves_per_warehouse = defaultdict(lambda: self.env['stock.move'])
        for move in product_moves:
            picking_type = move.picking_type_id or move.picking_id.picking_type_id
            is_unreserved = move.state in ('waiting', 'confirmed', 'partially_available')
            if picking_type.code in self._consuming_picking_types() and is_unreserved:
                outgoing_unreserved_moves_per_warehouse[warehouse_by_location[move.location_id]] |= move
            elif picking_type.code in self._consuming_picking_types():
                move.forecast_availability = move.reserved_availability

        for warehouse, moves in outgoing_unreserved_moves_per_warehouse.items():
            if not warehouse:  # No prediction possible if no warehouse.
                continue
            product_variant_ids = moves.product_id.ids
            wh_location_ids = [loc['id'] for loc in self.env['stock.location'].search_read(
                [('id', 'child_of', warehouse.view_location_id.id)],
                ['id'],
            )]
            forecast_lines = self.env['report.stock.report_product_product_replenishment'] \
                ._get_report_lines(None, product_variant_ids, wh_location_ids)
            for move in moves:
                forecast_availability = 0
                if move.location_id:
                    forecast_availability = move.product_id.with_context(location=move.location_id.id).qty_available
                lines = [l for l in forecast_lines if
                         l["move_out"] == move._origin and l["replenishment_filled"] is True]
                if lines:
                    if not move.location_id:
                        forecast_availability = sum(m['quantity'] for m in lines)
                    move_ins_lines = list(filter(lambda report_line: report_line['move_in'], lines))
                    if move_ins_lines:
                        expected_date = max(m['move_in'].date for m in move_ins_lines)
                        move.forecast_expected_date = expected_date
                move.forecast_availability = forecast_availability
