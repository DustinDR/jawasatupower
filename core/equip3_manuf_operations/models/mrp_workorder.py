# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import date


class Equip3MrpWorkOrder(models.Model):
    _name = 'mrp.workorder'
    _inherit = ['mrp.workorder', 'mrp.material.request']

    @api.model
    def create(self, vals):
        vals['workorder_id'] = self.env['ir.sequence'].next_by_code('mrp.workorder')
        return super(Equip3MrpWorkOrder, self).create(vals)

    @api.depends('assign_ids', 'assign_ids.labor_id', 'assign_ids.labor_id.user_id')
    def _compute_wo_user_ids(self):
        for record in self:
            employee_ids = record.assign_ids.mapped('labor_id')
            user_ids = employee_ids.mapped('user_id') | self.env.user
            record.wo_user_ids = [(6, 0, user_ids.ids)]

    @api.depends('leave_id')
    def _compute_dates_planned(self):
        super(Equip3MrpWorkOrder, self)._compute_dates_planned()
        for workorder in self:
            workorder.date_planned_start = workorder.leave_id.datetime_from
            workorder.date_planned_finished = workorder.leave_id.datetime_to

    def _set_dates_planned(self):
        super(Equip3MrpWorkOrder, self)._set_dates_planned()
        date_from = self[0].date_planned_start
        date_to = self[0].date_planned_finished
        self.mapped('leave_id').sudo().write({
            'datetime_from': date_from,
            'datetime_to': date_to
        })

    def _compute_working_users(self):
        for order in self:
            order.working_user_ids = [(4, order.id) for order in order.time_ids.filtered(lambda time: not time.date_end).sorted('date_start').mapped('user_id')]
            if order.working_user_ids:
                order.last_working_user_id = order.working_user_ids[-1]
            elif order.time_ids:
                order.last_working_user_id = order.time_ids.filtered('date_end').sorted('date_end')[-1].user_id if order.time_ids.filtered('date_end') else order.time_ids[-1].user_id
            else:
                order.last_working_user_id = False
            if order.time_ids.filtered(lambda x: (x.user_id.id in order.wo_user_ids.ids) and (not x.date_end) and (x.loss_type in ('productive', 'performance'))):
                order.is_user_working = True
            else:
                order.is_user_working = False

    def _compute_actual_date(self):
        time = len(self.time_ids)
        for rec in self:
            i = 0
            rec.actual_start_date = False
            rec.actual_end_date = False
            for time_id in rec.time_ids:
                i += 1
                if time_id:
                    if i == 1:
                        rec.actual_end_date = time_id.date_end
                    if i == time:
                        rec.actual_start_date = time_id.date_start

    def _compute_actual_duration(self):
        for rec in self:
            rec.actual_duration = 0
            for time_id in rec.time_ids:
                if time_id:
                    rec.actual_duration += time_id.duration

    state = fields.Selection([
        ('pending', 'Waiting for another WO'),
        ('ready', 'Ready'),
        ('progress', 'In Progress'),
        ('pause', 'Paused'),
        ('block', 'Blocked'),
        ('done', 'Finished'),
        ('cancel', 'Cancelled')], string='Status',
        default='pending', copy=False, readonly=True)

    workcenter_id = fields.Many2one(
        'mrp.workcenter', 'Work Center', required=False,
        states={'done': [('readonly', True)], 'cancel': [('readonly', True)], 'progress': [('readonly', True)]},
        group_expand='_read_group_workcenter_id', check_company=True)

    location_id = fields.Many2one(
        'stock.location', 'Location',
        states={'done': [('readonly', True)], 'cancel': [
            ('readonly', True)], 'progress': [('readonly', True)]},
        check_company=True)

    # TODO: not sure why move_raw_ids & byproduct_ids replaced here
    move_raw_ids = fields.One2many('stock.move', 'mrp_workorder_component_id', 'Components')
    byproduct_ids = fields.One2many('stock.move', 'mrp_workorder_byproduct_id', 'By-Products')

    mrp_plan_id = fields.Many2one(related='production_id.mrp_plan_id', string='Manufacturing Plan', store=True)
    workorder_id = fields.Char(string='Workorder ID')
    company_id = fields.Many2one(related='production_id.company_id')
    analytic_group = fields.Many2many(related='production_id.analytic_tag_ids')
    branch = fields.Many2one(related='production_id.branch')

    actual_duration = fields.Float('Actual Duration', compute='_compute_actual_duration')
    actual_start_date = fields.Datetime('Actual Start Date', compute='_compute_actual_date')
    actual_end_date = fields.Datetime('Actual End Date', compute='_compute_actual_date')

    work_center_group_line_id = fields.Many2many('mrp.workcenter', 'work_center_group_line_id', 'operation_ids', 'work_center_line_id', string="Work Center Group Line Id")
    work_center_group_ids = fields.Many2many('mrp.workcenter', string="Work Center For MRP Plan")

    assign_ids = fields.One2many('mrp.labor.assign', 'workorder_id', string='Assigned Labors')
    wo_user_ids = fields.Many2many('res.users', compute=_compute_wo_user_ids, store=True)

    def button_start(self):
        result = super(Equip3MrpWorkOrder, self).button_start()
        for workorder in self:
            if workorder.mrp_plan_id:
                workorder.mrp_plan_id.sudo().write({
                    'state': 'progress'
                })
            workorder.write({'should_hide_duration': True})
        return result

    def button_finish(self):
        result = super(Equip3MrpWorkOrder, self).button_finish()
        for workorder in self:
            if workorder.mrp_plan_id:
                any_unfinished_wos = any(wo.state != 'done' for wo in workorder.mrp_plan_id.workorder_ids)
                if not any_unfinished_wos:
                    workorder.mrp_plan_id.sudo().write({'state': 'to_close'})
        return result

    def button_pending(self):
        self.end_previous()
        return self.write({'state': 'pause'})

    def button_unblock_all(self, order_ids):
        for workorder in order_ids:
            if workorder.workcenter_id.working_state == 'blocked':
                workorder.workcenter_id.unblock()
            if workorder.state == 'block':
                if workorder.qty_produced > 0:
                    workorder.sudo().write({'state': 'progress'})
                else:
                    workorder.sudo().write({'state': 'ready'})
        return True

    def button_unblock(self):
        for order in self:
            order.workcenter_id.unblock()
            if order.qty_produced > 0:
                order.sudo().write({'state': 'progress'})
            else:
                order.sudo().write({'state': 'ready'})
            order.button_unblock_all(order_ids=self.workcenter_id.order_ids - order)
        return True


class MrpWorkcenterProductivity(models.Model):
    _inherit = "mrp.workcenter.productivity"

    def button_block(self):
        self.ensure_one()
        self.workcenter_id.order_ids.end_all()
        self.workcenter_id.order_ids.write({'state': 'block'})
