# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import UserError
from odoo.addons.stock.models.stock_move import PROCUREMENT_PRIORITIES
from datetime import date, datetime


class MrpMaterialRequest(models.Model):
    _name = "material.request"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _description = "Material Request"
    _rec_name = "name"
    _order = "create_date desc, id desc"

    name = fields.Char(
        string="Material Request",
        readonly=True,
        required=True,
        copy=False,
        default="New",
    )
    requested_by = fields.Many2one(
        "res.users",
        string="Created By",
        default=lambda self: self.env.user,
        tracking=True,
    )
    branch = fields.Many2one(
        "res.branch",
        string="Branch",
        default=lambda self: self.env.user.branch_id,
        tracking=True,
    )
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        tracking=True,
        default=lambda self: self.env.company,
    )
    destination_location = fields.Many2one(
        "stock.location",
        "Destination Location",
        check_company=True,
        required=True,
    )
    creation_date = fields.Datetime(
        "Create On",
        copy=False,
        default=lambda self: fields.Datetime.now(),
        index=True,
        readonly=True
    )
    create_uid = fields.Many2one(
        "res.users",
        string="Created By",
        default=lambda self: self.env.user,
        tracking=True,
        readonly=True
    )
    date_planned = fields.Datetime(
        "Scheduled Date",
        copy=False,
        default=lambda self: fields.Datetime.now(),
        help="Date at which you plan to start the production.",
        index=True,
        required=True,
        tracking=True,
    )
    date_expiry = fields.Datetime(
        "Expiry Date",
        copy=False,
        index=True,
        tracking=True,
    )
    description = fields.Text(string="Description")
    origin = fields.Char(string="Source Documant")

    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("approval", "Wating for Approval"),
            ("approved", "Approve"),
            ("reject", "Reject "),
            ("confirm", "Confirmed"),
            ("cancel", "Cancelled"),
            ("lock", "Done"),
        ],
        default="draft",
        tracking=True,
    )

    @api.model
    def create(self, vals):
        if vals.get("name", "New") == "New":
            vals["name"] = (
                self.env["ir.sequence"].next_by_code(
                    "material.request") or "New"
            )
        result = super(MrpMaterialRequest, self).create(vals)
        return result

    mo_id = fields.Many2one("mrp.production", string="Manufacturing Order")
    wo_id = fields.Many2one("mrp.workorder", string="Work Order")
    plan_id = fields.Many2one("mrp.plan", string="Manufacturing Plan")
    material_request_line_ids = fields.One2many(
        "material.request.line", "material_request_id", string="Products"
    )


class MrpMaterialRequestLine(models.Model):
    _name = "material.request.line"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _description = "Material Request Line"
    _rec_name = "material_request_id"
    _order = "create_date desc, id desc"

    product_id = fields.Many2one(
        "product.product",
        string="Product",
        change_default=True,
    )
    description = fields.Text(string="Description")
    qty = fields.Float(string="Quantity", default=1.0)
    qty_done = fields.Float(string="Done Quantity", default=1.0)
    uom_id = fields.Many2one("uom.uom", string="Unit Of Measure")
    destination_location = fields.Many2one(
        "stock.location", "Destination Location", check_company=True
    )
    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("approval", "Waiting for Approval"),
            ("approved", "Approve"),
            ("reject", "Reject "),
            ("confirm", "Confirmed"),
            ("cancel", "Cancelled"),
            ("lock", "Done"),
        ],
        default="draft",
        tracking=True,
    )
    material_request_id = fields.Many2one(
        "material.request", string="Material Request")
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        tracking=True,
        default=lambda self: self.env.company,
    )
