# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, date, timedelta
import pytz


class MrpProductionInherit(models.Model):
    _name = 'mrp.production'
    _inherit = ['mrp.production', 'mrp.material.request']

    # OVERRIDE METHODS & FIELDS
    # ==================================================================================================================
    def unlink(self):
        for plan in self:
            if plan.mrp_plan_id:
                for comp in plan.mrp_plan_id.mo_stock_move_ids.filtered('mrp_plan_id'):
                    if plan.id and comp.raw_material_production_id:
                        if plan.id == comp.raw_material_production_id.id:
                            comp.sudo().unlink()
        res = super(MrpProductionInherit, self).unlink()
        return res

    create_uid = fields.Many2one('res.users', string='Created By', default=lambda self: self.env.user, tracking=True)
    product_id = fields.Many2one(domain="[('bom_ids', '!=', False), ('bom_ids.branch', '=', branch)]", tracking=True)

    state = fields.Selection([
        ('draft', 'Draft'),
        ('approval', 'To be Approved '),
        ('approved', 'Approved'),
        ('reject', 'Rejected'),
        ('confirmed', 'Confirmed'),
        ('progress', 'In Progress'),
        ('to_close', 'To Close'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')],
        string='State',
        compute='_compute_state',
        copy=False,
        index=True,
        readonly=True,
        store=True,
        tracking=True,
        help=" * Draft: The MO is not confirmed yet.\n"
             " * Confirmed: The MO is confirmed, the stock rules and the reordering of the components are trigerred.\n"
             " * Approval: The MO is To be Approved.\n"
             " * Approved: The MO is Approved.\n"
             " * Reject: The MO is To be Rejected.\n"
             " * In Progress: The production has started (on the MO or on the WO).\n"
             " * To Close: The production is done, the MO has to be closed.\n"
             " * Done: The MO is closed, the stock moves are posted. \n"
             " * Cancelled: The MO has been cancelled, can't be confirmed anymore.")

    def _create_workorder(self):
        super(MrpProductionInherit, self)._create_workorder()
        for production in self:
            for workorder in production.workorder_ids:
                workcenter_id, location_id = self._get_workcenter(workorder.operation_id)
                workorder.workcenter_id = workcenter_id.id
                workorder.location_id = location_id.id

    def _get_move_raw_values(self, product_id, product_uom_qty, product_uom, operation_id=False, bom_line=False):
        res = super(MrpProductionInherit, self)._get_move_raw_values(product_id, product_uom_qty, product_uom,
                                                                     operation_id, bom_line)
        operation = self.env['mrp.routing.workcenter'].browse(operation_id)
        _, location_id = self._get_workcenter(operation)
        res['location_id'] = location_id.id
        return res

    def _get_move_finished_values(self, product_id, product_uom_qty, product_uom, operation_id=False,
                                  byproduct_id=False):
        res = super(MrpProductionInherit, self)._get_move_finished_values(product_id, product_uom_qty, product_uom,
                                                                          operation_id, byproduct_id)
        if byproduct_id:
            operation = self.env['mrp.routing.workcenter'].browse(operation_id)
            _, location_id = self._get_workcenter(operation)
            res['production_byproduct_loc_id'] = location_id.id
        return res

    @api.onchange('location_src_id', 'move_raw_ids', 'bom_id')
    def _onchange_location(self):
        pass

    def action_confirm(self):
        res = super(MrpProductionInherit, self).action_confirm()
        stock_move = self.env['stock.move'].sudo()
        for production in self:
            for workorder in production.workorder_ids:
                component_line = stock_move.search([
                    ('operation_id', '=', workorder.operation_id.id),
                    ('id', 'in', workorder.production_id.move_raw_ids.ids)
                ])
                by_product_line = stock_move.search([
                    ('operation_id', '=', workorder.operation_id.id),
                    ('id', 'in', workorder.production_id.move_byproduct_ids.ids)
                ])
                workorder.move_raw_ids = [(6, 0, component_line.ids)]
                workorder.byproduct_ids = [(6, 0, by_product_line.ids)]
            production._plan_workorders()

            line_ids = production.action_assign(return_action=False, raise_error=False)
            if line_ids is not True:
                wizard = self.env['mrp.plan.reserve.material.wizard'].create({'line_ids': line_ids})
                wizard.confirm(raise_error=False)
        return res

    def button_plan(self):
        res = super(MrpProductionInherit, self).button_plan()
        if self.mrp_plan_id:
            self.mrp_plan_id._recompute_scheduled_date()
        return res

    def button_unplan(self):
        res = super(MrpProductionInherit, self).button_unplan()
        now = fields.Datetime.now()
        self.date_planned_start = now
        self.date_planned_finished = now + timedelta(hours=1)
        if self.mrp_plan_id:
            self.mrp_plan_id._recompute_scheduled_date()
        return res

    def _plan_workorders(self, replan=False):
        self.ensure_one()
        if not self.workorder_ids:
            return
        for workorder in self.workorder_ids:
            workorder.workcenter_id.resource_id.tz = self.env.user.tz
            workorder.workcenter_id.resource_calendar_id.tz = self.env.user.tz
        if self.child_ids:
            self.date_planned_start = max(self.child_ids.mapped('date_planned_finished'))
        return super(MrpProductionInherit, self)._plan_workorders(replan=replan)

    def action_assign(self, return_action=True, raise_error=True):
        for production in self:
            if not self.env.company.mrp_production_partial_availability:
                product_message = ''
                count = 1
                for line in production.move_raw_ids:
                    if line.forecast_qty <= 0 or line.forecast_qty < line.product_uom_qty:
                        if line.product_id and line.location_id:
                            if line.product_id and line.location_id:
                                if product_message == '':
                                    product_message = str(count) + _('. "%s" on location "%s"') % (
                                        line.product_id.display_name, line.location_id.display_name)
                                else:
                                    product_message = product_message + '\r\n' + str(count) + _(
                                        '. "%s" on location "%s"') % (
                                                          line.product_id.display_name, line.location_id.display_name)
                                count = count + 1
                if product_message != '':
                    if raise_error:
                        raise UserError(_('There is not enough stock for: \r\n%s') % product_message)
                production.move_raw_ids._action_assign()
                return True
            else:
                view_id = self.env.ref('equip3_manuf_operations.manu_plan_reserve_material_wiz_form').id
                line_ids = []
                for line in self.move_raw_ids.filtered(lambda move: move.state not in ("done", "cancelled")):
                    line_ids.append((0, 0, {
                        'product_id': line.product_id and line.product_id.id or False,
                        'product_qty': line.product_uom_qty,
                        'product_reserve_qty': line.product_uom_qty,
                        'move_id': line.id,
                        'production_id': line.raw_material_production_id and line.raw_material_production_id.id or False,
                        'workorder_id': line.workorder_id and line.workorder_id.id or False
                    }))

                if return_action:
                    return {
                        'type': 'ir.actions.act_window',
                        'name': _('Reserve Material'),
                        'res_model': 'mrp.plan.reserve.material.wizard',
                        'target': 'new',
                        'view_mode': 'form',
                        'views': [[view_id, 'form']],
                        'context': {
                            'default_line_ids': line_ids
                        }
                    }
                return line_ids
        return True

    # NEW ADDED METHODS & FIELDS
    # ==================================================================================================================
    @api.model
    def _get_default_location_reject_id(self):
        return self._get_default_location_dest_id()

    @api.model
    def _get_default_analytic_tag_ids(self):
        user = self.env.user
        analytic_priority = self.env['analytic.priority'].sudo().search([], limit=1, order='priority')
        analytic_tag_ids = []
        if analytic_priority.object_id == 'user' and user.analytic_tag_ids:
            analytic_tag_ids = user.analytic_tag_ids.ids
        elif analytic_priority.object_id == 'branch' and user.branch_id and user.branch_id.analytic_tag_ids:
            analytic_tag_ids = user.branch_id.analytic_tag_ids.ids
        elif analytic_priority.object_id == 'product_category':
            product_category = self.env['product.category'].sudo().search([('analytic_tag_ids', '!=', False)], limit=1)
            analytic_tag_ids = product_category.analytic_tag_ids.ids
        return [(6, 0, analytic_tag_ids)]

    @api.depends('qc_fail', 'qc_pass', 'pending_qc', 'need_qc')
    def compute_qc_state(self):
        self.filtered(lambda s: s.qc_fail).qc_state = 'failed'
        self.filtered(lambda s: s.qc_pass).qc_state = 'passed'
        self.filtered(lambda s: not s.qc_fail and not s.qc_pass).qc_state = 'pending'

    @api.depends('company_id')
    def _compute_allowed_branch(self):
        user_branch = self.env.user.branch_ids
        for record in self:
            allowed_branch = user_branch.filtered(lambda b: b.company_id == record.company_id)
            record.allowed_branch_ids = [(6, 0, allowed_branch.ids)]

    qc_state = fields.Selection(
        selection=[
            ('pending', 'Pending'),
            ('failed', 'Failed'),
            ('passed', 'Passed')],
        string='QC State',
        compute=compute_qc_state,
        store=True)

    allowed_branch_ids = fields.One2many('res.branch', compute=_compute_allowed_branch)
    branch = fields.Many2one(
        'res.branch',
        string='Branch',
        default=lambda self: self.env.user.branch_id,
        domain="[('id', 'in', allowed_branch_ids)]",
        readonly=True,
        states={'draft': [('readonly', False)]},
        tracking=True)

    mrp_plan_id = fields.Many2one('mrp.plan', string='Manufacturing Plan', tracking=True)
    mrp_plan_state = fields.Selection(related='mrp_plan_id.state')
    sale_order_id = fields.Many2one('sale.order', string='Sale Order', readonly=True, copy=False)
    rejected_location_dest_id = fields.Many2one(
        'stock.location',
        string='Rejected Products Location',
        default=_get_default_location_reject_id,
        readonly=True, required=True,
        domain="[('usage','=','internal'), '|', ('company_id', '=', False), ('company_id', '=', company_id)]",
        states={'draft': [('readonly', False)]},
        check_company=True,
        help="Location where the system will stock the rejected products.")

    additional_mo = fields.Boolean(string='Additional', default=False)

    analytic_tag_ids = fields.Many2many(
        'account.analytic.tag',
        string='Analytical Group',
        domain="[('company_id', '=', company_id)]",
        default=_get_default_analytic_tag_ids)

    parent_id = fields.Many2one('mrp.production', string='Production Ancestor')
    child_ids = fields.One2many('mrp.production', 'parent_id', string='Ancestor To')

    @api.model
    def change_mo_seq(self):
        company_ids = self.env['res.company'].sudo().search([])
        picking_type = self.env['stock.picking.type'].sudo()
        for company_id in company_ids:
            picking_type_id = picking_type.search([
                ('code', '=', 'mrp_operation'),
                ('warehouse_id.company_id', '=', company_id.id)
            ], limit=1)
            if not picking_type_id or not picking_type_id.sequence_id:
                continue
            sequence_id = picking_type_id.sequence_id
            sequence_id.write({
                'prefix': 'MOR/%(y)s/%(month)s/%(day)s/',
                'padding': 3,
                'use_date_range': True
            })
            today = fields.Date.today()
            date_range_ids = sequence_id.date_range_ids
            if not date_range_ids:
                date_range_ids = [sequence_id._create_date_range_seq(today)]
            for date_range in date_range_ids:
                if date_range.date_from <= today <= date_range.date_to:
                    next_number = date_range.number_next_actual
                    while True:
                        mo_name = sequence_id.get_next_char(next_number)
                        production_id = self.search([('name', '=', mo_name)])
                        if not production_id:
                            break
                        next_number += 1
                    date_range.number_next_actual = next_number

    @api.onchange('bom_id', 'state')
    def onchange_product_id_workcenter(self):
        for operation in self.bom_id.operation_ids:
            for work in self.workorder_ids:
                if operation.work_center == 'with_group':
                    if operation.name == work.name:
                        for i in operation.work_center_group.mrp_work_center_group_ids:
                            work.write({
                                'work_center_group_line_id': [(4, i.work_center.id)],
                            })
                if operation.work_center == 'without_group':
                    if operation.name == work.name:
                        work_center = self.env['mrp.workcenter'].search([])
                        for workcenter in work_center:
                            work.write({
                                'work_center_group_line_id': [(4, workcenter.id)]
                            })

    @api.onchange('workorder_ids')
    def onchange_workorder_ids(self):
        if self.workorder_ids:
            try:
                last_workorder = self.workorder_ids.sorted(key=lambda x: x.id)[-1]
            except TypeError:
                last_workorder = self.workorder_ids.sorted(key=lambda x: x._origin.id)[-1]

            self.location_dest_id = last_workorder.workcenter_id.finished_good_loc.id
            self.rejected_location_dest_id = last_workorder.workcenter_id.rejected_good_loc.id

    def _get_workcenter(self, operation):

        workcenter_id = self.env['mrp.workcenter']
        location_id = self.env['stock.location']

        if operation.work_center == 'without_group':
            workcenter_id = operation.workcenter_id
            location_id = operation.workcenter_id.wc_location

        elif operation.work_center == 'with_group':
            workorder_ids = self.env['mrp.workorder']
            for workcenter in operation.work_center_group.mrp_work_center_group_ids:
                workorder_id = self.env['mrp.workorder'].search([
                    ('workcenter_id', '=', workcenter.work_center.id),
                    ('date_planned_finished', '!=', False)
                ], order='date_planned_finished desc', limit=1)
                workorder_ids |= workorder_id

            if workorder_ids:
                workcenter_id = sorted(workorder_ids, key=lambda w: w.date_planned_finished)[0].workcenter_id
                location_id = workcenter_id.wc_location

        return workcenter_id, location_id

    # If field wc_location in work center is blank then show a warning message
    @api.constrains("workorder_ids")
    def check_workcenter_location(self):
        for rec in self:
            for line in rec.workorder_ids:
                for i in rec.bom_id.operation_ids:
                    if i.work_center == 'without_group':
                        if i.name == line.name and not line.workcenter_id.wc_location:
                            raise ValidationError(
                                _('WARNING \nThe work center location of product %s is blank. \nPlease enter the location in the work center to continue.' % (
                                    rec.product_id.name)))

    # If field operation_id in BoM is blank then show warning message
    @api.constrains("bom_id")
    def check_bom_op(self):
        for rec in self:
            for line in rec.bom_id.bom_line_ids:
                if not line.operation_id:
                    raise ValidationError(
                        _('WARNING \nThe field consumed in operation of product %s is blank. \nPlease set the consumed in operation field in Bills of Materials to continue.' % (
                            rec.bom_id.product_tmpl_id.name)))

    @api.onchange("bom_id")
    def onchange_check_bom_op(self):
        for rec in self:
            for line in rec.bom_id.bom_line_ids:
                if not line.operation_id:
                    raise ValidationError(
                        _('WARNING \nThe field consumed in operation of product %s is blank. \nPlease set the consumed in operation field in Bills of Materials to continue.' % (
                            rec.bom_id.product_tmpl_id.name)))

    # if field location_id in component is blank then show warning message
    @api.constrains("move_raw_ids")
    def check_component_location(self):
        for rec in self:
            for line in rec.move_raw_ids:
                if not line.location_id:
                    raise ValidationError(
                        _('WARNING \n The work center location of product %s is blank. \nPlease enter the location in the work center to continue.' % (
                            rec.product_id.name)))

    @api.onchange('move_raw_ids')
    def onchange_move_raw_ids(self):
        for rec in self:
            for line in rec.workorder_ids:
                # raise ValidationError(_(line.workcenter_id.wc_location))
                if not line.workcenter_id.wc_location:
                    raise ValidationError(
                        _('WARNING \n The work center location of product %s is blank. \nPlease enter the location in the work center to continue.' % (
                            rec.product_id.name)))

    # APPROVAL MATRIX
    # ==================================================================================================================
    @api.model
    def _get_default_is_matrix_on(self):
        return self.env.company.manufacturing_order_conf

    @api.model
    def _get_default_approval_matrix(self, is_matrix_on=None, company=None, branch=None):
        if is_matrix_on is None:
            is_matrix_on = self._get_default_is_matrix_on()

        if is_matrix_on is False:
            return False

        if company is None:
            company = self.env.company
        if branch is None:
            branch = self.env.user.branch_id

        approval_matrix = self.env['mrp.approval.matrix'].sudo().search([
            ('company', '=', company.id),
            ('branch', '=', branch.id),
            ('matrix_type', '=', 'mo')
        ], limit=1)
        return approval_matrix.id

    @api.depends('approval_matrix')
    def _compute_approval_matrix_lines(self):
        for rec in self:
            lines = []
            for app_matrix in rec.approval_matrix.mrp_approval_matrix_ids:
                lines.append((0, 0, {
                    'mo_approval_matrix_id': rec.approval_matrix.id,
                    'sequence': app_matrix.sequence,
                    'approve': [(6, 0, app_matrix.approve.ids)],
                    'minimum_approver': app_matrix.minimum_approver,
                }))
            rec.mo_approval_matrix_ids = lines

    def _compute_user_is_approver(self):
        for rec in self:
            b = False
            next_approved = 0
            for line in rec.mo_approval_matrix_ids:
                if line.state == 'approved':
                    next_approved += 1
                if line.state == 'rejected':
                    next_approved += 1

            if len(rec.mo_approval_matrix_ids) != next_approved:
                if self.env.user.id in rec.mo_approval_matrix_ids[next_approved].approve.ids:
                    b = True
                if self.env.user.id in rec.mo_approval_matrix_ids.approve_by.ids:
                    b = False
            rec.user_is_approver = b

    is_matrix_on = fields.Boolean(default=_get_default_is_matrix_on)
    user_is_approver = fields.Boolean(compute=_compute_user_is_approver)
    approval_matrix = fields.Many2one('mrp.approval.matrix', 'Approval Matrix', default=_get_default_approval_matrix)
    mo_approval_matrix_ids = fields.One2many(
        'mo.approval.matrix.line',
        'mo_approval_matrix_id',
        string='Approval Lines',
        compute=_compute_approval_matrix_lines,
        store=True)
    state_no_approval_matrix = fields.Selection(related='state')

    @api.onchange('is_matrix_on', 'company_id', 'branch')
    def onchange_branch(self):
        self.approval_matrix = self._get_default_approval_matrix(self.is_matrix_on, self.company_id, self.branch)

    def check_approve_reject_state(self):
        for rec in self:
            approve_state = len(
                [line_approve for line_approve in rec.mo_approval_matrix_ids if line_approve.state == 'approved'])
            rejected_state = len(
                [line_reject for line_reject in rec.mo_approval_matrix_ids if line_reject.state == 'rejected'])
            if all([line.state for line in rec.mo_approval_matrix_ids]):
                if approve_state >= 2:
                    if approve_state >= rejected_state:
                        rec.write({'state': 'approved'})
                    else:
                        rec.write({'state': 'reject'})
                else:
                    if rejected_state < 1:
                        rec.write({'state': 'approved'})
                    else:
                        rec.write({'state': 'reject'})

    def action_approval(self):
        for rec in self:
            for line in rec.mo_approval_matrix_ids:
                # if self.env.user.id in line.approve.ids:
                line.requested_time = fields.Datetime.now()
        self.write({'state': 'approval'})

    def action_approve(self):
        tz = pytz.timezone(self.env.user.tz) or 'UTC'
        current_time = datetime.now(tz)
        for rec in self:
            for line in rec.mo_approval_matrix_ids:
                if self.env.user.id in line.approve.ids:
                    line.approve_by = [(4, self.env.user.id)]
                    if line.approval_matrix_status and line.approval_matrix_status != '':
                        line.approval_matrix_status = line.approval_matrix_status + '\r\nApproved By: ' + \
                                                      str(self.env.user.name) + ', ' + \
                                                      current_time.strftime('%m/%d/%Y %H:%M:%S')
                    else:
                        line.approval_matrix_status = 'Approved By: ' + str(
                            self.env.user.name) + ', ' + current_time.strftime('%m/%d/%Y %H:%M:%S')
                    if line.minimum_approver < 2:
                        line.state = 'approved'
                    if line.approve_by == line.approve:
                        line.state = 'approved'
                # line.approved_time = fields.Datetime.now()
            if all([line.state == 'approved' for line in rec.mo_approval_matrix_ids]):
                rec.write({'state': 'approved'})
            # if line.minimum_approver >= 2 :
            #     if len(line.approve_by) >= line.minimum_approver:
            #         rec.write({'state': 'approved'})
            self.check_approve_reject_state()

    def action_reject(self, reject_reason=''):
        tz = pytz.timezone(self.env.user.tz) or 'UTC'
        current_time = datetime.now(tz)
        for rec in self:
            for line in rec.mo_approval_matrix_ids:
                reject_msg = ''
                if self.env.user.id in line.approve.ids:
                    line.state = 'rejected'
                    if line.approval_matrix_status and line.approval_matrix_status != '':
                        reject_msg = line.approval_matrix_status + '\r\nRejected By: ' + str(
                            self.env.user.name) + ', ' + current_time.strftime('%m/%d/%Y %H:%M:%S')
                    else:
                        reject_msg = 'Rejected By: ' + str(
                            self.env.user.name) + ', ' + current_time.strftime('%m/%d/%Y %H:%M:%S')

                    if reject_reason != '':
                        reject_msg = reject_msg + ' Reason: ' + reject_reason
                    line.approval_matrix_status = reject_msg
            if all([line.state == 'rejected' for line in rec.mo_approval_matrix_ids if not line.minimum_approver >= 1]):
                rec.write({'state': 'reject'})
            self.check_approve_reject_state()

    def action_reject_wiz(self):
        view_id = self.env.ref('equip3_manuf_operations.manu_order_reject_reason_wiz').id
        return {
            'type': 'ir.actions.act_window',
            'name': _('Reject Reason'),
            'res_model': 'mo.matrix.reject.reason.wizard',
            'target': 'new',
            'view_mode': 'form',
            'views': [[view_id, 'form']],
        }

    # ASSIGNED LABORS
    # ==================================================================================================================
    @api.depends(
        'labor_type', 'labor_workorder_id', 'labor_group_id',
        'labor_group_id.labor_ids', 'labor_ids')
    def _compute_labor_hide_assign_button(self):
        for record in self:
            err_message = record.check_action_assign_labor()
            record.labor_hide_assign_button = err_message is not False

    labor_type = fields.Selection(
        selection=[
            ('with_group', 'With Group'),
            ('without_group', 'Without Group')
        ],
        string='Labor Type',
        default='with_group',
        required=True,
        tracking=True)
    labor_workorder_id = fields.Many2one(
        'mrp.workorder',
        string='Labor Work Order',
        domain="[('id', 'in', workorder_ids), ('state', 'not in', ('done', 'cancel'))]",
        tracking=True)
    labor_group_id = fields.Many2one('mrp.labor.group', string='Labor Group', tracking=True)
    labor_ids = fields.Many2many('hr.employee', string='Labor', domain="[('active', '=', True)]")
    labor_hide_assign_button = fields.Boolean(compute=_compute_labor_hide_assign_button)
    assign_ids = fields.One2many('mrp.labor.assign', 'production_id', string='Assigned Labors')

    def reset_labor_form(self):
        self.ensure_one()
        return self.write({
            'labor_type': 'with_group',
            'labor_workorder_id': False,
            'labor_group_id': False,
            'labor_ids': [(5,)]
        })

    def check_action_assign_labor(self):
        self.ensure_one()
        err_message = False
        if not self.labor_workorder_id:
            err_message = _('Please select Work Order to assign!')

        if self.labor_type == 'with_group':
            if not self.labor_group_id:
                err_message = _('Please select Labor Group to assign!')
            elif not self.labor_group_id.labor_ids:
                err_message = _('The selected labor group has no employees to assign!')
        else:
            if not self.labor_ids:
                err_message = _('Please select Labors to assign!')
        return err_message

    def action_assign_labor(self):
        assign_labor = self.env['mrp.labor.assign']
        for record in self:
            err_message = record.check_action_assign_labor()
            if err_message is not False:
                raise UserError(err_message)

            plan_id = False
            if record.mrp_plan_id:
                plan_id = record.mrp_plan_id.id

            labor_ids = record.labor_ids
            if record.labor_type == 'with_group':
                labor_ids = record.labor_group_id.labor_ids

            for labor in labor_ids:
                assign_labor.create({
                    'plan_id': plan_id,
                    'production_id': record.id,
                    'workorder_id': record.labor_workorder_id.id,
                    'labor_id': labor.id
                })
            record.reset_labor_form()

    def action_remove_labor(self):
        assign_to_delete = self.assign_ids.filtered(lambda a: a.workorder_id.state in ('pending', 'ready', 'cancel'))
        assign_to_delete.unlink()

    @api.depends('state', 'move_raw_ids.quantity_done', 'move_raw_ids.product_uom_qty')
    def _compute_show_transfer_back_material_button(self):
        for rec in self:
            yes = False
            if rec.state == 'done' or rec.state == 'to_close' or rec.state == 'cancel':
                for line in rec.move_raw_ids:
                    if line.product_uom_qty >= line.quantity_done:
                        yes = True
                    else:
                        yes = False
            rec.show_transfer_back_material_button = yes

    show_transfer_back_material_button = fields.Boolean(compute='_compute_show_transfer_back_material_button',
                                                        string='Show Transfer Back Material Button', default=False)

    def transfer_back_material(self):
        lines = []
        for line in self.move_raw_ids:
            if line.product_uom_qty > line.quantity_done:
                line_data = {
                    'name': self.name,
                    'product_id': line.product_id.id,
                    'product_uom_qty': line.product_uom_qty - line.quantity_done,
                    'product_uom': line.product_uom,
                    'location_id': line.location_id.id,
                }
                lines.append((0, 0, line_data))

        picking_id = self.env['stock.picking'].create({
            "picking_type_id": self.env['stock.picking.type'].search([('sequence_code', '=', 'MO')], limit=1).id,
            "location_id": self.location_dest_id.id,
            "location_dest_id": self.env['stock.location'].search([], limit=1).id,
            "move_ids_without_package": lines,
            "origin": self.name
        })

        return {
            'name': 'Transfer Back Material',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'stock.picking',
            'view_id': self.env.ref('stock.view_picking_form').id,
            'res_id': picking_id.id,
            'type': 'ir.actions.act_window',
            'context': self.env.context,
            'target': 'new'
        }

    check_quantity = fields.Float(string='Check Quantity')
    qty_bool = fields.Boolean(compute="check_demand_quantity", store=True)

    def action_transfer_finished_goods(self):
        line_ids = []
        tags = []
        for i in self.analytic_tag_ids:
            tags.append(i.id)
        stock_picking = self.env['stock.picking'].search(
            [('origin', '=ilike', self.name), ('is_transfer_good', '=', True)], order='name desc')
        qty_produced = 0
        if stock_picking:
            b = 0
            for i in stock_picking:
                for a in i.move_ids_without_package:
                    b += a.product_uom_qty
                qty_produced = self.qty_produced - b
                self.check_quantity = b + qty_produced
        else:
            qty_produced = self.qty_produced
            self.check_quantity = qty_produced

        values = {
            'name': self.product_id.name,
            'product_id': self.product_id.id,
            'product_uom_qty': qty_produced,
            'product_uom': self.product_id.uom_id.id,
            'analytic_account_group_ids': tags
        }
        line_ids.append((0, 0, values))
        context = {
            'default_origin': self.name,
            'default_location_id': self.location_dest_id.id,
            'default_analytic_account_group_ids': tags,
            'default_picking_type_id': self.picking_type_id.id,
            'default_location_dest_id': self.picking_type_id.default_location_dest_id.id,
            'default_move_ids_without_package': line_ids,
            'picking_type_code': 'mrp_operation',
            'default_is_readonly_origin': True,
            'default_is_transfer_good': True,
        }
        action = {
            'name': 'Transfer Finished Goods',
            'type': 'ir.actions.act_window',
            'res_model': 'stock.picking',
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new',
            'context': context,
        }

        return action

    @api.depends('check_quantity', 'qty_produced')
    def check_demand_quantity(self):
        for rec in self:
            if rec.check_quantity:
                if rec.check_quantity >= rec.qty_produced:
                    rec.qty_bool = True
                else:
                    rec.qty_bool = False



class StockPicking(models.Model):
    _inherit = 'stock.picking'

    is_transfer_good = fields.Boolean(default=False)

    @api.onchange('location_id', 'location_dest_id')
    def onchange_location_id(self):
        super(StockPicking, self).onchange_location_id()
        force_manuf_picking_type = self.env.context.get('force_manuf_picking_type')
        if force_manuf_picking_type:
            self.picking_type_id = self.env.context.get('default_picking_type_id', False)
