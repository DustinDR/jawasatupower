# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.

from odoo import http,fields
from odoo.http import request
import json
import base64
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager
from odoo.tools import date_utils, groupby as groupbyelem

from odoo import fields, api, SUPERUSER_ID, _
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.osv.expression import AND
from operator import itemgetter
from collections import OrderedDict
# from odoo.addons.sh_rfq_portal.controllers.portal import CustomerPortal
from odoo.addons.sh_po_tender_portal.controllers.portal import TenderRFQPOrtal
from ...equip3_purchase_other_operation.controllers.portal import PurchaseRFQPortal
import base64
import csv
import io
import os
from io import BytesIO, TextIOWrapper
from odoo import models, fields, api, _
from odoo.exceptions import Warning
from datetime import datetime

class ChangeVendorPricelist(http.Controller):

    @http.route(['/vendor_pricelist/<int:quote_id>'], type='http', auth="public", website=True)
    def portal_my_vendor_pricelist_form(self, quote_id, report_type=None, access_token=None, message=False, download=False, **kw):
        quote_sudo = request.env['product.supplierinfo'].sudo().browse(quote_id)
        values = {
            'token': access_token,
            'vendor_pricelist': quote_sudo,
            'vp_id': True,
            'message': message,
            'bootstrap_formatting': True,
            'partner_id': quote_sudo.name.id,
            'report_type': 'html',
        }
        return request.render('equip3_purchase_vendor_portal.vendor_pricelist_form_view_new', values)

class CreateVendorPricelist(http.Controller):

    @http.route(['/vendor_pricelist'], type='http', auth="public", website=True)
    def create_vendor_pricelist(self, **post):
        quote_msg = {}
        emails = []
        image = 0
        multi_users_value = [0]
        contacts = []

        if post:
            changes = post.get('changes',False)
            changes_id = post.get('changes_id',False)
            vendor_product_name = post.get('vendor_product_name',False)
            vendor_product_code = post.get('vendor_product_code',False)
            delay = post.get('delivery_lead_time',False)
            qty = post.get('quantity',False)
            unit_price = post.get('unit_price',False)
            validity_start = post.get('validity_start',False)
            validity_end = post.get('validity_end',False)
            vendor_uom = post.get('vendor_uom', False)
            branch_id = post.get('branch_id',False)
            old_id = request.env['product.supplierinfo'].search([('id', '=', changes_id)])
            vendor_pric = {
                'product_name':vendor_product_name,
                'product_code':vendor_product_code,
                'delay':delay,
                'branch_id':branch_id,
                'vendor_uom': vendor_uom,
                'min_qty':qty,
                'price':unit_price,
                'date_start':validity_start,
                'date_end':validity_end,
                'changes_id': changes_id,
                'changes': changes,
                'product_id': old_id.product_id.id or False,
                'product_tmpl_id': old_id.product_tmpl_id.id or False,
                'product_uom': old_id.product_uom.id or False,
            }
            vendor_id = request.env['product.supplierinfo'].sudo().create(vendor_pric)
            if vendor_id:
                quote_msg = {
                    'success': 'Vendor Pricelist ' + vendor_product_name + ' created successfully.'
                }


        values = {
            'page_name': 'vendor_pricelist_form_page',
            'default_url': '/vendor_pricelist',
            'quote_msg':quote_msg,
        }
        return request.render("equip3_purchase_vendor_portal.vendor_pricelist_form_view",values)

class VendorPricelistPortal(CustomerPortal):

    def _prepare_portal_layout_values(self):

        values = super(VendorPricelistPortal, self)._prepare_portal_layout_values()
        vendor_pricelist_obj = request.env['product.supplierinfo']
        vendor_pricelist = vendor_pricelist_obj.sudo().search([('name', 'in', [request.env.user.partner_id.id])])
        vendor_pricelist_count = vendor_pricelist_obj.sudo().search_count([('name', 'in', [request.env.user.partner_id.id])])
        values['vendor_pricelist_count'] = vendor_pricelist_count
        values['vendor_pricelist'] = vendor_pricelist
        return values

    @http.route(['/my/vendor_pricelist/<int:quote_id>'], type='http', auth="user", website=True)
    def portal_my_vendor_pricelist_form(self, quote_id, report_type=None, access_token=None, message=False, download=False, **kw):
        quote_sudo = request.env['product.supplierinfo'].sudo().browse(quote_id)
        values = {
            'token': access_token,
            'vendor_pricelist': quote_sudo,
            'vp_id': True,
            'message': message,
            'bootstrap_formatting': True,
            'partner_id': quote_sudo.name.id,
            'report_type': 'html',
        }
        return request.render('equip3_purchase_vendor_portal.portal_vendor_pricelist_form_template', values)

    def vendor_pricelist_import(self, data):
        for line in data:
            product_name = line[0]
            product_id = request.env['product.product'].search([('name', 'ilike', product_name)], limit=1)
            product_uom = line[5]
            product_uom_id = request.env['uom.uom'].search([('name', 'ilike', product_uom)], limit=1)
            vailidity = line[6]
            vailidity_date = vailidity.split(",")
            branch_ids = line[7]
            branch_id = request.env['res.branch'].search([('name', 'ilike', branch_ids)], limit=1)              
            vals = {
                'product_id': product_id.id, 
                'product_name': product_name,  
                'product_tmpl_id': product_id.product_tmpl_id.id,
                'product_code': line[1],
                'delay': line[2],
                'min_qty': line[3],
                'price': line[4],
                'product_uom_new': product_uom_id.id,
                'date_start': datetime.strptime(vailidity_date[0], '%m/%d/%Y').date(),
                'date_end': datetime.strptime(vailidity_date[1], '%m/%d/%Y').date(),
                'branch_id': branch_id.id,
            }
            pricelist_id = request.env['product.supplierinfo'].create(vals)

    @http.route(['/my/vendor_pricelist_import'], type='json', auth="user", website=True)
    def portal_vendor_pricelist_import_form(self, file, file_name, **kw):
        fileformat = os.path.splitext(file_name)[1]
        data = []
        if fileformat == ".csv":
            decoded_datas = base64.b64decode(file)
            file = TextIOWrapper(BytesIO(decoded_datas))
            sniffer = csv.Sniffer()
            sniffer.preferred = [';',',']
            dialect = sniffer.sniff(file.read())
            file.seek(0)
            csvreader = csv.reader(file)
            fields = next(csvreader)
            for row in csvreader:
                data.append(row)
            self.vendor_pricelist_import(data)
        elif fileformat == ".xls":
            xlDecoded = base64.b64decode(file)
            workbook = open_workbook(file_contents=xlDecoded)
            data = []
            for sheet in workbook.sheets():
                for count in range(1, sheet.nrows):
                    line = sheet.row_values(count)
                    data.append(line)
            self.vendor_pricelist_import(data)
        else:
            return {'message':"Please select xls or csv file and try again!" }

    @http.route(['/my/vendor_pricelist', '/my/vendor_pricelist/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_home_vendor_pricelist(self, page=1, step=20, sortby=None, filterby=None, search=None, search_in='all', groupby='none', **kw):
        values = self._prepare_portal_layout_values()

        searchbar_sortings = {
            'product_name': {'label': _('Product Name A-Z'), 'order': 'product_name asc'},
            'product_name1': {'label': _('Product Name Z-A'), 'order': 'product_name desc'},
            'product_code': {'label': _('Product Code A-Z'), 'order': 'product_code asc'},
            'product_code1': {'label': _('Product Code Z-A'), 'order': 'product_code desc'},
            'delay': {'label': _('Delivery Lead Time'), 'order': 'delay asc'},
            'date_end': {'label': _('Expiry Date'), 'order': 'date_end asc'},
            'state1': {'label': _('Status'), 'order': 'state asc'},
            'date': {'label': _('Date'), 'order':'create_date desc'},
        }
        searchbar_inputs = {
            'all': {'input': 'all', 'label': _('Search in All')},
        }

        searchbar_filters = {
            'all': {'label': _('All'), 'domain': []},
            'draft': {'label': _('Draft'), 'domain': [("state1", "=", "draft")]},
            'waiting': {'label': _('Waiting for Approval'), 'domain': [("state1", "=", "waiting_approval")]},
            'approved': {'label': _('Approved'), 'domain': [("state1", "=", "approved")]},
            'rejected': {'label': _('Rejected'), 'domain': [("state1", "=", "rejected")]},
            'expired': {'label': _('Expired'), 'domain': [("state1", "=", "expire")]},
        }

        domain = [('name', '=', request.env.user.partner_id.id)]

        if not sortby:
            sortby = 'product_name'
        order = searchbar_sortings[sortby]['order']

        if not filterby:
            filterby = 'all'
        domain += searchbar_filters[filterby]['domain']

        vendor_pricelist_obj = request.env['product.supplierinfo']
        vendor_pricelist_count = vendor_pricelist_obj.sudo().search_count(domain)

        pager = portal_pager(
            url="/my/vendor_pricelist",
            url_args={'sortby': sortby, 'search_in': search_in,
                        'search': search, 'filterby': filterby},
            total=vendor_pricelist_count,
            page=page,
            step=step,
        )

        vendor_pricelist = vendor_pricelist_obj.search(domain, order=order, limit=self._items_per_page, offset=pager['offset'])

        values.update({
            'vendor_pricelist': vendor_pricelist,
            'page_name': 'vendor_pricelist',
            'pager': pager,
            'default_url': '/my/vendor_pricelist',
            'searchbar_sortings': searchbar_sortings,
            'sortby': sortby,
            'searchbar_filters': searchbar_filters,
            'filterby':filterby,
        })
        return request.render("equip3_purchase_vendor_portal.portal_my_vendor_pricelist", values)

class CustomPurchaseRFQPOrtal(TenderRFQPOrtal):

    @http.route(['/rfq/update'], type='http', auth="user", website=True, csrf=False)
    def custom_rfq_update(self, access_token=None, **kw):
        print (">>>>>>>", kw)
        if kw.get('order_id'):
            purchase_order = request.env['purchase.order'].sudo().search(
                [('id', '=', int(kw.get('order_id')))], limit=1)
            if purchase_order and purchase_order.agreement_id.state != 'closed':
                if purchase_order.order_line:
                    for k, v in kw.items():
                        if k != 'order_id' and '_' not in k and k!='rfq_note' and 'qty' not in k and k.isdigit():
                            purchase_order_line = request.env['purchase.order.line'].sudo().search(
                                [('order_id', '=', purchase_order.id), ('id', '=', k)], limit=1)
                            if purchase_order_line:
                                price = v
                                price_bef = purchase_order_line.price_unit
                                if ',' in price:
                                    price = price.replace(",", ".")
                                purchase_order_line.sudo().write({
                                    'price_unit':float(price),
                                })
                                # if str(price_bef) != price:
                                #     message = "Unit Price: %s becomes %s" % (price_bef, price)
                                #     purchase_order.message_post(body=message, subtype_xmlid="mail.mt_comment", author_id=request.env.user.partner_id.id, type="comment")
                        if k != 'order_id' and '_' in k and k!='rfq_note' and 'qty' not in k:
                            notes = True
                            order_line = k.split("_note")
                            if "_" in order_line[0]:
                                order_line = order_line[0].split("_qty")
                                notes = False
                            purchase_order_line = request.env['purchase.order.line'].sudo().search(
                                [('order_id', '=', purchase_order.id), ('id', '=', int(order_line[0]))], limit=1)
                            if purchase_order_line:
                                if notes:
                                    note = v
                                    note_bef = purchase_order_line.sh_tender_note
                                    purchase_order_line.sudo().write({
                                        'sh_tender_note':note,
                                    })
                                    # if str(note_bef) != notes:
                                    #     message = "Notes: %s becomes %s" % (note_bef, notes)
                                    #     purchase_order.message_post(body=message, subtype_xmlid="mail.mt_comment", author_id=request.env.user.partner_id.id, type="comment")
                        if 'qty' in k:
                            qty = v
                            qty_bef = purchase_order_line.product_qty
                            purchase_order_line.sudo().write({
                                'product_qty':qty,
                            })
                                    # if str(qty_bef) != qty:
                                    #     message = "Quantity: %s becomes %s" % (qty_bef, qty)
                                    #     purchase_order.message_post(body=message, subtype_xmlid="mail.mt_comment", author_id=request.env.user.partner_id.id, type="comment")

                        if k == 'rfq_note':
                            purchase_order.sudo().write({
                                'term_condition_box': kw.get(k),
                            })
        url = '/my/rfq/update/'+str(kw.get('order_id'))
        return request.redirect(url)

class PurchaseRFQPortal(PurchaseRFQPortal):

    @http.route(['/my/rfq/<int:quote_id>'], type='http', auth="user", website=True)
    def portal_my_rfq_form(self, quote_id, report_type=None, access_token=None, message=False, download=False, **kw):
        quote_sudo = request.env['purchase.order'].sudo().browse(quote_id)
        if report_type in ('html', 'pdf', 'text'):
            return self._show_report(model=quote_sudo, report_type=report_type, report_ref='purchase.report_purchase_quotation', download=download)
        values = {
            'token': access_token,
            'quotes': quote_sudo,
            'message': message,
            'bootstrap_formatting': True,
            'partner_id': quote_sudo.partner_id.id,
            'report_type': 'html',
        }
        if quote_sudo.not_editable and quote_sudo.is_editable:
            return request.render('equip3_purchase_vendor_portal.purchase_tender_portal_my_rfq_order_update', values)
        else:
            return request.render('equip3_purchase_vendor_portal.portal_rfq_form_templates', values)


    @http.route(['/my/rfq/update/<int:quote_id>'], type='http', auth="user", website=True)
    def portal_my_rfqs_update(self, quote_id=None, report_type=None, access_token=None, message=False, download=False, **kw):
        quote_sudo = request.env['purchase.order'].sudo().browse(quote_id)
        if report_type in ('html', 'pdf', 'text'):
            return self._show_report(model=quote_sudo, report_type=report_type, report_ref='purchase.report_purchase_quotation', download=download)
        values = {
            'token': access_token,
            'quotes': quote_sudo,
            'message': message,
            'bootstrap_formatting': True,
            'partner_id': quote_sudo.partner_id.id,
            'report_type': 'html',
        }
        return request.render("equip3_purchase_vendor_portal.sh_portal_my_rfq_order_update", values)