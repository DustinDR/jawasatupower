# -*- coding: utf-8 -*-
{
    'name': "equip3_purchase_vendor_portal",

    'summary': """
        Vendor Pricelist - Portal""",

    'description': """
        Vendor Pricelist - Portal
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Purchase/Purchase',
    'version': '1.1.10',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'website',
        'portal',
        'purchase',
        'sh_vendor_signup',
        'sh_rfq_portal',
        'equip3_purchase_other_operation',
        'equip3_purchase_operation',
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/vendor_pricelist_portal_template_view.xml',
        'views/purchase_portal_template_view.xml',
        'views/assets.xml',
        "data/vendor_pricelist_menu.xml",
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}