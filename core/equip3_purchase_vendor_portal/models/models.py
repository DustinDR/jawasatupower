# -*- encoding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api
from odoo.http import request


class SupplierInfo(models.Model):
    _inherit = 'product.supplierinfo'

    @api.model
    def create(self, vals):
        if 'name' not in vals:
            if self.env.user.partner_id.id:
                vals['name'] = self.env.user.partner_id.id
        if 'date_start' in vals:
            if vals['date_start'] == '':
                vals['date_start'] = False
        if 'date_end' in vals:
            if vals['date_end'] == '':
                vals['date_end'] = False
        res = super(SupplierInfo, self).create(vals)
        return res

    def test(self):
        return print("yessss")

class UoM(models.Model):
    _inherit = 'uom.uom'

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    def set_notes(self):
        res_text = ""
        for res in self:
            if res.term_condition_box:
                res_text = res.term_condition_box
        return res_text

    def write(self, vals):
        res = super(PurchaseOrder, self).write(vals)
        return res

class TermCondition(models.Model):
    _inherit = 'term.condition'

    @api.constrains('term_condition')
    def update_term_con(self):
        for res in self:
            purchases = self.env['purchase.order'].search([('term_condition', '=', res.id)])
            for purchase in purchases:
                purchase._set_term_condition_box()