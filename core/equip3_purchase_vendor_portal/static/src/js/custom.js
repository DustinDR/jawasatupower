$(document).ready(function (e) {
    $("#btn_update_bids").click(function (e) {
        $.ajax({
            url: "/rfq/updates",
            data: { order_id: $("#order_id").val() },
            type: "post",
            cache: false,
            success: function (result) {
                $("#update_message").show();
                $("#error_message").show();
            },
        });
    });
});
