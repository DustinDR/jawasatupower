from odoo import models, fields, api
from odoo.exceptions import ValidationError



class HashMicroConfigSettings(models.TransientModel):
    _name = 'expiry.contract.notification'
    name = fields.Char()
    month = fields.Integer()
    days = fields.Integer()
    employee_ids = fields.Many2many('hr.employee',string="Notify To PIC")
    deparment_ids = fields.Many2many('hr.department',string="Notify To Department")
    job_ids = fields.Many2many('hr.job',string="Job Position")
    company_id = fields.Many2one('res.company',default=lambda self:self.env.user.company_id.id)


    def unlink(self):
        if self.name:
            return ValidationError("Can't delete record");
        res=super(HashMicroConfigSettings, self).unlink()
        return  res




    def send_notification(self,type,id):
        employee_ids = []

        if type == "contract_expire":
            action_id = self.env.ref('equip3_hr_contract_extend.contract_expire_action')
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url') + (
                    "/web?&#min=1&limit=80&view_type=list&model=hr.contract&action=%s" % (action_id.id))
            template = self.env.ref('equip3_hr_contract_extend.mail_template_conract_expired')
            for data in self.employee_ids:
                employee_ids.append(data.id)
                context = self.env.context = dict(self.env.context)
                context.update({
                    'email_to': data.work_email,
                    'name': data.name,
                    'url_test':base_url
                })
                template.send_mail(id, force_send=True)
                template.with_context(context)
            employee_department = self.env['hr.employee'].search([('job_id','in',self.job_ids.ids),('id','not in',employee_ids)])
            if employee_department:
                for data_department in employee_department:
                    context = self.env.context = dict(self.env.context)
                    context.update({
                        'email_to': data_department.work_email,
                        'name': data_department.name,
                        'url_test': base_url
                    })
                    template.send_mail(id, force_send=True)
                    template.with_context(context)
        if type == "contract_renew":
            action_id_renew = self.env.ref('equip3_hr_contract_extend.contract_renew_action')
            data_id_renew = self.env.ref('equip3_hr_contract_extend.renew_contract_notification')
            base_url_renew = self.env['ir.config_parameter'].sudo().get_param('web.base.url') + (
                        '/web?&#view_type=form&model=to.renew.contract&action=%s&id=%s' % (
                action_id_renew.id, data_id_renew.id))
            template = self.env.ref('equip3_hr_contract_extend.mail_template_conract_to_renew')
            for data in self.employee_ids:
                employee_ids.append(data.id)
                context = self.env.context = dict(self.env.context)
                context.update({
                    'email_to': data.work_email,
                    'name': data.name,
                    'url_test':base_url_renew
                })
                template.send_mail(id, force_send=True)
                template.with_context(context)
            employee_department = self.env['hr.employee'].search([('job_id','in',self.job_ids.ids),('id','not in',employee_ids)])
            if employee_department:
                for data_department in employee_department:
                    context = self.env.context = dict(self.env.context)
                    context.update({
                        'email_to': data_department.work_email,
                        'name': data_department.name,
                        'url_test': base_url_renew
                    })
                    template.send_mail(id, force_send=True)
                    template.with_context(context)