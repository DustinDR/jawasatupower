# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import contract
from . import expiry_contract_notification
from . import hr_contract
from . import to_renew_contract
from . import hr_employee
from . import hr_contract_letter