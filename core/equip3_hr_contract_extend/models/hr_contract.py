from odoo import _, api, fields, models
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta
import base64


class HrContractInherit(models.Model):
    _inherit = 'hr.contract'

    work_location_id = fields.Many2one('work.location.object', "Work Location")
    branch_id = fields.Many2one("res.branch", string="Branch",
                                tracking=True)
    daily_wage = fields.Monetary()
    contract_template = fields.Many2one('hr.contract.letter')
    edit_hide_css = fields.Html(string='CSS', sanitize=False, compute='_compute_edit_hide_css')
    certificate_attachment = fields.Binary(string='Certificate Attachment')
    certificate_attachment_fname = fields.Char('Certificate Name')

    @api.depends('state')
    def _compute_edit_hide_css(self):
        for rec in self:
            if rec.state not in ['draft']:
                rec.edit_hide_css = '<style>.btn.btn-primary.o_form_button_edit {display: none !important;} .o_form_label.o_readonly_modifier{display: none !important;} </style>'
            else:
                rec.edit_hide_css = False

    def print_on_page(self):
        for record in self:
            if record.date_start:
                date_start = datetime.strptime(str(record.date_start), "%Y-%m-%d")
                date_start_string = datetime(date_start.year, date_start.month,
                                             date_start.day).strftime("%d %B %Y")
            if record.visa_expire:
                visa_expire = datetime.strptime(str(record.visa_expire), "%Y-%m-%d")
                visa_expire_string = datetime(visa_expire.year, visa_expire.month,
                                              visa_expire.day).strftime("%d %B %Y")

            if record.trial_date_end:
                trial_date_end = datetime.strptime(str(record.trial_date_end), "%Y-%m-%d")
                trial_date_end_string = datetime(trial_date_end.year, trial_date_end.month,
                                                 trial_date_end.day).strftime("%d %B %Y")
            if record.first_contract_date:
                first_contract_date = datetime.strptime(str(record.first_contract_date), "%Y-%m-%d")
                first_contract_date_string = datetime(first_contract_date.year, first_contract_date.month,
                                                      first_contract_date.day).strftime("%d %B %Y")
            if record.date_end:
                date_end = datetime.strptime(str(record.date_end), "%Y-%m-%d")
                date_end_string = datetime(date_end.year, date_end.month,
                                           date_end.day).strftime("%d %B %Y")
            if record.create_date:
                create_date = datetime.strptime(str(record.create_date), "%Y-%m-%d %H:%M:%S.%f")
                create_date_string = datetime(create_date.year, create_date.month,
                                              create_date.day).strftime("%d %B %Y")
            if not record.contract_template:
                raise ValidationError("Letter not set in Contract")
            temp = record.contract_template.letter_content
            letter_content_replace = record.contract_template.letter_content
            if "$(name)" in letter_content_replace:
                if not record.name:
                    raise ValidationError("Contract Reference is empty")
                letter_content_replace = str(letter_content_replace).replace("$(name)", record.name)
            if "$(branch_id)" in letter_content_replace:
                if not record.branch_id:
                    raise ValidationError("Branch is empty")
                letter_content_replace = str(letter_content_replace).replace("$(branch_id)", record.branch_id.name)
            if "$(company_country_id)" in letter_content_replace:
                if not record.company_country_id:
                    raise ValidationError("Company country is empty")
                letter_content_replace = str(letter_content_replace).replace("$(company_country_id)",
                                                                             record.company_country_id.name)
            if "$(company_id)" in letter_content_replace:
                if not record.company_id:
                    raise ValidationError("Company is empty")
            if "$(currency_id)" in letter_content_replace:
                if not record.currency_id:
                    raise ValidationError("Currency is empty")
                letter_content_replace = str(letter_content_replace).replace("$(currency_id)", record.currency_id.name)
            if "$(daily_wage)" in letter_content_replace:
                if not record.daily_wage:
                    raise ValidationError("Daily Wage is empty")
                letter_content_replace = str(letter_content_replace).replace("$(daily_wage)", record.daily_wage)
            if "$(date_end)" in letter_content_replace:
                if not record.date_end:
                    raise ValidationError("Date end is empty")
                letter_content_replace = str(letter_content_replace).replace("$(date_end)", date_end_string)
            if "$(employee_id)" in letter_content_replace:
                if not record.employee_id:
                    raise ValidationError("Employee is empty")
                letter_content_replace = str(letter_content_replace).replace("$(employee_id)", record.employee_id.name)
            if "$(date_start)" in letter_content_replace:
                if not record.date_start:
                    raise ValidationError("Date  Start is empty")
                letter_content_replace = str(letter_content_replace).replace("$(date_start)", date_start_string)
            if "$(first_contract_date)" in letter_content_replace:
                if not record.first_contract_date:
                    raise ValidationError("First Contract Date   is empty")
                letter_content_replace = str(letter_content_replace).replace("$(first_contract_date)",
                                                                             first_contract_date_string)
            if "$(resource_calendar_id)" in letter_content_replace:
                if not record.resource_calendar_id:
                    raise ValidationError("Resource Calendar is empty")
                letter_content_replace = str(letter_content_replace).replace("$(resource_calendar_id)",
                                                                             record.resource_calendar_id.name)
            if "$(hr_responsible_id)" in letter_content_replace:
                if not record.hr_responsible_id:
                    raise ValidationError("HR Responsible is empty")
                letter_content_replace = str(letter_content_replace).replace("$(hr_responsible_id)",
                                                                             record.hr_responsible_id.name)
            if "$(notes)" in letter_content_replace:
                if not record.notes:
                    raise ValidationError("Notes is empty")
                letter_content_replace = str(letter_content_replace).replace("$(notes)", record.notes)

            if "$(permit_no)" in letter_content_replace:
                if not record.notes:
                    raise ValidationError("Work permit is empty")
                letter_content_replace = str(letter_content_replace).replace("$(permit_no)", record.permit_no)
            if "$(state)" in letter_content_replace:
                if not record.state:
                    raise ValidationError("Status  is empty")
                letter_content_replace = str(letter_content_replace).replace("$(state)", record.permit_no)
            if "$(structure_type_id)" in letter_content_replace:
                if not record.structure_type_id:
                    raise ValidationError("Salary Structure Type is empty")
                letter_content_replace = str(letter_content_replace).replace("$(structure_type_id)",
                                                                             record.structure_type_id.name)
            if "$(trial_date_end_string)" in letter_content_replace:
                if not record.trial_date_end:
                    raise ValidationError("End of Trial Period is empty")
                letter_content_replace = str(letter_content_replace).replace("$(trial_date_end_string)",
                                                                             trial_date_end_string)
            if "$(type_id)" in letter_content_replace:
                if not record.type_id:
                    raise ValidationError("Employee Category is empty")
                letter_content_replace = str(letter_content_replace).replace("$(type_id)", record.type_id.name)
            if "$(visa_expire_string)" in letter_content_replace:
                if not record.visa_expire_string:
                    raise ValidationError("Visa Expire Date is Empty")
                letter_content_replace = str(letter_content_replace).replace("$(visa_expire_string)",
                                                                             visa_expire_string)
            if "$(visa_no)" in letter_content_replace:
                if not record.visa_no:
                    raise ValidationError("Visa No is Empty")
                letter_content_replace = str(letter_content_replace).replace("$(visa_expire_string)",
                                                                             visa_expire_string)
            if "$(wage)" in letter_content_replace:
                if not record.wage:
                    raise ValidationError("Wage is Empty")
                letter_content_replace = str(letter_content_replace).replace("$(wage)", str(record.wage))

            if "$(job_id)" in letter_content_replace:
                if not record.job_id:
                    raise ValidationError("Job is empty")
                letter_content_replace = str(letter_content_replace).replace("$(job_id)", record.job_id.name)
            if "$(department_id)" in letter_content_replace:
                if not record.department_id:
                    raise ValidationError("Department is empty")
                letter_content_replace = str(letter_content_replace).replace("$(department_id)",
                                                                             record.department_id.name)
            if "$(work_location_id)" in letter_content_replace:
                if not record.work_location_id:
                    raise ValidationError("Work Location is empty")
                letter_content_replace = str(letter_content_replace).replace("$(work_location_id)",
                                                                             record.work_location_id.name)
            if "$(create_date)" in letter_content_replace:
                letter_content_replace = str(letter_content_replace).replace("$(create_date)", create_date_string)

            record.contract_template.letter_content = letter_content_replace
            data = record.contract_template.letter_content
            record.contract_template.letter_content = temp

            return data

    def search_manpower_planning(self):
        mpp_on = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment_extend.mpp')
        if mpp_on:
            if self.first_contract_date:
                first_contract_date = datetime.strptime(str(self.first_contract_date), "%Y-%m-%d")
                mpp_line = self.env['manpower.planning.line'].search(
                    [('job_position_id', '=', self.job_id.id), ('work_location_id', '=', self.work_location_id.id)])
                if mpp_line:
                    for record in mpp_line:
                        if record.manpower_id:
                            if first_contract_date.date() >= record.manpower_id.mpp_period.start_period and first_contract_date.date() <= record.manpower_id.mpp_period.end_period:
                                record.total_fullfillment = record.total_fullfillment + 1

    def ir_cron_send_notification(self):
        global_setting = self.env['expiry.contract.notification'].search([])

        now = datetime.now()
        draft_contract = self.search([('state', '=', 'draft'), ('date_start', '<=', now.date())])
        if draft_contract:
            for data in draft_contract:
                data.state = 'open'
                ## start add by hadorik
                employee = self.env['hr.employee'].browse(data.employee_id.id)
                employee.department_id = data.department_id.id
                employee.job_id = data.job_id.id
                data.search_manpower_planning()
                ## end of add
        running_contract = self.search([('state', '=', 'open'), ('date_end', '<=', now.date())])
        if running_contract:
            for data_running in running_contract:
                data_running.state = 'close'
                setting = self.env['expiry.contract.notification'].search([])
                setting.send_notification("contract_expire", data_running.id)
        total_days = global_setting.days
        to_renew_contract = self.search([('date_end', '=', now.date() + timedelta(days=total_days))])
        if to_renew_contract:
            for data_renew in to_renew_contract:
                global_setting.send_notification("contract_renew", data_renew.id)

    def write(self, vals):
        res = super(HrContractInherit, self).write(vals)
        if vals.get('state'):
            if vals.get('state') == 'open':
                self.search_manpower_planning()
            self.employee_id.department_id = self.department_id.id
            self.employee_id.job_id = self.job_id.id
        return res

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        if self.employee_id:
            self.work_location_id = self.employee_id.location_id
        else:
            self.work_location_id = False

    def get_certificate_template(self):
        for rec in self:
            parent_record = rec
            if parent_record.contract_template:
                temp = parent_record.contract_template.letter_content
                letter_content_replace = parent_record.contract_template.letter_content
                if "$(name)" in letter_content_replace:
                    if not parent_record.name:
                        raise ValidationError("Certificate Name is empty")
                    letter_content_replace = str(letter_content_replace).replace("$(name)",
                                                                                 parent_record.name)
                if "$(employee_id)" in letter_content_replace:
                    if not rec.employee_id.name:
                        raise ValidationError("Employee Name is empty")
                    letter_content_replace = str(letter_content_replace).replace("$(employee_id)",
                                                                                 rec.employee_id.name)
                if "$(job_id)" in letter_content_replace:
                    if not parent_record.job_id.name:
                        raise ValidationError("Job is empty")
                    letter_content_replace = str(letter_content_replace).replace("$(job_id)",
                                                                                 rec.job_id.name)
                if "$(wage)" in letter_content_replace:
                    if not rec.wage:
                        raise ValidationError("Wage is empty")
                    letter_content_replace = str(letter_content_replace).replace("$(wage)",
                                                                                 str(rec.wage))
                if "$(create_date)" in letter_content_replace:
                    if not rec.date_start:
                        raise ValidationError("Create Date is empty")
                    c_date = rec.create_date
                    c_date_format = c_date.strftime('%m/%d/%Y')
                    letter_content_replace = str(letter_content_replace).replace("$(create_date)",
                                                                                 str(c_date_format))

                if "$(start_date)" in letter_content_replace:
                    if not parent_record.start_date:
                        raise ValidationError("Start Date is empty")
                    s_date = parent_record.start_date
                    s_date_format = s_date.strftime('%m/%d/%Y')
                    letter_content_replace = str(letter_content_replace).replace("$(start_date)",
                                                                                 str(s_date_format))
                if "$(end_date)" in letter_content_replace:
                    if not parent_record.end_date:
                        raise ValidationError("End Date is empty")
                    e_date = parent_record.end_date
                    e_date_format = e_date.strftime('%m/%d/%Y')
                    letter_content_replace = str(letter_content_replace).replace("$(end_date)",
                                                                                 str(e_date_format))
                if "$(resource_calendar_id)" in letter_content_replace:
                    if parent_record.resource_calendar_id:
                        letter_content_replace = str(letter_content_replace).replace("$(resource_calendar_id)",
                                                                                     rec.resource_calendar_id.name)
                parent_record.contract_template.letter_content = letter_content_replace
                data = parent_record.contract_template.letter_content
                parent_record.contract_template.letter_content = temp
                return data

    def update_certificate(self):
        for rec in self:
            pdf = self.env.ref('equip3_hr_contract_extend.equip3_hr_contract_letter_mail')._render_qweb_pdf(rec.id)
            attachment = base64.b64encode(pdf[0])
            self.certificate_attachment = attachment
            self.certificate_attachment_fname = f"{'contract'}_{self.employee_id.name}"

    def action_contract_email_send(self):
        ir_model_data = self.env['ir.model.data']
        self.update_certificate()
        for rec in self:
            try:
                template_id = ir_model_data.get_object_reference(
                    'equip3_hr_contract_extend',
                    'email_template_contract_letter')[1]
            except ValueError:
                template_id = False
            ir_values = {
                'name': self.certificate_attachment_fname + '.pdf',
                'type': 'binary',
                'datas': self.certificate_attachment,
                'store_fname': self.certificate_attachment_fname,
                'mimetype': 'application/x-pdf',
            }
            data_id = self.env['ir.attachment'].create(ir_values)
            template = self.env['mail.template'].browse(template_id)
            template.attachment_ids = [(6, 0, [data_id.id])]
            template.send_mail(rec.id, force_send=True)
            template.attachment_ids = [(3, data_id.id)]
            break
