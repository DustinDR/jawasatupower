# -*- coding: utf-8 -*-
{
    'name': "Equip3 Sale E-Signature",

    'summary': """
        Manage digital signature integrate with Privy""",

    'description': """
        This module manages these features :
        1. E-Signature in settings
        2. Sales Orders Quotations
        3. Sales Orders Customers
    """,

    'author': "Hashmicro/Muhammad Saleem",
    'category': 'Sales',
    'version': '1.1.1',

    # any module necessary for this one to work correctly
    'depends': [
        'sale',
    ],

    # always loaded
    'data': [
        'views/res_config_setting_view.xml',
        'views/sale_view.xml',
        'views/partner_view.xml',
    ],
    'auto_install': False,
    'installable': True,

    'demo': [

    ],
}