
from odoo import api, fields, models

class ResPartner(models.Model):
    _inherit = 'res.partner'

    signature_name = fields.Char(string="Name")
    signature_privy_id = fields.Char(string="Privy Id")
    signature_email = fields.Char(string="Email")
    signature_mobile = fields.Char(string="Mobile")

    def get_default_field_value(self):
        config = self.env['ir.config_parameter']
        return config.sudo().get_param("customer_esignature")

    customer_esignature = fields.Boolean(string="Customer Signature", default=get_default_field_value,
                                         compute='_compute_customer_esignature')

    def _compute_customer_esignature(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        for record in self:
            record.customer_esignature = IrConfigParam.get_param('customer_esignature')
