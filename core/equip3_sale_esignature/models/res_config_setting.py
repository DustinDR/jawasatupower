# -*- coding: utf-8 -*-

from odoo import models, api, fields

class ResConfigSetting(models.TransientModel):
    _inherit = 'res.config.settings'

    customer_esignature = fields.Boolean(string="Customer Signature")

    @api.model
    def get_values(self):
        res = super(ResConfigSetting, self).get_values()
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        res.update({
            'customer_esignature': IrConfigParam.get_param('customer_esignature'),
        })
        return res

    def set_values(self):
        super(ResConfigSetting, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param('customer_esignature', self.customer_esignature)
