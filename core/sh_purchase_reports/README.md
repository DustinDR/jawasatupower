About
============
All in one purchase report useful to provide different purchases and bill reports to do analysis. A purchase analysis report shows the trends that occur in a company's purchase volume over time. In its most basic form, a purchase analysis report shows whether purchases are increasing or declining. At any time during the fiscal year, purchase managers may analyze the trends in the report to determine the best course of action. Purchase reports are a record of purchase activity over a particular period.


Installation
============
1) Copy module files to addon folder.
2) Restart odoo service (sudo service odoo-server restart).
3) Go to your odoo instance and open apps (make sure to activate debug mode).
4) click on update app list. 
5) search module name and hit install button.

Softhealer Technologies Support Team
=====================================
Skype: live:softhealertechnologies
What's app: +917984575681
E-Mail: suppport@softhealer.com
Website: https://softhealer.com
