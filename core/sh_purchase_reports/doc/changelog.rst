14.0.1 (Date : 02 November 2020)
-------

- initial release

14.0.2 (Date : 5th Feb 2021)
---------------------------
[FIX] fix table class.

14.0.3 (Date : 28th May 2021)
-------------------------------
[UPDATE] update some payment calculation of group payments.

14.0.4 (Date : 28th June 2021)
-------------------------------
[ADDED] sh_product_purchase_indent,sh_purchase_bill_summary,sh_purchase_by_category,sh_purchase_product_profit,sh_vendor_purchase_analysis  Added.

14.0.5 (Date : 31st July 2021)
---------------------------------
[ADD] datetime field is added in all reports and filter data with date and time.

14.0.6 (Date : 10th August 2021)
-----------------------------------
[FIX] small bug fixed.

14.0.7 (Date : 20th August 2021)
-------------------------------
[UPDATE] update journal multi company wise and filtered and get data with that in print.
