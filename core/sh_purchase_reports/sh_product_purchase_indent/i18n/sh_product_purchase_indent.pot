# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* sh_product_purchase_indent
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 14.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-05 08:02+0000\n"
"PO-Revision-Date: 2021-06-05 08:02+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: sh_product_purchase_indent
#: model:ir.actions.report,print_report_name:sh_product_purchase_indent.sh_purchase_product_indent_action
msgid "'Purchase Product Indent'"
msgstr ""

#. module: sh_product_purchase_indent
#: model_terms:ir.ui.view,arch_db:sh_product_purchase_indent.sh_po_product_indent_doc
msgid "<strong>Total</strong>"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields.selection,name:sh_product_purchase_indent.selection__sh_purchase_product_indent_wizard__sh_status__all
msgid "All"
msgstr ""

#. module: sh_product_purchase_indent
#: model_terms:ir.ui.view,arch_db:sh_product_purchase_indent.sh_purchase_product_indent_wizard
msgid "Cancel"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_wizard__sh_category_ids
msgid "Categories"
msgstr ""

#. module: sh_product_purchase_indent
#: model_terms:ir.ui.view,arch_db:sh_product_purchase_indent.sh_purchase_product_indent_xls_view
msgid "Close"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_wizard__create_uid
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_xls__create_uid
msgid "Created by"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_wizard__create_date
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_xls__create_date
msgid "Created on"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_report_sh_product_purchase_indent_sh_po_product_indent_doc__display_name
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_wizard__display_name
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_xls__display_name
msgid "Display Name"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_xls__excel_file
msgid "Download report Excel"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields.selection,name:sh_product_purchase_indent.selection__sh_purchase_product_indent_wizard__sh_status__draft
msgid "Draft"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_wizard__sh_end_date
msgid "End Date"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_xls__file_name
msgid "Excel File"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_report_sh_product_purchase_indent_sh_po_product_indent_doc__id
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_wizard__id
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_xls__id
msgid "ID"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_report_sh_product_purchase_indent_sh_po_product_indent_doc____last_update
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_wizard____last_update
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_xls____last_update
msgid "Last Modified on"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_wizard__write_uid
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_xls__write_uid
msgid "Last Updated by"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_wizard__write_date
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_xls__write_date
msgid "Last Updated on"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields.selection,name:sh_product_purchase_indent.selection__sh_purchase_product_indent_wizard__sh_status__done
msgid "Locked"
msgstr ""

#. module: sh_product_purchase_indent
#: model_terms:ir.ui.view,arch_db:sh_product_purchase_indent.sh_purchase_product_indent_wizard
msgid "Print"
msgstr ""

#. module: sh_product_purchase_indent
#: model_terms:ir.ui.view,arch_db:sh_product_purchase_indent.sh_purchase_product_indent_wizard
msgid "Print In XLS"
msgstr ""

#. module: sh_product_purchase_indent
#: model_terms:ir.ui.view,arch_db:sh_product_purchase_indent.sh_po_product_indent_doc
msgid "Product"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields.selection,name:sh_product_purchase_indent.selection__sh_purchase_product_indent_wizard__sh_status__purchase
msgid "Purchase Order"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.actions.act_window,name:sh_product_purchase_indent.action_purchase_product_indent
#: model:ir.actions.act_window,name:sh_product_purchase_indent.action_purchase_product_indent_xls
#: model:ir.actions.report,name:sh_product_purchase_indent.sh_purchase_product_indent_action
#: model:ir.ui.menu,name:sh_product_purchase_indent.menu_purchase_product_indent
#: model_terms:ir.ui.view,arch_db:sh_product_purchase_indent.sh_po_product_indent_doc
#: model_terms:ir.ui.view,arch_db:sh_product_purchase_indent.sh_purchase_product_indent_wizard
#: model_terms:ir.ui.view,arch_db:sh_product_purchase_indent.sh_purchase_product_indent_xls_view
msgid "Purchase Product Indent"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model,name:sh_product_purchase_indent.model_sh_purchase_product_indent_wizard
msgid "Purchase Product Indent Wizard"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model,name:sh_product_purchase_indent.model_sh_purchase_product_indent_xls
msgid "Purchase Product Indent Xls Report"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model,name:sh_product_purchase_indent.model_report_sh_product_purchase_indent_sh_po_product_indent_doc
msgid "Purchase product indent report abstract model"
msgstr ""

#. module: sh_product_purchase_indent
#: model_terms:ir.ui.view,arch_db:sh_product_purchase_indent.sh_po_product_indent_doc
msgid "Quantity"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields.selection,name:sh_product_purchase_indent.selection__sh_purchase_product_indent_wizard__sh_status__sent
msgid "RFQ Sent"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_wizard__sh_start_date
msgid "Start Date"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_wizard__sh_status
msgid "Status"
msgstr ""

#. module: sh_product_purchase_indent
#: model_terms:ir.ui.view,arch_db:sh_product_purchase_indent.sh_po_product_indent_doc
msgid "To"
msgstr ""

#. module: sh_product_purchase_indent
#: model:ir.model.fields,field_description:sh_product_purchase_indent.field_sh_purchase_product_indent_wizard__sh_partner_ids
msgid "Vendors"
msgstr ""

#. module: sh_product_purchase_indent
#: model_terms:ir.ui.view,arch_db:sh_product_purchase_indent.sh_purchase_product_indent_wizard
msgid "or"
msgstr ""

#. module: sh_product_purchase_indent
#: code:addons/sh_product_purchase_indent/wizard/purchase_product_indent_wizard.py:0
#, python-format
msgid "start date must be less than end date."
msgstr ""
