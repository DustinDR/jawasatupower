Changelog
=========
14.0.1
-------------------------
Initial Release

Changelog
=========
Version 14.0.2 (Date : 4th Feb 2021)
-----------------------------------------
[ADD] add configuration add vendors as followers in tender when validate the tender and send the tender using send by email button.
[ADD] add configuration to create portal users(vendors) based on the configuration when validate the tender. 

Version 14.0.3 (Date : 9th Feb 2021)
------------------------------------
[FIX] fix small bug when create portal users(vendors).

Version 14.0.4 (Date : 22nd June 2021)
------------------------------------
[FIX] fix singleton error when print report from tree view and add condition in analyze multiple quotation view in group by tender.

Version 14.0.5 (Date : 22nd June 2021)
------------------------------------
[ADD] configuration for manage tender documents and smart button for tender documents.
