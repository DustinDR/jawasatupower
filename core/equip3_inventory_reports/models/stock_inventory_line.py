from odoo import models, fields


class StockInventoryLine(models.Model):
    _inherit = "stock.inventory.line"

    to_call_difference_qty_compute = fields.Boolean(compute='_compute_to_call_difference_qty_compute')
    difference_qty = fields.Float('Changes', compute='_compute_difference',
        help="Indicates the gap between the product's theoretical quantity and its newest quantity.",
        readonly=True, digits='Product Unit of Measure', search="_search_difference_qty", store=True)

    def _compute_to_call_difference_qty_compute(self):
        for rec in self:
            rec._compute_difference()
            rec.to_call_difference_qty_compute = False