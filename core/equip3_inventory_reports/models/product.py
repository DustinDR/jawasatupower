from odoo import models, api, fields


class ProductProduct(models.Model):
    _inherit = "product.product"

    @api.depends_context('search_warehouse_id', 'compute_for_ids')
    @api.depends('stock_move_ids.product_qty', 'stock_move_ids.state')
    def _compute_inv_reports_warehouse_quantities(self):
        for product_id in self:
            product_id.inv_report_qty_available = 0.0
            product_id.inv_report_incoming_qty = 0.0
            product_id.inv_report_outgoing_qty = 0.0
            product_id.inv_report_virtual_available = 0.0

        product_ids = self.browse(self.env.context.get('compute_for_ids', []))

        if not product_ids:
            return

        warehouse_id = self._get_inv_reports_context_values()

        product_ids = product_ids.with_context(
			warehouse=warehouse_id.id,
			location=warehouse_id.view_location_id.id
		)

        res = product_ids._compute_quantities_dict(
			None, # lot_id
			None, # owner_id
			None, # package_id
		)

        for product_id in product_ids:
            qty_available = res[product_id.id]['qty_available']
            incoming_qty = res[product_id.id]['incoming_qty']
            outgoing_qty = res[product_id.id]['outgoing_qty']
            virtual_available = res[product_id.id]['virtual_available']
            free_qty = res[product_id.id]['free_qty']

            product_id.inv_report_qty_available = qty_available
            product_id.inv_report_incoming_qty = incoming_qty
            product_id.inv_report_outgoing_qty = outgoing_qty
            product_id.inv_report_virtual_available = virtual_available
            product_id.inv_report_free_qty = free_qty

    def _get_inv_reports_context_values(self):
        warehouse = int(self.env.context.get('search_warehouse_id', '-1'))

        warehouse_id = self.env['stock.warehouse']
        if warehouse != -1:
            warehouse_id = warehouse_id.browse(warehouse)

        return warehouse_id


    inv_report_qty_available = fields.Float(compute=_compute_inv_reports_warehouse_quantities)
    inv_report_incoming_qty = fields.Float(compute=_compute_inv_reports_warehouse_quantities)
    inv_report_outgoing_qty = fields.Float(compute=_compute_inv_reports_warehouse_quantities)
    inv_report_virtual_available = fields.Float(compute=_compute_inv_reports_warehouse_quantities)
    inv_report_free_qty = fields.Float(compute=_compute_inv_reports_warehouse_quantities)


    @api.model
    def get_warehouse_values(self, values=None):

        if values:
            values = list(values.values())[0]
            if '-1' in values['warehouses'] and values['warehouse_id'] != -1:
                del values['warehouses']['-1']
            return values

        result = {
			'warehouses': {-1: 'Please Select Warehouse'},
			'warehouse_id': -1
		}
        for warehouse_id in self.env['stock.warehouse'].search([]):
            result['warehouses'][warehouse_id.id] = warehouse_id.name
        return result

    @api.model
    def get_warehouse_based_product(self):
        warehouse_id = self._get_inv_reports_context_values()

        lot_stock_id = False
        if warehouse_id:
            lot_stock_id = warehouse_id.lot_stock_id

        stock_move_ids = self.env['stock.move'].search([]).filtered(lambda s: s.location_id == lot_stock_id or s.location_dest_id == lot_stock_id)

        if not stock_move_ids:
            return []

        product_ids = stock_move_ids.mapped('product_id')
        return product_ids.ids