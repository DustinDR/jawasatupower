from odoo import models, fields, api, _
from odoo.exceptions import UserError

class stock_picking(models.Model):
    _inherit = 'stock.picking'

    fulfillment = fields.Float(string="Fulfillment (%)", compute='_calculate_fulfillment', store=True)
    operation_warehouse_id = fields.Many2one(related='picking_type_id.warehouse_id', store=True)
    move_line_ids_without_package = fields.One2many('stock.move.line', 'picking_id', 'Operations without package',order = 'move_line_sequence',  domain=['|',('package_level_id', '=', False), ('picking_type_entire_packs', '=', False)])
       
    def action_assign(self):
        self.filtered(lambda picking: picking.state == 'draft').action_confirm()
        moves = self.mapped('move_ids_without_package').filtered(lambda move: move.state not in ('draft', 'cancel', 'done'))
        if moves and self.filtered(lambda picking: picking.state in ('confirmed','waiting')):
            moves.write({'reserved_by_id': self.env.user.id, 'is_reserved': True})
        if not moves:
            raise UserError(_('Nothing to check the availability for.'))
        moves._action_assign()
        move_line_list = []

        sort_quants_by = self.env['ir.config_parameter'].sudo().get_param('sort_quants_by') or False
        routing_order = self.env['ir.config_parameter'].sudo().get_param('routing_order') or False
        # print('ro', routing_order)
        # location_dup = []
        if sort_quants_by == 'location_name':
            location_name_list = []
            for line in self.move_line_ids_without_package:
                # if line.location_id.display_name not in location_dup:
                #     data = {'name': line.location_id.display_name}
                    location_name_list.append(line.location_id.display_name)
            # print('ln', location_name_list)
            if routing_order == "ascending":
                location_name_list_sorted = sorted(location_name_list)
                # print('ln_sorted', location_name_list_sorted)
            elif routing_order == 'descending':
                location_name_list_sorted = sorted(location_name_list, reverse=True)
                # print('ln_sorted', location_name_list_sorted)
            priority = 1
            for name in location_name_list_sorted:
                for line in self.move_line_ids_without_package:
                    if name == line.location_id.display_name:
                        line.move_line_sequence = priority
                        priority += 1


            # print(cccc)
        else:
            location_priority_list = []
            location_dup = []
            for line in self.move_line_ids_without_package:
                if line.location_id.display_name not in location_dup:
                    data = {'name': line.location_id.display_name, 'priority': line.location_id.removal_priority}
                    location_priority_list.append(data)
                    location_dup.append(line.location_id.display_name)
            location_priority_list = sorted(location_priority_list, key=lambda i:  i['priority'])
            priority = 1
            for prior in location_priority_list:
                for line in self.move_line_ids_without_package:
                    if prior['name'] == line.location_id.display_name:
                        line.move_line_sequence = priority
                        priority += 1
        return True

    def button_validate(self):
        res = super(stock_picking, self).button_validate()
        sort_quants_by = self.env['ir.config_parameter'].sudo().get_param('sort_quants_by') or False
        routing_order = self.env['ir.config_parameter'].sudo().get_param('routing_order') or False
        # print('ro', routing_order)
        # location_dup = []
        if sort_quants_by == 'location_name':
            location_name_list = []
            for line in self.move_line_ids_without_package:
                location_name_list.append(line.location_id.display_name)
            # print('ln', location_name_list)
            if routing_order == "ascending":
                # print('aesc')
                location_name_list_sorted = sorted(location_name_list)
                # print('ln_sorted', location_name_list_sorted)
            elif routing_order == 'descending':
                # print('desc')
                location_name_list_sorted = sorted(location_name_list, reverse=True)
                # print('ln_sorted', location_name_list_sorted)
            priority = 1
            for name in location_name_list_sorted:
                for line in self.move_line_ids_without_package:
                    if name == line.location_id.display_name:
                        line.move_line_sequence = priority
                        priority += 1

        else:
            location_priority_list = []
            location_dup = []
            for line in self.move_line_ids_without_package:
                if line.location_id.display_name not in location_dup:
                    data = {'name': line.location_id.display_name, 'priority': line.location_id.removal_priority}
                    location_priority_list.append(data)
                    location_dup.append(line.location_id.display_name)
            location_priority_list = sorted(location_priority_list, key=lambda i:  i['priority'])
            priority = 1
            for prior in location_priority_list:
                for line in self.move_line_ids_without_package:
                    if prior['name'] == line.location_id.display_name:
                        line.move_line_sequence = priority
                        priority += 1
        return res

    @api.model
    def action_internal_transfer_menu(self):
        res = super(stock_picking, self).action_internal_transfer_menu()
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        internal_type = IrConfigParam.get_param('internal_type', "with_transit")
        if internal_type == 'with_transit':
            self.env.ref('equip3_inventory_reports.menu_inventory_in_transit').active = True
        else:
            self.env.ref('equip3_inventory_reports.menu_inventory_in_transit').active = False
        return res

    @api.depends('move_ids_without_package', 'move_ids_without_package.fulfillment')
    def _calculate_fulfillment(self):
        for record in self:
            record.fulfillment = 0
            if record.move_ids_without_package:
                record.fulfillment = sum(record.move_ids_without_package.mapped('fulfillment'))/len(record.move_ids_without_package)


class StockImmediateTransfer(models.TransientModel):
    _inherit = 'stock.immediate.transfer'

    def process(self):
        res = super(StockImmediateTransfer, self).process()
        for picking in self.pick_ids:
            # print('zzzzz', klla)
            sort_quants_by = self.env['ir.config_parameter'].sudo().get_param('sort_quants_by') or False
            routing_order = self.env['ir.config_parameter'].sudo().get_param('routing_order') or False
            if sort_quants_by == 'location_name':
                location_name_list = []
                for line in picking.move_line_ids_without_package:
                    location_name_list.append(line.location_id.display_name)
                # print('ln', location_name_list)
                if routing_order == "ascending":
                    # print('aesc')
                    location_name_list_sorted = sorted(location_name_list)
                    # print('ln_sorted', location_name_list_sorted)
                elif routing_order == 'descending':
                    # print('desc')
                    location_name_list_sorted = sorted(location_name_list, reverse=True)
                    # print('ln_sorted', location_name_list_sorted)
                priority = 1
                for name in location_name_list_sorted:
                    for line in picking.move_line_ids_without_package:
                        if name == line.location_id.display_name:
                            line.move_line_sequence = priority
                            priority += 1
            else:
                location_priority_list = []
                location_dup = []
                for line in picking.move_line_ids_without_package:
                    if line.location_id.display_name not in location_dup:
                        data = {'name': line.location_id.display_name, 'priority': line.location_id.removal_priority}
                        location_priority_list.append(data)
                        location_dup.append(line.location_id.display_name)
                    # print('line', line)
                    # self.write({'move_line_ids_without_package': [(2,line.id)]})
                # print('ld', location_dup)
                # print('lpl', location_priority_list)
                location_priority_list = sorted(location_priority_list, key=lambda i:  i['priority'])
                # print('lpl_sorted', location_priority_list)
                move_lines_desc = picking.move_line_ids_without_package.search([('reference', '=', picking.name)], order='product_uom_qty desc')
                for line in move_lines_desc:
                    print('reserve_qty', line.product_uom_qty)
                priority = 1
                for prior in location_priority_list:
                    for line in picking.move_line_ids_without_package:
                        if prior['name'] == line.location_id.display_name:
                            line.move_line_sequence = priority
                            priority += 1
        return res

