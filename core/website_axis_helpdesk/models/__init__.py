# -*- coding: utf-8 -*-

from . import digest
from . import helpdesk
from . import helpdesk_ticket
from . import res_users
from . import res_partner
from . import helpdesk_ticket_inherit
from . import mail_thread_inherit
from . import res_config_settings
