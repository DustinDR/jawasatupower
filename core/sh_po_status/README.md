About
============
This module is useful to get the status of shipment and bill of purchase orders. Easily filters purchase orders with fully shipped, partial shipped, paid, partially paid.


Installation
============
1) Copy module files to addon folder.
2) Restart odoo service (sudo service odoo-server restart).
3) Go to your odoo instance and open apps (make sure to activate debug mode).
4) click on update app list. 
5) search module name and hit install button.

Softhealer Technologies Support Team
=====================================
Skype: live:softhealertechnologies
What's app: +917984575681
E-Mail: suppport@softhealer.com
Website: https://softhealer.com
