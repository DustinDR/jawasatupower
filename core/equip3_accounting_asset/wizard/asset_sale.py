from odoo import api, fields, models, _

# class AccountJournal(models.Model):
#     _inherit = 'account.journal'

#     is_fiscal_book_exclude = fields.Boolean('')

# class AccountMove(models.Model):
#     _inherit = 'account.move'

#     is_fiscal_book_exclude = fields.Boolean('')
#     total_inv_ppn = fields.Float('')
#     total_tax_ppn = fields.Float('')
#     subtotal_inv_ppn = fields.Float('')

# class AccountMoveLine(models.Model):
#     _inherit = 'account.move.line'

#     tax_ids_ppn = fields.Many2one('accont.tax', '')


class AssetAssetSale(models.TransientModel):
    _name = 'asset.asset.sale'
    _description = 'Sale Asset Wizard'

    partner_id = fields.Many2one('res.partner', 'Sold To', required=1)
    amount = fields.Float('Sale Price', required=1)
    is_invoice = fields.Boolean('Create Invoice', required=1)
    currency_id = fields.Many2one('res.currency', 'Currency', required=1)
    journal_id = fields.Many2one('account.journal', 'Payment Method')
    asset_journal_id = fields.Many2one('account.journal', 'Journal')

    def confirm_asset_sale(self):
        asset_id = self.env.context.get('active_id')
        asset = self.env['account.asset.asset'].browse(asset_id)
        move_obj = self.env['account.move']
        company_id = asset.category_id.company_id

        posted_depreciation_lines = self.env['account.asset.depreciation.line'].search([('asset_id', '=', asset_id),('move_id','!=',False)], order='depreciation_date asc')
        
        asset_value = asset.value - asset.salvage_value
        if posted_depreciation_lines:
            
            unposted_depreciation_lines = self.env['account.asset.depreciation.line'].search([('asset_id', '=', asset_id),('move_id','=',False)])
            for line in unposted_depreciation_lines:
                line.unlink()


            cumulative_amount = 0
            last_remaining_value = 0
            sequence = 1
            for line in posted_depreciation_lines:
                cumulative_amount += line.amount
                line.write({'remaining_value' : asset_value-cumulative_amount,
                            'depreciated_value' : cumulative_amount,
                            'sequence' : sequence,
                            'name': (asset.code or '') + '/' + str(sequence),
                            })
                last_remaining_value = asset_value-cumulative_amount
                sequence += 1
                
            for line in posted_depreciation_lines:
                line.move_id.action_post()
            

            if last_remaining_value > self.amount:
                vals = {
                    'amount': last_remaining_value - self.amount,
                    'asset_id': asset_id,
                    'sequence': sequence,
                    'name': (asset.code or '') + '/' + str(sequence),
                    'remaining_value': self.amount,
                    'depreciated_value': cumulative_amount + last_remaining_value - self.amount,
                    'depreciation_date': fields.Date.today(),
                }
                
                new_depreciation_lines = self.env['account.asset.depreciation.line'].create(vals)
                
                for line in new_depreciation_lines:
                    line.create_move()
                    line.move_id.action_post()
     
        else:
            unposted_depreciation_lines = self.env['account.asset.depreciation.line'].search([('asset_id', '=', asset_id)])
            for line in unposted_depreciation_lines:
                line.unlink()
            
            vals = {
                    'amount': asset.value-asset.salvage_value >= self.amount and asset.value-self.amount-asset.salvage_value or 0,
                    'asset_id': asset_id,
                    'sequence': 1,
                    'name': (asset.code or '') + '/' + str(1),
                    'remaining_value': asset.value-asset.salvage_value >= self.amount and self.amount+asset.salvage_value or asset.value,
                    'depreciated_value': asset.value-asset.salvage_value >= self.amount and asset.value-asset.salvage_value-self.amount or 0,
                    'depreciation_date': fields.Date.today(),
                }

            new_depreciation_lines = self.env['account.asset.depreciation.line'].create(vals)
            
            for line in new_depreciation_lines:
                line.create_move()
                line.move_id.action_post()
            
        asset.message_post(body=_("Document closed."))
        asset.write({'state' : 'close'})
        
        if self.is_invoice:
            inv_vals = {
                'date': fields.Date.context_today(self),
                'invoice_date': fields.Date.context_today(self),
                'journal_id': self.asset_journal_id.id,
                'currency_id' : self.currency_id,
                'move_type' : 'out_invoice',
                'partner_id' :  self.partner_id.id,
            }
            
            inv_line_vals = {
                'name' : asset.name,
                'quantity' : 1.00,
                'price_unit' : abs(self.amount),
                'account_id' : (asset.category_id and asset.category_id.account_sales_id and asset.category_id.account_sales_id.id) or 
                                    (self.asset_journal_id.default_account_id and self.asset_journal_id.default_account_id.id) or False,
            }
            
            inv_vals.update({'invoice_line_ids' : [(0,0,inv_line_vals)]})
            
            move_id = move_obj.create(inv_vals)
            
            return {
                'name': _('Customer Invoice'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'account.move',
                'res_id': move_id.id,
            }
        
        else:
            #create the journal entries
            vals = {
                'name': '/',
                'ref': asset.name,
                'date': fields.Date.context_today(self),
                'journal_id': self.journal_id.id,
            }
            
            
            debit_currency = (self.journal_id.default_account_id and self.journal_id.default_account_id.currency_id) or company_id.currency_id
            credit_currency = (self.journal_id.suspense_account_id and self.journal_id.suspense_account_id.currency_id) or company_id.currency_id
                    
            amount_in_company_curr = self.env['res.currency']._compute(self.currency_id, company_id.currency_id, self.amount)        
            debit_foreign_amount = self.env['res.currency']._compute(self.currency_id, debit_currency, self.amount)
            credit_foreign_amount = self.env['res.currency']._compute(self.currency_id, credit_currency, self.amount)

            vals_debit = {
                'debit' : abs(amount_in_company_curr), 
                'credit' : 0, 
                'name' : asset.name, 
                'date' : fields.Date.context_today(self),
                'journal_id' : self.journal_id.id,
                'account_id' : self.journal_id.default_account_id.id,
                'currency_id' : debit_currency.id,
                'company_currency_id' : company_id.currency_id.id, 
                'amount_currency' : abs(debit_foreign_amount),
                'company_id' : company_id.id,
            }
            
            vals_credit = {
                'debit' : 0, 
                'credit' : abs(amount_in_company_curr), 
                'name' : asset.name,
                'date' : fields.Date.context_today(self), 
                'journal_id' : self.journal_id.id,
                'account_id' : self.journal_id.suspense_account_id.id,
                'currency_id' : credit_currency.id,
                'company_currency_id' : company_id.currency_id.id, 
                'amount_currency' : -abs(credit_foreign_amount),
                'company_id' : company_id.id,
            }
            vals.update({'line_ids' : [(0,0,vals_debit), (0,0,vals_credit)]})
            move_id = move_obj.create(vals)

            return {
                'name': asset.name,
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'account.move',
                'res_id': move_id.id,
            }