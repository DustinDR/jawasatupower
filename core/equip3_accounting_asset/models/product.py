from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError
import logging
_logger = logging.getLogger(__name__)

class Product(models.Model):
    _inherit = 'product.template'

    # type = fields.Selection(selection_add=[('asset', 'Asset')], ondelete='cascade')
    type = fields.Selection([
        ('consu', 'Consumable'),
        ('service', 'Service'),
        ('product', 'Storable Product'),
        ('asset', 'Asset')], string='Product Type', default='consu', required=True,
        help='A storable product is a product for which you manage stock. The Inventory app has to be installed.\n'
             'A consumable product is a product for which stock is not managed.\n'
             'A service is a non-material product you provide.')
    asset_category_id = fields.Many2one('account.asset.category', string='Asset Category')
    asset_entry_perqty = fields.Boolean('Asset Entry Perquantity')