
import calendar
from datetime import date, datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_compare, float_is_zero
import logging
_logger = logging.getLogger(__name__)


class AccountMove(models.Model):
    _inherit = 'account.move'
    
    def action_post(self):
        res = super(AccountMove, self).action_post()
        for move in self:
            for depreciation_line in move.asset_depreciation_ids:
                depreciation_line.post_lines_and_close_asset()
        return res


class AccountAssetCategory(models.Model):
    _inherit = 'account.asset.category'
    
    cut_off_asset_date = fields.Integer(string='Cut Off Asset Date', default=31)
    date_first_depreciation = fields.Selection([
        ('last_day_period', 'Based on Last Day of Purchase Period'),
        ('manual', 'Manual (Defaulted on Purchase Date)')],
        string='Depreciation Dates', default='manual', required=False,
        help='The way to compute the date of the first depreciation.\n'
             '  * Based on last day of purchase period: The depreciation dates will be based on the last day of the purchase month or the purchase year (depending on the periodicity of the depreciations).\n'
             '  * Based on purchase date: The depreciation dates will be based on the purchase date.')
    account_sales_id = fields.Many2one('account.account', 'Sales Asset Account', required=1)
    account_revaluation_surplus_id = fields.Many2one('account.account', 'Revaluation Asset Surplus', required=1)
    account_revaluation_loss_id = fields.Many2one('account.account', 'Revaluation Asset Loss', required=1)

    @api.constrains('cut_off_asset_date')
    def _check_cut_off_asset_date(self):
        if self.cut_off_asset_date < 1 or self.cut_off_asset_date > 31:
            raise ValidationError(_('Fill in the Cut off Asset Date with a value between the 1st to 31st.'))
        
        
    @api.onchange('cut_off_asset_date')
    def _onchange_cut_off_asset_date(self):
        if self.cut_off_asset_date < 1 or self.cut_off_asset_date > 31:
            raise ValidationError(_('Fill in the Cut off Asset Date with a value between the 1st to 31st.'))




class Asset(models.Model):
    _inherit = 'account.asset.asset'

    is_monthly_depreciation = fields.Boolean(string='Monthly Depreciation')
    is_reset_january = fields.Boolean(string='Reset on January')
    months_remaining = fields.Integer(string='Months Remaining', compute='_compute_months_remaining')
    method = fields.Selection(selection_add=[('double_declining', 'Double Declining Balance Method')]
                              , ondelete={'double_declining': 'cascade'})
    cut_off_asset_date = fields.Integer(string='Cut Off Asset Date', default=31)
    date_first_depreciation = fields.Selection([
        ('last_day_period', 'Based on Last Day of Purchase Period'),
        ('manual', 'Manual (Defaulted on Purchase Date)')],
        string='Depreciation Dates', default='manual',
        readonly=True, states={'draft': [('readonly', False)]}, required=False,
        help='The way to compute the date of the first depreciation.\n'
             '  * Based on last day of purchase period: The depreciation dates will be based on the last day of the purchase month or the purchase year (depending on the periodicity of the depreciations).\n'
             '  * Based on purchase date: The depreciation dates will be based on the purchase date.')
    
    fiscal_category_id = fields.Many2one('account.asset.category.fiscal', string='Fiscal Asset Category', change_default=True, readonly=True, states={'draft': [('readonly', False)]})
    method_fiscal = fields.Selection([('linear', 'Linear'), ('double_declining', 'Double Declining Balance Method')], string='Computation Method', required=True, readonly=True, states={'draft': [('readonly', False)]}, default='linear',)
    is_monthly_depreciation_fiscal = fields.Boolean(string='Monthly Depreciation')
    is_reset_january_fiscal = fields.Boolean(string='Reset on January')
    months_remaining_fiscal = fields.Integer(string='Months Remaining', compute='_compute_months_remaining_fiscal')
    method_number_fiscal = fields.Integer(string='Number of Depreciations', readonly=True, states={'draft': [('readonly', False)]}, default=5, help="The number of depreciations needed to depreciate your asset")
    method_period_fiscal = fields.Integer(string='Number of Months in a Period', required=True, readonly=True, default=12, states={'draft': [('readonly', False)]},
        help="The amount of time between two depreciations, in months")
    method_end_fiscal = fields.Date(string='Ending Date', readonly=True, states={'draft': [('readonly', False)]})
    depreciation_line_ids_fiscal = fields.One2many('account.asset.fiscal.line', 'asset_id', string='Fiscal Depreciation Lines', readonly=True, states={'draft': [('readonly', False)], 'open': [('readonly', False)]})
    asset_history_lines_ids = fields.One2many('asset.asset.revalue', 'asset_id', string='Asset History Lines')
    
    
    @api.onchange('fiscal_category_id')
    def onchange_fiscal_category_id_id(self):
        vals = self.onchange_fiscal_category_id_values(self.fiscal_category_id.id)
        # We cannot use 'write' on an object that doesn't exist yet
        if vals:
            for k, v in vals['value'].items():
                setattr(self, k, v)

    def onchange_fiscal_category_id_values(self, fiscal_category_id):
        if fiscal_category_id:
            fiscal_category = self.env['account.asset.category.fiscal'].browse(fiscal_category_id)
            return {
                'value': {
                    'method_fiscal': fiscal_category.method,
                    'method_number_fiscal': fiscal_category.method_number,
                    'method_period_fiscal': fiscal_category.method_period,
                    'is_monthly_depreciation_fiscal' : fiscal_category.is_monthly_depreciation,
                    'is_reset_january_fiscal' : fiscal_category.is_reset_january,
                }
            }
            
            
    def _compute_months_remaining_fiscal(self):
        fiscal_depreciation_lines = self.env['account.asset.fiscal.line'].search([('asset_id', '=', self.id)], order='depreciation_date_fiscal desc', limit=1)

        if len(fiscal_depreciation_lines) > 0:
            fiscal_depreciation_date = fiscal_depreciation_lines.depreciation_date_fiscal
            self.months_remaining_fiscal = 12 - fiscal_depreciation_date.month
        else:
            self.months_remaining_fiscal = 0
            
            
            
         
    @api.onchange('category_id')
    def onchange_category_id(self):
        vals = self.onchange_category_id_values(self.category_id.id)
        # We cannot use 'write' on an object that doesn't exist yet
        if vals:
            for k, v in vals['value'].items():
                setattr(self, k, v)         
    
    
    def onchange_category_id_values(self, category_id):
        if category_id:
            category = self.env['account.asset.category'].browse(category_id)
            return {
                'value': {
                    'method': category.method,
                    'method_number': category.method_number,
                    'method_time': category.method_time,
                    'method_period': category.method_period,
                    'method_progress_factor': category.method_progress_factor,
                    'method_end': category.method_end,
                    'prorata': category.prorata,
                    'date_first_depreciation': category.date_first_depreciation,
                    'account_analytic_id': category.account_analytic_id.id,
                    'analytic_tag_ids': [(6, 0, category.analytic_tag_ids.ids)],
                    'cut_off_asset_date' : category.cut_off_asset_date,
                }
            }

         
    @api.onchange('cut_off_asset_date','prorata','date')
    def onchange_cut_off_asset_date(self):
        date = self.date
        if date:
            date_list = str(date).split('-')
            cut_off_date_str = len(str(self.cut_off_asset_date))>=2 and str(self.cut_off_asset_date) or '0' + str(self.cut_off_asset_date)
            cut_off_date = '%s-%s-%s'%(date_list[0], date_list[1], cut_off_date_str)
            if not self.prorata and str(date) > cut_off_date:
                month = (int(date_list[1]) < 10 and '0%s'%(int(date_list[1])+1))\
                        or (int(date_list[1]) >= 10 and int(date_list[1]) < 12 and str(int(date_list[1])+1))\
                        or '01'
                year = int(date_list[1]) < 12 and str(int(date_list[0])) or str(int(date_list[0])+1)
                self.first_depreciation_manual_date = '%s-%s-%s'%(year, month, '01')
            elif not self.prorata and str(date) <= cut_off_date:
                self.first_depreciation_manual_date = '%s-%s-%s'%(date_list[0], date_list[1], '01')
            else:
                self.first_depreciation_manual_date = date
                
                
                
    @api.onchange('cut_off_asset_date')
    def _onchange_cut_off_asset_date(self):
        if self.cut_off_asset_date < 1 or self.cut_off_asset_date > 31:
            raise ValidationError(_('Fill in the Cut off Asset Date with a value between the 1st to 31st.'))
        
        
    @api.constrains('cut_off_asset_date')
    def _check_cut_off_asset_date(self):
        if self.cut_off_asset_date < 1 or self.cut_off_asset_date > 31:
            raise ValidationError(_('Fill in the Cut off Asset Date with a value between the 1st to 31st.'))



    def _compute_board_amount(self, sequence, residual_amount, amount_to_depr, undone_dotation_number, posted_depreciation_line_ids, total_days, depreciation_date):
        amount = super(Asset, self)._compute_board_amount(sequence, residual_amount, amount_to_depr, undone_dotation_number, posted_depreciation_line_ids, total_days, depreciation_date)
        if sequence == undone_dotation_number:
            amount = residual_amount
        else:
            if self.method == 'double_declining':
                if self.is_reset_january and sequence == 1:
                    months_remaining = 12 - depreciation_date.month
                    amount = (1/undone_dotation_number)*(2*residual_amount)*(months_remaining/12)
                else:
                    amount = (residual_amount / undone_dotation_number) * 2
        return amount
    
    
    def _compute_board_amount_fiscal(self, sequence, residual_amount, amount_to_depr, fiscal_undone_dotation_number, depreciation_line_ids_fiscal, total_days, depreciation_date):
        amount = 0
        if sequence == fiscal_undone_dotation_number:
            amount = residual_amount
        else:
            if self.method == 'linear':
                amount = amount_to_depr / (fiscal_undone_dotation_number - len(depreciation_line_ids_fiscal))
            elif self.method_fiscal == 'double_declining':
                if self.is_reset_january_fiscal and sequence == 1:
                    months_remaining_fiscal = 12 - depreciation_date.month
                    amount = (1/fiscal_undone_dotation_number)*(2*residual_amount)*(months_remaining_fiscal/12)
                else:
                    amount = (residual_amount / fiscal_undone_dotation_number) * 2

        return amount
    
    
    
    def compute_fiscal_depreciation(self):
        self.ensure_one()

        depreciation_line_ids_fiscal = []
        commands = []
        if self.depreciation_line_ids_fiscal:
            commands = [(5,0,0)]
        
        if self.value_residual != 0.0:
            amount_to_depr = residual_amount = self.value_residual

            # depreciation_date computed from the purchase date
            depreciation_date = self.date
            if self.first_depreciation_manual_date and self.first_depreciation_manual_date != self.date:
                # depreciation_date set manually from the 'first_depreciation_manual_date' field
                depreciation_date = self.first_depreciation_manual_date

            total_days = (depreciation_date.year % 4) and 365 or 366
            current_amount = 0
            fiscal_undone_dotation_number = self.method_number_fiscal

            _logger.exception(_("Debug: %s", range(len(depreciation_line_ids_fiscal), fiscal_undone_dotation_number)))
            
            for x in range(len(depreciation_line_ids_fiscal), fiscal_undone_dotation_number):
                sequence = x + 1
                amount = self._compute_board_amount_fiscal(sequence, residual_amount, amount_to_depr,
                                                    fiscal_undone_dotation_number, depreciation_line_ids_fiscal,
                                                    total_days, depreciation_date)
                amount = self.currency_id.round(amount)
                if float_is_zero(amount, precision_rounding=self.currency_id.rounding):
                    continue

                residual_amount -= amount

                if self.method_fiscal == 'double_declining' and self.is_monthly_depreciation_fiscal:
                    start_month = 0 if self.is_reset_january_fiscal else depreciation_date.month - 1
                    for i in range(start_month if x == 0 else 0, 12):

                        amt = (amount / 2 / 12) if not self.is_reset_january_fiscal and x == 0 else (amount / 12)
                        current_amount = current_amount + amt
                        vals = {
                            'amount_fiscal': amt,
                            'asset_id': self.id,
                            'sequence': sequence,
                            'name': (self.code or '') + '/' + str(sequence),
                            'remaining_value_fiscal': self.value - current_amount,
                            'depreciated_value_fiscal': current_amount,
                            'depreciation_date_fiscal': date(depreciation_date.year, i + 1, 1),
                        }
                        commands.append((0, False, vals))

                else:
                    vals = {
                        'amount_fiscal': amount,
                        'asset_id': self.id,
                        'sequence': sequence,
                        'name': (self.code or '') + '/' + str(sequence),
                        'remaining_value_fiscal': residual_amount,
                        'depreciated_value_fiscal': self.value - (self.salvage_value + residual_amount),
                        'depreciation_date_fiscal': depreciation_date,
                    }
                    commands.append((0, 0, vals))

                depreciation_date = depreciation_date + relativedelta(months=+self.method_period_fiscal)
                 
        

        return commands
    

    def compute_depreciation_board(self):
        self.ensure_one()

        posted_depreciation_line_ids = self.depreciation_line_ids.filtered(lambda x: x.move_check).sorted(
            key=lambda l: l.depreciation_date)
        unposted_depreciation_line_ids = self.depreciation_line_ids.filtered(lambda x: not x.move_check)
        
        # Remove old unposted depreciation lines. We cannot use unlink() with One2many field
        commands = [(2, line_id.id, False) for line_id in unposted_depreciation_line_ids]
        
        if self.value_residual != 0.0:
            amount_to_depr = residual_amount = self.value_residual

            # if we already have some previous validated entries, starting date is last entry + method period
            if posted_depreciation_line_ids and posted_depreciation_line_ids[-1].depreciation_date:
                last_depreciation_date = fields.Date.from_string(posted_depreciation_line_ids[-1].depreciation_date)
                depreciation_date = last_depreciation_date + relativedelta(months=+self.method_period)
            else:
                # depreciation_date computed from the purchase date
                depreciation_date = self.date
                if self.date_first_depreciation == 'last_day_period':
                    # depreciation_date = the last day of the month
                    depreciation_date = depreciation_date + relativedelta(day=31)
                    # ... or fiscalyear depending the number of period
                    if self.method_period == 12:
                        depreciation_date = depreciation_date + relativedelta(
                            month=self.company_id.fiscalyear_last_month)
                        depreciation_date = depreciation_date + relativedelta(
                            day=self.company_id.fiscalyear_last_day)
                        if depreciation_date < self.date:
                            depreciation_date = depreciation_date + relativedelta(years=1)
                elif self.first_depreciation_manual_date and self.first_depreciation_manual_date != self.date:
                    # depreciation_date set manually from the 'first_depreciation_manual_date' field
                    depreciation_date = self.first_depreciation_manual_date

            total_days = (depreciation_date.year % 4) and 365 or 366
            month_day = depreciation_date.day
            current_amount = 0
            undone_dotation_number = self._compute_board_undone_dotation_nb(depreciation_date, total_days)

            _logger.exception(_("Debug: %s", range(len(posted_depreciation_line_ids), undone_dotation_number)))
            
            for x in range(len(posted_depreciation_line_ids), undone_dotation_number):
                sequence = x + 1
                amount = self._compute_board_amount(sequence, residual_amount, amount_to_depr,
                                                    undone_dotation_number, posted_depreciation_line_ids,
                                                    total_days, depreciation_date)
                amount = self.currency_id.round(amount)
                if float_is_zero(amount, precision_rounding=self.currency_id.rounding):
                    continue

                residual_amount -= amount

                if self.method == 'double_declining' and self.is_monthly_depreciation:
                    start_month = 0 if self.is_reset_january else depreciation_date.month - 1
                    for i in range(start_month if x == 0 else 0, 12):

                        amt = (amount / 2 / 12) if not self.is_reset_january and x == 0 else (amount / 12)
                        current_amount = current_amount + amt
                        vals = {
                            'amount': amt,
                            'asset_id': self.id,
                            'sequence': sequence,
                            'name': (self.code or '') + '/' + str(sequence),
                            'remaining_value': self.value - current_amount,
                            'depreciated_value': current_amount,
                            'depreciation_date': date(depreciation_date.year, i + 1, 1),
                        }
                        commands.append((0, False, vals))

                else:
                    vals = {
                        'amount': amount,
                        'asset_id': self.id,
                        'sequence': sequence,
                        'name': (self.code or '') + '/' + str(sequence),
                        'remaining_value': residual_amount,
                        'depreciated_value': self.value - (self.salvage_value + residual_amount),
                        'depreciation_date': depreciation_date,
                    }
                    commands.append((0, False, vals))
            #
                depreciation_date = depreciation_date + relativedelta(months=+self.method_period)

                if month_day > 28 and self.date_first_depreciation == 'manual':
                    max_day_in_month = calendar.monthrange(depreciation_date.year, depreciation_date.month)[1]
                    depreciation_date = depreciation_date.replace(day=min(max_day_in_month, month_day))

                # datetime doesn't take into account that the number of days is not the same for each month
                if not self.prorata and self.method_period % 12 != 0 and self.date_first_depreciation == 'last_day_period':
                    max_day_in_month = calendar.monthrange(depreciation_date.year, depreciation_date.month)[1]
                    depreciation_date = depreciation_date.replace(day=max_day_in_month)
        
        depreciation_line_ids_fiscal = self.compute_fiscal_depreciation()
        self.write({'depreciation_line_ids': commands, 'depreciation_line_ids_fiscal' : depreciation_line_ids_fiscal})

        return True

    def _compute_months_remaining(self):
        depreciation_lines = self.env['account.asset.depreciation.line'].search([('asset_id', '=', self.id)], order='depreciation_date desc', limit=1)

        if len(depreciation_lines) > 0:
            depreciation_date = depreciation_lines.depreciation_date
            self.months_remaining = 12 - depreciation_date.month
        else:
            self.months_remaining = 0
            



class AccountAssetDepreciationLine(models.Model):
    _inherit = 'account.asset.depreciation.line'
    
    
    def create_move(self, post_move=True):
        created_moves = self.env['account.move']
        for line in self:
            if line.move_id:
                raise UserError(_('This depreciation is already linked to a journal entry. Please post or delete it.'))
            move_vals = self._prepare_move(line)
            move = self.env['account.move'].create(move_vals)
            line.write({'move_id': move.id, 'move_check': True})
            created_moves |= move
        #if post_move and created_moves:
        #   created_moves.filtered(lambda m: any(m.asset_depreciation_ids.mapped('asset_id.category_id.open_asset'))).action_post()
        return [x.id for x in created_moves]
    
    
class AccountAssetFiscalLine(models.Model):
    _name = 'account.asset.fiscal.line'
    _description = 'Fiscal depreciation line'

    name = fields.Char(string='Depreciation Name', required=True, index=True)
    sequence = fields.Integer(required=True)
    asset_id = fields.Many2one('account.asset.asset', string='Asset', required=True, ondelete='cascade')
    amount_fiscal = fields.Float(string='Current Depreciation', digits=0, required=True)
    remaining_value_fiscal = fields.Float(string='Next Period Depreciation', digits=0, required=True)
    depreciated_value_fiscal = fields.Float(string='Cumulative Depreciation', required=True)
    depreciation_date_fiscal = fields.Date('Depreciation Date', index=True)

    
    
    
class AccountAssetCategoryFiscal(models.Model):
    _name = 'account.asset.category.fiscal'
    _description = 'Fiscal Asset Group'
    _rec_name = 'asset_category_id'
    
    asset_category_id = fields.Many2one('account.asset.category', string='Fiscal Asset Type', required=True)
    method_time = fields.Selection([('number', 'Number of Entries'), ('end', 'Ending Date')], string='Time Method', default='number',
        help="Choose the method to use to compute the dates and number of entries.\n"
           "  * Number of Entries: Fix the number of entries and the time between 2 depreciations.\n"
           "  * Ending Date: Choose the time between 2 depreciations and the date the depreciations won't go beyond.")
    method_number = fields.Integer(string='Number of Entries', default=5)
    method_period = fields.Integer(string='One Entry Every', default=12,
        help="The amount of time between two depreciations, in months")
    method_end = fields.Date('Ending date')    
    method = fields.Selection([('linear', 'Linear'), ('double_declining', 'Double Declining Balance Method')], string='Computation Method')
    prorata = fields.Boolean(string='Prorata Temporis', help='Indicates that the first depreciation entry for this asset have to be done from the purchase date instead of the first of January')
    cut_off_asset_date = fields.Integer(string='Cut Off Asset Date', default=31)
    is_monthly_depreciation = fields.Boolean(string='Monthly Depreciation')
    is_reset_january = fields.Boolean(string='Reset on January')
    double_declining_method_number = fields.Integer(string='Number of Depreciations', default=5, help="The number of depreciations needed to depreciate your asset")
    double_declining_method_period = fields.Integer(string='Number of Months in a Period', default=12,
        help="The amount of time between two depreciations, in months")


class AssetAssetRevalueWizard(models.TransientModel):
    _name = 'asset.asset.revalue.wizard'
    _description = 'Asset Revaluation Wizard'
    
    def default_get(self, default_fields):
        context = self._context
        active_id = context.get('active_id', False)
        company = self.env['res.company'].browse([self.env.company])
        result = super(AssetAssetRevalueWizard, self).default_get(default_fields)
        if active_id:
            asset = self.env['account.asset.asset'].browse([active_id])
            result['remaining_value'] = asset.value_residual
            result['currency_id'] = asset.currency_id.id or company.currency_id.id
        return result
    
    reason = fields.Char('Reason', required=True)
    remaining_value = fields.Monetary('Current Amount', readonly=True, currency_field='currency_id', required=True)
    amount = fields.Monetary('Revalued Amount', currency_field='currency_id', required=True)
    journal_id = fields.Many2one('account.journal', 'Revalued Entry', required=True)
    method_number = fields.Integer('Number of Depreciation', required=True)
    method_period = fields.Integer('Number of Months', required=True)
    company_id = fields.Many2one('res.company', store=True, readonly=True, default=lambda self: self.env.company)
    company_currency_id = fields.Many2one(related='company_id.currency_id', string='Company Currency',
        readonly=True, store=True,
        help='Utility field to express amount currency')
    currency_id = fields.Many2one('res.currency', string='Currency',
        readonly=True, store=True,
        help='Utility field to express amount currency')
    
    def confirm_revalue_asset(self):
        asset_revalue_obj = self.env['asset.asset.revalue']
        context = self._context
        active_id = context.get('active_id', False)
        if active_id:
            asset = self.env['account.asset.asset'].browse([active_id])
            vals = {
                'reason' : self.reason,
                'asset_id' : active_id,
                'remaining_value' : self.remaining_value,
                'amount' : self.amount,
                'journal_id' : self.journal_id.id,
                'method_number' : self.method_number,
                'method_period' : self.method_period,
                'user_id' : self.env.uid,
                'date' : fields.Date.today(),
                'prev_method_number' : asset.method_number,
                'prev_method_period' : asset.method_period,
            }
            asset_revalue_obj.create(vals)          
        return True
            
    
    
        
class AssetAssetRevalue(models.Model):
    _name = 'asset.asset.revalue'
    _description = 'Asset Revaluation'
    _rec_name = 'reason'
    
    reason = fields.Char('History')
    asset_id = fields.Many2one('account.asset.asset', 'Asset')
    remaining_value = fields.Monetary('Previous Amount', readonly=True, currency_field='currency_id')
    amount = fields.Monetary('New Amount', currency_field='currency_id')
    journal_id = fields.Many2one('account.journal', 'Revalued Entry')
    method_number = fields.Integer('Number of Depreciation')
    method_period = fields.Integer('Number of Months')
    user_id = fields.Many2one('res.users', 'User')
    date = fields.Date('Date')
    prev_method_number = fields.Integer('Previous Number of Depreciation')
    prev_method_period = fields.Integer('Previous Number of Months')
    dispose_price = fields.Float('Dispose Price')
    company_id = fields.Many2one('res.company', store=True, readonly=True, default=lambda self: self.env.company)
    company_currency_id = fields.Many2one(related='company_id.currency_id', string='Company Currency',
        readonly=True, store=True,
        help='Utility field to express amount currency')
    currency_id = fields.Many2one(related='asset_id.currency_id', string='Currency',
        readonly=True, store=True,
        help='Utility field to express amount currency')
    
    
    