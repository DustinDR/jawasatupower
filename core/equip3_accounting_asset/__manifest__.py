{
    'name': 'Accounting Asset',
    'version': '1.1.12',
    'category': 'Accounting',
    'author': 'Uchechukeu Umeevuruo',
    'depends': [
        'account',
        'stock',
        'purchase_stock',
        'sale',
        'stock_account',
        'sale_stock',
        'equip3_purchase_operation',
        'equip3_accounting_accessright_setting',
        # 'om_account_asset'
    ],
    'data': [
        'security/ir.model.access.csv',
        'wizard/asset_sale_wizard.xml',
        'views/account_asset_view.xml',
        'views/sale_view.xml',
        # 'account_asset_asset_report_test.xml',
        # 'views/purchase_view.xml',
        'views/product_view.xml',
    ],
    'demo': [],
    'auto_install': False,
    'installable': True,
    'application': False
}
