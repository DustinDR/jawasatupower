# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api, _
from odoo import tools
from odoo.exceptions import ValidationError
from datetime import date, datetime, timedelta
from pytz import timezone
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, float_compare
import time

class EmployeePerformance(models.Model):
    _inherit = 'employee.performance'

    is_period = fields.Boolean(string="is Period", default=False, compute='compute_is_period')
    state = fields.Selection(
        [('draft', 'Draft'), ('sent_to_employee', 'Sent To Employee'), ('sent_to_manager', 'Sent To Manager'),
         ('done', 'Approved'), ('cancel', 'Rejected')],
        string='Status', track_visibility='onchange', required=True,
        copy=False, default='draft')

    job_id = fields.Many2one('hr.job', related='employee_id.job_id')
    department_id = fields.Many2one('hr.department', related='employee_id.department_id')
    appraisal_approver_user_ids = fields.One2many('appraisal.approver.user', 'emp_appraisal_id', string='Approver')
    approvers_ids = fields.Many2many('res.users', 'emp_appraisal_approvers_rel', string='Approvers List')
    approved_user_ids = fields.Many2many('res.users', string='Approved by User')
    is_approver = fields.Boolean(string="Is Approver", compute='_compute_can_approve')
    approved_user_text = fields.Text(string="Approved User", tracking=True)
    approved_user = fields.Text(string="Approved User", tracking=True)
    feedback_parent = fields.Text(string='Parent Feedback')

    @api.depends('date_range_id')
    def compute_is_period(self):
        for rec in self:
            if rec.date_range_id:
                rec.is_period = True
            else:
                rec.is_period = False

    @api.onchange('employee_id')
    def onchange_approver_user(self):
        for appraisal in self:
            if appraisal.appraisal_approver_user_ids:
                remove = []
                for line in appraisal.appraisal_approver_user_ids:
                    remove.append((2, line.id))
                appraisal.appraisal_approver_user_ids = remove
            setting = self.env['ir.config_parameter'].sudo().get_param(
                'equip3_hr_employee_appraisals.appraisal_type_approval')
            if setting == 'employee_hierarchy':
                appraisal.appraisal_approver_user_ids = self.appraisal_emp_by_hierarchy(appraisal)
                self.app_list_appraisal_emp_by_hierarchy()
            if setting == 'approval_matrix':
                self.appraisal_approval_by_matrix(appraisal)

    def appraisal_emp_by_hierarchy(self, appraisal):
        approval_ids = []
        seq = 1
        data = 0
        line = self.get_manager(appraisal, appraisal.employee_id, data, approval_ids, seq)
        return line

    def get_manager(self, appraisal, employee_manager, data, approval_ids, seq):
        setting_level = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_employee_appraisals.appraisal_level')
        if not setting_level:
            raise ValidationError("Level not set")
        if not employee_manager['parent_id']['user_id']:
            return approval_ids
        while data < int(setting_level):
            approval_ids.append(
                (0, 0, {'user_ids': [(4, employee_manager['parent_id']['user_id']['id'])]}))
            data += 1
            seq += 1
            if employee_manager['parent_id']['user_id']['id']:
                self.get_manager(appraisal, employee_manager['parent_id'], data, approval_ids, seq)
                break
        return approval_ids

    def app_list_appraisal_emp_by_hierarchy(self):
        for appraisal in self:
            app_list = []
            for line in appraisal.appraisal_approver_user_ids:
                app_list.append(line.user_ids.id)
            appraisal.approvers_ids = app_list

    def appraisal_approval_by_matrix(self, appraisal):
        app_list = []
        approval_matrix = self.env['hr.appraisals.approval.matrix'].search([('apply_to', '=', 'by_employee')])
        matrix = approval_matrix.filtered(lambda line: appraisal.employee_id.id in line.employee_ids.ids)
        if matrix:
            data_approvers = []
            for line in matrix[0].approval_matrix_ids:
                data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                              'user_ids': [(6, 0, line.approvers.ids)]}))
                for approvers in line.approvers:
                    app_list.append(approvers.id)
            appraisal.approvers_ids = app_list
            appraisal.appraisal_approver_user_ids = data_approvers

        if not matrix:
            data_approvers = []
            approval_matrix = self.env['hr.appraisals.approval.matrix'].search([('apply_to', '=', 'by_job_position')])
            matrix = approval_matrix.filtered(lambda line: appraisal.job_id.id in line.job_ids.ids)
            if matrix:
                for line in matrix[0].approval_matrix_ids:
                    data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                                  'user_ids': [(6, 0, line.approvers.ids)]}))
                    for approvers in line.approvers:
                        app_list.append(approvers.id)
                appraisal.approvers_ids = app_list
                appraisal.appraisal_approver_user_ids = data_approvers
            if not matrix:
                data_approvers = []
                approval_matrix = self.env['hr.appraisals.approval.matrix'].search([('apply_to', '=', 'by_department')])
                matrix = approval_matrix.filtered(lambda line: appraisal.department_id.id in line.department_ids.ids)
                if matrix:
                    for line in matrix[0].approval_matrix_ids:
                        data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                                      'user_ids': [(6, 0, line.approvers.ids)]}))
                        for approvers in line.approvers:
                            app_list.append(approvers.id)
                    appraisal.approvers_ids = app_list
                    appraisal.appraisal_approver_user_ids = data_approvers

    @api.depends('state', 'employee_id')
    def _compute_can_approve(self):
        for appraisal in self:
            if appraisal.approvers_ids:
                setting = self.env['ir.config_parameter'].sudo().get_param(
                    'equip3_hr_employee_appraisals.appraisal_type_approval')
                setting_level = self.env['ir.config_parameter'].sudo().get_param(
                    'equip3_hr_employee_appraisals.appraisal_level')
                app_level = int(setting_level)
                current_user = appraisal.env.user
                if setting == 'employee_hierarchy':
                    matrix_line = sorted(appraisal.appraisal_approver_user_ids.filtered(lambda r: r.is_approve == True))
                    app = len(matrix_line)
                    a = len(appraisal.appraisal_approver_user_ids)
                    if app < app_level and app < a:
                        if current_user in appraisal.appraisal_approver_user_ids[app].user_ids:
                            appraisal.is_approver = True
                        else:
                            appraisal.is_approver = False
                    else:
                        appraisal.is_approver = False
                elif setting == 'approval_matrix':
                    matrix_line = sorted(appraisal.appraisal_approver_user_ids.filtered(lambda r: r.is_approve == True))
                    app = len(matrix_line)
                    a = len(appraisal.appraisal_approver_user_ids)
                    if app < a:
                        for line in appraisal.appraisal_approver_user_ids[app]:
                            if current_user in line.user_ids:
                                appraisal.is_approver = True
                            else:
                                appraisal.is_approver = False
                    else:
                        appraisal.is_approver = False

                else:
                    appraisal.is_approver = False
            else:
                appraisal.is_approver = False

    def action_done(self):
        for record in self:
            current_user = self.env.uid
            setting = self.env['ir.config_parameter'].sudo().get_param(
                'equip3_hr_employee_appraisals.appraisal_type_approval')
            now = datetime.now(timezone(self.env.user.tz))
            dateformat = f"{now.day}/{now.month}/{now.year} {now.hour}:{now.minute}:{now.second}"
            date_approved = time.strftime(DEFAULT_SERVER_DATE_FORMAT)
            date_approved_obj = datetime.strptime(date_approved, DEFAULT_SERVER_DATE_FORMAT)
            if setting == 'employee_hierarchy':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for user in record.appraisal_approver_user_ids:
                            if current_user == user.user_ids.id:
                                user.is_approve = True
                                user.timestamp = fields.Datetime.now()
                                user.approver_state = 'approved'
                                string_approval = []
                                if user.approval_status:
                                    string_approval.append(f"{self.env.user.name}:Approved")
                                    user.approval_status = "\n".join(string_approval)
                                    string_timestammp = [user.approved_time]
                                    string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                    user.approved_time = "\n".join(string_timestammp)
                                    if record.feedback_parent:
                                        feedback_list = [user.feedback,
                                                         f"{self.env.user.name}:{record.feedback_parent}"]
                                        final_feedback = "\n".join(feedback_list)
                                        user.feedback = f"{final_feedback}"
                                    elif user.feedback and not record.feedback_parent:
                                        user.feedback = user.feedback
                                    else:
                                        user.feedback = ""
                                else:
                                    user.approval_status = f"{self.env.user.name}:Approved"
                                    user.approved_time = f"{self.env.user.name}:{dateformat}"
                                    if record.feedback_parent:
                                        user.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                    else:
                                        user.feedback = ""
                                record.approved_user_ids = [(4, current_user)]
                        matrix_line = sorted(record.appraisal_approver_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            record.write({'state': 'done'})
                        else:
                            record.approved_user = self.env.user.name + ' ' + 'has been approved the Request!'
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved'
                    ))
            elif setting == 'approval_matrix':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for line in record.appraisal_approver_user_ids:
                            for user in line.user_ids:
                                if current_user == user.user_ids.id:
                                    line.timestamp = fields.Datetime.now()
                                    record.approved_user_ids = [(4, current_user)]
                                    var = len(line.approved_employee_ids) + 1
                                    if line.minimum_approver <= var:
                                        line.approver_state = 'approved'
                                        string_approval = []
                                        string_approval.append(line.approval_status)
                                        if line.approval_status:
                                            string_approval.append(f"{self.env.user.name}:Approved")
                                            line.approval_status = "\n".join(string_approval)
                                            string_timestammp = [line.approved_time]
                                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                            line.approved_time = "\n".join(string_timestammp)
                                            if record.feedback_parent:
                                                feedback_list = [line.feedback,
                                                                 f"{self.env.user.name}:{record.feedback_parent}"]
                                                final_feedback = "\n".join(feedback_list)
                                                line.feedback = f"{final_feedback}"
                                            elif line.feedback and not record.feedback_parent:
                                                line.feedback = line.feedback
                                            else:
                                                line.feedback = ""
                                        else:
                                            line.approval_status = f"{self.env.user.name}:Approved"
                                            line.approved_time = f"{self.env.user.name}:{dateformat}"
                                            if record.feedback_parent:
                                                line.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                            else:
                                                line.feedback = ""
                                        line.is_approve = True
                                    else:
                                        line.approver_state = 'pending'
                                        if line.approval_status:
                                            string_approval.append(f"{self.env.user.name}:Approved")
                                            line.approval_status = "\n".join(string_approval)
                                            string_timestammp = [line.approved_time]
                                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                            line.approved_time = "\n".join(string_timestammp)
                                            if record.feedback_parent:
                                                feedback_list = [line.feedback,
                                                                 f"{self.env.user.name}:{record.feedback_parent}"]
                                                final_feedback = "\n".join(feedback_list)
                                                line.feedback = f"{final_feedback}"
                                            elif line.feedback and not record.feedback_parent:
                                                line.feedback = line.feedback
                                            else:
                                                line.feedback = ""
                                        else:
                                            line.approval_status = f"{self.env.user.name}:Approved"
                                            line.approved_time = f"{self.env.user.name}:{dateformat}"
                                            if record.feedback_parent:
                                                line.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                            else:
                                                line.feedback = ""
                                    line.approved_employee_ids = [(4, current_user)]

                        matrix_line = sorted(record.appraisal_approver_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Request!'
                            record.write({'state': 'done'})
                        else:
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Request!'
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved!'
                    ))
            else:
                raise ValidationError(_(
                    'Already approved!'
                ))

    def action_cancel(self):
        for record in self:
            for user in record.appraisal_approver_user_ids:
                for check_user in user.user_ids:
                    now = datetime.now(timezone(self.env.user.tz))
                    dateformat = f"{now.day}/{now.month}/{now.year} {now.hour}:{now.minute}:{now.second}"
                    if self.env.uid == check_user.id:
                        user.timestamp = fields.Datetime.now()
                        user.approver_state = 'refuse'
                        string_approval = []
                        string_approval.append(user.approval_status)
                        if user.approval_status:
                            string_approval.append(f"{self.env.user.name}:Refused")
                            user.approval_status = "\n".join(string_approval)
                            string_timestammp = [user.approved_time]
                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                            user.approved_time = "\n".join(string_timestammp)
                        else:
                            user.approval_status = f"{self.env.user.name}:Refused"
                            user.approved_time = f"{self.env.user.name}:{dateformat}"
            record.approved_user = self.env.user.name + ' ' + 'has been Rejected!'
            record.write({'state': 'cancel'})

    def wizard_approve(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'hr.appraisal.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'name': "Confirmation Message",
            'target': 'new',
        }

class AppraisalApproverUser(models.Model):
    _name = 'appraisal.approver.user'

    emp_appraisal_id = fields.Many2one('employee.performance', string="Employee appraisal Id")
    name = fields.Integer('Sequence', compute="fetch_sl_no")
    user_ids = fields.Many2many('res.users', string="Approvers")
    approved_employee_ids = fields.Many2many('res.users', 'emp_appraisal_user_ids', string="Approved user")
    minimum_approver = fields.Integer(string="Minimum Approver", default=1)
    timestamp = fields.Datetime(string="Timestamp")
    approved_time = fields.Text(string="Timestamp")
    feedback = fields.Text()
    approver_state = fields.Selection([('draft', 'Draft'), ('pending', 'Pending'), ('approved', 'Approved'),
                                       ('refuse', 'Refused')], default='draft', string="Approval Status")
    approval_status = fields.Text()
    is_approve = fields.Boolean(string="Is Approve", default=False)

    @api.depends('emp_appraisal_id')
    def fetch_sl_no(self):
        sl = 0
        for line in self.emp_appraisal_id.appraisal_approver_user_ids:
            sl = sl + 1
            line.name = sl

class HrAppraisalAnalysis(models.Model):
    _name = "hr.appraisal.analysis"
    _description = "Appraisal Report"
    _auto = False

    performance_id = fields.Many2one('employee.performance', 'Performance', required=True)
    employee_id = fields.Many2one('hr.employee', 'Employee', required=True)
    key_performance_id = fields.Many2one('key.competencies', 'Competencies')
    key_performance_area_id = fields.Many2one('key.performance', 'Key Performance Area')
    employee_self_assessment = fields.Integer('Employee Self Assessment')
    manager_rate = fields.Integer('Manager Rating')

    def _select(self):
        return """
            SELECT
                performance.id AS id,   
                performance.id AS key_performance_id,                             
                ep.id AS performance_id,
                employee.id AS employee_id,  
                performance_area.id As key_performance_area_id,
                performance_area.employee_rate As employee_self_assessment,
                performance_area.manager_rate As manager_rate
            """

    def _from(self):
        return """
            FROM
                employee_performance ep
                JOIN key_competencies performance ON ep.id = performance.performance_id
                JOIN hr_employee employee ON ep.employee_id = employee.id
                JOIN key_performance performance_area ON ep.id = performance_area.performance_id
            """

    def _group_by(self):
        return """
            GROUP BY
                performance.id,                            
                employee.id,
                ep.id,
                performance_area.id
            """

    def _order_by(self):
        return """
            ORDER BY
                employee_id
            """

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute(
            "CREATE or REPLACE VIEW %s as (%s %s %s %s)" % (
                self._table, self._select(), self._from(), self._group_by(), self._order_by()
            )
        )