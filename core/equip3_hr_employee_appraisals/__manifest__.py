# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Equip3 Appraisal Extend',
    'version': '1.1.6',
    'author': 'Hashmicro / Arivarasan',
    'category': 'Human Resources',
    'summary': """
    Employee Performance Evaluation.
    """,
    'depends': ['employee_performance','equip3_hr_basic_custom_menu'],
    'data': [
        'security/ir.model.access.csv',
        'wizard/appraisal_approve_wizard.xml',
        'views/appraisal_view.xml',
        'views/res_config_settings.xml',
        'views/hr_appraisals_approval_matrix.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
