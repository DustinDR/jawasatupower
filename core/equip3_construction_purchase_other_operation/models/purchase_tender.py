from dataclasses import field
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class PurchaseTender(models.Model):
    _inherit = 'purchase.agreement'

    project = fields.Many2one('project.project', 'Project')
    cost_sheet = fields.Many2one('job.cost.sheet', 'Cost Sheet')
    sub_contracting = fields.Selection([('main_contract', 'Main Contract'), ('addendum', 'Addendum')],
                                       string="Sub Contracting", required=True, default='main_contract')
    po_tender_count = fields.Integer("Purchase Tender Count",compute="_compute_purchase_tender_count")
    is_subcontracting = fields.Boolean('Is Subcontracting', default=False)
    is_material_orders = fields.Boolean('Is Material Orders', default=False)
    is_orders = fields.Boolean('Is Orders', default=False)
    material_order = fields.Boolean('Material Order')

    variable_line_ids = fields.One2many('pt.variable.line', 'variable_id', string='Variable Line')
    material_line_ids = fields.One2many('material.line', 'material_id', string='Material Line')
    service_line_ids = fields.One2many('service.line', 'service_id', string='Service Line')
    equipment_line_ids = fields.One2many('pa.equipment.line', 'equipment_id', string='Equipment Line')
    labour_line_ids = fields.One2many('labour.line', 'labour_id', string='Labour Line')
    overhead_line_ids = fields.One2many('overhead.line', 'overhead_id', string='Overhead Line')

    @api.onchange('variable_line_ids', 'variable_line_ids.variable_id')
    def _get_material_line(self):
        material = [(5, 0, 0)]
        services = [(5, 0, 0)]
        equipment = [(5, 0, 0)]
        labour = [(5, 0, 0)]
        overhead = [(5, 0, 0)]
        for rec in self.variable_line_ids:
            for mat in rec.variable.material_variable_ids:
                res = (0, 0, {
                    'project_scope': rec.project_scope.id,
                    'section': rec.section.id,
                    'variable': rec.variable.id,
                    'product': mat.product_id.id,
                    'description': mat.description,
                    'quantity': rec.quantity * mat.quantity,
                    'analytic_tag_ids': self.account_tag_ids,
                    'uom': mat.uom_id.id,
                    'destination_warehouse': self.destination_warehouse_id
                })
                material.append(res)

            for serv in rec.variable.service_variable_ids:
                res = (0, 0, {
                    'project_scope': rec.project_scope.id,
                    'section': rec.section.id,
                    'variable': rec.variable.id,
                    'product': serv.product_id.id,
                    'description': serv.description,
                    'quantity': rec.quantity * serv.quantity,
                    'analytic_tag_ids': self.account_tag_ids,
                    'uom': serv.uom_id.id,
                    'destination_warehouse': self.destination_warehouse_id
                })
                services.append(res)

            for equi in rec.variable.equipment_variable_ids:
                res = (0, 0, {
                    'project_scope': rec.project_scope.id,
                    'section': rec.section.id,
                    'variable': rec.variable.id,
                    'product': equi.product_id.id,
                    'description': equi.description,
                    'quantity': rec.quantity * equi.quantity,
                    'analytic_tag_ids': self.account_tag_ids,
                    'uom': equi.uom_id.id,
                    'destination_warehouse': self.destination_warehouse_id
                })
                equipment.append(res)

            for lab in rec.variable.labour_variable_ids:
                res = (0, 0, {
                    'project_scope': rec.project_scope.id,
                    'section': rec.section.id,
                    'variable': rec.variable.id,
                    'product': lab.product_id.id,
                    'description': lab.description,
                    'quantity': rec.quantity * lab.quantity,
                    'analytic_tag_ids': self.account_tag_ids,
                    'uom': lab.uom_id.id,
                    'destination_warehouse': self.destination_warehouse_id
                })
                labour.append(res)

            for over in rec.variable.overhead_variable_ids:
                res = (0, 0, {
                    'project_scope': rec.project_scope.id,
                    'section': rec.section.id,
                    'variable': rec.variable.id,
                    'product': over.product_id.id,
                    'description': over.description,
                    'quantity': rec.quantity * over.quantity,
                    'analytic_tag_ids': self.account_tag_ids,
                    'uom': over.uom_id.id,
                    'destination_warehouse': self.destination_warehouse_id
                })
                overhead.append(res)

        self.material_line_ids = material
        self.service_line_ids = services
        self.equipment_line_ids = equipment
        self.labour_line_ids = labour
        self.overhead_line_ids = overhead

    @api.onchange('is_subcontracting')
    def _onchange_is_subcontracting(self):
        context = dict(self.env.context) or {}
        if context.get('services_good'):
            self.is_subcontracting = True

    @api.onchange('is_material_orders')
    def _onchange_is_material_orders(self):
        context = dict(self.env.context) or {}
        if context.get('goods_order'):
            self.is_material_orders = True

    @api.onchange('is_orders')
    def _onchange_is_orders(self):
        context = dict(self.env.context) or {}
        if context.get('orders'):
            self.is_orders = True

    @api.onchange('material_order')
    def _onchange_material_order(self):
        context = dict(self.env.context) or {}
        if context.get('goods_order'):
            self.material_order = True

    def _compute_purchase_tender_count(self):
        print(';:::::::::::::::::------------:::::::::::::::::',self.search_count([('material_order','=',True)]))
        purchase_agreement_obj = self.env['purchase.agreement']
        for task in self:
            po_count = purchase_agreement_obj.search_count([('sh_source','=',task.name)])
            print("==============",po_count)
            if po_count:
                task.po_tender_count = po_count
            else:
                task.po_tender_count = 0

    def action_material_tender(self):
        print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> RRRRRR >>>>>>>>>>>>>>>>>>>>>>')

    def split_purchase_agreement(self):
        """
        showing Split Purchase Agreement wizard and
        raise error if Received Quotation=0
        """
        for rec in self:
            if rec.rfq_count >= 0:
                return {    # Return Wizard Action calls if condition is True
                    'name': _('Split Purchase Agreement'),
                    'type': 'ir.actions.act_window',
                    'res_model': 'wizard.purchase.agreement',
                    'view_id': self.env.ref(
                        'equip3_construction_purchase_other_operation.wizard_split_purchase_agreement_form').id,
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'new',
                }


        vals = self.agreement_id
        res = self.env['purchase.agreement'].browse(vals.id)
        if self.split_table == "material":
            for rec in self:
                rec.material_line_ids = [(5, 0, 0)]
    
    def action_confirm(self):
        res = super(PurchaseTender, self).action_confirm()
        for rec in self.variable_line_ids:
            if rec.quantity > rec.budget_qty:
                 raise ValidationError(_("The quantity is over the remaining budget"))
            elif rec.reference_price > rec.budget_amount:
                 raise ValidationError(_("The reference amount is over the unit price budget"))
        return res


class VariableLine(models.Model):
    _name = 'pt.variable.line'
    _description = "Variable Line"
    _order = "sequence"

    sequence = fields.Integer('Sequence', default=1)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    cs_subcon_id = fields.Many2one('material.subcon', 'Subcon ID')
    variable_id = fields.Many2one('purchase.agreement', 'Variable ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable', required=True)
    quantity = fields.Float('Quantity')
    budget_qty = fields.Float('Budget Quantity')
    budget_amount = fields.Float('Budget Unit Price')
    uom = fields.Many2one('uom.uom', string='UoM', readonly=True)
    reference_price = fields.Float('Reference Price')

    @api.onchange('variable')
    def onchange_variable(self):
        res = {}
        if not self.variable:
            return res
        self.uom = self.variable.variable_uom.id
        self.quantity = 1.0

    @api.depends('variable_id.variable_line_ids', 'variable_id.variable_line_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.variable_id.variable_line_ids:
                no += 1
                l.sr_no = no

    @api.model
    def create(self, vals):
        vals['sr_no'] = self.env['ir.sequence'].next_by_code('variable_product') or ('New')
        res = super(VariableLine, self).create(vals)
        return res


class PurchaseTenderLine(models.Model):
    _inherit = 'purchase.agreement.line'

    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
    budget_quantity = fields.Float('Budget Quantity')

