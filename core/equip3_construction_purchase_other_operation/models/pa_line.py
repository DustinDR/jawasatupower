from dataclasses import field
from odoo import models, fields, api


class PurchaseTender(models.Model):
    _inherit = 'purchase.agreement'


    #equipment_line_ids = fields.One2many('pa.equipment.line', 'equipment_id', string='Equipment Line')
    #labour_line_ids = fields.One2many('labour.line', 'labour_id', string='Labour Line')
    #overhead_line_ids = fields.One2many('overhead.line', 'overhead_id', string='Overhead Line')


class PAEquipmentLine(models.Model):
    _name = 'pa.equipment.line'
    _description = "Equipment Line"
    _order = "sequence"

    sequence = fields.Integer('Sequence', default=1)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    equipment_id = fields.Many2one('purchase.agreement', 'Equipment ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product', compute='compute_groupofproduct', readonly=True)
    product = fields.Many2one('product.product', 'Product', required=True)
    description = fields.Char('Custom Description')
    quantity = fields.Float('Quantity')
    budget_quantity = fields.Float('Budget Quantity')
    ordered_quantity = fields.Float('Ordered Quantity', readonly=True)
    uom = fields.Many2one('uom.uom', 'UoM', readonly=True)
    destination_warehouse = fields.Many2one('stock.warehouse', 'Destination Warehouse', required=True)
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Group')
    schedule_date = fields.Date('Scheduled Date')
    reference_price = fields.Float('Reference Price')
    company_id = fields.Many2one('res.company', related='equipment_id.company_id',
                                 string='Company', store=True, readonly=True, default=lambda self: self.env.company)

    @api.onchange('product')
    def onchange_product(self):
        res = {}
        if not self.product:
            return res
        self.ordered_quantity = 0
        self.quantity = 1.0
        self.uom = self.product.uom_id.id
    
    @api.onchange('product')
    def compute_groupofproduct(self):
        if self.product:
            self.group_of_product = self.product.group_of_product.id

    @api.depends('equipment_id.equipment_line_ids', 'equipment_id.equipment_line_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.equipment_id.equipment_line_ids:
                no += 1
                l.sr_no = no

    @api.model
    def create(self, vals):
        vals['sr_no'] = self.env['ir.sequence'].next_by_code('pa_equipment_request') or ('New')
        res = super(PAEquipmentLine, self).create(vals)
        return res


class LabourLine(models.Model):
    _name = 'labour.line'
    _description = "Labour Line"
    _order = "sequence"

    sequence = fields.Integer('Sequence', default=1)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    labour_id = fields.Many2one('purchase.agreement', 'Labour ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product', compute='compute_groupofproduct', readonly=True)
    product = fields.Many2one('product.product', 'Product', required=True)
    description = fields.Char('Custom Description')
    quantity = fields.Float('Quantity')
    budget_quantity = fields.Float('Budget Quantity')
    ordered_quantity = fields.Float('Ordered Quantity', readonly=True)
    uom = fields.Many2one('uom.uom', 'UoM', readonly=True)
    destination_warehouse = fields.Many2one('stock.warehouse', 'Destination Warehouse', required=True)
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Group')
    schedule_date = fields.Date('Scheduled Date')
    reference_price = fields.Float('Reference Price')
    company_id = fields.Many2one('res.company', related='labour_id.company_id',
                                 string='Company', store=True, readonly=True, default=lambda self: self.env.company)

    @api.onchange('product')
    def onchange_product(self):
        res = {}
        if not self.product:
            return res
        self.ordered_quantity = 0
        self.quantity = 1.0
        self.uom = self.product.uom_id.id
    
    @api.onchange('product')
    def compute_groupofproduct(self):
        if self.product:
            self.group_of_product = self.product.group_of_product.id

    @api.depends('labour_id.labour_line_ids', 'labour_id.labour_line_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.labour_id.labour_line_ids:
                no += 1
                l.sr_no = no

    @api.model
    def create(self, vals):
        vals['sr_no'] = self.env['ir.sequence'].next_by_code('labour_request') or ('New')
        res = super(LabourLine, self).create(vals)
        return res


class OverheadLine(models.Model):
    _name = 'overhead.line'
    _description = "Overhead Line"
    _order = "sequence"

    sequence = fields.Integer('Sequence', default=1)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    overhead_id = fields.Many2one('purchase.agreement', 'Overhead ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product', compute='compute_groupofproduct', readonly=True)
    product = fields.Many2one('product.product', 'Product', required=True)
    description = fields.Char('Custom Description')
    quantity = fields.Float('Quantity')
    budget_quantity = fields.Float('Budget Quantity')
    ordered_quantity = fields.Float('Ordered Quantity', readonly=True)
    uom = fields.Many2one('uom.uom', 'UoM', readonly=True)
    destination_warehouse = fields.Many2one('stock.warehouse', 'Destination Warehouse', required=True)
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Group')
    schedule_date = fields.Date('Scheduled Date')
    reference_price = fields.Float('Reference Price')
    company_id = fields.Many2one('res.company', related='overhead_id.company_id',
                                 string='Company', store=True, readonly=True, default=lambda self: self.env.company)

    @api.onchange('product')
    def onchange_product(self):
        res = {}
        if not self.product:
            return res
        self.ordered_quantity = 0
        self.quantity = 1.0
        self.uom = self.product.uom_id.id

    @api.onchange('product')
    def compute_groupofproduct(self):
        if self.product:
            self.group_of_product = self.product.group_of_product.id

    @api.depends('overhead_id.overhead_line_ids', 'overhead_id.overhead_line_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.overhead_id.overhead_line_ids:
                no += 1
                l.sr_no = no

    @api.model
    def create(self, vals):
        vals['sr_no'] = self.env['ir.sequence'].next_by_code('overhead_request') or ('New')
        res = super(OverheadLine, self).create(vals)
        return res