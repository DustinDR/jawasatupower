from odoo import models, fields, api


class PurchaseRequest(models.Model):
    _inherit = 'purchase.request'

    def create_purchase_agreement(self):

        data = []
        for record in self:
            record.purchase_req_state = 'in_progress'
            for line_id in record.variable_line_ids:
                for ref in line_id.cs_subcon_id:
                    data.append((0, 0, {'cs_subcon_id': line_id.cs_subcon_id.id,
                                        'project_scope': line_id.project_scope.id,
                                        'section': line_id.section.id,
                                        'variable': line_id.variable.id,
                                        'quantity': line_id.quantity,
                                        'budget_qty': line_id.quantity,
                                        'uom': line_id.uom.id,
                                        'budget_amount': ref.price_unit
                }))
            purchase_tender = self.env['purchase.agreement'].create({
                    'sh_purchase_user_id' : record.requested_by.id,
                    'sh_source' : record.name,
                    'company_id' : record.company_id.id,
                    'variable_line_ids' : data,
                    'purchase_request_id' : record.id,
                    'project': record.project.id,
                    'cost_sheet': record.cost_sheet.id,
                    'account_tag_ids': record.analytic_account_group_ids.ids,
                    'is_subcontracting':True,
            })
            record.line_ids.write({'agreement_id': purchase_tender})
            return {
                'type': 'ir.actions.act_window',
                'name': 'Purchaes Tender',
                'view_mode': 'form',
                'res_model': 'purchase.agreement',
                'res_id' : purchase_tender.id,
                'target': 'current'
            }


class PurchaseRequestLine(models.Model):
    _inherit = 'purchase.request.line'


    @api.model   
    def create_purchase_agreement(self):
        return