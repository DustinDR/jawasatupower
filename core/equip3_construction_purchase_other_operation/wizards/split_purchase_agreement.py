from odoo import api, fields, models, _


# class PurchaseAgreementInherit(models.TransientModel):
#     _inherit = "purchase.agreement"
#
#     @api.one
#     def copy(self, default=None):
#         default = dict(default or {})
#         default.update({
#             'name': "sddsdsd",
#         })
#         return super(PurchaseAgreementInherit, self).copy(default)


class WizardPurchaseAgreement(models.TransientModel):
    _name = "wizard.purchase.agreement"

    agreement_id = fields.Many2one('purchase.agreement', string="Tender")
    rfq_id = fields.Many2one('purchase.order', string="RFQ")
    partner_ids = fields.Many2many('res.partner','partner_agreement','agreementid','partnerid', string='Vendors')
    temp_current_vendors = fields.Many2many('res.partner', string="Temp Current Vendors")
    split_table = fields.Selection([
        ('service', 'Service'),
        ('material', 'Material')
    ], string='Split Table', default='material')

    # @api.onchange('split_table')
    # def update_material_line_id(self):
    #     vals = self.agreement_id
    #     res = self.env['purchase.agreement'].browse(vals.id)
    #     if self.split_table == "material":
    #         for rec in res:
    #             rec.material_line_ids = [(5,0,0)]

    # call onclick split purchase agreement button
    # update record
    def submit_split(self):
        vals = self.agreement_id
        res = self.env['purchase.agreement'].browse(vals.id)
        new_record = res.copy()
        variables_lines = []
        material_lines = []

        if self.split_table != 'material':
            vals['partner_ids'] = self.partner_ids
        
        for rec in vals.variable_line_ids:
            new_variable_line = rec.copy()
            print("============ new_variable_line",new_variable_line)
            variables_lines.append(new_variable_line.id)
        for rec in vals.material_line_ids:
            new_material_line = rec.copy()
            print("============ new_material_line", new_material_line)
            material_lines.append(new_material_line.id)

        new_record.write({
            'is_goods_orders': True,
            'material_order': True,
            'is_subcontracting': False,
            'partner_ids': self.partner_ids,
            'sh_source': vals.name,
            'variable_line_ids': variables_lines,
            'material_line_ids': material_lines
        })

        self.material_line_ids = [(5,0,0)]
