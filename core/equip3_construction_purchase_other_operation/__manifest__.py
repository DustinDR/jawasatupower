# -*- coding: utf-8 -*-
{
    'name': "Equip3 Construction Purchase Other Operation",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "HashMicro",
    'website': "http://www.hashmicro.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '1.1.3',

    # any module necessary for this one to work correctly
    'depends': ['base', 'equip3_purchase_other_operation', 'equip3_construction_sales_operation', 'product', 'equip3_construction_operation'],

    # always loaded
    'data': [
        'wizards/split_purchase_agreement.xml',
        'security/ir.model.access.csv',
        'views/purchase_tender_view.xml',
        'views/variable_material_line_view.xml',
        'views/service_line_view.xml',
        'views/material_order_menu_view.xml',
        'views/subcontracting_menu_view.xml',
        'views/orders_menu_purchase_tender.xml',
        'views/agreement_tender_line_assets.xml',
        'views/pa_line_view.xml',
        'views/purchase_request_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
