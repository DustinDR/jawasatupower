from odoo import api, models, fields, _


class DeliveryBoyLog(models.Model):
    _name = "delivery.boy.log"
    _rec_name = "driver_id"

    driver_id = fields.Many2one("res.users",string="Driver",domain=lambda self: [('partner_id.is_driver', '=', True)])
    in_out_time = fields.Datetime(string="In time out time")
    state = fields.Selection([('log_in','Log In'),('log_out','Log Out')],string="State")

    @api.model
    def create(self,vals):
        res = super(DeliveryBoyLog, self).create(vals)
        delivery_boy_status = self.env['delivery.boy.status'].search([('driver_id','=',res.driver_id.id)])
        if res.state == 'log_in':
            delivery_boy_status.state = 'online'
        else:
            delivery_boy_status.state = 'offline'
        return res
