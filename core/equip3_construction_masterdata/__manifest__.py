# -*- coding: utf-8 -*-
{
    'name': "Equip3 Construction Masterdata",

    'summary': """
        Manage your Master Data""",

    'description': """
        Manage your Master Data
    """,

    'author': "Hashmicro",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': '',
    'version': '1.1.24',

    # any module necessary for this one to work correctly
    'depends': ['stock', 'base', 'product', 'project', 'abs_construction_management', 'equip3_accounting_analytical', 'ks_gantt_view_base', 'equip3_purchase_masterdata'],

    # always loaded
    'data': [
        'security/project_security.xml',
        'security/ir.model.access.csv',
        'views/menu_icon.xml',
        'views/product_template_view.xml',
        'views/res_partner_view.xml',
        'views/project_view.xml',
        'views/variable_view.xml',
        'views/guarantee_table_view.xml',
        'views/product_category_views.xml',
        'views/purchase_action_view.xml',
        'views/addendum_line_view.xml',
        'views/line_assets.xml',
        'data/sequence.xml',
        'views/inherited_res_users.xml',
        'views/group_of_product_view.xml',
        'views/budget_period_view.xml',
    ],
    # only loaded in demonstration mode
    # 'demo': [
    #     'demo/demo.xml',
    # ],
}
