# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import ValidationError, _logger


class ProjectProject(models.Model):
    _inherit = 'project.project'

    @api.constrains('name')
    def _check_existing_record(self):
        for record in self:
            name_id = self.env['project.project'].search(
                [('name', '=', record.name)])
            if len(name_id) > 1:
                raise ValidationError(
                    f'The Project name already exists. Please change the Project name.')
    
    project_id = fields.Char(string="Project ID", readonly=True, required=True, copy=False, default='New')
    created_date = fields.Date("Creation Date", default=fields.Date.today, readonly=True)
    created_by = fields.Many2one("res.users", string="Created By", default=lambda self: self.env.uid, readonly=True)
    payment_states = fields.Selection([
        ('settled', 'Settled'),
        ('ongoing', 'Ongoing'),
        ('late', 'Late'),
    ], string='Payment States')
    branch_id = fields.Many2one('res.branch', string='Branch')
    department_type = fields.Selection([
        ('department', 'Department'),
        ('project', 'Project'),
    ], string='Type of Department')
    project_scope_line_ids = fields.One2many('project.scope.line', 'scope_id')
    customer_ref = fields.Char(string="Customer Reference")
    section_ids = fields.One2many('section.line', 'section_id')
    stage_ids = fields.One2many('project.stage', 'stage_id')

    # Project Milestone
    contract_categ = fields.Selection([
                        ('main', 'Main Contract'),
                        ('var', 'Variation Order')
                        ],string="Contract Category", readonly=True, default='main')
    contract_amount = fields.Float(string='Contract Amount', required=True)
    dp_method = fields.Selection([
                        ('fix', 'Fixed'),
                        ('per', 'Percentage')
                        ],string="Down Payment Method", default='fix')
    dp_amount = fields.Float(string="Down Payment Amount")
    down_payment = fields.Float(string="Down Payment", compute="_compute_total_downpayment", required=True)
    retention1 = fields.Float(string='Retention 1', required=True)
    retention1_date = fields.Date(string='Retention 1 Date', tracking=True)
    retention2 = fields.Float(string='Retention 2', required=True)
    retention2_date = fields.Date(string='Retention 2 Date', tracking=True)
    tax_id = fields.Many2many('account.tax', string='Taxes', domain=['|', ('active', '=', False), ('active', '=', True)])
    start_date = fields.Date(string='Start Date', required=True)
    end_date = fields.Date(string='End Date')
    payment_term = fields.Many2one('account.payment.term', string='Payment Term')
    outstanding_limit = fields.Float(string='Outstanding Limit')
    status_progress = fields.Float(string='Status Progress')
    full_stage_weightage = fields.Float(string="Stage Full", compute="_compute_full_stage") 

    # Adds New Fields on Master Data Table (Construction Management > Projects > Projects )
    project_director = fields.Many2one('res.users', string='Project Director', default=lambda self: self.env.user)
    project_coordinator = fields.Many2one('res.users', string='Project Coordinator')
    project_team_ids = fields.Many2many(relation='project_associated_rel', comodel_name='res.users',
                                        column1='project_id', column2='associated_id', default=lambda self: self.env.user)
    project_team_user = fields.Many2one('res.users', string='Project Team User')
    sales_team = fields.Many2one('crm.team', string='Sales Team')
    sales_person = fields.Many2one('res.users', string='Sales Person')
    associated_user_ids = fields.Many2many(relation='project_associated_rel', comodel_name='res.users',
                                           column1='project_id', column2='associated_id', default=lambda self: self.env.user)
    warehouse_address = fields.Many2one('stock.warehouse', string='Warehouse Address')
    # adding field to the project.project for showing the current user assigned projects
    analytic_idz = fields.Many2many('account.analytic.tag', string='Analytic Group', domain="[('company_id', '=', company_id)]")
    show_own_records = fields.Char(string="Own Record", compute='_get_own_project_offer',
                                   search='_search_own_project_offer')
    total_job_estimate = fields.Integer(string="Job Estimate",compute='_comute_job_estimate')
    total_sale_order = fields.Integer(string="Sales Order",compute='_comute_sales_orders')
    budgeting_method = fields.Selection([
        ('product_budget', 'Based on Product Budget'),
        ('total_budget', 'Based on Total Budget'),], string='Budgeting Method', default='product_budget')

    @api.depends('dp_method', 'dp_amount', 'contract_amount')
    def _compute_total_downpayment(self):
        for res in self:
            if res.dp_method == 'fix':
                res.down_payment = res.dp_amount
            else:
                res.down_payment = res.contract_amount * (res.dp_amount / 100)

    def _get_own_project_offer(self):
        _logger.info("user can show only his projects")

    def _search_own_project_offer(self, operator, value):
        user_pool = self.env['res.users']
        user = user_pool.browse(self._uid)
        project_ids = user.project_ids
        if project_ids:
            return [('id', 'in', project_ids.ids)]
        else:
            return [('id', '=', -1)]
    # Add different states in project model for business needs

    primary_states = fields.Selection([
        ('draft', 'Draft'),
        ('progress', 'In Progress'),
        ('suspended', 'Suspended'),
        ('cancelled', 'Cancelled'),
        ('completed', 'Completed'),
    ], string='Primary States', default='draft')

    def _comute_sales_orders(self):
        for order in self:
            order_count = self.env['sale.order.const'].search_count([('project_id', '=', self.id), ('state', 'in', ('sale','done'))])
            order.total_sale_order = order_count

    def action_sale_order(self):
        return {
            'name': ("Sales Order"),
            'view_mode': 'tree,form',
            'res_model': 'sale.order.const',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('project_id', '=', self.id), ('state', 'in', ('sale','done'))],
        }

    def _comute_job_estimate(self):
        for job in self:
            job_count = self.env['job.estimate'].search_count([('project_id', '=', self.id), ('state', '=', 'sale')])
            job.total_job_estimate = job_count

    def action_job_estimate(self):
        return {
            'name': ("Job Estimates"),
            'view_mode': 'tree,form',
            'res_model': 'job.estimate',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('project_id', '=', self.id), ('state', '=', 'sale')],
        }
    
    def button_verify(self):
        """
        Set state to verified for current project
        """
        for rec in self:
            if rec.cost_sheet_count >= 1:
                rec.primary_states = 'progress'
            else:
                raise ValidationError("cost sheet is not created")

    def button_suspend(self):
        """
        Set state to suspended  for current project
        """
        for rec in self:
            rec.primary_states = 'suspended'

    def button_complete(self):
        """
        Set state to complete for current project
        """
        for rec in self:
            rec.primary_states = 'completed'

    def button_continue(self):
        """
        Set state to continue for current project
        """
        for rec in self:
            rec.primary_states = 'progress'

    def button_cancel(self):
        """
        Set state to cancel for current project
        """
        for rec in self:
            rec.primary_states = 'cancelled'

    def button_rest_draft(self):
        """
        Set state to draft for current project
        """
        for rec in self:
            rec.primary_states = 'draft'

    @api.model
    def create(self, vals):
        if vals.get('project_id', 'New') == 'New':
            vals['project_id'] = self.env['ir.sequence'].next_by_code(
                'project.seq') or 'New'
        result = super(ProjectProject, self).create(vals)
        return result

    @api.depends('stage_ids.stage_weightage')
    def _compute_full_stage(self):
        for res in self:
            res.full_stage_weightage = sum(res.stage_ids.mapped('stage_weightage'))

    @api.onchange('full_stage_weightage')
    def onchange_stage_weightage(self):
        if self.full_stage_weightage > 100:
            raise ValidationError(_("The total of stage weightage is more than 100%.\nPlease, re-set the weightage of each stage."))

    @api.constrains('project_scope_line_ids')
    def _check_exist_project_scope(self):
        for scope in self:
            exist_scope_list = []
            for line in scope.project_scope_line_ids:
                if line.name in exist_scope_list:
                    raise ValidationError(_('The Project Scope "%s" already exists. Please change the Project Scope "%s" (must be unique).'%((line.name),(line.name))))
                exist_scope_list.append(line.name)

    @api.constrains('section_ids')
    def _check_exist_section(self):
        for section in self:
            exist_section_list = []
            for line in section.section_ids:
                if line.name in exist_section_list:
                    raise ValidationError(_('The Section "%s" already exists. Please change the Section "%s" (must be unique).'%((line.name),(line.name))))
                exist_section_list.append(line.name)

class ProjectScopeLine(models.Model):
    _name = "project.scope.line"
    _description = "Project Scope Line"
    _order = "sequence" 

    scope_id = fields.Many2one('project.project', string="Project")
    sequence = fields.Integer(string="Sequence", default=1)
    name = fields.Char(string="Project Scope")
    scope_description = fields.Text(string='Description')
    project_id = fields.Many2one('project.project')
    sr_no = fields.Integer(string="No.", compute="_sequence_ref")

    @api.depends('scope_id.project_scope_line_ids', 'scope_id.project_scope_line_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.scope_id.project_scope_line_ids:
                no += 1
                l.sr_no = no


class SectionLine(models.Model):
    _name = "section.line"
    _description = "Section"
    _order = "sequence"

    section_id = fields.Many2one('project.project', string="Project")
    sequence = fields.Integer(string="Sequence", default=1)
    project_scope = fields.Many2one('project.scope.line', string='Project Scope')
    name = fields.Char(string='Section')
    description = fields.Text(string='Description')
    sr_no = fields.Integer(string="No.", compute="_sequence_ref")
    project_id = fields.Many2one('project.project', string='Project')
    project_scope_computed = fields.Many2many('project.scope.line', string='Project Scope', compute='get_project_scopes')

    
    @api.depends('section_id.section_ids', 'section_id.section_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.section_id.section_ids:
                no += 1
                l.sr_no = no

    @api.depends('section_id.project_scope_line_ids')
    def get_project_scopes(self):
        for rec in self:
            if rec.section_id.project_scope_line_ids:
                rec.project_scope_computed = [(6,0,rec.section_id.project_scope_line_ids.ids)]
            else:
                rec.project_scope_computed = [(6,0,[])]

class ProjectStage(models.Model):
    _name = "project.stage"
    _description = "Stage"
    _order = "sequence"

    @api.depends('stage_id.stage_ids', 'stage_id.stage_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.stage_id.stage_ids:
                no += 1
                l.sr_no = no

    stage_id = fields.Many2one('project.project', string="Project")
    sequence = fields.Integer(string="Sequence", default=1)
    sr_no = fields.Integer(string="Sequence", compute="_sequence_ref")
    name = fields.Many2one('project.task.type', string="Stage")
    stage_weightage = fields.Float(string="Stage Weightage (%)")
    project_id = fields.Char(related='stage_id.name', string='Project')