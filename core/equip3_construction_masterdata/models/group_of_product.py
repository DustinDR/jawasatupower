from dataclasses import field
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError
from odoo.osv import expression


class GroupOfProduct(models.Model):
    _name = 'group.of.product'
    _description = 'Group of Product'
    _rec_name = 'rec_name'

    rec_name = fields.Char('rec_name', compute='_compute_name')
    name = fields.Char('Name')
    cost_code = fields.Char('Cost Code')
    products = fields.Many2one('product.template', 'Product',
                               domain="[('company_id', '=', False), ('company_id', '=', company_id)'|', ('multi_companies_all', '=', False), ('multi_companies_all', '=', company_id)]")
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True, default=lambda self: self.env.company, readonly=True)

    def _compute_name(self):
        for rec in self:
            record = rec.name + ' - ' + rec.cost_code
            rec.write({'rec_name' : record })

    @api.constrains('cost_code')
    def _check_existing_record(self):
        for record in self:
            cost_code_id = self.env['group.of.product'].search(
                [('cost_code', '=', record.cost_code)])
            if len(cost_code_id) > 1:
                raise ValidationError(
                    f'The cost code in this group of product is the same as another group of product. Please change the costcode')


    