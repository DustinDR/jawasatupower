# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class VariableTemplate(models.Model):
    _name = 'variable.template'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _description = 'Variable'
    _check_company_auto = True

    @api.onchange('variable_subcon')
    def _onchange_variable_subcon(self):
        context = dict(self.env.context) or {}
        if context.get('variable_subcon'):
            self.variable_subcon = True
    
    @api.depends('material_variable_ids.subtotal', 'labour_variable_ids.subtotal', 
                 'subcon_variable_ids.subtotal', 'overhead_variable_ids.subtotal', 
                 'equipment_variable_ids.subtotal', 'service_variable_ids.subtotal')
    def _calculate_total(self):
        total_job_cost = 0.0
        for order in self:
            if order.material_variable_ids : 
                for line in order.material_variable_ids:
                    material_price =  (line.quantity * line.unit_price)
                    order.total_variable_material += material_price
                    total_job_cost += material_price

            else : 

                order.total_variable_material = 0

            if order.labour_variable_ids :
                for line in order.labour_variable_ids:
                    labour_price =  (line.quantity * line.unit_price) 
                    order.total_variable_labour += labour_price
                    total_job_cost += labour_price
            else :

                order.total_variable_labour = 0

            if order.overhead_variable_ids: 
                for line in order.overhead_variable_ids:
                    overhead_price =  (line.quantity * line.unit_price) 
                    order.total_variable_overhead += overhead_price
                    total_job_cost += overhead_price

            else :

                order.total_variable_overhead = 0
            
            if order.subcon_variable_ids :
                for line in order.subcon_variable_ids:
                    subcon_price =  (line.quantity * line.unit_price) 
                    order.total_variable_subcon += subcon_price
                    total_job_cost += subcon_price
            else :

                order.total_variable_subcon = 0

            if order.service_variable_ids :
                for line in order.service_variable_ids:
                    service_price =  (line.quantity * line.unit_price) 
                    order.total_variable_service += service_price
                    total_job_cost += service_price
            else :

                order.total_variable_service = 0

            if order.equipment_variable_ids :
                for line in order.equipment_variable_ids:
                    equipment_price =  (line.quantity * line.unit_price) 
                    order.total_variable_equipment += equipment_price
                    total_job_cost += equipment_price
            else :

                order.total_variable_equipment = 0

            order.total_variable = total_job_cost

        return

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id
            line.company_id = res_user_id.company_id

    @api.onchange('variable_subcon')
    def onchange_variable_subcon(self):
        if self.variable_subcon == True:
            self.subcon_variable_ids = False
        else:
            self.service_variable_ids = False

    @api.constrains('name')
    def _check_existing_record(self):
        for record in self:
            name_id = self.env['variable.template'].search(
                [('name', '=', record.name)])
            if len(name_id) > 1:
                if self.variable_subcon == False:
                    raise ValidationError(
                        f'The Variable name already exists, which is the same as the previous Variable name.\nPlease change the Variable name.')
                elif self.variable_subcon == True:
                    raise ValidationError(
                        f'The Subcon name already exists, which is the same as the previous Subcon name.\nPlease change the Subcon name.')    

    @api.onchange('material_variable_ids')
    def _check_exist_group_of_product_material(self):
        exist_group_list = []
        for line in self.material_variable_ids:
            if line.group_of_product.id in exist_group_list:
                raise ValidationError(_('The Group of Product "%s" already exists, please change the Product with different Group of Product.'%((line.group_of_product.name))))
            exist_group_list.append(line.group_of_product.id)
                 
    
    @api.onchange('labour_variable_ids')
    def _check_exist_group_of_product_labour(self):
        exist_group_list = []
        for line in self.labour_variable_ids:
            if line.group_of_product.id in exist_group_list:
                raise ValidationError(_('The Group of Product "%s" already exists, please change the Product with different Group of Product.'%((line.group_of_product.name))))
            exist_group_list.append(line.group_of_product.id)

    @api.onchange('overhead_variable_ids')
    def _check_exist_group_of_product_overhead(self):
        exist_group_list = []
        for line in self.overhead_variable_ids:
            if line.group_of_product.id in exist_group_list:
                raise ValidationError(_('The Group of Product "%s" already exists, please change the Product with different Group of Product.'%((line.group_of_product.name))))
            exist_group_list.append(line.group_of_product.id)

    @api.onchange('equipment_variable_ids')
    def _check_exist_group_of_product_equipment(self):
        exist_group_list = []
        for line in self.equipment_variable_ids:
            if line.group_of_product.id in exist_group_list:
                raise ValidationError(_('The Group of Product "%s" already exists, please change the Product with different Group of Product.'%((line.group_of_product.name))))
            exist_group_list.append(line.group_of_product.id)

    @api.onchange('service_variable_ids')
    def _check_exist_subcon(self):
        exist_group_list = []
        for line in self.service_variable_ids:
            if line.group_of_product.id in exist_group_list:
                raise ValidationError(_('The Group of Product "%s" already exists, please change the Product with different Group of Product.'%((line.group_of_product.name))))
            exist_group_list.append(line.group_of_product.id)

    @api.onchange('subcon_variable_ids')
    def _check_exist_subcon(self):
        exist_subcon_list = []
        for line in self.subcon_variable_ids:
            if line.variable.name in exist_subcon_list:
                raise ValidationError(_('The subcon "%s" already exists, please change the subcon.'%((line.variable.name))))
            exist_subcon_list.append(line.variable.name)

    name = fields.Char("Variable Name", index=True, required=True)
    variable_subcon = fields.Boolean(string="Variable Subcon", default=False)
    variable_uom = fields.Many2one('uom.uom', required=True, string="Unit of Measure")
    total_variable = fields.Monetary(string="Total Variable", default=0.0, readonly=True, tracking=True)
    company_currency_id = fields.Many2one('res.currency', string='Currency', related='company_id.currency_id')
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True, readonly=True,
                                 default=lambda self: self.env.company)

    material_variable_ids = fields.One2many('material.variable', 'material_id', string="Material")
    labour_variable_ids = fields.One2many('labour.variable', 'labour_id', string="Labour")
    subcon_variable_ids = fields.One2many('subcon.variable', 'subcon_id', string="Subcon")
    overhead_variable_ids = fields.One2many('overhead.variable', 'overhead_id', string="Overhead")
    equipment_variable_ids = fields.One2many('equipment.variable', 'equipment_id', string="Equipment")
    service_variable_ids = fields.One2many('service.variable', 'service_id', string="Service")
    
    total_variable_material = fields.Monetary(compute='_calculate_total', string="Total Material", default=0.0, readonly=True)
    total_variable_labour = fields.Monetary(compute='_calculate_total', string="Total Labour", default=0.0, readonly=True)
    total_variable_subcon = fields.Monetary(compute='_calculate_total', string="Total Subcon", default=0.0, readonly=True)
    total_variable_overhead = fields.Monetary(compute='_calculate_total', string="Total Overhead", default=0.0, readonly=True)
    total_variable_equipment = fields.Monetary(compute='_calculate_total', string="Total Equipment", default=0.0, readonly=True)
    total_variable_service = fields.Monetary(compute='_calculate_total', string="Total Service", default=0.0, readonly=True)


class MaterialEstimation(models.Model):
    _name = 'material.variable'
    _order = 'sequence'
    _check_company_auto = True

    
    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.unit_price = self.product_id.list_price
            self.description = self.product_id.display_name
            self.group_of_product = self.product_id.group_of_product.id 
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False
            self.unit_price = False
            self.group_of_product = False

    @api.depends('material_id.material_variable_ids', 'material_id.material_variable_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.material_id.material_variable_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price =  (line.quantity * line.unit_price) 
            line.subtotal = price


    material_id = fields.Many2one('variable.template', string="Variable")
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
    product_id = fields.Many2one('product.product', string='Product', 
                 domain="[('sale_ok', '=', True), '|', ('purchase_ok', '=', True), '|', ('company_id', '=', parent.company_id), '|', ('multi_companies_all', '=', False), ('multi_companies_all', '=', parent.company_id)]",
                 check_company=True, ondelete='restrict', tracking=True, required=True)
    company_id = fields.Many2one(related='material_id.company_id', string='Company', readonly=True)
    description = fields.Text(string="Description", required=True)
    quantity = fields.Float(string="Quantity")
    uom_id = fields.Many2one('uom.uom', required=True, string="Unit of Measure")
    unit_price = fields.Float(string="Unit Price")
    subtotal = fields.Float(readonly=True, string="Subtotal")
    currency_id = fields.Many2one('res.currency', compute='get_currency_id', string="Currency")

class LabourEstimation(models.Model):
    _name = 'labour.variable'
    _order = 'sequence'
    _check_company_auto = True

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.unit_price = self.product_id.list_price
            self.description = self.product_id.display_name
            self.group_of_product = self.product_id.group_of_product.id 
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False
            self.unit_price = False
            self.group_of_product = False

    @api.depends('labour_id.labour_variable_ids', 'labour_id.labour_variable_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.labour_id.labour_variable_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price =  (line.quantity * line.unit_price) 
            line.subtotal = price


    labour_id = fields.Many2one('variable.template', string="Variable")
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
    product_id = fields.Many2one('product.product', string='Product', 
                 domain="[('sale_ok', '=', True), '|', ('purchase_ok', '=', True), '|', ('company_id', '=', parent.company_id), '|', ('multi_companies_all', '=', False), ('multi_companies_all', '=', parent.company_id)]",
                 check_company=True, ondelete='restrict', tracking=True, required=True)
    company_id = fields.Many2one(related='labour_id.company_id', string='Company', readonly=True)
    description = fields.Text(string="Description", required=True)
    quantity = fields.Float(string="Quantity")
    uom_id = fields.Many2one('uom.uom', required=True, string="Unit of Measure")
    unit_price = fields.Float(string="Unit Price")
    subtotal = fields.Float(readonly=True, string="Subtotal")
    currency_id = fields.Many2one('res.currency', compute='get_currency_id', string="Currency")
    
class SubconEstimation(models.Model):
    _name = 'subcon.variable'
    _order = 'sequence'
    _check_company_auto = True

    @api.onchange('variable')
    def onchange_variable(self):
        if self.variable:
            self.uom_id = self.variable.variable_uom.id
            self.quantity = 1.0
            self.unit_price = self.variable.total_variable
            self.description = self.variable.name
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False
            self.unit_price = False
    
    @api.depends('subcon_id.subcon_variable_ids', 'subcon_id.subcon_variable_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.subcon_id.subcon_variable_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price =  (line.quantity * line.unit_price) 
            line.subtotal = price


    subcon_id = fields.Many2one('variable.template', string="Variable")
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    variable = fields.Many2one('variable.template', string="Subcon",
               domain="[('variable_subcon', '=', True), ('company_id', '=', parent.company_id)]")
    company_id = fields.Many2one(related='subcon_id.company_id', string='Company', readonly=True)
    description = fields.Text(string="Description", required=True)
    quantity = fields.Float(string="Quantity")
    uom_id = fields.Many2one('uom.uom', required=True, string="Unit of Measure")
    unit_price = fields.Float(string="Unit Price")
    subtotal = fields.Float(readonly=True, string="Subtotal")
    currency_id = fields.Many2one('res.currency', compute='get_currency_id', string="Currency")
    
class OverheadEstimation(models.Model):
    _name = 'overhead.variable'
    _order = 'sequence'
    _check_company_auto = True

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.unit_price = self.product_id.list_price
            self.description = self.product_id.display_name
            self.group_of_product = self.product_id.group_of_product.id 
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False
            self.unit_price = False
            self.group_of_product = False

    @api.depends('overhead_id.overhead_variable_ids', 'overhead_id.overhead_variable_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.overhead_id.overhead_variable_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price =  (line.quantity * line.unit_price) 
            line.subtotal = price

    
    overhead_id = fields.Many2one('variable.template', string="Variable")
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
    product_id = fields.Many2one('product.product', string='Product', 
                 domain="[('sale_ok', '=', True), '|', ('purchase_ok', '=', True), '|', ('company_id', '=', parent.company_id), '|', ('multi_companies_all', '=', False), ('multi_companies_all', '=', parent.company_id)]",
                 check_company=True, ondelete='restrict', tracking=True, required=True)
    company_id = fields.Many2one(related='overhead_id.company_id', string='Company', readonly=True)
    description = fields.Text(string="Description", required=True)
    quantity = fields.Float(string="Quantity")
    uom_id = fields.Many2one('uom.uom', required=True, string="Unit of Measure")
    unit_price = fields.Float(string="Unit Price")
    subtotal = fields.Float(readonly=True, string="Subtotal")
    currency_id = fields.Many2one('res.currency', compute='get_currency_id', string="Currency")
    
class EquipmentEstimation(models.Model):
    _name = 'equipment.variable'
    _order = 'sequence'
    _check_company_auto = True

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.unit_price = self.product_id.list_price
            self.description = self.product_id.display_name
            self.group_of_product = self.product_id.group_of_product.id 
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False
            self.unit_price = False
            self.group_of_product = False

    @api.depends('equipment_id.equipment_variable_ids', 'equipment_id.equipment_variable_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.equipment_id.equipment_variable_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price =  (line.quantity * line.unit_price) 
            line.subtotal = price
    
    equipment_id = fields.Many2one('variable.template', string="Variable")
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
    product_id = fields.Many2one('product.product', string='Product', 
                 domain="[('sale_ok', '=', True), '|', ('purchase_ok', '=', True), '|', ('company_id', '=', parent.company_id), '|', ('multi_companies_all', '=', False), ('multi_companies_all', '=', parent.company_id)]",
                 check_company=True, ondelete='restrict', tracking=True, required=True)
    company_id = fields.Many2one(related='equipment_id.company_id', string='Company', readonly=True)
    description = fields.Text(string="Description", required=True)
    quantity = fields.Float(string="Quantity")
    uom_id = fields.Many2one('uom.uom', required=True, string="Unit of Measure")
    unit_price = fields.Float(string="Unit Price")
    subtotal = fields.Float(readonly=True, string="Subtotal")
    currency_id = fields.Many2one('res.currency', compute='get_currency_id', string="Currency")


class ServiceEstimation(models.Model):
    _name = 'service.variable'
    _order = 'sequence'
    _check_company_auto = True

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.unit_price = self.product_id.list_price
            self.description = self.product_id.display_name
            self.group_of_product = self.product_id.group_of_product.id 
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False
            self.unit_price = False
            self.group_of_product = False

    @api.depends('service_id.service_variable_ids', 'service_id.service_variable_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.service_id.service_variable_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price =  (line.quantity * line.unit_price) 
            line.subtotal = price


    service_id = fields.Many2one('variable.template', string="Variable")
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
    product_id = fields.Many2one('product.product', string='Product', 
                 domain="[('sale_ok', '=', True), '|', ('purchase_ok', '=', True), '|', ('company_id', '=', parent.company_id), '|', ('multi_companies_all', '=', False), ('multi_companies_all', '=', parent.company_id)]",
                 check_company=True, ondelete='restrict', tracking=True, required=True)
    company_id = fields.Many2one(related='service_id.company_id', string='Company', readonly=True)
    description = fields.Text(string="Description", required=True)
    quantity = fields.Float(string="Quantity")
    uom_id = fields.Many2one('uom.uom', required=True, string="Unit of Measure")
    unit_price = fields.Float(string="Unit Price")
    subtotal = fields.Float(readonly=True, string="Subtotal")
    currency_id = fields.Many2one('res.currency', compute='get_currency_id', string="Currency")