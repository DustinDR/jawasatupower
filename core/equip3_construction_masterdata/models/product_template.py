# -*- coding: utf-8 -*-

from odoo import fields, models, api, _


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    control_purchase_bill = fields.Selection([
        ('on_order_qty', 'On Order Quantity'),
        ('on_received_qty', 'On Received Quantity'),
    ], string='Control Purhase Bill', default='on_received_qty')
    is_work_order = fields.Boolean(string="Is Work Order")
    investment = fields.Boolean(string="Investment")
    product_type_id = fields.Selection(selection_add=[('assets', 'Assets')])
    type = fields.Selection(selection_add=[('overhead', 'Overhead')], ondelete={'overhead': 'cascade'})
    service_category = fields.Selection([('labour', 'Labour'), ('subcontracting', 'Subcontracting')], string='Service Category')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
