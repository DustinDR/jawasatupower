# -*- coding: utf-8 -*-

# from . import models
from . import product_template
from . import res_partner
from . import project
from . import guarantee_table
from . import cost_code
from . import purchase_action
from . import addendum
from . import inherited_res_users
from . import group_of_product
from . import variable
from . import budget_period
