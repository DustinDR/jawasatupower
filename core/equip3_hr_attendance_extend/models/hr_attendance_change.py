# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime, date, timedelta
import pytz
from odoo.exceptions import ValidationError
from pytz import timezone


class HrAttendanceChange(models.Model):
    _name = 'hr.attendance.change'
    _description = 'Attendance Change Reuest'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    
    
    

    @api.model
    def default_get(self, fields):
        res = super(HrAttendanceChange, self).default_get(fields)
        employees = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        res['employee_id'] = employees.id
        return res

    @api.model
    def create(self, vals):
        sequence_no = self.env['ir.sequence'].next_by_code('hr.attendance.change')
        vals.update({'name': sequence_no})
        return super(HrAttendanceChange, self).create(vals)

    name = fields.Char(string="Name", readonly=True, required=True, copy=False, default='New')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    domain_employee_ids = fields.Many2many('hr.employee',string="Employee Domain",compute='_compute_employee_ids')
    is_readonly = fields.Boolean(compute='_compute_read_only')
    request_date_from = fields.Date('Request Start Date', required=True, tracking=True)
    request_date_to = fields.Date('Request End Date', required=True, tracking=True)
    attachment = fields.Binary('Attachment', tracking=True)
    state = fields.Selection([("draft", "Draft"),
                              ("to_approve", "To Approve"),
                              ("approved", "Approved"),
                              ("refused", "Refused")
                              ], string='State', default="draft", tracking=True)
    attendance_change_line_ids = fields.One2many('hr.attendance.change.line', 'hr_attendance_change_id',
                                                 string='Attendance Line Ids', tracking=True)
    # is_approver = fields.Boolean('Is Approver', compute='_compute_is_approver', default=False)

    attendance_change_user_ids = fields.One2many('attendance.change.approver.user', 'attendance_change_approver_id',
                                                string='Approver')
    approvers_ids = fields.Many2many('res.users', 'attendance_change_approvers_rel', string='Approvers List')
    approved_user_ids = fields.Many2many('res.users', string='Approved by User')
    is_approver = fields.Boolean(string="Is Approver", compute='_compute_is_approver')
    approved_user_text = fields.Text(string="Approved User", tracking=True)
    approved_user = fields.Text(string="Approved User", tracking=True)
    job_id = fields.Many2one('hr.job', related='employee_id.job_id')
    department_id = fields.Many2one('hr.department', related='employee_id.department_id')
    feedback_parent = fields.Text(string='Parent Feedback')
    
    
    @api.depends('employee_id')
    def _compute_read_only(self):
        for record in self:
            if self.env.user.has_group('hr_attendance.group_hr_attendance') and not self.env.user.has_group('hr_attendance.group_hr_attendance_user'):
                record.is_readonly = True
            else:
                record.is_readonly = False
                
    @api.depends('employee_id')
    def _compute_employee_ids(self):
        for record in self:
            employee_ids = []
            if self.env.user.has_group('hr_attendance.group_hr_attendance_user') and not self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_attendance_hr_manager'):
                my_employee = self.env['hr.employee'].sudo().search([('user_id','=',self.env.user.id)])
                if my_employee:
                    for child_record in my_employee.child_ids:
                        employee_ids.append(my_employee.id)
                        employee_ids.append(child_record.id)
                        child_record._get_amployee_hierarchy(employee_ids,child_record.child_ids,my_employee.id)
                record.domain_employee_ids = [(6,0,employee_ids)]
            else:
                all_employee = self.env['hr.employee'].sudo().search([])
                for data_employee in all_employee:
                    employee_ids.append(data_employee.id)
                record.domain_employee_ids = [(6,0,employee_ids)]
    
    def custom_menu_request(self):
            # views = [(self.env.ref('bi_employee_travel_managment.view_travel_req_tree').id, 'tree'),
        #              (self.env.ref('bi_employee_travel_managment.view_travel_req_form').id, 'form')]
        # search_view_id = self.env.ref("equip3_hr_holidays_extend.view_my_leave_cancel_tree")
        if  self.env.user.has_group('hr_attendance.group_hr_attendance_user') and not self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_attendance_hr_manager'):
            employee_ids = []
            my_employee = self.env['hr.employee'].sudo().search([('user_id','=',self.env.user.id)])
            if my_employee:
                for child_record in my_employee.child_ids:
                    employee_ids.append(my_employee.id)
                    employee_ids.append(child_record.id)
                    child_record._get_amployee_hierarchy(employee_ids,child_record.child_ids,my_employee.id)
                return {
                    'type': 'ir.actions.act_window',
                    'name': 'Attendance Change Request',
                    'res_model': 'hr.attendance.change',
                    'target':'current',
                    'view_mode': 'tree,form',
                    # 'views':views,
                    'domain': [('employee_id', 'in', employee_ids)],
                    'context':{},
                    'help':"""<p class="o_view_nocontent_smiling_face">
                        Create a new Attendance Change Request
                    </p>""",
                    # 'context':{'search_default_current':1, 'search_default_group_by_state': 1},
                    # 'search_view_id':search_view_id.id,
                    
                }
        
        else:
            return {
                'type': 'ir.actions.act_window',
                'name': 'Attendance Change Request',
                'res_model': 'hr.attendance.change',
                'target':'current',
                'view_mode': 'tree,form',
                'domain': [],
                'help':"""<p class="o_view_nocontent_smiling_face">
                    Create a new Attendance Change Request
                </p>""",
                'context':{},
                # 'views':views,
                # 'search_view_id':search_view_id.id,
            }
    
    
    def custom_menu(self):
        views = [(self.env.ref('equip3_hr_attendance_extend.view_hr_working_schedule_exchange_tree').id, 'tree'),
                    (self.env.ref('equip3_hr_attendance_extend.view_hr_working_schedule_exchange_form').id, 'form')]
        if  self.env.user.has_group('hr_attendance.group_hr_attendance') and not self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_attendance_hr_manager'):
            return {
                'type': 'ir.actions.act_window',
                'name': 'My Attendance Change Request',
                'res_model': 'hr.attendance.change',
                'view_mode': 'tree,form',
                'domain': [('employee_id.user_id', '=', self.env.user.id)],
                # 'context':{'search_default_current':1, 'search_default_group_by_state': 1},
                # 'search_view_id':search_view_id.id,
                
            }
        else:
            return {
                'type': 'ir.actions.act_window',
                'name': 'My Attendance Change Request',
                'res_model': 'hr.attendance.change',
                'view_mode': 'tree,form',
                # 'context':{'search_default_current':1, 'search_default_group_by_state': 1},
                # 'search_view_id':search_view_id.id,
                
            }

    @api.onchange('employee_id', 'request_date_from')
    def onchange_approver_user(self):
        for attendance_change in self:
            if attendance_change.attendance_change_user_ids:
                remove = []
                for line in attendance_change.attendance_change_user_ids:
                    remove.append((2, line.id))
                attendance_change.attendance_change_user_ids = remove
            setting = self.env['ir.config_parameter'].sudo().get_param(
                'equip3_hr_attendance_extend.attendance_type_approval')
            if setting == 'employee_hierarchy':
                attendance_change.attendance_change_user_ids = self.attendance_change_emp_by_hierarchy(attendance_change)
                self.app_list_attendance_change_emp_by_hierarchy()
            if setting == 'approval_matrix':
                self.attendance_change_approval_by_matrix(attendance_change)

    def attendance_change_emp_by_hierarchy(self, attendance_change):
        approval_ids = []
        seq = 1
        data = 0
        line = self.get_manager(attendance_change, attendance_change.employee_id, data, approval_ids, seq)
        return line

    def get_manager(self, attendance_change, employee_manager, data, approval_ids, seq):
        setting_level = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_extend.attendance_level')
        if not setting_level:
            raise ValidationError("Level not set")
        if not employee_manager['parent_id']['user_id']:
            return approval_ids
        while data < int(setting_level):
            approval_ids.append(
                (0, 0, {'user_ids': [(4, employee_manager['parent_id']['user_id']['id'])]}))
            data += 1
            seq += 1
            if employee_manager['parent_id']['user_id']['id']:
                self.get_manager(attendance_change, employee_manager['parent_id'], data, approval_ids, seq)
                break

        return approval_ids

    def app_list_attendance_change_emp_by_hierarchy(self):
        for attendance_change in self:
            app_list = []
            for line in attendance_change.attendance_change_user_ids:
                app_list.append(line.user_ids.id)
            attendance_change.approvers_ids = app_list

    def attendance_change_approval_by_matrix(self, attendance_change):
        app_list = []
        approval_matrix = self.env['hr.attendance.approval.matrix'].search(
            [('apply_to', '=', 'by_employee')])
        matrix = approval_matrix.filtered(lambda line: attendance_change.employee_id.id in line.employee_ids.ids)
        if matrix:
            data_approvers = []
            for line in matrix[0].approval_matrix_ids:
                data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                              'user_ids': [(6, 0, line.approvers.ids)]}))
                for approvers in line.approvers:
                    app_list.append(approvers.id)
            attendance_change.approvers_ids = app_list
            attendance_change.attendance_change_user_ids = data_approvers

        if not matrix:
            data_approvers = []
            approval_matrix = self.env['hr.attendance.approval.matrix'].search(
                [('apply_to', '=', 'by_job_position')])
            matrix = approval_matrix.filtered(lambda line: attendance_change.job_id.id in line.job_ids.ids)
            if matrix:
                for line in matrix[0].approval_matrix_ids:
                    data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                                  'user_ids': [(6, 0, line.approvers.ids)]}))
                    for approvers in line.approvers:
                        app_list.append(approvers.id)
                attendance_change.approvers_ids = app_list
                attendance_change.attendance_change_user_ids = data_approvers
            if not matrix:
                data_approvers = []
                approval_matrix = self.env['hr.attendance.approval.matrix'].search(
                    [('apply_to', '=', 'by_department')])
                matrix = approval_matrix.filtered(lambda line: attendance_change.department_id.id in line.department_ids.ids)
                if matrix:
                    for line in matrix[0].approval_matrix_ids:
                        data_approvers.append((0, 0, {'minimum_approver': line.minimum_approver,
                                                      'user_ids': [(6, 0, line.approvers.ids)]}))
                        for approvers in line.approvers:
                            app_list.append(approvers.id)
                    attendance_change.approvers_ids = app_list
                    attendance_change.attendance_change_user_ids = data_approvers

    @api.depends('state', 'employee_id', 'request_date_from')
    def _compute_is_approver(self):
        for attendance_change in self:
            if attendance_change.approvers_ids:
                setting = self.env['ir.config_parameter'].sudo().get_param(
                    'equip3_hr_attendance_extend.attendance_type_approval')
                setting_level = self.env['ir.config_parameter'].sudo().get_param(
                    'equip3_hr_attendance_extend.attendance_level')
                app_level = int(setting_level)
                current_user = attendance_change.env.user
                if setting == 'employee_hierarchy':
                    matrix_line = sorted(attendance_change.attendance_change_user_ids.filtered(lambda r: r.is_approve == True))
                    app = len(matrix_line)
                    a = len(attendance_change.attendance_change_user_ids)
                    if app < app_level and app < a:
                        if current_user in attendance_change.attendance_change_user_ids[app].user_ids:
                            attendance_change.is_approver = True
                        else:
                            attendance_change.is_approver = False
                    else:
                        attendance_change.is_approver = False
                elif setting == 'approval_matrix':
                    matrix_line = sorted(attendance_change.attendance_change_user_ids.filtered(lambda r: r.is_approve == True))
                    app = len(matrix_line)
                    a = len(attendance_change.attendance_change_user_ids)
                    if app < a:
                        for line in attendance_change.attendance_change_user_ids[app]:
                            if current_user in line.user_ids:
                                attendance_change.is_approver = True
                            else:
                                attendance_change.is_approver = False
                    else:
                        attendance_change.is_approver = False

                else:
                    attendance_change.is_approver = False
            else:
                attendance_change.is_approver = False

    # def _compute_is_approver(self):
    #     for rec in self:
    #         current_user = rec.env.user
    #         if rec.employee_id.parent_id.user_id == current_user and rec.state == 'to_approve':
    #             rec.is_approver = True
    #         else:
    #             rec.is_approver = False

    @api.onchange('request_date_from', 'request_date_to')
    def onchange_fetch_attendance(self):
        for rec in self:
            if rec.attendance_change_line_ids:
                remove = []
                for line in rec.attendance_change_line_ids:
                    remove.append((2, line.id))
                rec.attendance_change_line_ids = remove
            if rec.request_date_from and rec.request_date_to:
                fmt = '%Y-%m-%d'
                d1 = datetime.strptime(str(rec.request_date_from), fmt)
                d2 = datetime.strptime(str(rec.request_date_to), fmt)
                delta = d2 - d1
                for i in range(0, delta.days + 1):
                    between = d1 + timedelta(days=i)
                    date_vals = [(0, 0, {'hr_attendance_change_id': self.id, 'date': between,
                                         'resource_calendar_id': rec.employee_id.resource_calendar_id.id})]
                    rec.attendance_change_line_ids = date_vals
                for hr_att in rec.env['hr.attendance'].search(
                        [('employee_id', '=', rec.employee_id.id), ('check_in', '>=', rec.request_date_from),
                         ('check_in', '<=', rec.request_date_to)]):
                    for line in rec.attendance_change_line_ids:
                        d3 = datetime.strptime(str(line.date), fmt)
                        # Checkin cnverting into timezone pick time
                        now_in_utc = pytz.utc.localize(hr_att.check_in)
                        tz_in = pytz.timezone(rec.env.user.tz or 'UTC')
                        now_in_tz = now_in_utc.astimezone(tz_in)
                        time_3 = str(now_in_tz)
                        time_4 = time_3[-14:][:-8]
                        # Checkin cnverting into timezone pick date
                        pick_date_checkin = time_3[:-6][:-9]
                        d4 = datetime.strptime(str(pick_date_checkin), fmt)
                        # Checkin string to float convert
                        vals = time_4.split(':')
                        t, hours = divmod(float(vals[0]), 24)
                        t, minutes = divmod(float(vals[1]), 60)
                        minutes = minutes / 60.0
                        convert_time = hours + minutes
                        # Checkout cnverting into timezone
                        now_out_utc = pytz.utc.localize(hr_att.check_out)
                        tz_out = pytz.timezone(self.env.user.tz or 'UTC')
                        now_out_tz = now_out_utc.astimezone(tz_out)
                        time_5 = str(now_out_tz)
                        time_6 = time_5[-14:][:-8]
                        # Checkin string to float convert
                        vals_out = time_6.split(':')
                        t, hours_out = divmod(float(vals_out[0]), 24)
                        t, minutes_out = divmod(float(vals_out[1]), 60)
                        minutes_out = minutes_out / 60.0
                        convert_time_out = hours_out + minutes_out
                        if hr_att and d3 == d4:
                            line.update({
                                'hr_attendance_id': hr_att.id,
                                'check_in': convert_time,
                                'check_out': convert_time_out,
                                'checkin_status':hr_att.checkin_status,
                                'checkout_status':hr_att.checkout_status,
                            })

    def get_url(self, obj):
        url = ''
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        menu_id = self.env['ir.model.data'].get_object_reference(
            'equip3_hr_attendance_extend', 'hr_attendance_change_manager_menu')[1]
        action_id = self.env['ir.model.data'].get_object_reference(
            'equip3_hr_attendance_extend', 'hr_attendance_change_action_manager')[1]
        url = base_url + "/web?db=" + str(self._cr.dbname) + "#id=" + str(
            obj.id) + "&view_type=form&model=hr.holidays&menu_id=" + str(
            menu_id) + "&action=" + str(action_id)
        return url

    def approver_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.attendance_change_user_ids:
                matrix_line = sorted(rec.attendance_change_user_ids.filtered(lambda r: r.is_approve == True))
                approver = rec.attendance_change_user_ids[len(matrix_line)]
                for user in approver.user_ids:
                    try:
                        template_id = ir_model_data.get_object_reference(
                            'equip3_hr_attendance_extend',
                            'email_template_attendance_change_request_approval')[1]
                    except ValueError:
                        template_id = False
                    ctx = self._context.copy()
                    url = self.get_url(self)
                    ctx.update({
                        'email_from': self.env.user.email,
                        'email_to': user.email,
                        'url': url,
                        'approver_name': user.name,
                        'emp_name': self.employee_id.name,
                    })
                    if self.request_date_from:
                        ctx.update(
                            {'date_from': fields.Datetime.from_string(self.request_date_from).strftime('%d/%m/%Y')})
                    if self.request_date_to:
                        ctx.update(
                            {'date_to': fields.Datetime.from_string(self.request_date_to).strftime('%d/%m/%Y')})
                    self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                              force_send=True)
                break

    def approved_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.attendance_change_user_ids:
                for rec in rec.attendance_change_user_ids.sorted(key=lambda r: r.name):
                    for user in rec.user_ids:
                        try:
                            template_id = ir_model_data.get_object_reference(
                                'equip3_hr_attendance_extend',
                                'email_template_attendance_change_approved')[1]
                        except ValueError:
                            template_id = False
                        ctx = self._context.copy()
                        url = self.get_url(self)
                        ctx.update({
                            'email_from': self.env.user.email,
                            'email_to': self.employee_id.user_id.email,
                            'url': url,
                            'name': self.name,
                            'emp_name': self.employee_id.name,
                        })
                        if self.request_date_from:
                            ctx.update(
                                {'date_from': fields.Datetime.from_string(self.request_date_from).strftime('%d/%m/%Y')})
                        if self.request_date_to:
                            ctx.update(
                                {'date_to': fields.Datetime.from_string(self.request_date_to).strftime('%d/%m/%Y')})
                        self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                                  force_send=True)
                    break

    def reject_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.attendance_change_user_ids:
                for rec in rec.attendance_change_user_ids.sorted(key=lambda r: r.name):
                    for user in rec.user_ids:
                        try:
                            template_id = ir_model_data.get_object_reference(
                                'equip3_hr_attendance_extend',
                                'email_template_attendance_change_reject')[1]
                        except ValueError:
                            template_id = False
                        ctx = self._context.copy()
                        url = self.get_url(self)
                        ctx.update({
                            'email_from': self.env.user.email,
                            'email_to': self.employee_id.user_id.email,
                            'url': url,
                            'name': self.name,
                            'emp_name': self.employee_id.name,
                        })
                        if self.request_date_from:
                            ctx.update(
                                {'date_from': fields.Datetime.from_string(self.request_date_from).strftime('%d/%m/%Y')})
                        if self.request_date_to:
                            ctx.update(
                                {'date_to': fields.Datetime.from_string(self.request_date_to).strftime('%d/%m/%Y')})
                        self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                                  force_send=True)
                    break

    def action_confirm(self):
        for rec in self:
            self.approver_mail()
            rec.write({'state': 'to_approve'})
            #Dont delete, validation warning for reason field
            # if rec.attendance_change_line_ids:
            #     for line in rec.attendance_change_line_ids:
            #         if line.hr_attendance_id and line.check_in_correction != 0.0 and not line.reason:
            #             raise ValidationError("Please fill the reason field.")
            #         elif line.hr_attendance_id and line.check_out_correction != 0.0 and not line.reason:
            #             raise ValidationError("Please fill the reason field")
            #         else:
            #             rec.write({'state': 'to_approve'})

    def action_approve(self):
        for rec in self:
            if rec.attendance_change_line_ids:
                for line in rec.attendance_change_line_ids:
                    if line.hr_attendance_id and line.check_in_correction != 0.0:
                        # Float to time convertion
                        check_in_correction_hrs = timedelta(hours=line.check_in_correction)
                        check_in_correction_hours = check_in_correction_hrs
                        check_in_correction_hr = str(check_in_correction_hours)
                        time_3 = check_in_correction_hr[-8:]
                        float_datetime_1 = datetime.strptime(time_3, '%H:%M:%S')

                        # spliting from check_in_correction float field
                        correction_checkin_hour = float_datetime_1.hour
                        correction_checkin_minute = float_datetime_1.minute

                        # spliting from check_in from attendance datetime field
                        original_checkin_date = line.hr_attendance_id.check_in.day
                        original_checkin_month = line.hr_attendance_id.check_in.month
                        original_checkin_year = line.hr_attendance_id.check_in.year
                        concatenated_checkin = "%02d-%02d-%02d %02d:%02d:00" % (
                            original_checkin_year, original_checkin_month, original_checkin_date,
                            correction_checkin_hour,
                            correction_checkin_minute)
                        # converting to timezone
                        update_checkin = fields.Datetime.to_string(
                            pytz.timezone(self.env.context['tz']).localize(
                                fields.Datetime.from_string(concatenated_checkin),
                                is_dst=None).astimezone(pytz.utc))

                        line.hr_attendance_id.update({
                            'check_in': update_checkin,
                            'hr_attendance_change_id': rec.id,
                        })
                        # rec.update({
                        #     'state': 'approved',
                        #     })
                    if line.hr_attendance_id and line.check_out_correction != 0.0:
                        # Float to time convertion
                        check_out_correction_hrs = timedelta(hours=line.check_out_correction)
                        check_out_correction_hours = check_out_correction_hrs
                        check_out_correction_hr = str(check_out_correction_hours)
                        time_out_1 = check_out_correction_hr[-8:]
                        float_datetime_2 = datetime.strptime(time_out_1, '%H:%M:%S')

                        # spliting from check_in_correction float field
                        correction_checkout_hour = float_datetime_2.hour
                        correction_checkout_minute = float_datetime_2.minute

                        # spliting from check_in from attendance datetime field
                        original_checkout_date = line.hr_attendance_id.check_out.day
                        original_checkout_month = line.hr_attendance_id.check_out.month
                        original_checkout_year = line.hr_attendance_id.check_out.year
                        concatenated_checkout = "%02d-%02d-%02d %02d:%02d:00" % (
                            original_checkout_year, original_checkout_month, original_checkout_date,
                            correction_checkout_hour,
                            correction_checkout_minute)
                        # converting to timezone
                        update_checkout = fields.Datetime.to_string(
                            pytz.timezone(self.env.context['tz']).localize(
                                fields.Datetime.from_string(concatenated_checkout),
                                is_dst=None).astimezone(pytz.utc))

                        line.hr_attendance_id.update({
                            'check_out': update_checkout,
                            'hr_attendance_change_id': rec.id,
                        })
                        # rec.update({
                        #     'state': 'approved',
                        # })
                    if line.hr_attendance_id and line.checkin_status_correction:
                        line.hr_attendance_id.update({
                            'hr_attendance_change_id': rec.id,
                            'checkin_status_correction': line.checkin_status_correction,
                        })
                        # rec.update({
                        #     'state': 'approved',
                        # })
                    if line.hr_attendance_id and line.checkout_status_correction:
                        line.hr_attendance_id.update({
                            'hr_attendance_change_id': rec.id,
                            'checkout_status_correction': line.checkout_status_correction,
                        })
                        # rec.update({
                        #     'state': 'approved',
                        # })
            rec.approve_attendance_change_schedule_by_list()

    def approve_attendance_change_schedule_by_list(self):
        for record in self:
            current_user = self.env.uid
            setting = self.env['ir.config_parameter'].sudo().get_param(
                'equip3_hr_attendance_extend.attendance_type_approval')
            now = datetime.now(timezone(self.env.user.tz))
            dateformat = f"{now.day}/{now.month}/{now.year} {now.hour}:{now.minute}:{now.second}"
            if setting == 'employee_hierarchy':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for user in record.attendance_change_user_ids:
                            if current_user == user.user_ids.id:
                                user.is_approve = True
                                user.timestamp = fields.Datetime.now()
                                user.approver_state = 'approved'
                                string_approval = []
                                if user.approval_status:
                                    string_approval.append(f"{self.env.user.name}:Approved")
                                    user.approval_status = "\n".join(string_approval)
                                    string_timestammp = [user.approved_time]
                                    string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                    user.approved_time = "\n".join(string_timestammp)
                                    if record.feedback_parent:
                                        feedback_list = [user.feedback,
                                                         f"{self.env.user.name}:{record.feedback_parent}"]
                                        final_feedback = "\n".join(feedback_list)
                                        user.feedback = f"{final_feedback}"
                                    elif user.feedback and not record.feedback_parent:
                                        user.feedback = user.feedback
                                    else:
                                        user.feedback = ""
                                else:
                                    user.approval_status = f"{self.env.user.name}:Approved"
                                    user.approved_time = f"{self.env.user.name}:{dateformat}"
                                    if record.feedback_parent:
                                        user.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                    else:
                                        user.feedback = ""
                                record.approved_user_ids = [(4, current_user)]
                        matrix_line = sorted(record.attendance_change_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            self.approved_mail()
                            record.write({'state': 'approved'})
                        else:
                            self.approver_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has been approved the Request!'
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved'
                    ))
            elif setting == 'approval_matrix':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for line in record.attendance_change_user_ids:
                            for user in line.user_ids:
                                if current_user == user.user_ids.id:
                                    line.timestamp = fields.Datetime.now()
                                    record.approved_user_ids = [(4, current_user)]
                                    var = len(line.approved_employee_ids) + 1
                                    if line.minimum_approver <= var:
                                        line.approver_state = 'approved'
                                        string_approval = []
                                        string_approval.append(line.approval_status)
                                        if line.approval_status:
                                            string_approval.append(f"{self.env.user.name}:Approved")
                                            line.approval_status = "\n".join(string_approval)
                                            string_timestammp = [line.approved_time]
                                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                            line.approved_time = "\n".join(string_timestammp)
                                            if record.feedback_parent:
                                                feedback_list = [line.feedback,
                                                                 f"{self.env.user.name}:{record.feedback_parent}"]
                                                final_feedback = "\n".join(feedback_list)
                                                line.feedback = f"{final_feedback}"
                                            elif line.feedback and not record.feedback_parent:
                                                line.feedback = line.feedback
                                            else:
                                                line.feedback = ""
                                        else:
                                            line.approval_status = f"{self.env.user.name}:Approved"
                                            line.approved_time = f"{self.env.user.name}:{dateformat}"
                                            if record.feedback_parent:
                                                line.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                            else:
                                                line.feedback = ""
                                        line.is_approve = True
                                    else:
                                        line.approver_state = 'pending'
                                        if line.approval_status:
                                            string_approval.append(f"{self.env.user.name}:Approved")
                                            line.approval_status = "\n".join(string_approval)
                                            string_timestammp = [line.approved_time]
                                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                                            line.approved_time = "\n".join(string_timestammp)
                                            if record.feedback_parent:
                                                feedback_list = [line.feedback,
                                                                 f"{self.env.user.name}:{record.feedback_parent}"]
                                                final_feedback = "\n".join(feedback_list)
                                                line.feedback = f"{final_feedback}"
                                            elif line.feedback and not record.feedback_parent:
                                                line.feedback = line.feedback
                                            else:
                                                line.feedback = ""
                                        else:
                                            line.approval_status = f"{self.env.user.name}:Approved"
                                            line.approved_time = f"{self.env.user.name}:{dateformat}"
                                            if record.feedback_parent:
                                                line.feedback = f"{self.env.user.name}:{record.feedback_parent}"
                                            else:
                                                line.feedback = ""
                                    line.approved_employee_ids = [(4, current_user)]

                        matrix_line = sorted(record.attendance_change_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Request!'
                            self.approved_mail()
                            record.write({'state': 'approved'})
                        else:
                            self.approver_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Request!'
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved!'
                    ))
            else:
                raise ValidationError(_(
                    'Already approved!'
                ))

    def action_refuse(self):
        for record in self:
            for user in record.attendance_change_user_ids:
                for check_user in user.user_ids:
                    now = datetime.now(timezone(self.env.user.tz))
                    dateformat = f"{now.day}/{now.month}/{now.year} {now.hour}:{now.minute}:{now.second}"
                    if self.env.uid == check_user.id:
                        user.timestamp = fields.Datetime.now()
                        user.approver_state = 'refuse'
                        string_approval = []
                        string_approval.append(user.approval_status)
                        if user.approval_status:
                            string_approval.append(f"{self.env.user.name}:Refused")
                            user.approval_status = "\n".join(string_approval)
                            string_timestammp = [user.approved_time]
                            string_timestammp.append(f"{self.env.user.name}:{dateformat}")
                            user.approved_time = "\n".join(string_timestammp)
                        else:
                            user.approval_status = f"{self.env.user.name}:Refused"
                            user.approved_time = f"{self.env.user.name}:{dateformat}"
            self.reject_mail()
            record.approved_user = self.env.user.name + ' ' + 'has been Rejected!'
            record.write({'state': 'refused'})

    def wizard_approve(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'hr.attendance.change.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'name': "Confirmation Message",
            'target': 'new',
        }
    # Dont delete, validation warning for reason field
    # @api.constrains('attendance_change_line_ids')
    # def _check_reason(self):
    #     for rec in self:
    #         if rec.attendance_change_line_ids:
    #             for line in rec.attendance_change_line_ids:
    #                 if line.hr_attendance_id and line.check_in_correction != 0.0 and not line.reason :
    #                     raise ValidationError("Please fill the reason field.")
    #                 elif line.hr_attendance_id and line.check_out_correction != 0.0 and not line.reason:
    #                     raise ValidationError("Please fill the reason field")
    @api.constrains('request_date_from')
    def _check_request_date_from(self):
        for rec in self:
            if rec.request_date_from and rec.request_date_from  >= date.today():
                raise ValidationError("Cannot changes attendance data on a date that has not passed")

class HrAttendanceChangeLine(models.Model):
    _name = 'hr.attendance.change.line'

    hr_attendance_change_id = fields.Many2one('hr.attendance.change', string="Attendance Line")
    date = fields.Date('Date')
    resource_calendar_id = fields.Many2one('resource.calendar', 'Working Schedule')
    check_in = fields.Float(string="Original Check In")
    check_out = fields.Float(string="Original Check Out")
    checkin_status = fields.Selection(
        [('early', 'Early'), ('ontime', 'Ontime'), ('late', 'Late'), ('no_checking', 'No Checkin')],
        string='Checkin Status' )
    checkout_status = fields.Selection(
        [('early', 'Early'), ('ontime', 'Ontime'), ('late', 'Late'), ('no_checkout', 'No Checkout')],
        string='Checkout Status')
    check_in_correction = fields.Float(string="Check In Correction", tracking=True)
    check_out_correction = fields.Float(string="Check Out Correction", tracking=True)
    checkin_status_correction = fields.Selection(
        [('early', 'Early'), ('ontime', 'Ontime'), ('late', 'Late'), ('no_checking', 'No Checkin')],
        string='Checkin Status Correction')
    checkout_status_correction = fields.Selection(
        [('early', 'Early'), ('ontime', 'Ontime'), ('late', 'Late'), ('no_checkout', 'No Checkout')],
        string='Checkout Status Correction')
    reason = fields.Text(string='Reason', tracking=True)
    hr_attendance_id = fields.Many2one('hr.attendance', string="Attendance")

class AttendanceChangeApproverUser(models.Model):
    _name = 'attendance.change.approver.user'

    attendance_change_approver_id = fields.Many2one('hr.attendance.change', string="Advance Id")
    name = fields.Integer('Sequence', compute="fetch_sl_no")
    user_ids = fields.Many2many('res.users', string="Approvers")
    approved_employee_ids = fields.Many2many('res.users', 'attendance_change_app_emp_ids', string="Approved user")
    minimum_approver = fields.Integer(string="Minimum Approver", default=1)
    timestamp = fields.Datetime(string="Timestamp")
    approved_time = fields.Text(string="Timestamp")
    feedback = fields.Text()
    approver_state = fields.Selection([('draft', 'Draft'), ('pending', 'Pending'), ('approved', 'Approved'),
                                       ('refuse', 'Refused')], default='draft', string="Approval Status")
    approval_status = fields.Text()
    is_approve = fields.Boolean(string="Is Approve", default=False)

    @api.depends('attendance_change_approver_id')
    def fetch_sl_no(self):
        sl = 0
        for line in self.attendance_change_approver_id.attendance_change_user_ids:
            sl = sl + 1
            line.name = sl