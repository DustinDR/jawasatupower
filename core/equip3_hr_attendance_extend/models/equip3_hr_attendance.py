# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import json
from odoo import models, fields, api, _
import datetime
from datetime import date, datetime, timedelta
import pytz
import requests
from odoo.exceptions import UserError
import logging
from dateutil.relativedelta import relativedelta
from lxml import etree

logger = logging.getLogger(__name__)


class HrAttendance(models.Model):
    _inherit = "hr.attendance"

    def _get_attendance_status(self):
        return self.env['ir.config_parameter'].sudo().get_param('equip3_hr_attendance_extend.attendance_status') or ''

    active = fields.Boolean(default=True)
    is_created = fields.Boolean('Created', default=False)
    check_in = fields.Datetime(string="Check In", default=fields.Datetime.now, required=False)
    sequence_code = fields.Char(string='Employee ID', related="employee_id.sequence_code",
                                readonly=True, store=True)
    department_id = fields.Many2one('hr.department', string='Department', related="employee_id.department_id",
                                    readonly=True, store=True)
    attendance_status = fields.Selection([('present', 'Present'), ('absent', 'Absent'), ('leave', 'Leave'),
                                          ('travel', 'Travel')],
                                         string='Attendance Status', readonly=True, default=_get_attendance_status)
    checkin_status = fields.Selection(
        [('early', 'Early'), ('ontime', 'Ontime'), ('late', 'Late'), ('no_checking', 'No Checkin')],
        string='Checkin Status', compute='_compute_worked_status', store=True)
    checkout_status = fields.Selection(
        [('early', 'Early'), ('ontime', 'Ontime'), ('late', 'Late'), ('no_checkout', 'No Checkout')],
        string='Checkout Status', compute='_compute_worked_status', store=True)
    minimum_hours = fields.Float(string='Minimum Hours', compute='_compute_minimum_work_hrs', readonly=True, store=True)
    check_in_diff = fields.Float(string='Check In Difference', compute='_compute_worked_status',
                                 readonly=True, store=True)
    check_out_diff = fields.Float(string='Check Out Difference', compute='_compute_worked_status',
                                  readonly=True, store=True)
    hr_attendance_change_id = fields.Many2one('hr.attendance.change', string='Attendance Change', readonly=True)
    check_in_address = fields.Char('Check In Address', compute="_get_address", store=True, readonly=True)
    check_out_address = fields.Char('Check Out Address', compute="_get_address", store=True, readonly=True)
    calendar_id = fields.Many2one('employee.working.schedule.calendar', string='Calendar', compute='_compute_calendar')
    leave_id = fields.Many2one('hr.leave', string='Leave')
    start_working_times = fields.Datetime(string='Start Working Times')
    start_working_date = fields.Date(string='Working Date', default=fields.date.today())
    hour_from = fields.Float(string='Work From', related='calendar_id.hour_from', store=True)
    hour_to = fields.Float(string='Work To', related='calendar_id.hour_to', store=True)
    tolerance_late = fields.Float(string='Tolerance for Late',
                                  related="calendar_id.tolerance_late", store=True)
    job_id = fields.Many2one('hr.job', related='employee_id.job_id', string='Job Positions', store=True)
    active_location_id = fields.Many2one('res.partner', related='employee_id.active_location', string='Active Location',
                                         store=True)
    date_localization = fields.Date('Date Localization', related="employee_id.user_id.partner_id.date_localization",
                                    store=True)
    partner_latitude = fields.Float(string='Partner Latitude',
                                    related="employee_id.user_id.partner_id.partner_latitude", store=True)
    partner_longitude = fields.Float(string='Partner Longitude',
                                     related="employee_id.user_id.partner_id.partner_longitude", store=True)
    is_holiday = fields.Boolean(string='Is Holiday', default=False)
    holiday_remark = fields.Char(string='Holiday Remark')
    checkin_status_correction = fields.Selection(
        [('early', 'Early'), ('ontime', 'Ontime'), ('late', 'Late'), ('no_checking', 'No Checkin')],
        string='Checkin Status Correction')
    checkout_status_correction = fields.Selection(
        [('early', 'Early'), ('ontime', 'Ontime'), ('late', 'Late'), ('no_checkout', 'No Checkout')],
        string='Checkout Status Correction')
    working_schedule_id = fields.Many2one('resource.calendar', string='Working Schedule', compute='_compute_working_schedule')
    is_absent = fields.Boolean('is Absent', default=False)
    is_absent_email_sent = fields.Boolean('is Absent Email Sent', default=False)

    def _compute_working_schedule(self):
        for rec in self:
            contract = self.env['hr.contract'].search([('employee_id', '=', rec.employee_id.id),], order='id desc', limit=1)
            if contract:
                rec.working_schedule_id = contract.resource_calendar_id
            else:
                rec.working_schedule_id = False


    @api.model
    def fields_view_get(self, view_id=None, view_type=None,
                        toolbar=True, submenu=True):
        res = super(HrAttendance, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if not self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_attendance_hr_manager'):
            root = etree.fromstring(res['arch'])
            root.set('create', 'false')
            root.set('edit', 'false')
            root.set('delete', 'false')
            res['arch'] = etree.tostring(root)
        return res

    @api.constrains('check_in', 'check_out', 'employee_id')
    def _check_validity(self):
        """ Verifies the validity of the attendance record compared to the others from the same employee.
            For the same employee we must have :
                * maximum 1 "open" attendance record (without check_out)
                * no overlapping time slices with previous employee records
        """
        for attendance in self:
            # we take the latest attendance before our check_in time and check it doesn't overlap with ours
            last_attendance_before_check_in = self.env['hr.attendance'].search([
                ('employee_id', '=', attendance.employee_id.id),
                ('check_in', '<=', attendance.check_in),
                ('id', '!=', attendance.id),
            ], order='check_in desc', limit=1)
            # if last_attendance_before_check_in and last_attendance_before_check_in.check_out and last_attendance_before_check_in.check_out > attendance.check_in:
            #     raise exceptions.ValidationError(
            #         _("Cannot create new attendance record for %(empl_name)s, the employee was already checked in on %(datetime)s") % {
            #             'empl_name': attendance.employee_id.name,
            #             'datetime': format_datetime(self.env, attendance.check_in, dt_format=False),
            #         })

            if not attendance.check_out:
                # if our attendance is "open" (no check_out), we verify there is no other "open" attendance
                no_check_out_attendances = self.env['hr.attendance'].search([
                    ('employee_id', '=', attendance.employee_id.id),
                    ('check_out', '=', False),
                    ('id', '!=', attendance.id),
                ], order='check_in desc', limit=1)
                # if no_check_out_attendances:
                #     raise exceptions.ValidationError(
                #         _("Cannot create new attendance record for %(empl_name)s, the employee hasn't checked out since %(datetime)s") % {
                #             'empl_name': attendance.employee_id.name,
                #             'datetime': format_datetime(self.env, no_check_out_attendances.check_in, dt_format=False),
                #         })
            else:
                # we verify that the latest attendance with check_in time before our check_out time
                # is the same as the one before our check_in time computed before, otherwise it overlaps
                last_attendance_before_check_out = self.env['hr.attendance'].search([
                    ('employee_id', '=', attendance.employee_id.id),
                    ('check_in', '<', attendance.check_out),
                    ('id', '!=', attendance.id),
                ], order='check_in desc', limit=1)
                # if last_attendance_before_check_out and last_attendance_before_check_in != last_attendance_before_check_out:
                #     raise exceptions.ValidationError(
                #         _("Cannot create new attendance record for %(empl_name)s, the employee was already checked in on %(datetime)s") % {
                #             'empl_name': attendance.employee_id.name,
                #             'datetime': format_datetime(self.env, last_attendance_before_check_out.check_in,
                #                                         dt_format=False),
                #         })

    @api.depends('start_working_date')
    def _compute_calendar(self):
        for rec in self:
            schedule = self.env['employee.working.schedule.calendar'].search(
                [('employee_id', '=', rec.employee_id.id), ('date_start', '=', rec.start_working_date)], limit=1)
            if schedule:
                rec.calendar_id = schedule.id
                rec.start_working_date = schedule.date_start
            else:
                rec.calendar_id = False

    @api.depends('check_in', 'check_out')
    def _compute_minimum_work_hrs(self):
        for attendance in self:
            res_calendar = attendance.employee_id.resource_calendar_id.attendance_ids
            if res_calendar:
                # Checkin
                if attendance.check_in:
                    res_checkin = attendance.check_in
                    checkin_day_name = res_checkin.strftime("%A")
                    if checkin_day_name == 'Monday':
                        day_name_in = '0'
                    elif checkin_day_name == 'Tuesday':
                        day_name_in = '1'
                    elif checkin_day_name == 'Wednesday':
                        day_name_in = '2'
                    elif checkin_day_name == 'Thursday':
                        day_name_in = '3'
                    elif checkin_day_name == 'Friday':
                        day_name_in = '4'
                    elif checkin_day_name == 'Saturday':
                        day_name_in = '5'
                    else:
                        day_name_in = '6'
                    for calendar_data in res_calendar:
                        if calendar_data.dayofweek == day_name_in:
                            attendance.minimum_hours = calendar_data.minimum_hours

    @api.depends('check_in', 'check_out', 'calendar_id')
    def _compute_worked_status(self):
        for attendance in self:
            if attendance.calendar_id:
                checkin_ontime = attendance.calendar_id.checkin + relativedelta(
                    hours=attendance.calendar_id.tolerance_late)
                checkout_ontime = attendance.calendar_id.checkout + relativedelta(
                    hours=attendance.calendar_id.tolerance_late)
                attendance.check_in_diff = 0
                attendance.check_out_diff = 0
                # Checkin Status Calculation
                if attendance.hr_attendance_change_id and attendance.checkin_status_correction:  # if 'My Attendance change req'
                    attendance.checkin_status = attendance.checkin_status_correction
                else:
                    if not attendance.check_in:  # no check in
                        attendance.checkin_status = 'no_checking'
                    elif attendance.check_in < attendance.calendar_id.checkin:  # Early
                        attendance.checkin_status = 'early'
                    elif attendance.check_in > checkin_ontime:  # late
                        attendance.checkin_status = 'late'
                    elif attendance.check_in >= attendance.calendar_id.checkin <= checkin_ontime:  # ontime
                        attendance.checkin_status = 'ontime'

                # Checkin Difference calculation
                if not attendance.check_in:
                    attendance.check_in_diff = attendance.check_in_diff
                elif attendance.check_in < attendance.calendar_id.checkin:  # Early
                    attendance.check_in_diff = (attendance.check_in - checkin_ontime).total_seconds() / 3600
                elif attendance.check_in > checkin_ontime:  # late
                    attendance.check_in_diff = (attendance.check_in - checkin_ontime).total_seconds() / 3600
                else:
                    attendance.check_in_diff = attendance.check_in_diff

                # CheckOut Status Calculation
                if attendance.hr_attendance_change_id and attendance.checkout_status_correction:  # if 'My Attendance change req'
                    attendance.checkout_status = attendance.checkout_status_correction
                else:
                    if not attendance.check_out:
                        attendance.checkout_status = 'no_checkout'
                    elif attendance.check_out < attendance.calendar_id.checkout:  # Early
                        attendance.checkout_status = 'early'
                    elif attendance.check_out > checkout_ontime:  # late
                        attendance.checkout_status = 'late'
                    elif attendance.check_out >= attendance.calendar_id.checkout <= checkout_ontime:  # ontime
                        attendance.checkout_status = 'ontime'

                # CheckOut Difference calculation
                if not attendance.check_out:
                    attendance.check_out_diff = attendance.check_out_diff
                elif attendance.check_out < attendance.calendar_id.checkout:  # Early
                    attendance.check_out_diff = (
                                                        attendance.calendar_id.checkout - attendance.check_out).total_seconds() / 3600
                elif attendance.check_out > checkout_ontime:  # late
                    attendance.check_out_diff = (
                                                        attendance.calendar_id.checkout - attendance.check_out).total_seconds() / 3600
                else:
                    attendance.check_out_diff = attendance.check_out_diff
            else:
                attendance.checkin_status = 'no_checking'
                attendance.checkout_status = 'no_checkout'

    @api.depends('check_in_latitude', 'check_in_longitude', 'check_out_latitude', 'check_out_longitude')
    def _get_address(self):
        for attendance in self:
            self.env['ir.config_parameter'].set_param('gmaps_reverse_geocoding_token',
                                                      'AIzaSyDVSmaYy-QzDU0yANwsZv2lURQGMN3vnMU')
            api_key = self.env['ir.config_parameter'].get_param('gmaps_reverse_geocoding_token')
            if api_key:
                if attendance.check_in_latitude != 0 and attendance.check_in_longitude != 0:
                    response = requests.get(
                        'https://maps.googleapis.com/maps/api/geocode/json?latlng={},{}&key={}'.format(
                            attendance.check_in_latitude, attendance.check_in_longitude, api_key))
                    if response.json().get('status') == 'OK':
                        attendance.update({
                            'check_in_address': response.json().get('results')[0].get('formatted_address'),
                        })
                else:
                    attendance.check_in_address = '-'

                if attendance.check_out_latitude != 0 and attendance.check_out_longitude != 0:
                    response = requests.get(
                        'https://maps.googleapis.com/maps/api/geocode/json?latlng={},{}&key={}'.format(
                            attendance.check_out_latitude, attendance.check_out_longitude, api_key))
                    if response.json().get('status') == 'OK':
                        attendance.update({
                            'check_out_address': response.json().get('results')[0].get('formatted_address'),
                        })
                else:
                    attendance.check_out_address = '-'
            else:
                raise Warning(_('Google Reverse GeoCoding Token Not found!!'))

    @api.model
    def _prepare_url(self, url, replace):
        assert url, 'Missing URL'
        for key, value in replace.items():
            if not isinstance(value, str):
                # for latitude and longitude which are floats
                value = str(value)
            url = url.replace(key, value)
        logger.debug('Final URL: %s', url)
        return url

    def open_map_check_in(self):
        self.ensure_one()
        map_website = "https://maps.google.com/maps?z=17&t=m&q={LATITUDE},{LONGITUDE}"
        if (hasattr(self, 'check_in_latitude') and
                self.check_in_latitude and self.check_in_longitude):
            url = self._prepare_url(
                map_website, {
                    '{LATITUDE}': self.check_in_latitude,
                    '{LONGITUDE}': self.check_in_longitude})
        else:
            raise UserError(
                _("Missing parameter 'Latitude / Longitude' "
                  "for Check-in"))
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def open_map_check_out(self):
        self.ensure_one()
        map_website = "https://maps.google.com/maps?z=17&t=m&q={LATITUDE},{LONGITUDE}"
        if (hasattr(self, 'check_out_latitude') and
                self.check_out_latitude and self.check_out_longitude):
            url = self._prepare_url(
                map_website, {
                    '{LATITUDE}': self.check_out_latitude,
                    '{LONGITUDE}': self.check_out_longitude})
        else:
            raise UserError(
                _("Missing parameter 'Latitude / Longitude' "
                  "for Check-out"))
        return {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def _cron_update_attendance_status(self):
        one_week = date.today() - relativedelta(weeks=1)
        for employee in self.env['hr.employee'].search([('active', '=', True)]):
            for attendance in self.env['hr.attendance'].search(
                    [('employee_id', '=', employee.id), ('is_created', '!=', True), ('active', '=', True)]):
                attendance_checkin = self.env['hr.attendance'].search(
                    [('employee_id', '=', employee.id), ('start_working_date', '=', attendance.start_working_date)],
                    order='check_in', limit=1)
                attendance_checkout = self.env['hr.attendance'].search(
                    [('employee_id', '=', employee.id), ('start_working_date', '=', attendance.start_working_date)],
                    order='check_out desc', limit=1)
                check_in = attendance_checkin.check_in
                check_out = attendance_checkout.check_out
                attendance_exist = self.env['hr.attendance'].search(
                    [('employee_id', '=', attendance.employee_id.id),
                     ('start_working_date', '=', attendance.start_working_date), ('is_created', '=', True),
                     ('active', '=', True)])
                if not attendance_exist:
                    self.env['hr.attendance'].create({
                        'employee_id': employee.id,
                        'check_in': check_in,
                        'check_out': check_out,
                        'start_working_date': attendance.start_working_date,
                        'is_created': True,
                        'attendance_status': 'present'
                    })
                attendance.active = False

            for employee_calendar in self.env['employee.working.schedule.calendar'].search(
                    [('employee_id', '=', employee.id), ('date_start', '>=', one_week),
                     ('date_start', '<', date.today())]):
                if employee_calendar.checkout.date() < date.today():
                    attendance = self.env['hr.attendance'].search(
                        [('employee_id', '=', employee_calendar.employee_id.id),
                         ('start_working_date', '=', employee_calendar.date_start)])
                    for vals in attendance:
                        if not vals.check_in:
                            vals.attendance_status = 'absent'
                            vals.is_absent = True
                        if not vals.check_out:
                            vals.attendance_status = 'absent'
                            vals.is_absent = True
                    if not attendance:
                        self.env['hr.attendance'].create({
                            'employee_id': employee.id,
                            'check_in': False,
                            'check_out': False,
                            'start_working_times': employee_calendar.checkin,
                            'start_working_date': employee_calendar.date_start,
                            'calendar_id': employee_calendar.id,
                            'attendance_status': 'absent',
                            'is_absent': True
                        })
            one_month = date.today() - relativedelta(months=1)
            for leave in self.env['hr.leave'].search(
                    [('employee_id', '=', employee.id), ('request_date_from', '>=', one_month),
                     ('request_date_from', '<=', date.today()), ('state', '=', 'validate')]):
                start_leave = leave.request_date_from
                while start_leave <= leave.request_date_to:
                    if start_leave <= date.today():
                        attendance_leave = self.env['hr.attendance'].search(
                            [('employee_id', '=', leave.employee_id.id),
                             ('start_working_date', '=', start_leave)])
                        attendance_leave.attendance_status = 'leave'
                        if not attendance_leave:
                            self.env['hr.attendance'].create({
                                'employee_id': employee.id,
                                'check_in': False,
                                'check_out': False,
                                'start_working_date': start_leave,
                                'leave_id': leave.id,
                                'attendance_status': 'leave'
                            })
                    start_leave += relativedelta(days=1)
        self.sub_cron_attendance_absent()

    def _cron_update_employee_calendar(self):
        one_week = date.today() - relativedelta(weeks=1)
        for employee in self.env['hr.employee'].search(
                [('active', '=', True), ('contract_id.date_start', '>=', one_week)]):
            calendar = self.env['hr.generate.workingcalendar'].search(
                [('employee_ids', 'in', employee.id), ('date', '>=', one_week)])
            if not calendar:
                current_year = date.today().year
                working_calendar = self.env['hr.generate.workingcalendar'].create({
                    'generate_type': 'create_update',
                    'employee_ids': employee.ids,
                    'follow_contract_period': False,
                    'start_date': employee.contract_id.date_start,
                    'end_date': employee.contract_id.date_end or date(current_year, 12, 31),
                })
                working_calendar.action_generate()

    def sub_cron_attendance_absent(self):
        current_date = date.today()

        for attendance in self.env['hr.attendance'].search(
                [('employee_id.active', '=', True), ('is_absent', '=', True), ('is_absent_email_sent', '!=', True),
                 ('active', '=', True), ('attendance_status', '=', 'absent')]):
            attendance.attendance_absent_mail()
            attendance.attendance_notify_absent_mail()
            attendance.is_absent = False
            attendance.is_absent_email_sent = True

        for attendance in self.env['hr.attendance'].search([('employee_id.active', '=', True), ('active', '=', True)]):
            one_day = attendance.start_working_date + relativedelta(days=1)
            if one_day == current_date and attendance.minimum_hours > attendance.worked_hours:
                attendance.attendance_working_under_minimum_hours_mail()
                attendance.attendance_notify_working_under_minimum_hours_mail()

    def attendance_absent_mail(self):
        ir_model_data = self.env['ir.model.data']
        template_id = ir_model_data.get_object_reference('equip3_hr_attendance_extend', 'email_template_attendance_absent')[1]
        ctx = self._context.copy()
        ctx.update({
            'email_from': self.env.user.email,
            'email_to': self.employee_id.user_id.email,
        })
        if self.start_working_date:
            ctx.update(
                {'working_date': fields.Datetime.from_string(self.start_working_date).strftime('%d/%m/%Y')})
        self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                  force_send=True)

    def attendance_notify_absent_mail(self):
        ir_model_data = self.env['ir.model.data']
        template_id = ir_model_data.get_object_reference('equip3_hr_attendance_extend', 'email_template_notify_attendance_absent')[1]
        ctx = self._context.copy()
        ctx.update({
            'email_from': self.env.user.email,
            'email_to': self.employee_id.parent_id.user_id.email,
        })
        if self.start_working_date:
            ctx.update(
                {'working_date': fields.Datetime.from_string(self.start_working_date).strftime('%d/%m/%Y')})
        self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                  force_send=True)

    def attendance_working_under_minimum_hours_mail(self):
        ir_model_data = self.env['ir.model.data']
        template_id = ir_model_data.get_object_reference('equip3_hr_attendance_extend', 'email_template_working_under_minimum_hours')[1]
        ctx = self._context.copy()
        ctx.update({
            'email_from': self.env.user.email,
            'email_to': self.employee_id.user_id.email,
        })
        if self.start_working_date:
            ctx.update(
                {'working_date': fields.Datetime.from_string(self.start_working_date).strftime('%d/%m/%Y')})
        if self.minimum_hours:
            ctx.update(
                {'minimum_hrs': round(self.minimum_hours, 2)})
        if self.worked_hours:
            ctx.update(
                {'worked_hrs': round(self.worked_hours, 2)})
        self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                  force_send=True)

    def attendance_notify_working_under_minimum_hours_mail(self):
        ir_model_data = self.env['ir.model.data']
        template_id = ir_model_data.get_object_reference('equip3_hr_attendance_extend', 'email_template_notify_working_under_minimum_hours')[1]
        ctx = self._context.copy()
        ctx.update({
            'email_from': self.env.user.email,
            'email_to': self.employee_id.parent_id.user_id.email,
        })
        if self.start_working_date:
            ctx.update(
                {'working_date': fields.Datetime.from_string(self.start_working_date).strftime('%d/%m/%Y')})
        if self.minimum_hours:
            ctx.update(
                {'minimum_hrs': round(self.minimum_hours, 2)})
        if self.worked_hours:
            ctx.update(
                {'worked_hrs': round(self.worked_hours, 2)})
        self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                  force_send=True)



class resource_calendar_attendance_in(models.Model):
    _inherit = 'resource.calendar.attendance'

    minimum_hours = fields.Float(string='Minimum Hours', required=0)
