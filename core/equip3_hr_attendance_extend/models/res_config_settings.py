# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        param_obj = self.env['ir.config_parameter']
        res.update({'attendance_status': param_obj.sudo().get_param('equip3_hr_attendance_extend.attendance_status'),
                    })
        return res

    def set_values(self):
        res = super(ResConfigSettings, self).set_values()
        param_obj = self.env['ir.config_parameter']
        param_obj.sudo().set_param('equip3_hr_attendance_extend.attendance_status', self.attendance_status)

    attendance_status = fields.Selection([('present', 'Present'), ('absent', 'Absent'), ('leave', 'Leave')],
                                         string='Default Attendance Status', readonly=False)
    attendance_type_approval = fields.Selection(
        [('employee_hierarchy', 'By Employee Hierarchy'), ('approval_matrix', 'By Approval Matrix')], default='employee_hierarchy',
        config_parameter='equip3_hr_attendance_extend.attendance_type_approval')
    attendance_level = fields.Integer(config_parameter='equip3_hr_attendance_extend.attendance_level', default=1)
    attendance_validation = fields.Boolean(config_parameter='equip3_hr_attendance_extend.attendance_validation')

    @api.onchange("attendance_level")
    def _onchange_attendance_level(self):
        if self.attendance_level < 1:
            self.attendance_level = 1