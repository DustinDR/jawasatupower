# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import equip3_hr_attendance
from . import res_config_settings
from . import hr_attendance_change
from . import hr_employee
from . import hr_attendance_approval_matrix
from . import hr_schedule_exchange
from . import resource_calendar
