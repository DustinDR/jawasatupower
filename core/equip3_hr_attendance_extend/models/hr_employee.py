# -*- coding: utf-8 -*-
from geopy import distance
from odoo import api, fields, models, _


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    def attendance_manual(self, next_action, entered_pin=False, location=False):
        res = super(HrEmployee, self.with_context(att_location=location)).attendance_manual(next_action, entered_pin)
        latitude = float(location[0])
        longitude = float(location[1])
        params = self.env['ir.config_parameter'].sudo()
        att_range = int(params.get_param('hr_attendance_location_knk.attendance_range', default=50))
        if latitude is not None and longitude is not None:
            act_latitude = self.sudo().active_location.partner_latitude
            act_longitude = self.sudo().active_location.partner_longitude
            if act_latitude and act_longitude:
                pdistance = distance.distance((act_latitude, act_longitude), (latitude, longitude)).km
                if att_range == 0:
                    return res
                elif (pdistance * 1000) <= att_range:
                    return res
                else:
                    return {'warning': _("You can only do check in/out within Active Location range")}
        return res

    def _attendance_action_change(self):
        res = super()._attendance_action_change()
        location = self.env.context.get("att_location", False)
        face_recognition_store = self.env['ir.config_parameter'].sudo(
        ).get_param('hr_attendance_face_recognition_store')
        snapshot = False
        if face_recognition_store:
            snapshot = self.env.context.get("webcam", False)
        if location:
            if self.attendance_state == "checked_in":
                res.write(
                    {
                        "check_in_latitude": location[0],
                        "check_in_longitude": location[1],
                        'face_recognition_access_check_in':snapshot,
                    }
                )
            else:
                res.write(
                    {
                        "check_out_latitude": location[0],
                        "check_out_longitude": location[1],
                        'face_recognition_access_check_out':snapshot,
                    }
                )
        return res
