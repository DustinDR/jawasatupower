from odoo import fields, models, api


class HrWorkingSheetWizard(models.TransientModel):
    _name = 'hr.working.sheet.wizard'

    feedback = fields.Text()

    def submit(self):
        """ Prepare the Working Schedule feedback and trigger Approve. """
        self.ensure_one()
        hr_working = self.env['schedule.exchange'].browse(self._context.get('active_ids', []))
        hr_working.feedback_parent = self.feedback
        hr_working.action_approve()
