from odoo import models, fields, api
from odoo.exceptions import UserError


class CuttingOrderInherit(models.Model):
    _inherit = 'cutting.order'

    def button_test(self):
        product_type = self.env.context.get('product_type')
        is_lot = product_type == 'lot'
        quantity = 10 if is_lot else 1

        name = 'AUTO-%s' % product_type.upper()
        number = 1
        while True:
            if not self.env['product.product'].search([('name', '=', name + str(number))]):
                name += str(number)
                break
            number += 1

        product_values = {
            'name': name,
            'type': 'product',
            'tracking': product_type,
            'is_in_autogenerate': False,
            'is_sn_autogenerate': False,
            'is_cutting_product': True,
            'cutting_unit_measure': self.env['uom.uom'].search([('name', '=', 'Kodi')], limit=1).id,
            'standard_price': 10000
        }
        product_id = self.env['product.product'].create(product_values)

        picking = self.env['stock.picking'].create({
            "is_locked": True,
            "immediate_transfer": False,
            "priority": "0",
            "partner_id": False,
            "picking_type_id": 1,
            "location_id": 4,
            "location_dest_id": 8,
            "analytic_account_group_ids": [
                [
                    6,
                    False,
                    [
                        1,
                        2
                    ]
                ]
            ],
            "is_a_delivery": False,
            "subcon_production_id": False,
            "subcon_qty_producing": 0,
            "scheduled_date": "2022-03-25 05:34:12",
            "rma_id": False,
            "origin": False,
            "kitchen_id": False,
            "is_readonly_origin": False,
            "company_id": 1,
            "report_template_id1": False,
            "report_template_id": False,
            "branch_id": 1,
            "owner_id": False,
            "sh_stock_barcode_mobile": False,
            "move_ids_without_package": [
                [
                    0,
                    0,
                    {
                        "company_id": 1,
                        "name": product_id.name,
                        "state": "draft",
                        "picking_type_id": 1,
                        "location_id": 4,
                        "location_dest_id": 8,
                        "additional": False,
                        "sequence": 3,
                        "move_line_sequence": "1",
                        "product_id": product_id.id,
                        "package_type": False,
                        "analytic_account_group_ids": [
                            [
                                6,
                                False,
                                [
                                    1,
                                    2
                                ]
                            ]
                        ],
                        "scheduled_date": False,
                        "partner_id": False,
                        "responsible": False,
                        "description_picking": False,
                        "date": "2022-03-25 05:34:12",
                        "initial_demand": 0,
                        "remaining": quantity,
                        "product_uom_qty": quantity,
                        "sh_quality_point": False,
                        "sh_quality_point_id": False,
                        "quantity_done": 0,
                        "move_progress": 0,
                        "fulfillment": 0,
                        "product_uom": 1,
                        "lot_ids": [
                            [
                                6,
                                False,
                                []
                            ]
                        ]
                    }
                ]
            ],
            "package_level_ids": [],
            "package_level_ids_details": [],
            "transfer_id": False,
            "is_expired_tranfer": False,
            "carrier_id": False,
            "carrier_tracking_ref": False,
            "move_type": "direct",
            "user_id": 2,
            "attachment_ids": [
                [
                    6,
                    False,
                    []
                ]
            ],
            "cancel_reason": False,
            "note": False,
            "message_follower_ids": [],
            "activity_ids": [],
            "message_ids": []
        })

        for move in picking.move_ids_without_package:
            move_line_vals = move._prepare_move_line_vals()

            move_line_vals.update({
                'qty_done': quantity,
                'lot_name': '%s001' % product_id.name,
                'length': 6,
                'width': 4
            })
            move.move_line_ids = [(0, 0, move_line_vals)]
        picking.button_validate()

        self.product_id = product_id.id
        self.lot_ids = [(6, 0, 0)]
        self.line_ids = [(6, 0, 0)]
        self.leftover_ids = [(6, 0, 0)]
        self.onchange_workcenter_id()
