# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Equip 3 - Manufacturing Other Operations',
    'author': 'Hashmicro',
    'version': '1.1.46',
    'category': 'Manufacturing',
    'summary': 'Manufacturing Other Operations',
    'description': '''
    i. Unbuild Order
    ii. Material to Purchase
    iii. Manufacturing Cost Actualization
    ''',
    'author': 'HashMicro',
    'website': 'www.hashmicro.com',
    'depends': [
        'web',
        'mrp',
        'branch', 
        'account',
        'stock',
        'stock_account',
        'equip3_manuf_masterdata', 
        'equip3_manuf_operations',
        'equip3_manuf_operations_contd',
        'equip3_manuf_accessright_settings',
        'equip3_purchase_operation',
        'equip3_purchase_other_operation',
        'app_mrp_superbar'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'data/ir_sequence_data.xml',
        'views/mrp_production_view.xml',
        'views/purchase_request_views.xml',
        'views/purchase_order_views.xml',
        'views/purchase_requisition_views.xml',
        'views/assets.xml',
        'views/mrp_unbuild_views.xml',
        'views/mrp_views_menus.xml',
        'views/mrp_material_purchase_views.xml',
        'views/mrp_cost_actualization_views.xml',
        'views/product_template_views.xml',
        'views/stock_quant_views.xml',
        'views/stock_valuation_layer_views.xml',
        'views/res_config_settings_views.xml',
        # 'views/stock_picking_view.xml',
        'views/mrp_workorder_view.xml',
        'views/cutting_order_views.xml',
        'views/cutting_order_line_views.xml',
        'views/cutting_order_menuitems.xml',
        'views/cutting_plan_views.xml',
        'views/stock_move_views.xml',
        'views/fg_simulation_views.xml',
        'views/mrp_consumption_views.xml',
        'wizard/mrp_subcontracting_wizard_view.xml',
        'wizard/requisition_delivery_order_views.xml',
        'wizard/cutting_plan_add_cutting_view.xml',
        'tests/cutting_order_tests.xml',
        
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'pre_init_hook': 'pre_init_hook',
}
