from odoo import fields, models, api, _
from odoo.exceptions import UserError


class MrpSubcontractingWizard(models.TransientModel):
    _name = 'mrp.subcontracting.wizard'
    _description = 'Subcontracting Wizard'

    @api.depends('production_id', 'production_id.product_id')
    def _compute_allowed_requisition_ids(self):
        requisitions = self.env['purchase.requisition'].search([])
        for record in self:
            req_ids = self.env['purchase.requisition']
            for req in requisitions:
                product_ids = req.line_ids.mapped('product_id')
                product_ids = product_ids.filtered(lambda p: p == record.production_id.product_id)
                if product_ids:
                    req_ids |= req
            record.allowed_requisition_ids = [(6, 0, req_ids.ids)]

    production_id = fields.Many2one('mrp.production', string='Manufacturing order', required=True)
    origin = fields.Char(string='Origin', related='production_id.name')
    subcon_type = fields.Selection(
        string='Options',
        selection=[('purchase.request', 'Purchase Requests'), ('purchase.order', 'Request for Quotations')],
        default='purchase.order'
    )
    uom_id = fields.Many2one('uom.uom', string='UoM', related='production_id.product_uom_id')
    product_qty = fields.Float(string='To Subcontract', digits='Product Unit of Measure')
    allowed_requisition_ids = fields.One2many('purchase.requisition', compute=_compute_allowed_requisition_ids)
    requisition_id = fields.Many2one('purchase.requisition', string='Blanket Order', domain="[('id', 'in', allowed_requisition_ids)]")

    @api.constrains('product_qty')
    def _check_requisition_id(self):
        for record in self:
            if not record.requisition_id:
                continue
            total_remaining_qty = sum(record.requisition_id.line_ids.filtered(
                lambda l: l.product_id == record.production_id.product_id
            ).mapped('qty_remaining'))
            if record.product_qty > total_remaining_qty:
                raise UserError(_('Your selected Blanket Order has remaining qty < To Subcontract!'))

    @api.constrains('product_qty')
    def _check_subcontract(self):
        for record in self:
            product_qty = record.product_qty
            production_qty = record.production_id.product_qty
            if product_qty > production_qty or product_qty <= 0:
                raise UserError(_('Quantity should be greater than 0.0 and less or equal than %s' % production_qty))

    def action_confirm(self):
        self.ensure_one()

        values = getattr(self, '_prepare_%s_vals' % self.subcon_type.replace('.', '_'))()

        res_id = self.env[self.subcon_type].search([
            ('is_a_subcontracting', '=', True),
            ('subcon_production_id', '=', self.production_id.id)
        ], limit=1).id
        if not res_id:
            res_id = self.env[self.subcon_type].create(values).id

        if self.requisition_id:
            values = self._prepare_purchase_requisition_vals(res_id)
            self.requisition_id.write(values)

        self.production_id.write({'is_subcontracted': True})
        action = {
            'type': 'ir.actions.act_window',
            'name': dict(self._fields['subcon_type'].selection).get(self.subcon_type, ''),
            'res_model': self.subcon_type,
            'target': 'new',
            'view_mode': 'form',
            'res_id': res_id
        }
        return action

    def _prepare_subcon_vals(self):
        self.ensure_one()
        values = {
            'origin': self.origin,
            'is_a_subcontracting': True,
            'subcon_production_id': self.production_id.id,
            'subcon_uom_id': self.uom_id.id,
            'subcon_product_qty': self.product_qty,
        }
        return values

    def _prepare_purchase_request_vals(self):
        self.ensure_one()

        picking_type_id = self.env.ref('stock.picking_type_in').id
        company_id = self.production_id.company_id
        branch_id = self.production_id.branch

        values = {
            'picking_type_id': picking_type_id,
            'company_id': company_id.id,
            'branch_id': branch_id.id,
            'line_ids': [],
            'is_readonly_origin': True,
            'request_date': fields.Datetime.now(),
            'is_goods_orders': True,
            'subcon_requisition_id': self.requisition_id.id,
        }
        values.update(self._prepare_subcon_vals())
        return values

    def _prepare_purchase_order_vals(self):
        self.ensure_one()

        production_id = self.production_id
        picking_type_id = self.env.ref('stock.picking_type_in')
        company_id = production_id.company_id
        branch_id = self.production_id.branch

        partner_id = self.env['res.partner']
        if production_id.bom_id.subcontractor_ids:
            partner_id = production_id.bom_id.subcontractor_ids[0]

        values = {
            'partner_id': partner_id.id,
            'picking_type_id': picking_type_id.id,
            'company_id': company_id.id,
            'branch_id': branch_id.id,
            'order_line': [],
            'state': 'draft',
            'requisition_id': self.requisition_id.id
        }
        values.update(self._prepare_subcon_vals())
        return values

    def _prepare_purchase_requisition_vals(self, purchase_id):
        self.ensure_one()

        production_id = self.production_id
        line_ids = self.requisition_id.line_ids.filtered(
            lambda l: l.product_id == production_id.product_id)

        values = {
            'line_ids': [(1, line.id, {
                'bom_id': production_id.bom_id.id,
                'wizard_qty': line.qty_remaining
            }) for line in line_ids]
        }

        if self.subcon_type == 'purchase.order':
            values.update({'purchase_id': purchase_id})

        values.update(self._prepare_subcon_vals())
        return values
