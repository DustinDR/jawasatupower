from odoo import models, fields, api, _


class PurchaseRequestLineMakePurchaseOrder(models.TransientModel):
    _inherit = 'purchase.request.line.make.purchase.order'

    @api.model
    def _prepare_purchase_order(self, picking_type, group_id, company, origin):
        vals = super(PurchaseRequestLineMakePurchaseOrder, self)._prepare_purchase_order(picking_type, group_id, company, origin)
        active_model = self.env.context.get('active_model')
        active_id = self.env.context.get('active_id')

        if active_model != 'purchase.request' or not active_id:
            return vals

        purchase_request_id = self.env[active_model].browse(active_id)

        if not purchase_request_id.is_a_subcontracting:
            return vals

        vals.update({
            'is_a_subcontracting': purchase_request_id.is_a_subcontracting,
            'subcon_production_id': purchase_request_id.subcon_production_id.id,
            'subcon_product_qty': purchase_request_id.subcon_product_qty,
            'subcon_uom_id': purchase_request_id.subcon_uom_id.id,
            'requisition_id': purchase_request_id.subcon_requisition_id.id
        })
        return vals
