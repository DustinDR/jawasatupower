from odoo import fields, models
import logging

_logger = logging.getLogger(__name__)

class CuttingPlanAddCutting(models.TransientModel):
    _name = 'cutting.plan.add.cutting.wizard'
    _description = 'Cutting Plan Add Cutting Wizard'

    cutting_plan_line_ids = fields.One2many(
        comodel_name='cutting.plan.add.cutting.line', 
        inverse_name='cutting_plan_id', 
        string='Add Cutting Lines')

    def get_default_branch(self):
        if self.env.user.pos_branch_id:
            return self.env.user.pos_branch_id.id
        else:
            branches = self.sudo().search(['|', ('user_ids', 'child_of', [self.env.user.id]), ('user_id', '=', self.env.user.id)])
            if branches:
                return branches[0].id
            else:
                _logger.info('[get_default_branch] User [ %s ] have not set Branch' % self.env.user.login)
                return None

    #button methods
    def action_confirm(self):
        _logger.info(str(self.env.user))
        _logger.info("action_confrim() triggered")
        for line in self.cutting_plan_line_ids:
            vals = {
                'product_id' : line.product_id.id,
                'workcenter_id' : line.work_center_id.id,
                'lot_ids' : [(0, 0, {
                    'lot_id' : lot.id,
                }) for lot in line.lot_ids],
                'fixed_length' : line.fixed_length,
                'fixed_width' : line.fixed_width,
                'fixed_height' : line.fixed_height,
            }
            _logger.info(str(vals))
            cutting_order = self.env['cutting.order'].create(vals)
            cutting_plan = self.env['cutting.plan'].browse(self._context.get('active_id', False))
            cutting_plan.write({'cutting_order_ids' : [(4, cutting_order.id)]})        
        #cutting_plan = self.env['cutting.plan'].browse(self._context.get('active_id', False))._get_cutting_order_line()
        cutting_plan = self.env['cutting.plan'].browse(self._context.get('active_id', False))
        cutting_orders = cutting_plan.cutting_order_ids
        for order in cutting_orders:
            for lot in order.lot_ids:
                cutting_plan.write({
                    'cutting_order_line_ids' : [(0, 0, {
                        'name' : lot.name,
                        'product_id' : lot.product_id.id,
                        'lot_id' : lot.id,
                        'product_qty' : lot.product_qty,
                        'product_uom_id' : lot.product_uom_id.id,
                        'volume' : lot.volume,
                    })]
                })
        _logger.info("action_confirm() Done")
        pass
    

class CuttingPlanAddCuttingLine(models.TransientModel):
    _name = 'cutting.plan.add.cutting.line'
    _description = 'Cutting Plan Add Cutting Line for Wizard'
    
    def get_lot_domain(self):
        lot_ids = []
        lots = self.env['stock.production.lot'].search([])
        for lot in lots:
            quants = lot.quant_ids.filtered(lambda q: q.location_id.usage == 'internal' or (q.location_id.usage == 'transit' and q.location_id.company_id))
            qty = sum(quants.mapped('quantity'))
            if qty > 0:
                lot_ids.append(lot.id)
        
        return [('id', 'in', lot_ids)]

    cutting_plan_id = fields.Many2one(
        comodel_name='cutting.plan.add.cutting.wizard',
        string='Cutting Plan',
    )

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='Product')

    work_center_id = fields.Many2one(
        comodel_name='mrp.workcenter',
        string='Work Center')

    lot_ids = fields.Many2many(
        comodel_name='stock.production.lot', 
        string='Lot/Serial Number',
        domain= get_lot_domain)

    fixed_length = fields.Boolean(string='Fixed Length')
    fixed_width = fields.Boolean(string='Fixed Width')
    fixed_height = fields.Boolean(string='Fixed Height')

        

    
