# -*- coding: utf-8 -*-
from odoo import api, SUPERUSER_ID, _

def pre_init_hook(cr):
    """Loaded before installing the module.

    None of this module's DB modifications will be available yet.

    If you plan to raise an exception to abort install, put all code inside a
    ``with cr.savepoint():`` block to avoid broken databases.

    :param openerp.sql_db.Cursor cr:
        Database cursor.
    """
    try:
        env = api.Environment(cr, SUPERUSER_ID, {})
        ids = env['res.company'].sudo().search([], order='id desc')
        for i, rec in enumerate(ids):
            # search warehouse
            wh = env['stock.warehouse'].sudo().search([('company_id', '=', rec.id), ('name', '=', 'Subcontracting Warehouse')])
            if not wh:
                # create new warehouse
                # new_loc = env['stock.location'].sudo().create({
                #     'name': 'SWH',
                #     'usage': 'internal',
                #     'capacity_type': 'unit',
                #     'company_id': rec.id,
                # })
                new_wh = env['stock.warehouse'].sudo().create({
                    'name': 'Subcontracting Warehouse',
                    'code': 'SWH%s' % (i+1),
                    'company_id': rec.id,
                })
    except Exception as e:
        raise Warning(e)

def post_init_hook(cr, registry):
    """Loaded after installing the module.

    This module's DB modifications will be available.

    :param openerp.sql_db.Cursor cr:
        Database cursor.

    :param openerp.modules.registry.RegistryManager registry:
        Database registry, using v7 api.
    """
    pass


def uninstall_hook(cr, registry):
    """Loaded before uninstalling the module.

    This module's DB modifications will still be available. Raise an exception
    to abort uninstallation.

    :param openerp.sql_db.Cursor cr:
        Database cursor.

    :param openerp.modules.registry.RegistryManager registry:
        Database registry, using v7 api.
    """
    pass


def post_load():
    """Loaded before any model or data has been initialized.

    This is ok as the post-load hook is for server-wide
    (instead of registry-specific) functionalities.
    """
    pass