# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import models
from . import wizard
from . import tests
from .hooks import pre_init_hook, post_load, post_init_hook, uninstall_hook