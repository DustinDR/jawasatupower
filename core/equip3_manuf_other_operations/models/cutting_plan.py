import logging
from odoo import api, fields, models, _
from odoo.addons.stock.models.stock_move import PROCUREMENT_PRIORITIES

_logger = logging.getLogger(__name__)

class CuttingPlan(models.Model):
    _name = 'cutting.plan'
    _description = 'Cutting Plan'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    """
    cutting.plan
    Sequence = cutting.plan
    View = views/cutting_plan_views.xml
    Relation =
    cutting.order (cutting_order_ids)
    cutting.order.line (cutting_order_line_ids)
    sorted by priority, then date (check view)
    """

    @api.depends('company_id')
    def _compute_allowed_branch(self):
        user_branch = self.env.user.branch_ids
        for record in self:
            allowed_branch = user_branch.filtered(lambda b: b.company_id == record.company_id)
            record.allowed_branch_ids = [(6, 0, allowed_branch.ids)]    

    #Fields
    name = fields.Char(string='Cutting Plan', required=True, copy=False, readonly=True, default=_('New'), tracking=True)
    priority = fields.Selection(PROCUREMENT_PRIORITIES, string='Priority', default='0', index=True)
    plan_name = fields.Char(string='Plan Name')
    ppic_id = fields.Many2one(
        comodel_name='res.users', 
        string='PPIC',
        required=False)

    date_start = fields.Datetime(
        string='Scheduled Date',
        default=fields.Datetime.now(),
        readonly=True,)
    
    date_end = fields.Datetime(
        string='To',
        default=fields.Datetime.now(),
        readonly=True)
    
    analytic_tag_ids = fields.Many2many(
        'account.analytic.tag', 
        string='Analytical Group', 
        domain="[('company_id', '=', company_id)]", 
        default=lambda self: self._get_default_analytic_tag_ids())

    ppic_id = fields.Many2one(
        'res.users', 
        string='PPIC', 
        tracking=True, 
        readonly=True, 
        states={'draft': [('readonly', False)]})

    company_id = fields.Many2one(
        'res.company', 
        string='Company', 
        tracking=True, 
        default=lambda self: self.env.company,
        readonly=True,)

    branch_id = fields.Many2one(
        'res.branch', 
        string='Branch', 
        default=lambda self: self.env.user.branch_id,
        domain="[('id', 'in', allowed_branch_ids)]", 
        readonly=True,
        states={'draft': [('readonly', False)]}, 
        tracking=True)

    create_uid = fields.Many2one(
        'res.users', 
        string='Created By', 
        default=lambda self: self.env.user, 
        tracking=True,
        readonly=True,)

    create_date = fields.Date(
        string='Created On',
        default=fields.Date.today(),
        readonly=True,)

    approval_matrix = fields.Char(
        string='Approval Matrix',
        tracking=True)

    allowed_branch_ids = fields.One2many(
        'res.branch', 
        compute=_compute_allowed_branch)

    cutting_order_ids = fields.Many2many(
        comodel_name='cutting.order', 
        #inverse_name='cutting_plan_id', 
        string='Cutting Order')

    cutting_order_line_ids = fields.One2many(
        comodel_name='cutting.plan.order.line', 
        inverse_name='cutting_plan_id', 
        string='Cutting Order Line',
        save=True,
        store=True,
        )
    
    
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirmed'),
        ('progress', 'In Progress'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], default='draft', tracking=True)

    state_cancel = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirmed'),
        ('progress', 'In Progress'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], default='cancel', tracking=True)
    
    

    @api.model
    def _get_default_analytic_tag_ids(self):
        user = self.env.user
        analytic_priority = self.env['analytic.priority'].sudo().search([], limit=1, order='priority')
        analytic_tag_ids = []
        if analytic_priority.object_id == 'user' and user.analytic_tag_ids:
            analytic_tag_ids = user.analytic_tag_ids.ids
        elif analytic_priority.object_id == 'branch' and user.branch_id and user.branch_id.analytic_tag_ids:
            analytic_tag_ids = user.branch_id.analytic_tag_ids.ids
        elif analytic_priority.object_id == 'product_category':
            product_category = self.env['product.category'].sudo().search([('analytic_tag_ids', '!=', False)], limit=1)
            analytic_tag_ids = product_category.analytic_tag_ids.ids
        return [(6, 0, analytic_tag_ids)]
    
    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code(
                'cutting.plan', sequence_date=None
            ) or _('New')
        return super(CuttingPlan, self).create(vals)

    @api.onchange('cutting_order_ids')
    def _get_cutting_order_line(self):
        self.cutting_order_line_ids = [(5, 0, 0)]
        for record in self:
            record.cutting_order_line_ids = [
                (0, 0, {
                    'name' : lot.name,
                    'product_id' : lot.product_id,
                    'lot_id' : lot.lot_id,
                    'product_qty' : lot.product_qty,
                    'product_uom_id' : lot.product_uom_id,
                    'volume' : lot.volume,
                }) for lot in record.cutting_order_ids.lot_ids
            ]
    

    #Button Methods
    def button_confirm(self):
        self.state = 'confirm'

    def button_add_cutting(self):
        return {
            'name': 'Add Cutting Order',
            'res_model': 'cutting.plan.add.cutting.wizard',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'context': {'active_id': self.id},
            'target': 'new',
        }

    def button_cancel(self):
        self.state = 'cancel'

    def button_done(self):
        self.state = 'done'
        for line in self.cutting_order_ids:
            #line.cutting_id.button_done() <--
            line.state = 'done'
    
    
class CuttingPlanOrderLine(models.Model):
    _name = 'cutting.plan.order.line'
    _description = 'Cutting Plan Order Line'
    _inherit = 'cutting.order.line'

    cutting_plan_id = fields.Many2one(
        comodel_name='cutting.plan', 
        string='Cutting Plan')

    product_id = fields.Many2one(
        'product.product', 
        string='Product', 
        required=True, 
        readonly=False, 
        states={}, 
        domain="{}", 
        tracking=True,
        store=True)

    product_uom_id = fields.Many2one(
        'uom.uom',
        string='Product UoM', 
        # related='cutting_id.product_uom_id',
        readonly=False,
        store=True)
    
    
    
