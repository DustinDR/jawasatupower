from odoo import api, models, fields, _
from odoo.exceptions import ValidationError


class StockMove(models.Model):
    _inherit = 'stock.move'

    sml_cutting_unit = fields.Boolean(related="product_id.is_cutting_product")
    cutting_line_id = fields.Many2one('cutting.order.line', string='Cutting Order Line')
    mrp_consumption_subcon_id = fields.Many2one('mrp.consumption', string='MPR Subcontracting')
    subcon_picking_id = fields.Many2one('stock.picking', string='Subcontracting Picking', copy=False)
    subcon_is_finished_good = fields.Boolean()
    requisition_line_id = fields.Many2one('purchase.requisition.line', string='Requisition Line')

    def _create_out_svl(self, forced_quantity=None):
        svl_ids = super(StockMove, self)._create_out_svl(forced_quantity=forced_quantity)
        if any(not move.unbuild_id for move in self):
            return svl_ids

        for svl in svl_ids:
            unbuild_id = svl.stock_move_id.unbuild_id
            mo_id = unbuild_id.mo_id
            if not mo_id:
                continue

            finished_move = mo_id.move_finished_ids.filtered(lambda m: m.product_id == svl.product_id)
            finished_move_value = sum(finished_move.stock_valuation_layer_ids.mapped('value'))

            value = (unbuild_id.product_qty / mo_id.product_qty) * finished_move_value
            svl.value = value
            svl.unit_cost = value / svl.quantity
        return svl_ids

    def _create_in_svl(self, forced_quantity=None):
        svl_ids = super(StockMove, self)._create_in_svl(forced_quantity=forced_quantity)
        if any(not move.unbuild_id for move in self):
            return svl_ids

        for svl in svl_ids:
            mo_id = svl.stock_move_id.unbuild_id.mo_id
            if not mo_id:
                continue

            material_move = mo_id.move_raw_ids.filtered(lambda m: m.product_id == svl.product_id)
            material_move_value = sum(material_move.stock_valuation_layer_ids.mapped('value'))

            value = (svl.quantity / material_move.product_uom_qty) * material_move_value
            svl.value = value
            svl.unit_cost = value / svl.quantity
        return svl_ids

    def _account_entry_move(self, qty, description, svl_id, cost):
        if (self.unbuild_id and self.unbuild_id.mo_id) or self.cutting_line_id:
            # create moves from mrp.unbuild / cutting.order model instead
            return False
        return super(StockMove, self)._account_entry_move(qty, description, svl_id, cost)

    @api.onchange('mpr_quantity_done')
    def onchange_mpr_quantity_done(self):
        if self.subcon_is_finished_good:
            return
        super(StockMove, self).onchange_mpr_quantity_done()


class StockMoveLine(models.Model):
    _inherit = "stock.move.line"

    length = fields.Float('Length', default=1.0)
    width = fields.Float('Width', default=1.0)
    height = fields.Float('Height', default=1.0)
    cutting_uom = fields.Many2one(related='product_id.cutting_unit_measure', string="Cutting UOM")

    @api.onchange('length')
    def onchange_length(self):
        if self.length <= 0:
            raise ValidationError(_('Length value must be greater than 0!'))

    @api.onchange('width')
    def onchange_width(self):
        if self.width <= 0:
            raise ValidationError(_('Width value must be greater than 0!'))

    @api.onchange('height')
    def onchange_height(self):
        if self.height <= 0:
            raise ValidationError(_('Height value must be greater than 0!'))

    @api.model
    def _create_correction_svl(self, move, diff):
        if self.env.context.get('do_not_create_correction'):
            return
        return super(StockMoveLine, self)._create_correction_svl(move, diff)

