from datetime import datetime
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class MRPMaterialPurchase(models.Model):
	_name = 'mrp.material.purchase'
	_inherit = ['mail.thread', 'mail.activity.mixin']
	_description = 'MRP Material Purchase'

	@api.depends('company_id')
	def _compute_allowed_branch(self):
		user_branch = self.env.user.branch_ids
		for record in self:
			allowed_branch = user_branch.filtered(lambda b: b.company_id == record.company_id)
			record.allowed_branch_ids = [(6, 0, allowed_branch.ids)]

	name = fields.Char(
		string='Reference', 
		required=True, 
		copy=False, 
		readonly=True, 
		default=lambda self: _('New'),
		tracking=True
	)
	mrp_production_ids = fields.Many2many(
		'mrp.production', 
		'mrp_material_purchase_ids', 
		string='Manufacturing Order',
		tracking=True,
		domain="[('state', 'in', ('confirmed', 'progress')), ('create_date', '>=', date_from), ('create_date', '<=', date_to)]"
	)
	company_id = fields.Many2one(
		'res.company', 
		string='Company', 
		required=True, 
		index=True,
		default=lambda self: self.env.company, 
		tracking=True
	)
	state = fields.Selection(
		selection=[('draft', 'Draft'), ('submitted', 'Submitted')], 
		string='Status', 
		readonly=True, 
		copy=False, 
		index=True, 
		default='draft', 
		tracking=True
	)
	allowed_branch_ids = fields.One2many('res.branch', compute=_compute_allowed_branch)
	branch_id = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id,
								domain="[('id', 'in', allowed_branch_ids)]", readonly=True,
								states={'draft': [('readonly', False)]}, tracking=True)
	finished_product_ids = fields.One2many(
		'mrp.material.purchase.finished.product', 
		'mrp_material_purchase_id', 
		string='Finished Products'
	)
	component_ids = fields.One2many(
		'mrp.material.purchase.component', 
		'mrp_material_purchase_id', 
		string='Components'
	)
	purchase_request_id = fields.Many2one(
		'purchase.request',
		string='Purchase Request',
		copy=False,
		readonly=True
	)
	date_from = fields.Datetime('Period', index=True, copy=False, default=fields.Date.context_today)
	date_to = fields.Datetime('Date To', index=True, copy=False, default=fields.Date.context_today)

	# def _generate_order_by(self, order_spec, query):
	# 	my_order = "CASE WHEN state='draft'  THEN 1   WHEN state = 'submitted'  THEN 10 ELSE 100 END"
	# 	if order_spec:
	# 		return super(MRPMaterialPurchase, self)._generate_order_by(order_spec, query) + ", " + my_order
	# 	return my_order

	@api.onchange('date_from', 'date_to')
	def _onchange_date_from_to(self):
		domain = [
			('state', 'in', ['confirmed', 'progress'])
		]
		if self.date_from:
			domain.append(('create_date', '>=', self.date_from))
		if self.date_to:
			domain.append(('create_date', '<=', self.date_to))

		if self.date_from and self.date_to:
			production_ids = self.env['mrp.production'].search(domain)
			self.mrp_production_ids = [(6, 0, production_ids.ids)]

	@api.model
	def create(self, vals):
		if vals.get('name', _('New')) == _('New'):
			seq_date = None
			vals['name'] = self.env['ir.sequence'].next_by_code(
				'mrp.material.purchase', sequence_date=seq_date
			) or _('New')
		return super(MRPMaterialPurchase, self).create(vals)

	@api.onchange('mrp_production_ids')
	def _onchange_mrp_production_ids(self):
		if self.mrp_production_ids:
			values = [(5,)]
			for production_id in self.mrp_production_ids:
				values.append((0, 0, {
					'product_id': production_id.product_id.id,
					'product_bom': production_id.bom_id.id,
					'product_uom_qty': production_id.product_qty
				}))
			self.finished_product_ids = values

	def action_calculate(self):
		production_ids = self.mrp_production_ids

		if self.finished_product_ids:
			values = [(5,)]
			for finished_good in self.finished_product_ids:
				# mno = self.env['mrp.bom'].sudo().search([('product_id','=', finished_good.product_id.id)])
				product_bom = finished_good.product_bom
				main_product_qty = product_bom.product_qty
				bom_line_ids = product_bom.bom_line_ids
				bom_qty = 0
				required_qty = 0
				to_purchase = 0
				if len(product_bom) > 0:
					for bom_line in bom_line_ids:
						bom_qty = bom_line.product_qty
						product_id = bom_line.product_id
						finished_good_qty = finished_good.product_uom_qty
						if main_product_qty > 0:
							required_qty = int(finished_good_qty * bom_qty)/int(main_product_qty)
						else:
							required_qty = bom_qty
						if product_id.qty_available > required_qty:
							to_purchase = 0
						else:
							to_purchase = required_qty - product_id.qty_available
						values.append((0, 0, {
							'product_id': product_id.id,
							'qty_available': product_id.qty_available,
							'incoming_qty': product_id.incoming_qty,
							'outgoing_qty': product_id.outgoing_qty,
							'virtual_available': product_id.virtual_available,
							'needed': required_qty,
							'to_purchase': to_purchase
						}))
						self.component_ids = values
				# else:
				# 	if production_ids:
				# 		move_raw_ids = production_ids.mapped('move_raw_ids')
				# 		product_ids = set(move_raw_ids.mapped('product_id'))
				# 		for product_id in product_ids:
				# 			product_id = product_id
				# 			required_qty = sum(move_raw_ids.filtered(lambda x: x.product_id == product_id).mapped('product_uom_qty'))
				# 			values.append((0, 0, {
				# 				'product_id': product_id.id,
				# 				'qty_available': product_id.qty_available,
				# 				'incoming_qty': product_id.incoming_qty,
				# 				'outgoing_qty': product_id.outgoing_qty,
				# 				'virtual_available': product_id.virtual_available,
				# 				'needed': required_qty,
				# 				'to_purchase': max(0, required_qty - product_id.virtual_available),
				#
				# 			}))
				# 		self.component_ids = values

	def action_submit_pr(self):
		component_ids = self.component_ids.filtered(lambda x: x.to_purchase > 0.0)

		if not component_ids:
			raise ValidationError(_('No components to purchase!'))

		line_ids = []
		for component_id in component_ids:
			line_ids.append((0, 0, {
				'name': component_id.product_id.name,
				'product_id': component_id.product_id.id,
				'product_qty': component_id.to_purchase,
				'product_uom_id': component_id.product_id.uom_id.id
			}))

		purchase_request_id = self.env['purchase.request'].create({
			'origin': self.name,
			'requested_by': self.create_uid.id,
			'line_ids': line_ids
		})

		self.purchase_request_id = purchase_request_id.id
		self.state = 'submitted'

	def action_submit_mr(self):
			component_ids = self.component_ids.filtered(lambda x: x.to_purchase > 0.0)

			if not component_ids:
				raise ValidationError(_('No components to purchase!'))
			wh_id = self.env['stock.warehouse'].search([('company_id', '=', self.env.user.company_id.id)], limit=1)
			line_ids = []
			for component_id in component_ids:
				line_ids.append((0, 0, {
					'product': component_id.product_id.id,
					'quantity': component_id.to_purchase,
					'product_unit_measure': component_id.product_id.uom_id.id,
					'destination_warehouse_id': wh_id.id,
				}))


			material_request_id = self.env['material.request'].create({
				'source_document': self.name,
				'schedule_date': datetime.now(),
				'product_line': line_ids,
				'destination_warehouse_id': wh_id.id,
				'material_purchase_id': self.id
			})
			self.state = 'submitted'
	
	def action_view_mr(self):
		mr_ids = self.env['material.request'].search([('material_purchase_id', '=', self.id)])
		
		action = {
			'name': 'Material Requests',
			'type': 'ir.actions.act_window',
			'res_model': 'material.request',
			'target': 'current'
		}
		if len(mr_ids) == 1:
			action.update({
				'view_mode': 'form',
				'res_id': mr_ids[0].id
			})
		else:
			action.update({
				'view_mode': 'tree,form',
				'domain': [('material_purchase_id', '=', self.id)]
			})

		return action
	def _compute_submit_mr_count(self):
		for record in self:
			mr_ids = self.env['material.request'].search([('material_purchase_id', '=', record.id)])
			record.submit_mr_count = len(mr_ids)

	submit_mr_count = fields.Integer(compute='_compute_submit_mr_count')

	def action_view_purchase_request(self):
		if self.purchase_request_id:
			action = {
				'name': 'Purchase Request',
				'type': 'ir.actions.act_window',
				'res_model': 'purchase.request',
				'view_mode': 'form',
				'res_id': self.purchase_request_id.id,
				'target': 'current'
			}
			return action


class MRPProduction(models.Model):
	_inherit = "mrp.production"

	mrp_material_purchase_ids = fields.Many2many(
		'mrp.material.purchase', 
		'mrp_production_ids', 
		string='Material to Purchases'
	)
class MaterialRequestInherit(models.Model):
	_inherit = "material.request"

	material_purchase_id = fields.Many2one(
		'mrp.material.purchase',
		string='MRP Material Purchase',
		copy=False,
	)


class MRPMaterialPurchaseFinishedProducts(models.Model):
	_name = "mrp.material.purchase.finished.product"
	_description = "Fisihed Products on MPR Material to Purchase"

	mrp_material_purchase_id = fields.Many2one('mrp.material.purchase', index=True, required=True, ondelete='cascade')
	company_id = fields.Many2one('res.company', related='mrp_material_purchase_id.company_id', store=True, index=True)
	product_id = fields.Many2one('product.product', string='Finished Product', required=True, check_company=True)
	product_bom = fields.Many2one('mrp.bom', string='Bill of Material', required=True)
	product_uom_qty = fields.Float(string='Quantity', default=1.0, digits='Product Unit of Measure', required=True)


class MRPMaterialPurchaseComponent(models.Model):
	_name = "mrp.material.purchase.component"
	_description = "Components on MPR Material to Purchase"

	mrp_material_purchase_id = fields.Many2one('mrp.material.purchase', index=True, required=True, ondelete='cascade')
	company_id = fields.Many2one('res.company', related='mrp_material_purchase_id.company_id', store=True, index=True)
	product_id = fields.Many2one('product.product', string='Product', required=True, readonly=True, check_company=True)
	qty_available = fields.Float(string='On Hand', readonly=True, digits='Product Unit of Measure')
	incoming_qty = fields.Float(string='Forecast Incoming', readonly=True, digits='Product Unit of Measure')
	outgoing_qty = fields.Float(string='Forecast Outgoing', readonly=True, digits='Product Unit of Measure')
	virtual_available = fields.Float(string='Forecasted', readonly=True, digits='Product Unit of Measure')
	needed = fields.Float(string='Needed', readonly=True, digits='Product Unit of Measure')
	to_purchase = fields.Float(string='To Purchase', digits='Product Unit of Measure')

	@api.onchange('to_purchase')
	def _onchange_to_purchase(self):
		if self.to_purchase < 0:
			raise ValidationError(_("You can't have To Purchase value < 0!"))
