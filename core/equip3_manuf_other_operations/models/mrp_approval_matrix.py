from odoo import models, fields, api, _


class MrpApprovalMatrix(models.Model):
    _inherit = 'mrp.approval.matrix'

    matrix_type = fields.Selection(selection_add=[('cutting', 'Cutting Order')])

