from odoo import models, fields, api


class ProductTemlateInherit(models.Model):
	_inherit = 'product.template'

	manuf_cost = fields.Boolean('Is Manufacturing Cost')
	manuf_cost_category = fields.Selection(
		selection=[('material', 'Material'), ('overhead', 'Overhead'), ('labor', 'Labor'), ('subcontracting', 'Subcontracting')],
		string='Cost Category'
	)
	manuf_account_id = fields.Many2one('account.account', string='Account', domain="[('user_type_id.name', 'in', ['Payable', 'Utang', 'Credit Card', 'Kartu Kredit', 'Current Liabilities', 'Pasiva Terkini', 'Non-current Liabilities', 'Hutang Tidak Lancar'])]")
	
	is_a_service = fields.Boolean(string='Is a Service', default=False)
	is_cutting_product = fields.Boolean(string="Is a Cutting Product")
	cutting_unit_measure = fields.Many2one('uom.uom', string='Cutting Unit of Measure')
	check_cutting = fields.Boolean(string="Cutting", related="equi3_company_id.cutting", readonly=False)
	equi3_company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)


	@api.model
	def create(self, vals):
		if vals.get('type'):
			if vals['type'] == 'service':
				vals['is_a_service'] = True
		return super(ProductTemlateInherit, self).create(vals)
	
	def write(self, vals):
		if 'type' in vals.keys():
			if vals['type'] == 'service':
				vals['is_a_service'] = True
		return super(ProductTemlateInherit, self).write(vals)
	
	@api.onchange('type')
	def _onchange_type(self):
		if self.type == 'service':
			self.is_a_service = True
		else:
			self.is_a_service = False


class ProductProductInherit(models.Model):
	_inherit = 'product.product'
	
	is_a_service = fields.Boolean(string='Is a Service', default=False)

	@api.model
	def create(self, vals):
		if vals.get('type'):
			if vals['type'] == 'service':
				vals['is_a_service'] = True
		return super(ProductProductInherit, self).create(vals)
	
	def write(self, vals):
		if 'type' in vals.keys():
			if vals['type'] == 'service':
				vals['is_a_service'] = True
		return super(ProductProductInherit, self).write(vals)
	
	@api.onchange('type')
	def _onchange_type(self):
		if self.type == 'service':
			self.is_a_service = True
		else:
			self.is_a_service = False
