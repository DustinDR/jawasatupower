from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import logging

_logger = logging.getLogger(__name__)


class MRPCostActualization(models.Model):
	_name = 'mrp.cost.actualization'
	_inherit = ['mail.thread', 'mail.activity.mixin']
	_description = 'Manufacturing Cost Actualization'

	@api.depends('line_ids', 'line_ids.cost')
	def _compute_total_cost(self):
		for record in self:
			record.total_cost = sum(record.line_ids.mapped('cost'))

	@api.model
	def create(self, values):
		if values.get('name', _('New')) == _('New'):
			values['name'] = self.env['ir.sequence'].next_by_code(
				'mrp.cost.actualization.day', sequence_date=None
			) or _('New')
		return super(MRPCostActualization, self).create(values)

	@api.depends('company_id')
	def _compute_allowed_branch(self):
		user_branch = self.env.user.branch_ids
		for record in self:
			allowed_branch = user_branch.filtered(lambda b: b.company_id == record.company_id)
			record.allowed_branch_ids = [(6, 0, allowed_branch.ids)]

	name = fields.Char(default=_('New'), required=True, readonly=True, copy=False)
	company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True, default=lambda self: self.env.company)
	currency_id = fields.Many2one('res.currency', related='company_id.currency_id')
	allowed_branch_ids = fields.One2many('res.branch', compute=_compute_allowed_branch)
	branch_id = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id,
								domain="[('id', 'in', allowed_branch_ids)]", readonly=True,
								states={'draft': [('readonly', False)]}, tracking=True)
	state = fields.Selection(
		selection=[('draft', 'Draft'), ('post', 'Posted'), ('cancel', 'Cancelled')],
		required=True,
		copy=False,
		default='draft',
		tracking=True
	)
	create_uid = fields.Many2one('res.users', default=lambda self: self.env.user, readonly=True)
	date_from = fields.Date(string='From Date', readonly=True, states={'draft': [('readonly', False)]}, default=fields.Date.today(), tracking=True)
	date_to = fields.Date(string='To Date', readonly=True, states={'draft': [('readonly', False)]}, default=fields.Date.today(), tracking=True)
	production_ids = fields.Many2many('mrp.production', readonly=True, states={'draft': [('readonly', False)]}, string='Manufacturing Order', copy=False, domain="[('state', 'in', ('to_close', 'done')), ('date_start', '>=', date_from), ('date_finished', '<=', date_to)]")
	account_move_id = fields.Many2one('account.move', string='Journal Entry', copy=False, readonly=True)
	line_ids = fields.One2many('mrp.cost.actualization.line', 'mrp_cost_actualization_id', string='Cost Lines')
	valuation_line_ids = fields.One2many('mrp.cost.actualization.valuation', 'mrp_cost_actualization_id', string='Valuation Adjusments', readonly=True)
	production_line_ids = fields.One2many('mrp.cost.actualization.production','mrp_cost_actualization_id', string='Production Cost Lines', readonly=True)
	total_cost = fields.Monetary(string='Total', compute=_compute_total_cost)

	@api.onchange('date_from', 'date_to')
	def _onchange_date_from_to(self):
		domain = [
			('state', 'in', ('to_close', 'done'))
		]
		if self.date_from:
			domain.append(('date_start', '>=', self.date_from))
		if self.date_to:
			domain.append(('date_finished', '<=', self.date_to))

		if self.date_from and self.date_to:
			production_ids = self.env['mrp.production'].search(domain)
			self.production_ids = [(6, 0, production_ids.ids)]

	def action_validate(self):
		values = self._prepare_move_vals()
		account_move_id = self.env['account.move'].create(values)
		account_move_id.action_post()
		self.account_move_id = account_move_id.id

		for line_id in self.production_line_ids:
			line_id._assign_valuations()
			for svl_id in line_id.stock_valuation_layer_ids:
				svl_id.update_value()

		self.state = 'post'

	def action_cancel(self):
		self.state = 'cancel'

	def _prepare_move_vals(self):
		journal_ids = self.env['account.journal'].search([
			('type', '=', 'general'),
			('name', '=', 'Manufacturing Journal')
		])

		if not journal_ids:
			raise ValidationError(_('You have to set Manufacturing Journal first!'))

		line_ids = []
		for line in self.valuation_line_ids:
			if line.production_id.product_id.categ_id.property_valuation != 'real_time':
				raise ValidationError(_(f"Set category of product {line.production_id.product_id.name} to Automated first!"))

			mo_account_id = line.production_id.product_id.categ_id.property_stock_valuation_account_id

			if line.production_id.product_id.default_code:
				mo_product = '%s %s' % (line.production_id.product_id.default_code, line.production_id.product_id.name)
			else:
				mo_product = line.production_id.product_id.name

			line_product = '%s %s' % (line.product_id.default_code, line.product_id.name)
			name = '%s - %s' % (line_product, mo_product)

			line_ids += [
				(0, 0, {
					'account_id': line.account_id.id,
					'name': name,
					'debit': 0.0,
					'credit': line.add_cost,
				}),
				(0, 0, {
					'account_id': mo_account_id.id,
					'name': name,
					'debit': line.add_cost,
					'credit': 0.0,
				})
			]

		values = {
			'ref': self.name,
			'date': fields.Datetime.now(),
			'discount_type': 'global',
			'journal_id': journal_ids[0].id,
			'line_ids': line_ids
		}
		return values

	def action_compute(self):
		
		total = {
			'material': 0.0,
			'overhead': 0.0,
			'labor': 0.0,
			'subcontracting': 0.0
		}
		valuation_values = []
		total_qty = sum(self.production_ids.mapped('product_qty'))
		for mo in self.production_ids:

			product_qty = mo.product_qty
			for line in self.line_ids:
				if line.split_method == 'equal':
					add_cost = line.cost / len(self.production_ids)
				else:
					add_cost = (product_qty / total_qty) * line.cost

				valuation = {
					'mrp_cost_actualization_id': self.id,
					'company_id': self.company_id.id,
					'production_id': mo.id,
					'category': line.cost_category,
					'account_type': line.account_type,
					'account_id': line.account_id.id,
					'add_cost': add_cost
				}
				total[line.cost_category] += add_cost
				valuation_values.append(valuation)

		valuation_line_ids = self.env['mrp.cost.actualization.valuation'].create(valuation_values)
		self.valuation_line_ids = [(6, 0, valuation_line_ids.ids)]

		production_values = []
		for mo in self.production_ids:
			valuations = self.valuation_line_ids.filtered(lambda v: v.production_id == mo)
			finished_moves = mo.move_finished_ids.filtered(lambda p: p.product_id == mo.product_id)
			svl_ids = finished_moves.stock_valuation_layer_ids.filtered(lambda s: s.type == 'finished')
			former_cost = abs(sum(svl_ids.mapped('value')))
			production = {
				'mrp_cost_actualization_id': self.id,
				'company_id': self.company_id.id,
				'production_id': mo.id,
				'former_cost': former_cost,
				'total_material': sum(valuations.filtered(lambda v: v.category == 'material').mapped('add_cost')),
				'total_overhead': sum(valuations.filtered(lambda v: v.category == 'overhead').mapped('add_cost')),
				'total_labor': sum(valuations.filtered(lambda v: v.category == 'labor').mapped('add_cost')),
				'total_subcontracting': sum(valuations.filtered(lambda v: v.category == 'subcontracting').mapped('add_cost'))
			}
			production_values.append(production)

		
		production_line_ids = self.env['mrp.cost.actualization.production'].create(production_values)
		self.production_line_ids = [(6, 0, production_line_ids.ids)]


class MRPCostActualizationLine(models.Model):
	_name = 'mrp.cost.actualization.line'
	_description = 'Manufacturing Cost Actualization Line'

	mrp_cost_actualization_id = fields.Many2one('mrp.cost.actualization', copy=False, required=True, ondelete='cascade')
	company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company)
	currency_id = fields.Many2one('res.currency', related='company_id.currency_id')
	product_id = fields.Many2one('product.product', string='Product', domain="[('manuf_cost', '=', True)]")
	cost_category = fields.Selection(
		selection=[('material', 'Material'), ('overhead', 'Overhead'), ('labor', 'Labor'), ('subcontracting', 'Subcontracting')],
		required=True,
		string='Cost Category'
	)
	account_type = fields.Selection(
		selection=[
			('Payable', 'Payable'), 
			('Credit Card', 'Credit Card'), 
			('Current Liabilities', 'Current Liabilities'), 
			('Non-current Liabilities', 'Non-current Liabilities')
		],
		required=False,
		string='AccountType'
	)
	account_id = fields.Many2one('account.account', string='Account', required=True, domain="[('user_type_id.name', 'in', ['Payable', 'Credit Card', 'Current Liabilities', 'Non-current Liabilities'])]")
	description = fields.Char(string='Description', copy=False)
	split_method = fields.Selection(
		selection=[('equal', 'Equal'), ('by_quantity', 'By Quantity')],
		string='Split Method',
		required=True,
		default='equal'
	)
	cost = fields.Monetary(string='Cost')

	@api.onchange('product_id')
	def _onchange_product_id(self):
		if self.product_id:
			self.cost_category = self.product_id.manuf_cost_category
			self.cost = self.product_id.standard_price
			if self.product_id.manuf_account_id:
				self.account_id = self.product_id.manuf_account_id.id
				account_type_names = {
					'Utang': 'Payable',
					'Kartu Kredit': 'Credit Card',
					'Pasiva Terkini': 'Current Liabilities',
					'Hutang Tidak Lancar': 'Non-current Liabilities'
				}
				self.account_type = account_type_names.get(self.product_id.manuf_account_id.user_type_id.name, False)
			
	@api.onchange('cost_category')
	def _onchange_cost_category(self):
		if self.cost_category:
			self.description = dict(self._fields['cost_category'].selection).get(self.cost_category, False)

class MRPCostActualizationValuation(models.Model):
	_name = 'mrp.cost.actualization.valuation'
	_description = 'Manufacturing Cost Actualization Valuation Adjusment'

	mrp_cost_actualization_id = fields.Many2one('mrp.cost.actualization', copy=False, required=True, ondelete='cascade')
	company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company)
	currency_id = fields.Many2one('res.currency', related='company_id.currency_id')
	production_id = fields.Many2one('mrp.production', string='Production', required=True, readonly=True, copy=False)
	product_id = fields.Many2one('product.product', string='Product', related='production_id.product_id')
	quantity = fields.Float(string='Quantity', related='production_id.product_qty', digits='Product Unit of Measure')
	category = fields.Selection(
		string='Category',
		selection=[('material', 'Material'), ('overhead', 'Overhead'), ('labor', 'Labor'), ('subcontracting', 'Subcontracting')],
		required=True,
		readonly=True,
		copy=False
	)
	account_type = fields.Selection(
		selection=[
			('Payable', 'Payable'), 
			('Credit Card', 'Credit Card'), 
			('Current Liabilities', 'Current Liabilities'), 
			('Non-current Liabilities', 'Non-current Liabilities')
		],
		required=False,
		string='AccountType'
	)
	account_id = fields.Many2one('account.account', string='Account', required=True, domain="[('user_type_id.name', 'in', ['Payable', 'Credit Card', 'Current Liabilities', 'Non-current Liabilities'])]")
	add_cost = fields.Monetary(string='Additional Cost', required=True, readonly=True, copy=False)

class MRPCostActualizationProduction(models.Model):
	_name = 'mrp.cost.actualization.production'
	_description = 'Manufacturing Cost Actualization Production Cost Lines'

	@api.depends('total_material', 'total_overhead', 'total_labor', 'total_subcontracting', 'former_cost')
	def _compute_total(self):
		for record in self:
			total = record.total_material + record.total_overhead + record.total_labor + record.total_subcontracting
			new_cost = total + record.former_cost
			
			record.total = total
			record.new_cost = new_cost

	def _assign_valuations(self):
		self.ensure_one()
		if self.production_id:
			production_id = self.production_id
			svl_ids = production_id.move_finished_ids.stock_valuation_layer_ids
			self.stock_valuation_layer_ids = [(6, 0, svl_ids.ids)]

	mrp_cost_actualization_id = fields.Many2one('mrp.cost.actualization', copy=False, required=True, ondelete='cascade')
	company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company)
	currency_id = fields.Many2one('res.currency', related='company_id.currency_id')

	production_id = fields.Many2one('mrp.production', string='Production', required=True, readonly=True, copy=False)
	product_id = fields.Many2one('product.product', related='production_id.product_id')
	quantity = fields.Float(string='Quantity', related='production_id.product_qty', digits='Product Unit of Measure')
	product_uom_id = fields.Many2one('uom.uom', related='production_id.product_uom_id')

	total_material = fields.Monetary(string='Total Material', required=True, readonly=True, copy=False)
	total_labor = fields.Monetary(string='Total Labor', required=True, readonly=True, copy=False)
	total_overhead = fields.Monetary(string='Total Overhead', required=True, readonly=True, copy=False)
	total_subcontracting = fields.Monetary(string='Total Subcontracting', required=True, readonly=True, copy=False)
	
	former_cost = fields.Monetary(string='Former Cost', required=True, readonly=True, copy=False)
	total = fields.Monetary(string='Total Cost', compute=_compute_total)
	new_cost = fields.Monetary(string='New Cost', compute=_compute_total)

	stock_valuation_layer_ids = fields.Many2many('stock.valuation.layer', string='Valuations')
