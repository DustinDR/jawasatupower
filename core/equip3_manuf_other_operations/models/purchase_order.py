from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class PurchaseOrderInheritSub(models.Model):
    _inherit = 'purchase.order'

    subcon_picking_ids = fields.Many2many('stock.picking', relation='purchase_picking_subcon_ids', string='Subcontracting Delivery Orders')

    @api.depends('subcon_production_id', 'is_a_subcontracting')
    def _compute_production_name(self):
        for record in self:
            name = ''
            production_id = record.subcon_production_id
            if record.is_a_subcontracting and production_id:
                name = '%s - %s' % (production_id.name, production_id.product_id.name)
            record.subcon_production_name = name

    @api.depends('subcon_picking_ids')
    def _compute_delivery_order_count(self):
        for record in self:
            record.subcon_delivery_count = len(record.subcon_picking_ids)

    is_a_subcontracting = fields.Boolean(string='Is a Subcontracting')
    subcon_production_id = fields.Many2one('mrp.production', string='Manufacturing Order')
    subcon_production_name = fields.Char(string='Manufacturing Order Name', compute=_compute_production_name, store=True)
    subcon_product_qty = fields.Float(string='Subcontract Quantity', digits='Product Unit of Measure')
    subcon_uom_id = fields.Many2one('uom.uom', string='Subcontracting Unit of Measure')
    subcon_delivery_count = fields.Integer(compute=_compute_delivery_order_count)

    @api.constrains('is_a_subcontracting')
    def is_a_subcontracting_constraints(self):
        for record in self:
            if not record.is_a_subcontracting:
                continue
            missing_field = False
            if not record.subcon_production_id:
                missing_field = 'Manufacturing Order (subcon_production_id)'
            if not record.subcon_uom_id:
                missing_field = 'Subcontracting UoM (subcon_uom_id)'
            if record.subcon_product_qty <= 0.0:
                missing_field = 'Subcontracting Quantity (subcon_product_qty)'
            if missing_field:
                raise ValidationError(_('%s is mandatory when Subcontracting is True!' % missing_field))

    def action_view_delivery_order(self):
        self.ensure_one()
        action = self.env['ir.actions.actions']._for_xml_id('equip3_inventory_operation.action_delivery_order')
        orders = self.env['stock.picking'].search([('id', 'in', self.subcon_picking_ids.ids)])
        if not orders:
            return
        if len(orders) > 1:
            action['domain'] = [('id', 'in', orders.ids)]
        elif orders:
            views = [(self.env.ref('stock.view_picking_form').id, 'form')]
            if 'views' in action:
                views += [(state, view) for state, view in action['views'] if view != 'form']
            action['views'] = views
            action['res_id'] = orders.id

        action['context'] = dict(
            active_model='stock.picking',
            active_ids=orders.ids,
            active_id=orders.ids[0]
        )
        return action

    def _prepare_subcon_vals(self):
        self.ensure_one()
        values = {
            'origin': self.name,
            'is_readonly_origin': True,
            'is_a_subcontracting': True,
            'subcon_production_id': self.subcon_production_id.id,
            'subcon_product_qty': self.subcon_product_qty,
            'subcon_uom_id': self.subcon_uom_id.id,
            'subcon_qty_producing': self.subcon_product_qty,
            'subcon_qty_produced': 0.0,
        }
        return values

    def _prepare_subcon_delivery_vals(self, workorders):
        self.ensure_one()
        picking_type_id = self.env['stock.picking.type'].search([
            ('company_id', '=', self.company_id.id),
            ('sequence_code', '=', 'OUT')], limit=1)
        picking_type_location = picking_type_id.default_location_src_id
        first_location = self.env['stock.location'].search([
            ('company_id', '=', self.company_id.id)], limit=1)

        location_id = picking_type_location or first_location
        location_dest_id = self.subcon_production_id.get_subcon_location(self.partner_id, self.company_id)

        move_ids_without_package = []
        for move in workorders.move_raw_ids:
            move_ids_without_package += [(0, 0, {
                'name': self.name,
                'product_id': move.product_id.id,
                'initial_demand': move.product_uom_qty,
                'product_uom_qty': move.product_uom_qty,
                'product_uom': move.product_uom
            })]

        values = {
            'is_a_delivery': True,
            'name': picking_type_id.sequence_id.next_by_id(),
            'picking_type_id': picking_type_id.id,
            'location_id': location_id.id,
            'location_dest_id': location_dest_id.id,
            'move_ids_without_package': move_ids_without_package
        }
        values.update(self._prepare_subcon_vals())
        return values

    def _prepare_subcon_receipt_vals(self, workorders):
        self.ensure_one()
        production_id = self.subcon_production_id
        byproduct_ids = workorders.byproduct_ids
        finished_ids = production_id.move_finished_ids.filtered(
            lambda m: m.product_id == production_id.product_id)

        subcon_byproduct_ids = self.env['stock.move']
        if byproduct_ids:
            subcon_byproduct_ids = byproduct_ids | finished_ids

        values = {
            'is_a_delivery': False,
            'subcon_byproduct_ids': [(6, 0, subcon_byproduct_ids.ids)],
            'subcon_requisition_id': self.requisition_id.id
        }

        values.update(self._prepare_subcon_vals())
        return values

    def button_confirm(self):
        res = super(PurchaseOrderInheritSub, self).button_confirm()
        for record in self:
            if not record.is_a_subcontracting:
                continue
            production_id = record.subcon_production_id
            requisition_id = record.requisition_id

            # TODO: should split move_raw_ids and move_finished_ids instead of workorders?
            workorder_ids = production_id.split_workorders(record.subcon_product_qty, record.partner_id)
            subcon_workorders = workorder_ids.filtered(lambda w: w.is_a_subcontracting)

            if not requisition_id:
                values = record._prepare_subcon_delivery_vals(subcon_workorders)
                subcon_picking_values = [(0, 0, values)]
            else:
                picking_ids = self.env['stock.picking'].search([('subcon_requisition_id', '=', requisition_id.id)])
                subcon_picking_values = [(4, picking.id) for picking in picking_ids]

            values = record._prepare_subcon_receipt_vals(subcon_workorders)
            picking_values = [(1, picking.id, values) for picking in record.picking_ids]

            record.write({'picking_ids': picking_values, 'subcon_picking_ids': subcon_picking_values})

            production_id.button_unplan()
            production_id.button_plan()

        return res
    
    def create_blanket_order(self):
        res = super(PurchaseOrderInheritSub, self).create_blanket_order()
        requisition = self.env['purchase.requisition']
        for record in self:
            if record.is_a_subcontracting:
                requisition_id = requisition.search([('purchase_id', '=', record.id)])
                if requisition_id:
                    record.requisition_id = requisition_id.id
        return res
