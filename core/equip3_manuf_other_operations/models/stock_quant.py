from odoo import models, fields, api
from odoo.tools import float_is_zero


class StockQuant(models.Model):
    _inherit = 'stock.quant'

    @api.depends('company_id', 'location_id', 'owner_id', 'product_id', 'quantity', 
    'actualization_value', 'cutting_value', 'should_use_cutting_value')
    def _compute_value(self):
        super(StockQuant, self)._compute_value()
        for quant in self:
            value = quant.value
            if quant.should_use_cutting_value:
                value = quant.cutting_value
            quant.value = value + quant.actualization_value

    @api.depends('product_id', 'location_id', 'lot_id', 'package_id')
    def _compute_custom_value(self):
        for quant in self:
            stock_move_line_ids = self.env['stock.move.line'].search([
                ('product_id', '=', quant.product_id.id),
                '|',
                    ('location_id', '=', quant.location_id.id),
                    ('location_dest_id', '=', quant.location_id.id),
                ('lot_id', '=', quant.lot_id.id),
                '|',
                    ('package_id', '=', quant.package_id.id),
                    ('result_package_id', '=', quant.package_id.id),
            ])
            stock_move_ids = stock_move_line_ids.mapped('move_id')
            svl_ids = stock_move_ids.stock_valuation_layer_ids
            act_svl_ids = svl_ids.filtered(lambda s: s.type == 'finished')

            quant.cutting_value = sum(svl_ids.mapped('value'))
            quant.actualization_value = sum(act_svl_ids.mapped('actualization_value'))

    @api.depends('product_id', 'location_id', 'lot_id', 'package_id')
    def _compute_stock_move_ref(self):
        for quant in self:
            stock_move_line_ids = self.env['stock.move.line'].search([
                ('product_id', '=', quant.product_id.id),
                '|',
                    ('location_id', '=', quant.location_id.id),
                    ('location_dest_id', '=', quant.location_id.id),
                ('lot_id', '=', quant.lot_id.id),
                '|',
                    ('package_id', '=', quant.package_id.id),
                    ('result_package_id', '=', quant.package_id.id),
            ])
            stock_move_ids = stock_move_line_ids.mapped('move_id')
            production_ids = stock_move_ids.mapped('production_id')

            quant.stock_move_ref = ' - '.join(stock_move_ids.mapped('reference'))
            quant.production_ref = ' - '.join(production_ids.mapped('name'))
    
    @api.depends('lot_id')
    def _compute_should_use_cutting_value(self):
        for quant in self:
            should_use_cutting_value = False
            if quant.lot_id:
                if quant.lot_id.original_lot_id:
                    should_use_cutting_value = True
            quant.should_use_cutting_value = should_use_cutting_value

    stock_move_ref = fields.Char(compute=_compute_stock_move_ref)
    production_ref = fields.Char(compute=_compute_stock_move_ref)
    actualization_value = fields.Float(string='Actualization Value', compute=_compute_custom_value)
    cutting_value = fields.Float(string='Cutting Value', compute=_compute_custom_value)
    should_use_cutting_value = fields.Boolean(compute=_compute_should_use_cutting_value)
    original_lot_id = fields.Many2one('stock.production.lot', string='Original Lot/Serial Number', related='lot_id.original_lot_id')
