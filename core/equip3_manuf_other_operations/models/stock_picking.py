from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError


class StockPickingInherit(models.Model):
    _inherit = 'stock.picking'

    @api.depends('subcon_production_id', 'is_a_subcontracting')
    def _compute_production_name(self):
        for record in self:
            name = ''
            production_id = record.subcon_production_id
            if record.is_a_subcontracting and production_id:
                name = '%s - %s' % (production_id.name, production_id.product_id.name)
            record.subcon_production_name = name

    @api.depends('is_a_subcontracting', 'subcon_byproduct_ids', 'subcon_byproduct_ids.mpr_quantity_done')
    def _compute_subcon_qty_producing(self):
        for record in self:
            record.set_subcon_qty_producing()

    def _inverse_subcon_qty_producing(self):
        for record in self:
            record.set_subcon_fg_line_qty_done()

    is_a_delivery = fields.Boolean(string='Is a Delivery')
    is_a_subcontracting = fields.Boolean(string='Is a Subcontracting')
    subcon_production_id = fields.Many2one('mrp.production', string='Manufacturing Order')
    subcon_production_name = fields.Char(string='Manufacturing Order Name', compute=_compute_production_name, store=True)
    subcon_product_qty = fields.Float(string='Subcontract Quantity', digits='Product Unit of Measure', copy=False)
    subcon_qty_producing = fields.Float(string='Subcontract Producing Quantity', digits='Product Unit of Measure', compute=_compute_subcon_qty_producing, inverse=_inverse_subcon_qty_producing, store=True, copy=False)
    subcon_qty_produced = fields.Float(string='Subcontract Produced Quantity', digits='Product Unit of Measure', copy=False)
    subcon_uom_id = fields.Many2one('uom.uom', string='Unit of Measure')
    subcon_byproduct_ids = fields.One2many('stock.move', 'subcon_picking_id', 'Subcontracting By-Products')
    subcon_requisition_id = fields.Many2one('purchase.requisition', string='Subcon Blanket Order')
    is_readonly_origin = fields.Boolean()

    @api.constrains('is_a_subcontracting')
    def is_a_subcontracting_constraints(self):
        for record in self:
            if not record.is_a_subcontracting:
                continue
            missing_field = False
            if not record.subcon_production_id:
                missing_field = 'Manufacturing Order (subcon_production_id)'
            if not record.subcon_uom_id:
                missing_field = 'Subcontracting UoM (subcon_uom_id)'
            if missing_field:
                raise ValidationError(_('%s is mandatory when Subcontracting is True!' % missing_field))

    def _action_done(self):
        res = super(StockPickingInherit, self)._action_done()

        for record in self:
            if not record.is_a_subcontracting:
                continue

            service_line_ids = record.move_ids_without_package.filtered(
                lambda line: line.product_id.type == 'service')

            if not service_line_ids or record.picking_type_code != 'incoming':
                continue

            production_id = record.subcon_production_id
            requisition_id = record.subcon_requisition_id

            # TODO: minus quantity for move in?
            for svl in service_line_ids.stock_valuation_layer_ids:
                svl.write({'value': abs(svl.value) * -1, 'quantity': abs(svl.quantity) * -1})

            qty_producing = record.subcon_qty_producing
            qty_produced = record.subcon_qty_produced
            subcon_workorders = production_id.workorder_ids.filtered(lambda w: w.is_a_subcontracting)
            byproduct_ids = record.subcon_byproduct_ids.filtered(lambda b: not b.subcon_is_finished_good)

            for i, workorder in enumerate(subcon_workorders):
                workorder_byproduct_ids = byproduct_ids.filtered(lambda b: b.mrp_workorder_byproduct_id == workorder)
                consumption_values = {
                    'finished_product': qty_producing,
                    'is_a_subcontracting': True,
                    'requisition_id': requisition_id.id,
                    'purchase_id': record.purchase_id.id,
                    'picking_id': record.id,
                    'byproduct_ids': [(6, 0, workorder_byproduct_ids.ids)]
                }

                if i == len(subcon_workorders) - 1:
                    new_byproduct_ids = byproduct_ids.filtered(lambda b: not b.mrp_workorder_byproduct_id)
                    last_byproduct_ids = workorder_byproduct_ids | new_byproduct_ids
                    move_subcon_ids = record.move_ids_without_package
                    move_finished_ids = production_id.move_finished_ids.filtered(
                        lambda m: m.state not in ('done', 'cancel') and m.product_id == production_id.product_id
                    )

                    consumption_values.update({
                        'byproduct_ids': [(6, 0, last_byproduct_ids.ids)],
                        'move_subcon_ids': [(6, 0, move_subcon_ids.ids)],
                        'move_finished_ids': [(6, 0, move_finished_ids.ids)]
                    })

                workorder = workorder.with_context(
                    consumption_values=consumption_values,
                    bypass_approval=True,
                    bypass_onchange=True
                )
                workorder.button_start()
                workorder.button_finish()

            record.write({'subcon_qty_produced': qty_produced + qty_producing})
        return res

    def _create_backorder(self):
        backorders = super(StockPickingInherit, self)._create_backorder()

        for backorder in backorders:
            if not backorder.is_a_subcontracting:
                continue

            # copy subcon_byproduct_ids
            subcon_byproduct_ids = self.env['stock.move']
            for move in backorder.backorder_id.subcon_byproduct_ids:
                new_move = move.copy({
                    'mrp_plan_id': False,
                    'raw_material_production_id': False,
                    'production_id': False,
                    'mrp_workorder_component_id': False,
                    'mrp_workorder_byproduct_id': False,
                    'product_uom_qty': 1.0,
                    'mpr_quantity_done': 0.0,
                    'quantity_done': 0.0
                })
                subcon_byproduct_ids |= new_move
            backorder.subcon_byproduct_ids = [(6, 0, subcon_byproduct_ids.ids)]

            # calculate produced nested backorder
            origin = backorder
            qty_produced = 0.0
            while origin.backorder_id:
                origin = origin.backorder_id
                qty_produced += origin.subcon_qty_producing

            product_qty = origin.subcon_product_qty
            backorder.write({
                'subcon_product_qty': product_qty,
                'subcon_qty_produced': qty_produced,
                'subcon_qty_producing': product_qty - qty_produced
            })

        return backorders

    def set_subcon_qty_producing(self):
        self.ensure_one()
        qty_producing = 0.0
        if self.is_a_subcontracting:
            qty_producing = self.subcon_product_qty - self.subcon_qty_produced
            if self.subcon_byproduct_ids:
                qty_producing = sum(self.subcon_byproduct_ids.mapped('mpr_quantity_done'))
        self.subcon_qty_producing = qty_producing

    def validate_subcon_qty_producing(self):
        self.ensure_one()
        max_qty = self.subcon_product_qty - self.subcon_qty_produced
        min_qty = min(1.0, max_qty)
        if not min_qty <= self.subcon_qty_producing <= max_qty:
            message = _('Subcontracted must be at: %s <= Subcontracted <= %s!' % (min_qty, max_qty))
            raise ValidationError(message)

    def set_subcon_fg_line_qty_done(self):
        self.ensure_one()
        if not self.is_a_subcontracting:
            return
        byproduct_qty = sum(self.subcon_byproduct_ids.filtered(lambda b: not b.subcon_is_finished_good).mapped('mpr_quantity_done'))
        self.subcon_byproduct_ids.filtered(lambda b: b.subcon_is_finished_good).mpr_quantity_done = self.subcon_qty_producing - byproduct_qty

    def set_move_ids_without_package_from_subcon(self, validate=False):
        self.ensure_one()
        if not self.is_a_subcontracting:
            return
        if validate:
            self.validate_subcon_qty_producing()
        try:
            ratio = self.subcon_qty_producing / (self.subcon_product_qty - self.subcon_qty_produced)
        except ZeroDivisionError:
            ratio = 1.0
        for move in self.move_ids_without_package:
            move.update({'quantity_done': move.product_uom_qty * ratio})

    @api.constrains('subcon_qty_producing')
    def force_move_ids_without_package_from(self):
        for record in self:
            record.set_move_ids_without_package_from_subcon(validate=True)

    @api.onchange('subcon_qty_producing')
    def onchange_subcon_qty_producing(self):
        self.set_subcon_fg_line_qty_done()
        self.set_move_ids_without_package_from_subcon()

    def button_validate(self):
        for record in self:
            if not record.is_a_subcontracting:
                continue

            byproduct_ids = record.subcon_byproduct_ids.filtered(lambda m: not m.subcon_is_finished_good)
            total_allocated_cost = sum(byproduct_ids.mapped('allocated_cost'))
            if total_allocated_cost > 100:
                raise UserError(_('Total By-Products Allocated Cost must be less or equal to 100%!'))

            elif total_allocated_cost < 100:
                if any(move.mpr_quantity_done <= 0 for move in byproduct_ids):
                    raise UserError(_('One or more byproduct(s) has <= 0.0 quantity, it will not recorded on MPR!'))

        res = super(StockPickingInherit, self).button_validate()
        for line in self.move_ids_without_package:
            if line.move_line_nosuggest_ids:
                for i in line.move_line_nosuggest_ids:
                    lot = i.lot_id
                    lot.sudo().write({
                        'length': i.length,
                        'height': i.height,
                        'width': i.width,
                    })
        return res


class MRPSubcontractingByProduct(models.Model):
    _name = 'mrp.subcon.byproduct'
    _description = 'MRP Subcontracting By-Product'

    picking_id = fields.Many2one('stock.picking', string='Picking')
    is_finished_good = fields.Boolean()
    product_id = fields.Many2one('product.product', string='Product', required=True)
    product_qty = fields.Float(string='Quantity', digits='Product Unit of Measure')
    product_uom_id = fields.Many2one('uom.uom', string='UoM', required=True)
    allocated_cost = fields.Float('Allocated Cost (%)')

    def create_moves(self):
        stock_move_ids = self.env['stock.move']
        for record in self:
            if record.is_finished_good:
                continue
            production_id = record.picking_id.subcon_production_id
            move_values = production_id._get_move_finished_values(
                record.product_id.id,
                record.product_qty,
                record.product_uom_id.id
            )
            move_values.update({'quantity_done': record.product_qty})
            stock_move_ids |= self.env['stock.move'].create(move_values)
        return stock_move_ids
