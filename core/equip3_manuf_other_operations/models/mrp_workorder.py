from odoo import models, fields, api, _


class MrpWorkorderInherit(models.Model):
    _inherit = 'mrp.workorder'

    force_qty = fields.Float()

    @api.depends('production_id', 'force_qty')
    def _compute_qty_production(self):
        for wo in self:
            if wo.force_qty:
                wo.qty_production = wo.force_qty
                continue
            wo.qty_production = 0.0
            if wo.production_id:
                wo.qty_production = wo.production_id.product_qty

    def _inverse_qty_production(self):
        pass

    is_a_subcontracting = fields.Boolean(string='Subcontracting', default=False)
    qty_production = fields.Float('Original Production Quantity', compute=_compute_qty_production, inverse=_inverse_qty_production, related=False)

    def _prepare_consumption_vals(self):
        consumption_values = super(MrpWorkorderInherit, self)._prepare_consumption_vals()
        consumption_values.update(self.env.context.get('consumption_values', dict()))
        return consumption_values

    def _split(self, to_split_qty, subcon_location):
        self.ensure_one()

        if self.state not in ('pending', 'ready'):
            return

        new_ratio = to_split_qty / self.qty_production
        old_ratio = (self.qty_production - to_split_qty) / self.qty_production

        new_workorder = self.env['mrp.workorder']

        if to_split_qty < self.qty_production:
            new_move_raw = self.env['stock.move']
            new_byproduct = self.env['stock.move']
            for move in self.move_raw_ids | self.byproduct_ids:
                new_move = move.copy({
                    'product_uom_qty': old_ratio * move.product_uom_qty,
                    'allocated_cost': old_ratio * move.allocated_cost,
                })
                if new_move.raw_material_production_id:
                    new_move_raw |= new_move
                else:
                    new_byproduct |= new_move

            new_workorder = self.copy({
                'force_qty': self.qty_production - to_split_qty,
                'production_id': self.production_id.id,
                'move_raw_ids': [(6, 0, new_move_raw.ids)],
                'byproduct_ids': [(6, 0, new_byproduct.ids)]
            })

        self.write({
            'is_a_subcontracting': True,
            'force_qty': to_split_qty,
            'location_id': subcon_location.id,
            'move_raw_ids': [(1, move.id, {
                'product_uom_qty': new_ratio * move.product_uom_qty,
                'location_id': subcon_location.id
            }) for move in self.move_raw_ids],
            'byproduct_ids': [(1, move.id, {
                'product_uom_qty': new_ratio * move.product_uom_qty,
                'allocated_cost': new_ratio * move.allocated_cost
            }) for move in self.byproduct_ids]
        })

        return new_workorder
