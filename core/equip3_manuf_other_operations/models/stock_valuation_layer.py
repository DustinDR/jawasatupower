from odoo import models, fields, api
from odoo.exceptions import UserError


class StockValuationLayer(models.Model):
	_inherit = 'stock.valuation.layer'

	actualization_value = fields.Monetary(string='Actualization Value')
	mca_production_ids = fields.Many2many('mrp.cost.actualization.production', string='Production Cost Actualizations')
	type = fields.Selection(selection=[
		('none', 'None'),
		('component', 'Material'),
		('byproduct', 'By-Product'),
		('finished', 'Finished Goods'),
		('subcon', 'Subcontracting'),
	], string='Type', required=True, copy=False, default='none', readonly=True)
	move_lot_ids = fields.Many2many('stock.production.lot', related='stock_move_id.lot_ids')

	def update_value(self):
		self.ensure_one()
		if not self.quantity:
			return

		total_act_value = sum(self.mca_production_ids.mapped('total'))
		current_act_value = self.mca_production_ids[-1].total
		current_act_value = (self.quantity / self.mrp_production_id.product_qty) * current_act_value

		order_svl_ids = self.mrp_production_id.stock_valuation_layer_ids
		total_material = sum(order_svl_ids.filtered(lambda s: s.type == 'component').mapped('value'))

		percentage = self.value / (abs(total_material) + (total_act_value - current_act_value))
		new_actualization_value = percentage * current_act_value

		self.actualization_value += new_actualization_value

		self.value += new_actualization_value
		self.unit_cost = self.value / self.quantity
