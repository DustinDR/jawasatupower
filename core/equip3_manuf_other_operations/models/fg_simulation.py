from odoo import models, fields, api, _


class FinishedGoodSimulation(models.Model):
    _name = 'finished.good.simulation'
    _description = 'Finished Good Simulation'

    @api.depends('company_id')
    def _compute_allowed_branch(self):
        user_branch = self.env.user.branch_ids
        for record in self:
            allowed_branch = user_branch.filtered(lambda b: b.company_id == record.company_id)
            record.allowed_branch_ids = [(6, 0, allowed_branch.ids)]

    @api.onchange('finished_line_ids')
    def _onchange_finished_line_ids(self):
        material_lines = []

        material_bom = self.material_line_ids.mapped('bom_id')
        finished_bom = self.finished_line_ids.mapped('bom_id')
        bom_to_remove = material_bom - finished_bom
        material_to_remove = self.material_line_ids.filtered(lambda m: m.bom_id in bom_to_remove)
        for material in material_to_remove:
            material_lines.append((2, material.id))

        values = {}
        for line in self.finished_line_ids:
            material_ids = self.material_line_ids.filtered(lambda m: m.bom_id == line.bom_id)
            if not line.product_id or not line.bom_id or line.is_stimulated or material_ids:
                continue

            value = {
                'display_type': 'line_section',
                'simulation_id': self.id,
                'bom_id': line.bom_id.id,
                'product_id': line.product_id.id
            }
            material_lines.append((0, 0, value))

            for bom_line in line.bom_id.bom_line_ids:
                product_id = bom_line.product_id
                workcenter_id, location_id = self._get_workcenter(bom_line.operation_id)
                warehouse_id = location_id.get_warehouse()
                key = '%s_%s' % (product_id.id, warehouse_id.id)

                if not values.get(key):
                    product_id = product_id.with_context(warehouse=warehouse_id.id, location=False)
                    res = product_id._compute_quantities_dict(False, False, False, False, False)
                    qty_available = res[product_id.id]['qty_available']
                    free_qty = res[product_id.id]['free_qty']
                    leftover_qty = free_qty
                else:
                    qty_available = values[key]['on_hand_qty']
                    free_qty = values[key]['available_qty']
                    leftover_qty = 0.0

                value = {
                    'simulation_id': self.id,
                    'bom_id': line.bom_id.id,
                    'bom_qty': bom_line.product_qty,
                    'max_fg_qty': leftover_qty // bom_line.product_qty,
                    'product_id': product_id.id,
                    'on_hand_qty': qty_available,
                    'available_qty': free_qty,
                    'leftover_qty': leftover_qty,
                    'warehouse_id': warehouse_id.id,
                }

                material_lines.append((0, 0, value))
                values[key] = value
        self.material_line_ids = material_lines

    name = fields.Char(default=_('New'), required=True, readonly=True, copy=False, string='Reference')
    description = fields.Char(string='Name')
    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True, default=lambda self: self.env.company)
    allowed_branch_ids = fields.One2many('res.branch', compute=_compute_allowed_branch)
    branch_id = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id, domain="[('id', 'in', allowed_branch_ids)]")
    create_uid = fields.Many2one('res.users', string='Created By', default=lambda self: self.env.user, readonly=True)
    finished_line_ids = fields.One2many('finished.good.simulation.line', 'simulation_id', string='Finished Goods')
    material_line_ids = fields.One2many('finished.good.simulation.material', 'simulation_id', string='Materials')

    def _get_workcenter(self, operation):

        workcenter_id = self.env['mrp.workcenter']
        location_id = self.env['stock.location']

        if operation.work_center == 'without_group':
            workcenter_id = operation.workcenter_id
            location_id = operation.workcenter_id.wc_location

        elif operation.work_center == 'with_group':
            workorder_ids = self.env['mrp.workorder']
            for workcenter in operation.work_center_group.mrp_work_center_group_ids:
                workorder_id = self.env['mrp.workorder'].search([
                    ('workcenter_id', '=', workcenter.work_center.id),
                    ('date_planned_finished', '!=', False)
                ], order='date_planned_finished desc', limit=1)
                workorder_ids |= workorder_id

            if workorder_ids:
                workcenter_id = sorted(workorder_ids, key=lambda w: w.date_planned_finished)[0].workcenter_id
                location_id = workcenter_id.wc_location

        return workcenter_id, location_id

    def action_simulate(self):
        for line in self.finished_line_ids:
            if line.is_stimulated:
                continue
            material_ids = self.material_line_ids.filtered(lambda l: l.bom_id == line.bom_id and not l.display_type)
            max_fg_qty = min(material_ids.mapped('max_fg_qty'))
            estimated_fg_qty = max_fg_qty >= 0.0 and max_fg_qty or 0.0
            line.estimated_qty = estimated_fg_qty
            line.is_stimulated = True
            for material in material_ids:
                material.leftover_qty -= estimated_fg_qty * material.bom_qty
                material.max_fg_qty = material.leftover_qty // material.bom_qty

    @api.model
    def create(self, values):
        if values.get('name', _('New')) == _('New'):
            values['name'] = self.env['ir.sequence'].next_by_code(
                'finished.good.simulation', sequence_date=None
            ) or _('New')
        return super(FinishedGoodSimulation, self).create(values)


class FinishedGoodSimulationLine(models.Model):
    _name = 'finished.good.simulation.line'
    _description = 'Finished Good Simulation Line'

    def _bom_search(self):
        domain = [
            '&',
                '|',
                    ('company_id', '=', False),
                    ('company_id', '=', self.company_id.id),
            '&',
                '|',
                    '&',
                        ('product_id', '=', self.product_id.id),
                        ('product_id', '!=', False),
                    '&',
                        ('product_tmpl_id.product_variant_ids', '=', self.product_id.id),
                        ('product_id', '=', False),
                ('type', '=', 'normal')
        ]
        bom_ids = self.env['mrp.bom'].search(domain)
        other_lines = self.simulation_id.finished_line_ids - self
        other_bom_ids = other_lines.mapped('bom_id')
        return bom_ids - other_bom_ids

    @api.depends('simulation_id.company_id', 'simulation_id.finished_line_ids.bom_id', 'product_id')
    def _compute_allowed_bom_ids(self):
        for record in self:
            bom_ids = record._bom_search()
            record.allowed_bom_ids = [(6, 0, bom_ids.ids)]

    @api.onchange('product_id')
    def _onchange_product_id(self):
        if not self.company_id or not self.product_id:
            return
        bom_ids = self._bom_search()
        if bom_ids:
            self.bom_id = bom_ids[0].id

    simulation_id = fields.Many2one('finished.good.simulation', string='Simulation', ondelete='cascade')
    company_id = fields.Many2one('res.company', related='simulation_id.company_id')
    branch_id = fields.Many2one('res.branch', related='simulation_id.branch_id')
    product_id = fields.Many2one('product.product', required=True, domain="[('bom_ids', '!=', False), ('bom_ids.branch', '=', branch_id)]", string='Finished Good')
    allowed_bom_ids = fields.One2many('mrp.bom', compute=_compute_allowed_bom_ids)
    bom_id = fields.Many2one('mrp.bom', domain="[('id', 'in', allowed_bom_ids)]", required=True, string='BoM')
    estimated_qty = fields.Float(string='Estimated Producible Quantity', readonly=True, digits='Product Unit of Measure')
    is_stimulated = fields.Boolean()


class FinishedGOodSimulationMaterial(models.Model):
    _name = 'finished.good.simulation.material'
    _description = 'Finished Good Simulation Material'

    @api.depends('product_id', 'display_type')
    def _compute_name(self):
        for record in self:
            record.name = ''
            if record.product_id:
                product_name = record.product_id.display_name
                if not record.display_type:
                    record.name = '• ' + product_name
                else:
                    record.name = product_name

    name = fields.Text(string='Description', compute=_compute_name)
    simulation_id = fields.Many2one('finished.good.simulation', string='Simulation', ondelete='cascade')
    bom_id = fields.Many2one('mrp.bom', string='Finished Good')
    product_id = fields.Many2one('product.product', string='Material')
    on_hand_qty = fields.Float(string='On Hand', digits='Product Unit of Measure')
    available_qty = fields.Float(string='Available', digits='Product Unit of Measure')
    leftover_qty = fields.Float(string='Leftover', digits='Product Unit of Measure')
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse')

    bom_qty = fields.Float(string='BoM Quantity', digits='Product Unit of Measure')
    max_fg_qty = fields.Float(string='Maximum FG Produced', digits='Product Unit of Measure')
    display_type = fields.Selection([('line_section', 'Section'), ('line_note', 'Note')], default=False, help="Technical field for UX purpose.")
