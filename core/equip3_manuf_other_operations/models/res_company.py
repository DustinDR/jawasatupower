from odoo import models, fields, api
import random


class ResCompany(models.Model):
	_inherit = 'res.company'

	@api.model
	def create(self, vals):
		company = super(ResCompany, self).create(vals)
		self.env['stock.warehouse'].sudo().create({
			'name': "Subcontracting Warehouse",
			'code': 'SWH%s' % (random.randint(1, 999)),
			'company_id': company.id,
			'partner_id': company.partner_id.id
		})
		return company

	def write(self, vals):
		res = super(ResCompany, self).write(vals)
		menu_mrp_cost_act = self.env.ref("equip3_manuf_other_operations.menu_finance_entries_manufacturing")
		menu_cutting_root = self.env.ref('equip3_manuf_other_operations.menu_cutting_root')
		for company in self:
			if not company == self.env.company:
				continue
			menu_mrp_cost_act.active = company.manufacturing
			menu_cutting_root.active = company.cutting
		return res
