from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_compare


class MrpUnbuild(models.Model):
    _inherit = 'mrp.unbuild'

    @api.constrains('product_qty')
    def _check_qty(self):
        pass

    @api.model
    def change_sequence(self):

        company_ids = self.env['res.company'].sudo().search([])

        for company_id in company_ids:

            sequence_id = self.env['ir.sequence'].search([
                ('code', '=', 'mrp.unbuild'),
                ('company_id', '=', company_id.id)
            ])

            if sequence_id:
                sequence_id.write({
                    'prefix': 'UBR/%(y)s/%(month)s/%(day)s/',
                    'padding': 3,
                    'use_date_range': True
                })
            else:
                sequence_id = self.env['ir.sequence'].create({
                    'name': 'Unbuild',
                    'code': 'mrp.unbuild',
                    'company_id': company_id.id,
                    'prefix': 'UBR/%(y)s/%(month)s/%(day)s/',
                    'padding': 3,
                    'use_date_range': True,
                    'number_next': 1,
                    'number_increment': 1
                })

            today = fields.Date.today()
            date_range_ids = sequence_id.date_range_ids
            if not date_range_ids:
                date_range_ids = [sequence_id._create_date_range_seq(today)]

            for date_range in date_range_ids:
                if date_range.date_from <= today <= date_range.date_to:
                    next_number = date_range.number_next_actual
                    while True:
                        mo_name = sequence_id.get_next_char(next_number)
                        production_id = self.search([('name', '=', mo_name)])
                        if not production_id:
                            break
                        next_number += 1
                    date_range.number_next_actual = next_number

    def _get_product_id_domain(self):
        bom_ids = self.env['mrp.bom'].sudo().search([])
        product_tmpl_ids = []
        for bom_id in bom_ids:
            product_tmpl_ids.append(bom_id.product_tmpl_id.id)
        product_ids = self.env['product.product'].sudo().search([('product_tmpl_id', 'in', product_tmpl_ids)])
        domain = [('id', 'in', product_ids.ids)]
        return domain

    @api.depends('company_id')
    def _compute_allowed_branch(self):
        user_branch = self.env.user.branch_ids
        for record in self:
            allowed_branch = user_branch.filtered(lambda b: b.company_id == record.company_id)
            record.allowed_branch_ids = [(6, 0, allowed_branch.ids)]

    product_id = fields.Many2one(domain=_get_product_id_domain, tracking=True)
    company_id = fields.Many2one(tracking=True)
    product_qty = fields.Float(digits='Product Unit of Measure', tracking=True)
    product_uom_id = fields.Many2one(tracking=True)

    bom_id = fields.Many2one(tracking=True)
    mo_id = fields.Many2one(tracking=True)
    lot_id = fields.Many2one(tracking=True)

    blank_bom_id = fields.Many2one(
        'mrp.bom', 'Blank Bill of Material',
        domain="""[
        '|',
            ('product_id', '=', product_id),
            '&',
                ('product_tmpl_id.product_variant_ids', '=', product_id),
                ('product_id','=',False),
        ('type', '=', 'normal'),
        '|',
            ('company_id', '=', company_id),
            ('company_id', '=', False)
        ]""",
        states={'done': [('readonly', True)]}, check_company=True)

    allowed_branch_ids = fields.One2many('res.branch', compute=_compute_allowed_branch)
    branch_id = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id,
                             domain="[('id', 'in', allowed_branch_ids)]", readonly=True,
                             states={'draft': [('readonly', False)]}, tracking=True)
    mrp_unbuild_line_ids = fields.One2many('mrp.unbuild.line', 'mrp_unbild_id', string='Materials')

    trigger_field = fields.Char()

    @api.onchange('trigger_field')
    def onchange_trigger_field(self):
        if not self.product_id or not self.location_id or not self.product_uom_id:
            return
        self.check_availability()
        return self.action_validate()

    def check_availability(self):
        if self.product_qty <= 0:
            raise UserError(_('Unbuild Order product quantity has to be strictly positive.'))

        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        available_qty = self.env['stock.quant']._get_available_quantity(self.product_id, self.location_id, self.lot_id, strict=True)
        unbuild_qty = self.product_uom_id._compute_quantity(self.product_qty, self.product_id.uom_id)

        if float_compare(available_qty, unbuild_qty, precision_digits=precision) < 0:
            raise UserError(_('The product is not available in sufficient quantity in %s' % self.location_id.display_name))
        return True

    @api.onchange('bom_id')
    def _onchange_bom_id(self):
        if self.bom_id:
            lines = [(5, 0, 0)]
            if self.mo_id:
                lines = [(5, 0, 0)]
                for line in self.mo_id.move_raw_ids:
                    val = {
                        'product_id': line.product_id.id,
                        'product_uom_id': line.product_uom.id,
                        'product_qty': line.product_qty,
                        'bom_line_id': line.bom_line_id.id
                    }
                    lines.append((0, 0, val))
            else:
                for line in self.bom_id.bom_line_ids:
                    val = {
                        'product_id': line.product_id.id,
                        'product_uom_id': line.product_uom_id.id,
                        'product_qty': (self.product_qty / self.bom_id.product_qty) * line.product_qty,
                        'bom_line_id': line.id
                    }
                    lines.append((0, 0, val))

            self.mrp_unbuild_line_ids = lines

    @api.onchange('mo_id')
    def _onchange_mo_id(self):
        super(MrpUnbuild, self)._onchange_mo_id()
        if self.mo_id:
            self.blank_bom_id = False
            self._onchange_bom_id()

    @api.onchange('blank_bom_id')
    def onchange_blank_bom_id(self):
        if self.blank_bom_id:
            self.bom_id = self.blank_bom_id.id
            self.product_qty = self.blank_bom_id.product_qty
            self.mo_id = False
            self._onchange_bom_id()

    @api.onchange('product_qty')
    def _onchange_product_qty(self):
        if self.mo_id:
            if self.product_qty > self.mo_id.qty_produced:
                raise UserError(_('Quantity should be less or equal to selected Manufacturing order.'))

        if self.bom_id:
            for line in self.mrp_unbuild_line_ids:
                line.product_qty = (self.product_qty / self.bom_id.product_qty) * line.bom_line_id.product_qty

    def _account_entry_move(self):
        self.ensure_one()
        fg_journal_id = self.product_id.categ_id.property_stock_journal.id

        move_ids = self.env['stock.move'].search([('unbuild_id', '=', self.id)])
        svl_ids = move_ids.mapped('stock_valuation_layer_ids')

        move_lines = []
        for svl in svl_ids:
            svl_product_id = svl.product_id
            svl_value = svl.value
            svl_quantity = svl.quantity

            line = {
                'name': svl_product_id.name,
                'ref': self.name,
                'product_id': svl_product_id.id,
                'product_uom_id': svl_product_id.uom_id.id,
                'quantity': svl_quantity,
                'account_id': svl_product_id.categ_id.property_stock_valuation_account_id.id,
                'debit': svl_value < 0 and abs(svl_value) or 0.0,
                'credit': svl_value > 0 and svl_value or 0.0,
            }
            move_lines.append((0, 0, line))

        account_move_values = {
            'consumption_id': self.id,
            'journal_id': fg_journal_id,
            'date': fields.Datetime.now(),
            'move_type': 'entry',
            'line_ids': move_lines,
            'stock_valuation_layer_ids': [(6, False, svl_ids.ids)]
        }
        account_move_id = self.env['account.move'].create(account_move_values)
        account_move_id._post()
        account_move_id.ref = '%s - %s' % (self.name, self.product_id.name)

    def action_validate(self):
        self.ensure_one()
        self.check_availability()
        return super(MrpUnbuild, self).action_validate()

    def action_unbuild(self):
        res = super(MrpUnbuild, self).action_unbuild()

        if not self.mo_id:
            return res

        self._account_entry_move()
        return res


class MrpUnbuildLine(models.Model):
    _name = 'mrp.unbuild.line'

    def _get_default_product_uom_id(self):
        return self.env['uom.uom'].search([], limit=1, order='id').id

    mrp_unbild_id = fields.Many2one('mrp.unbuild', 'Parent MRP Unbuild ID', index=True, ondelete='cascade', required=True)
    product_id = fields.Many2one('product.product', 'Component', required=True, check_company=True)
    product_tmpl_id = fields.Many2one('product.template', 'Product Template', related='product_id.product_tmpl_id')
    company_id = fields.Many2one(related='mrp_unbild_id.company_id', store=True, index=True, readonly=True)
    product_qty = fields.Float('Quantity', default=1.0, digits='Product Unit of Measure', required=True)
    product_uom_id = fields.Many2one(
        'uom.uom', 'Product Unit of Measure',
        default=_get_default_product_uom_id,
        required=True,
        help="Unit of Measure (Unit of Measure) is the unit of measurement for the inventory control", domain="[('category_id', '=', product_uom_category_id)]")
    product_uom_category_id = fields.Many2one(related='product_id.uom_id.category_id')
    bom_line_id = fields.Many2one('mrp.bom.line', 'BOM Line ID')