from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class MrpProductionInherit(models.Model):
    _inherit = 'mrp.production'

    def _get_move_finished_values(self, product_id, product_uom_qty, product_uom, operation_id=False, byproduct_id=False):
        values = super(MrpProductionInherit, self)._get_move_finished_values(product_id, product_uom_qty, product_uom, operation_id, byproduct_id)
        if product_id == self.product_id.id:
            values['subcon_is_finished_good'] = True
        return values

    def _compute_subcon_count(self):
        po_subcon = self.env['purchase.order'].search([('is_a_subcontracting', '=', True)])
        pending_pr = self.env['purchase.request'].search([
            ('purchase_req_state', '=', 'pending'),
            ('is_a_subcontracting', '=', True)
        ])
        pending_po = po_subcon.filtered(lambda p: p.state in ('draft', 'sent'))
        subcon_count = po_subcon - pending_po
        requisition = self.env['purchase.requisition']
        for production in self:
            production.subcon_pr_pending_count = len(pending_pr.filtered(lambda p: p.subcon_production_id == production))
            production.subcon_po_pending_count = len(pending_po.filtered(lambda p: p.subcon_production_id == production))
            production.subcon_count = len(subcon_count.filtered(lambda p: p.subcon_production_id == production))
            production.subcon_requisition_count = requisition.search_count([('subcon_production_id', '=', production.id)])

    mca_production_ids = fields.One2many('mrp.cost.actualization.production', 'production_id', string='MCA Cost Lines')

    is_subcontracted = fields.Boolean()
    can_be_subcontracted = fields.Boolean(related='bom_id.can_be_subcontracted')
    subcon_pr_pending_count = fields.Integer(compute=_compute_subcon_count)
    subcon_po_pending_count = fields.Integer(compute=_compute_subcon_count)
    subcon_count = fields.Integer(compute=_compute_subcon_count)
    subcon_requisition_count = fields.Integer(compute=_compute_subcon_count)

    def get_subcon_location(self, partner_id, company_id):
        self.ensure_one()

        if company_id != self.company_id:
            raise ValidationError(_('The company must be same with company in Manufacturing Order!'))

        subcon_location = self.env['stock.location'].search([
            ('company_id', '=', company_id.id),
            ('barcode', 'like', 'SWH')], limit=1)
        if subcon_location:
            return subcon_location

        partner_subcon_location = partner_id.property_stock_subcontractor
        if partner_subcon_location:
            return partner_subcon_location

        return self.env['stock.location'].search([('company_id', '=', company_id.id)], limit=1)

    def split_workorders(self, to_subcontract_qty, partner_id):
        self.ensure_one()
        subcon_location = self.get_subcon_location(partner_id, self.company_id)

        for workorder in self.workorder_ids:
            new_workorder = workorder._split(to_subcontract_qty, subcon_location)

            if not new_workorder:
                continue

            (new_workorder.move_raw_ids | new_workorder.byproduct_ids)._adjust_procure_method()
            (new_workorder.move_raw_ids | new_workorder.byproduct_ids)._action_confirm()

            new_workorder._action_confirm()
            (new_workorder.move_raw_ids | new_workorder.byproduct_ids)._trigger_scheduler()

        return self.workorder_ids

    def action_confirm(self):
        for record in self:
            move_byproduct_ids = record.move_byproduct_ids or record.move_finished_ids.filtered(
                lambda m: m.product_id != record.product_id
            )
            if sum(move_byproduct_ids.mapped('allocated_cost')) > 100:
                raise UserError(_('Total By-Products Allocated Cost must be less or equal to 100%!'))
        return super(MrpProductionInherit, self).action_confirm()

    def action_subcontracting(self):
        view_id = self.env.ref('equip3_manuf_other_operations.mrp_subcontracting_wizard_view').id
        return {
            'type': 'ir.actions.act_window',
            'name': _('Subcontracting'),
            'res_model': 'mrp.subcontracting.wizard',
            'target': 'new',
            'view_mode': 'form',
            'views': [[view_id, 'form']],
            'context': {
                'default_production_id': self.id,
                'default_product_qty': self.product_qty,
            }
        }

    def action_view_pr_subcon_pending(self):
        return self.action_view_model(
            'purchase_request.purchase_request_form_action',
            'purchase.request',
            'purchase_request.view_purchase_request_form',
            [('subcon_production_id', '=', self.id), ('is_a_subcontracting', '=', True)],
        )

    def action_view_po_subcon_pending(self):
        return self.action_view_model(
            'equip3_purchase_operation.purchase_rfq_orders',
            'purchase.order',
            'purchase.purchase_order_form',
            [('subcon_production_id', '=', self.id), ('is_a_subcontracting', '=', True)],
        )

    def action_view_purchase_requisition(self):
        return self.action_view_model(
            'equip3_purchase_other_operation.product_purchase_blanket_order_services_orders',
            'purchase.requisition',
            'purchase_requisition.view_purchase_requisition_form',
            [('subcon_production_id', '=', self.id)],
        )
