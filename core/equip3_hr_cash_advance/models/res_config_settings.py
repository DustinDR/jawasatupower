from odoo import api, fields, models


class CashAdvanceResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    cash_type_approval = fields.Selection(
        [('employee_hierarchy', 'By Employee Hierarchy'), ('approval_matrix', 'By Approval Matrix')], default='employee_hierarchy',
        config_parameter='equip3_hr_cash_advance.cash_type_approval')
    cash_level = fields.Integer(config_parameter='equip3_hr_cash_advance.cash_level', default=1)
    send_by_wa_cashadvance = fields.Boolean(config_parameter='equip3_hr_cash_advance.send_by_wa_cashadvance')
    send_by_email_cashadvance = fields.Boolean(config_parameter='equip3_hr_cash_advance.send_by_email_cashadvance', default=True)

    @api.onchange("cash_level")
    def _onchange_cash_level(self):
        if self.cash_level < 1:
            self.cash_level = 1