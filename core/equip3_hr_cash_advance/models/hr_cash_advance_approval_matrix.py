# -*- coding: utf-8 -*-

from odoo import api, fields, models, _, tools
from lxml import etree

class CashAdvanceApprovalMatrix(models.Model):
    _name = 'hr.cash.advance.approval.matrix'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Cash Advance Approval Matrix"
    _order = 'create_date desc'

    name = fields.Char('Name')
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company.id)
    apply_to = fields.Selection(
        [('by_employee', 'By Employee'), ('by_job_position', 'By Job Position'), ('by_department', 'By Department')])
    employee_ids = fields.Many2many('hr.employee')
    deparment_ids = fields.Many2many('hr.department')
    job_ids = fields.Many2many('hr.job')
    minimum_amount = fields.Integer('Minimum Amount')
    maximum_amount = fields.Integer('Maximum Amount')

    level = fields.Integer(compute="_get_level")
    # career_transition_type = fields.Many2many('career.transition.type')
    approval_matrix_ids = fields.One2many('hr.cash.advance.approval.matrix.line', 'approval_matrix_id')
    
    
    @api.model
    def fields_view_get(self, view_id=None, view_type=None,
                        toolbar=True, submenu=True):
        res = super(CashAdvanceApprovalMatrix, self).fields_view_get(
            view_id=view_id, view_type=view_type,toolbar=toolbar,submenu=submenu)
        if  self.env.user.has_group('equip3_accounting_accessright_setting.group_cash_advance_manager'):
            root = etree.fromstring(res['arch'])
            root.set('create', 'true')
            root.set('edit', 'true')
            root.set('delete', 'true')
            res['arch'] = etree.tostring(root)
        else:
            root = etree.fromstring(res['arch'])
            root.set('create', 'false')
            root.set('edit', 'false')
            root.set('delete', 'false')
            res['arch'] = etree.tostring(root)
            
        return res

    @api.depends('approval_matrix_ids')
    def _get_level(self):
        for record in self:
            if record.approval_matrix_ids:
                record.level = len(record.approval_matrix_ids)
            else:
                record.level = 0


class CashAdvanceApprovalMatrixline(models.Model):
    _name = 'hr.cash.advance.approval.matrix.line'

    approval_matrix_id = fields.Many2one('hr.cash.advance.approval.matrix')
    sequence = fields.Integer()
    approvers = fields.Many2many('res.users')
    minimum_approver = fields.Integer(default=1)

    @api.model
    def default_get(self, fields):
        res = super(CashAdvanceApprovalMatrixline, self).default_get(fields)
        if self.env.context:
            context_keys = self.env.context.keys()
            next_sequence = 1
            if 'approval_matrix_ids' in context_keys:
                if len(self.env.context.get('approval_matrix_ids')) > 0:
                    next_sequence = len(self.env.context.get('approval_matrix_ids')) + 1
        res.update({'sequence': next_sequence})
        return res