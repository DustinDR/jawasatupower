from odoo import fields, models, api


class VendorDepositHrCashAdvanceWizard(models.TransientModel):
    _name = 'vendor.deposit.wizard'

    feedback = fields.Text()

    def submit(self):
        """ Prepare the Cash Advance feedback and trigger Approve. """
        self.ensure_one()
        cash_advance = self.env['vendor.deposit'].browse(self._context.get('active_ids', []))
        cash_advance.feedback_parent = self.feedback
        cash_advance.action_approve()
