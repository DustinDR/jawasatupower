# -*- coding: utf-8 -*-
{
    'name': "Equip3 - General Features",

    'summary': """
        Manage General Features include Company, Branch, and Product""",

    'description': """
        
    """,

    'author': "Hashmicro",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '1.1.9',

    # any module necessary for this one to work correctly
    'depends': ['sale_stock', 'base', 'product', 'branch', 'mail'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        # 'views/views.xml',
        # 'views/templates.xml',
        'views/product_category_views.xml',
        'views/product_product_views.xml',
        'views/product.xml',
        'views/product_brand.xml',
        'views/res_company_views.xml',
        "views/res_config_settings_view.xml",
        'views/res_branch_views.xml',
        'views/res_users_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
