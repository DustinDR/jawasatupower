
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class ProductCategory(models.Model):
    _inherit = 'product.category'

    is_generate_product_code = fields.Boolean(string="Autogenerate Product Code", default=True)
    category_prefix_preference = fields.Selection([
        ('all_products_in_this_category_will_have_same_prefix' , 'All Products In This Category Will Have Same Prefix'),
        ('each_product_will_have_different_prefix' , 'Additional Prefix Will Be Define On Product'),
        ],string="Prefix Preference", default='all_products_in_this_category_will_have_same_prefix')
    category_prefix = fields.Char(string="Product Category Prefix")
    digits = fields.Integer(string="Digits", default=3)
    current_sequence = fields.Char(string="Current Sequence", default='1')

    @api.constrains('category_prefix')
    def _check_prefix_limit(self):
        for record in self:
            if record.is_generate_product_code:
                IrConfigParam = self.env['ir.config_parameter'].sudo()
                prefix_limit = int(IrConfigParam.get_param('prefix_limit', 5))
                if len(record.category_prefix) > prefix_limit:
                    raise ValidationError("Prefix value size must be less or equal to %s" % (prefix_limit))

    @api.onchange('digits')
    def generate_current_sequence(self):
        number = self.current_sequence.lstrip('0')
        if self.digits < len(number):
            raise ValidationError(_('Digits Not Acceptable!'))
        current_sequence_length = len(self.current_sequence)
        if self.digits > current_sequence_length:
            number_length = len(number)
            original_number_length = self.digits - number_length
            add_zero_original_number = '0' * original_number_length
            self.current_sequence = add_zero_original_number + number
        elif self.digits < current_sequence_length:
            self.current_sequence = self.current_sequence[-self.digits:]

    @api.onchange('name')
    def _onchange_name(self):
        if self.name:
            split_name = self.name.split(" ")
            if len(split_name) == 1:
                name = split_name[0][:3].upper()
                self.category_prefix = name
            elif len(split_name) == 2:
                name = split_name[0][0]
                name_1 = split_name[1][0]
                name_2 = ''
                if len(split_name[1]) > 1:
                    name_2 = split_name[1][1]
                final_name = (name + name_1 + name_2).upper()
                self.category_prefix = final_name
            elif len(split_name) > 2:
                name = split_name[0][0]
                name_1 = split_name[1][0]
                name_2 = split_name[2][0]
                final_name = (name + name_1 + name_2).upper()
                self.category_prefix = final_name
        else:
            self.category_prefix = False
