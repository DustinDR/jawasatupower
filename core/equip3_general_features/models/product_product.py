
from re import findall as regex_findall
from re import split as regex_split
from odoo import api, fields, models, _

class ProductProduct(models.Model):
    _inherit = 'product.product'

    product_prefix = fields.Char(string="Product Code Prefix", size=10)
    is_generate_product = fields.Boolean(related='categ_id.is_generate_product_code')
    category_prefix_preference = fields.Selection(related='categ_id.category_prefix_preference')

    @api.onchange('categ_id', 'product_prefix')
    def _onchange_product_categ_id(self):
        name = ''
        if self.product_prefix and self.categ_id and \
            self.categ_id.category_prefix and \
            self.categ_id.current_sequence and \
            self.categ_id.is_generate_product_code and \
            self.categ_id.category_prefix_preference == 'each_product_will_have_different_prefix':
            name = self.categ_id.category_prefix + "-" + self.product_prefix + "-" + self.categ_id.current_sequence
            self.default_code = name
        elif self.categ_id and \
            self.categ_id.is_generate_product_code and \
            self.categ_id.category_prefix_preference == 'all_products_in_this_category_will_have_same_prefix':
            if self.variant_short_name and self.categ_id.category_prefix and self.categ_id.current_sequence:
                name = self.categ_id.category_prefix + "-" + self.variant_short_name +"-" + self.categ_id.current_sequence
            elif self.categ_id.category_prefix and self.categ_id.current_sequence:
                name = self.categ_id.category_prefix + "-" + self.categ_id.current_sequence
            self.default_code = name
        else:
            self.default_code = ""

    @api.model_create_multi
    def create(self, vals_list):
        product_ids = super(ProductProduct, self).create(vals_list)
        for product_id in product_ids:
            if product_id.is_generate_product and \
                not product_id.product_prefix:
                name = ''
                if product_id.product_tmpl_id.product_prefix:
                    name = product_id.product_tmpl_id.product_prefix
                if name != '' and product_id.variant_short_name:
                    name += '-' + product_id.variant_short_name
                product_id.product_prefix = name
                product_id._onchange_product_categ_id()
                next_serial = product_id.categ_id.current_sequence
                caught_initial_number = regex_findall("\d+", next_serial)
                initial_number = caught_initial_number[-1]
                padding = len(initial_number)
                # We split the serial number to get the prefix and suffix.
                splitted = regex_split(initial_number, next_serial)
                # initial_number could appear several times in the SN, e.g. BAV023B00001S00001
                prefix = initial_number.join(splitted[:-1])
                suffix = splitted[-1]
                initial_number = int(initial_number)

                lot_names = []
                for i in range(0, 2):
                    lot_names.append('%s%s%s' % (
                        prefix,
                        str(initial_number + i).zfill(padding),
                        suffix
                    ))
                next_serial_number = lot_names[-1]
                product_id.categ_id.current_sequence = next_serial_number
        return product_ids
