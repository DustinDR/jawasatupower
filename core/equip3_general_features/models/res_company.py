
from odoo import models, fields, api, _


class ResCompany(models.Model):
    _name = 'res.company'
    _inherit = ['res.company', 'mail.thread', 'mail.activity.mixin']

    partner_id = fields.Many2one(tracking=True)
    street = fields.Char(tracking=True)
    phone = fields.Char(tracking=True)
    email = fields.Char(tracking=True)
    vat = fields.Char(tracking=True)
    company_registry = fields.Char(tracking=True)
    currency_id = fields.Many2one(tracking=True)
    parent_id = fields.Many2one(tracking=True)
    social_twitter = fields.Char(tracking=True)
    social_facebook = fields.Char(tracking=True)
    social_github = fields.Char(tracking=True)
    social_linkedin = fields.Char(tracking=True)
    social_youtube = fields.Char(tracking=True)    
    social_instagram = fields.Char(tracking=True)
    


