from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class StockProduct(models.Model):
    _inherit = 'product.template'

    multi_companies_all = fields.Many2many('res.company', 'product_id', 'company_id', 'prod_comp_id', string='Allowed  Companies', tracking=True)
    product_brand_id = fields.Many2many('product.brand', string='Brand')
    product_prefix = fields.Char(string="Product Code Prefix", required=True, size=10)
    is_generate_product = fields.Boolean(related='categ_id.is_generate_product_code')
    category_prefix_preference = fields.Selection(related='categ_id.category_prefix_preference')
    alternative_product_ids = fields.Many2many('product.product', 'product_product_alternative_rel', 'product_id', 'tmpl_id', string="Alternative Product")
    category_sequence = fields.Char(string='Category Sequence')
    default_code = fields.Char(string="Product Code")

    @api.onchange('categ_id')
    def onchange_categ_id_seq(self):
        self.category_sequence = False
        if self.categ_id:
            self.category_sequence = self.categ_id.current_sequence

    @api.onchange('categ_id', 'product_prefix', 'category_sequence')
    def _onchange_categ_id(self):
        if self.product_prefix and self.category_sequence and self.categ_id and \
            self.categ_id.is_generate_product_code and \
            self.categ_id.category_prefix_preference == 'each_product_will_have_different_prefix':
            name = self.categ_id.category_prefix + "-" + self.product_prefix + "-" + self.category_sequence
            self.default_code = name
        elif self.categ_id and \
            self.category_sequence and \
            self.categ_id.category_prefix and \
            self.categ_id.is_generate_product_code and \
            self.categ_id.category_prefix_preference == 'all_products_in_this_category_will_have_same_prefix':
            name = self.categ_id.category_prefix + "-" + self.category_sequence
            self.default_code = name
        else:
            self.default_code = " "

    def write(self, vals):
        res = super(StockProduct, self).write(vals)
        for record in self:
            if record.is_generate_product and \
                record.product_variant_ids and \
                record.category_prefix_preference == 'each_product_will_have_different_prefix' and \
                record.product_variant_ids.filtered(lambda r:not r.product_prefix):
                product_variant_ids = record.product_variant_ids.filtered(lambda r:not r.product_prefix)
                for variant_id in product_variant_ids:
                    name = record.product_prefix
                    if variant_id.variant_short_name:
                        name += ' - ' + variant_id.variant_short_name
                    variant_id.product_prefix = name
            if vals.get('attribute_line_ids'):
                record._onchange_categ_id()
        return res

class ProductProduct(models.Model):
    _inherit = 'product.product'

    alternative_product_ids = fields.Many2many('product.product', 'prod_id_alternative_rel', 'prod', 'prod_id', string="Alternative Product")
    default_code = fields.Char(string="Product Code")

