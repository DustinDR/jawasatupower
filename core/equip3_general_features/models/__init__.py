# -*- coding: utf-8 -*-

from . import product
from . import product_brand
from . import product_category
from . import product_product
from . import res_company
from . import res_branch
from . import res_config
