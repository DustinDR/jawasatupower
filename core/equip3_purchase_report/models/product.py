from odoo import api, fields, models, _
from datetime import date, datetime

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @staticmethod
    def _get_empty_widget_lines(domain):
        empty_widget_lines = [
            {'name': _('Requests for Quotation'), 'states': ('draft', 'sent'), 'count': 0, 'amount': 0.0},
            {'name': _('RFQ Under Approval'), 'states': ('waiting_for_approve',), 'count': 0, 'amount': 0.0},
            {'name': _('Purchase Order'), 'states': ('purchase', 'done'), 'count': 0, 'amount': 0.0},
            {'name': _('Cancelled PO'), 'states': ('cancel',), 'count': 0, 'amount': 0.0}
        ]
        for empty_widget_line in empty_widget_lines:
            empty_widget_line['domain'] = str(domain + [('state', 'in', empty_widget_line['states'])])
        return empty_widget_lines

    def _retrieve_dashboard_data_by_users(self, year):
        year_start = fields.Date.to_string(date(year, 1, 1))
        year_end = fields.Date.to_string(date(year, 12, 31))

        query = """
        SELECT
        po.user_id,
        partner.name,
        po.state,
        COUNT(1) as count,
        SUM(COALESCE(po.amount_total / NULLIF(po.currency_rate, 0), po.amount_total)) as amount,
        MIN(curr.decimal_places) as decimal_places
        FROM purchase_order po
        JOIN res_users users ON (po.user_id = users.id)
        JOIN res_partner partner ON (users.partner_id = partner.id)
        JOIN res_company comp ON (po.company_id = comp.id)
        JOIN res_currency curr ON (comp.currency_id = curr.id)
        WHERE po.company_id = %s AND po.date_calendar_start >= %s AND po.date_calendar_start <= %s AND po.dp = False
        GROUP BY po.user_id, partner.name, po.state;
        """
        self._cr.execute(query, (self.env.company.id, year_start, year_end))
        return self.env.cr.dictfetchall()

    def _retrieve_dashboard_data_by_partners(self, year):
        year_start = fields.Date.to_string(date(year, 1, 1))
        year_end = fields.Date.to_string(date(year, 12, 31))
        query = """
        SELECT
        po.partner_id,
        partner.name,
        po.state,
        COUNT(1) as count,
        SUM(COALESCE(po.amount_total / NULLIF(po.currency_rate, 0), po.amount_total)) as amount,
        MIN(curr.decimal_places) as decimal_places
        FROM purchase_order po
        JOIN res_partner partner ON (po.partner_id = partner.id)
        JOIN res_company comp ON (po.company_id = comp.id)
        JOIN res_currency curr ON (comp.currency_id = curr.id)
        WHERE po.company_id = %s AND po.date_calendar_start >= %s AND po.date_calendar_start <= %s AND po.dp = False
        GROUP BY po.partner_id, partner.name, po.state;
        """
        self._cr.execute(query, (self.env.company.id, year_start, year_end))
        return self.env.cr.dictfetchall()

    def _retrieve_dashboard_data_by_products(self, year):
        year_start = fields.Date.to_string(date(year, 1, 1))
        year_end = fields.Date.to_string(date(year, 12, 31))
        query = """
        SELECT
        pol.product_id,
        product_template.name,
        pol.state,
        COUNT(1) as count,
        SUM(COALESCE(pol.price_total / NULLIF(po.currency_rate, 0), pol.price_total)) as amount,
        MIN(curr.decimal_places) as decimal_places
        FROM purchase_order_line pol
        JOIN purchase_order po ON (pol.order_id = po.id)
        JOIN product_product product ON (pol.product_id = product.id)
        JOIN product_template product_template ON (product.product_tmpl_id = product_template.id)
        JOIN res_company comp ON (po.company_id = comp.id)
        JOIN res_currency curr ON (comp.currency_id = curr.id)
        WHERE po.company_id = %s AND po.date_calendar_start >= %s AND po.date_calendar_start <= %s AND po.dp = False
        GROUP BY pol.product_id, product_template.name, pol.state;
        """
        self._cr.execute(query, (self.env.company.id, year_start, year_end))
        return self.env.cr.dictfetchall()

    @api.model
    def get_dashboard_widgets(self, year, group_by='user_id'):

        if group_by == 'partner_id':
            res_model = 'res.partner'
            res = self._retrieve_dashboard_data_by_partners(year)
        elif group_by == 'product_id':
            res_model = 'product.product'
            res = self._retrieve_dashboard_data_by_products(year)
        else:
            group_by = 'user_id'
            res_model = 'res.users'
            res = self._retrieve_dashboard_data_by_users(year)

        dashboard_widgets = {}
        for line in res:
            self._append_dashboard_widgets(dashboard_widgets, line, res_model, group_by, year)

        return dashboard_widgets

    @api.constrains('state', 'po_state')
    def set_po_qty(self):
        for res in self:
            if res.state == 'purchase' or res.po_state == 'purchase':
                for line in res.order_line:
                    line.product_id.get_po_ids()

class PurchaseRequest(models.Model):
    _inherit = 'purchase.request'

    @api.constrains('pr_state', 'purchase_req_state')
    def set_pr_qty(self):
        for res in self:
            if res.pr_state == 'approved':
                for line in res.line_ids:
                    line.product_id.get_pr_ids()

class ProductProduct(models.Model):
    _inherit = "product.product"

    po_count = fields.Float(string="Purchase Qty")
    pr_count = fields.Float(string="Purchase Request")
    purchase_req_line_ids = fields.One2many(
        'purchase.request.line',
        'product_id',
        string="Purchase Request Line",
        readonly=True,
        copy=False,
    )

    def get_po_ids(self):
        for rec in self:
            count = 0
            total = 0
            for line in rec.purchase_order_line_ids:
                if line.order_id.state == 'purchase':
                    count += line.product_qty
                    total += line.price_subtotal
            rec.write({
                'purchase_price': rec.last_purchase_price,
                'purchase_price_totals': total,
                'po_count': count
            })

    def get_pr_ids(self):
        for rec in self:
            count = 0
            for line in rec.purchase_req_line_ids:
                if line.request_id.state == 'approved':
                    count += line.product_qty
            rec.write({
                'purchase_price': rec.last_purchase_price,
                'pr_count': count
            })