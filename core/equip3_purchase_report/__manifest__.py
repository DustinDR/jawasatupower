# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Equip3 Purchase Report',
    'version': '1.1.8',
    'summary': 'Manage your Purchase Requests and Purchase Orders Report Analysis',
    'depends': ['purchase','purchase_request','sh_purchase_reports','equip3_purchase_operation','equip3_purchase_masterdata','xf_purchase_dashboard','ks_dashboard_ninja','sh_po_tender_management','vendor_evaluation'],
    'category': 'Purchase/Purchase',
    'data': [
        'data/ks_purchase_dash.xml',
        'security/ir.model.access.csv',
        'report/purchase_analysis_report_view.xml',
        'views/purchase_order_view.xml',
        'views/vendor_rating_view.xml',
        'views/report_purchase_product_profit.xml',
        'views/payment_report.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}