from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import date, datetime


class ProgressiveBill(models.Model):
    _inherit = 'progressive.claim'


    source_document = fields.Char("Source Document")
    vendor = fields.Many2one("res.partner", "Vendor")
    progressive_bill = fields.Boolean('Progressive Bill', default=False)

    # tab Bill History
    bill_history_ids = fields.One2many('bill.history.line', 'bill_history_id', string="Bill History")

    bill_total_invoice = fields.Float(string="Total Amount Invoiced", readonly=True, store=True, currency_field='currency_id')
    bill_total_claim = fields.Float(readonly=True, string="Total Amount Claimed", compute='_compute_bill_amount', store=True)
    bill_remaining_amount = fields.Float(readonly=True, string="Remaining Amount", compute='_compute_bill_amount', store=True)

    # tab Bill Request
    bill_request_ids = fields.One2many('bill.request.line', 'bill_request_id', string="Bill Request")

    @api.onchange('progressive_bill')
    def _onchange_progressive_bill(self):
        context = dict(self.env.context) or {}
        if context.get('progressive_bill'):
            self.progressive_bill = True

    @api.depends('bill_request_ids.approved_progress')
    def _compute_approved_progress(self):
        total = 0
        for res in self:
            total = sum(res.bill_request_ids.mapped('approved_progress'))
            res.approved_progress = total
        return total

    @api.depends('bill_history_ids')
    def _compute_bill_amount(self):
        for rec in self:
            bill_total_claim = 0.0
            bill_remaining_amount = 0.0
            bill_history_ids = rec.bill_history_ids
            if len(bill_history_ids) > 0:
                bill_total_claim = sum([i.amount_invoice for i in bill_history_ids if i.payment_status in ['paid','partial']])
                bill_remaining_amount = self.contract_amount - bill_total_claim
            rec.bill_total_claim = bill_total_claim
            rec.bill_remaining_amount = bill_remaining_amount


class BillRequest(models.Model):
    _name = "bill.request.line"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _description = "Bill Request Line"
    _rec_name = 'request_id'
    _order = 'sequence'
    _check_company_auto = True

    def send_request(self):
        res = self.write({'request_status': 'waiting',
                          'request_status_2': 'waiting' 
                         })
        return res

    def confirm_request(self):
        res = self.write({'request_status': 'approved',
                          'request_status_2': 'approved', 
                          'approved_date' : datetime.now()
                         })
        return res


    def reject_request(self):
        res = self.write({'request_status': 'rejected',
                          'request_status_2': 'rejected' 
                         })
        return res

    @api.depends('bill_request_id.bill_request_ids', 'bill_request_id.bill_request_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.bill_request_id.bill_request_ids:
                no += 1
                l.sr_no = no

    
    @api.depends('request_status', 'requested_progress')
    def _approved_progress_line(self):
        for line in self:
            line.approved_progress = 0
            if line.request_status == 'approved':
                line.approved_progress = line.requested_progress
            return line.approved_progress

    bill_request_id = fields.Many2one('progressive.claim', string='Bill Request ID', ondelete='cascade')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer(string="No", compute="_sequence_ref")
    request_id = fields.Many2one('claim.request', string="Claim ID")
    company_id = fields.Many2one(related='bill_request_id.company_id', string='Company')
    creation_date = fields.Datetime(string='Creation Date')
    approved_date = fields.Datetime(string='Approved Date')
    created_by = fields.Many2one('res.users', string='Created By')
    project_id = fields.Many2one(related='bill_request_id.project_id', string='Project')
    partner_id = fields.Many2one(related='bill_request_id.partner_id', string='Customer')
    partner_request_id = fields.Many2one('res.partner', string='Request Address')
    project_director = fields.Many2one(related='bill_request_id.project_director', string='Project Director')
    requested_progress = fields.Float(string='Requested Progress (%)')
    approved_progress = fields.Float(string='Approved Progress (%)', compute="_approved_progress_line")
    requested_amount = fields.Float(string='Requested Amount')
    requested_amount1 = fields.Float(string='Requested Amount')
    contract_amount = fields.Float(string='Contract Amount')
    #request_line = fields.Many2many('request.line', string='Request Lines', copy=False)
    request_ids = fields.One2many('request.from.line', 'request_line_id', string="Request ID")
    request_status = fields.Selection([
                        ('draft', 'Draft'),
                        ('waiting', 'Waiting For Approval'),
                        ('approved', 'Approved'),
                        ('rejected', 'Rejected')
                ], string="Request Status", tracking=True, default='draft')
    request_status_2 = fields.Selection(related="request_status", default='draft')
    request_for = fields.Selection([
        ('down_payment', 'Down Payment'),
        ('progress', 'Progress'),
        ('retention', 'Retention')
        ], string='Requested Type')
    retention = fields.Selection([
        ('retention1', 'Retention 1'),
        ('retention2', 'Retention 2')
        ], string='Retention')
    show_approval_buttons = fields.Binary(default=False, compute='_show_approval_buttons')

    def _show_approval_buttons(self):
        for bill_request in self:
            if bill_request.project_director and self.env.user.id == bill_request.project_director.id:
                bill_request.show_approval_buttons = True
            else:
                bill_request.show_approval_buttons = False


class BillHistory(models.Model):
    _name = 'bill.history.line'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _rec_name = 'claim_name'
    _description = "Bill History"
    _order = "date, create_date desc, id desc"

    @api.depends('bill_history_id.bill_history_ids', 'bill_history_id.bill_history_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.bill_history_id.bill_history_ids:
                no += 1
                l.sr_no = no

    bill_history_id = fields.Many2one('progressive.claim', string="Bill History ID", ondelete='cascade')
    invoice_id = fields.Many2one('account.move','Invoice', ondelete='cascade')
    company_id = fields.Many2one(related='bill_history_id.company_id', string='Company', readonly=True)
    currency_id = fields.Many2one(related='bill_history_id.currency_id', string="Currency", readonly=True)
    claim_name = fields.Char(string="Claim ID", related='invoice_id.claim_description')
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date =  fields.Date(string="Date", related='invoice_id.invoice_date')
    progressline = fields.Float(string = 'Progress (%)', related='invoice_id.progress')
    gross_amount = fields.Monetary(string="Gross Amount", currency_field='currency_id', compute='_compute_progressive')
    dp_deduction = fields.Monetary(string="DP Deduction", currency_field='currency_id', compute='_compute_progressive')
    retention_deduction = fields.Monetary(string="Retention Deduction", currency_field='currency_id', compute='_compute_progressive')
    amount_deduction = fields.Monetary(string="Amount After Deduction", currency_field='currency_id', compute='_compute_progressive')
    paid_invoice = fields.Monetary(string="Paid Invoice", currency_field='currency_id', compute='_compute_invoice')
    amount_untaxed = fields.Float(string = 'Amount Untaxed',related='invoice_id.amount_untaxed', currency_field='currency_id')
    tax_amount = fields.Monetary(string="Tax Amount", currency_field='currency_id', compute='_compute_progressive')
    amount_invoice = fields.Monetary(string="Amount Invoice", currency_field='currency_id', compute='_compute_progressive')
    invoice_status = fields.Selection(related='invoice_id.state',string='Invoice Status')
    payment_status = fields.Selection(related='invoice_id.payment_state',string='Invoice Status')
    claim_for = fields.Selection(related='invoice_id.progressive_method', string='Claim Type')
    progress = fields.Float(string = 'Progress (%)',compute='_compute_invoice')
    retention = fields.Selection(related='invoice_id.progressive_method', string="Retention")

    def _compute_invoice(self):
        for rec in self:
            def _get_amount_deduction_retention(inv_id):
                vals = {
                    'dp':0.0,
                    'retention':0.0,
                    'progress':0.0,
                }
                invoice_line_ids = inv_id.invoice_line_ids
                if len(invoice_line_ids) > 0:
                    for line in invoice_line_ids:
                        vals['dp'] += line.dp_deduction
                        vals['retention'] += line.retention_deduction
                        vals['progress'] += line.progress
                return vals
            
            def _get_paid_inv(bill_history_id,progressive_method):
                value = 0.0
                domain = [('bill_history_id','=',bill_history_id.id),('progressive_method','=','progress')]
                inv_ids = self.env['account.move'].search(domain,order='create_date')
                if progressive_method == 'down_payment' or progressive_method == 'retention':
                    value = 0.0
                elif len(inv_ids) > 0:
                    vals = []
                    for inv in inv_ids:
                        if inv.id == rec.invoice_id.id:
                            break
                        vals.append(inv.amount_untaxed)
                    if len(vals) > 0:
                        value += sum(vals)
                return value
            
            inv_id = rec.invoice_id
            bill_history_id = rec.bill_history_id
            total_dp_deduction = 0.0
            total_retention = 0.0
            total_progress = 0.0
            progressive_method = rec.claim_for
            total_paid_invoice = _get_paid_inv(bill_history_id,progressive_method)
            get_amount = _get_amount_deduction_retention(inv_id)     

            if get_amount['dp'] > 0:
                total_dp_deduction += get_amount['dp']
            if get_amount['retention'] > 0:
                total_retention += get_amount['retention']
            if get_amount['progress'] > 0:
                total_progress += get_amount['progress']

            rec.paid_invoice = total_paid_invoice
            rec.progress = total_progress
            rec.dp_deduction = total_dp_deduction
            rec.retention_deduction = total_retention
    

    def _compute_progressive(self):
        for rec in self:
            bill_history_id = rec.bill_history_id
            claim_for = rec.claim_for
            retention = rec.retention
            progressline = rec.progressline
            contract_amount = bill_history_id.contract_amount
            dp_amount = bill_history_id.dp_amount
            retention1 = bill_history_id.retention1
            retention2 = bill_history_id.retention2
            tax_id = bill_history_id.tax_id
            amount_untaxed = rec.amount_untaxed

            

            def _compute_gross_amount(claim_for,contract_amount,dp_amount,progressline,retention,retention1,retention2):
                value = 0.0
                if claim_for == 'down_payment':
                    value += dp_amount
                elif claim_for == 'progress':
                    value += contract_amount * (progressline/100)
                elif claim_for == 'retention':
                    if retention == 'retention1':
                        value += contract_amount * (retention1/100)
                    elif retention == 'retention2':
                        value += contract_amount * (retention2/100)
                return value

            def _compute_dp_deduction(claim_for,progressline,dp_amount):
                vals = 0.0
                if claim_for == 'down_payment' or claim_for == 'retention':
                    vals += 0.0
                elif claim_for == 'progress':
                    vals += (dp_amount * (progressline/100)) * -1
                return vals

            def _compute_retention_deduction(claim_for,gross_amount,retention1,retention2):
                cons = 0.0
                if claim_for == 'down_payment' or claim_for == 'retention':
                    cons += 0.0
                elif claim_for == 'progress':
                    cons += (gross_amount * ((retention1/100) + (retention2/100))) * -1
                return cons

            def _compute_amount_deduction(claim_for,gross_amount,dp_deduction,retention_deduction):
                amt = 0.0
                if claim_for == 'down_payment' or claim_for == 'retention':
                    amt += 0.0
                elif claim_for == 'progress':
                    amt += sum([gross_amount,dp_deduction,retention_deduction])
                return amt

            def _compute_amount_tax(tax_id,amount_untaxed):
                vals = 0.0
                for tax in tax_id:
                    vals += (tax.tax_amount/100) * amount_untaxed
                return vals

            
            gross_amount = _compute_gross_amount(claim_for,contract_amount,dp_amount,progressline,retention,retention1,retention2)
            dp_deduction = _compute_dp_deduction(claim_for,progressline,dp_amount)
            retention_deduction =  _compute_retention_deduction(claim_for,gross_amount,retention1,retention2)
            amount_deduction = _compute_amount_deduction(claim_for,gross_amount,dp_deduction,retention_deduction)
            tax_amount = _compute_amount_tax(tax_id,amount_untaxed)

            rec.gross_amount = gross_amount
            rec.dp_deduction = dp_deduction
            rec.retention_deduction = retention_deduction
            rec.amount_deduction = amount_deduction
            rec.tax_amount = tax_amount
            rec.amount_invoice = amount_untaxed + tax_amount