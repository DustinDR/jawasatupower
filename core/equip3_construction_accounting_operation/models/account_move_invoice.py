from odoo import models, fields, api


class AccountMove(models.Model):
    _inherit = 'account.move'

    # project_invoice = fields.Selection([
    #                   ('regular', 'Regular'),
    #                   ('progressive', 'Progressive')
    #                   ],string="Project Invoice", default=False)
    # regular_method = fields.Selection([
    #                  ('regular', 'Regular Invoice'),
    #                  ('down_payment', 'Down Payment'),
    #                  ('retention1', 'Retention 1'),
    #                  ('retention2', 'Retention 2')
    #                  ],string="Regular Method", default=False)
    progressive_method = fields.Selection([
                         ('down_payment', 'Down Payment'),
                         ('progress', 'Progress'),
                         ('retention1', 'Retention 1'),
                         ('retention2', 'Retention 2')
                         ], string='Progressive Method')
    project_id = fields.Many2one('project.project', string="Project")
    # sale_order_ref = fields.Many2one('sale.order.const', string="Sale Order Reference")
    
    # adjustment_scope =  fields.Float(string="Adjustment",readonly=True)
    # discount_scope =  fields.Float(string="Discount",readonly=True)
    # adjustment_section =  fields.Float(string="Adjustment",readonly=True)
    # discount_section =  fields.Float(string="Discount",readonly=True)

    # adjustment_global =  fields.Float(string="Adjustment",readonly=True)
    # discount_global =  fields.Float(string="Discount",readonly=True)
    # adjustment_subline =  fields.Float(string="Adjustment",readonly=True)
    # discount_subline =  fields.Float(string="Discount",readonly=True)
    # amount_tax = fields.Float(string="Taxes",readonly=True)
    # amount_total_const = fields.Float(string="Total",readonly=True)
    # adjustment_type_ref = fields.Selection([
    #                     ('scope', 'Project Scope'),
    #                     ('section', 'Section'),
    #                     ('line', 'Order Line'), 
    #                     ('global', 'Global')
    #                     ],string='Adjustment Applies to',default='global')
    # discount_type_ref = fields.Selection([
    #                     ('scope', 'Project Scope'),
    #                     ('section', 'Section'),
    #                     ('line', 'Order Line'), 
    #                     ('global', 'Global')
    #                     ],string='Discount Applies to', default='global')
    
    progress = fields.Float('Progress', readonly='1', digits=(2,2))
    claim_description = fields.Char('Claim ID',default=False)
    claim_id = fields.Many2one('progressive.claim',
                               'Progressive Claim',
                               ondelete='restrict',
                               store=True)
    


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    # project_invoice = fields.Selection(related='move_id.project_invoice', string="Project Invoice")
    # adjustment_line = fields.Float(string="Adjustment")
    # discount_line = fields.Float(string="Discount")
    # variable = fields.Many2one('variable.template', string="Subcon", tracking=True)
    # discount_type_ref = fields.Selection(related='move_id.discount_type_ref', string="Discount Applies to")
    # adjustment_type_ref = fields.Selection(related='move_id.adjustment_type_ref', string="Adjustment Applies to")
    # amount_line = fields.Float(string="Amount Line")
    progressive_method = fields.Selection(related='move_id.progressive_method', string="Progressive Method")
    progress = fields.Float('Progress (%)', digits=(2,2))
    dp_deduction = fields.Monetary('DP Deduction',currency_field='company_currency_id')
    retention_deduction = fields.Monetary('Retention Deduction',currency_field='company_currency_id')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate','Section')
    group_of_product = fields.Many2one('group.of.product','Group of Product')
    
            