from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import date, datetime


class ProgressiveClaim(models.Model):
    _name = 'progressive.claim'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _order = 'create_date desc, id desc'
    _check_company_auto = True
    _description = "Progressive Claim"

    @api.onchange('project_id')
    def _onchange_project_id(self):
        if self.project_id:
            res = self.project_id
            self.partner_id = res.partner_id.id
            self.partner_invoice_id = res.partner_id.id
            self.project_director = res.project_director.id
            self.analytic_idz = res.analytic_idz
            self.start_date = res.start_date
            self.end_date = res.end_date
            self.contract_amount = res.contract_amount
            self.dp_amount = res.down_payment
            self.retention1 = res.retention1
            self.retention2 = res.retention2
            self.tax_id = res.tax_id

                
    def create_request(self):
        if self.claim_request == True:
            context = {'default_request_for': 'progress',
                       'default_project_id': self.project_id and self.project_id.id or False,
                       'default_partner_id': self.partner_id and self.partner_id.id or False,
                       'default_partner_request_id': self.partner_id and self.partner_id.id or False,
                       'default_branch_id': self.branch_id and self.branch_id.id or False,
                       'default_project_director': self.project_director and self.project_director.id or False,
                       'default_contract_amount': self.contract_amount,
                       'default_progressive_claim_id': self.id
                       }
        else:
            context = {'default_request_for': False,
                       'default_project_id': self.project_id and self.project_id.id or False,
                       'default_partner_id': self.partner_id and self.partner_id.id or False,
                       'default_partner_request_id': self.partner_id and self.partner_id.id or False,
                       'default_branch_id': self.branch_id and self.branch_id.id or False,
                       'default_project_director': self.project_director and self.project_director.id or False,
                       'default_contract_amount': self.contract_amount,
                       'default_progressive_claim_id': self.id
                        }
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'claim.request',
            'name': "Create Claim Request",
            "context": context,
            'target': 'new',
            'view_type': 'form',
            'view_mode': 'form',
            }

    def create_invoice_dp(self):
        if not self.claim_ids:
            context = {'default_invoice_for': 'down_payment',
                       'default_invoice_progress': self.dp_amount,
                       'default_approved_progress': self.approved_progress,
                       'default_contract_amount': self.contract_amount,
                       'default_dp_amount': self.dp_amount,
                       'default_retention1': self.retention1,
                       'default_retention2': self.retention2,
                       'default_retention1_amount': self.retention1_amount,
                       'default_retention2_amount': self.retention2_amount,
                       'default_tax_id': [(6, 0, [v.id for v in self.tax_id])],
                       'default_progressive_claim_id': self.id,
                       }
        else:
            context = {'default_invoice_for': False,
                       'default_approved_progress': self.approved_progress,
                       'default_contract_amount': self.contract_amount,
                       'default_dp_amount': self.dp_amount,
                       'default_retention1': self.retention1,
                       'default_retention2': self.retention2,
                       'default_retention1_amount': self.retention1_amount,
                       'default_retention2_amount': self.retention2_amount,
                       'default_tax_id': [(6, 0, [v.id for v in self.tax_id])],
                       'default_progressive_claim_id': self.id
                       }
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'progressive.invoice.wiz',
            'name': "Create Progressive Invoice",
            "context": context,
            'target': 'new',
            'view_type': 'form',
            'view_mode': 'form',
            }

    
    def create_invoice_progress(self):
        if self.invoiceable_progress == True:
            context = {'default_invoice_for': 'progress',
                       'default_approved_progress': self.approved_progress,
                       'default_contract_amount': self.contract_amount,
                       'default_last_progress': self.invoiced_progress,
                       'default_dp_amount': self.dp_amount,
                       'default_retention1': self.retention1,
                       'default_retention2': self.retention2,
                       'default_retention1_amount': self.retention1_amount,
                       'default_retention2_amount': self.retention2_amount,
                       'default_tax_id': [(6, 0, [v.id for v in self.tax_id])],
                       'default_progressive_claim_id': self.id
                       }
        else:
            context = {'default_invoice_for': False,
                       'default_approved_progress': self.approved_progress,
                       'default_contract_amount': self.contract_amount,
                       'default_dp_amount': self.dp_amount,
                       'default_retention1': self.retention1,
                       'default_retention2': self.retention2,
                       'default_retention1_amount': self.retention1_amount,
                       'default_retention2_amount': self.retention2_amount,
                       'default_tax_id': [(6, 0, [v.id for v in self.tax_id])],
                       'default_progressive_claim_id': self.id
                       }
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'progressive.invoice.wiz',
            'name': "Create Progressive Invoice",
            "context": context,
            'target': 'new',
            'view_type': 'form',
            'view_mode': 'form',
            }

    def create_invoice_retention1(self):
        if self.progress_full_invoiced:
            context = {'default_invoice_for': 'retention1',
                       'default_invoice_progress': self.retention1_amount,
                       'default_approved_progress': self.approved_progress,
                       'default_contract_amount': self.contract_amount,
                       'default_dp_amount': self.dp_amount,
                       'default_retention1': self.retention1,
                       'default_retention2': self.retention2,
                       'default_retention1_amount': self.retention1_amount,
                       'default_retention2_amount': self.retention2_amount,
                       'default_tax_id': [(6, 0, [v.id for v in self.tax_id])],
                       'default_progressive_claim_id': self.id
                       }
        else:
            context = {'default_invoice_for': False,
                       'default_approved_progress': self.approved_progress,
                       'default_contract_amount': self.contract_amount,
                       'default_dp_amount': self.dp_amount,
                       'default_retention1': self.retention1,
                       'default_retention2': self.retention2,
                       'default_retention1_amount': self.retention1_amount,
                       'default_retention2_amount': self.retention2_amount,
                       'default_tax_id': [(6, 0, [v.id for v in self.tax_id])],
                       'default_progressive_claim_id': self.id
                       }
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'progressive.invoice.wiz',
            'name': "Create Progressive Invoice",
            "context": context,
            'target': 'new',
            'view_type': 'form',
            'view_mode': 'form',
            }

    def create_invoice_retention2(self):
        if self.progress_full_invoiced:
            context = {'default_invoice_for': 'retention2',
                       'default_invoice_progress': self.retention2_amount,
                       'default_approved_progress': self.approved_progress,
                       'default_contract_amount': self.contract_amount,
                       'default_dp_amount': self.dp_amount,
                       'default_retention1': self.retention1,
                       'default_retention2': self.retention2,
                       'default_retention1_amount': self.retention1_amount,
                       'default_retention2_amount': self.retention2_amount,
                       'default_tax_id': [(6, 0, [v.id for v in self.tax_id])],
                       'default_progressive_claim_id': self.id
                       }
        else:
            context = {'default_invoice_for': False,
                       'default_approved_progress': self.approved_progress,
                       'default_contract_amount': self.contract_amount,
                       'default_dp_amount': self.dp_amount,
                       'default_retention1': self.retention1,
                       'default_retention2': self.retention2,
                       'default_retention1_amount': self.retention1_amount,
                       'default_retention2_amount': self.retention2_amount,
                       'default_tax_id': [(6, 0, [v.id for v in self.tax_id])],
                       'default_progressive_claim_id': self.id
                       }
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'progressive.invoice.wiz',
            'name': "Create Progressive Invoice",
            "context": context,
            'target': 'new',
            'view_type': 'form',
            'view_mode': 'form',
            }

    def claim_confirm(self):
        if self.tax_id:
            for tax in self.tax_id:
                taxs = []
                if tax.amount > 0:
                    taxs.append(tax.id)
                    self.vat_tax = [(4,tax.id)]
                taxs = []
                if tax.amount < 0:
                    taxs.append(tax.id)
                    self.income_tax = [(4, tax.id)]
        res = self.write({'state':'in_progress'})
        return res

    def reset_draft(self):
        res = self.write({'state':'draft'})
        return res
    
    def action_cancel(self):
        res = self.write({'state':'cancel'})
        return res

    def action_done(self):
        res = self.write({'state':'done'})
        return res

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.company_currency_id = res_user_id.company_id.currency_id
            line.company_id = res_user_id.company_id

    @api.depends('retention1', 'contract_amount')
    def _compute_total_retention1(self):
        for res in self:
            res.retention1_amount = res.contract_amount * (res.retention1 / 100)

    @api.depends('retention2', 'contract_amount')
    def _compute_total_retention2(self):
        for res in self:
            res.retention2_amount = res.contract_amount * (res.retention2 / 100)

    @api.constrains('start_date', 'end_date')
    def constrains_date(self):
        for rec in self:
            if rec.start_date != False and rec.end_date != False:
                if rec.start_date > rec.end_date:
                    raise UserError(_('End date should be after start date.'))

    def _compute_count_invoices(self):
        for rec in self:
            def _get_history_lines_vals(inv_ids):
                vals = []
                if len(inv_ids) > 0:
                    for inv in inv_ids:
                        val = {
                            'invoice_id':inv.id
                        }
                        vals.append((0,0,val))
                if len(vals) > 0:
                    rec.claim_ids = False
                return vals 
            
            
            @api.depends('claim_ids.progress')
            def _compute_Invoiced_progress(self):
                total = 0
                for res in self:
                    total = sum(res.claim_ids.mapped('progress'))
                    res.invoiced_progress = total
                return total

            total = 0
            history_lines = False
            inv_ids = self.env['account.move'].search([('claim_id','=',rec.id)],order='create_date')
            get_history_lines = _get_history_lines_vals(inv_ids)
            
            if len(inv_ids) > 0:
                total += len(inv_ids)
            if len(get_history_lines) > 0:
                history_lines = get_history_lines
            
            rec.count_invoice = total
            rec.claim_ids = history_lines
            rec.invoiced_progress = _compute_Invoiced_progress(self)

    def action_view_invoice(self):
        inv_ids = self.env['account.move'].search([('claim_id','=',self.id)],order='id desc').ids
        domain_ids = [('id','in',inv_ids)]
        tree_view = self.env.ref('equip3_construction_accounting_operation.account_move_progressive_view_tree').id
        if len(inv_ids) < 1:
            domain_ids = [('id','=',False)]
        action = {
            "name": _("Progressive Invoices"),
            "type": "ir.actions.act_window",
            "res_model": "account.move",
            "views": [(tree_view, "tree"),(False, "form")],
            "domain": domain_ids,
        }
        return action
    
    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('progressive.project.claim.sequence')
        return super(ProgressiveClaim, self).create(vals)


    name = fields.Char(string='Number', required=True, copy=False, readonly=True,
                        index=True, default=lambda self: _('New'), ondelete='cascade')
 
    start_date = fields.Date(string="Start Date", tracking=True, readonly=True,
                    states={'draft': [('readonly', False)]})
    end_date = fields.Date(string="End Date", tracking=True, readonly=True,
                    states={'draft': [('readonly', False)]})
    partner_id = fields.Many2one(
        'res.partner', string='Customer',
        change_default=True, index=True, tracking=1,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")
    partner_invoice_id = fields.Many2one(
        'res.partner', string='Invoice Address',
        change_default=True, index=True, tracking=1,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")
    project_director = fields.Many2one('res.users', string='Project Director')
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True, readonly=True,
                                 default=lambda self: self.env.company)
    branch_id = fields.Many2one('res.branch', string="Branch", readonly=True, default=lambda self: self.env.user.branch_id.id,
                states={'draft': [('readonly', False)]})
    company_currency_id = fields.Many2one('res.currency', string='Currency', related='company_id.currency_id')
    project_id = fields.Many2one('project.project', string="Project")
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    contract_amount = fields.Float(string='Contract Amount')
    dp_amount = fields.Float(string="Down Payment")
    retention1 = fields.Float(string="Retention 1")
    retention2 = fields.Float(string="Retention 2")
    retention1_amount = fields.Float(string="Retention 1 Amount", compute="_compute_total_retention1")
    retention2_amount = fields.Float(string="Retention 2 Amount", compute="_compute_total_retention2")
    tax_id = fields.Many2many('account.tax', 'taxes', string="Taxes")
    vat_tax = fields.Many2many('account.tax', 'vat_tax', string="VAT Tax")
    income_tax = fields.Many2many('account.tax', 'income_tax', string="Income Tax")
    analytic_idz = fields.Many2many('account.analytic.tag', string='Analytic Group', domain="[('company_id', '=', company_id)]")
    progress_full_approved = fields.Boolean(string="Progress Full Approved", compute="_onchange_progress_full_approved")
    progress_full_invoiced = fields.Boolean(string="Progress Full Invoiced", compute="_onchange_progress_full_invoiced")
    count_invoice = fields.Integer(compute="_compute_count_invoices")
    progressive_bill = fields.Boolean('Progressive Bill', default=False)
    so_split = fields.Boolean('SO Split', default=False)
    sale_order_ref = fields.Many2one('sale.order.const', string="Sale Order Reference")

    state = fields.Selection([
            ('draft','Draft'),
            ('in_progress','In Progress'),
            ('done','Done'),
            ('cancel','Cancel'),
            ], string='Status', readonly=True, copy=False, index=True, tracking=1, default='draft')

    actual_progress = fields.Float(string="Actual Progress",compute="_compute_actual_progress", digits=(2,2))
    approved_progress = fields.Float(string = 'Approved Progress', compute="_compute_approved_progress_2", digits=(2,2))
    requested_progress = fields.Float(string = 'Requested Progress', compute="_compute_requested_progress", digits=(2,2))
    invoiced_progress = fields.Float(string = 'Invoiced Progress', compute='_compute_count_invoices', digits=(2,2)) 
    temp_rent = fields.Float(string = 'Retention', compute='_compute_temp_rent') 

    dp_claim = fields.Boolean(string="Down Payment", compute="_onchange_dp_claim")
    request = fields.Boolean(string="Request", compute="_onchange_request")
    request_2 = fields.Boolean(string="Request 2", compute="_onchange_request_2")
    claim_request = fields.Boolean(string="Claim Request", compute="_onchange_claim_request")
    claim_request_filled = fields.Boolean(string="Claim Filled", compute="_onchange_claim_request_filled")
    invoiceable = fields.Boolean(string="Invoiceable", compute="_onchange_invoiceable")
    invoiceable_progress = fields.Boolean(string="Invoiceable Progress", compute="_onchange_invoiceable_progress")
    invoiceable_rent1 = fields.Boolean(string="Invoiceable Retention 1", compute="_onchange_invoiceable_rent1")
    rent1_invoice = fields.Boolean(string="Retention 1 Invoiced", compute="_onchange_rent_invoice")
    invoiceable_rent2 = fields.Boolean(string="Invoiceable Retention 2", compute="_onchange_invoiceable_rent2")
    rent2_avail = fields.Boolean(string="Retention 2 Available", compute="_onchange_rent2_avail")
    rent2_invoice = fields.Boolean(string="Retention 2 Invoiced", compute="_onchange_rent_invoice")

    # tab Claim History
    claim_ids = fields.One2many('project.claim', 'claim_id', string="Claim History")

    # tab Claim Request 
    claim_request_ids = fields.One2many('claim.request.line', 'claim_id', string="Claim Request")

    total_invoice = fields.Float(string="Total Amount Invoiced", readonly=True, store=True, compute='_compute_amount', currency_field='currency_id')
    total_claim = fields.Float(readonly=True, string="Total Amount Claimed", compute='_compute_amount', store=True, currency_field='currency_id')
    remaining_amount = fields.Float(readonly=True, string="Remaining Amount", compute='_compute_amount', store=True, currency_field='currency_id')
    total_work_order = fields.Integer(string="Work Order",compute='_comute_work_order')

    @api.depends('claim_request_ids')
    def _onchange_claim_request_filled(self):
        for res in self:
            if not res.claim_request_ids:
                res.claim_request_filled = False
            else:
                res.claim_request_filled = True

    @api.depends('claim_ids')
    def _onchange_dp_claim(self):
        for res in self:
            if not res.claim_ids:
                res.dp_claim = False
            else:
                res.dp_claim = True

    @api.depends('actual_progress', 'approved_progress')
    def _onchange_request(self):
        for res in self:
            if res.actual_progress > res.approved_progress:
                res.request = True
            else:
                res.request = False

    @api.depends('actual_progress', 'requested_progress')
    def _onchange_request_2(self):
        for res in self:
            if res.actual_progress > res.requested_progress:
                res.request_2 = True
            else:
                res.request_2 = False

    @api.depends('dp_claim', 'request', 'request_2')
    def _onchange_claim_request(self):
        for res in self:
            if (res.dp_claim == True and res.request == True and res.request_2 == True):
                res.claim_request = True
            else:
                res.claim_request = False

    @api.depends('approved_progress', 'invoiced_progress')
    def _onchange_invoiceable(self):
        for res in self:
            if res.approved_progress > res.invoiced_progress:
                res.invoiceable = True
            elif res.approved_progress == res.invoiced_progress:
                res.invoiceable = False
            else:
                res.invoiceable = False

    @api.depends('dp_claim', 'invoiceable')
    def _onchange_invoiceable_progress(self):
        for res in self:
            if (res.dp_claim == True and res.invoiceable == True):
                res.invoiceable_progress = True
            else:
                res.invoiceable_progress = False

    @api.depends('temp_rent')
    def _onchange_rent_invoice(self):
        for res in self:
            if res.temp_rent == 1:
                res.rent1_invoice = True
                res.rent2_invoice = False
            elif res.temp_rent == 2:
                res.rent1_invoice = True
                res.rent2_invoice = True
            elif res.temp_rent == 0:
                res.rent1_invoice = False
                res.rent2_invoice = False
            else:
                res.rent1_invoice = False
                res.rent2_invoice = False
            
    @api.depends('progress_full_invoiced', 'rent1_invoice')
    def _onchange_invoiceable_rent1(self):
        for res in self:
            if (res.progress_full_invoiced == True and res.rent1_invoice == False):
                res.invoiceable_rent1 = True
            else:
                res.invoiceable_rent1 = False

    @api.depends('retention2')
    def _onchange_rent2_avail(self):
        for res in self:
            if res.retention2 > 0:
                res.rent2_avail = True
            elif res.retention2 == 0:
                res.rent2_avail = False
            else:
                res.rent2_avail = False

    @api.depends('progress_full_invoiced', 'rent1_invoice', 'rent2_avail', 'rent2_invoice')
    def _onchange_invoiceable_rent2(self):
        for res in self:
            if (res.progress_full_invoiced == True and res.rent1_invoice == True and res.rent2_avail == True and res.rent2_invoice == False):
                res.invoiceable_rent2 = True
            else:
                res.invoiceable_rent2 = False

    def _comute_work_order(self):
        for work in self:
            work_count = self.env['project.task'].search_count([('project_id', '=', self.project_id.id), ('state', 'in', ('inprogress', 'pending', 'complete'))])
            work.total_work_order = work_count

    def action_work_order(self):
        tree_view = self.env.ref('equip3_construction_masterdata.view_task_tree_project').id
        return {
            'name': ("Work Order"),
            'views': [(tree_view, "tree"),(False, "form")],
            'res_model': 'project.task',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('project_id', '=', self.project_id.id), ('state', 'in', ('inprogress', 'pending', 'complete'))],
        }
    
    @api.depends('claim_ids')
    def _compute_amount(self):
        for rec in self:
            total_invoice = 0.0
            total_claim = 0.0
            remaining_amount = 0.0
            claim_ids = rec.claim_ids
            if len(claim_ids) > 0:
                total_claim = sum([i.amount_invoice for i in claim_ids if i.payment_status in ['paid','partial']])
                total_invoice = sum([i.amount_invoice for i in claim_ids])
                remaining_amount = total_invoice - total_claim
            rec.total_invoice = total_invoice
            rec.total_claim = total_claim
            rec.remaining_amount = remaining_amount

    @api.depends('approved_progress')
    def _onchange_progress_full_approved(self):
        for res in self:
            if res.approved_progress == 100:
                res.progress_full_approved = True
            else:
                res.progress_full_approved = False

    @api.depends('invoiced_progress')
    def _onchange_progress_full_invoiced(self):
        for res in self:
            if res.invoiced_progress == 100:
                res.progress_full_invoiced = True
            else:
                res.progress_full_invoiced = False

    @api.depends('claim_request_ids.approved_progress')
    def _compute_approved_progress_2(self):
        total = 0
        for res in self:
            total = sum(res.claim_request_ids.mapped('approved_progress'))
            res.approved_progress = total
        return total

    @api.depends('claim_request_ids.requested_progress_2')
    def _compute_requested_progress(self):
        total_1 = 0
        for res in self:
            total_1 = sum(res.claim_request_ids.mapped('requested_progress_2'))
            res.requested_progress = total_1
        return total_1

    @api.depends('claim_ids.temp_rent')
    def _compute_temp_rent(self):
        total = 0
        for res in self:
            total = sum(res.claim_ids.mapped('temp_rent'))
            res.temp_rent = total
        return total

    def _compute_actual_progress(self):
        total = 0
        work_order = self.env['project.task'].search(
            [('project_id', '=', self.project_id.id), ('state', 'in', ('inprogress', 'pending', 'complete'))])
        for work in self:
            total = sum(work_order.mapped('project_progress'))
            work.actual_progress = total
        return total

class ProjectClaim(models.Model):
    _name = 'project.claim'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _rec_name = 'claim_name'
    _order = 'sequence'
    _description = "Project Claim"

    @api.depends('claim_id.claim_ids', 'claim_id.claim_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.claim_id.claim_ids:
                no += 1
                l.sr_no = no

    claim_id = fields.Many2one('progressive.claim', string="Claim", ondelete='cascade')
    invoice_id = fields.Many2one('account.move','Invoice', ondelete='cascade')
    company_id = fields.Many2one(related='claim_id.company_id', string='Company', readonly=True)
    currency_id = fields.Many2one(related='invoice_id.currency_id', string="Currency", readonly=True)
    claim_name = fields.Char(string="Claim ID", related='invoice_id.claim_description')
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    date =  fields.Date(string="Date", related='invoice_id.invoice_date')
    progressline = fields.Float(string = 'Progress (%)', related='invoice_id.progress', digits=(2,2))
    gross_amount = fields.Monetary(string="Gross Amount", currency_field='currency_id', compute='_compute_progressive')
    dp_deduction = fields.Monetary(string="DP Deduction", currency_field='currency_id', compute='_compute_progressive')
    retention_deduction = fields.Monetary(string="Retention Deduction", currency_field='currency_id', compute='_compute_progressive')
    amount_deduction = fields.Monetary(string="Amount After Deduction", currency_field='currency_id', compute='_compute_progressive')
    paid_invoice = fields.Monetary(string="Paid Invoice", currency_field='currency_id', compute='_compute_invoice')
    amount_untaxed = fields.Float(string = 'Amount Untaxed',related='invoice_id.amount_untaxed', currency_field='currency_id')
    tax_amount = fields.Monetary(string="Tax Amount", currency_field='currency_id', compute='_compute_progressive')
    amount_invoice = fields.Monetary(string="Amount Invoice", currency_field='currency_id', compute='_compute_progressive')
    invoice_status = fields.Selection(related='invoice_id.state',string='Invoice Status')
    payment_status = fields.Selection(related='invoice_id.payment_state',string='Invoice Status')
    claim_for = fields.Selection(related='invoice_id.progressive_method', string='Claim Type')
    progress = fields.Float(string = 'Progress (%)',compute='_compute_invoice', digits=(2,2))
    temp_rent = fields.Float(string = 'Retention', compute="_compute_retention_1")

    @api.depends('claim_for')
    def _compute_retention_1(self):
        for res in self:
            res.temp_rent = 0
            if res.claim_for == 'retention1' or res.claim_for == 'retention2':
                res.temp_rent = 1
            return res.temp_rent

    def _compute_invoice(self):
        for rec in self:
            def _get_amount_deduction_retention(inv_id):
                vals = {
                    'dp':0.0,
                    'retention':0.0,
                    'progress':0.0,
                }
                invoice_line_ids = inv_id.invoice_line_ids
                if len(invoice_line_ids) > 0:
                    for line in invoice_line_ids:
                        vals['dp'] += line.dp_deduction
                        vals['retention'] += line.retention_deduction
                        vals['progress'] += line.progress
                return vals
            
            def _get_paid_inv(claim_id,progressive_method):
                value = 0.0
                domain = [('claim_id','=',claim_id.id),('progressive_method','=','progress')]
                inv_ids = self.env['account.move'].search(domain,order='create_date')
                if progressive_method != 'progress':
                    value = 0.0
                elif len(inv_ids) > 0:
                    vals = []
                    for inv in inv_ids:
                        if inv.id == rec.invoice_id.id:
                            break
                        vals.append(inv.amount_untaxed)
                    if len(vals) > 0:
                        value += sum(vals)
                return value
            
            inv_id = rec.invoice_id
            claim_id = rec.claim_id
            total_dp_deduction = 0.0
            total_retention = 0.0
            total_progress = 0.0
            progressive_method = rec.claim_for
            total_paid_invoice = _get_paid_inv(claim_id,progressive_method)
            get_amount = _get_amount_deduction_retention(inv_id)     

            if get_amount['dp'] > 0:
                total_dp_deduction += get_amount['dp']
            if get_amount['retention'] > 0:
                total_retention += get_amount['retention']
            if get_amount['progress'] > 0:
                total_progress += get_amount['progress']

            rec.paid_invoice = total_paid_invoice
            rec.progress = total_progress
            rec.dp_deduction = total_dp_deduction
            rec.retention_deduction = total_retention
    

    def _compute_progressive(self):
        for rec in self:
            claim_id = rec.claim_id
            claim_for = rec.claim_for
            progressline = rec.progressline
            contract_amount = claim_id.contract_amount
            dp_amount = claim_id.dp_amount
            retention1 = claim_id.retention1
            retention2 = claim_id.retention2
            tax_id = claim_id.tax_id
            amount_untaxed = rec.amount_untaxed

            

            def _compute_gross_amount(claim_for,contract_amount,dp_amount,progressline,retention1,retention2):
                value = 0.0
                if claim_for == 'down_payment':
                    value += dp_amount
                elif claim_for == 'progress':
                    value += contract_amount * (progressline/100)
                elif claim_for == 'retention1':
                    value += contract_amount * (retention1/100)
                elif claim_for == 'retention2':
                    value += contract_amount * (retention2/100)
                return value

            def _compute_dp_deduction(claim_for,progressline,dp_amount):
                vals = 0.0
                if claim_for != 'progress':
                    vals += 0.0
                elif claim_for == 'progress':
                    vals += (dp_amount * (progressline/100))
                return vals

            def _compute_retention_deduction(claim_for,gross_amount,retention1,retention2):
                cons = 0.0
                if claim_for != 'progress':
                    cons += 0.0
                elif claim_for == 'progress':
                    cons += (gross_amount * ((retention1/100) + (retention2/100)))
                return cons

            def _compute_amount_deduction(claim_for,gross_amount,dp_deduction,retention_deduction):
                amt = 0.0
                if claim_for != 'progress':
                    amt += 0.0
                elif claim_for == 'progress':
                    amt += gross_amount - (dp_deduction + retention_deduction)
                return amt

            def _compute_amount_tax(tax_id,amount_untaxed):
                vals = 0.0
                for tax in tax_id:
                    vals += (tax.amount/100) * amount_untaxed
                return vals

            
            gross_amount = _compute_gross_amount(claim_for,contract_amount,dp_amount,progressline,retention1,retention2)
            dp_deduction = _compute_dp_deduction(claim_for,progressline,dp_amount)
            retention_deduction =  _compute_retention_deduction(claim_for,gross_amount,retention1,retention2)
            amount_deduction = _compute_amount_deduction(claim_for,gross_amount,dp_deduction,retention_deduction)
            tax_amount = _compute_amount_tax(tax_id,amount_untaxed)

            rec.gross_amount = gross_amount
            rec.dp_deduction = dp_deduction
            rec.retention_deduction = retention_deduction
            rec.amount_deduction = amount_deduction
            rec.tax_amount = tax_amount
            rec.amount_invoice = amount_untaxed + tax_amount
            

class ClaimRequest(models.Model):
    _name = "claim.request.line"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _description = "Claim Request Line"
    _rec_name = 'request_id'
    _order = 'sequence'
    _check_company_auto = True

    def confirm_request(self):
        self.write({'request_status': 'approved',
                    'request_status_2': 'approved', 
                    'approved_date' : datetime.now(),
                    'approved_progress' : self.requested_progress})
        claim_id = self.claim_id.id
        action = self.env.ref('equip3_construction_accounting_operation.progressive_claim_action_form').read()[0]
        action['res_id'] = claim_id
        return action

    # def reject_request(self):
    #     self.write({'request_status': 'rejected',
    #                 'request_status_2': 'rejected'})
    #     claim_id = self.claim_id.id
    #     action = self.env.ref('equip3_construction_accounting_operation.progressive_claim_action_form').read()[0]
    #     action['res_id'] = claim_id
    #     return action

    @api.depends('claim_id.claim_request_ids', 'claim_id.claim_request_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.claim_id.claim_request_ids:
                no += 1
                l.sr_no = no
    

    claim_id = fields.Many2one('progressive.claim', string='Claim ID', ondelete='cascade')
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer(string="No", compute="_sequence_ref")
    request_id = fields.Many2one('claim.request', string="Request ID")
    company_id = fields.Many2one(related='claim_id.company_id', string='Company')
    branch_id = fields.Many2one(related='claim_id.branch_id', string="Branch")
    creation_date = fields.Datetime(string='Creation Date')
    approved_date = fields.Datetime(string='Approved Date')
    created_by = fields.Many2one('res.users', string='Created By')
    project_id = fields.Many2one(related='claim_id.project_id', string='Project')
    partner_id = fields.Many2one(related='claim_id.partner_id', string='Customer')
    partner_request_id = fields.Many2one('res.partner', string='Request Address')
    project_director = fields.Many2one(related='claim_id.project_director', string='Project Director')
    requested_progress = fields.Float(string='Requested Progress (%)', digits=(2,2))
    requested_progress_2 = fields.Float(string='Requested Progress (%)', digits=(2,2))
    approved_progress = fields.Float(string='Approved Progress (%)', digits=(2,2))
    requested_amount = fields.Float(string='Requested Amount')
    contract_amount = fields.Float(string='Contract Amount')
    request_ids = fields.One2many('request.from.line', 'request_line_id', string="Request ID")
    request_status = fields.Selection([
                        ('draft', 'Draft'),
                        ('waiting', 'Waiting For Approval'),
                        ('approved', 'Approved'),
                        ('rejected', 'Rejected')
                ], string="Request Status", tracking=True, default='draft')
    request_status_2 = fields.Selection(related="request_status", default='draft')
    request_for = fields.Selection([
        ('down_payment', 'Down Payment'),
        ('progress', 'Progress'),
        ('retention1', 'Retention 1'),
        ('retention2', 'Retention 2')
        ], string='Requested Type')
    show_approval_buttons = fields.Binary(default=False, compute='_show_approval_buttons')
    rejected_reason = fields.Text(string="Reason")
    rejected_date = fields.Datetime(string="Rejected Date")

    def _show_approval_buttons(self):
        for claim_request in self:
            if claim_request.project_director and self.env.user.id == claim_request.project_director.id:
                claim_request.show_approval_buttons = True
            else:
                claim_request.show_approval_buttons = False

    def line_delete(self):
        claim_id = self.claim_id.id
        self.unlink()
        action = self.env.ref('equip3_construction_accounting_operation.progressive_claim_action_form').read()[0]
        action['res_id'] = claim_id
        return action

    def action_reject(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Reject Reason',
            'res_model': 'claim.request.reject',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            "context": {'default_claim_id': self.claim_id and self.claim_id.id or False}
        }


class RequestLinesIds(models.Model):
    _name = 'request.from.line'
    _description = 'Request Lines'

    request_line_id = fields.Many2one('claim.request.line', string="Request ID")
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    work_order = fields.Many2one('project.task', string="Work Order",
                 domain="[('project_id', '=', parent.project_id)]")
    stage = fields.Many2one('project.stage' ,string="Stage")
    assigned_to = fields.Many2one('res.users', string="PIC")
    completion_date = fields.Datetime(string="Completion Date")
    stage_weightage = fields.Float(string="Stage Weightage (%)")
    work_progress = fields.Float(string="Work Order Progress (%)")
    work_weightage = fields.Float(string="Work Order Weightage (%)")
    progress = fields.Float(string="Project Progress (%)", digits=(2,2))

    @api.depends('request_line_id.request_ids', 'request_line_id.request_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.request_line_id.request_ids:
                no += 1
                l.sr_no = no

























