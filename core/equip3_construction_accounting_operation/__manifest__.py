# -*- coding: utf-8 -*-
{
    'name': "Equip3 Construction Accounting Operation",

    'summary': """
        This module to manage all operation of accounting in construction""",

    'description': """
        This module manages these features :
        - Invoice 
        - Vendor Bill
        - Risalah AR
        - Risalah AP
    """,

    'author': "HashMicro",
    'website': "http://www.hashmicro.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Construction',
    'version': '1.1.7',

    # any module necessary for this one to work correctly
    'depends': ['base','mail', 'account','sale','bi_warranty_registration','equip3_sale_operation', 
                'equip3_construction_sales_operation', 'equip3_accounting_operation', 'account', 
                'payment', 'project', 'equip3_construction_operation'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/progresive_sequence.xml',
        'views/progressive_claim_view.xml',
        'wizard/claim_request_rejected_reason_view.xml',
        'views/construction_accounting_css_assets.xml',
        'views/account_move_invoice_view.xml',
        'wizard/create_claim_request_view.xml',
        'wizard/progressive_invoice_view.xml',
        'views/progressive_bill_view.xml',
    ],

}
