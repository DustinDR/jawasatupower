# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import create_claim_request
from . import progressive_invoice
from . import claim_request_rejected_reason
