
from odoo import api , models, fields 
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

class ClaimRequestReject(models.TransientModel):
    _name = 'claim.request.reject'

    reason = fields.Text(string="Reason")
    claim_id = fields.Many2one('progressive.claim', string='Claim ID',)

    def action_confirm(self):
        job_id = self.env['claim.request.line'].browse([self._context.get('active_id')])
        job_id.write({'rejected_reason': self.reason, 
                      'rejected_date': datetime.now(), 
                      'request_status': 'rejected',
                      'request_status_2': 'rejected',
                      'requested_progress_2': 0
                      })
        claim_id = self.claim_id.id
        action = self.env.ref('equip3_construction_accounting_operation.progressive_claim_action_form').read()[0]
        action['res_id'] = claim_id
        return action
            
