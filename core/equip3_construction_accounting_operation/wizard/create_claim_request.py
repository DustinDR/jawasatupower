from odoo import _, api, fields, models
from datetime import date, datetime
from odoo.exceptions import ValidationError

class CreateClaimRequest(models.Model):
    _name = 'claim.request'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _description = 'Claim Request'
    _rec_name = 'name'
    _check_company_auto = True

    def send_request(self):
        if self.progressive_claim_id:
            progressive_claim_id = self.progressive_claim_id
        else:
            progressive_claim_id = self.env['progressive.claim'].browse(self.env.context.get('active_id'))

        res = self.request_line_ids
        request_line = []
        for request in res:
            request_line.append(
                (0, 0, {'work_order': request.work_order and request.work_order.id or False, 
                        'stage': request.stage and request.stage.id or False, 
                        'assigned_to': request.assigned_to and request.assigned_to.id or False,
                        'completion_date': request.completion_date, 
                        'stage_weightage': request.stage_weightage,
                        'work_weightage': request.work_weightage,
                        'work_progress': request.work_progress,
                        'progress' : request.progress, 
                        }
                ))

        self.env['claim.request.line'].sudo().create({
            'claim_id': progressive_claim_id and progressive_claim_id.id or False,
            'request_for': self.request_for,
            'partner_request_id': self.partner_request_id and self.partner_request_id.id or False,
            'created_by': self.create_uid and self.create_uid.id or False,
            'creation_date': datetime.now(),
            'requested_progress': self.requested_progress,
            'requested_progress_2': self.requested_progress,
            'requested_amount': self.requested_amount,
            'contract_amount': self.contract_amount,                                    
            #'request_line': [(6, 0, self.request_line_ids.ids)],
            'request_status': 'waiting',
            'request_ids': request_line,
            'request_id': self.id
        })

        if self.request_for == 'progress':
            if not self.request_line_ids:
                raise ValidationError(_( "Add at least one work order to request."))
        return self.write({'state': 'waiting'}) 


    name = fields.Char(string='Number', copy=False, required=True, readonly=True, 
                        index=True, default=lambda self: _('New'))
    
    request_for = fields.Selection([
        ('down_payment', 'Down Payment'),
        ('progress', 'Progress'),
        ('retention1', 'Retention 1'),
        ('retention2', 'Retention 2')
        ], string='Requested Type', required=True)

    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting', 'Waiting For Approval'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected')
        ], string='Status', readonly=True, copy=False, index=True, default='draft')

    project_id = fields.Many2one('project.project', 'Project')
    partner_id = fields.Many2one(
        'res.partner', string='Customer',
        change_default=True, index=True,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")
    partner_request_id = fields.Many2one(
        'res.partner', string='Request Address',
        change_default=True, index=True,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True, readonly=True,
                                 default=lambda self: self.env.company)
    branch_id = fields.Many2one('res.branch', string="Branch")
    create_uid = fields.Many2one('res.users', index=True, readonly=True, default=lambda self: self.env.user)
    create_date = fields.Datetime(default=fields.Datetime.now, readonly=True)
    project_director = fields.Many2one('res.users', string='Project Director')
    requested_progress = fields.Float(string='Requested Progress', digits=(2,2))
    requested_amount = fields.Float(string='Requested Amount')
    contract_amount = fields.Float(string='Contract Amount')
    request_line_ids = fields.One2many('request.line', 'request_id', string="Request Lines")
    progressive_claim_id = fields.Many2one('progressive.claim', string="Progressive Claim")

    
    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('claim.request.sequence') or '/'
        return super(CreateClaimRequest, self).create(vals)

    @api.onchange('request_line_ids')
    def onchange_request_line_ids(self):
        if self.request_line_ids:
            self.requested_progress = sum(self.request_line_ids.mapped('progress'))

class RequestLines(models.Model):
    _name = 'request.line'
    _description = 'Request Lines'

    request_id = fields.Many2one('claim.request', string="Request ID")
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    work_order = fields.Many2one('project.task', string="Work Order",
                 domain="[('project_id', '=', parent.project_id), ('state', '!=', 'draft')]")
    stage = fields.Many2one('project.stage' ,string="Stage")
    assigned_to = fields.Many2one('res.users', string="PIC")
    completion_date = fields.Datetime(string="Completion Date")
    approved_date = fields.Datetime(string="Approved Date")
    stage_weightage = fields.Float(string="Stage Weightage (%)")
    work_weightage = fields.Float(string="Work Order Weightage (%)")
    work_progress = fields.Float(string="Work Order Progress (%)")
    progress = fields.Float(string="Project Progress (%)", compute="_compute_progress", digits=(2,2))
    project_id = fields.Many2one(related='request_id.project_id', string='Project')

    @api.depends('request_id.request_line_ids', 'request_id.request_line_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.request_id.request_line_ids:
                no += 1
                l.sr_no = no

    @api.onchange('work_order')
    def onchange_work_order(self):
        if self.work_order:
            self.stage = self.work_order.stage and self.work_order.stage.id or False
            self.assigned_to = self.work_order.assigned_to and self.work_order.assigned_to.id or False
            self.completion_date = self.work_order.actual_end_date
            self.stage_weightage = self.work_order.stage_weightage
            self.work_progress = self.work_order.progress
            self.work_weightage = self.work_order.work_weightage

    
    @api.depends('stage_weightage', 'work_weightage', 'work_progress')
    def _compute_progress(self):
        total = 0
        for res in self:
            total = (res.stage_weightage * ((res.work_weightage * res.work_progress)/100)) / 100
            res.progress = total
        return total
