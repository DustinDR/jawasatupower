from odoo import _, api, fields, models
from odoo.exceptions import ValidationError, UserError


class CreateProgressiveInvoiceWiz(models.TransientModel):
    _name = 'progressive.invoice.wiz'
    _description = 'Create Progressive Invoice'

    def create_invoice(self):
        form_view = self.env.ref('account.view_move_form')
        return {
                "name": "Invoice",
                "type": "ir.actions.act_window",
                "res_model": "account.move",
                "context": {'default_project_invoice': 'progressive', 'default_move_type' : 'out_invoice' },
                'views': [(form_view.id, 'form')],
                "target": "current",
            }

    approved_progress = fields.Float(string='Approved Progress', digits=(2,2))
    invoice_progress = fields.Float(string='Invoice Progress', digits=(2,2))
    invoice_for = fields.Selection([
        ('down_payment', 'Down Payment'),
        ('progress', 'Progress'),
        ('retention1', 'Retention 1'),
        ('retention2', 'Retention 2')
        ], string='Invoice Type')
    method = fields.Selection([
        ('fix', 'Fixed'),
        ('per', 'Percentage')
        ], string='Method', default="per")
    progressive_claim_id = fields.Many2one('progressive.claim', string="Progressive Claim")
    approved_amount = fields.Float(string='Approved Amount', compute="_compute_approved_amount")
    currency_id = fields.Many2one('res.currency')
    contract_amount = fields.Float(string='Contract Amount')
    dp_amount = fields.Float(string="Down Payment")
    retention1 = fields.Float(string="Retention 1")
    retention2 = fields.Float(string="Retention 2")
    retention1_amount = fields.Float(string="Amount")
    retention2_amount = fields.Float(string="Amount")
    tax_id = fields.Many2many('account.tax', 'taxes1', string="Taxes")
    vat_tax = fields.Many2many('account.tax', 'vat_tax1', string="VAT Tax")
    income_tax = fields.Many2many('account.tax', 'income_tax1', string="Income Tax")
    gross_amount = fields.Float(string="Gross Amount", compute="_compute_amount")
    dp_deduction = fields.Float(string="DP Deduction", compute="_compute_amount")
    dp_deduction_temp = fields.Float(string="DP Deduction Temp", compute="_compute_amount")
    retention_deduction = fields.Float(string="Retention Deduction", compute="_compute_amount")
    retention_deduction_temp = fields.Float(string="Retention Deduction Temp", compute="_compute_amount")
    amount_deduction = fields.Float(string="Amount After Deduction", compute="_compute_amount")
    amount_untaxed = fields.Float(string="Amount Untaxed", compute="_compute_amount")
    tax_amount = fields.Float(string="Tax Amount", compute="_compute_amount")
    amount_invoice = fields.Float(string="Total Amount to Invoice", compute="_compute_amount")
    progress = fields.Float(string="Progress", compute="_compute_amount", digits=(2,2))
    last_progress = fields.Float('Last Invoice Progress', digits=(2,2))
    last_amount = fields.Float('Last Invoice Amount', compute="_compute_last_amount")
    
    @api.onchange('invoice_progress')
    def onchange_invoice_progress1(self):
        if self.invoice_for == 'progress':
            if self.method == 'fix':
                if self.invoice_progress > self.approved_amount:
                    raise ValidationError(_("The inputted Invoice Progress exceeds the Approved Progress.\nPlease, re-input Invoice Progress."))
            elif self.method == 'per':
                if self.invoice_progress > self.approved_progress:
                    raise ValidationError(_("The inputted Invoice Progress exceeds the Approved Progress.\nPlease, re-input Invoice Progress."))

    @api.onchange('invoice_progress')
    def onchange_invoice_progress2(self):
        if self.invoice_for == 'progress':
            if self.method == 'fix':
                if self.invoice_progress <= self.last_amount:
                    raise ValidationError(_("The inputted Invoice Progress is less than the Last Invoice Progress.\nPlease, re-input Invoice Progress."))
            elif self.method == 'per':
                if self.invoice_progress <= self.last_progress:
                    raise ValidationError(_("The inputted Invoice Progress is less than the Last Invoice Progress.\nPlease, re-input Invoice Progress."))

    @api.depends('contract_amount', 'approved_progress')
    def _compute_approved_amount(self):
        total = 0.0
        for res in self:
            total = res.contract_amount * (res.approved_progress / 100)
            res.approved_amount = total
        return total

    @api.depends('contract_amount', 'last_progress')
    def _compute_last_amount(self):
        tal = 0.0
        for res in self:
            tal = res.contract_amount * (res.last_progress / 100)
            res.last_amount = tal
        return tal

    @api.onchange('method')
    def onchange_method(self):
        if self.invoice_for == 'progress':
            for res in self.progressive_claim_id:
                if self.method == 'fix':
                    self.invoice_progress = res.contract_amount * (res.approved_progress / 100)
                elif self.method == 'per':
                    self.invoice_progress = res.approved_progress


    # Calculation of amount invoice
    @api.depends('invoice_progress', 'contract_amount', 'dp_amount', 'retention1',
                 'retention2', 'method', 'invoice_for', 'last_progress','tax_id')    
    def _compute_amount(self):
        for rec in self:
            invoice_for = rec.invoice_for
            method = rec.method
            invoice_progress = rec.invoice_progress
            contract_amount = rec.contract_amount
            dp_amount = rec.dp_amount
            retention1 = rec.retention1
            retention2 = rec.retention2
            last_progress = rec.last_progress
            tax_id = rec.tax_id
            
            def _compute_progress(invoice_for,contract_amount,invoice_progress,method):
                vals = 0.0
                if invoice_for != 'progress':
                    vals += 0.0
                elif invoice_for == 'progress':
                    if method == 'fix':
                        vals += (invoice_progress/contract_amount) * 100
                    elif method == 'per':
                        vals += invoice_progress
                return vals

            def _compute_gross_amount(invoice_for,contract_amount,dp_amount,invoice_progress,method,retention1,retention2):
                value = 0.0
                if invoice_for == 'down_payment':
                    value += dp_amount
                elif invoice_for == 'progress':
                    if method == 'fix':
                        value += invoice_progress
                    elif method == 'per':
                        value += contract_amount * (invoice_progress/100)
                elif invoice_for == 'retention1':
                    value += contract_amount * (retention1/100)
                elif invoice_for == 'retention2':
                    value += contract_amount * (retention2/100)
                return value

            def _compute_dp_deduction(invoice_for,contract_amount,invoice_progress,method,dp_amount):
                vals = 0.0
                if invoice_for != 'progress':
                    vals += 0.0
                elif invoice_for == 'progress':
                    if method == 'fix':
                        vals += ((invoice_progress/contract_amount) * dp_amount) 
                    elif method == 'per':
                        vals += (dp_amount * (invoice_progress/100))
                return vals

            def _compute_retention_deduction(invoice_for,gross_amount,retention1,retention2):
                cons = 0.0
                if invoice_for != 'progress':
                    cons += 0.0
                elif invoice_for == 'progress':
                    cons += (gross_amount * ((retention1/100) + (retention2/100)))
                return cons

            def _compute_amount_deduction(invoice_for,gross_amount,dp_deduction,retention_deduction):
                amt = 0.0
                if invoice_for != 'progress':
                    amt += 0.0
                elif invoice_for == 'progress':
                    amt += gross_amount - (dp_deduction + retention_deduction)
                return amt

            def _compute_dp_deduction_temp(invoice_for,contract_amount,progress,last_progress,dp_amount):
                tot = 0.0
                if invoice_for != 'progress':
                    tot += 0.0
                elif invoice_for == 'progress':
                    tot += (contract_amount * ((progress - last_progress) / 100)) * ((dp_amount / contract_amount))
                return tot

            def _compute_retention_deduction_temp(invoice_for,contract_amount,progress,last_progress,retention1,retention2):
                tes = 0.0
                if invoice_for != 'progress':
                    tes += 0.0
                elif invoice_for == 'progress':
                    tes += (contract_amount * ((progress - last_progress) / 100)) * ((retention1 + retention2) / 100)
                return tes

            def _compute_amount_untaxed(invoice_for,invoice_progress,contract_amount,progress,last_progress,dp_deduction_temp,retention_deduction_temp):
                cob = 0.0
                if invoice_for != 'progress':
                    cob += invoice_progress
                elif invoice_for == 'progress':
                    cob += (contract_amount * ((progress - last_progress) / 100)) - (dp_deduction_temp + retention_deduction_temp)
                return cob

            def _compute_amount_tax(tax_id,amount_untaxed):
                vals = 0.0
                for tax in tax_id:
                    vals += (tax.amount/100) * amount_untaxed
                return vals

            
            progress = _compute_progress(invoice_for,contract_amount,invoice_progress,method)
            gross_amount = _compute_gross_amount(invoice_for,contract_amount,dp_amount,invoice_progress,method,retention1,retention2)
            dp_deduction = _compute_dp_deduction(invoice_for,contract_amount,invoice_progress,method,dp_amount)
            retention_deduction =  _compute_retention_deduction(invoice_for,gross_amount,retention1,retention2)
            amount_deduction = _compute_amount_deduction(invoice_for,gross_amount,dp_deduction,retention_deduction)
            dp_deduction_temp = _compute_dp_deduction_temp(invoice_for,contract_amount,progress,last_progress,dp_amount)
            retention_deduction_temp = _compute_retention_deduction_temp(invoice_for,contract_amount,progress,last_progress,retention1,retention2)
            amount_untaxed = _compute_amount_untaxed(invoice_for,invoice_progress,contract_amount,progress,last_progress,dp_deduction_temp,retention_deduction_temp)
            tax_amount = _compute_amount_tax(tax_id,amount_untaxed) 


            rec.progress = progress
            rec.gross_amount = gross_amount
            rec.dp_deduction = dp_deduction
            rec.retention_deduction = retention_deduction
            rec.amount_deduction = amount_deduction
            rec.dp_deduction_temp = dp_deduction_temp
            rec.retention_deduction_temp = retention_deduction_temp
            rec.amount_untaxed = amount_untaxed
            rec.tax_amount = tax_amount
            rec.amount_invoice = amount_untaxed + tax_amount


    def create_invoice(self):
        
        def _get_context_invoice(progressive_claim_id,invoice_for,progress):
            vals = {}
            vals['move_type'] = 'out_invoice'
            vals['invoice_date'] = fields.Date.today()
            vals['partner_id'] = progressive_claim_id.partner_invoice_id.id
            vals['claim_description'] = _get_claim_description(invoice_for,progress)
            vals['state'] = 'draft'
            vals['claim_id'] = progressive_claim_id.id
            vals['project_invoice'] = 'progressive'
            vals['progressive_method'] = invoice_for
            vals['progress'] = progress
            vals['project_id'] = progressive_claim_id.project_id.id or False
            vals['company_id'] = progressive_claim_id.company_id.id or False
            vals['analytic_group_ids'] = progressive_claim_id.analytic_idz or False
                
            return vals
        
        def _get_claim_description(invoice_for,progress):
            value = ''
            if invoice_for == 'down_payment':
                value = f'Down Payment'
            elif invoice_for == 'progress':
                value = f'Progressive {progress}%'
            elif invoice_for == 'retention1':
                value = f'Retention 1'
            elif invoice_for == 'retention2':
                value = f'Retention 2'
            return value
        
        def _get_invoice_line_ids(progressive_claim_id,invoice_for,progress,last_progress,inv):
            vals = False
            name = ''
            name1 = ''
            analytic_account_id = progressive_claim_id.project_id.analytic_account_id.id
            analytic_tag_ids = progressive_claim_id.project_id.analytic_idz.ids
            
            if invoice_for == 'down_payment':
                name = 'Down Payment '
                name1 = ''
            elif invoice_for == 'progress':
                name = 'Progressive '
                name1 += (' ' +str(progress) + '%')
            elif invoice_for == 'retention1':
                name = 'Retention 1 '
                name1 = ''
            elif invoice_for == 'retention2':
                name = 'Retention 2 '
                name1 = ''
            
            vals ={
                'name': name + name1,
                'analytic_tag_ids': analytic_tag_ids,
                'analytic_account_id':analytic_account_id,
                'quantity': 1,
                'progress': progress - last_progress,
                'dp_deduction': self.dp_deduction,
                'retention_deduction':self.retention_deduction,
                'account_id':inv.journal_id.default_account_id.id,
                'price_unit':self.amount_untaxed,
                'tax_ids': progressive_claim_id.tax_id, 
                'price_tax': self.tax_amount,
            }
            
            return vals
        
        def _create_inv(vals):
            move_obj = self.env['account.move']
            inv = move_obj.create(vals)
            invoice_line_ids = _get_invoice_line_ids(progressive_claim_id,invoice_for,progress,last_progress,inv)
            if invoice_line_ids:
                inv.invoice_line_ids = [(0,0,invoice_line_ids)] 
            return inv        
        
        active_id = self._context.get('active_id')
        progressive_claim_id = self.env['progressive.claim'].browse(active_id)
        invoice_for = self.invoice_for
        progress = self.progress
        last_progress = self.last_progress
        
        vals = _get_context_invoice(progressive_claim_id,invoice_for,progress)
        inv = _create_inv(vals)
        
        action = {
            "type": "ir.actions.act_window",
            "res_model": "account.move",
            "res_id": inv.id,
            "views": [(False, "form")],
            "target": "current",            
        }
        return action