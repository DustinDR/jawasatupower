# -*- coding: utf-8 -*-
{
    'name': "equip3_accounting_giro",
    'author': "Irfan Suendi",
    'category': 'accounting',
    'version': '1.1.8',
    'depends': ['account','equip3_accounting_operation'],
    'data': [
        # 'security/ir.model.access.csv',
        'views/account_multipayment_giro_views.xml',
        'data/ir_sequence_data.xml',
    ],
    'demo': [],
    'auto_install': True,
    'installable': True,
    'application': True
}
