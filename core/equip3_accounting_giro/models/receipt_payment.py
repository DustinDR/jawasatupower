from odoo import models, fields, api, _
from odoo.exceptions import UserError
import datetime

class accountmultipayment(models.Model):
    _inherit = "account.multipayment"

    payment_type = fields.Selection([
        ('payment', 'Payment'),
        ('giro', 'Giro')
        ], string='Payment Type', tracking=True, default='payment')
    clearing_account_id = fields.Many2one('account.account', string='Clearing Account', tracking=True)
    move_id = fields.Many2one('account.move', string='Clear Journal Entry Reconcile Journal Entry', tracking=True, readonly=True)
    due_date = fields.Date(string='Due Date', tracking=True)
    receive_date = fields.Date(string='Receive Date', tracking=True)
    clearing_date = fields.Date(string='Clearing Date', tracking=True)
    is_vendor = fields.Boolean(compute='_compute_partner_type')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('post', 'Received'),
        ('canceled', 'Cancel'),
        ('cleared', 'Cleared'),
        ('rejected', 'Rejected'),
        ], string='Status',default='draft', tracking=True)

    def _compute_partner_type(self):
        if self.partner_type == 'Supplier':
            self.is_payment_vendor = True
        else:
            self.is_payment_vendor = False

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            seq_date = None
            if 'date' in vals:
                seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['date']))
            if vals['partner_type'] == 'customer':
                if 'payment_type' in vals:
                    if vals['payment_type'] == 'giro':
                        vals['name'] = self.env['ir.sequence'].next_by_code('receipt.giro', sequence_date=seq_date) or _('New')
                    else:
                        vals['name'] = self.env['ir.sequence'].next_by_code('customer.account.multipayment', sequence_date=seq_date) or _('New')
                else:
                    vals['name'] = self.env['ir.sequence'].next_by_code('customer.account.multipayment', sequence_date=seq_date) or _('New')
            elif vals['partner_type'] == 'supplier':
                if 'payment_type' in vals:
                    if vals['payment_type'] == 'giro':
                        vals['name'] = self.env['ir.sequence'].next_by_code('payment.giro', sequence_date=seq_date) or _('New')
                    else:                    
                        vals['name'] = self.env['ir.sequence'].next_by_code('vendor.account.multipayment', sequence_date=seq_date) or _('New')
                else:                    
                    vals['name'] = self.env['ir.sequence'].next_by_code('vendor.account.multipayment', sequence_date=seq_date) or _('New')
        result = super(accountmultipayment, self).create(vals)
        return result

    def action_validate(self):
        for rec in self:
            if len(rec.line_credit_ids) > 0:
                for inv_credit in rec.line_credit_ids:
                    if inv_credit.amount == 0:
                        continue
                    payment = self._create_payments(inv_credit)
                    rec.receive_date = payment.date
                    rec.due_date = payment.create_date
                    inv_credit.payment_id = payment.id
                    payment.update({'payment_transaction_credit_count' : '1'})
            if len(rec.line_debit_ids) > 0:
                for inv_debit in rec.line_debit_ids:
                    if inv_debit.amount == 0:
                        continue
                    payment = self._create_payments(inv_debit)
                    rec.receive_date = payment.date
                    rec.due_date = payment.create_date
                    inv_debit.payment_id = payment.id
                    payment.update({'payment_transaction_debit_count' : '1'})            
            rec.write({'state': 'post'})

    def action_clearing(self):
        line_ids_det = []
        line = {
                'name': '',
                'account_id': self.clearing_account_id.id,
                'debit': self.amount,
                'credit': 0.0
                }
        line_ids_det.append((0,0,line))
        line = {
                'name': '',
                'account_id': self.journal_id.default_account_id.id,
                'debit':  0.0,
                'credit': self.amount
                }
        line_ids_det.append((0,0,line))
        all_move_vals = {
                'date': datetime.date.today(),
                'journal_id': self.journal_id.id,
                'line_ids': line_ids_det
            }
                
        AccountMove = self.env['account.move']
        moves = AccountMove.create(all_move_vals)
        moves.post()
        self.update({'state' : 'cleared',
                     'clearing_date' : moves.create_date,
                     'move_id': moves.id})

    