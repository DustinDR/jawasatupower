from odoo import models, fields, api, _
from odoo.exceptions import UserError
import datetime
from lxml import etree
from odoo.addons.base.models.ir_ui_view import (
transfer_field_to_modifiers, transfer_node_to_modifiers, transfer_modifiers_to_node,
)


def setup_modifiers(node, field=None, context=None, in_tree_view=False):
    modifiers = {}
    if field is not None:
        transfer_field_to_modifiers(field, modifiers)
    transfer_node_to_modifiers(
        node, modifiers, context=context)
    transfer_modifiers_to_node(modifiers, node)
    
    

class AccountReceipPayment(models.Model):
    _name = 'receipt.payment'
    _description = "Receipt Payment"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']

    name = fields.Char(string='Number', readonly=True, tracking=True)
    partner_id = fields.Many2one('res.partner', readonly=True, string='Customer', required=True, tracking=True)
    amount = fields.Monetary(string='Paid Amount', readonly=True, store=True, compute='compute_payment_amount', tracking=True)
    journal_id = fields.Many2one('account.journal', string='Payment Method', required=True, tracking=True)
    currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.company.currency_id.id, readonly=True, store=True, required=True, tracking=True)
    clearing_account_id = fields.Many2one('account.account', string='Clearing Account', required=True, tracking=True)
    move_id = fields.Many2one('account.move', string='Clear Journal Entry Reconcile Journal Entry', tracking=True, readonly=True)
    date = fields.Date(string='Date', required=True, tracking=True)
    payment_ref = fields.Char(string='Payment Ref', tracking=True)
    memo = fields.Char(string='memo', tracking=True)
    due_date = fields.Datetime(string='Due Date', tracking=True)
    receive_date = fields.Datetime(string='Receive Date', tracking=True)
    clearing_date = fields.Datetime(string='Clearing Date', tracking=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company.id, tracking=True, readonly=True)
    branch_id = fields.Many2one('res.branch', string='Branch', default=lambda self: self.env.user.branch_id.id, tracking=True)
    diff_amount = fields.Monetary(string='Difference Amount', readonly=True, tracking=True)
    writeoff_acc_id = fields.Many2one('account.account', string='Counterpart Account', domain=[('deprecated','=',False)], tracking=True)
    naration = fields.Text(string='Notes', tracking=True)
    create_uid = fields.Many2one('res.users', string='Created by', readonly=True, tracking=True)
    create_date = fields.Datetime(string="Created Date", readonly=True, tracking=True)
    partner_type = fields.Selection([
        ('customer', 'Customer'),
        ('supplier', 'Vendor')
        ], string='Partner Type', tracking=True)
    line_credit_ids = fields.One2many('receipt.payment.line.credit', 'line_id', string='Credits')
    line_debit_ids = fields.One2many('receipt.payment.line.debit', 'line_id', string='Debits')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('open', 'Open'),
        ('post', 'Posted'),
        ('cancel', 'Cancelled'),
        ('cleared', 'Cleared'),
        ('rejected', 'Rejected'),
        ], string='State', default='draft', tracking=True)
    total_amount_credit = fields.Monetary(string="Total Amount", compute='_calculate_credit_amount_total', store=True, tracking=True)
    total_amount_debit = fields.Monetary(string="Total Amount", compute='_calculate_credit_amount_total', store=True, tracking=True)
    payment_id_count = fields.Integer(string="Payment", compute='_calculate_credit_amount_total')

class ReceiptPaymentLineCredit(models.Model):
    _name = "receipt.payment.line.credit"

    line_id = fields.Many2one('receipt.payment', string='Detail', readonly=True)
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.company.currency_id, string='Currency')
    invoice_id = fields.Many2one('account.move', string='Invoice', readonly=True)
    account_id = fields.Many2one('account.account', string='Account', readonly=True)
    invoice_date = fields.Date(string='Date', readonly=True)
    invoice_date_due = fields.Date(string='Due Date', readonly=True)
    original_amount = fields.Monetary(string='Original Currency Amount', readonly=True)
    base_amount = fields.Monetary(string='Base Currency Amount', readonly=True)
    original_unreconcile = fields.Monetary(string='Original Open Balance', readonly=True)
    base_unreconcile = fields.Monetary(string='Base Currency Open Balancet', readonly=True)
    is_full_reconcile = fields.Boolean(string='Full Reconcile')
    amount = fields.Monetary(string='Amount')
    payment_id = fields.Many2one('account.payment', string='Payment')    

    @api.onchange('is_full_reconcile')
    def calculate_credit_amount_base(self):
        for rec in self:
            if rec.is_full_reconcile:
                rec.amount = rec.base_unreconcile
            else:
                rec.amount = False

class ReceiptPaymentLineDebit(models.Model):
    _name = "receipt.payment.line.debit"

    line_id = fields.Many2one('receipt.payment', string='Detail', readonly=True)
    currency_id = fields.Many2one('res.currency', default=lambda self: self.env.company.currency_id, string='Currency')
    invoice_id = fields.Many2one('account.move', string='Invoice', readonly=True)
    account_id = fields.Many2one('account.account', string='Account', readonly=True)
    invoice_date = fields.Date(string='Date', readonly=True)
    invoice_date_due = fields.Date(string='Due Date', readonly=True)
    original_amount = fields.Monetary(string='Original Currency Amount', readonly=True)
    base_amount = fields.Monetary(string='Base Currency Amount', readonly=True)
    original_unreconcile = fields.Monetary(string='Original Open Balance', readonly=True)
    base_unreconcile = fields.Monetary(string='Base Currency Open Balancet', readonly=True)
    is_full_reconcile = fields.Boolean(string='Full Reconcile')
    amount = fields.Monetary(string='Amount')
    payment_id = fields.Many2one('account.payment', string='Payment')
    
    @api.onchange('is_full_reconcile')
    def calculate_debit_amount_base(self):
        for rec in self:
            if rec.is_full_reconcile:
                rec.amount = rec.base_unreconcile
            else:
                rec.amount = False
                
class accountmultipayment(models.Model):
    _inherit = "account.multipayment"
    
    
    @api.model
    def get_state_selection(self):
        context = self._context
        result = [('draft', 'Draft'), 
                  ('to_approve', 'Waiting For Approval'),
                  ('approved', 'Approved'),
                  ('post', 'Received'),
                  ('canceled', 'Cancel'), 
                  ('cleared', 'Cleared'),
                  ('rejected', 'Rejected')]
        # if context.get('default_partner_type') == 'supplier':
        #     result = [('draft', 'Draft'), 
        #           ('to_approve', 'Waiting For Approval'),
        #           ('approved', 'Approved'),
        #           ('post', 'Received'),
        #           ('canceled', 'Cancel'), 
        #           ('cleared', 'Cleared'),
        #           ('rejected', 'Rejected')]
        return result
    
    
    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        result = super(accountmultipayment, self).fields_view_get(
            view_id, view_type, toolbar=toolbar, submenu=submenu)
        context = self._context
        doc = etree.XML(result['arch'])
        if context.get('default_partner_type') == 'supplier':
            for node in doc.xpath("//field[@name='receive_date']"):
                node.set('string', 'Payment Date')
                setup_modifiers(node, result['fields']['receive_date'])

        result['arch'] = etree.tostring(doc, encoding='unicode')
        return result

    payment_type = fields.Selection([
        ('payment', 'Payment'),
        ('giro', 'Giro')
        ], string='Payment Type', tracking=True, default='payment')
    clearing_account_id = fields.Many2one('account.account', string='Clearing Account', tracking=True)
    move_id = fields.Many2one('account.move', string='Clear Journal Entry Reconcile Journal Entry', tracking=True, readonly=True)
    due_date = fields.Date(string='Due Date', tracking=True)
    receive_date = fields.Date(string='Receive Date', tracking=True)
    clearing_date = fields.Date(string='Clearing Date', tracking=True)
    is_vendor = fields.Boolean(compute='_compute_partner_type')
    state = fields.Selection(selection= lambda self: self.get_state_selection(), string='Status', default='draft', tracking=True)
    state1 = fields.Selection(related="state")
    state2 = fields.Selection(related="state")

    def _compute_partner_type(self):
        if self.partner_type == 'supplier':
            self.is_vendor = True
        else:
            self.is_vendor = False

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            seq_date = None
            if 'date' in vals:
                seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['date']))
            if vals['partner_type'] == 'customer':
                if 'payment_type' in vals:
                    if vals['payment_type'] == 'giro':
                        vals['name'] = self.env['ir.sequence'].next_by_code('receipt.giro', sequence_date=seq_date) or _('New')
                    else:
                        vals['name'] = self.env['ir.sequence'].next_by_code('customer.account.multipayment', sequence_date=seq_date) or _('New')
                else:
                    vals['name'] = self.env['ir.sequence'].next_by_code('customer.account.multipayment', sequence_date=seq_date) or _('New')
            elif vals['partner_type'] == 'supplier':
                if 'payment_type' in vals:
                    if vals['payment_type'] == 'giro':
                        vals['name'] = self.env['ir.sequence'].next_by_code('payment.giro', sequence_date=seq_date) or _('New')
                    else:                    
                        vals['name'] = self.env['ir.sequence'].next_by_code('vendor.account.multipayment', sequence_date=seq_date) or _('New')
                else:                    
                    vals['name'] = self.env['ir.sequence'].next_by_code('vendor.account.multipayment', sequence_date=seq_date) or _('New')
        result = super(accountmultipayment, self).create(vals)
        return result

    def action_validate_giro(self):
        for rec in self:
            if len(rec.line_credit_ids) > 0:
                for inv_credit in rec.line_credit_ids:
                    if inv_credit.amount == 0:
                        continue
                    payment = self._create_payments(inv_credit)
                    rec.receive_date = payment.date
                    rec.due_date = payment.create_date
                    inv_credit.payment_id = payment.id
                    payment.update({'payment_transaction_credit_count' : '1'})
            if len(rec.line_debit_ids) > 0:
                for inv_debit in rec.line_debit_ids:
                    if inv_debit.amount == 0:
                        continue
                    payment = self._create_payments(inv_debit)
                    rec.receive_date = payment.date
                    rec.due_date = payment.create_date
                    inv_debit.payment_id = payment.id
                    payment.update({'payment_transaction_debit_count' : '1'})            
            if self.diff_amount == 0:
                rec.write({'state': 'post'})
            else:
                moves = self.create_diff_Journal()
                rec.write({'state': 'post', 'different_move_id' : moves.id})

    def action_validate(self):
        for record in self:
            if record.is_multi_payment_approval_matrix and record.payment_type == 'giro':
                record.action_validate_giro()
            else:
                return super(accountmultipayment, self).action_validate()

    @api.depends('amount', 'company_id', 'branch_id')
    def _get_multi_payment_approval_matrix(self):
        for record in self:
            matrix_id = False
            if record.partner_type == 'customer' and not record.payment_type == 'giro':
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.amount),
                        ('max_amount', '>=', record.amount),
                        ('approval_matrix_type', '=', 'customer_multi_receipt_approval_matrix')
                    ], limit=1)
            elif record.partner_type == 'supplier' and not record.payment_type == 'giro':
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.amount),
                        ('max_amount', '>=', record.amount),
                        ('approval_matrix_type', '=', 'vendor_multi_receipt_approval_matrix')
                    ], limit=1)
            elif record.partner_type == 'customer' and record.payment_type == 'giro':
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.amount),
                        ('max_amount', '>=', record.amount),
                        ('approval_matrix_type', '=', 'receipt_giro_approval_matrix')
                    ], limit=1)
            elif record.partner_type == 'supplier' and record.payment_type == 'giro':
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.amount),
                        ('max_amount', '>=', record.amount),
                        ('approval_matrix_type', '=', 'payment_giro_approval_matrix')
                    ], limit=1)
            record.multipul_payment_approval_matrix_id = matrix_id

    def _get_multi_payment_approve_button_from_config(self):
        for record in self:
            is_multi_payment_approval_matrix = False
            if record.partner_type == 'customer' and not record.payment_type == 'giro':
                is_multi_payment_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_customer_multi_receipt_approval_matrix', False)
            elif record.partner_type == 'supplier' and not record.payment_type == 'giro':
                is_multi_payment_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_vendor_multipayment_approval_matrix', False)
            elif record.payment_type == 'giro' and record.partner_type == 'customer':
                is_multi_payment_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_receipt_giro_approval_matrix', False)
            elif record.payment_type == 'giro' and record.partner_type == 'supplier':
                is_multi_payment_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_payment_giro_approval_matrix', False)
            record.is_multi_payment_approval_matrix = is_multi_payment_approval_matrix

    def action_rejected(self):
        self.unlink_all_moves()
        self.write({'state': 'rejected'})

    def action_draft(self):
        self.unlink_all_moves()
        if self.different_move_id != False:
            self.update({'different_move_id' : False})
        if self.move_id != False:
            self.update({'move_id' : False})
        self.unlink_payment()
        if len(self.line_credit_ids) > 0:
            for rec in self.line_credit_ids:
                if rec.payment_id.id != False:
                    rec.update({'payment_id' : False})
        if len(self.line_debit_ids) > 0:
            for rec in self.line_debit_ids:
                if rec.payment_id.id != False:
                    rec.update({'payment_id' : False})
        self.write({'state': 'draft'})

    def action_clearing(self):
        line_ids_det = []
        partner_type = self.partner_type

        debit_line_account = partner_type=='supplier' and self.clearing_account_id.id or self.journal_id.default_account_id.id
        credit_line_account = partner_type=='supplier' and self.journal_id.default_account_id.id or self.clearing_account_id.id
        
        debit_line = {
                'name': '',
                'account_id': debit_line_account,
                'credit':  0.0,
                'debit': self.amount
                }
        
        credit_line = {
                'name': '',
                'account_id': credit_line_account,
                'credit': self.amount,
                'debit': 0.0
                }
        
        line_ids_det.append((0,0, debit_line))
        line_ids_det.append((0,0, credit_line))
        
          
        all_move_vals = {
                'date': datetime.date.today(),
                'journal_id': self.journal_id.id,
                'line_ids': line_ids_det
            }
                
        AccountMove = self.env['account.move']
        moves = AccountMove.create(all_move_vals)
        moves.post()
        self.update({'state' : 'cleared',
                     'clearing_date' : moves.create_date,
                     'move_id': moves.id})

    def unlink_all_moves(self):
        if self.different_move_id != False:
            self.unlink_diff_journal()
        if self.move_id != False:
            self.unlink_clearing()
        self.unlink_payment()

    def unlink_clearing(self):
        # OVERRIDE to unlink the inherited account.move (move_id field) as well.
        moves = self.with_context(force_delete=True).move_id
        moves.button_draft()
        moves.button_cancel()
        # moves.unlink()
        return moves

    def unlink_diff_journal(self):
        # OVERRIDE to unlink the inherited account.move (move_id field) as well.
        moves = self.with_context(force_delete=True).different_move_id
        moves.button_draft()
        moves.button_cancel()
        # moves.unlink()
        return moves

    def unlink_payment(self):
        payment = self.env['account.payment']
        if len(self.line_credit_ids) > 0:
            for rec in self.line_credit_ids:
                if rec.payment_id.id != False:
                    payment_id = payment.search([('id', '=', rec.payment_id.id)])
                    payment_id.action_draft()
                    payment_id.action_cancel()
                    # payment_id.unlink()
        if len(self.line_debit_ids) > 0:
            for rec in self.line_debit_ids:
                if rec.payment_id.id != False:
                    payment_id = payment.search([('id', '=', rec.payment_id.id)])
                    payment_id.action_draft()
                    payment_id.action_cancel()
                    # payment_id.unlink()
        return payment