# -*- coding: utf-8 -*-
{
    'name': "Equip3 HR Dashboard Extend",
    'summary': '',
    'description': '',
    'author': "HashMicro",
    'website': "https://www.hashmicro.com",
    'category': 'Human Resources',
    'version': '1.1.4',
    'depends': ['hr_reward_warning','sh_hr_dashboard'],
    'data': [
        # 'security/ir.model.access.csv',
        'views/templates.xml',
        'views/hr_dashboard.xml',
        'views/menu.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
