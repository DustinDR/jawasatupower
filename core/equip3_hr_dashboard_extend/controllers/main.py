# -*- coding: utf-8 -*-
import json
from odoo import http
from odoo.http import request


class Equip3HrDashboardExtend(http.Controller):

    @http.route('/get_hr_annoucement_data', type='http', auth="public", methods=['GET'])
    def get_annoucement_data(self, **post):
        current_user = request.session.uid
        announcements = request.env['hr.announcement'].sudo().search(
            [('state', '=', 'submitted'), ('email_employee_ids.user_id', '=', current_user)], order='date_start')
        return request.env['ir.ui.view'].with_context()._render_template(
            'equip3_hr_dashboard_extend.hr_annoucements_data_tbl', {'annoucements': announcements})

    @http.route(['/get-popup-announcement'], type='http', auth='public')
    def get_popup_announcement(self, **post):
        announce = request.env['hr.announcement'].sudo().browse(int(post.get('announce_id')))

        if announce:
            data = {
                'announcement_name': announce.announcement_reason,
                'date_start': str(announce.date_start.strftime('%d %B %Y')),
                'date_end': str(announce.date_end.strftime('%d %B %Y')),
                'announcement': announce.announcement
            }
            return json.dumps(data)
