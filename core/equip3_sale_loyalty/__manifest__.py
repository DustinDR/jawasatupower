# -*- coding: utf-8 -*-
{
    'name': "Equip3 Sale Loyality",

    'summary': """
        Manage your Loyality Management in sale""",

    'description': """
        This module manages these features :
        1. Loyalty Product
        2. Loyalty Point
    """,

    'author': "Hashmicro",
    'category': 'Sale',
    'version': '1.1.3',

    # any module necessary for this one to work correctly
    'depends': [
        'product',
        'bi_loyalty_generic', 
        'equip3_sale_other_operation',
        'point_of_sale',
    ],

    # always loaded
    'data': [
        'data/product_demo.xml',
        'views/sale_order_view.xml'
    ],
    # only loaded in demonstration mode
    'demo': [

    ],
}