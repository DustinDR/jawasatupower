# -*- coding: utf-8 -*-

{
    'name': 'ChatRoom WhatsApp ChatApi Connector',
    'summary': 'Real WhatsApp All in One Screen integration. Send Product, Drag and Drop. Connector. Chat-Api. ChatApi. ChatRoom',
    'description': 'Real WhatsApp All in One Screen integration. Send Product, Drag and Drop. Connector. Chat-Api. ChatApi. ChatRoom',
    'version': "1.1.2",
    'author': 'AcruxLab',
    'live_test_url': 'https://chatroom.acruxlab.com/web/signup',
    'support': 'info@acruxlab.com',
    'price': 50,
    'currency': 'USD',
    'images': ['static/description/Banner.gif'],
    'website': 'https://acruxlab.com',
    'license': 'OPL-1',
    'application': True,
    'installable': True,
    'category': 'Sales',
    'depends': [
        'acrux_chat',
    ],
    'data': [
        'data/data.xml',
        'view/Connector.xml'
    ],
}
