from odoo import api, fields, models, _
import datetime
from datetime import date
from dateutil.relativedelta import relativedelta
from datetime import timedelta
import calendar
from odoo.exceptions import ValidationError
import time


class HrGenerateWorkingCalendar(models.Model):
    _name = 'hr.generate.workingcalendar'
    _description = "Generate Working Calendar"
    _order = 'id desc'

    generate_type = fields.Selection([('create_update', 'Create/Update working calendar'),
                                      ('clear', 'Clear working calendar'),
                                      ('update', 'Update working calendar with other working hours')
                                      ], string='Generate Type', states={'generated': [('readonly', True)]},
                                     required=True)
    schedule_type = fields.Selection([('fixed_schedule', 'Fixed Schedule'),
                                      ('shift_pattern', 'Shift Schedule'),
                                      ('roster_schedule', 'Roster Schedule'),
                                      ], states={'generated': [('readonly', True)]}, string='Schedule Type')
    follow_contract_period = fields.Boolean('Follow Contract Period', default=True,
                                            states={'generated': [('readonly', True)]})
    date = fields.Date('Date', default=fields.Date.today(), states={'generated': [('readonly', True)]})
    start_date = fields.Date('Start Date', default=time.strftime('%Y-01-01'),
                             states={'generated': [('readonly', True)]})
    end_date = fields.Date('End Date', default=time.strftime('%Y-12-31'), states={'generated': [('readonly', True)]})
    past_record = fields.Char('Record', default='User cannot delete for past dated employee working schedule record')
    is_past_date = fields.Boolean('Past Date', compute='compute_current_date')
    state = fields.Selection([('draft', 'Draft'), ('generated', 'Generated')], string="State", default='draft')
    resource_calendar_id = fields.Many2one('resource.calendar', string="Working Schedule",
                                           states={'generated': [('readonly', True)]})
    employee_ids = fields.Many2many('hr.employee', string="Employee", states={'generated': [('readonly', True)]})

    @api.onchange('generate_type')
    def onchange_employee(self):
        res = {}
        cotract_obj = self.env['hr.contract'].search([('state', '=', 'open')])
        employee_obj = self.env['hr.employee'].search(
            [('contract_ids', '!=', False), ('contract_ids', 'in', cotract_obj.ids)])
        employee_working_calendar = self.env['employee.working.schedule.calendar'].search([])
        if self.generate_type != 'create_update':
            employee_list = []
            for vals in employee_working_calendar:
                employee_list.append(vals.employee_id.id)
            res['domain'] = {'employee_ids': [('id', 'in', employee_list)]}
        elif employee_obj:
            employee_list = []
            for vals in employee_obj:
                employee_list.append(vals.id)
                res['domain'] = {'employee_ids': [('id', 'in', employee_list)]}
        else:
            res['domain'] = {'employee_ids': []}
        if self.generate_type == 'update':
            self.follow_contract_period = False
        else:
            self.follow_contract_period = True
        return res

    @api.depends('start_date', 'follow_contract_period', 'generate_type')
    def compute_current_date(self):
        for rec in self:
            if rec.generate_type != 'create_update' and rec.follow_contract_period == False and rec.start_date and rec.start_date < date.today():
                rec.is_past_date = True
            else:
                rec.is_past_date = False

    def clear_working_schedule(self):
        if self.generate_type == 'clear':
            if not self.is_past_date:
                for employee in self.employee_ids:
                    if self.follow_contract_period:
                        start_date = date.today()
                        end_date = employee.contract_id.date_end
                        if not employee.contract_id.date_end:
                            current_year = date.today().year
                            end_date = date(current_year, 12, 31)
                    else:
                        start_date = self.start_date
                        end_date = self.end_date
                    query_start_date = "'" + str(start_date) + "'"
                    query_end_date = "'" + str(end_date) + "'"
                    self.env.cr.execute("""delete from
                                                employee_working_schedule_calendar where employee_id=%d and date_start between 
                                                %s and %s""" % (
                        employee, query_start_date, query_end_date))
            else:
                raise ValidationError(
                    _("User cannot Update and delete for past dated employee working schedule record"))

    def action_generate(self):
        if self.generate_type == 'clear':
            self.clear_working_schedule()
        else:
            if self.is_past_date:
                raise ValidationError(
                    _("User cannot Update and delete for past dated employee working schedule record"))
            for rec in self:
                for employee in rec.employee_ids:
                    contract = employee.contract_id
                    if self.generate_type == 'update':
                        resource = self.resource_calendar_id
                    else:
                        resource = employee.contract_id.resource_calendar_id
                    holiday_list = []
                    weekend_list = []
                    if rec.follow_contract_period:
                        current_year = date.today().year
                        start_date = date(current_year, 1, 1)
                        end_date = contract.date_end
                        if not contract.date_end:
                            end_date = date(current_year, 12, 31)
                        if self.generate_type == 'update':
                            start_date = date.today()
                    else:
                        end_date = rec.end_date
                        if rec.start_date >= contract.date_start:
                            start_date = rec.start_date
                        else:
                            start_date = contract.date_start
                    if not resource.allow_public_holidays:
                        weekend_start_date = start_date
                        weekend_end_date = end_date
                        while weekend_start_date <= weekend_end_date:
                            if weekend_start_date.weekday() in (5, 6):
                                weekend_list.append(weekend_start_date)
                            weekend_start_date += relativedelta(days=1)
                        for holiday in resource.global_leave_ids:
                            holiday_start_date = holiday.date_from
                            holiday_end_date = holiday.date_to
                            while holiday_start_date <= holiday_end_date:
                                holiday_list.append(holiday_start_date)
                                holiday_start_date += relativedelta(days=1)
                    if start_date and end_date:
                        query_start_date = "'" + str(start_date) + "'"
                        query_end_date = "'" + str(end_date) + "'"
                        self.env.cr.execute("""delete from
                            employee_working_schedule_calendar where employee_id=%d and date_start between 
                            %s and %s""" % (
                            employee.id, query_start_date, query_end_date))
                        start = start_date
                        if resource.schedule == 'roster_schedule':
                            date_list = []
                            leave_days = 0
                            while start <= end_date:
                                leave_days += 1
                                if leave_days <= resource.week_working_day:
                                    if start not in holiday_list:
                                        date_list.append(start)
                                if leave_days == resource.week_working_day + resource.no_of_off_days:
                                    leave_days = 0
                                start += relativedelta(days=1)
                            val = 0
                            loop1 = []
                            loop2 = []
                            loop3 = []
                            for day in range((end_date - start_date).days + 1):
                                last_date_list = len(date_list) - 1
                                cycle = (resource.week_working_day * resource.number_of_variation) * val
                                work1 = cycle + resource.week_working_day - 1
                                if work1 < last_date_list:
                                    variation_working_ids = date_list[work1]
                                else:
                                    variation_working_ids = date_list[last_date_list]
                                variation_working_diff2 = date_list.index(variation_working_ids)
                                work2 = (variation_working_diff2 + resource.week_working_day)
                                if work2 < last_date_list:
                                    variation_working_ids_2 = date_list[work2]
                                else:
                                    variation_working_ids_2 = date_list[last_date_list]
                                variation_working_diff3 = date_list.index(variation_working_ids_2)
                                work3 = (variation_working_diff3 + resource.week_working_day)
                                if work3 < last_date_list:
                                    variation_working_ids_3 = date_list[work3]
                                else:
                                    variation_working_ids_3 = date_list[last_date_list]
                                var1 = 1
                                var2 = 1
                                var3 = 1
                                for dates in date_list:
                                    if resource.number_of_variation > 0:
                                        if variation_working_ids >= dates and dates not in loop1 and dates not in loop2 and dates not in loop3:
                                            for variation_x in resource.shift_pattern_line_ids:
                                                if variation_x.sequence == var1:
                                                    loop1.append(dates)
                                                    query_start_date = "'" + str(dates) + "'"
                                                    query_end_date = "'" + str(end_date) + "'"
                                                    if employee.department_id.id:
                                                        department = employee.department_id.id
                                                        active = True
                                                        dayofweek = dates.weekday()
                                                        self.env.cr.execute("""
                                                            INSERT
                                                            INTO
                                                            employee_working_schedule_calendar(
                                                            employee_id, contract_id, department_id, working_hours, dayofweek, date_start, 
                                                            date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                            )
                                                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                            employee.id, contract.id, department, resource.id,
                                                            dayofweek, query_start_date,
                                                            query_end_date, variation_x.start_time,
                                                            variation_x.end_time, variation_x.grace_time_for_late,
                                                            variation_x.break_from,
                                                            variation_x.break_to, active))
                                            var1 += 1
                                    if resource.number_of_variation > 1:
                                        if variation_working_ids_2 >= dates and dates not in loop1 and dates not in loop2 and dates not in loop3:
                                            for variation_x_2 in resource.shift_pattern_line_ids_2:
                                                if variation_x_2.sequence == var2:
                                                    loop2.append(dates)
                                                    query_start_date = "'" + str(dates) + "'"
                                                    query_end_date = "'" + str(end_date) + "'"
                                                    if employee.department_id.id:
                                                        department = employee.department_id.id
                                                        active = True
                                                        dayofweek = dates.weekday()
                                                        self.env.cr.execute("""
                                                            INSERT
                                                            INTO
                                                            employee_working_schedule_calendar(
                                                            employee_id, contract_id, department_id, working_hours, dayofweek, date_start, 
                                                            date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                            )
                                                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                            employee.id, contract.id, department, resource.id,
                                                            dayofweek, query_start_date,
                                                            query_end_date, variation_x_2.start_time,
                                                            variation_x_2.end_time,
                                                            variation_x_2.grace_time_for_late,
                                                            variation_x_2.break_from,
                                                            variation_x_2.break_to, active))
                                            var2 += 1
                                    if resource.number_of_variation > 2:
                                        if variation_working_ids_3 >= dates and dates not in loop1 and dates not in loop2 and dates not in loop3:
                                            for variation_x_3 in resource.shift_pattern_line_ids_3:
                                                if variation_x_3.sequence == var3:
                                                    loop3.append(dates)
                                                    query_start_date = "'" + str(dates) + "'"
                                                    query_end_date = "'" + str(end_date) + "'"
                                                    if employee.department_id.id:
                                                        department = employee.department_id.id
                                                        active = True
                                                        dayofweek = dates.weekday()
                                                        self.env.cr.execute("""
                                                            INSERT
                                                            INTO
                                                            employee_working_schedule_calendar(
                                                            employee_id, contract_id, department_id, working_hours, dayofweek, date_start, 
                                                            date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                            )
                                                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                            employee.id, contract.id, department, resource.id,
                                                            dayofweek, query_start_date,
                                                            query_end_date, variation_x_3.start_time,
                                                            variation_x_3.end_time,
                                                            variation_x_3.grace_time_for_late,
                                                            variation_x_3.break_from,
                                                            variation_x_3.break_to, active))
                                            var3 += 1
                                val += 1
                        else:
                            last_year_week_number = 0
                            last_year_month_number = 0
                            one_week = start_date
                            one_month = start_date
                            cycle_month = start_date.replace(day=1) + relativedelta(months=resource.number_of_variation)
                            month_count_1 = 0
                            month_count_2 = 0
                            month_count_3 = 0
                            month_count_4 = 0
                            month_count_5 = 0
                            month_count_6 = 0
                            month_count_7 = 0
                            while start_date <= end_date:
                                if start_date not in holiday_list and start_date not in weekend_list:
                                    if resource.schedule == 'fixed_schedule':
                                        for attendance in resource.attendance_ids:
                                            if str(start_date.weekday()) in attendance.dayofweek:
                                                query_start_date = "'" + str(start_date) + "'"
                                                query_end_date = "'" + str(end_date) + "'"
                                                if employee.department_id.id:
                                                    department = employee.department_id.id
                                                    active = True
                                                    self.env.cr.execute("""INSERT
                                                    INTO
                                                    employee_working_schedule_calendar(
                                                    employee_id, contract_id, department_id, working_hours, dayofweek, date_start, 
                                                    date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                    )
                                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                        employee.id, contract.id, department, resource.id,
                                                        attendance.dayofweek, query_start_date, query_end_date,
                                                        attendance.hour_from,
                                                        attendance.hour_to, attendance.grace_time_for_late,
                                                        attendance.break_from,
                                                        attendance.break_to, active))
                                    elif resource.schedule == 'shift_pattern':
                                        if resource.interval == 'weekly':
                                            current_day = calendar.day_name[start_date.weekday()]
                                            if start_date >= one_week:
                                                last_year_week_number += 1
                                                if current_day == 'Monday':
                                                    one_week = start_date + relativedelta(days=7)
                                                elif current_day == 'Tuesday':
                                                    one_week = start_date + relativedelta(days=6)
                                                elif current_day == 'Wednesday':
                                                    one_week = start_date + relativedelta(days=5)
                                                elif current_day == 'Thursday':
                                                    one_week = start_date + relativedelta(days=4)
                                                elif current_day == 'Friday':
                                                    one_week = start_date + relativedelta(days=3)
                                            weekdays_count = last_year_week_number
                                            loop_count = 0
                                            loop2_count = 0
                                            loop3_count = 0
                                            loop4_count = 0
                                            loop5_count = 0
                                            for day in range((end_date - start).days + 1):
                                                if resource.number_of_variation >= 1:
                                                    for shift_one in resource.shift_pattern_line_ids:
                                                        week_count1 = loop_count * resource.number_of_variation + 1
                                                        if str(start_date.weekday()) in shift_one.variation_name:
                                                            if weekdays_count == week_count1:
                                                                query_start_date = "'" + str(start_date) + "'"
                                                                query_end_date = "'" + str(end_date) + "'"
                                                                if employee.department_id.id:
                                                                    department = employee.department_id.id
                                                                    active = True
                                                                    self.env.cr.execute("""
                                                                    INSERT
                                                                    INTO
                                                                    employee_working_schedule_calendar(
                                                                    employee_id, contract_id, department_id, working_hours, dayofweek, date_start,
                                                                    date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                                    )
                                                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                                        employee.id, contract.id, department,
                                                                        resource.id,
                                                                        shift_one.variation_name, query_start_date,
                                                                        query_end_date, shift_one.start_time,
                                                                        shift_one.end_time,
                                                                        shift_one.grace_time_for_late,
                                                                        shift_one.break_from,
                                                                        shift_one.break_to, active))
                                                    loop_count += 1
                                                if resource.number_of_variation >= 2:
                                                    for shift_two in resource.shift_pattern_line_ids_2:
                                                        week_count2 = loop2_count * resource.number_of_variation + 2
                                                        if str(start_date.weekday()) in shift_two.variation_name:
                                                            if weekdays_count == week_count2:
                                                                query_start_date = "'" + str(start_date) + "'"
                                                                query_end_date = "'" + str(end_date) + "'"
                                                                if employee.department_id.id:
                                                                    department = employee.department_id.id
                                                                    active = True
                                                                    self.env.cr.execute("""
                                                                    INSERT
                                                                    INTO
                                                                    employee_working_schedule_calendar(
                                                                    employee_id, contract_id, department_id, working_hours, dayofweek, date_start,
                                                                    date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                                    )
                                                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                                        employee.id, contract.id, department,
                                                                        resource.id,
                                                                        shift_two.variation_name, query_start_date,
                                                                        query_end_date, shift_two.start_time,
                                                                        shift_two.end_time,
                                                                        shift_two.grace_time_for_late,
                                                                        shift_two.break_from,
                                                                        shift_two.break_to, active))
                                                    loop2_count += 1
                                                if resource.number_of_variation >= 3:
                                                    for shift_three in resource.shift_pattern_line_ids_3:
                                                        week_count3 = loop3_count * resource.number_of_variation + 3
                                                        if str(start_date.weekday()) in shift_three.variation_name:
                                                            if weekdays_count == week_count3:
                                                                query_start_date = "'" + str(start_date) + "'"
                                                                query_end_date = "'" + str(end_date) + "'"
                                                                if employee.department_id.id:
                                                                    department = employee.department_id.id
                                                                    active = True
                                                                    self.env.cr.execute("""
                                                                    INSERT
                                                                    INTO
                                                                    employee_working_schedule_calendar(
                                                                    employee_id, contract_id, department_id, working_hours, dayofweek, date_start,
                                                                    date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                                    )
                                                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                                        employee.id, contract.id, department,
                                                                        resource.id,
                                                                        shift_three.variation_name, query_start_date,
                                                                        query_end_date, shift_three.start_time,
                                                                        shift_three.end_time,
                                                                        shift_three.grace_time_for_late,
                                                                        shift_three.break_from,
                                                                        shift_three.break_to, active))
                                                    loop3_count += 1
                                                if resource.number_of_variation >= 4:
                                                    for shift_four in resource.shift_pattern_line_ids_4:
                                                        week_count4 = loop4_count * resource.number_of_variation + 4
                                                        if str(start_date.weekday()) in shift_four.variation_name:
                                                            if weekdays_count == week_count4:
                                                                query_start_date = "'" + str(start_date) + "'"
                                                                query_end_date = "'" + str(end_date) + "'"
                                                                if employee.department_id.id:
                                                                    department = employee.department_id.id
                                                                    active = True
                                                                    self.env.cr.execute("""
                                                                    INSERT
                                                                    INTO
                                                                    employee_working_schedule_calendar(
                                                                    employee_id, contract_id, department_id, working_hours, dayofweek, date_start, 
                                                                    date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                                    )
                                                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                                        employee.id, contract.id, department,
                                                                        resource.id,
                                                                        shift_four.variation_name, query_start_date,
                                                                        query_end_date, shift_four.start_time,
                                                                        shift_four.end_time,
                                                                        shift_four.grace_time_for_late,
                                                                        shift_four.break_from,
                                                                        shift_four.break_to, active))
                                                    loop4_count += 1
                                                if resource.number_of_variation >= 5:
                                                    for shift_five in resource.shift_pattern_line_ids_5:
                                                        week_count5 = loop5_count * resource.number_of_variation + 5
                                                        if str(start_date.weekday()) in shift_five.variation_name:
                                                            if weekdays_count == week_count5:
                                                                query_start_date = "'" + str(start_date) + "'"
                                                                query_end_date = "'" + str(end_date) + "'"
                                                                if employee.department_id.id:
                                                                    department = employee.department_id.id
                                                                    active = True
                                                                    self.env.cr.execute("""
                                                                    INSERT
                                                                    INTO
                                                                    employee_working_schedule_calendar(
                                                                    employee_id, contract_id, department_id, working_hours, dayofweek, date_start, 
                                                                    date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                                    )
                                                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                                        employee.id, contract.id, department,
                                                                        resource.id,
                                                                        shift_five.variation_name, query_start_date,
                                                                        query_end_date, shift_five.start_time,
                                                                        shift_five.end_time,
                                                                        shift_five.grace_time_for_late,
                                                                        shift_five.break_from,
                                                                        shift_five.break_to, active))
                                                    loop5_count += 1
                                        elif resource.interval == 'monthly':
                                            if start_date >= one_month:
                                                last_year_month_number += 1
                                                one_month = start_date.replace(day=1) + relativedelta(months=1)
                                            if start_date >= cycle_month:
                                                month_count_1 += 1
                                                month_count_2 += 1
                                                month_count_3 += 1
                                                month_count_4 += 1
                                                month_count_5 += 1
                                                month_count_6 += 1
                                                month_count_7 += 1
                                                cycle_month = start_date.replace(day=1) + relativedelta(
                                                    months=resource.number_of_variation)
                                            custom_month_number = last_year_month_number
                                            get_month_1 = month_count_1 * resource.number_of_variation + 1
                                            get_month_2 = month_count_2 * resource.number_of_variation + 2
                                            get_month_3 = month_count_3 * resource.number_of_variation + 3
                                            get_month_4 = month_count_3 * resource.number_of_variation + 3
                                            get_month_5 = month_count_3 * resource.number_of_variation + 3
                                            get_month_6 = month_count_3 * resource.number_of_variation + 3
                                            get_month_7 = month_count_3 * resource.number_of_variation + 3
                                            if custom_month_number == get_month_1:
                                                for variation_x in resource.shift_pattern_line_ids:
                                                    if str(start_date.weekday()) in variation_x.variation_name:
                                                        query_start_date = "'" + str(start_date) + "'"
                                                        query_end_date = "'" + str(end_date) + "'"
                                                        if employee.department_id.id:
                                                            department = employee.department_id.id
                                                            active = True
                                                            self.env.cr.execute("""
                                                            INSERT
                                                            INTO
                                                            employee_working_schedule_calendar(
                                                            employee_id, contract_id, department_id, working_hours, dayofweek, date_start, 
                                                            date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                            )
                                                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                                employee.id, contract.id, department, resource.id,
                                                                variation_x.variation_name, query_start_date,
                                                                query_end_date, variation_x.start_time,
                                                                variation_x.end_time, variation_x.grace_time_for_late,
                                                                variation_x.break_from,
                                                                variation_x.break_to, active))
                                            elif custom_month_number == get_month_2:
                                                for variation_x_2 in resource.shift_pattern_line_ids_2:
                                                    if resource.variation2:
                                                        if str(start_date.weekday()) in variation_x_2.variation_name:
                                                            query_start_date = "'" + str(start_date) + "'"
                                                            query_end_date = "'" + str(end_date) + "'"
                                                            if employee.department_id.id:
                                                                department = employee.department_id.id
                                                                active = True
                                                                self.env.cr.execute("""
                                                                    INSERT
                                                                    INTO
                                                                    employee_working_schedule_calendar(
                                                                    employee_id, contract_id, department_id, working_hours, dayofweek, date_start, 
                                                                    date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                                    )
                                                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                                    employee.id, contract.id, department, resource.id,
                                                                    variation_x_2.variation_name, query_start_date,
                                                                    query_end_date, variation_x_2.start_time,
                                                                    variation_x_2.end_time,
                                                                    variation_x_2.grace_time_for_late,
                                                                    variation_x_2.break_from,
                                                                    variation_x_2.break_to, active))
                                            elif custom_month_number == get_month_3:
                                                for variation_x_3 in resource.shift_pattern_line_ids_3:
                                                    if resource.variation3:
                                                        if str(start_date.weekday()) in variation_x_3.variation_name:
                                                            query_start_date = "'" + str(start_date) + "'"
                                                            query_end_date = "'" + str(end_date) + "'"
                                                            if employee.department_id.id:
                                                                department = employee.department_id.id
                                                                active = True
                                                                self.env.cr.execute("""
                                                                    INSERT
                                                                    INTO
                                                                    employee_working_schedule_calendar(
                                                                    employee_id, contract_id, department_id, working_hours, dayofweek, date_start, 
                                                                    date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                                    )
                                                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                                    employee.id, contract.id, department, resource.id,
                                                                    variation_x_3.variation_name, query_start_date,
                                                                    query_end_date, variation_x_3.start_time,
                                                                    variation_x_3.end_time,
                                                                    variation_x_3.grace_time_for_late,
                                                                    variation_x_3.break_from,
                                                                    variation_x_3.break_to, active))
                                            elif custom_month_number == get_month_4:
                                                for variation_x_4 in resource.shift_pattern_line_ids_4:
                                                    if resource.variation4:
                                                        if str(start_date.weekday()) in variation_x_4.variation_name:
                                                            query_start_date = "'" + str(start_date) + "'"
                                                            query_end_date = "'" + str(end_date) + "'"
                                                            if employee.department_id.id:
                                                                department = employee.department_id.id
                                                                active = True
                                                                self.env.cr.execute("""
                                                                    INSERT
                                                                    INTO
                                                                    employee_working_schedule_calendar(
                                                                    employee_id, contract_id, department_id, working_hours, dayofweek, date_start, 
                                                                    date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                                    )
                                                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                                    employee.id, contract.id, department, resource.id,
                                                                    variation_x_4.variation_name, query_start_date,
                                                                    query_end_date, variation_x_4.start_time,
                                                                    variation_x_4.end_time,
                                                                    variation_x_4.grace_time_for_late,
                                                                    variation_x_4.break_from,
                                                                    variation_x_4.break_to, active))
                                            elif custom_month_number == get_month_5:
                                                for variation_x_5 in resource.shift_pattern_line_ids_5:
                                                    if resource.variation4:
                                                        if str(start_date.weekday()) in variation_x_5.variation_name:
                                                            query_start_date = "'" + str(start_date) + "'"
                                                            query_end_date = "'" + str(end_date) + "'"
                                                            if employee.department_id.id:
                                                                department = employee.department_id.id
                                                                active = True
                                                                self.env.cr.execute("""
                                                                    INSERT
                                                                    INTO
                                                                    employee_working_schedule_calendar(
                                                                    employee_id, contract_id, department_id, working_hours, dayofweek, date_start, 
                                                                    date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                                    )
                                                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                                    employee.id, contract.id, department, resource.id,
                                                                    variation_x_5.variation_name, query_start_date,
                                                                    query_end_date, variation_x_5.start_time,
                                                                    variation_x_5.end_time,
                                                                    variation_x_5.grace_time_for_late,
                                                                    variation_x_5.break_from,
                                                                    variation_x_5.break_to, active))
                                            elif custom_month_number == get_month_6:
                                                for variation_x_6 in resource.shift_pattern_line_ids_6:
                                                    if resource.variation6:
                                                        if str(start_date.weekday()) in variation_x_6.variation_name:
                                                            query_start_date = "'" + str(start_date) + "'"
                                                            query_end_date = "'" + str(end_date) + "'"
                                                            if employee.department_id.id:
                                                                department = employee.department_id.id
                                                                active = True
                                                                self.env.cr.execute("""
                                                                    INSERT
                                                                    INTO
                                                                    employee_working_schedule_calendar(
                                                                    employee_id, contract_id, department_id, working_hours, dayofweek, date_start, 
                                                                    date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                                    )
                                                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                                    employee.id, contract.id, department, resource.id,
                                                                    variation_x_6.variation_name, query_start_date,
                                                                    query_end_date, variation_x_6.start_time,
                                                                    variation_x_6.end_time,
                                                                    variation_x_6.grace_time_for_late,
                                                                    variation_x_6.break_from,
                                                                    variation_x_6.break_to, active))
                                            elif custom_month_number == get_month_7:
                                                for variation_x_7 in resource.shift_pattern_line_ids_7:
                                                    if resource.variation7:
                                                        if str(start_date.weekday()) in variation_x_7.variation_name:
                                                            query_start_date = "'" + str(start_date) + "'"
                                                            query_end_date = "'" + str(end_date) + "'"
                                                            if employee.department_id.id:
                                                                department = employee.department_id.id
                                                                active = True
                                                                self.env.cr.execute("""
                                                                    INSERT
                                                                    INTO
                                                                    employee_working_schedule_calendar(
                                                                    employee_id, contract_id, department_id, working_hours, dayofweek, date_start, 
                                                                    date_end, hour_from, hour_to, tolerance_late, break_from, break_to, active
                                                                    )
                                                                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""" % (
                                                                    employee.id, contract.id, department, resource.id,
                                                                    variation_x_7.variation_name, query_start_date,
                                                                    query_end_date, variation_x_7.start_time,
                                                                    variation_x_7.end_time,
                                                                    variation_x_7.grace_time_for_late,
                                                                    variation_x_7.break_from,
                                                                    variation_x_7.break_to, active))

                                start_date += relativedelta(days=1)
                leaves = self.env['resource.calendar.leaves'].search([('resource_id', '=', False)])
                for holiday in leaves:
                    start_holiday = holiday.date_from
                    end_holiday = holiday.date_to
                    while start_holiday <= end_holiday:
                        leaves_exist = self.env['employee.working.schedule.calendar'].search(
                            [('date_start', '=', start_holiday), ('is_holiday', '=', True)])
                        if not leaves_exist:
                            self.env['employee.working.schedule.calendar'].create({
                                'employee_id': False,
                                'contract_id': False,
                                'department_id': False,
                                'working_hours': False,
                                'dayofweek': str(start_holiday.weekday()),
                                'date_start': start_holiday,
                                'date_end': start_holiday,
                                'hour_from': 0.01,
                                'hour_to': 23.99,
                                'is_holiday': True,
                                'holiday_remark': holiday.name,
                            })
                        start_holiday += relativedelta(days=1)
        self.state = 'generated'
