# -*- coding: utf-8 -*-

{
    'name': "Equip3 Sale Customer Portal",

    'summary': """
        Manage your customer portal""",

    'description': """
        This module manages these features :
        1. Quotes and Sales Order for Customer Portal
        2. Invoices for Customer Portal
        3. Delivery Order for Customer Portal
    """,

    "author": "Hashmicro/Prince",
    'category': 'Sales',
    'version': '1.1.4',

    # any module necessary for this one to work correctly
    "depends": ["website", "sale_stock", "portal"],

    # always loaded
    "data": [
        "security/ir.model.access.csv",
        'views/portal_templates.xml',
    ],
    "auto_install": False,
    "installable": True,
}
