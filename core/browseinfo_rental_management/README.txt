1) Version: 14.0.0.1 | Date : 18/05/2021
-->  Fixed the issue of invoice is not cancel automatically after close rental.
-->  Fixed the issue in open invoice from demo user.
-->  Fixed the issue in open delivery from demo user.

1) Version: 14.0.0.2 | Date : 24/05/2021
-->  Fixed the issue of renting more then one products. and access error for adjusting rental and saleable products with user login.

1) Version: 14.0.0.3 | Date : 9/07/2021

-->  fixed the issue in Rental Orders->in Rental Product give multiple products->replace products- it give error is solved 
-->  fixed the issue on validate delivery it gives error is solved 
-->  fixed the lot_id issue after replace the multi prodcut

version 14.0.0.4 (15/11/2021) :- "Improvements"
		- Added two fields i.e. weekly rental and daily rental on 'product.product' .
		- set rental value according to the selection for invoice [weekly,daily basis].

=> 14.0.0.5 : Add French, Spanish , Arabic and Dutch translation in module also improved an index.

version 14.0.0.6 (Dt. 13/01/2022) :- "Improvements" + "Fixes".
        - Improved start and end date fieldType date  to datetime.
        - Fixed rental flow for allowing hrs basis rental and also applied change on rental product lines.
        - Improved flow for replace rental products price_unit.
        - Removed Unwanted Code.
        - Added hourly rent field on rental product.
        - improved extend+ renewal functionality [added rental_initial_type + rental_initial field done proper
         calculations].
version 14.0.0.7 (Dt. 27/01/2022) :- "Improvements"
        - improved wizard validations for  rental terms and improved UI for extend wizard
version 14.0.0.8 (Dt. 18/02/2022)
        - fixed the rental order line product price value.
        - fixed cron issue[Expired Rental Email].
version 14.0.0.9 (Dt. 02/03/2022)
        - fixed replace product price issue
        - Improved code for rental history invoice amount
