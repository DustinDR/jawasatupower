# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class Lead(models.Model):
    _inherit = 'crm.lead'

    meeting_ids = fields.One2many('calendar.event', 'opportunity_id')
    one_metting = fields.Boolean(string="Had first meeting", compute="_compute_meeting", store=True)
    multiple_metting = fields.Boolean(string="Had multiple meetings", compute="_compute_meeting", store=True)
    type_id = fields.Many2one('crm.lead.type', string="Types")
    decision_maker = fields.Boolean(string="Decision Maker?")
    has_budget = fields.Boolean(string="Has Budget?")
    activity_data = fields.One2many(
        'mail.activity', 'res_id', 'Next Activity',
        auto_join=True,
        groups="base.group_user",)

    scale = fields.Selection([
        ('large', 'Large Company'),
        ('medium', 'Medium Company'),
        ('small', 'Small Company')
    ], string="Company Scale")
    einstein_score = fields.Float(string="Einstein Score", compute="_compute_einstein_score", store=True)
    einstein_score_text = fields.Html(string="Einstein Score Text", compute="_compute_einstein_score", store=True)
    probability_new = fields.Float(string="Probability", related="einstein_score", store=True)
    
    @api.onchange('stage_id')
    def _onchange_stage_id(self):
        if self.stage_id.is_won:
            self.partner_id.write({
                'is_leads' : False,
                'is_customer' : True,
                'customer_rank' : 1
            })

    @api.depends('has_budget','decision_maker', 'partner_name', 
                'street2', 'street', 'city', 'state_id', 'zip', 
                'country_id', 'website', 'scale', 'meeting_ids', 'meeting_ids.state',
                'order_ids', 'order_ids.state')
    def _compute_einstein_score(self):
        for rec in self:
            total = 0
            message = "<div class='einstein_score_text ml-5 mt-4'><p class='mb-4 top_positive'>Top Positives</p>"
            if rec.decision_maker:
                total += 10
                message += "<p>Decision Maker is True</p>"
            else:
                message += "<p>Decision Maker is False</p>"
            if rec.has_budget:
                total += 10
                message += "<p>Has Budget is True</p>"
            else:
                message += "<p>Has Budget is False</p>"
            all_fields_filled = 0
            if rec.partner_name:
                total += 7
                all_fields_filled += 1
            if rec.street2 or rec.street or rec.city or rec.state_id or rec.zip or rec.country_id:
                total += 7
                all_fields_filled += 1
            if rec.website:
                total += 6
                all_fields_filled += 1
            if all_fields_filled == 3:
                message += "<p>Company is Specified</p>"
            elif all_fields_filled == 1 or all_fields_filled == 2:
                message += "<p>Company is Almost Specified</p>"
            else:
                message += "<p>Company is Not Specified</p>"

            if rec.scale and rec.scale == "large":
                total += 20
                message += "<p>Company Scale is Large</p>"
            elif rec.scale and rec.scale == "medium":
                total += 15
                message += "<p>Company Scale is Medium</p>"
            elif rec.scale and rec.scale == "small":
                total += 10
                message += "<p>Company Scale is Small</p>"
            meeting_count = rec.meeting_ids.filtered(lambda r:r.state in ('meeting', 'done'))
            if len(meeting_count) == 1:
                total += 5
                message += "<p>1 Meeting</p>"
            elif len(meeting_count) == 2:
                total += 10
                message += "<p>2 Meetings</p>"
            elif len(meeting_count) == 3:
                total += 15
                message += "<p>3 Meetings</p>"
            elif len(meeting_count) > 3:
                total += 20
                message += "<p>%d Meetings</p>" %(len(meeting_count))
            if len(rec.order_ids.filtered(lambda r:r.state != 'cancel')) > 0:
                total += 20
                message += "<p>Quotation is Created</p>"
            message += "</div>"
            rec.einstein_score = total
            rec.einstein_score_text = message

    @api.depends('meeting_ids')
    def _compute_meeting(self):
        for rec in self:
            meeting_count = len(rec.meeting_ids)
            if meeting_count >= 1:
                rec.one_metting = True
            else:
                rec.one_metting = False
            if meeting_count > 1:
                rec.multiple_metting = True
            else:
                rec.multiple_metting = False

    # @api.model
    # def create(self, vals):
    #     res = super(Lead, self).create(vals)
    #     if res.partner_id:
    #         new_partner = res.partner_id.copy({
    #             'is_customer': False,
    #             'is_leads' : True,
    #             'customer_rank' : 0
    #         })
    #         res.partner_id = new_partner.id
    #     return res

    def action_set_won_rainbowman(self):
        res = super(Lead, self).action_set_won_rainbowman()
        if self.partner_id:
            self.partner_id.write({
                'is_leads' : False,
                'is_customer' : True,
                'customer_rank' : 1
            })
        return res

