# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class ResPartner(models.Model):
    _inherit = 'res.partner'

    lead_sequence = fields.Char(string="Leads ID", readonly=True, copy=False)

    @api.model
    def create(self, values):
        res = super(ResPartner, self).create(values)
        if res.is_leads == 1:
            sequence = self.env['ir.sequence'].next_by_code('res.partner.lead.sequence')
            res.write({
                'lead_sequence': sequence,
                'is_customer': False,
                'customer_rank': 0
            })
        return res

    
