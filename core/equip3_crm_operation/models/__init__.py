# -*- coding: utf-8 -*-

from . import ir_http
from . import models
from . import crm_lead
from . import crm_lead_type
from . import res_partner