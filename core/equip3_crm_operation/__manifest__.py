# -*- coding: utf-8 -*-
{
    'name': "Equip3 CRM Operation",

    'summary': """
          Manage Lead, Pipeline, Opportunity, Activity Operation""",

    'description': """
        This module manages these features :
        1. Leads 
        2. Pipelines 
        3. Opportunity 
        4. Activity 
    """,

    'author': "Hashmicro",
    'category': 'CRM',
    'version': '1.2.15',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'branch',
        'sale_crm',
        'app_crm_superbar',
        'base_geolocalize',
        'calendar',
        "equip3_crm_masterdata",
        'sales_team'
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence.xml',
        'data/crm_types_data.xml',
        'wizard/crm_lead_meeting_lost_views.xml',
        'wizard/crm_lead_meeting_reschedule_views.xml',
        "views/assets.xml",
        'views/views.xml',
        "views/calendar_event_views.xml",
        'views/templates.xml',
        'views/res_config_settings_views.xml',
        'views/crm_lead_view_updated.xml',
        'views/crm_lead_type_views.xml',
        'views/res_partner_views.xml',
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
    "auto_install": True,
}