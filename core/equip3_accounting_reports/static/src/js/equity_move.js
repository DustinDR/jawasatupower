odoo.define('equip3_accounting_reports.equity_move', function (require) {
    'use strict';
    var AbstractAction = require('web.AbstractAction');
    var core = require('web.core');
    var field_utils = require('web.field_utils');
    var rpc = require('web.rpc');
    var session = require('web.session');
    var utils = require('web.utils');
    var QWeb = core.qweb;
    var _t = core._t;

    window.click_num = 0;
    var FinancialRatio = AbstractAction.extend({
    template: 'EquityMoveTemp',
        events: {
            'click #apply_filter': 'apply_filter',
            'click #pdf': 'print_pdf',
            'click #xlsx': 'print_xlsx',
            'click .show-gl': 'show_gl',
        },

        init: function(parent, action) {
        this._super(parent, action);
                this.currency=action.currency;
                this.report_lines = action.report_lines;                
                this.wizard_id = action.context.wizard | null;
                this.account_equity_filtered = action.account_equity_filtered;
                this.account_equity = action.account_equity;
                this.account_retained_earnings = action.account_retained_earnings;
                this.account_current_earnings = action.account_current_earnings;
                this.account_prive = action.account_prive;
                this.records = action.records;
                
            },


          start: function() {
            var self = this;
            self.initial_render = true;
            rpc.query({
                model: 'account.equity.move',
                method: 'create',
                args: [{

                }]
            }).then(function(t_res) {
                self.wizard_id = t_res;
                self.load_data(self.initial_render);
            })
        },

        load_data: function (initial_render = true) {
            var self = this;
                self.$(".categ").empty();
                $('div.o_action_manager').css('overflow-y', 'auto');
                try{
                    var self = this;
                    self._rpc({
                        model: 'account.equity.move',
                        method: 'view_report',
                        args: [[this.wizard_id]],
                    }).then(function(datas) {
                            _.each(datas['report_lines'], function(rep_lines) {
                            rep_lines.debit = self.format_currency(datas['currency'],rep_lines.debit);
                            rep_lines.credit = self.format_currency(datas['currency'],rep_lines.credit);
                            rep_lines.balance = self.format_currency(datas['currency'],rep_lines.balance);
                            });

                            _.each(datas['account_equity_filtered'], function(rep_lines) {
                            rep_lines.debit = self.format_currency(datas['currency'],rep_lines.debit);
                            rep_lines.credit = self.format_currency(datas['currency'],rep_lines.credit);
                            rep_lines.balance = self.format_currency(datas['currency'],rep_lines.balance);
                            });

                            _.each(datas['account_equity'], function(rep_lines) {
                            rep_lines.debit = self.format_currency(datas['currency'],rep_lines.debit);
                            rep_lines.credit = self.format_currency(datas['currency'],rep_lines.credit);
                            rep_lines.balance = self.format_currency(datas['currency'],rep_lines.balance);
                            });
                            
                            _.each(datas['account_retained_earnings'], function(rep_lines) {
                            rep_lines.debit = self.format_currency(datas['currency'],rep_lines.debit);
                            rep_lines.credit = self.format_currency(datas['currency'],rep_lines.credit);
                            rep_lines.balance = self.format_currency(datas['currency'],rep_lines.balance);
                            });

                            _.each(datas['account_current_earnings'], function(rep_lines) {
                            rep_lines.debit = self.format_currency(datas['currency'],rep_lines.debit);
                            rep_lines.credit = self.format_currency(datas['currency'],rep_lines.credit);
                            rep_lines.balance = self.format_currency(datas['currency'],rep_lines.balance);
                            });

                            _.each(datas['account_prive'], function(rep_lines) {
                            rep_lines.debit = self.format_currency(datas['currency'],rep_lines.debit);
                            rep_lines.credit = self.format_currency(datas['currency'],rep_lines.credit);
                            rep_lines.balance = self.format_currency(datas['currency'],rep_lines.balance);
                            });
                            

                            if (initial_render) {
                                    self.$('.filter_view_tb').html(QWeb.render('EquityMoveFilterView', {
                                        filter_data: datas['filters'],
                                    }));
                                    self.$el.find('.target_move').select2({
                                        placeholder: 'Target Move...',
                                    });
                            }
                            var child=[];

                        self.$('.table_view_tb').html(QWeb.render('EquityMoveTable', {

                                            report_lines : datas['report_lines'],                                            
                                            filter : datas['filters'],
                                            currency : datas['currency'],
                                            credit_total : self.format_currency(datas['currency'],datas['credit_total']),
                                            debit_total : self.format_currency(datas['currency'],datas['debit_total']),
                                            balance_total : self.format_currency(datas['currency'],datas['balance_total']),
                                            account_equity_filtered : datas['account_equity_filtered'],
                                            account_equity : datas['account_equity'],
                                            account_retained_earnings : datas['account_retained_earnings'],
                                            account_current_earnings : datas['account_current_earnings'],
                                            account_prive : datas['account_prive'],
                                            records : datas['records'],
                                            
                                        }));
                });

                    }
                catch (el) {
                    window.location.href
                    }
        },

        format_currency: function(currency, amount) {
            if (typeof(amount) != 'number') {
                amount = parseFloat(amount);
            }
            var formatted_value = (parseInt(amount)).toLocaleString(currency[2],{
                minimumFractionDigits: 2
            })
            return formatted_value
        },

        show_gl: function(e) {
            var self = this;
            var account_id = $(e.target).attr('data-account-id');
            var options = {
                account_ids: [account_id],
            }

                var action = {
                    type: 'ir.actions.client',
                    name: 'GL View',
                    tag: 'g_l',
                    target: 'new',
                    domain: [['account_ids','=', account_id]],
                }
                return this.do_action(action);

        },

        print_pdf: function(e) {
            e.preventDefault();
            var self = this;
            self._rpc({
                model: 'account.equity.move',
                method: 'view_report',
                args: [
                    [self.wizard_id]
                ],
            }).then(function(data) {
                var action = {
                    'type': 'ir.actions.report',
                    'report_type': 'qweb-pdf',
                    'report_name': 'equip3_accounting_reports.equity_move',
                    'report_file': 'equip3_accounting_reports.equity_move',
                    'data': {
                        'report_data': data
                    },
                    'context': {
                        'active_model': 'account.equity.move',
                        'landscape': 1,
                        'equity_move_pdf_report': true
                    },
                    'display_name': 'Equity Movement',
                };
                return self.do_action(action);
            });
        },

        print_xlsx: function() {
            var self = this;
            self._rpc({
                model: 'account.equity.move',
                method: 'view_report',
                args: [
                    [self.wizard_id]
                ],
            }).then(function(data) {
                var action = {
                    'type': 'ir_actions_dynamic_xlsx_download',
                    'data': {
                            'model': 'account.equity.move',
                            'options': JSON.stringify(data['filters']),
                            'output_format': 'xlsx',
                            'report_data': JSON.stringify(data['records']),
                            'report_name': 'Equity Movement',
                            'dfr_data': JSON.stringify(data),

                    },
                };
                return self.do_action(action);
            });
        },

        apply_filter: function(event) {

            event.preventDefault();
            var self = this;
            self.initial_render = false;
            var filter_data_selected = {};

            if ($("#date_from").val()) {
                var dateString = $("#date_from").val();
                filter_data_selected.date_from = dateString;
            }
            if ($("#date_to").val()) {
                var dateString = $("#date_to").val();
                filter_data_selected.date_to = dateString;
            }

            if ($(".target_move").length) {
            var post_res = document.getElementById("post_res")
            filter_data_selected.target_move = $(".target_move")[1].value
            post_res.value = $(".target_move")[1].value
                    post_res.innerHTML=post_res.value;
              if ($(".target_move")[1].value == "") {
              post_res.innerHTML="posted";

              }
            }
            rpc.query({
                model: 'account.equity.move',
                method: 'write',
                args: [
                    self.wizard_id, filter_data_selected
                ],
            }).then(function(res) {
            self.initial_render = false;
                self.load_data(self.initial_render);
            });
        },

    });
    core.action_registry.add("e_m", FinancialRatio);
    return FinancialRatio;
});