odoo.define('equip3_accounting_reports.tax_report', function (require) {
    'use strict';

    var AbstractAction = require('web.AbstractAction');
    var core = require('web.core');
    var field_utils = require('web.field_utils');
    var rpc = require('web.rpc');
    var session = require('web.session');
    var utils = require('web.utils');
    var QWeb = core.qweb;
    var _t = core._t;

    window.click_num = 0;
    var TaxReport = AbstractAction.extend({
    template: 'TaxReportView',
        events: {
            'click .filter_date': '_onFilterDate',
            'click .filter_currency': '_onFilterCurrency',
            'click .o_add_custom_filter': '_onCustomFilter',
            'click .view-account-move': 'view_acc_move',
            'click #apply_filter': 'apply_filter',
        },
        start: function() {
            var self = this;
            self.initial_render = true;
            rpc.query({
                model: 'tax.report',
                method: 'create',
                args: [{
                }]
            }).then(function(t_res) {
                self.wizard_id = t_res;
                self.load_data(self.initial_render);
            })
        },
        _onFilterDate: function(ev) {
            ev.preventDefault();
            $(ev.target).parents().find('ul.o_date_filter').find('li > a.selected').removeClass('selected');
            if (!$('.o_account_reports_custom-dates').hasClass('d-none')) {
                $('.o_account_reports_custom-dates').addClass('d-none');
            }
            if ($(ev.target).is('a')) {
                $(ev.target).addClass('selected');
            }
            else {
                $(ev.target).find('a').addClass('selected');
            }
            var title = $(ev.target).parents().find('ul.o_date_filter').find('li > a.selected').parent().attr('title');
            $('.date_caret').text(title);
        },
        _onFilterCurrency: function(ev) {
            ev.preventDefault();
            $(ev.target).parents().find('ul.o_currency_filter').find('li > a.selected').removeClass('selected');
            if ($(ev.target).is('a')) {
                $(ev.target).addClass('selected');
            }
            else {
                $(ev.target).find('a').addClass('selected');
            }
            var title = $(ev.target).parents().find('ul.o_currency_filter').find('li > a.selected').parent().attr('title');
            $('.currency_caret').text(title);
        },
        _onCustomFilter: function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            $(ev.target).parents().find('ul.o_filters_menu').find('li > a.selected').removeClass('selected');
            var custom_dates = $(ev.target).parents().find('ul.o_filters_menu').find('.o_account_reports_custom-dates');
            if (custom_dates.hasClass('d-none')) {
                custom_dates.removeClass('d-none');
                $('.date_caret').text('Custom');
            } else {
                custom_dates.addClass('d-none');
            }
        },
        load_data: function (initial_render=true) {
            var self = this;
            self.$(".categ").empty();
            $('div.o_action_manager').css('overflow-y', 'auto');
            try{
                var self = this;
                self._rpc({
                    model: 'tax.report',
                    method: 'view_report',
                    args: [[this.wizard_id]],
                }).then(function(datas) {
                    if (initial_render) {
                        self.$('.filter_view_tb').html(QWeb.render('TaxReportFilterView', {
                            filter_data: datas['filters'],
                            currencies: datas['currencies'],
                        }));
                        $('.filter_date[data-value="this_month"]').click();
                        self.$el.find('.account').select2({
                            placeholder: ' Accounts...',
                        });
                        self.$el.find('.target_move').select2({
                            placeholder: 'Target Move...',
                        });
                        self.$el.find('.companies').select2({
                            placeholder: 'Companies...',
                        });
                        self.$el.find('.taxes').select2({
                            placeholder: 'Taxes...',
                        });
                    }
                    self.$('.table_view_tb').html(QWeb.render('TaxReportTable', {
                        report_lines : datas['report_lines'],
                        currency : datas['currency'],
                        total_lines: datas['total_lines'],
                    }));
                });
            }
            catch (el) {
                window.location.href
            }
        },
        view_acc_move: function(event) {
            event.preventDefault();
            var self = this;
            var context = {};
            var show_acc_move = function(res_model, res_id, view_id) {
                var action = {
                    type: 'ir.actions.act_window',
                    view_type: 'form',
                    view_mode: 'form',
                    res_model: res_model,
                    views: [
                        [view_id || false, 'form']
                    ],
                    res_id: res_id,
                    target: 'current',
                    context: context,
                };
                return self.do_action(action);
            };
            rpc.query({
                    model: 'account.move',
                    method: 'search_read',
                    domain: [
                        ['id', '=', $(event.currentTarget).data('move-id')]
                    ],
                    fields: ['id'],
                    limit: 1,
                })
                .then(function(record) {
                    if (record.length > 0) {
                        show_acc_move('account.move', record[0].id);
                    } else {
                        show_acc_move('account.move', $(event.currentTarget).data('move-id'));
                    }
                });
        },

        apply_filter: function(event) {
            event.preventDefault();
            var self = this;
            self.initial_render = false;
            var filter_data_selected = {};

            var account_ids = [];
            var account_text = [];
            var span_res = document.getElementById("account_res")
            var account_list = $(".account").select2('data')
            for (var i = 0; i < account_list.length; i++) {
                if(account_list[i].element[0].selected === true)
                    {account_ids.push(parseInt(account_list[i].id))
                if(account_text.includes(account_list[i].text) === false)
                    {account_text.push(account_list[i].text)
                }
                span_res.value = account_text
                span_res.innerHTML=span_res.value;
                }
            }
            if (account_list.length == 0){
                span_res.value = ""
                span_res.innerHTML=""; 
            }
            filter_data_selected.account_ids = account_ids

            var company_ids = [];
            var company_text = [];
            var span_res = document.getElementById("company_res")
            var company_list = $(".companies").select2('data')
            for (var i = 0; i < company_list.length; i++) {
                if(company_list[i].element[0].selected === true)
                    {company_ids.push(parseInt(company_list[i].id))
                if(company_text.includes(company_list[i].text) === false)
                    {company_text.push(company_list[i].text)
                }
                span_res.value = company_text
                span_res.innerHTML=span_res.value;
                }
            }
            if (company_list.length == 0){
                span_res.value = ""
                span_res.innerHTML=""; 
            }
            filter_data_selected.company_ids = company_ids

            var tax_ids = [];
            var tax_text = [];
            var span_res = document.getElementById("tax_res")
            var tax_list = $(".taxes").select2('data')
            for (var i = 0; i < tax_list.length; i++) {
                if(tax_list[i].element[0].selected === true)
                    {tax_ids.push(parseInt(tax_list[i].id))
                if(tax_text.includes(tax_list[i].text) === false)
                    {tax_text.push(tax_list[i].text)
                }
                span_res.value = tax_text
                span_res.innerHTML=span_res.value;
                }
            }
            if (tax_list.length == 0){
                span_res.value = ""
                span_res.innerHTML=""; 
            }
            filter_data_selected.tax_ids = tax_ids

            var dt;
            var list_item_selected = $('ul.o_date_filter').find('li > a.selected');
            var currency_item_selected = $('ul.o_currency_filter').find('li > a.selected');
            filter_data_selected.date_from = "";
            filter_data_selected.date_to = "";
            if (list_item_selected.length) {
                var filter_value = $('ul.o_date_filter').find('li > a.selected').parent().data('value');
                if (filter_value == "this_month") {
                    dt = new Date();
                    dt.setDate(1);
                    filter_data_selected.date_from = moment(dt).format('YYYY-MM-DD');
                    dt.setMonth(dt.getMonth() + 1);
                    dt.setDate(0);
                    filter_data_selected.date_to = moment(dt).format('YYYY-MM-DD');
                }
                else if (filter_value == "this_quarter") {
                    dt = new moment();
                    filter_data_selected.date_from = dt.startOf('quarter').format('YYYY-MM-DD');
                    filter_data_selected.date_to = dt.endOf('quarter').format('YYYY-MM-DD');
                    
                }
                else if (filter_value == "this_financial_year") {
                    dt = new Date();
                    var year = dt.getFullYear();
                    filter_data_selected.date_from = moment([year]).startOf('year').format('YYYY-MM-DD');
                    filter_data_selected.date_to = moment([year]).endOf('year').format('YYYY-MM-DD');
                }
                else if (filter_value == "last_month") {
                    dt = new Date();
                    dt.setDate(0);
                    filter_data_selected.date_to = moment(dt).format('YYYY-MM-DD');
                    dt.setDate(1);
                    filter_data_selected.date_from = moment(dt).format('YYYY-MM-DD');
                }
                else if (filter_value == "last_quarter") {
                    dt = new Date();
                    dt.setMonth((moment(dt).quarter() - 1) * 3); // Go to the first month of this quarter
                    dt.setDate(0); // Then last day of last month (= last day of last quarter)
                    filter_data_selected.date_to = moment(dt).format('YYYY-MM-DD');
                    dt.setDate(1);
                    dt.setMonth(dt.getMonth() - 2);
                    filter_data_selected.date_from = moment(dt).format('YYYY-MM-DD');
                }
                else if (filter_value == "last_year") {
                    dt = new Date();
                    var year = dt.getFullYear() - 1;
                    filter_data_selected.date_to = moment([year]).endOf('year').format('YYYY-MM-DD');
                    filter_data_selected.date_from = moment([year]).startOf('year').format('YYYY-MM-DD');
                }
            }
            else if (list_item_selected.length == 0) {
                if ($("#date_from").val()) {
                    var dateString = $("#date_from").val();
                    filter_data_selected.date_from = dateString;
                }
                if ($("#date_to").val()) {
                    var dateString = $("#date_to").val();
                    filter_data_selected.date_to = dateString;
                }
            }
            if (currency_item_selected.length) {
                var currency_value = $('ul.o_currency_filter').find('li > a.selected').parent().data('value');
                filter_data_selected.report_currency_id = currency_value;
            }

            
            if ($(".target_move").length) {
                var post_res = document.getElementById("post_res")
                filter_data_selected.target_move = $(".target_move")[1].value
                post_res.value = $(".target_move")[1].value
                post_res.innerHTML=post_res.value;
                if ($(".target_move")[1].value == "") {
                    post_res.innerHTML="posted";
                }
            }
            rpc.query({
                model: 'tax.report',
                method: 'write',
                args: [
                    self.wizard_id, filter_data_selected
                ],
            }).then(function(res) {
            self.initial_render = false;
                self.load_data(self.initial_render);
            });
        },
    });
    core.action_registry.add("account_tax_report", TaxReport);
    return TaxReport;
});
