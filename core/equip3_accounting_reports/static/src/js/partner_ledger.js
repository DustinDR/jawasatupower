odoo.define('equip3_accounting_reports.partner_ledger', function (require) {
    'use strict';

    var PartnerLedger = require('dynamic_accounts_report.partner_ledger');
    var core = require('web.core');
    var rpc = require('web.rpc');
    var QWeb = core.qweb;
    var _t = core._t;

    window.click_num = 0;

    PartnerLedger.include({
        events: _.extend({}, PartnerLedger.prototype.events, {
            'click .filter_date': '_onFilterDate',
            'click .filter_currency': '_onFilterCurrency',
            'click .o_add_custom_filter': '_onCustomFilter'
        }),
        _onFilterDate: function(ev) {
            ev.preventDefault();
            $(ev.target).parents().find('ul.o_date_filter').find('li > a.selected').removeClass('selected');
            if (!$('.o_account_reports_custom-dates').hasClass('d-none')) {
                $('.o_account_reports_custom-dates').addClass('d-none');
            }
            if ($(ev.target).is('a')) {
                $(ev.target).addClass('selected');
            }
            else {
                $(ev.target).find('a').addClass('selected');
            }
            var title = $(ev.target).parents().find('ul.o_date_filter').find('li > a.selected').parent().attr('title');
            $('.date_caret').text(title);
        },
        _onFilterCurrency: function(ev) {
            ev.preventDefault();
            $(ev.target).parents().find('ul.o_currency_filter').find('li > a.selected').removeClass('selected');
            if ($(ev.target).is('a')) {
                $(ev.target).addClass('selected');
            }
            else {
                $(ev.target).find('a').addClass('selected');
            }
            var title = $(ev.target).parents().find('ul.o_currency_filter').find('li > a.selected').parent().attr('title');
            $('.currency_caret').text(title);
        },
        _onCustomFilter: function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            $(ev.target).parents().find('ul.o_filters_menu').find('li > a.selected').removeClass('selected');
            var custom_dates = $(ev.target).parents().find('ul.o_filters_menu').find('.o_account_reports_custom-dates');
            if (custom_dates.hasClass('d-none')) {
                custom_dates.removeClass('d-none');
                $('.date_caret').text('Custom');
            } else {
                custom_dates.addClass('d-none');
            }
        },
        load_data: function (initial_render = true) {
            var self = this;
            self.$(".categ").empty();
            $('div.o_action_manager').css('overflow-y', 'auto');
            try{
                var self = this;
                self._rpc({
                    model: 'account.partner.ledger',
                    method: 'view_report',
                    args: [[this.wizard_id]],
                }).then(function(datas) {
                    _.each(datas['report_lines'], function(rep_lines) {
                        rep_lines.debit = self.format_currency(datas['currency'],rep_lines.debit);
                        rep_lines.credit = self.format_currency(datas['currency'],rep_lines.credit);
                        rep_lines.balance = self.format_currency(datas['currency'],rep_lines.balance);
                    });
                    if (initial_render) {
                        self.$('.filter_view_tb').html(QWeb.render('PLFilterView', {
                            filter_data: datas['filters'],
                            currencies: datas['currencies'],
                        }));
                        self.$el.find('.journals').select2({
                            placeholder: ' Journals...',
                        });

                        self.$el.find('.account').select2({
                            placeholder: ' Accounts...',
                        });
                        self.$el.find('.partners').select2({
                        placeholder: 'Partners...',
                        });
                        self.$el.find('.reconciled').select2({
                        placeholder: 'Reconciled status...',
                        });
                        self.$el.find('.type').select2({
                        placeholder: 'Account Type...',
                        });
                        self.$el.find('.category').select2({
                        placeholder: 'Partner Tag...',
                        });
                        self.$el.find('.acc').select2({
                        placeholder: 'Select Acc...',
                        });
                        self.$el.find('.target_move').select2({
                                    placeholder: 'Target Move...',
                                });
                    }
                    var child=[];

                    self.$('.table_view_tb').html(QWeb.render('PLTable', {
                        report_lines : datas['report_lines'],
                        filter : datas['filters'],
                        currency : datas['currency'],
                        credit_total : datas['credit_total'],
                        debit_total : datas['debit_total'],
                        debit_balance : datas['debit_balance'],
                        currencies: datas['currencies']
                    }));
                });
            }
            catch (el) {
                window.location.href
            }
        },
        apply_filter: function(event) {
            event.preventDefault();
            var self = this;
            self.initial_render = false;
            var filter_data_selected = {};

            var account_ids = [];
            var account_text = [];
            var span_res = document.getElementById("account_res")
            var account_list = $(".account").select2('data')
            for (var i = 0; i < account_list.length; i++) {
                if(account_list[i].element[0].selected === true)
                    {account_ids.push(parseInt(account_list[i].id))
                if(account_text.includes(account_list[i].text) === false)
                    {account_text.push(account_list[i].text)
                }
                span_res.value = account_text
                span_res.innerHTML=span_res.value;
                }
            }
            if (account_list.length == 0){
            span_res.value = ""
            span_res.innerHTML=""; }
            filter_data_selected.account_ids = account_ids


            var journal_ids = [];
            var journal_text = [];
            var journal_res = document.getElementById("journal_res")
            var journal_list = $(".journals").select2('data')
            for (var i = 0; i < journal_list.length; i++) {
                if(journal_list[i].element[0].selected === true){
                    journal_ids.push(parseInt(journal_list[i].id))
                    if(journal_text.includes(journal_list[i].text) === false){
                        journal_text.push(journal_list[i].text)
                    }
                    journal_res.value = journal_text
                    journal_res.innerHTML=journal_res.value;
                }
            }
            if (journal_list.length == 0){
               journal_res.value = ""
                    journal_res.innerHTML="";
            }
            filter_data_selected.journal_ids = journal_ids

            var partner_ids = [];
            var partner_text = [];
            var span_res = document.getElementById("partner_res")
            var partner_list = $(".partners").select2('data')
            for (var i = 0; i < partner_list.length; i++) {
            if(partner_list[i].element[0].selected === true)
            {partner_ids.push(parseInt(partner_list[i].id))
            if(partner_text.includes(partner_list[i].text) === false)
            {partner_text.push(partner_list[i].text)
            }
            span_res.value = partner_text
            span_res.innerHTML=span_res.value;
            }
            }
            if (partner_list.length == 0){
            span_res.value = ""
            span_res.innerHTML="";
            }
            filter_data_selected.partner_ids = partner_ids

            var account_type_ids = [];
            var account_type_ids_text = [];
            var span_res = document.getElementById("type_res")
            var type_list = $(".type").select2('data')
            for (var i = 0; i < type_list.length; i++) {
            if(type_list[i].element[0].selected === true)
            {account_type_ids.push(parseInt(type_list[i].id))
            if(account_type_ids_text.includes(type_list[i].text) === false)
            {account_type_ids_text.push(type_list[i].text)
            }
            span_res.value = account_type_ids_text
            span_res.innerHTML=span_res.value;
            }
            }
            if (type_list.length == 0){
            span_res.value = ""
            span_res.innerHTML="";
            }
            filter_data_selected.account_type_ids = account_type_ids

            var partner_category_ids = [];
            var partner_category_text = [];
            var span_res = document.getElementById("category_res")
            var category_list = $(".category").select2('data')
            for (var i = 0; i < category_list.length; i++) {
            if(category_list[i].element[0].selected === true)
            {partner_category_ids.push(parseInt(category_list[i].id))
            if(partner_category_text.includes(category_list[i].text) === false)
            {partner_category_text.push(category_list[i].text)
            }
            span_res.value = partner_category_text
            span_res.innerHTML=span_res.value;
            }
            }
            if (category_list.length == 0){
            span_res.value = ""
            span_res.innerHTML="";
            }
            filter_data_selected.partner_category_ids = partner_category_ids
            var dt;
            var list_item_selected = $('ul.o_date_filter').find('li > a.selected');
            var currency_item_selected = $('ul.o_currency_filter').find('li > a.selected');
            filter_data_selected.date_from = "";
            filter_data_selected.date_to = "";
            if (list_item_selected.length) {
                var filter_value = $('ul.o_date_filter').find('li > a.selected').parent().data('value');
                if (filter_value == "this_month") {
                    dt = new Date();
                    dt.setDate(1);
                    filter_data_selected.date_from = moment(dt).format('YYYY-MM-DD');
                    dt.setMonth(dt.getMonth() + 1);
                    dt.setDate(0);
                    filter_data_selected.date_to = moment(dt).format('YYYY-MM-DD');
                }
                else if (filter_value == "this_quarter") {
                    dt = new moment();
                    filter_data_selected.date_from = dt.startOf('quarter').format('YYYY-MM-DD');
                    filter_data_selected.date_to = dt.endOf('quarter').format('YYYY-MM-DD');
                    
                }
                else if (filter_value == "this_financial_year") {
                    dt = new Date();
                    var year = dt.getFullYear();
                    filter_data_selected.date_from = moment([year]).startOf('year').format('YYYY-MM-DD');
                    filter_data_selected.date_to = moment([year]).endOf('year').format('YYYY-MM-DD');
                }
                else if (filter_value == "last_month") {
                    dt = new Date();
                    dt.setDate(0);
                    filter_data_selected.date_to = moment(dt).format('YYYY-MM-DD');
                    dt.setDate(1);
                    filter_data_selected.date_from = moment(dt).format('YYYY-MM-DD');
                }
                else if (filter_value == "last_quarter") {
                    dt = new Date();
                    dt.setMonth((moment(dt).quarter() - 1) * 3); // Go to the first month of this quarter
                    dt.setDate(0); // Then last day of last month (= last day of last quarter)
                    filter_data_selected.date_to = moment(dt).format('YYYY-MM-DD');
                    dt.setDate(1);
                    dt.setMonth(dt.getMonth() - 2);
                    filter_data_selected.date_from = moment(dt).format('YYYY-MM-DD');
                }
                else if (filter_value == "last_year") {
                    dt = new Date();
                    var year = dt.getFullYear() - 1;
                    filter_data_selected.date_to = moment([year]).endOf('year').format('YYYY-MM-DD');
                    filter_data_selected.date_from = moment([year]).startOf('year').format('YYYY-MM-DD');
                }
            }
            else if (list_item_selected.length == 0) {
                if ($("#date_from").val()) {
                    var dateString = $("#date_from").val();
                    filter_data_selected.date_from = dateString;
                }
                if ($("#date_to").val()) {
                    var dateString = $("#date_to").val();
                    filter_data_selected.date_to = dateString;
                }
            }
            if (currency_item_selected.length) {
                var currency_value = $('ul.o_currency_filter').find('li > a.selected').parent().data('value');
                filter_data_selected.report_currency_id = currency_value;
            }
            if ($(".reconciled").length){
                var reconciled_res = document.getElementById("reconciled_res");
                filter_data_selected.reconciled = $(".reconciled")[0].value;
                reconciled_res.value = $(".reconciled")[0].value;
                reconciled_res.innerHTML=reconciled_res.value;
                if ($(".reconciled").value==""){
                    reconciled_res.innerHTML="unreconciled";
                    filter_data_selected.reconciled = "unreconciled"
                }
            }

            if ($(".target_move").length) {
                var post_res = document.getElementById("post_res")
                filter_data_selected.target_move = $(".target_move")[1].value
                post_res.value = $(".target_move")[1].value
                post_res.innerHTML=post_res.value;
                if ($(".target_move")[1].value == "") {
                    post_res.innerHTML="posted";
                }
            }
            rpc.query({
                model: 'account.partner.ledger',
                method: 'write',
                args: [
                    self.wizard_id, filter_data_selected
                ],
            }).then(function(res) {
            self.initial_render = false;
                self.load_data(self.initial_render);
            });
        },
    });

});