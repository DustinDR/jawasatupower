odoo.define('equip3_accounting_reports.ageing', function (require) {
    'use strict';

    var PartnerAgeing = require('dynamic_accounts_report.ageing');
    var core = require('web.core');
    var rpc = require('web.rpc');
    var QWeb = core.qweb;
    var _t = core._t;

    PartnerAgeing.include({
        events: _.extend({}, PartnerAgeing.prototype.events, {
            'click .filter_date': '_onFilterDate',
            'click .filter_currency': '_onFilterCurrency',
            'click .o_add_custom_filter': '_onCustomFilter'
        }),
        _onFilterDate: function(ev) {
            ev.preventDefault();
            $(ev.target).parents().find('ul.o_date_filter').find('li > a.selected').removeClass('selected');
            if (!$('.o_account_reports_custom-dates').hasClass('d-none')) {
                $('.o_account_reports_custom-dates').addClass('d-none');
            }
            if ($(ev.target).is('a')) {
                $(ev.target).addClass('selected');
            }
            else {
                $(ev.target).find('a').addClass('selected');
            }
            var title = $(ev.target).parents().find('ul.o_date_filter').find('li > a.selected').parent().attr('title');
            $('.date_caret').text(title);
        },
        _onFilterCurrency: function(ev) {
            ev.preventDefault();
            $(ev.target).parents().find('ul.o_currency_filter').find('li > a.selected').removeClass('selected');
            if ($(ev.target).is('a')) {
                $(ev.target).addClass('selected');
            }
            else {
                $(ev.target).find('a').addClass('selected');
            }
            var title = $(ev.target).parents().find('ul.o_currency_filter').find('li > a.selected').parent().attr('title');
            $('.currency_caret').text(title);
        },
        _onCustomFilter: function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            $(ev.target).parents().find('ul.o_filters_menu').find('li > a.selected').removeClass('selected');
            var custom_dates = $(ev.target).parents().find('ul.o_filters_menu').find('.o_account_reports_custom-dates');
            if (custom_dates.hasClass('d-none')) {
                custom_dates.removeClass('d-none');
                $('.date_caret').text('Custom');
                
            } else {
                custom_dates.addClass('d-none');
            }
        },
        load_data: function (initial_render = true) {
            var self = this;
            self.$(".categ").empty();
            $('div.o_action_manager').css('overflow-y', 'auto');
            try{
                var self = this;
                self._rpc({
                    model: 'account.partner.ageing',
                    method: 'view_report',
                    args: [[this.wizard_id]],
                }).then(function(datas) {
                _.each(datas['report_lines'][0], function(rep_lines) {
                    rep_lines.total = self.format_currency(datas['currency'],rep_lines.total);
                    rep_lines[4] = self.format_currency(datas['currency'],rep_lines[4]);
                    rep_lines[3] = self.format_currency(datas['currency'],rep_lines[3]);
                    rep_lines[2] = self.format_currency(datas['currency'],rep_lines[2]);
                    rep_lines[1] = self.format_currency(datas['currency'],rep_lines[1]);
                    rep_lines[0] = self.format_currency(datas['currency'],rep_lines[0]);

                    rep_lines['direction'] = self.format_currency(datas['currency'],rep_lines['direction']);

                     });

                    if (initial_render) {
                            self.$('.filter_view_tb').html(QWeb.render('AgeingFilterView', {
                                filter_data: datas['filters'],
                                currencies: datas['currencies'],
                            }));
                            self.$el.find('.partners').select2({
                                placeholder: ' Partners...',
                            });
                            self.$el.find('.category').select2({
                                placeholder: ' Partner Category...',
                            });
                            self.$el.find('.target_move').select2({
                                placeholder: ' Target Move...',
                            });
                            self.$el.find('.result_selection').select2({
                                placeholder: ' Account Type...',
                            });

                    }
                var child=[];
                self.$('.table_view_tb').html(QWeb.render('Ageingtable', {
                    report_lines : datas['report_lines'],
                    move_lines :datas['report_lines'][2],
                    filter : datas['filters'],
                    currency : datas['currency'],
                    currencies: datas['currencies']
                }));
            });
            }
            catch (el) {
                window.location.href
            }
        },
        apply_filter: function(event) {
            event.preventDefault();
            var self = this;
            self.initial_render = false;

            var filter_data_selected = {};

            var dt;
            var list_item_selected = $('ul.o_date_filter').find('li > a.selected');
            var currency_item_selected = $('ul.o_currency_filter').find('li > a.selected');
            if (list_item_selected.length) {
                var filter_value = $('ul.o_date_filter').find('li > a.selected').parent().data('value');
                if (filter_value == "today") {
                    filter_data_selected.date_from = moment().format('YYYY-MM-DD');
                }
                else if (filter_value == "last_month") {
                    dt = new Date();
                    dt.setDate(0);
                    filter_data_selected.date_from = moment(dt).format('YYYY-MM-DD');
                }
                else if (filter_value == "last_quarter") {
                    dt = new Date();
                    dt.setMonth((moment(dt).quarter() - 1) * 3); // Go to the first month of this quarter
                    dt.setDate(0); // Then last day of last month (= last day of last quarter)
                    filter_data_selected.date_from = moment(dt).format('YYYY-MM-DD');
                }
                else if (filter_value == "last_year") {
                    dt = new Date();
                    var year = dt.getFullYear() - 1;
                    filter_data_selected.date_from = moment([year]).endOf('year').format('YYYY-MM-DD');
                }
            }
            else if (list_item_selected.length == 0) {
                if ($("#date_from").val()) {
                    var dateString = $("#date_from").val();
                    filter_data_selected.date_from = dateString;
                }
            }
            if (currency_item_selected.length) {
                var currency_value = $('ul.o_currency_filter').find('li > a.selected').parent().data('value');
                filter_data_selected.report_currency_id = currency_value;
            }

            var partner_ids = [];
            var partner_text = [];
            var span_res = document.getElementById("partner_res")
            var partner_list = $(".partners").select2('data')
            for (var i = 0; i < partner_list.length; i++) {
                if(partner_list[i].element[0].selected === true){
                    partner_ids.push(parseInt(partner_list[i].id))
                if(partner_text.includes(partner_list[i].text) === false){
                    partner_text.push(partner_list[i].text)
                }
                span_res.value = partner_text
                span_res.innerHTML=span_res.value;
                }
            }
            if (partner_list.length == 0){
            span_res.value = ""
            span_res.innerHTML="";
            }
            filter_data_selected.partner_ids = partner_ids

            var partner_category_ids = [];
            var partner_category_text = [];
            var span_res = document.getElementById("category_res")
            var category_list = $(".category").select2('data')

            for (var i = 0; i < category_list.length; i++) {
            if(category_list[i].element[0].selected === true)
            {partner_category_ids.push(parseInt(category_list[i].id))
            if(partner_category_text.includes(category_list[i].text) === false)
            {partner_category_text.push(category_list[i].text)
            }
            span_res.value = partner_category_text
            span_res.innerHTML=span_res.value;
            }
            }
            if (category_list.length == 0){
            span_res.value = ""
            span_res.innerHTML="";
            }
            filter_data_selected.partner_category_ids = partner_category_ids


            if ($(".target_move").length) {

            var post_res = document.getElementById("post_res")
            filter_data_selected.target_move = $(".target_move")[1].value

            post_res.value = $(".target_move")[1].value
            post_res.innerHTML=post_res.value;
            if ($(".target_move")[1].value == "") {
                post_res.innerHTML="posted";
            }
            }

            if ($(".result_selection").length) {
            var account_res = document.getElementById("account_res")
            filter_data_selected.result_selection = $(".result_selection")[1].value
            account_res.value = $(".result_selection")[1].value
                   account_res.innerHTML=account_res.value;
              if ($(".result_selection")[1].value == "") {
              account_res.innerHTML="customer";

              }
            }


            rpc.query({
                model: 'account.partner.ageing',
                method: 'write',
                args: [
                    self.wizard_id, filter_data_selected
                ],
            }).then(function(res) {
            self.initial_render = false;
                self.load_data(self.initial_render);
            });
        },
    });

});