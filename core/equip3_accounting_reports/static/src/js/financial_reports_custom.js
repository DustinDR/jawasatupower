odoo.define('equip3_accounting_reports.financial_reports_custom', function (require) {
    'use strict';
    var AbstractAction = require('web.AbstractAction');
    var core = require('web.core');
    var field_utils = require('web.field_utils');
    var rpc = require('web.rpc');
    var session = require('web.session');
    var utils = require('web.utils');
    var QWeb = core.qweb;
    var _t = core._t;

    window.click_num = 0;
    var ProfitAndLoss = AbstractAction.extend({
    template: 'custom_dfr_template_new',
        events: {
            'click .parent-line': 'journal_line_click',
            'click .child_col1': 'journal_line_click',
            'click #apply_filter': 'apply_filter',
            'click #apply_filter_prev': 'apply_filter_prev',
            'click #pdf': 'print_pdf',
            'click #xlsx': 'print_xlsx',
            'click .show-gl': 'show_gl',
            'click .filter_date': '_onFilterDate',
            'click .o_add_custom_filter': '_onCustomFilter',
            'click .o_add_custom_filter_prev': '_onPrevFilter',
            'click .o_add_custom_filter_last': '_onLastFilter',
            'click .filter_currency': '_onFilterCurrency',
        },

    _onFilterDate: function(ev) {
            ev.preventDefault();
            $(ev.target).parents().find('ul.o_date_filter').find('li > a.selected').removeClass('selected');
            if (!$('.o_account_reports_custom-dates').hasClass('d-none')) {
                $('.o_account_reports_custom-dates').addClass('d-none');
            }
            if ($(ev.target).is('a')) {
                $(ev.target).addClass('selected');
            }
            else {
                $(ev.target).find('a').addClass('selected');
            }
            var title = $(ev.target).parents().find('ul.o_date_filter').find('li > a.selected').parent().attr('title');
            $('.date_caret').text(title);
        },

    _onCustomFilter: function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            $(ev.target).parents().find('ul.o_filters_menu').find('li > a.selected').removeClass('selected');
            var custom_dates = $(ev.target).parents().find('ul.o_filters_menu').find('.o_account_reports_custom-dates');
            if (custom_dates.hasClass('d-none')) {
                custom_dates.removeClass('d-none');
                $('.date_caret').text('Custom');
            } else {
                custom_dates.addClass('d-none');
            }
            var prev_dates = $(ev.target).parents().find('ul.o_filters_menu').find('.o_account_reports_custom-prev');
            prev_dates.addClass('d-none');
            var last_dates = $(ev.target).parents().find('ul.o_filters_menu').find('.o_account_reports_custom-last');
            last_dates.addClass('d-none');
        },
    _onPrevFilter: function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            $(ev.target).parents().find('ul.o_filters_menu').find('li > a.selected').removeClass('selected');
            var prev_dates = $(ev.target).parents().find('ul.o_filters_menu').find('.o_account_reports_custom-prev');
            if (prev_dates.hasClass('d-none')) {
                prev_dates.removeClass('d-none');
                $('.date_caret').text('Preview');
            } else {
                prev_dates.addClass('d-none');
            }
            var custom_dates = $(ev.target).parents().find('ul.o_filters_menu').find('.o_account_reports_custom-dates');
            custom_dates.addClass('d-none');
            var last_dates = $(ev.target).parents().find('ul.o_filters_menu').find('.o_account_reports_custom-last');
            last_dates.addClass('d-none');
        },
    _onLastFilter: function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            $(ev.target).parents().find('ul.o_filters_menu').find('li > a.selected').removeClass('selected');
            var last_dates = $(ev.target).parents().find('ul.o_filters_menu').find('.o_account_reports_custom-last');
            if (last_dates.hasClass('d-none')) {
                last_dates.removeClass('d-none');
                $('.date_caret').text('Last');
            } else {
                last_dates.addClass('d-none');
            }
            var custom_dates = $(ev.target).parents().find('ul.o_filters_menu').find('.o_account_reports_custom-dates');
            custom_dates.addClass('d-none');
            var prev_dates = $(ev.target).parents().find('ul.o_filters_menu').find('.o_account_reports_custom-prev');
            prev_dates.addClass('d-none');
        },
    _onFilterCurrency: function(ev) {
            ev.preventDefault();
            $(ev.target).parents().find('ul.o_currency_filter').find('li > a.selected').removeClass('selected');
            if ($(ev.target).is('a')) {
                $(ev.target).addClass('selected');
            }
            else {
                $(ev.target).find('a').addClass('selected');
            }
            var title = $(ev.target).parents().find('ul.o_currency_filter').find('li > a.selected').parent().attr('title');
            $('.currency_caret').text(title);
        },

    init: function(parent, action) {
                this._super(parent, action);
                this.currency=action.currency;
                this.bs_lines = action.bs_lines;
                this.cat_report_line = action.cat_report_line;
                this.years_preview = action.years_preview;
                this.wizard_id = action.context.wizard | null;
            },
    start: function() {
            var self = this;
            self.initial_render = true;
            rpc.query({
                model: 'ctm.dynamic.balance.sheet.report',
                method: 'create',
                args: [{
                }]
            }).then(function(t_res) {
                self.wizard_id = t_res;
                self.load_data(self.initial_render);
            })
        },

    load_data: function (initial_render = true) {
            var self = this;
            var action_title = self._title;
            $('div.o_action_manager').css('overflow-y', 'auto');
                self.$(".categ").empty();
                try{
                    var self = this;
                    self._rpc({
                        model: 'ctm.dynamic.balance.sheet.report',
                        method: 'view_report',
                        args: [[this.wizard_id], action_title],
                    }).then(function(datas) {

                            if (initial_render) {
                                    self.$('.filter_view_dfr').html(QWeb.render('custom_DfrFilterView', {
                                        filter_data: datas['filters'],
                                        title : datas['name'],
                                    }));
                                    self.$el.find('.journals').select2({
                                        placeholder: ' Journals...',
                                    });
                                    self.$el.find('.account').select2({
                                        placeholder: ' Accounts...',
                                    });
                                    self.$el.find('.account-tag').select2({
                                        placeholder: 'Account Tag...',
                                    });
                                    self.$el.find('.analytics').select2({
                                        placeholder: 'Analytic Accounts...',
                                    });
                                    self.$el.find('.analytic-tag').select2({
                                        placeholder: 'Analytic Tag...',
                                    });

                                    self.$el.find('.target_move').select2({
                                        placeholder: 'Target Move...',
                                    });
                                    self.$el.find('.analytic_group_ids').select2({
                                        placeholder: 'analytic_group_ids...',
                                    });

                                    self.$el.find('.book').select2({
                                        placeholder: 'Book...',
                                    });

                            }
                            var child=[];
                        self.$('.table_view_dfr').html(QWeb.render('custom_dfr_table', {

                                            name : datas['name'],
                                            report_lines : datas['report_lines'],
                                            filter : datas['filters'],
                                            currency : datas['currency'],
                                            credit_total : datas['credit_total'],
                                            debit_total : datas['debit_total'],
                                            debit_balance : datas['debit_balance'],
                                            bs_lines : datas['bs_lines'],
                                            cat_report_line : datas['cat_report_line'],
                                            years_preview : datas['years_preview']
                                        }));
                });
                    }
                catch (el) {
                    window.location.href
                    }
            },

    format_currency: function(currency, amount) {
                if (typeof(amount) != 'number') {
                    amount = parseFloat(amount);
                }
                var formatted_value = (parseInt(amount)).toLocaleString(currency[2],{
                    minimumFractionDigits: 2
                })
                return formatted_value
            },

    show_gl: function(e) {
            var self = this;
            var account_id = $(e.target).attr('data-account-id');
            var options = {
                account_ids: [account_id],
            }
                var action = {
                    type: 'ir.actions.client',
                    name: 'GL View',
                    tag: 'g_l',
                    target: 'new',
                    domain: [['account_ids','=', account_id]],

                }
                return this.do_action(action);
        },

    print_pdf: function(e) {
            e.preventDefault();
            var self = this;
            var action_title = self._title;
            self._rpc({
                model: 'ctm.dynamic.balance.sheet.report',
                method: 'view_report',
                args: [
                    [self.wizard_id], action_title
                ],
            }).then(function(data) {
                var action = {
                    'type': 'ir.actions.report',
                    'report_type': 'qweb-pdf',
                    'report_name': 'equip3_accounting_reports.balance_sheet',
                    'report_file': 'equip3_accounting_reports.balance_sheet',
                    'data': {
                        'report_data': data,
                        'report_name': action_title
                    },
                    'context': {
                        'active_model': 'ctm.dynamic.balance.sheet.report',
                        'landscape': 1,
                        'bs_report': true
                    },
                    'display_name': action_title,
                };
                return self.do_action(action);
            });
        },

    print_xlsx: function() {
            var self = this;
            var action_title = self._title;
            self._rpc({
                model: 'ctm.dynamic.balance.sheet.report',
                method: 'view_report',
                args: [
                    [self.wizard_id],  action_title
                ],
            }).then(function(data) {
                var action = {
                    'type': 'ir_actions_dynamic_xlsx_download',
                    'data': {
                         'model': 'ctm.dynamic.balance.sheet.report',
                         'options': JSON.stringify(data['filters']),
                         'output_format': 'xlsx',
                         'report_data': action_title,
                         'report_name': action_title,
                         'dfr_data': JSON.stringify(data),
                    },
                };
                return self.do_action(action);
            });
        },

    journal_line_click: function (el){
            click_num++;
            var self = this;
            var line = $(el.target).parent().data('id');
            return self.do_action({
                type: 'ir.actions.act_window',
                    view_type: 'form',
                    view_mode: 'form',
                    res_model: 'account.move',
                    views: [
                        [false, 'form']
                    ],
                    res_id: line,
                    target: 'current',
            });

        },

    apply_filter: function(event) {
            event.preventDefault();
            var self = this;
            self.initial_render = false;
            var filter_data_selected = {};
            filter_data_selected.comparison = null;
            filter_data_selected.previous = null;
            var account_ids = [];
            var account_text = [];
            var account_res = document.getElementById("acc_res")
            var account_list = $(".account").select2('data')
            for (var i = 0; i < account_list.length; i++) {
                if(account_list[i].element[0].selected === true){

                    account_ids.push(parseInt(account_list[i].id))
                    if(account_text.includes(account_list[i].text) === false){
                        account_text.push(account_list[i].text)
                    }
                    account_res.value = account_text
                    account_res.innerHTML=account_res.value;
                }
            }
            if (account_list.length == 0){
               account_res.value = ""
                    account_res.innerHTML="";

            }
            filter_data_selected.account_ids = account_ids


            var journal_ids = [];
            var journal_text = [];
            var journal_res = document.getElementById("journal_res")
            var journal_list = $(".journals").select2('data')
            for (var i = 0; i < journal_list.length; i++) {
                if(journal_list[i].element[0].selected === true){

                    journal_ids.push(parseInt(journal_list[i].id))
                    if(journal_text.includes(journal_list[i].text) === false){
                        journal_text.push(journal_list[i].text)
                    }
                    journal_res.value = journal_text
                    journal_res.innerHTML=journal_res.value;
                }
            }
            if (journal_list.length == 0){
               journal_res.value = ""
                    journal_res.innerHTML="";

            }
            filter_data_selected.journal_ids = journal_ids

            var account_tag_ids = [];
            var account_tag_text = [];
            var account_tag_res = document.getElementById("acc_tag_res")

            var account_tag_list = $(".account-tag").select2('data')
            for (var i = 0; i < account_tag_list.length; i++) {
                if(account_tag_list[i].element[0].selected === true){

                    account_tag_ids.push(parseInt(account_tag_list[i].id))
                    if(account_tag_text.includes(account_tag_list[i].text) === false){
                        account_tag_text.push(account_tag_list[i].text)
                    }

                    account_tag_res.value = account_tag_text
                    account_tag_res.innerHTML=account_tag_res.value;
                }
            }
            if (account_tag_list.length == 0){
               account_tag_res.value = ""
                    account_tag_res.innerHTML="";

            }
            filter_data_selected.account_tag_ids = account_tag_ids

            var analytic_ids = []
            var analytic_text = [];
            var analytic_res = document.getElementById("analytic_res")
            var analytic_list = $(".analytics").select2('data')

            for (var i = 0; i < analytic_list.length; i++) {
                if(analytic_list[i].element[0].selected === true){

                    analytic_ids.push(parseInt(analytic_list[i].id))
                    if(analytic_text.includes(analytic_list[i].text) === false){
                        analytic_text.push(analytic_list[i].text)
                    }
                    analytic_res.value = analytic_text
                    analytic_res.innerHTML=analytic_res.value;
                }
            }
            if (analytic_list.length == 0){
               analytic_res.value = ""
                    analytic_res.innerHTML="";

            }
            filter_data_selected.analytic_ids = analytic_ids

            var analytic_tag_ids = [];
            var analytic_tag_text = [];
            var analytic_tag_res = document.getElementById("analic_tag_res")
            var analytic_tag_list = $(".analytic-tag").select2('data')
            for (var i = 0; i < analytic_tag_list.length; i++) {
                if(analytic_tag_list[i].element[0].selected === true){

                    analytic_tag_ids.push(parseInt(analytic_tag_list[i].id))
                    if(analytic_tag_text.includes(analytic_tag_list[i].text) === false){
                        analytic_tag_text.push(analytic_tag_list[i].text)

                    }

                    analytic_tag_res.value = analytic_tag_text
                    analytic_tag_res.innerHTML=analytic_tag_res.value;
                }
            }
            if (analytic_tag_list.length == 0){
               analytic_tag_res.value = ""
                    analytic_tag_res.innerHTML="";

            }
            filter_data_selected.analytic_tag_ids = analytic_tag_ids


            if ($(".analytic_group_ids").length) {
                var cat_res = document.getElementById("cat_res")
                filter_data_selected.analytic_group_ids = $(".analytic_group_ids")[1].value
                cat_res.value = $(".analytic_group_ids")[1].value
                cat_res.innerHTML= $("#category option:selected").text();
                  if ($(".analytic_group_ids")[1].value == "") {
                  cat_res.innerHTML="All";

                  }
                  if ($(".analytic_group_ids")[1].value == "All") {
                  cat_res.innerHTML="All";

                  }
            }

            // if ($("#date_from").val()) {
            //     var dateString = $("#date_from").val();
            //     filter_data_selected.date_from = dateString;
                
            // }
            // if ($("#date_to").val()) {
            //     var dateString = $("#date_to").val();
            //     filter_data_selected.date_to = dateString;
            // }

            if ($(".target_move").length) {
                var post_res = document.getElementById("post_res")
                filter_data_selected.target_move = $(".target_move")[1].value
                post_res.value = $(".target_move")[1].value
                post_res.innerHTML=post_res.value;
                  if ($(".target_move")[1].value == "") {
                  post_res.innerHTML="posted";

                  }
            }

            if ($(".book").length) {
                var book_res = document.getElementById("book_res")
                filter_data_selected.book = $(".book")[1].value
                book_res.value = $(".book")[1].value
                book_res.innerHTML=book_res.value;
                  if ($(".book")[1].value == "") {
                  book_res.innerHTML="commercial";

                  }
            }

            var currency_item_selected = $('ul.o_currency_filter').find('li > a.selected');
            if (currency_item_selected.length) {
                var currency_value = $('ul.o_currency_filter').find('li > a.selected').parent().data('value');
                filter_data_selected.report_currency_id = currency_value;
            }

            var dt;
            var list_item_selected = $('ul.o_date_filter').find('li > a.selected');
            var currency_item_selected = $('ul.o_currency_filter').find('li > a.selected');
            filter_data_selected.date_from = "";
            filter_data_selected.date_to = "";
            if (list_item_selected.length) {
                var filter_value = $('ul.o_date_filter').find('li > a.selected').parent().data('value');
                if (filter_value == "this_month") {
                    dt = new Date();
                    dt.setDate(1);
                    filter_data_selected.date_from = moment(dt).format('YYYY-MM-DD');
                    dt.setMonth(dt.getMonth() + 1);
                    dt.setDate(0);
                    filter_data_selected.date_to = moment(dt).format('YYYY-MM-DD');
                }
                else if (filter_value == "this_quarter") {
                    dt = new moment();
                    filter_data_selected.date_from = dt.startOf('quarter').format('YYYY-MM-DD');
                    filter_data_selected.date_to = dt.endOf('quarter').format('YYYY-MM-DD');
                    
                }
                else if (filter_value == "this_financial_year") {
                    dt = new Date();
                    var year = dt.getFullYear();
                    filter_data_selected.date_from = moment([year]).startOf('year').format('YYYY-MM-DD');
                    filter_data_selected.date_to = moment([year]).endOf('year').format('YYYY-MM-DD');
                }
                else if (filter_value == "last_month") {
                    dt = new Date();
                    dt.setDate(0);
                    filter_data_selected.date_to = moment(dt).format('YYYY-MM-DD');
                    dt.setDate(1);
                    filter_data_selected.date_from = moment(dt).format('YYYY-MM-DD');
                }
                else if (filter_value == "last_quarter") {
                    dt = new Date();
                    dt.setMonth((moment(dt).quarter() - 1) * 3); // Go to the first month of this quarter
                    dt.setDate(0); // Then last day of last month (= last day of last quarter)
                    filter_data_selected.date_to = moment(dt).format('YYYY-MM-DD');
                    dt.setDate(1);
                    dt.setMonth(dt.getMonth() - 2);
                    filter_data_selected.date_from = moment(dt).format('YYYY-MM-DD');
                }
                else if (filter_value == "last_year") {
                    dt = new Date();
                    var year = dt.getFullYear() - 1;
                    filter_data_selected.date_to = moment([year]).endOf('year').format('YYYY-MM-DD');
                    filter_data_selected.date_from = moment([year]).startOf('year').format('YYYY-MM-DD');
                }
                if (filter_value == "today") {
                    dt = new Date();
                    dt.setDate(1);
                    filter_data_selected.date_from = moment(dt).format('YYYY-MM-DD');
                    dt.setDate(0);
                    filter_data_selected.date_to = moment(dt).format('YYYY-MM-DD');
                }
            }
            else if (list_item_selected.length == 0) {
                if ($("#date_from").val()) {
                    var dateString = $("#date_from").val();
                    filter_data_selected.date_from = dateString;
                }
                if ($("#date_to").val()) {
                    var dateString = $("#date_to").val();
                    filter_data_selected.date_to = dateString;
                }
            }

            // var dt;
            // var list_item_selected = $('ul.o_date_filter').find('li > a.selected');
            // filter_data_selected.date_from = "";
            // filter_data_selected.date_to = "";
            // if ($("#date_from").val()) {
            //     var dateString = $("#date_from").val();
            //     filter_data_selected.date_from = dateString;
            // }
            // if ($("#date_to").val()) {
            //     var dateString = $("#date_to").val();
            //     filter_data_selected.date_to = dateString;
            // }

            // if ($(".analytic_group").length) {
            //     var analic_group_res = document.getElementById("analic_group_res")
            //     filter_data_selected.analytic_group_ids = $(".analytic_group")[0].value
            //     analic_group_res.value = $(".analytic_group")[0].value
            //             post_res.innerHTML=analic_group_res.value;
            //       if ($(".analytic_group")[0].value == "") {
            //       analic_group_res.innerHTML="";

            //       }
            // }

            rpc.query({
                model: 'ctm.dynamic.balance.sheet.report',
                method: 'write',
                args: [
                    self.wizard_id, filter_data_selected
                ],
            }).then(function(res) {
            self.initial_render = false;
                self.load_data(self.initial_render);
            });
        },

    apply_filter_prev: function(event) {

            event.preventDefault();
            var self = this;
            self.initial_render = false;
            var filter_data_selected = {};
            var account_ids = [];
            var account_text = [];
            var account_res = document.getElementById("acc_res")
            var account_list = $(".account").select2('data')
            for (var i = 0; i < account_list.length; i++) {
                if(account_list[i].element[0].selected === true){

                    account_ids.push(parseInt(account_list[i].id))
                    if(account_text.includes(account_list[i].text) === false){
                        account_text.push(account_list[i].text)
                    }
                    account_res.value = account_text
                    account_res.innerHTML=account_res.value;
                }
            }
            if (account_list.length == 0){
               account_res.value = ""
                    account_res.innerHTML="";

            }
            filter_data_selected.account_ids = account_ids


            var journal_ids = [];
            var journal_text = [];
            var journal_res = document.getElementById("journal_res")
            var journal_list = $(".journals").select2('data')
            for (var i = 0; i < journal_list.length; i++) {
                if(journal_list[i].element[0].selected === true){

                    journal_ids.push(parseInt(journal_list[i].id))
                    if(journal_text.includes(journal_list[i].text) === false){
                        journal_text.push(journal_list[i].text)
                    }
                    journal_res.value = journal_text
                    journal_res.innerHTML=journal_res.value;
                }
            }
            if (journal_list.length == 0){
               journal_res.value = ""
                    journal_res.innerHTML="";

            }
            filter_data_selected.journal_ids = journal_ids

            var account_tag_ids = [];
            var account_tag_text = [];
            var account_tag_res = document.getElementById("acc_tag_res")

            var account_tag_list = $(".account-tag").select2('data')
            for (var i = 0; i < account_tag_list.length; i++) {
                if(account_tag_list[i].element[0].selected === true){

                    account_tag_ids.push(parseInt(account_tag_list[i].id))
                    if(account_tag_text.includes(account_tag_list[i].text) === false){
                        account_tag_text.push(account_tag_list[i].text)
                    }

                    account_tag_res.value = account_tag_text
                    account_tag_res.innerHTML=account_tag_res.value;
                }
            }
            if (account_tag_list.length == 0){
               account_tag_res.value = ""
                    account_tag_res.innerHTML="";

            }
            filter_data_selected.account_tag_ids = account_tag_ids

            var analytic_ids = []
            var analytic_text = [];
            var analytic_res = document.getElementById("analytic_res")
            var analytic_list = $(".analytics").select2('data')

            for (var i = 0; i < analytic_list.length; i++) {
                if(analytic_list[i].element[0].selected === true){

                    analytic_ids.push(parseInt(analytic_list[i].id))
                    if(analytic_text.includes(analytic_list[i].text) === false){
                        analytic_text.push(analytic_list[i].text)
                    }
                    analytic_res.value = analytic_text
                    analytic_res.innerHTML=analytic_res.value;
                }
            }
            if (analytic_list.length == 0){
               analytic_res.value = ""
                    analytic_res.innerHTML="";

            }
            filter_data_selected.analytic_ids = analytic_ids

            var analytic_tag_ids = [];
            var analytic_tag_text = [];
            var analytic_tag_res = document.getElementById("analic_tag_res")
            var analytic_tag_list = $(".analytic-tag").select2('data')
            for (var i = 0; i < analytic_tag_list.length; i++) {
                if(analytic_tag_list[i].element[0].selected === true){

                    analytic_tag_ids.push(parseInt(analytic_tag_list[i].id))
                    if(analytic_tag_text.includes(analytic_tag_list[i].text) === false){
                        analytic_tag_text.push(analytic_tag_list[i].text)

                    }

                    analytic_tag_res.value = analytic_tag_text
                    analytic_tag_res.innerHTML=analytic_tag_res.value;
                }
            }
            if (analytic_tag_list.length == 0){
               analytic_tag_res.value = ""
                    analytic_tag_res.innerHTML="";

            }
            filter_data_selected.analytic_tag_ids = analytic_tag_ids

            if ($(".analytic_group_ids").length) {
                var cat_res = document.getElementById("cat_res")
                filter_data_selected.analytic_group_ids = $(".analytic_group_ids")[1].value
                cat_res.value = $(".analytic_group_ids")[1].value
                cat_res.innerHTML= $("#category option:selected").text();
                  if ($(".analytic_group_ids")[1].value == "") {
                  cat_res.innerHTML="All";

                  }
                  if ($(".analytic_group_ids")[1].value == "All") {
                  cat_res.innerHTML="All";

                  }
            }

            // if ($("#date_from").val()) {
            //     var dateString = $("#date_from").val();
            //     filter_data_selected.date_from = dateString;
            // }
            // if ($("#date_to").val()) {
            //     var dateString = $("#date_to").val();
            //     filter_data_selected.date_to = dateString;
            // }

            if ($(".target_move").length) {
                var post_res = document.getElementById("post_res")
                filter_data_selected.target_move = $(".target_move")[1].value
                post_res.value = $(".target_move")[1].value
                post_res.innerHTML=post_res.value;
                  if ($(".target_move")[1].value == "") {
                  post_res.innerHTML="posted";

                  }
            }

            if ($("#prev").val()) {
                var prev = $("#prev").val();
                filter_data_selected.comparison = prev;
                filter_data_selected.previous = true;
                document.getElementById("last").value = null;
                document.getElementById("cat_res").value = null;
                document.getElementById("prev").value = null;
                document.getElementById("date_to").value = null;
                document.getElementById("date_from").value = null;
                filter_data_selected.date_from = document.getElementById("date_from").value;
                filter_data_selected.date_to = document.getElementById("date_to").value;
            }
            if ($("#last").val()) {
                var last = $("#last").val();
                filter_data_selected.comparison = last;
                filter_data_selected.previous = false;
                document.getElementById("prev").value = null;
                document.getElementById("cat_res").value = null;
                document.getElementById("last").value = null;
                document.getElementById("date_to").value = null;
                document.getElementById("date_from").value = null;
                filter_data_selected.date_from = document.getElementById("date_from").value;
                filter_data_selected.date_to = document.getElementById("date_to").value;
            }

            if ($(".analytic_group_ids").length) {
                var cat_res = document.getElementById("cat_res")
                filter_data_selected.analytic_group_ids = $(".analytic_group_ids")[1].value
                cat_res.value = $(".analytic_group_ids")[1].value
                cat_res.innerHTML= $("#category option:selected").text();
                  if ($(".analytic_group_ids")[1].value == "") {
                  cat_res.innerHTML="All";

                  }
                  if ($(".analytic_group_ids")[1].value == "All") {
                  cat_res.innerHTML="All";

                  }
            }
            var currency_item_selected = $('ul.o_currency_filter').find('li > a.selected');
            if (currency_item_selected.length) {
                var currency_value = $('ul.o_currency_filter').find('li > a.selected').parent().data('value');
                filter_data_selected.report_currency_id = currency_value;
            }

            if ($(".book").length) {
                var book_res = document.getElementById("book_res")
                filter_data_selected.book = $(".book")[1].value
                book_res.value = $(".book")[1].value
                book_res.innerHTML=book_res.value;
                  if ($(".book")[1].value == "") {
                  book_res.innerHTML="commercial";

                  }
            }


            // var dt;
            // var list_item_selected = $('ul.o_date_filter').find('li > a.selected');
            // filter_data_selected.date_from = "";
            // filter_data_selected.date_to = "";
            // if ($("#date_from").val()) {
            //     var dateString = $("#date_from").val();
            //     filter_data_selected.date_from = dateString;
            // }
            // if ($("#date_to").val()) {
            //     var dateString = $("#date_to").val();
            //     filter_data_selected.date_to = dateString;
            // }

            // if ($(".analytic_group").length) {
            //     var analic_group_res = document.getElementById("analic_group_res")
            //     filter_data_selected.analytic_group_ids = $(".analytic_group")[0].value
            //     analic_group_res.value = $(".analytic_group")[0].value
            //             post_res.innerHTML=analic_group_res.value;
            //       if ($(".analytic_group")[0].value == "") {
            //       analic_group_res.innerHTML="";

            //       }
            // }

            rpc.query({
                model: 'ctm.dynamic.balance.sheet.report',
                method: 'write',
                args: [
                    self.wizard_id, filter_data_selected
                ],
            }).then(function(res) {
            self.initial_render = false;
                self.load_data(self.initial_render);
            });
        },

    });
    core.action_registry.add("c_dfr_n", ProfitAndLoss);
    return ProfitAndLoss;
});
