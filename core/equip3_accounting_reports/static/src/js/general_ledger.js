odoo.define('equip3_accounting_reports.general_ledger', function (require) {

    var GeneralLedger = require('dynamic_cash_flow_statements.general_ledger');

    GeneralLedger.include({
        load_data: function (initial_render = true) {
            $('div.o_action_manager').css('overflow-y', 'auto');
            return this._super.apply(this, arguments);
        }
    })

});