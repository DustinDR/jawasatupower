odoo.define('equip3_accounting_reports.trial', function (require) {
    'use strict';
    var TrialBalance = require('dynamic_cash_flow_statements.trial');
    var core = require('web.core');
    var QWeb = core.qweb;
    var _t = core._t;
    var rpc = require('web.rpc');

    window.click_num = 0;
    TrialBalance.include({

        load_data: function (initial_render = true) {
            var self = this;
                self.$(".categ").empty();
                $('div.o_action_manager').css('overflow-y', 'auto');
                try{
                    var self = this;
                    self._rpc({
                        model: 'account.trial.balance',
                        method: 'view_report',
                        args: [[this.wizard_id]],
                    }).then(function(datas) {
                            _.each(datas['report_lines'], function(rep_lines) {
                            rep_lines.debit = self.format_currency(datas['currency'],rep_lines.debit);
                            rep_lines.credit = self.format_currency(datas['currency'],rep_lines.credit);
                            rep_lines.ending_debit = self.format_currency(datas['currency'],rep_lines.ending_debit);
                            rep_lines.ending_credit = self.format_currency(datas['currency'],rep_lines.ending_credit);
                            rep_lines.opening_debit = self.format_currency(datas['currency'],rep_lines.opening_debit);
                            rep_lines.opening_credit = self.format_currency(datas['currency'],rep_lines.opening_credit);
                            rep_lines.balance = self.format_currency(datas['currency'],rep_lines.balance);



                            });
                            if (initial_render) {
                                    self.$('.filter_view_tb').html(QWeb.render('TrialFilterView', {
                                        filter_data: datas['filters'],
                                    }));
                                    self.$el.find('.journals').select2({
                                        placeholder: 'Select Journals...',
                                    });
                                    self.$el.find('.target_move').select2({
                                        placeholder: 'Target Move...',
                                    });
                                    self.$el.find('.consolidate').select2({
                                        placeholder: 'consolidation...',
                                    });
                            }
                            var child=[];

                        self.$('.table_view_tb').html(QWeb.render('TrialTable', {

                                            report_lines : datas['report_lines'],
                                            filter : datas['filters'],
                                            currency : datas['currency'],
                                            credit_total : self.format_currency(datas['currency'],datas['debit_total']),
                                            debit_total : self.format_currency(datas['currency'],datas['debit_total']),
                                            opening_credit_total : self.format_currency(datas['currency'],datas['opening_credit_total']),
                                            opening_debit_total : self.format_currency(datas['currency'],datas['opening_debit_total']),
                                            ending_credit_total : self.format_currency(datas['currency'],datas['ending_credit_total']),
                                            ending_debit_total : self.format_currency(datas['currency'],datas['ending_debit_total']),
                                        }));
                });

                    }
                catch (el) {
                    window.location.href
                    }
            },
            apply_filter: function(event) {

            event.preventDefault();
            var self = this;
            self.initial_render = false;

            var filter_data_selected = {};
            var journal_ids = [];
            var journal_text = [];
            var journal_res = document.getElementById("journal_res")
            var journal_list = $(".journals").select2('data')

            for (var i = 0; i < journal_list.length; i++) {
                if(journal_list[i].element[0].selected === true){

                    journal_ids.push(parseInt(journal_list[i].id))
                    if(journal_text.includes(journal_list[i].text) === false){
                        journal_text.push(journal_list[i].text)
                    }
                    journal_res.value = journal_text
                    journal_res.innerHTML=journal_res.value;
                }
            }
            if (journal_list.length == 0){
               journal_res.value = ""
                    journal_res.innerHTML="";

            }
            filter_data_selected.journal_ids = journal_ids

            if ($("#date_from").val()) {
                var dateString = $("#date_from").val();
                filter_data_selected.date_from = dateString;
            }
            if ($("#date_to").val()) {
                var dateString = $("#date_to").val();
                filter_data_selected.date_to = dateString;
            }

            if ($(".target_move").length) {
            var post_res = document.getElementById("post_res")
            filter_data_selected.target_move = $(".target_move")[1].value
            post_res.value = $(".target_move")[1].value
                    post_res.innerHTML=post_res.value;
              if ($(".target_move")[1].value == "") {
              post_res.innerHTML="posted";

              }
            }

            if ($(".consolidate").length) {
            var consolidate_res = document.getElementById("consolidate_res")
            filter_data_selected.consolidate = $(".consolidate")[1].value
            consolidate_res.value = $(".consolidate")[1].value
                    consolidate_res.innerHTML=consolidate_res.value;
            if ($(".consolidate")[1].value == "") {
              consolidate_res.innerHTML="off";

              }
            }
            
            rpc.query({
                model: 'account.trial.balance',
                method: 'write',
                args: [
                    self.wizard_id, filter_data_selected
                ],
            }).then(function(res) {
            self.initial_render = false;
                self.load_data(self.initial_render);
            });
        },
    });
    core.action_registry.add("t_b", TrialBalance);
    return TrialBalance;
});