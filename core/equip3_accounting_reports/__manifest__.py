
{
    'name': 'Accounting Reports',
    'version': '1.1.31',
    'author': 'Hashmicro / Prince',
    'category' : 'Accounting',
    'depends': ['equip3_accounting_deposit',
                'equip3_accounting_pettycash',
                'equip3_accounting_masterdata',
                'equip3_accounting_asset',
                'equip3_accounting_efaktur',
                'accounting_pdf_reports',
                'om_account_budget',
                'account',
                'accounting_pdf_reports',
                'dynamic_accounts_report',
                'equip3_accounting_operation'
                ],
    'data': [
    
        'security/ir.model.access.csv',
        'views/customer_deposit_report_views.xml',
        'views/vendor_deposit_report_views.xml',
        'views/account_petty_cash_report.xml',
        'views/journal_entries_report.xml',
        "views/tax_report_view.xml",
        'views/templates.xml',
        'views/menuitem_inherit_views.xml',
        'views/crossovered_budget_lines_views.xml',
        'wizard/balance_sheet.xml',
        'report/cash_flow_statement_report.xml',
        'report/trial_balance.xml',
        'data/customer_statement_template.xml',
        'report/customer_statement.xml',
        'report/equity_move.xml',
        'report/financial_report_template.xml',
        
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
    'demo': [],
    'auto_install': False,
    'installable': True,
    'application': True
}