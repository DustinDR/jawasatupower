from odoo import api, models, _


class GeneralLedger(models.AbstractModel):
    _name = 'report.equip3_accounting_reports.cash_flow_statement'

    @api.model
    def _get_report_values(self, docids, data=None):

        if self.env.context.get('trial_pdf_report'):
            if data.get('report_data'):
                account_data = data.get('report_data')['report_lines']
                received_customer = self._sum_list(account_data['received_customer'],'Advance payments received from customers')
                cash_received = self._sum_list(account_data['cash_received'],'Cash received from')
                payment_supplier = self._sum_list(account_data['payment_supplier'],'Advance payments made to suppliers')
                cash_paid = self._sum_list(account_data['cash_paid'],'Cash paid for')
                sum_cf_statement = self._sum_list(received_customer + cash_received + payment_supplier + cash_paid,'Total cash flow from operating activities')
                cf_investing = self._sum_list(account_data['cf_investing'], "cf_investing")
                cf_finance = self._sum_list(account_data['cf_finance'], "cf_finance")
                cf_unclass = self._check_list(self._sum_list(account_data['cf_unclass'], "cf_unclass"))
                sum_all_cf_statement = self._sum_list(sum_cf_statement + cf_investing + cf_finance + cf_unclass,'Net increase in cash and cash equivalents')
                cf_beginning_period = self._sum_list(account_data['cf_beginning_period'], "Cash and cash equivalents, beginning of period")
                cf_closing_period = self._sum_list(sum_all_cf_statement + cf_beginning_period,'Cash and cash equivalents, closing of period')

                data.update({'account_data': account_data,
                             'received_customer': received_customer,
                             'cash_received': cash_received,
                             'payment_supplier': payment_supplier,
                             'cash_paid': cash_paid,
                             'sum_cf_statement': sum_cf_statement,
                             'cf_investing': cf_investing,
                             'cf_finance': cf_finance,
                             'cf_unclass': cf_unclass,
                             'sum_all_cf_statement': sum_all_cf_statement,
                             'cf_beginning_period': cf_beginning_period,
                             'cf_closing_period': cf_closing_period,
                             'Filters': data.get('report_data')['filters'],
                             'company': self.env.company,
                             })
        return data

    def _check_list(self,list_value):
        values=[]
        check = False
        for rec in list_value:
            if rec['total_debit'] > 0 or rec['total_credit'] > 0 or rec['total_balance'] > 0:
                check = True
        if check == True:
            values = list_value
        return values

    def _sum_list(self,list_value, value_name):
        values=[]
        value =  {'month_part': value_name, 
                  'year_part': 2022, 
                  'total_debit': 0.0, 
                  'total_credit': 0.0, 
                  'total_balance': 0.0}
        for rec in list_value:
            value['total_debit'] += rec['total_debit']
            value['total_credit'] += rec['total_credit']
            value['total_balance'] += rec['total_balance']
        values.append(value)
        return values

    def _sum_list_minus(self,list_value, value_name):
        values=[]
        value =  {'month_part': value_name, 
                  'year_part': 2022, 
                  'total_debit': 0.0, 
                  'total_credit': 0.0, 
                  'total_balance': 0.0}
        i = 0
        max_len = len(list_value)
        while i < max_len:
            if i == 0:
                value['total_debit'] = list_value[i]['total_debit']
                value['total_credit'] = list_value[i]['total_credit']
                value['total_balance'] = list_value[i]['total_balance']
                i += 1
            else:
                value['total_debit'] -= list_value[i]['total_debit']
                value['total_credit'] -= list_value[i]['total_credit']
                value['total_balance'] -= list_value[i]['total_balance']
                i += 1
        values.append(value)
        return values
