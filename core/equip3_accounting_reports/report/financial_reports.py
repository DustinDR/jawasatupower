# -*- coding: utf-8 -*-
from odoo import api, models, _


class InsReportBalanceSheet(models.AbstractModel):
    _name = 'report.equip3_accounting_reports.balance_sheet'

    @api.model
    def _get_report_values(self, docids, data=None):
        if self.env.context.get('bs_report'):
            print(data)
            if data.get('report_data'):
                data.update({
                    'Filters': data.get('report_data')['filters'],
                    'account_data': data.get('report_data')['report_lines'],
                    'report_lines': data.get('report_data')['bs_lines'],
                    'report_name': data.get('report_name'),
                    'title': data.get('report_data')['name'],
                    'company': self.env.company,
                    'bs_lines': data.get('report_data')['bs_lines'],
                    'cat_report_line': data.get('report_data')['cat_report_line'],
                    'years_preview': data.get('report_data')['years_preview'],
                })
        return data
