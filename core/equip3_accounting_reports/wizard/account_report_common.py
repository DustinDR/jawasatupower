# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class AccountCommonReport(models.TransientModel):
    _inherit = "account.common.report"

    report_currency_id = fields.Many2one('res.currency', stirng="Currency")
