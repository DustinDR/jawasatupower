import time
from odoo import fields, models, api, _

import io
import json
from odoo.exceptions import AccessError, UserError, AccessDenied
from datetime import date

try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    import xlsxwriter


class BalanceSheetView(models.TransientModel):
    _name = 'ctm.dynamic.balance.sheet.report'

    company_id = fields.Many2one('res.company', required=True,
                                 default=lambda self: self.env.company)
    journal_ids = fields.Many2many('account.journal',
                                   string='Journals', required=True,
                                   default=[])
    account_ids = fields.Many2many("account.account", string="Accounts")
    account_tag_ids = fields.Many2many("account.account.tag",
                                       string="Account Tags")
    analytic_ids = fields.Many2many(
        "account.analytic.account", string="Analytic Accounts")
    analytic_tag_ids = fields.Many2many("account.analytic.tag",
                                        string="Analytic Tags")
    display_account = fields.Selection(
        [('all', 'All'), ('movement', 'With movements'),
         ('not_zero', 'With balance is not equal to 0')],
        string='Display Accounts', required=True, default='movement')
    target_move = fields.Selection(
        [('all', 'All'), ('posted', 'Posted')],
        string='Target Move', required=True, default='posted')
    date_from = fields.Date(string="Start date")
    date_to = fields.Date(string="End date")

    analytic_group_ids = fields.Many2many("account.analytic.group", string="Analytic Groups")
    comparison = fields.Integer(string="Comparison")
    previous = fields.Boolean(string="Previous", default=False)
    report_currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.company.currency_id)

    book = fields.Selection(
        [('commercial', 'Commercial Book.'), ('fiscal', 'Fiscal Book')],
        string='Book', required=True, default='commercial')
    
    @api.model
    def view_report(self, option, tag):
        r = self.env['ctm.dynamic.balance.sheet.report'].search(
            [('id', '=', option[0])])
        data = {
            'display_account': r.display_account,
            'model': self,
            'journals': r.journal_ids,
            'target_move': r.target_move,
            'accounts': r.account_ids,
            'account_tags': r.account_tag_ids,
            'analytics': r.analytic_ids,
            'analytic_tags': r.analytic_tag_ids,
            'analytic_groups': r.analytic_group_ids,
            'comparison': r.comparison,
            'previous': r.previous,
            'currency_id': r.report_currency_id,
            'book': r.book,
        }
        if r.date_from:
            data.update({
                'date_from': r.date_from,
            })
        if r.date_to:
            data.update({
                'date_to': r.date_to,
            })

        company_id = self.env.company
        company_domain = [('company_id', '=', company_id.id)]
        if r.account_tag_ids:
            company_domain.append(
                ('tag_ids', 'in', r.account_tag_ids.ids))
        if r.account_ids:
            company_domain.append(('id', 'in', r.account_ids.ids))

        new_account_ids = self.env['account.account'].search(company_domain)
        data.update({'accounts': new_account_ids,})
        
        filters = self.get_filter(option)

        def rec_report():    
            records = self._get_report_values(data)

            company_id = self.env.company
            currency = data['currency_id']
            symbol = currency.symbol
            rounding = currency.rounding
            position = currency.position
            
            currency_rate = 0
            currency_id = currency
            if data.get('date_from') and data.get('date_to'):
                rate_ids = currency_id.rate_ids.filtered(lambda r: r.name >= data.get('date_from') and r.name <= data.get('date_to')).sorted(key=lambda r: r.name)
                if rate_ids:
                    currency_rate = rate_ids[-1].mr_rate
                else:
                    currency_rate = currency_id.rate
            elif data.get('date_from'):
                rate_ids = currency_id.rate_ids.filtered(lambda r: r.name >= data.get('date_from')).sorted(key=lambda r: r.name)
                if rate_ids:
                    currency_rate = rate_ids[-1].mr_rate
                else:
                    currency_rate = currency_id.rate
            elif data.get('date_to'):
                rate_ids = currency_id.rate_ids.filtered(lambda r: r.name <= data.get('date_to')).sorted(key=lambda r: r.name)
                if rate_ids:
                    currency_rate = rate_ids[-1].mr_rate
                else:
                    currency_rate = currency_id.rate
            else:
                currency_rate = currency_id.rate

            def final_record(rec_rprt): 
                if filters['account_tags'] != ['All']:
                    tag_accounts = list(map(lambda x: x.code, new_account_ids))

                    def filter_code(rec_dict):
                        if rec_dict['code'] in tag_accounts:
                            return True
                        else:
                            return False

                    new_records = list(filter(filter_code, records['Accounts']))
                    records['Accounts'] = new_records

                account_report_id = self.env['account.financial.report'].search([
                    ('name', 'ilike', tag)])

                new_data = {'id': self.id, 'date_from': False,
                            'enable_filter': True,
                            'debit_credit': True,
                            'date_to': False, 'account_report_id': account_report_id,
                            'target_move': filters['target_move'],
                            'view_format': 'vertical',
                            'company_id': self.company_id,
                            'used_context': {'journal_ids': False,
                                             'state': filters['target_move'].lower(),
                                             'date_from': filters['date_from'],
                                             'date_to': filters['date_to'],
                                             'strict_range': False,
                                             'company_id': self.company_id,
                                             'lang': 'en_US'}}

                account_lines = self.get_account_lines(new_data)
                report_lines = self.view_report_pdf(account_lines, new_data)[
                    'report_lines']
                    
                move_line_accounts = []
                move_lines_dict = {}

                for rec in rec_rprt['Accounts']:
                    move_line_accounts.append(rec['code'])
                    move_lines_dict[rec['code']] = {}
                    move_lines_dict[rec['code']]['debit'] = rec['debit']
                    move_lines_dict[rec['code']]['credit'] = rec['credit']
                    move_lines_dict[rec['code']]['balance'] = rec['balance']

                report_lines_move = []
                parent_list = []

                def filter_movelines_parents(obj):
                    for each in obj:
                        if each['report_type'] == 'accounts':
                            if each['code'] in move_line_accounts:
                                report_lines_move.append(each)
                                parent_list.append(each['p_id'])

                        elif each['report_type'] == 'account_report':
                            report_lines_move.append(each)
                        else:
                            report_lines_move.append(each)

                filter_movelines_parents(report_lines)

                for rec in report_lines_move:
                    if rec['report_type'] == 'accounts':
                        if rec['code'] in move_line_accounts:
                            rec['debit'] = move_lines_dict[rec['code']]['debit']
                            rec['credit'] = move_lines_dict[rec['code']]['credit']
                            rec['balance'] = move_lines_dict[rec['code']]['balance']

                parent_list = list(set(parent_list))
                max_level = 0
                for rep in report_lines_move:
                    if rep['level'] > max_level:
                        max_level = rep['level']

                def get_parents(obj):
                    for item in report_lines_move:
                        for each in obj:
                            if item['report_type'] != 'account_type' and \
                                    each in item['c_ids']:
                                obj.append(item['r_id'])
                        if item['report_type'] == 'account_report':
                            obj.append(item['r_id'])
                            break

                get_parents(parent_list)
                for i in range(max_level):
                    get_parents(parent_list)

                parent_list = list(set(parent_list))
                final_report_lines = []

                for rec in report_lines_move:
                    if rec['report_type'] != 'accounts':
                        if rec['r_id'] in parent_list:
                            final_report_lines.append(rec)
                    else:
                        final_report_lines.append(rec)

                def filter_sum(obj):
                    sum_list = {}
                    for pl in parent_list:
                        sum_list[pl] = {}
                        sum_list[pl]['s_debit'] = 0
                        sum_list[pl]['s_credit'] = 0
                        sum_list[pl]['s_balance'] = 0

                    for each in obj:
                        if each['p_id'] and each['p_id'] in parent_list:
                            sum_list[each['p_id']]['s_debit'] += each['debit']
                            sum_list[each['p_id']]['s_credit'] += each['credit']
                            sum_list[each['p_id']]['s_balance'] += each['balance']
                    return sum_list

                def assign_sum(obj):
                    for each in obj:
                        if each['r_id'] in parent_list and \
                                each['report_type'] != 'account_report':
                            each['debit'] = sum_list_new[each['r_id']]['s_debit']
                            each['credit'] = sum_list_new[each['r_id']]['s_credit']

                for p in range(max_level):
                    sum_list_new = filter_sum(final_report_lines)
                    assign_sum(final_report_lines)            

                for rec in final_report_lines:
                    rec['debit'] = round(rec['debit'] * currency_rate, 2)
                    rec['credit'] = round(rec['credit'] * currency_rate, 2)
                    rec['balance'] = rec['debit'] - rec['credit']
                    rec['balance'] = round(rec['balance'], 2)
                    if (rec['balance_cmp'] * currency_rate  < 0 and rec['balance'] > 0) or (
                            rec['balance_cmp'] * currency_rate  > 0 and rec['balance'] < 0):
                        rec['balance'] = rec['balance'] * -1

                    if position == "before":
                        rec['m_debit'] = symbol + " " + "{:,.2f}".format(rec['debit'])
                        rec['m_credit'] = symbol + " " + "{:,.2f}".format(rec['credit'])
                        rec['m_balance'] = symbol + " " + "{:,.2f}".format(
                            rec['balance'])
                    else:
                        rec['m_debit'] = "{:,.2f}".format(rec['debit']) + " " + symbol
                        rec['m_credit'] = "{:,.2f}".format(rec['credit']) + " " + symbol
                        rec['m_balance'] = "{:,.2f}".format(
                            rec['balance']) + " " + symbol

                return final_report_lines

            cat_report_line = []
            if data['analytic_groups']:            
                for cat in records['Accounts']:
                    res_rprt = {'anl_id' : cat['anl_id'],
                                'anl_name' : cat['anl_name'],
                                'report_lines' : cat['Accounts'],
                                'final_report_lines' : final_record(cat)}
                    cat_report_line.append(res_rprt)            
            else:
                res_rprt = {'anl_id' : '00001',
                            'anl_name' : 'Balance',
                            'report_lines' : records['Accounts'],
                            'final_report_lines' : final_record(records)}
                cat_report_line.append(res_rprt)

            vals_tmp=[]
            number = 1
            for s in cat_report_line:
                for q in s['final_report_lines']:
                    if 'code' in q:
                        for r in s['report_lines']:
                            if r['code'] == q['code']:
                                code_acc = list(filter(lambda x: x['number'] == r['code'], vals_tmp))
                                if code_acc:
                                    code_acc[0][s['anl_name']] = q['m_balance']
                                else:
                                    res={}
                                    res['code'] = r['code']
                                    res['name'] = r['name']
                                    res[s['anl_name']] = q['m_balance']
                                    res['number'] = r['code']
                                    vals_tmp.append(res)
                    else:
                        code_acc1 = list(filter(lambda x: x['name'] == q['name'], vals_tmp))                   
                        if code_acc1:
                            code_acc1[0][str(s['anl_name'])] = q['m_balance']
                        else:
                            res={}
                            res['number'] = '0000' + str(number)
                            res['name'] = q['name']
                            res[s['anl_name']] = q['m_balance']
                            vals_tmp.append(res)
                            number = number+1

            return {
                'name': tag,
                'type': 'ir.actions.client',
                'tag': tag,
                'filters': filters,
                'report_lines': records['Accounts'],
                'debit_total': records['debit_total'],
                'credit_total': records['credit_total'],
                'debit_balance': records['debit_balance'],
                'currency': currency,
                'bs_lines': vals_tmp,
                'cat_report_line': cat_report_line,
                'years_preview': False,
                
            }        

        
        
        def rec_report_comp(comp, prev):
            data['analytic_groups'] = False
            company_id = self.env.company
            currency = data['currency_id']
            symbol = currency.symbol
            rounding = currency.rounding
            position = currency.position

            currency_rate = 0
            currency_id = currency
            if data.get('date_from') and data.get('date_to'):
                rate_ids = currency_id.rate_ids.filtered(lambda r: r.name >= data.get('date_from') and r.name <= data.get('date_to')).sorted(key=lambda r: r.name)
                if rate_ids:
                    currency_rate = rate_ids[-1].mr_rate
                else:
                    currency_rate = currency_id.rate
            elif data.get('date_from'):
                rate_ids = currency_id.rate_ids.filtered(lambda r: r.name >= data.get('date_from')).sorted(key=lambda r: r.name)
                if rate_ids:
                    currency_rate = rate_ids[-1].mr_rate
                else:
                    currency_rate = currency_id.rate
            elif data.get('date_to'):
                rate_ids = currency_id.rate_ids.filtered(lambda r: r.name <= data.get('date_to')).sorted(key=lambda r: r.name)
                if rate_ids:
                    currency_rate = rate_ids[-1].mr_rate
                else:
                    currency_rate = currency_id.rate
            else:
                currency_rate = currency_id.rate

            vals_tmp=[]
            years_preview = []
            for rec_comp in range(0,comp):
                res_years_preview={}
                prev_year = rec_comp
                if prev == True:
                    prev_year = -rec_comp
                else:
                    prev_year = rec_comp
                years = date.today().year
                years = years + prev_year
                res_years_preview['years_preview'] = years
                years_preview.append(res_years_preview)
                data.update({
                    'date_from': str(years) + '-01-01',
                    'date_to': str(years) + '-12-31',
                })
                records = self._get_report_values(data)

                def final_record(rec_rprt):
                    if filters['account_tags'] != ['All']:
                        tag_accounts = list(map(lambda x: x.code, new_account_ids))

                        def filter_code(rec_dict):
                            if rec_dict['code'] in tag_accounts:
                                return True
                            else:
                                return False

                        new_records = list(filter(filter_code, records['Accounts']))
                        records['Accounts'] = new_records

                    account_report_id = self.env['account.financial.report'].search([
                        ('name', 'ilike', tag)])

                    new_data = {'id': self.id, 'date_from': False,
                                'enable_filter': True,
                                'debit_credit': True,
                                'date_to': False, 'account_report_id': account_report_id,
                                'target_move': filters['target_move'],
                                'view_format': 'vertical',
                                'company_id': self.company_id,
                                'used_context': {'journal_ids': False,
                                                 'state': filters['target_move'].lower(),
                                                 'date_from': filters['date_from'],
                                                 'date_to': filters['date_to'],
                                                 'strict_range': False,
                                                 'company_id': self.company_id,
                                                 'lang': 'en_US'}}

                    account_lines = self.get_account_lines(new_data)
                    report_lines = self.view_report_pdf(account_lines, new_data)[
                        'report_lines']
                        
                    move_line_accounts = []
                    move_lines_dict = {}

                    for rec in rec_rprt['Accounts']:
                        move_line_accounts.append(rec['code'])
                        move_lines_dict[rec['code']] = {}
                        move_lines_dict[rec['code']]['debit'] = rec['debit']
                        move_lines_dict[rec['code']]['credit'] = rec['credit']
                        move_lines_dict[rec['code']]['balance'] = rec['balance']

                    report_lines_move = []
                    parent_list = []

                    def filter_movelines_parents(obj):
                        for each in obj:
                            if each['report_type'] == 'accounts':
                                if each['code'] in move_line_accounts:
                                    report_lines_move.append(each)
                                    parent_list.append(each['p_id'])

                            elif each['report_type'] == 'account_report':
                                report_lines_move.append(each)
                            else:
                                report_lines_move.append(each)

                    filter_movelines_parents(report_lines)

                    for rec in report_lines_move:
                        if rec['report_type'] == 'accounts':
                            if rec['code'] in move_line_accounts:
                                rec['debit'] = move_lines_dict[rec['code']]['debit']
                                rec['credit'] = move_lines_dict[rec['code']]['credit']
                                rec['balance'] = move_lines_dict[rec['code']]['balance']

                    parent_list = list(set(parent_list))
                    max_level = 0
                    for rep in report_lines_move:
                        if rep['level'] > max_level:
                            max_level = rep['level']

                    def get_parents(obj):
                        for item in report_lines_move:
                            for each in obj:
                                if item['report_type'] != 'account_type' and \
                                        each in item['c_ids']:
                                    obj.append(item['r_id'])
                            if item['report_type'] == 'account_report':
                                obj.append(item['r_id'])
                                break

                    get_parents(parent_list)
                    for i in range(max_level):
                        get_parents(parent_list)

                    parent_list = list(set(parent_list))
                    final_report_lines = []

                    for rec in report_lines_move:
                        if rec['report_type'] != 'accounts':
                            if rec['r_id'] in parent_list:
                                final_report_lines.append(rec)
                        else:
                            final_report_lines.append(rec)

                    def filter_sum(obj):
                        sum_list = {}
                        for pl in parent_list:
                            sum_list[pl] = {}
                            sum_list[pl]['s_debit'] = 0
                            sum_list[pl]['s_credit'] = 0
                            sum_list[pl]['s_balance'] = 0

                        for each in obj:
                            if each['p_id'] and each['p_id'] in parent_list:
                                sum_list[each['p_id']]['s_debit'] += each['debit']
                                sum_list[each['p_id']]['s_credit'] += each['credit']
                                sum_list[each['p_id']]['s_balance'] += each['balance']
                        return sum_list

                    def assign_sum(obj):
                        for each in obj:
                            if each['r_id'] in parent_list and \
                                    each['report_type'] != 'account_report':
                                each['debit'] = sum_list_new[each['r_id']]['s_debit']
                                each['credit'] = sum_list_new[each['r_id']]['s_credit']

                    for p in range(max_level):
                        sum_list_new = filter_sum(final_report_lines)
                        assign_sum(final_report_lines)            

                    for rec in final_report_lines:
                        rec['debit'] = round(rec['debit'] * currency_rate, 2)
                        rec['credit'] = round(rec['credit'] * currency_rate, 2)
                        rec['balance'] = rec['debit'] - rec['credit']
                        rec['balance'] = round(rec['balance'], 2)
                        if (rec['balance_cmp'] * currency_rate < 0 and rec['balance'] > 0) or (
                                rec['balance_cmp'] * currency_rate > 0 and rec['balance'] < 0):
                            rec['balance'] = rec['balance'] * -1

                        if position == "before":
                            rec['m_debit'] = symbol + " " + "{:,.2f}".format(rec['debit'])
                            rec['m_credit'] = symbol + " " + "{:,.2f}".format(rec['credit'])
                            rec['m_balance'] = symbol + " " + "{:,.2f}".format(
                                rec['balance'])
                        else:
                            rec['m_debit'] = "{:,.2f}".format(rec['debit']) + " " + symbol
                            rec['m_credit'] = "{:,.2f}".format(rec['credit']) + " " + symbol
                            rec['m_balance'] = "{:,.2f}".format(
                                rec['balance']) + " " + symbol

                    return final_report_lines

                cat_report_line = []
                if data['analytic_groups']:            
                    for cat in records['Accounts']:
                        res_rprt = {'anl_id' : cat['anl_id'],
                                    'anl_name' : cat['anl_name'],
                                    'report_lines' : cat['Accounts'],
                                    'final_report_lines' : final_record(cat)}
                        cat_report_line.append(res_rprt)            
                else:
                    res_rprt = {'anl_id' : '00001',
                                'anl_name' : 'Balance',
                                'report_lines' : records['Accounts'],
                                'final_report_lines' : final_record(records)}
                    cat_report_line.append(res_rprt)

                
                
                number = 1
                for s in cat_report_line:
                    for q in s['final_report_lines']:
                        res_header={}
                        if 'code' in q:
                            for r in s['report_lines']:
                                res_tmp={}
                                if r['code'] == q['code']:
                                    code_acc = list(filter(lambda x: x['number'] == r['code'], vals_tmp))
                                    if code_acc:
                                        check_tmp = list(filter(lambda x: x['years'] == years, code_acc[0][s['anl_name']]))
                                        if check_tmp:
                                            for tmp in code_acc[0][s['anl_name']]:
                                                if tmp['years'] == years:
                                                    tmp['balance'] = q['m_balance']
                                        else:
                                            code_acc[0][s['anl_name']].append({'years' : years, 'balance' : q['m_balance']})
                                    else:
                                        res_tmp['code'] = r['code']
                                        res_tmp['name'] = r['name']
                                        res_tmp[s['anl_name']] = [{'years' : years, 'balance' : q['m_balance']}]
                                        res_tmp['number'] = r['code']
                                        vals_tmp.append(res_tmp)
                        else:
                            code_acc1 = list(filter(lambda x: x['name'] == q['name'], vals_tmp))                   
                            if code_acc1:
                                check_tmp = list(filter(lambda x: x['years'] == years, code_acc1[0][str(s['anl_name'])]))
                                if check_tmp:
                                    for tmp in code_acc1[0][str(s['anl_name'])]:
                                        if tmp['years'] == years:
                                            tmp['balance'] = q['m_balance']
                                else:
                                    code_acc1[0][str(s['anl_name'])].append({'years' : years, 'balance' : q['m_balance']})
                            else:
                                res_header['number'] = '0000' + str(number)
                                res_header['name'] = q['name']
                                res_header[s['anl_name']] = [{'years' : years, 'balance' : q['m_balance']}]
                                vals_tmp.append(res_header)
                                number = number+1

            return {
                'name': tag,
                'type': 'ir.actions.client',
                'tag': tag,
                'filters': filters,
                'report_lines': records['Accounts'],
                'debit_total': records['debit_total'],
                'credit_total': records['credit_total'],
                'debit_balance': records['debit_balance'],
                'currency': currency,
                'bs_lines': vals_tmp,
                'cat_report_line': cat_report_line,
                'years_preview': years_preview,
            }

        if data['comparison']:
            comparison = data['comparison']
            prev_comp = data['previous']
            rep_data = rec_report_comp(comparison,prev_comp)
            return rep_data
        else:
            rep_data = rec_report()
            return rep_data

    def get_filter(self, option):
        data = self.get_filter_data(option)
        filters = {}
        if data.get('journal_ids'):
            filters['journals'] = self.env['account.journal'].browse(
                data.get('journal_ids')).mapped('code')
        else:
            filters['journals'] = ['All']
        if data.get('account_ids', []):
            filters['accounts'] = self.env['account.account'].browse(
                data.get('account_ids', [])).mapped('code')
        else:
            filters['accounts'] = ['All']
        if data.get('target_move'):
            filters['target_move'] = data.get('target_move')
        else:
            filters['target_move'] = 'posted'
        if data.get('date_from'):
            filters['date_from'] = data.get('date_from')
        else:
            filters['date_from'] = False
        if data.get('date_to'):
            filters['date_to'] = data.get('date_to')
        else:
            filters['date_to'] = False
        if data.get('analytic_ids', []):
            filters['analytics'] = self.env['account.analytic.account'].browse(
                data.get('analytic_ids', [])).mapped('name')
        else:
            filters['analytics'] = ['All']

        if data.get('account_tag_ids'):
            filters['account_tags'] = self.env['account.account.tag'].browse(
                data.get('account_tag_ids', [])).mapped('name')
        else:
            filters['account_tags'] = ['All']

        if data.get('analytic_tag_ids', []):
            filters['analytic_tags'] = self.env['account.analytic.tag'].browse(
                data.get('analytic_tag_ids', [])).mapped('name')
        else:
            filters['analytic_tags'] = ['All']

        if data.get('analytic_group_ids', []):
            filters['analytic_groups'] = self.env['account.analytic.group'].browse(
                data.get('analytic_group_ids', [])).mapped('name')
        else:
            filters['analytic_groups'] = ['All']
        if data.get('comparison'):
            filters['comparison'] = data.get('comparison')
        else:
            filters['comparison'] = False
        if data.get('previous'):
            filters['previous'] = data.get('previous')
        else:
            filters['previous'] = False
        # if data.get('currencies'):
        #     filters['currencies'] = data.get('currencies')
        # else:
        #     filters['currencies'] = False
        if data.get('book'):
            filters['book'] = data.get('book')
        else:
            filters['book'] = 'commercial'

        filters['company_id'] = ''
        filters['accounts_list'] = data.get('accounts_list')
        filters['journals_list'] = data.get('journals_list')
        filters['analytic_list'] = data.get('analytic_list')
        filters['account_tag_list'] = data.get('account_tag_list')
        filters['analytic_tag_list'] = data.get('analytic_tag_list')
        filters['analytic_group_list'] = data.get('analytic_group_list')
        filters['company_name'] = data.get('company_name')
        filters['target_move'] = data.get('target_move').capitalize()
        # filters['comparison'] = data.get('comparison')
        # filters['previous'] = data.get('previous')
        filters['currencies'] = data.get('currencies')
        return filters

    def get_filter_data(self, option):
        r = self.env['ctm.dynamic.balance.sheet.report'].search(
            [('id', '=', option[0])])
        default_filters = {}
        company_id = self.env.company
        company_domain = [('company_id', '=', company_id.id)]
        journals = r.journal_ids if r.journal_ids else self.env[
            'account.journal'].search(company_domain)
        analytics = self.analytic_ids if self.analytic_ids else self.env[
            'account.analytic.account'].search(
            company_domain)
        account_tags = self.account_tag_ids if self.account_tag_ids else \
            self.env[
                'account.account.tag'].search([])
        analytic_tags = self.analytic_tag_ids if self.analytic_tag_ids else \
            self.env[
                'account.analytic.tag'].sudo().search(
                ['|', ('company_id', '=', company_id.id),
                 ('company_id', '=', False)])

        analytic_groups = self.analytic_group_ids if self.analytic_group_ids else \
            self.env[
                'account.analytic.group'].sudo().search(
                ['|', ('company_id', '=', company_id.id),
                 ('company_id', '=', False)])

        currency_ids = self.env['res.currency'].search([('active', '=', True)])

        if r.account_tag_ids:
            company_domain.append(
                ('tag_ids', 'in', r.account_tag_ids.ids))

        accounts = self.account_ids if self.account_ids else self.env[
            'account.account'].search(company_domain)
        filter_dict = {
            'journal_ids': r.journal_ids.ids,
            'account_ids': r.account_ids.ids,
            'analytic_ids': r.analytic_ids.ids,
            'company_id': company_id.id,
            'date_from': r.date_from,
            'date_to': r.date_to,
            'target_move': r.target_move,
            'journals_list': [(j.id, j.name, j.code) for j in journals],
            'accounts_list': [(a.id, a.name) for a in accounts],
            'analytic_list': [(anl.id, anl.name) for anl in analytics],
            'company_name': company_id and company_id.name,
            'analytic_tag_ids': r.analytic_tag_ids.ids,
            'analytic_tag_list': [(anltag.id, anltag.name) for anltag in
                                  analytic_tags],
            'analytic_group_ids': r.analytic_group_ids.ids,
            'analytic_group_list': [(anlgroup.id, anlgroup.name) for anlgroup in
                                  analytic_groups],
            'account_tag_ids': r.account_tag_ids.ids,
            'account_tag_list': [(a.id, a.name) for a in account_tags],
            'comparison': r.comparison,
            'previous': r.previous,
            'currencies': [{'name': currency_id.name, 'id': currency_id.id,} for currency_id in currency_ids]
        }
        filter_dict.update(default_filters)
        return filter_dict

    def _get_report_values(self, data):
        docs = data['model']
        display_account = data['display_account']
        init_balance = True
        journals = data['journals']
        accounts = self.env['account.account'].search([])
        if not accounts:
            raise UserError(_("No Accounts Found! Please Add One"))
        account_res = self._get_accounts(accounts, init_balance,
                                         display_account, data)
        
        debit_total = 0
        debit_total = 0
        credit_total = 0
        debit_balance = 0

        total_all = []
        if data['analytic_groups']:
            for x in account_res:
                res={}
                res['anl_id'] = x['anl_id']
                res['anl_name'] = x['anl_name']
                lines = x['Accounts']
                res['debit_total'] = sum(y['debit'] for y in lines)
                res['credit_total'] = sum(y['credit'] for y in lines)
                res['debit_balance'] = round(res['debit_total'], 2) - round(res['credit_total'], 2)
                total_all.append(res)
        else:
            debit_total = sum(x['debit'] for x in account_res)
            credit_total = sum(x['credit'] for x in account_res)
            debit_balance = round(debit_total, 2) - round(credit_total, 2)

        return {
            'doc_ids': self.ids,
            'debit_total': debit_total,
            'credit_total': credit_total,
            'debit_balance': debit_balance,
            'docs': docs,
            'time': time,
            'Accounts': account_res,
            'total_all': total_all,
        }

    @api.model
    def create(self, vals):
        vals['target_move'] = 'posted'
        res = super(BalanceSheetView, self).create(vals)
        return res

    def write(self, vals):
        if vals.get('target_move'):
            vals.update({'target_move': vals.get('target_move').lower()})
        if vals.get('journal_ids'):
            vals.update({'journal_ids': [(6, 0, vals.get('journal_ids'))]})
        if not vals.get('journal_ids'):
            vals.update({'journal_ids': [(5,)]})
        if vals.get('account_ids'):
            vals.update(
                {'account_ids': [(4, j) for j in vals.get('account_ids')]})
        if not vals.get('account_ids'):
            vals.update({'account_ids': [(5,)]})
        if vals.get('analytic_ids'):
            vals.update(
                {'analytic_ids': [(4, j) for j in vals.get('analytic_ids')]})
        if not vals.get('analytic_ids'):
            vals.update({'analytic_ids': [(5,)]})

        if vals.get('account_tag_ids'):
            vals.update({'account_tag_ids': [(4, j) for j in
                                             vals.get('account_tag_ids')]})
        if not vals.get('account_tag_ids'):
            vals.update({'account_tag_ids': [(5,)]})

        if vals.get('analytic_tag_ids'):
            vals.update({'analytic_tag_ids': [(4, j) for j in
                                              vals.get('analytic_tag_ids')]})
        if not vals.get('analytic_tag_ids'):
            vals.update({'analytic_tag_ids': [(5,)]})

        if vals.get('analytic_group_ids'):
            vals.update({'analytic_group_ids': [(6, 0, vals.get('analytic_group_ids'))]})
        
        if not vals.get('analytic_group_ids'):
            vals.update({'analytic_group_ids': [(5,)]})

        if vals.get('book'):
            vals.update({'book': vals.get('book').lower()})
        
        res = super(BalanceSheetView, self).write(vals)
        return res

    def _get_accounts(self, accounts, init_balance, display_account, data):
        cr = self.env.cr
        MoveLine = self.env['account.move.line']
        move_lines = {x: [] for x in accounts.ids}
        currency_id = self.env.company.currency_id

        # Prepare sql query base on selected parameters from wizard
        tables, where_clause, where_params = MoveLine._query_get()
        wheres = [""]
        if where_clause.strip():
            wheres.append(where_clause.strip())
        final_filters = " AND ".join(wheres)
        final_filters = final_filters.replace('account_move_line__move_id',
                                              'm').replace(
            'account_move_line', 'l')
        new_final_filter = final_filters

        if data['target_move'] == 'posted':
            new_final_filter += " AND m.state = 'posted'"
        else:
            new_final_filter += " AND m.state in ('draft','posted')"

        if data['book'] == 'fiscal':
            new_final_filter += " AND (m.is_fiscal_book_exclude isnull or m.is_fiscal_book_exclude = false)"

        if data.get('date_from'):
            new_final_filter += " AND l.date >= '%s'" % data.get('date_from')
        if data.get('date_to'):
            new_final_filter += " AND l.date <= '%s'" % data.get('date_to')

        if data['journals']:
            new_final_filter += ' AND j.id IN %s' % str(
                tuple(data['journals'].ids) + tuple([0]))

        if data.get('accounts'):
            WHERE = "WHERE l.account_id IN %s" % str(
                tuple(data.get('accounts').ids) + tuple([0]))
        else:
            WHERE = "WHERE l.account_id IN %s"

        if data['analytics']:
            WHERE += ' AND anl.id IN %s' % str(
                tuple(data.get('analytics').ids) + tuple([0]))

        if data['analytic_tags']:
            WHERE += ' AND anltag.account_analytic_tag_id IN %s' % str(
                tuple(data.get('analytic_tags').ids) + tuple([0]))

        if data['analytic_groups']:
            WHERE += ' AND anlgroup.id IN %s' % str(
                tuple(data.get('analytic_groups').ids) + tuple([0]))

        if data['analytic_groups']:
            # Get move lines base on sql query and Calculate the total balance of move lines
            sql = (''' SELECT anlacc.name, anlview.* FROM (
                    SELECT anl.id as anlid, l.id AS lid, m.id AS move_id, l.account_id AS account_id,
                    l.date AS ldate, j.code AS lcode, l.currency_id, l.amount_currency, l.ref AS lref,
                    l.name AS lname, COALESCE(l.debit,0) AS debit, COALESCE(l.credit,0) AS credit, 
                    COALESCE(SUM(l.balance),0) AS balance,
                    m.name AS move_name, c.symbol AS currency_code,c.position AS currency_position, p.name AS partner_name
                    FROM account_move_line l
                    JOIN account_move m ON (l.move_id=m.id)
                    LEFT JOIN res_currency c ON (l.currency_id=c.id)
                    LEFT JOIN res_partner p ON (l.partner_id=p.id)
                    LEFT JOIN account_analytic_line anltag ON (anltag.move_id = l.id)
                    LEFT JOIN account_analytic_account anl ON (anltag.account_id=anl.id)
                    left join account_analytic_group anlgroup on ( anl.group_id = anlgroup.id)
                    JOIN account_journal j ON (l.journal_id=j.id)
                    JOIN account_account acc ON (l.account_id = acc.id)
                    ''' + WHERE + '''
                    ''' + new_final_filter + '''
                    GROUP BY anl.id, l.id, m.id,  l.account_id, l.date, j.code, l.currency_id, l.amount_currency, l.ref, l.name, m.name, c.symbol, c.position, p.name
                    ) as anlview
                    inner join account_analytic_account anlacc on anlview.anlid = anlacc.id
                    ''')
        else:
            # Get move lines base on sql query and Calculate the total balance of move lines
            sql = ('''SELECT l.id AS lid,m.id AS move_id, l.account_id AS account_id,
                    l.date AS ldate, j.code AS lcode, l.currency_id, l.amount_currency, l.ref AS lref,
                    l.name AS lname, COALESCE(l.debit,0) AS debit, COALESCE(l.credit,0) AS credit, 
                    COALESCE(SUM(l.balance),0) AS balance,
                    m.name AS move_name, c.symbol AS currency_code,c.position AS currency_position, p.name AS partner_name
                    FROM account_move_line l
                    JOIN account_move m ON (l.move_id=m.id)
                    LEFT JOIN res_currency c ON (l.currency_id=c.id)
                    LEFT JOIN res_partner p ON (l.partner_id=p.id)
                    LEFT JOIN account_analytic_line anltag ON (anltag.move_id = l.id)
                    LEFT JOIN account_analytic_account anl ON (anltag.account_id=anl.id)
                    left join account_analytic_group anlgroup on ( anl.group_id = anlgroup.id)
                    JOIN account_journal j ON (l.journal_id=j.id)
                    JOIN account_account acc ON (l.account_id = acc.id)
                    ''' + WHERE + '''
                    ''' + new_final_filter + '''
                    GROUP BY l.id, m.id,  l.account_id, l.date, j.code, l.currency_id, l.amount_currency, l.ref, l.name, m.name, c.symbol, c.position, p.name''')
        
        if data.get('accounts'):
            params = tuple(where_params)
        else:
            params = (tuple(accounts.ids),) + tuple(where_params)

        cr.execute(sql, params)
        result_query = cr.dictfetchall()
        category_res = []
        move_cat=[]
        if data['analytic_groups']:
            for row in result_query:                               
                if len(category_res) > 0:
                    cek = list(filter(lambda x: x['code'] == row['anlid'], category_res))
                    if len(cek) > 0:
                        for cat in category_res:
                            if cat['code'] == row['anlid']:
                                cat['acc_list'].append(row)
                    else:
                        res={}
                        res['code'] = row['anlid']
                        res['name'] = row['name']
                        res['acc_list'] = [row]
                        category_res.append(res)        
                else:
                    res={}
                    res['code'] = row['anlid']
                    res['name'] = row['name']
                    res['acc_list'] = [row]
                    category_res.append(res)

            for row in category_res:
                res={}
                res['code'] = row['code']
                res['name'] = row['name']
                move_cat.append(res)

            for cat in move_cat:
                move_list = list(filter(lambda x: x['code'] == cat['code'], category_res))
                acc = accounts
                lines_cat = {x: [] for x in acc.ids}
                for row in move_list[0]['acc_list']:
                    balance = 0
                    for line in lines_cat.get(row['account_id']):
                        balance += round(line['debit'], 2) - round(line['credit'], 2)
                    row['balance'] += (round(balance, 2))
                    row['m_id'] = row['account_id']                    
                    lines_cat[row.pop('account_id')].append(row)
                cat['Accounts'] = lines_cat
        else:        
            for row in result_query:
                balance = 0
                for line in move_lines.get(row['account_id']):
                    balance += round(line['debit'], 2) - round(line['credit'], 2)
                row['balance'] += (round(balance, 2))
                row['m_id'] = row['account_id']
                move_lines[row.pop('account_id')].append(row)
        
        # Calculate the debit, credit and balance for Accounts
        account_res = []
        if data['analytic_groups']:
            for cat in move_cat:
                account_tmp = []
                rw={}
                for account in accounts:
                    currency = account.currency_id and account.currency_id or account.company_id.currency_id
                    res = dict((fn, 0.0) for fn in ['credit', 'debit', 'balance'])
                    res['code'] = account.code
                    res['name'] = account.name
                    res['id'] = account.id
                    moveline = cat['Accounts']
                    res['move_lines'] = moveline[account.id]
                    for line in res.get('move_lines'):
                        res['debit'] += round(line['debit'], 2)
                        res['credit'] += round(line['credit'], 2)
                        res['balance'] = round(line['balance'], 2)
                    if display_account == 'all':
                        account_tmp.append(res)
                    if display_account == 'movement' and res.get('move_lines'):
                        account_tmp.append(res)
                    if display_account == 'not_zero' and not currency.is_zero(res['balance']):
                        account_tmp.append(res)
                rw['anl_id'] = cat['code']
                rw['anl_name'] = cat['name']
                rw['Accounts'] = account_tmp
                account_res.append(rw)
        else:
            for account in accounts:
                currency = account.currency_id and account.currency_id or account.company_id.currency_id
                res = dict((fn, 0.0) for fn in ['credit', 'debit', 'balance'])
                res['code'] = account.code
                res['name'] = account.name
                res['id'] = account.id
                res['move_lines'] = move_lines[account.id]
                for line in res.get('move_lines'):
                    res['debit'] += round(line['debit'], 2)
                    res['credit'] += round(line['credit'], 2)
                    res['balance'] = round(line['balance'], 2)
                if display_account == 'all':
                    account_res.append(res)
                if display_account == 'movement' and res.get('move_lines'):
                    account_res.append(res)
                if display_account == 'not_zero' and not currency.is_zero(
                        res['balance']):
                    account_res.append(res)
        return account_res

    # @api.model
    # def _get_currency(self):
    #     journal = self.env['account.journal'].browse(
    #         self.env.context.get('default_journal_id', False))
    #     if journal.currency_id:
    #         return journal.currency_id.id
    #     currency_array = [self.env.company.currency_id.symbol,
    #                       self.env.company.currency_id.position]
    #     return currency_array
    @api.model
    def _get_currency(self):
        partner_ledger = self.search([], limit=1, order="id desc")
        report_currency_id = partner_ledger.report_currency_id
        journal = self.env['account.journal'].browse(
            self.env.context.get('default_journal_id', False))
        if journal.currency_id and not report_currency_id:
            return journal.currency_id.id
        lang = self.env.user.lang
        if not lang:
            lang = 'en_US'
        lang = lang.replace("_", '-')
        if not report_currency_id:
            currency_array = [self.env.company.currency_id.symbol,
                              self.env.company.currency_id.position, lang]
        else:
            currency_array = [report_currency_id.symbol,
                              report_currency_id.position, lang]
        return currency_array

    def get_dynamic_xlsx_report(self, options, response, report_data, dfr_data):
        i_data = str(report_data)
        filters = json.loads(options)
        j_data = dfr_data
        rl_data = json.loads(j_data)

        print(i_data)
        print(filters)
        print(j_data)
        print(rl_data)

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet()
        head = workbook.add_format({'align': 'center', 'bold': True,
                                    'font_size': '20px'})
        sub_heading = workbook.add_format(
            {'align': 'center', 'bold': True, 'font_size': '10px',
             'border': 1,
             'border_color': 'black'})
        side_heading_main = workbook.add_format(
            {'align': 'left', 'bold': True, 'font_size': '10px',
             'border': 1,
             'border_color': 'black'})

        side_heading_sub = workbook.add_format(
            {'align': 'left', 'bold': True, 'font_size': '10px',
             'border': 1,
             'border_color': 'black'})

        side_heading_sub.set_indent(1)
        txt = workbook.add_format({'font_size': '10px', 'border': 1})
        txt_name = workbook.add_format({'font_size': '10px', 'border': 1})
        txt_name_bold = workbook.add_format({'font_size': '10px', 'border': 1,
                                             'bold': True})
        txt_name.set_indent(2)
        txt_name_bold.set_indent(2)

        txt = workbook.add_format({'font_size': '10px', 'border': 1})

        sheet.merge_range('A2:D3',
                          filters.get('company_name') + ' : ' + i_data,
                          head)
        date_head = workbook.add_format({'align': 'center', 'bold': True,
                                         'font_size': '10px'})

        date_head.set_align('vcenter')
        date_head.set_text_wrap()
        date_head.set_shrink()
        date_head_left = workbook.add_format({'align': 'left', 'bold': True,
                                              'font_size': '10px'})

        date_head_right = workbook.add_format({'align': 'right', 'bold': True,
                                               'font_size': '10px'})

        date_head_left.set_indent(1)
        date_head_right.set_indent(1)

        if filters.get('date_from'):
            sheet.merge_range('A4:B4', 'From: ' + filters.get('date_from'),
                              date_head_left)
        if filters.get('date_to'):
            sheet.merge_range('C4:D4', 'To: ' + filters.get('date_to'),
                              date_head_right)

        sheet.merge_range('A5:D6', '  Accounts: ' + ', '.join(
            [lt or '' for lt in
             filters['accounts']]) + ';  Journals: ' + ', '.join(
            [lt or '' for lt in
             filters['journals']]) + ';  Account Tags: ' + ', '.join(
            [lt or '' for lt in
             filters['account_tags']]) + ';  Analytic Tags: ' + ', '.join(
            [lt or '' for lt in
             filters['analytic_tags']]) + ';Analytic Groups: ' + ', '.join(
            [lt or '' for lt in
             filters['analytic_groups']]) + ';  Analytic: ' + ', '.join(
            [at or '' for at in
             filters['analytics']]) + ';  Target Moves: ' + filters.get(
            'target_move').capitalize(), date_head)

        sheet.set_column(0, 0, 30)
        sheet.set_column(1, 1, 20)
        sheet.set_column(2, 2, 15)
        sheet.set_column(3, 3, 15)

        row = 5
        col = 0

        row += 2

        sheet.write(row, col, '', sub_heading)
        if rl_data['years_preview']:
            for year in rl_data['years_preview']:
                col += 1
                sheet.write(row, col, year['years_preview'], sub_heading)
        else:
            for cat in rl_data['cat_report_line']:
                sheet.write(row, col, cat['anl_name'], sub_heading)
        if rl_data['bs_lines']:
            for a in rl_data['bs_lines']:
                row += 1
                col = 0
                if 'code' in a:
                    sheet.write(row, col, a['code'] + ' - ' + a['name'], side_heading_sub)
                else:
                    sheet.write(row, col, a['name'], side_heading_main)
                if rl_data['years_preview']:
                    for account in rl_data['cat_report_line']:
                        if a[account['anl_name']]:
                            for anl_name in a[account['anl_name']]:
                                col += 1
                                if anl_name['balance']:
                                    sheet.write(row, col, anl_name['balance'], txt)
                else:
                    for account in rl_data['cat_report_line']:
                        col += 1
                        if a[account['anl_name']]:
                            sheet.write(row, col, a[account['anl_name']], txt)
        workbook.close()
        output.seek(0)
        response.stream.write(output.read())
        output.close()

        