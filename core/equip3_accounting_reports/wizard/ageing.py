
import time
from datetime import datetime

from dateutil.relativedelta import relativedelta
from odoo import fields, models, api, _
from odoo.tools import float_is_zero



class AgeingView(models.TransientModel):
    _inherit = "account.partner.ageing"

    @api.model
    def view_report(self, option):
        res = super(AgeingView, self).view_report(option)
        currency_ids = self.env['res.currency'].search([('active', '=', True)])
        res['currencies'] = [{
            'name': currency_id.name,
            'id': currency_id.id,
        } for currency_id in currency_ids]
        return res

    def _get_partner_move_lines(self, data, partners, date_from, target_move,
                                account_type,
                                period_length):
        periods = {}

        ageing_report = self.search([], limit=1, order="id desc")
        report_currency_id = ageing_report.report_currency_id
        currency_rate = 0
        start = datetime.strptime(date_from, "%Y-%m-%d")
        date_from = datetime.strptime(date_from, "%Y-%m-%d").date()
        for i in range(5)[::-1]:
            stop = start - relativedelta(days=period_length)
            period_name = str((5 - (i + 1)) * period_length + 1) + '-' + str(
                (5 - i) * period_length)
            period_stop = (start - relativedelta(days=1)).strftime('%Y-%m-%d')
            if i == 0:
                period_name = '+' + str(4 * period_length)
            periods[str(i)] = {
                'name': period_name,
                'stop': period_stop,
                'start': (i != 0 and stop.strftime('%Y-%m-%d') or False),
            }
            start = stop
        res = []
        total = []
        cr = self.env.cr
        user_company = self.env.company

        user_currency = user_company.currency_id

        if date_from and report_currency_id:
            rate_ids = report_currency_id.rate_ids.filtered(lambda r: r.name >= date_from).sorted(key=lambda r: r.name)
            if rate_ids:
                currency_rate = rate_ids[-1].mr_rate
            else:
                currency_rate = report_currency_id.rate

        ResCurrency = self.env['res.currency'].with_context(date=date_from)
        company_ids = self._context.get('company_ids') or [user_company.id]
        move_state = ['draft', 'posted']
        if target_move == 'posted':
            move_state = ['posted']
        arg_list = (tuple(move_state), tuple(account_type))

        reconciliation_clause = '(l.reconciled IS FALSE)'
        cr.execute(
            'SELECT debit_move_id, credit_move_id FROM account_partial_reconcile where max_date > %s',
            (date_from,))
        reconciled_after_date = []
        for row in cr.fetchall():
            reconciled_after_date += [row[0], row[1]]
        if reconciled_after_date:
            reconciliation_clause = '(l.reconciled IS FALSE OR l.id IN %s)'
            arg_list += (tuple(reconciled_after_date),)

        arg_list += (date_from, tuple(company_ids),)
        partner_list = '(l.partner_id IS NOT  NULL)'
        if partners:
            list = tuple(partners.ids) + tuple([0])
            if list:
                partner_list = '(l.partner_id IS NULL OR l.partner_id IN %s)'
                arg_list += (tuple(list),)
        query = '''
                    SELECT DISTINCT l.partner_id, UPPER(res_partner.name)
                    FROM account_move_line AS l left join res_partner on l.partner_id = res_partner.id, account_account, account_move am
                    WHERE (l.account_id = account_account.id)
                        AND (l.move_id = am.id)
                        AND (am.state IN %s)
                        AND (account_account.internal_type IN %s)
                       
                        AND ''' + reconciliation_clause + '''          
                        AND (l.date <= %s)
                        AND l.company_id IN %s
                        AND ''' + partner_list + '''
                           
                    ORDER BY UPPER(res_partner.name)'''
        cr.execute(query, arg_list)


        partners = cr.dictfetchall()

        # put a total of 0
        for i in range(7):
            total.append(0)

        # Build a string like (1,2,3) for easy use in SQL query
        partner_ids = [partner['partner_id'] for partner in partners if
                       partner['partner_id']]

        lines = dict(
            (partner['partner_id'] or False, []) for partner in partners)
        if not partner_ids:
            return [], [], {}

        # This dictionary will store the not due amount of all partners
        undue_amounts = {}
        query = '''SELECT l.id
                        FROM account_move_line AS l, account_account, account_move am
                        WHERE (l.account_id = account_account.id) AND (l.move_id = am.id)
                            AND (am.state IN %s)
                            AND (account_account.internal_type IN %s)
                            AND (COALESCE(l.date_maturity,l.date) >= %s)\
                            AND ((l.partner_id IN %s) OR (l.partner_id IS NULL))
                        AND (l.date <= %s)
                        AND l.company_id IN %s'''
        cr.execute(query, (
            tuple(move_state), tuple(account_type), date_from,
            tuple(partner_ids), date_from, tuple(company_ids)))
        aml_ids = cr.fetchall()
        aml_ids = aml_ids and [x[0] for x in aml_ids] or []
        for line in self.env['account.move.line'].browse(aml_ids):
            partner_id = line.partner_id.id or False
            move_id = line.move_id.id
            move_name = line.move_id.name
            date_maturity = line.date_maturity
            account_id = line.account_id.name
            account_code = line.account_id.code
            jrnl_id = line.journal_id.name
            if report_currency_id:
                currency_id = report_currency_id.position
                currency_symbol = report_currency_id.symbol
            else:
                currency_id = line.company_id.currency_id.position
                currency_symbol = line.company_id.currency_id.symbol

            if partner_id not in undue_amounts:
                undue_amounts[partner_id] = 0.0
            line_amount = ResCurrency._compute(line.company_id.currency_id,
                                               user_currency, line.balance)
            if user_currency.is_zero(line_amount):
                continue
            for partial_line in line.matched_debit_ids:
                if partial_line.max_date <= date_from:
                    line_amount += ResCurrency._compute(
                        partial_line.company_id.currency_id, user_currency,
                        partial_line.amount)
            for partial_line in line.matched_credit_ids:
                if partial_line.max_date <= date_from:
                    line_amount -= ResCurrency._compute(
                        partial_line.company_id.currency_id, user_currency,
                        partial_line.amount)
            if not self.env.company.currency_id.is_zero(line_amount):
                if currency_rate > 0:
                    line_amount *= currency_rate
                undue_amounts[partner_id] += line_amount
                if partner_id:
                    lines[partner_id].append({
                        'line': line,
                        'partner_id': partner_id,
                        'move': move_name,
                        'jrnl': jrnl_id,
                        'currency': currency_id,
                        'symbol': currency_symbol,
                        'acc_name': account_id,
                        'mov_id': move_id,
                        'acc_code': account_code,
                        'date': date_maturity,
                        'amount': line_amount,
                        'period6': 6,
                    })

        # Use one query per period and store results in history (a list variable)
        # Each history will contain: history[1] = {'<partner_id>': <partner_debit-credit>}
        history = []
        for i in range(5):
            args_list = (
                tuple(move_state), tuple(account_type), tuple(partner_ids),)
            dates_query = '(COALESCE(l.date_maturity,l.date)'

            if periods[str(i)]['start'] and periods[str(i)]['stop']:
                dates_query += ' BETWEEN %s AND %s)'

                args_list += (
                    periods[str(i)]['start'], periods[str(i)]['stop'])
            elif periods[str(i)]['start']:
                dates_query += ' >= %s)'

                args_list += (periods[str(i)]['start'],)
            else:
                dates_query += ' <= %s)'
                args_list += (periods[str(i)]['stop'],)

            args_list += (date_from, tuple(company_ids))

            query = '''SELECT l.id
                            FROM account_move_line AS l, account_account, account_move am
                            WHERE (l.account_id = account_account.id) AND (l.move_id = am.id)
                                AND (am.state IN %s)
                                AND (account_account.internal_type IN %s)
                                AND ((l.partner_id IN %s) OR (l.partner_id IS NULL))
                                AND ''' + dates_query + '''
                                
                                
                            AND (l.date <= %s)
                            AND l.company_id IN %s'''
            cr.execute(query, args_list)

            partners_amount = {}
            aml_ids = cr.fetchall()
            aml_ids = aml_ids and [x[0] for x in aml_ids] or []
            for line in self.env['account.move.line'].browse(aml_ids):
                partner_id = line.partner_id.id or False
                move_id = line.move_id.id
                move_name = line.move_id.name
                date_maturity = line.date_maturity
                account_id = line.account_id.name
                account_code = line.account_id.code
                jrnl_id = line.journal_id.name
                if report_currency_id:
                    currency_id = report_currency_id.position
                    currency_symbol = report_currency_id.symbol
                else:
                    currency_id = line.company_id.currency_id.position
                    currency_symbol = line.company_id.currency_id.symbol
                if partner_id not in partners_amount:
                    partners_amount[partner_id] = 0.0
                line_amount = ResCurrency._compute(line.company_id.currency_id,
                                                   user_currency, line.balance)
                if user_currency.is_zero(line_amount):
                    continue
                for partial_line in line.matched_debit_ids:
                    if partial_line.max_date <= date_from:
                        line_amount += ResCurrency._compute(
                            partial_line.company_id.currency_id, user_currency,
                            partial_line.amount)
                for partial_line in line.matched_credit_ids:
                    if partial_line.max_date <= date_from:
                        line_amount -= ResCurrency._compute(
                            partial_line.company_id.currency_id, user_currency,
                            partial_line.amount)

                if not self.env.company.currency_id.is_zero(
                        line_amount):
                    if currency_rate > 0:
                        line_amount *= currency_rate
                    partners_amount[partner_id] += line_amount
                    if i + 1 == 5:
                        period5 = i + 1
                        if partner_id:
                            lines[partner_id].append({
                                'period5': period5,
                                'line': line,
                                'partner_id': partner_id,
                                'move': move_name,
                                'currency': currency_id,
                                'symbol': currency_symbol,
                                'jrnl': jrnl_id,
                                'acc_name': account_id,
                                'mov_id': move_id,
                                'acc_code': account_code,
                                'date': date_maturity,
                                'amount': line_amount,
                            })
                    elif i + 1 == 4:
                        period4 = i + 1
                        if partner_id:
                            lines[partner_id].append({

                                'period4': period4,
                                'line': line,
                                'partner_id': partner_id,
                                'move': move_name,
                                'jrnl': jrnl_id,
                                'acc_name': account_id,
                                'currency': currency_id,
                                'symbol': currency_symbol,
                                'mov_id': move_id,
                                'acc_code': account_code,
                                'date': date_maturity,
                                'amount': line_amount,
                            })
                    elif i + 1 == 3:
                        period3 = i + 1
                        if partner_id:
                            lines[partner_id].append({

                                'period3': period3,
                                'line': line,
                                'partner_id': partner_id,
                                'move': move_name,
                                'jrnl': jrnl_id,
                                'acc_name': account_id,
                                'currency': currency_id,
                                'symbol': currency_symbol,
                                'mov_id': move_id,
                                'acc_code': account_code,
                                'date': date_maturity,
                                'amount': line_amount,
                            })
                    elif i + 1 == 2:
                        period2 = i + 1
                        if partner_id:
                            lines[partner_id].append({

                                'period2': period2,
                                'line': line,
                                'partner_id': partner_id,
                                'move': move_name,
                                'jrnl': jrnl_id,
                                'acc_name': account_id,
                                'currency': currency_id,
                                'symbol': currency_symbol,
                                'mov_id': move_id,
                                'acc_code': account_code,
                                'date': date_maturity,
                                'amount': line_amount,
                            })
                    else:
                        period1 = i + 1
                        if partner_id:
                            lines[partner_id].append({

                                'period1': period1,
                                'line': line,
                                'partner_id': partner_id,
                                'move': move_name,
                                'jrnl': jrnl_id,
                                'acc_name': account_id,
                                'currency': currency_id,
                                'symbol': currency_symbol,
                                'mov_id': move_id,
                                'acc_code': account_code,
                                'date': date_maturity,
                                'amount': line_amount,
                            })

            history.append(partners_amount)

        for partner in partners:
            if partner['partner_id'] is None:
                partner['partner_id'] = False
            at_least_one_amount = False
            values = {}
            undue_amt = 0.0
            if partner[
                'partner_id'] in undue_amounts:  # Making sure this partner actually was found by the query
                undue_amt = undue_amounts[partner['partner_id']]

            total[6] = total[6] + undue_amt
            values['direction'] = undue_amt
            for rec in lines:
                if partner['partner_id'] == rec:
                    child_lines = lines[rec]
            values['child_lines'] = child_lines
            if not float_is_zero(values['direction'],
                                 precision_rounding=self.env.company.currency_id.rounding):
                at_least_one_amount = True

            for i in range(5):
                during = False
                if partner['partner_id'] in history[i]:
                    during = [history[i][partner['partner_id']]]
                # Adding counter
                total[(i)] = total[(i)] + (during and during[0] or 0)
                values[str(i)] = during and during[0] or 0.0
                if not float_is_zero(values[str(i)],
                                     precision_rounding=self.env.company.currency_id.rounding):
                    at_least_one_amount = True
            values['total'] = sum(
                [values['direction']] + [values[str(i)] for i in range(5)])
            ## Add for total
            total[(i + 1)] += values['total']
            values['partner_id'] = partner['partner_id']
            if partner['partner_id']:
                browsed_partner = self.env['res.partner'].browse(
                    partner['partner_id'])
                values['name'] = browsed_partner.name and len(
                    browsed_partner.name) >= 45 and browsed_partner.name[
                                                    0:40] + '...' or browsed_partner.name
                values['trust'] = browsed_partner.trust
            else:
                values['name'] = _('Unknown Partner')
                values['trust'] = False

            if at_least_one_amount or (
                    self._context.get('include_nullified_amount') and lines[
                partner['partner_id']]):
                res.append(values)

        return res, total, lines

    @api.model
    def _get_currency(self):
        ageing_report = self.search([], limit=1, order="id desc")
        report_currency_id = ageing_report.report_currency_id
        journal = self.env['account.journal'].browse(
            self.env.context.get('default_journal_id', False))
        if journal.currency_id and not report_currency_id:
            return journal.currency_id.id
        lang = self.env.user.lang
        if not lang:
            lang = 'en_US'
        lang = lang.replace("_", '-')
        if not report_currency_id:
            currency_array = [self.env.company.currency_id.symbol,
                              self.env.company.currency_id.position, lang]
        else:
            currency_array = [report_currency_id.symbol,
                              report_currency_id.position, lang]
        return currency_array
