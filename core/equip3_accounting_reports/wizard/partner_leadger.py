
from odoo import fields, models, api, _


class PartnerView(models.TransientModel):
    _inherit = "account.partner.ledger"

    @api.model
    def view_report(self, option):
        res = super(PartnerView, self).view_report(option)
        currency_ids = self.env['res.currency'].search([('active', '=', True)])
        res['currencies'] = [{
            'name': currency_id.name,
            'id': currency_id.id,
        } for currency_id in currency_ids]
        return res

    def _get_partners(self, partners, accounts, init_balance, display_account, data):

        cr = self.env.cr
        move_line = self.env['account.move.line']
        move_lines = {x: [] for x in partners.ids}
        partner_ledger = self.search([], limit=1, order="id desc")
        report_currency_id = partner_ledger.report_currency_id
        currency_rate = 0
        if not report_currency_id:
            currency_id = self.env.company.currency_id
        else:
            currency_id = report_currency_id
            if data.get('date_from') and data.get('date_to'):
                rate_ids = currency_id.rate_ids.filtered(lambda r: r.name >= data.get('date_from') and r.name <= data.get('date_to')).sorted(key=lambda r: r.name)
                if rate_ids:
                    currency_rate = rate_ids[-1].mr_rate
                else:
                    currency_rate = currency_id.rate
            elif data.get('date_from'):
                rate_ids = currency_id.rate_ids.filtered(lambda r: r.name >= data.get('date_from')).sorted(key=lambda r: r.name)
                if rate_ids:
                    currency_rate = rate_ids[-1].mr_rate
                else:
                    currency_rate = currency_id.rate
            elif data.get('date_to'):
                rate_ids = currency_id.rate_ids.filtered(lambda r: r.name <= data.get('date_to')).sorted(key=lambda r: r.name)
                if rate_ids:
                    currency_rate = rate_ids[-1].mr_rate
                else:
                    currency_rate = currency_id.rate
            else:
                currency_rate = currency_id.rate
        tables, where_clause, where_params = move_line._query_get()
        wheres = [""]
        if where_clause.strip():
            wheres.append(where_clause.strip())
        final_filters = " AND ".join(wheres)
        final_filters = final_filters.replace('account_move_line__move_id', 'm').replace(
            'account_move_line', 'l')
        new_final_filter = final_filters
        if data['target_move'] == 'posted':
            new_final_filter += " AND m.state = 'posted'"
        else:
            new_final_filter += " AND m.state in ('draft','posted')"
        if data.get('date_from'):
            new_final_filter += " AND l.date >= '%s'" % data.get('date_from')
        if data.get('date_to'):
            new_final_filter += " AND l.date <= '%s'" % data.get('date_to')

        if data['journals']:
            new_final_filter += ' AND j.id IN %s' % str(tuple(data['journals'].ids) + tuple([0]))

        if data.get('accounts'):
            WHERE = "WHERE l.account_id IN %s" % str(tuple(data.get('accounts').ids) + tuple([0]))
        else:
            WHERE = "WHERE l.account_id IN %s"

        if data.get('partners'):
            WHERE += ' AND p.id IN %s' % str(
                tuple(data.get('partners').ids) + tuple([0]))

        if data.get('reconciled') == 'unreconciled':
            WHERE += ' AND l.full_reconcile_id is null AND' \
                     ' l.balance != 0 AND a.reconcile is true'

        sql = ('''SELECT l.id AS lid,l.partner_id AS partner_id,m.id AS move_id, 
                    l.account_id AS account_id, l.date AS ldate, j.code AS lcode, l.currency_id, 
                    l.amount_currency, l.ref AS lref, l.name AS lname, 
                    COALESCE(l.debit,0) AS debit, COALESCE(l.credit,0) AS credit, 
                    COALESCE(SUM(l.balance),0) AS balance,\
                    m.name AS move_name, c.symbol AS currency_code,c.position AS currency_position, p.name AS partner_name\
                    FROM account_move_line l\
                    JOIN account_move m ON (l.move_id=m.id)\
                    JOIN account_account a ON (l.account_id=a.id)
                    LEFT JOIN res_currency c ON (l.currency_id=c.id)\
                    LEFT JOIN res_partner p ON (l.partner_id=p.id)\
                    JOIN account_journal j ON (l.journal_id=j.id)\
                    JOIN account_account acc ON (l.account_id = acc.id) '''
                    + WHERE + new_final_filter + ''' GROUP BY l.id, m.id,  l.account_id, l.date, j.code, l.currency_id, l.amount_currency, l.ref, l.name, m.name, c.symbol, c.position, p.name''' )
        if data.get('accounts'):
            params = tuple(where_params)
        else:
            params = (tuple(accounts.ids),) + tuple(where_params)
        cr.execute(sql, params)

        account_list = { x.id : {'name' : x.name, 'code': x.code} for x in accounts}

        for row in cr.dictfetchall():
            balance = 0
            if row['partner_id'] in move_lines:
                for line in move_lines.get(row['partner_id']):
                    balance += round(line['debit'],2) - round(line['credit'],2)
                row['balance'] += (round(balance, 2))
                row['m_id'] = row['account_id']
                row['account_name'] = account_list[row['account_id']]['name'] + "(" +account_list[row['account_id']]['code'] + ")"
                move_lines[row.pop('partner_id')].append(row)

        partner_res = []
        for partner in partners:
            company_id = self.env.company
            if not report_currency_id:
                currency = company_id.currency_id
            else:
                currency = report_currency_id
            res = dict((fn, 0.0) for fn in ['credit', 'debit', 'balance'])
            res['name'] = partner.name
            res['id'] = partner.id
            res['move_lines'] = move_lines[partner.id]
            for line in res.get('move_lines'):
                line['currency_position'] = currency.position
                line['currency_code'] = currency.symbol
                if currency_rate > 0:
                    line['debit'] = round(line['debit'] * currency_rate, 2)
                    line['credit'] = round(line['credit'] * currency_rate, 2)
                    line['balance'] = round(line['balance'] * currency_rate, 2)

                res['debit'] += round(line['debit'], 2)
                res['credit'] += round(line['credit'], 2)
                res['balance'] = round(line['balance'], 2)

            if display_account == 'all':
                partner_res.append(res)
            if display_account == 'movement' and res.get('move_lines'):
                partner_res.append(res)
            if display_account == 'not_zero' and not currency.is_zero(
                    res['balance']):
                partner_res.append(res)
        return partner_res

    @api.model
    def _get_currency(self):
        partner_ledger = self.search([], limit=1, order="id desc")
        report_currency_id = partner_ledger.report_currency_id
        journal = self.env['account.journal'].browse(
            self.env.context.get('default_journal_id', False))
        if journal.currency_id and not report_currency_id:
            return journal.currency_id.id
        lang = self.env.user.lang
        if not lang:
            lang = 'en_US'
        lang = lang.replace("_", '-')
        if not report_currency_id:
            currency_array = [self.env.company.currency_id.symbol,
                              self.env.company.currency_id.position, lang]
        else:
            currency_array = [report_currency_id.symbol,
                              report_currency_id.position, lang]
        return currency_array
