import time
from datetime import datetime

from odoo import models, api, fields
FETCH_RANGE = 2000
import io
import json
try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    import xlsxwriter
year = datetime.now().year


class AccountCasgFlow(models.TransientModel):
    _name = "account.cash.flow.statement"
    _inherit = "account.common.report"

    date_from = fields.Date(string="Start Date", default=str(year)+'-01-01')
    date_to = fields.Date(string="End Date", default=fields.Date.today)
    today = fields.Date("Report Date", default=fields.Date.today)
    levels = fields.Selection([('summary', 'Summary'),
                               ('consolidated', 'Consolidated'),
                               ('detailed', 'Detailed'),
                               ('very', 'Very Detailed')],
                              string='Levels', required=True, default='summary',
                              help='Different levels for cash flow statements \n'
                                   'Summary: Month wise report.\n'
                                   'Consolidated: Based on account types.\n'
                                   'Detailed: Based on accounts.\n'
                                   'Very Detailed: Accounts with their move lines')

    account_ids = fields.Many2many(
        "account.account",
        string="Accounts",
    )
    company_ids = fields.Many2many(
    "res.company",
    string="Companies"
    )

    @api.model
    def view_report(self, option):
        r = self.env['account.cash.flow.statement'].search([('id', '=', option[0])])
        data = {
            'model': self,
            'journals': r.journal_ids,
            'target_move': r.target_move,
            'levels': r.levels,
            'company_ids': r.company_ids,
            
        }
        if r.date_from:
            data.update({
                'date_from': r.date_from,
            })
        if r.date_to:
            data.update({
                'date_to': r.date_to,
            })

        filters = self.get_filter(option)
        report_lines = self._get_report_values(data, option)
        fetched_data = report_lines['fetched_data']
        fetched = report_lines['fetched']
        account_res = report_lines['account_res']
        journal_res = report_lines['journal_res']
        levels = report_lines['levels']
        currency = self._get_currency()
        received_customer = self._sum_list(report_lines['received_customer'],'Advance payments received from customers')
        cash_received = self._sum_list(report_lines['cash_received'],'Cash received from')
        payment_supplier = self._sum_list(report_lines['payment_supplier'],'Advance payments made to suppliers')
        cash_paid = self._sum_list(report_lines['cash_paid'],'Cash paid for')
        sum_cf_statement = self._sum_list(received_customer + cash_received + payment_supplier + cash_paid,'Total cash flow from operating activities')
        cf_investing = self._sum_list(report_lines['cf_investing'], "cf_investing")
        cf_finance = self._sum_list(report_lines['cf_finance'], "cf_finance")
        cf_unclass = self._check_list(self._sum_list(report_lines['cf_unclass'], "cf_unclass"))
        sum_all_cf_statement = self._sum_list(sum_cf_statement + cf_investing + cf_finance + cf_unclass,'Net increase in cash and cash equivalents')
        cf_beginning_period = self._sum_list(report_lines['cf_beginning_period'], "Cash and cash equivalents, beginning of period")
        cf_closing_period = self._sum_list(sum_all_cf_statement + cf_beginning_period,'Cash and cash equivalents, closing of period')

        return {
            'name': "Cash Flow Report",
            'type': 'ir.actions.client',
            'tag': 'c_f_s',
            'report_lines': report_lines,
            'fetched_data': fetched_data,
            'fetched': fetched,
            'account_res': account_res,
            'journal_res': journal_res,
            'levels': r.levels,
            'filters': filters,
            'currency': currency,
            'received_customer': received_customer,
            'cash_received': cash_received,
            'payment_supplier': payment_supplier,
            'cash_paid': cash_paid,
            'sum_cf_statement': sum_cf_statement,
            'cf_investing': cf_investing,
            'cf_finance': cf_finance,
            'cf_unclass': cf_unclass,
            'sum_all_cf_statement': sum_all_cf_statement,
            'cf_beginning_period': cf_beginning_period,
            'cf_closing_period': cf_closing_period,
            
            
        }

    def _check_list(self,list_value):
        values=[]
        check = False
        for rec in list_value:
            if rec['total_debit'] > 0 or rec['total_credit'] > 0 or rec['total_balance'] > 0:
                check = True
        if check == True:
            values = list_value
        return values

    def _sum_list(self,list_value, value_name):
        values=[]
        value =  {'month_part': value_name, 
                  'year_part': 2022, 
                  'total_debit': 0.0, 
                  'total_credit': 0.0, 
                  'total_balance': 0.0}
        for rec in list_value:
            value['total_debit'] += rec['total_debit']
            value['total_credit'] += rec['total_credit']
            value['total_balance'] += rec['total_balance']
        values.append(value)
        return values

    def _sum_list_minus(self,list_value, value_name):
        values=[]
        value =  {'month_part': value_name, 
                  'year_part': 2022, 
                  'total_debit': 0.0, 
                  'total_credit': 0.0, 
                  'total_balance': 0.0}
        i = 0
        max_len = len(list_value)
        while i < max_len:
            if i == 0:
                value['total_debit'] = list_value[i]['total_debit']
                value['total_credit'] = list_value[i]['total_credit']
                value['total_balance'] = list_value[i]['total_balance']
                i += 1
            else:
                value['total_debit'] -= list_value[i]['total_debit']
                value['total_credit'] -= list_value[i]['total_credit']
                value['total_balance'] -= list_value[i]['total_balance']
                i += 1
        values.append(value)
        return values

    def get_filter(self, option):
        data = self.get_filter_data(option)
        filters = {}
        if data.get('journal_ids'):
            filters['journals'] = self.env['account.journal'].browse(data.get('journal_ids')).mapped('code')
        else:
            filters['journals'] = ['All']
        if data.get('account_ids', []):
            filters['accounts'] = self.env['account.account'].browse(data.get('account_ids', [])).mapped('code')
        else:
            filters['accounts'] = ['All']

        filters['company_id'] = self.env['res.company'].browse(self.env.company.ids).mapped('name')

        if data.get('target_move'):
            filters['target_move'] = data.get('target_move')
        if data.get('date_from'):
            filters['date_from'] = data.get('date_from')
        if data.get('date_to'):
            filters['date_to'] = data.get('date_to')
        if data.get('levels'):
            filters['levels'] = data.get('levels')

        filters['company_id'] = self.env.company
        filters['companies_list'] = data.get('companies')
        filters['accounts_list'] = data.get('accounts_list')
        filters['journals_list'] = data.get('journals_list')
        filters['company_name'] = data.get('company_name')
        filters['target_move'] = data.get('target_move').capitalize()

        return filters

    def get_filter_data(self, option):
        r = self.env['account.cash.flow.statement'].search([('id', '=', option[0])])
        default_filters = {}
        company_id = self.env.company
        company_ids = self.env.company
        company_domain = [('company_id', '=', company_id.id)]
        journals = r.journal_ids if r.journal_ids else self.env['account.journal'].search(company_domain)
        accounts = self.account_ids if self.account_ids else self.env['account.account'].search(company_domain)
        companies = self.company_ids if self.company_ids else self.env['res.company'].search([])


        filter_dict = {
            'journal_ids': r.journal_ids.ids,
            'account_ids': self.account_ids.ids,
            'company_id': company_id.id,
            'date_from': r.date_from,
            'date_to': r.date_to,
            'levels': r.levels,
            'target_move': r.target_move,
            'journals_list': [(j.id, j.name, j.code) for j in journals],
            'accounts_list': [(a.id, a.name) for a in accounts],
            'companies': [(c.id, c.name) for c in companies],
            'company_name': company_id and company_id.name,
        }
        filter_dict.update(default_filters)
        return filter_dict

    def _query_1(self, opt, state, account_type_id, data, tag_id=False, opt2=False):        
        if not opt2:
            option = 'inner'
        else:
            option = opt2            
        print(option)
        if not tag_id:
            tags = ""
        else:
            tags = """ and account_account_tag_id = '""" + str(tag_id) + """' """

        print(tags)
        # query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
        #             sum(am.debit) AS total_debit, 
        #             sum(am.credit) AS total_credit,
        #             sum(am.credit) - sum(am.debit) AS total_balance       
        #             from ( select am.date, am.debit, am.credit, am.balance, am.account_account_tag_id, am.state, am.is_reconciled, am.partner_type, am.payment_type FROM 
        #             (select am.name as nameam,aj.type,aa.code,aa.name as nameaa,tags.name as nametags, am.date, am.id, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
        #                 FROM account_move as am
        #                 left join account_move_line aml ON am.id = aml.move_id
        #                 left JOIN account_account aa ON aa.id = aml.account_id
        #                 left join account_journal aj ON aj.id = aml.journal_id
        #                 left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
        #                 left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
        #                 left JOIN account_payment ap ON ap.id = aml.payment_id
        #                 WHERE am.date BETWEEN '""" + str(
        #                 data.get('date_from')) + """' and '""" + str(
        #                 data.get('date_to')) + """' AND move_type = 'entry' 
        #                 and aj.type = 'bank' or aj.type = 'cash'
        #                 ) as am  
        #                 left join account_move_line aml ON am.id = aml.move_id
        #                 left JOIN account_account aa ON aa.id = aml.account_id
        #                 left join account_journal aj ON aj.id = aml.journal_id
        #                 left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
        #                 left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
        #                 left JOIN account_payment ap ON ap.id = aml.payment_id
        #                 WHERE am.date BETWEEN '""" + str(data.get('date_from')) + """' 
        #                 and '""" + str(data.get('date_to')) + """' """ + tags + """) as am                        
        #                 WHERE am.account_account_tag_id is not null
        #                 """ + opt + """                        
        #                 """ + state + """ 
        #                 GROUP BY year_part,month_part """

        query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
                    sum(am.debit) AS total_debit, 
                    sum(am.credit) AS total_credit,
                    sum(am.credit) - sum(am.debit) AS total_balance    
                    from (
                    select am.name as nameam,aj.type,aa.code,aa.name as nameaa, aa.user_type_id, tags.name as nametags, am.date, am.id as idam, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
                    FROM account_move as am
                    inner join account_move_line aml ON am.id = aml.move_id AND am.move_type = 'entry'
                    left JOIN account_account aa ON aa.id = aml.account_id 
                    inner join account_journal aj ON aj.id = aml.journal_id and (aj.type = 'bank' or aj.type = 'cash')
                    left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
                    left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
                    left JOIN account_payment ap ON ap.id = aml.payment_id
                    WHERE am.company_id = '""" + str(self.env.company.id) + """' and am.date BETWEEN '""" + str(data.get('date_from')) + """' 
                    and '""" + str(data.get('date_to')) + """' order by am.id ASC) as am
                    where user_type_id != '3'
                    """ + tags + """
                    """ + opt + """
                    """ + state + """ 
                    GROUP BY year_part,month_part """            
        cr = self._cr
        cr.execute(query)
        result = cr.dictfetchall()
        return result

    def _query_2_receipt(self, opt, state, account_type_id, data, tag_id=False, opt2=False):        
        if not opt2:
            option = 'inner'
        else:
            option = opt2            
        print(option)
        if not tag_id:
            tags = ''
        else:
            tags = """ and account_account_tag_id = '""" + str(tag_id) + """' """
        print(tags)

        # query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
        #             sum(am.debit) AS total_debit, 
        #             sum(am.credit) AS total_credit,
        #             sum(am.credit) AS total_balance
        #             from ( select am.idam, am.nameam, am.code,am.nameaa, am.date, am.debit, am.credit, am.balance, am.account_account_tag_id, am.state, am.is_reconciled, am.partner_type, am.payment_type FROM 
        #             (select am.name as nameam,aj.type,aa.code,aa.name as nameaa,tags.name as nametags, am.date, am.id as idam, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
        #                 FROM account_move as am
        #                 left join account_move_line aml ON am.id = aml.move_id
        #                 left JOIN account_account aa ON aa.id = aml.account_id
        #                 left join account_journal aj ON aj.id = aml.journal_id
        #                 left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
        #                 left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
        #                 left JOIN account_payment ap ON ap.id = aml.payment_id
        #                 WHERE am.date BETWEEN '""" + str(
        #                 data.get('date_from')) + """' and '""" + str(
        #                 data.get('date_to')) + """' AND move_type = 'entry'                         
        #                 and aj.type = 'bank' or aj.type = 'cash'                        
        #                   ) as am 
        #                 left join account_move_line aml ON idam = aml.move_id
        #                 left JOIN account_account aa ON aa.id = aml.account_id
        #                 left join account_journal aj ON aj.id = aml.journal_id
        #                 left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
        #                 left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
        #                 left JOIN account_payment ap ON ap.id = aml.payment_id
        #                 WHERE am.date BETWEEN '""" + str(
        #                 data.get('date_from')) + """' and '""" + str(
        #                 data.get('date_to')) + """'
        #                 """ + tags + """) as am
        #                 where am.account_account_tag_id is not null
        #                 and am.is_reconciled = false and am.partner_type = 'customer'
        #                 """ + state + """
        #                 GROUP BY year_part,month_part """

        query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
                    sum(am.debit) AS total_debit, 
                    sum(am.credit) AS total_credit,
                    sum(am.credit) AS total_balance
                    from (
                    select am.name as nameam,aj.type,aa.code,aa.name as nameaa, aa.user_type_id, tags.name as nametags, am.date, am.id as idam, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
                    FROM account_move as am
                    inner join account_move_line aml ON am.id = aml.move_id AND am.move_type = 'entry'
                    left JOIN account_account aa ON aa.id = aml.account_id 
                    inner join account_journal aj ON aj.id = aml.journal_id and (aj.type = 'bank' or aj.type = 'cash')
                    left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
                    left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
                    left JOIN account_payment ap ON ap.id = aml.payment_id
                    WHERE am.company_id = '""" + str(self.env.company.id) + """' and am.date BETWEEN '""" + str(data.get('date_from')) + """' 
                    and '""" + str(data.get('date_to')) + """' order by am.id ASC) as am
                    where user_type_id != '3'
                    and (is_reconciled = false or is_reconciled isnull)
                    """ + tags + """
                    """ + opt + """
                    """ + state + """ 
                    GROUP BY year_part,month_part """            
        cr = self._cr
        cr.execute(query)
        result = cr.dictfetchall()
        return result

    def _query_2_paid(self, opt, state, account_type_id, data, tag_id=False, opt2=False):        
        if not opt2:
            option = 'inner'
        else:
            option = opt2            
        print(option)
        if not tag_id:
            tags = ''
        else:
            tags = """ and account_account_tag_id = '""" + str(tag_id) + """' """

        print(tags)
        # query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
        #             sum(am.debit) AS total_debit, 
        #             sum(am.credit) AS total_credit,
        #             -sum(am.debit) AS total_balance    
        #             from ( select am.idam, am.nameam, am.code,am.nameaa, am.date, am.debit, am.credit, am.balance, am.account_account_tag_id, am.state, am.is_reconciled, am.partner_type, am.payment_type FROM 
        #             (select am.name as nameam,aj.type,aa.code,aa.name as nameaa,tags.name as nametags, am.date, am.id as idam, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
        #                 FROM account_move as am
        #                 left join account_move_line aml ON am.id = aml.move_id
        #                 left JOIN account_account aa ON aa.id = aml.account_id
        #                 left join account_journal aj ON aj.id = aml.journal_id
        #                 left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
        #                 left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
        #                 left JOIN account_payment ap ON ap.id = aml.payment_id
        #                 WHERE am.date BETWEEN '""" + str(
        #                 data.get('date_from')) + """' and '""" + str(
        #                 data.get('date_to')) + """' AND move_type = 'entry'                         
        #                 and aj.type = 'bank' or aj.type = 'cash'                        
        #                   ) as am 
        #                 left join account_move_line aml ON idam = aml.move_id
        #                 left JOIN account_account aa ON aa.id = aml.account_id
        #                 left join account_journal aj ON aj.id = aml.journal_id
        #                 left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
        #                 left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
        #                 left JOIN account_payment ap ON ap.id = aml.payment_id
        #                 WHERE am.date BETWEEN '""" + str(
        #                 data.get('date_from')) + """' and '""" + str(
        #                 data.get('date_to')) + """'
        #                 """ + tags + """) as am
        #                 where am.account_account_tag_id is not null
        #                 and am.is_reconciled = false and am.partner_type = 'supplier'
        #                 """ + state + """
        #                 GROUP BY year_part,month_part """

        query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
                    sum(am.debit) AS total_debit, 
                    sum(am.credit) AS total_credit,
                    -sum(am.debit) AS total_balance    
                    from (
                    select am.name as nameam,aj.type,aa.code,aa.name as nameaa, aa.user_type_id, tags.name as nametags, am.date, am.id as idam, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
                    FROM account_move as am
                    inner join account_move_line aml ON am.id = aml.move_id AND am.move_type = 'entry'
                    left JOIN account_account aa ON aa.id = aml.account_id 
                    inner join account_journal aj ON aj.id = aml.journal_id and (aj.type = 'bank' or aj.type = 'cash')
                    left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
                    left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
                    left JOIN account_payment ap ON ap.id = aml.payment_id
                    WHERE am.company_id = '""" + str(self.env.company.id) + """' and am.date BETWEEN '""" + str(data.get('date_from')) + """' 
                    and '""" + str(data.get('date_to')) + """' order by am.id ASC) as am
                    where user_type_id != '3'
                    and (is_reconciled = false or is_reconciled isnull)
                    """ + tags + """
                    """ + opt + """
                    """ + state + """ 
                    GROUP BY year_part,month_part """
        cr = self._cr
        cr.execute(query)
        result = cr.dictfetchall()
        return result

    def _query_3(self, opt, state, account_type_id, data, tag_id=False, opt2=False):        
        if not opt2:
            option = 'inner'
        else:
            option = opt2            
        print(option)
        if not tag_id:
            tags = ''
        else:
            tags = """ and account_account_tag_id = '""" + str(tag_id) + """' """
        print(tags)
        # query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
        #             sum(am.debit) AS total_credit, 
        #             sum(am.credit) AS total_debit,
        #             sum(am.credit) - sum(am.debit) AS total_balance        
        #             from ( select am.date, am.debit, am.credit, am.balance, am.account_account_tag_id, am.state, am.is_reconciled, am.partner_type, am.payment_type FROM 
        #             (select am.name,aj.type,aa.code,aa.name,tags.name, am.date, am.id, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
        #                 FROM account_move as am
        #                 left join account_move_line aml ON am.id = aml.move_id
        #                 left JOIN account_account aa ON aa.id = aml.account_id
        #                 left join account_journal aj ON aj.id = aml.journal_id
        #                 left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
        #                 left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
        #                 left JOIN account_payment ap ON ap.id = aml.payment_id
        #                 WHERE am.date BETWEEN '""" + str(
        #                 data.get('date_from')) + """' and '""" + str(
        #                 data.get('date_to')) + """' AND move_type = 'entry'                         
        #                 and aj.type = 'bank' or aj.type = 'cash'                        
        #                   ) as am  
        #                 left join account_move_line aml ON am.id = aml.move_id
        #                 left JOIN account_account aa ON aa.id = aml.account_id
        #                 left join account_journal aj ON aj.id = aml.journal_id
        #                 left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
        #                 left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
        #                 left JOIN account_payment ap ON ap.id = aml.payment_id
        #                 WHERE am.date BETWEEN '""" + str(
        #                 data.get('date_from')) + """' and '""" + str(
        #                 data.get('date_to')) + """'
        #                 """ + tags + """) as am
        #                 where am.account_account_tag_id is not null
        #                 """ + state + """
        #                 GROUP BY year_part,month_part """

        query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
                    -sum(am.debit) AS total_credit, 
                    sum(am.credit) AS total_debit,
                    sum(am.credit) - sum(am.debit) AS total_balance    
                    from (
                    select am.name as nameam,aj.type,aa.code,aa.name as nameaa, aa.user_type_id, tags.name as nametags, am.date, am.id as idam, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
                    FROM account_move as am
                    inner join account_move_line aml ON am.id = aml.move_id AND am.move_type = 'entry'
                    left JOIN account_account aa ON aa.id = aml.account_id 
                    inner join account_journal aj ON aj.id = aml.journal_id and (aj.type = 'bank' or aj.type = 'cash')
                    left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
                    left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
                    left JOIN account_payment ap ON ap.id = aml.payment_id
                    WHERE am.company_id = '""" + str(self.env.company.id) + """' and am.date BETWEEN '""" + str(data.get('date_from')) + """' 
                    and '""" + str(data.get('date_to')) + """' order by am.id ASC) as am
                    where user_type_id != '3'
                    """ + tags + """
                    """ + opt + """
                    """ + state + """ 
                    GROUP BY year_part,month_part """

        # query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
        #             sum(am.debit) AS total_credit, 
        #             sum(am.credit) AS total_debit,
        #             sum(am.credit) - sum(am.debit) AS total_balance    
        #             from (
        #             select am.name as nameam,aj.type,aa.code,aa.name as nameaa, aa.user_type_id, tags.name as nametags, am.date, am.id as idam, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
        #             FROM account_move as am
        #             inner join account_move_line aml ON am.id = aml.move_id AND am.move_type = 'entry'
        #             left JOIN account_account aa ON aa.id = aml.account_id 
        #             inner join account_journal aj ON aj.id = aml.journal_id and (aj.type = 'bank' or aj.type = 'cash')
        #             left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
        #             left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
        #             left JOIN account_payment ap ON ap.id = aml.payment_id
        #             WHERE am.date BETWEEN '""" + str(data.get('date_from')) + """' 
        #             and '""" + str(data.get('date_to')) + """' order by am.id ASC) as am
        #             where user_type_id != '3'
        #             and account_account_tag_id isnull
        #             """ + state + """
        #             GROUP BY year_part,month_part  """            
        cr = self._cr
        cr.execute(query)
        result = cr.dictfetchall()
        return result

    def _query_unclass(self, opt, state, account_type_id, data, tag_id=False, opt2=False):        
        if not opt2:
            option = 'inner'
        else:
            option = opt2            
        print(option)
        if not tag_id:
            tags = ''
        else:
            tags = """ and account_account_tag_id = '""" + str(tag_id) + """' """
        print(tags)
        # query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
        #             sum(am.debit) AS total_credit, 
        #             sum(am.credit) AS total_debit,
        #             sum(am.credit) - sum(am.debit) AS total_balance        
        #             from ( select am.date, am.debit, am.credit, am.balance, am.account_account_tag_id, am.state, am.is_reconciled, am.partner_type, am.payment_type FROM 
        #             (select am.name,aj.type,aa.code,aa.name,tags.name, am.date, am.id, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
        #                 FROM account_move as am
        #                 left join account_move_line aml ON am.id = aml.move_id
        #                 left JOIN account_account aa ON aa.id = aml.account_id
        #                 left join account_journal aj ON aj.id = aml.journal_id
        #                 left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
        #                 left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
        #                 left JOIN account_payment ap ON ap.id = aml.payment_id
        #                 WHERE am.date BETWEEN '""" + str(
        #                 data.get('date_from')) + """' and '""" + str(
        #                 data.get('date_to')) + """' AND move_type = 'entry'                         
        #                 and aj.type = 'bank' or aj.type = 'cash'                        
        #                   ) as am  
        #                 left join account_move_line aml ON am.id = aml.move_id
        #                 left JOIN account_account aa ON aa.id = aml.account_id
        #                 left join account_journal aj ON aj.id = aml.journal_id
        #                 left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
        #                 left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
        #                 left JOIN account_payment ap ON ap.id = aml.payment_id
        #                 WHERE am.date BETWEEN '""" + str(
        #                 data.get('date_from')) + """' and '""" + str(
        #                 data.get('date_to')) + """'
        #                 """ + tags + """) as am
        #                 where am.account_account_tag_id is not null
        #                 """ + state + """
        #                 GROUP BY year_part,month_part """

        query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
                    -sum(am.debit) AS total_credit, 
                    sum(am.credit) AS total_debit,
                    sum(am.credit) - sum(am.debit) AS total_balance 
                    from (
                    select am.name as nameam,aj.type,aa.code,aa.name as nameaa, aa.user_type_id, tags.name as nametags, am.date, am.id as idam, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
                    FROM account_move as am
                    inner join account_move_line aml ON am.id = aml.move_id AND am.move_type = 'entry'
                    left JOIN account_account aa ON aa.id = aml.account_id 
                    inner join account_journal aj ON aj.id = aml.journal_id and (aj.type = 'bank' or aj.type = 'cash')
                    left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
                    left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
                    left JOIN account_payment ap ON ap.id = aml.payment_id
                    WHERE am.company_id = '""" + str(self.env.company.id) + """' and am.date BETWEEN '""" + str(data.get('date_from')) + """' 
                    and '""" + str(data.get('date_to')) + """' order by am.id ASC) as am
                    where user_type_id != '3'
                    and account_account_tag_id isnull
                    """ + state + """
                    GROUP BY year_part,month_part  """            
        cr = self._cr
        cr.execute(query)
        result = cr.dictfetchall()
        return result

    def _query_all(self, opt, state, account_type_id, data, tag_id=False, opt2=False):        
        if not opt2:
            option = 'inner'
        else:
            option = opt2            
        print(option)
        if not tag_id:
            tags = ''
        else:
            tags = """ AND tg.account_account_tag_id = '""" + str(tag_id) + """' """

        print(tags)

        query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
                    sum(am.debit) AS total_credit, 
                    sum(am.credit) AS total_debit,
                    sum(am.credit) - sum(am.debit) AS total_balance         
                    from ( select am.date, am.debit, am.credit, am.balance, am.account_account_tag_id, am.state, am.is_reconciled, am.partner_type, am.payment_type FROM 
                    (select am.name,aj.type,aa.code,aa.name,tags.name, am.date, am.id, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
                        FROM account_move as am
                        left join account_move_line aml ON am.id = aml.move_id
                        left JOIN account_account aa ON aa.id = aml.account_id
                        left join account_journal aj ON aj.id = aml.journal_id
                        left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
                        left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
                        left JOIN account_payment ap ON ap.id = aml.payment_id
                        WHERE am.company_id = '""" + str(self.env.company.id) + """' and am.date BETWEEN '""" + str(
                        data.get('date_from')) + """' and '""" + str(
                        data.get('date_to')) + """' AND move_type = 'entry'                         
                        and aj.type = 'bank' or aj.type = 'cash' 
                          ) as am  
                        left join account_move_line aml ON am.id = aml.move_id
                        left JOIN account_account aa ON aa.id = aml.account_id
                        left join account_journal aj ON aj.id = aml.journal_id
                        left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
                        left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
                        left JOIN account_payment ap ON ap.id = aml.payment_id
                        WHERE am.date BETWEEN '""" + str(
                        data.get('date_from')) + """' and '""" + str(
                        data.get('date_to')) + """'
                        AND am.account_account_tag_id is not null ) as am
                        where am.account_account_tag_id is not null
                        """ + state + """
                        GROUP BY year_part,month_part """             
        cr = self._cr
        cr.execute(query)
        result = cr.dictfetchall()
        return result

    def _query_7(self, opt, state, account_type_id, data, tag_id=False, opt2=False):        
        # query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
        #             sum(am.debit) AS total_credit, 
        #             sum(am.credit) AS total_debit,
        #             sum(am.credit) - sum(am.debit) AS total_balance       
        #             from ( select am.date, am.debit, am.credit, am.balance, am.account_account_tag_id, am.state, am.is_reconciled, am.partner_type, am.payment_type FROM 
        #             (select am.name,aj.type,aa.code,aa.name,tags.name, am.date, am.id, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
        #                 FROM account_move as am
        #                 left join account_move_line aml ON am.id = aml.move_id
        #                 left JOIN account_account aa ON aa.id = aml.account_id
        #                 left join account_journal aj ON aj.id = aml.journal_id
        #                 left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
        #                 left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
        #                 left JOIN account_payment ap ON ap.id = aml.payment_id
        #                 WHERE am.date < '""" + str(
        #                 data.get('date_from')) + """' AND move_type = 'entry'                         
        #                 and aj.type = 'bank' or aj.type = 'cash' 
        #                   ) as am  
        #                 left join account_move_line aml ON am.id = aml.move_id
        #                 left JOIN account_account aa ON aa.id = aml.account_id
        #                 left join account_journal aj ON aj.id = aml.journal_id
        #                 left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
        #                 left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
        #                 left JOIN account_payment ap ON ap.id = aml.payment_id
        #                 WHERE am.date < '""" + str(
        #                 data.get('date_from')) + """'
        #                 AND am.account_account_tag_id is not null ) as am
        #                 where am.account_account_tag_id is not null
        #                 """ + state + """
        #                 GROUP BY year_part,month_part """

        query = """ SELECT to_char(am.date, 'Month') as month_part, extract(YEAR from am.date) as year_part,
                    sum(am.debit) AS total_credit, 
                    sum(am.credit) AS total_debit,
                    sum(am.credit) - sum(am.debit) AS total_balance    
                    from (
                    select am.name as nameam,aj.type,aa.code,aa.name as nameaa, aa.user_type_id, tags.name as nametags, am.date, am.id as idam, am.state, am.move_type, tg.account_account_tag_id, ap.is_reconciled, ap.partner_type, ap.payment_type, aml.debit, aml.credit, aml.balance
                    FROM account_move as am
                    inner join account_move_line aml ON am.id = aml.move_id AND am.move_type = 'entry'
                    left JOIN account_account aa ON aa.id = aml.account_id 
                    inner join account_journal aj ON aj.id = aml.journal_id and (aj.type = 'bank' or aj.type = 'cash')
                    left JOIN account_account_account_tag tg on tg.account_account_id = aml.account_id
                    left JOIN account_account_tag tags on tags.id = tg.account_account_tag_id
                    left JOIN account_payment ap ON ap.id = aml.payment_id
                    WHERE am.company_id = '""" + str(self.env.company.id) + """' and am.date < '""" + str(data.get('date_from')) + """' 
                    order by am.id ASC) as am
                    where user_type_id != '3'
                    """ + state + """
                    GROUP BY year_part,month_part  """
        cr = self._cr
        cr.execute(query)
        result = cr.dictfetchall()
        return result

    def _get_report_values(self, data, option):
        company_ids = self.env.company.ids
        cr = self.env.cr
        data = self.get_filter(option)
        company_id = self.env.company
        currency = company_id.currency_id
        symbol = company_id.currency_id.symbol
        rounding = company_id.currency_id.rounding
        position = company_id.currency_id.position

        fetched_data = []
        account_res = []
        journal_res = []
        fetched = []
        received_customer = []
        cash_received = []
        payment_supplier = []
        cash_paid = []
        cf_investing = []
        cf_finance = []
        cf_unclass = []
        cf_beginning_period = []

        account_type_id = self.env.ref('account.data_account_type_liquidity').id
        account_tag_operating_id = self.env.ref('account.account_tag_operating').id
        account_tag_financing_id = self.env.ref('account.account_tag_financing').id
        account_tag_investing_id = self.env.ref('account.account_tag_investing').id
        model = self.env.context.get('active_model')

        state = """ and state = 'posted' """ if data.get('target_move') == 'Posted' else ''

        # opt = """ and am.is_reconciled = true AND am.partner_type = 'customer' and am.payment_type = 'inbound' """ if data.get('target_move') == 'Posted' else """ AND am.partner_type = 'customer' and am.payment_type = 'inbound' """
        opt = """ and is_reconciled = true AND partner_type = 'customer' and payment_type = 'inbound' """
        query1 = self._query_1(opt, state, account_type_id, data, account_tag_operating_id)
        received_customer = query1

        opt = ""
        query3 = self._query_2_receipt(opt, state, account_type_id, data, account_tag_operating_id)
        cash_received = query3
        
        # opt = """ and am.is_reconciled = true AND am.partner_type = 'supplier' and am.payment_type = 'outbound' """ if data.get('target_move') == 'Posted' else """ AND am.partner_type = 'supplier' and am.payment_type = 'outbound' """
        opt = """ and is_reconciled = true AND partner_type = 'supplier' and payment_type = 'outbound' """
        query3 = self._query_1(opt, state, account_type_id, data, account_tag_operating_id)
        payment_supplier = query3

        opt = ""
        query3 = self._query_2_paid(opt, state, account_type_id, data, account_tag_operating_id)
        cash_paid = query3

        opt = ""
        query1 = self._query_3(opt, state,account_type_id, data, account_tag_investing_id)
        cf_investing = query1

        query2 = self._query_3(opt, state, account_type_id, data, account_tag_financing_id)
        cf_finance = query2\

        query3 = self._query_unclass(opt, state, account_type_id, data, account_tag_financing_id)
        cf_unclass = query3

        # query3 = self._query_3(opt, state, account_type_id, data, account_tag_operating_id)
        # cf_finance = query2

        # query_all = self._query_all(opt, state, account_type_id, data, account_tag_operating_id)
        
        # sum_query = self._sum_list(query1 + query2 + query3, 'sum_query')

        # cf_unclass = self._sum_list_minus(query_all + sum_query, "cf_unclass")

        query3 = self._query_7(opt, state, account_type_id, data)
        cf_beginning_period = query3

        return {
            'date_from': data.get('date_from'),
            'date_to': data.get('date_to'),
            'levels': data.get('level'),
            'doc_ids': self.ids,
            'doc_model': model,
            'fetched_data': fetched_data,
            'account_res': account_res,
            'journal_res': journal_res,
            'fetched': fetched,
            'company_currency_id': currency,
            'company_currency_symbol': symbol,
            'company_currency_position': position,
            'received_customer': received_customer,
            'cash_received': cash_received,
            'payment_supplier': payment_supplier,
            'cash_paid': cash_paid,
            'cf_investing': cf_investing,
            'cf_finance': cf_finance,
            'cf_unclass': cf_unclass,
            'cf_beginning_period': cf_beginning_period,
        }

    def _get_lines(self, account, data):
        account_type_id = self.env.ref(
            'account.data_account_type_liquidity').id
        state = """AND am.state = 'posted' """ if data.get('target_move') == 'posted' else ''
        query = """SELECT aml.account_id,aj.id as j_id,aj.name,am.id, am.name as move_name, sum(aml.debit) AS total_debit, 
                    sum(aml.credit) AS total_credit, COALESCE(SUM(aml.debit - aml.credit),0) AS balance FROM (SELECT am.* FROM account_move as am
                    LEFT JOIN account_move_line aml ON aml.move_id = am.id
                    LEFT JOIN account_account aa ON aa.id = aml.account_id
                    LEFT JOIN account_account_type aat ON aat.id = aa.user_type_id
                    WHERE am.date BETWEEN '""" + str(
            data.get('date_from')) + """' and '""" + str(
            data.get('date_to')) + """' AND aat.id='""" + str(
            account_type_id) + """' """ + state + """) am
                                        LEFT JOIN account_move_line aml ON aml.move_id = am.id
                                        LEFT JOIN account_account aa ON aa.id = aml.account_id
                                        LEFT JOIN account_journal aj ON aj.id = am.journal_id
                                        WHERE aa.id = """ + str(account.id) + """
                                        GROUP BY am.name, aml.account_id, aj.id, aj.name, am.id"""

        cr = self._cr
        cr.execute(query)
        fetched_data = cr.dictfetchall()

        sql2 = """SELECT aa.name as account_name,aa.id as account_id, aj.id, aj.name, sum(aml.debit) AS total_debit,
                        sum(aml.credit) AS total_credit, sum(aml.balance) AS total_balance FROM (SELECT am.* FROM account_move as am
                            LEFT JOIN account_move_line aml ON aml.move_id = am.id
                            LEFT JOIN account_account aa ON aa.id = aml.account_id
                            LEFT JOIN account_account_type aat ON aat.id = aa.user_type_id
                            WHERE am.date BETWEEN '""" + str(
            data.get('date_from')) + """' and '""" + str(
            data.get('date_to')) + """' AND aat.id='""" + str(
            account_type_id) + """' """ + state + """) am
                                                LEFT JOIN account_move_line aml ON aml.move_id = am.id
                                                LEFT JOIN account_account aa ON aa.id = aml.account_id
                                                LEFT JOIN account_journal aj ON aj.id = am.journal_id
                                                WHERE aa.id = """ + str(
            account.id) + """
                                                GROUP BY aa.name, aj.name, aj.id,aa.id"""

        cr = self._cr
        cr.execute(sql2)
        fetch_data = cr.dictfetchall()
        if fetched_data:
            return {
                'account': account.name,
                'id': account.id,
                'code': account.code,
                'move_lines': fetched_data,
                'journal_lines': fetch_data,
            }


    def get_journal_lines(self, account, data, offset=0, fetch_range=FETCH_RANGE):
        account_type_id = self.env.ref(
            'account.data_account_type_liquidity').id
        offset_count = offset * fetch_range
        state = """AND am.state = 'posted' """ if data.get('target_move') == 'posted' else ''
        sql2 = """SELECT aa.name as account_name, aj.name, sum(aml.debit) AS total_debit,
         sum(aml.credit) AS total_credit, COALESCE(SUM(aml.debit - aml.credit),0) AS balance FROM (SELECT am.* FROM account_move as am
             LEFT JOIN account_move_line aml ON aml.move_id = am.id
             LEFT JOIN account_account aa ON aa.id = aml.account_id
             LEFT JOIN account_account_type aat ON aat.id = aa.user_type_id
             WHERE am.date BETWEEN '""" + str(
            data.get('date_from')) + """' and '""" + str(
            data.get('date_to')) + """' AND aat.id='""" + str(
            account_type_id) + """' """ + state + """) am
                                 LEFT JOIN account_move_line aml ON aml.move_id = am.id
                                 LEFT JOIN account_account aa ON aa.id = aml.account_id
                                 LEFT JOIN account_journal aj ON aj.id = am.journal_id
                                 WHERE aa.id = """ + str(account.id) + """
                                 GROUP BY aa.name, aj.name"""

        cr = self._cr
        cr.execute(sql2)
        fetched_data = cr.dictfetchall()
        if fetched_data:
            return {
                'account': account.name,
                'id': account.id,
                'journal_lines': fetched_data,
                'offset': offset_count,
            }




    @api.model
    def create(self, vals):
        vals['target_move'] = 'posted'
        res = super(AccountCasgFlow, self).create(vals)
        return res

    def write(self, vals):
        if vals.get('target_move'):
            vals.update({'target_move': vals.get('target_move').lower()})
        if vals.get('journal_ids'):
            vals.update({'journal_ids': [(6, 0, vals.get('journal_ids'))]})
        if vals.get('journal_ids') == []:
            vals.update({'journal_ids': [(5,)]})
        if vals.get('account_ids'):
            vals.update({'account_ids': [(4, j) for j in vals.get('account_ids')]})
        if vals.get('account_ids') == []:
            vals.update({'account_ids': [(5,)]})
        if not vals.get('company_ids'):
            vals.update({'company_ids': [(5,)]})

        vals.update({'company_ids': [(4, j) for j in self.env.company.ids]})
        res = super(AccountCasgFlow, self).write(vals)
        return res

    @api.model
    def _get_currency(self):
        journal = self.env['account.journal'].browse(
            self.env.context.get('default_journal_id', False))
        if journal.currency_id:
            return journal.currency_id.id
        lang = self.env.user.lang
        if not lang:
            lang = 'en_US'
        lang = lang.replace("_", '-')
        currency_array = [self.env.company.currency_id.symbol,
                          self.env.company.currency_id.position, lang]
        return currency_array

    def get_dynamic_xlsx_report(self, data, response, report_data, dfr_data):
        report_main_data = json.loads(dfr_data)
        data = json.loads(data)
        report_data = report_main_data.get('report_lines')
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        fetched_data = report_data.get('fetched_data')
        account_res = report_data.get('account_res')
        journal_res = report_data.get('journal_res')
        fetched = report_data.get('fetched')
        account_type_id = self.env.ref('account.data_account_type_liquidity').id
        currency_symbol = self.env.company.currency_id.symbol

        received_customer = self._sum_list(report_data.get('received_customer'),'Advance payments received from customers')
        cash_received = self._sum_list(report_data.get('cash_received'),'Cash received from')
        payment_supplier = self._sum_list(report_data.get('payment_supplier'),'Advance payments made to suppliers')
        cash_paid = self._sum_list(report_data.get('cash_paid'),'Cash paid for')
        sum_cf_statement = self._sum_list(received_customer + cash_received + payment_supplier + cash_paid,'Total cash flow from operating activities')

        cf_investing = self._sum_list(report_data.get('cf_investing'), "cf_investing")
        cf_finance = self._sum_list(report_data.get('cf_finance'), "cf_finance")
        cf_unclass = self._check_list(self._sum_list(report_data.get('cf_unclass'), "cf_unclass"))
        sum_all_cf_statement = self._sum_list(sum_cf_statement + cf_investing + cf_finance + cf_unclass,'Net increase in cash and cash equivalents')
        cf_beginning_period = self._sum_list(report_data.get('cf_beginning_period'), "Cash and cash equivalents, beginning of period")
        cf_closing_period = self._sum_list(sum_all_cf_statement + cf_beginning_period,'Cash and cash equivalents, closing of period')

        logged_users = self.env['res.company']._company_default_get('account.account')
        sheet = workbook.add_worksheet()
        bold = workbook.add_format({'align': 'center',
                                    'bold': True,
                                    'font_size': '10px',
                                    'border': 1})
        date = workbook.add_format({'font_size': '10px'})
        cell_format = workbook.add_format({'bold': True,
                                           'font_size': '10px'})
        head = workbook.add_format({'align': 'center',
                                    'bold': True,
                                    'bg_color': '#D3D3D3',
                                    'font_size': '15px'})
        txt = workbook.add_format({'align': 'left',
                                   'font_size': '10px'})
        txt_left = workbook.add_format({'align': 'left',
                                        'font_size': '10px',
                                        'border': 1})
        txt_center = workbook.add_format({'align': 'center',
                                          'font_size': '10px',
                                          'border': 1})
        amount = workbook.add_format({'align': 'right',
                                      'font_size': '10px',
                                      'border': 1})
        amount_bold = workbook.add_format({'align': 'right',
                                           'bold': True,
                                           'font_size': '10px',
                                           'border': 1})
        txt_bold = workbook.add_format({'align': 'left',
                                        'bold': True,
                                        'font_size': '10px',
                                        'border': 1})

        sheet.set_column('C:C', 30, cell_format)
        sheet.set_column('D:E', 20, cell_format)
        sheet.set_column('F:F', 20, cell_format)
        sheet.merge_range('C3:F5', '')
        sheet.merge_range('C3:F4', 'CASH FLOW REPORT', head)
        sheet.merge_range('C4:F4', '')

        sheet.write('C6', "Date From", cell_format)
        sheet.write('D6', str(data['date_from']), date)
        sheet.write('E6', "Date To", cell_format)
        sheet.write('F6', str(data['date_to']), date)
        if data.get('levels'):
            sheet.write('C7', "Level", cell_format)
            sheet.write('D7', data.get("levels"), date)
        sheet.write('E7', "Target Moves", cell_format)
        sheet.write('F7', data.get("target_move"), date)
        sheet.write('C9', 'NAME', bold)
        sheet.write('D9', '', bold)
        sheet.write('E9', '', bold)
        sheet.write('F9', 'BALANCE', bold)

        row_num = 8
        col_num = 2
        received_customer_list = received_customer
        cash_received_list = cash_received
        payment_supplier_list = payment_supplier
        cash_paid_list = cash_paid
        sum_cf_statement_list = sum_cf_statement
        cf_investing_list = cf_investing
        cf_finance_list = cf_finance
        cf_unclass_list = cf_unclass
        sum_all_cf_statement_list = sum_all_cf_statement
        cf_beginning_period_list = cf_beginning_period
        cf_closing_period_list = cf_closing_period

        sheet.write(row_num + 1, col_num, 'Cash flow from operating activities', txt_bold)        
        row_num += 1
        for i_rec in received_customer_list:
            sheet.write(row_num + 1, col_num, str(i_rec['month_part']), txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_balance']), amount)
            row_num = row_num + 1
        for i_rec in cash_received_list:
            sheet.write(row_num + 1, col_num, str(i_rec['month_part']), txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_balance']), amount)
            row_num = row_num + 1
        for i_rec in payment_supplier_list:
            sheet.write(row_num + 1, col_num, str(i_rec['month_part']), txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_balance']), amount)
            row_num = row_num + 1
        for i_rec in cash_paid_list:
            sheet.write(row_num + 1, col_num, str(i_rec['month_part']), txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_balance']), amount)
            row_num = row_num + 1
        for i_rec in sum_cf_statement_list:
            sheet.write(row_num + 1, col_num, str(i_rec['month_part']), txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_balance']), amount)
            row_num = row_num + 1
        row_num += 1
        sheet.write(row_num + 1, col_num, 'Cash flow from investing and extraordinary activities', txt_bold)        
        row_num += 1
        for i_rec in cf_investing_list:
            sheet.write(row_num + 1, col_num, "Cash In", txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_debit']), amount)
            row_num = row_num + 1
        for i_rec in cf_investing_list:
            sheet.write(row_num + 1, col_num, "Cash Out", txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_credit']), amount)
            row_num = row_num + 1
        for i_rec in cf_investing_list:
            sheet.write(row_num + 1, col_num, "Total cash flow from financing activities", txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_balance']), amount)
            row_num = row_num + 1
        row_num += 1
        sheet.write(row_num + 1, col_num, 'Cash flow from financing activities', txt_bold)        
        row_num += 1
        for i_rec in cf_finance_list:
            sheet.write(row_num + 1, col_num, "Cash In", txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_debit']), amount)
            row_num = row_num + 1
        for i_rec in cf_finance_list:
            sheet.write(row_num + 1, col_num, "Cash Out", txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_credit']), amount)
            row_num = row_num + 1
        for i_rec in cf_finance_list:
            sheet.write(row_num + 1, col_num, "Total cash flow from financing activities", txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_balance']), amount)
            row_num = row_num + 1
        if len(cf_unclass_list) > 0:
            row_num += 1
            sheet.write(row_num + 1, col_num, 'Cash flow from unclassified activities', txt_bold)        
            row_num += 1
            for i_rec in cf_unclass_list:
                sheet.write(row_num + 1, col_num, "Cash In", txt_left)
                sheet.write(row_num + 1, col_num + 1, "")
                sheet.write(row_num + 1, col_num + 2, "")
                sheet.write(row_num + 1, col_num + 3, str(i_rec['total_debit']), amount)
                row_num = row_num + 1
            for i_rec in cf_unclass_list:
                sheet.write(row_num + 1, col_num, "Cash Out", txt_left)
                sheet.write(row_num + 1, col_num + 1, "")
                sheet.write(row_num + 1, col_num + 2, "")
                sheet.write(row_num + 1, col_num + 3, str(i_rec['total_credit']), amount)
                row_num = row_num + 1
            for i_rec in cf_unclass_list:
                sheet.write(row_num + 1, col_num, "Total cash flow from unclassified activities", txt_left)
                sheet.write(row_num + 1, col_num + 1, "")
                sheet.write(row_num + 1, col_num + 2, "")
                sheet.write(row_num + 1, col_num + 3, str(i_rec['total_balance']), amount)
                row_num = row_num + 1
        row_num += 1
        sheet.write(row_num + 1, col_num, 'Net increase in cash and cash equivalents', txt_bold)        
        row_num += 1
        for i_rec in sum_all_cf_statement_list:
            sheet.write(row_num + 1, col_num, str(i_rec['month_part']), txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_balance']), amount)
            row_num = row_num + 1
        row_num += 1
        sheet.write(row_num + 1, col_num, 'Cash and cash equivalents', txt_bold)        
        row_num += 1
        for i_rec in cf_beginning_period_list:
            sheet.write(row_num + 1, col_num, str(i_rec['month_part']), txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_balance']), amount)
            row_num = row_num + 1
        for i_rec in cf_closing_period_list:
            sheet.write(row_num + 1, col_num, str(i_rec['month_part']), txt_left)
            sheet.write(row_num + 1, col_num + 1, "")
            sheet.write(row_num + 1, col_num + 2, "")
            sheet.write(row_num + 1, col_num + 3, str(i_rec['total_balance']), amount)
            row_num = row_num + 1

        workbook.close()
        output.seek(0)
        response.stream.write(output.read())
        output.close()