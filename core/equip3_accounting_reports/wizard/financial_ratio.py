import time
from odoo import fields, models, api, _

import io
import json
from odoo.exceptions import AccessError, UserError, AccessDenied

try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    import xlsxwriter

# map_current_ratio = ['Receivable', 'Bank and Cash', 'Current Asset','Payable', 'Current Liability']
# map_current_ratio_Current_Asset = ['Receivable', 'Bank and Cash', 'Current Asset']
# map_current_ratio_Curent_Liability = ['Payable', 'Current Liability']

# map_quick_ratio = ['Receivable', 'Bank and Cash', 'Current Asset', 'Inventory', 'Payable', 'Current Liability']
# map_quick_ratio_Current_Asset = ['Receivable', 'Bank and Cash', 'Current Asset']
# map_quick_ratio_Current_Inventory = ['Inventory']
# map_quick_ratio_Current_Liability = ['Payable', 'Current Liability']

# map_cash_ratio = ['Bank and Cash', 'Giro', 'Payable', 'Current Liability']
# map_cash_ratio_Cash_Equivalent = ['Bank and Cash', 'Giro']
# map_cash_ratio_Current_Liability = ['Payable', 'Current Liability']

# map_debt_to_asset_ratio = ['Payable', 'Current Liability', ' -Current Liability', 'Receivable', 'Bank and Cash', 'Current Assets', 'Non-current Assets', 'Fixed Assets']
# map_debt_to_asset_ratio_Liability = ['Payable', 'Current Liability', 'Non-Current Liability']
# map_debt_to_asset_ratio_Asset = ['Receivable', 'Bank and Cash', 'Current Assets', 'Non-current Assets', 'Fixed Assets']

# map_debt_to_equity_ratio = ['Payable', 'Current Liabilities', 'Non-current Liabilities', 'Equity']
# map_debt_to_equity_ratio_Liability = ['Payable', 'Current Liabilities', 'Non-current Liabilities']
# map_debt_to_equity_ratio_Equity = ['Equity']

# map_long_term_debt_to_equity_ratio = ['Non-current Liabilities', 'Equity']
# map_long_term_debt_to_equity_ratio_Liability = ['Non-current Liabilities']
# map_long_term_debt_to_equity_ratio_Equity = ['Equity']

# map_times_interest_earned_ratio = ['Income', 'Interest', 'Tax', 'Depreciation', 'Amortization', 'Intereset Expense']
# map_times_interest_earned_ratio_EBITDA = ['Income', 'Interest', 'Tax', 'Depreciation', 'Amortization']
# map_times_interest_earned_ratio_Expense = ['Intereset Expense']


# map_EBITDA = ['Income', 'Interest', 'Tax', 'Depreciation', 'Amortization']

# map_return_on_asset = ['Income', 'Other income', 'Receivable', 'Bank and Cash', 'Current Assets', 'Non-current Assets', 'Fixed Assets']
# map_return_on_asset_Net_Income = ['Income', 'Other income']
# map_return_on_asset_Total_Asset = ['Receivable', 'Bank and Cash', 'Current Assets', 'Non-current Assets', 'Fixed Assets']

# map_return_on_equity = ['Income', 'Other income', 'Equity']
# map_return_on_equity_Net_Income = ['Income', 'Other income']
# map_return_on_equity_Total_Equity = ['Equity']




class FinancialRatioView(models.TransientModel):
    _inherit = "account.common.report"
    _name = 'account.financial.ratio'

    journal_ids = fields.Many2many('account.journal',

                                   string='Journals', required=True,
                                   default=[])
    display_account = fields.Selection(
        [('all', 'All'), ('movement', 'With movements'),
         ('not_zero', 'With balance is not equal to 0')],
        string='Display Accounts', required=True, default='movement')

    @api.model
    def view_report(self, option):
        r = self.env['account.financial.ratio'].search([('id', '=', option[0])])

        data = {
            'display_account': r.display_account,
            'model':self,
            'journals': r.journal_ids,
            'target_move': r.target_move,

        }
        if r.date_from:
            data.update({
                'date_from':r.date_from,
            })
        if r.date_to:
            data.update({
                'date_to':r.date_to,
            })

        filters = self.get_filter(option)
        records = self._get_report_values(data)
        currency = self._get_currency()
        
        return {
            'name': "Financial Ratio",
            'type': 'ir.actions.client',
            'tag': 'f_r',
            'filters': filters,            
            'report_lines': records['Accounts'],
            'debit_total': records['debit_total'],
            'credit_total': records['credit_total'],
            'currency': currency,
            'report_lines_current_ratio' : records['current_ratio'],
            'report_lines_quick_ratio' : records['quick_ratio'],
            'report_lines_capital_ratio' : records['capital_ratio'],
            'report_lines_cash_ratio' : records['cash_ratio'],
            'report_lines_debt_to_asset_ratio' : records['debt_to_asset_ratio'],
            'report_lines_debt_to_equity_ratio' : records['debt_to_equity_ratio'],
            'report_lines_long_term_debt_to_equity_ratio' : records['long_term_debt_to_equity_ratio'],
            'report_lines_times_interest_earned_ratio' : records['times_interest_earned_ratio'],
            'report_lines_EBITDA' : records['EBITDA'],
            'report_lines_return_on_asset' : records['return_on_asset'],
            'report_lines_return_on_equity' : records['return_on_equity'],
            'report_lines_profit_margin' : records['profit_margin'],
            'report_lines_gross_profit_margin' : records['gross_profit_margin'],
            'report_lines_ar_turnover_ratio' : records['ar_turnover_ratio'],
            'report_lines_merchandise_inventory' : records['merchandise_inventory'],
            'report_lines_total_assets' : records['total_assets'],
            'report_lines_net_fixed_assets' : records['net_fixed_assets'],
        }

    def get_filter(self, option):
        data = self.get_filter_data(option)
        filters = {}
        if data.get('journal_ids'):
            filters['journals'] = self.env['account.journal'].browse(data.get('journal_ids')).mapped('code')
        else:
            filters['journals'] = ['All']
        if data.get('target_move'):
            filters['target_move'] = data.get('target_move')
        if data.get('date_from'):
            filters['date_from'] = data.get('date_from')
        if data.get('date_to'):
            filters['date_to'] = data.get('date_to')

        filters['company_id'] = ''
        filters['journals_list'] = data.get('journals_list')
        filters['company_name'] = data.get('company_name')
        filters['target_move'] = data.get('target_move').capitalize()

        return filters

    def get_filter_data(self, option):
        r = self.env['account.financial.ratio'].search([('id', '=', option[0])])
        default_filters = {}
        company_id = self.env.company
        company_domain = [('company_id', '=', company_id.id)]
        journals = r.journal_ids if r.journal_ids else self.env['account.journal'].search(company_domain)

        filter_dict = {
            'journal_ids': r.journal_ids.ids,
            'company_id': company_id.id,
            'date_from': r.date_from,
            'date_to': r.date_to,
            'target_move': r.target_move,
            'journals_list': [(j.id, j.name, j.code) for j in journals],
            'company_name': company_id and company_id.name,
        }
        filter_dict.update(default_filters)
        return filter_dict

    def _get_list_report(self,report_name):
        list_report=[]
        if isinstance(report_name, list) == True:
            for list_rec in report_name:
                rec_list_report = self.env['account.financial.report'].search([('name', '=', list_rec)])
                for rec_reports in rec_list_report.account_type_ids:
                    list_report.append(rec_reports.name)
        else:
            rec_list_report = self.env['account.financial.report'].search([('name', '=', report_name)])
            for rec_reports in rec_list_report.account_type_ids:
                list_report.append(rec_reports.name)
        return list_report

    def _get_report_values(self, data):
        docs = data['model']
        display_account = data['display_account']
        journals = data['journals']        
        accounts = self.env['account.account'].search([])
        if not accounts:
            raise UserError(_("No Accounts Found! Please Add One"))
        account_res = self._get_accounts(accounts, display_account, data)        
        debit_total = 0
        debit_total = sum(x['debit'] for x in account_res)
        credit_total = sum(x['credit'] for x in account_res)

        account_types = self.env['account.account.type'].search([])
        if not account_types:
            raise UserError(_("No Account Types Found! Please Add One"))



        account_type_current_ratio = self._get_account_types(account_res, display_account, data, ['Current Assets' ,'Bank and Cash' ,'Receivable' ,'Inventory','Current Liabilities'])        
        current_Current_Asset = self._get_account_types_total(account_type_current_ratio, ['Current Assets' ,'Bank and Cash' ,'Receivable' ,'Inventory '], "Current Asset", data)
        current_Current_Liability = self._get_account_types_total(account_type_current_ratio, ['Current Liabilities'], "Current Liability", data)
        # current_ratio = [current_Current_Asset[0] if len(current_Current_Asset) > 0 else {}, current_Current_Liability[0] if len(current_Current_Liability) > 0 else {}]        
        current_ratio = self._value_type(current_Current_Asset,current_Current_Liability, data)

        account_type_quick_ratio = self._get_account_types(account_res, display_account, data, ['Current Assets', 'Inventory', 'Current Liabilities'])
        quick_Current_Asset = self._get_account_types_total(account_type_quick_ratio, ['Current Assets'], "Current Asset", data)
        quick_Current_Inventory = self._get_account_types_total(account_type_quick_ratio, ['Inventory'], "Inventory", data)
        quick_Current_Liability = self._get_account_types_total(account_type_quick_ratio, ['Current Liabilities'], "Current Liability", data)
        # quick_ratio = [quick_Current_Asset_Inventory[0] if len(quick_Current_Asset_Inventory) > 0 else {},quick_Current_Liability[0] if len(quick_Current_Liability) > 0 else {}]       
        quick_ratio = self._value_type_div_minus(quick_Current_Asset,quick_Current_Liability,quick_Current_Inventory, data)

        ##
        account_type_capital_ratio = self._get_account_types(account_res, display_account, data, ['Current Assets' ,'Bank and Cash' ,'Receivable' ,'Inventory','Current Liabilities'])
        capital_Current_Asset = self._get_account_types_total(account_type_capital_ratio, ['Current Assets' ,'Bank and Cash' ,'Receivable' ,'Inventory '], "Current Asset", data)
        capital_Current_Liability = self._get_account_types_total(account_type_capital_ratio, ['Current Liabilities'], "Current Liability", data)
        capital_ratio = self._value_type_minus(capital_Current_Asset,capital_Current_Liability, data)
        
        account_type_cash_ratio = self._get_account_types(account_res, display_account, data, ['Bank and Cash', 'Current Liabilities'])
        ratio_Cash_Equivalent = self._get_account_types_total(account_type_cash_ratio, ['Bank and Cash'], "(Cash + Cash Equivalent)", data)
        ratio_Current_Liability = self._get_account_types_total(account_type_cash_ratio, ['Current Liabilities'], "Current Liability", data)
        # cash_ratio = [ratio_Cash_Equivalent[0] if len(ratio_Cash_Equivalent) > 0 else {},ratio_Current_Liability[0] if len(ratio_Current_Liability) > 0 else {}]
        cash_ratio = self._value_type(ratio_Cash_Equivalent,ratio_Current_Liability, data)        

        account_type_debt_to_asset_ratio = self._get_account_types(account_res, display_account, data, ['Current Liabilities', 'Long Term Liability', 'Current Assets', 'Fixed Assets'])
        to_asset_ratio_Liability = self._get_account_types_total(account_type_debt_to_asset_ratio, ['Current Liabilities', 'Long Term Liability'], "Total Liability", data)
        to_asset_ratio_Asset = self._get_account_types_total(account_type_debt_to_asset_ratio, ['Current Assets', 'Fixed Assets'], "Total Asset", data)
        # debt_to_asset_ratio = [to_asset_ratio_Liability[0] if len(to_asset_ratio_Liability) > 0 else {},to_asset_ratio_Asset[0] if len(to_asset_ratio_Asset) > 0 else {}]
        debt_to_asset_ratio = self._value_type(to_asset_ratio_Liability,to_asset_ratio_Asset, data) 
        
        account_type_debt_to_equity_ratio = self._get_account_types(account_res, display_account, data, ['Current Liabilities' ,'Long Term Liability' ,'Equity'])
        to_equity_ratio_Liability = self._get_account_types_total(account_type_debt_to_equity_ratio, ['Current Liabilities' ,'Long Term Liability'], "Total Liability", data)
        to_equity_ratio_Equity = self._get_account_types_total(account_type_debt_to_equity_ratio, ['Equity'], "Total Equity", data)
        # debt_to_equity_ratio = [to_equity_ratio_Liability[0] if len(to_equity_ratio_Liability) > 0 else {},to_equity_ratio_Equity[0]  if len(to_equity_ratio_Equity) > 0 else {}]
        debt_to_equity_ratio = self._value_type(to_equity_ratio_Liability,to_equity_ratio_Equity, data)

        account_type_long_term_debt_to_equity_ratio = self._get_account_types(account_res, display_account, data, ['Long Term Liability' ,'Equity'])
        long_term_debt_to_equity_ratio_Liability = self._get_account_types_total(account_type_long_term_debt_to_equity_ratio, ['Long Term Liability'], "Long Term Liability", data)
        long_term_debt_to_equity_ratio_Equity = self._get_account_types_total(account_type_long_term_debt_to_equity_ratio, ['Equity'], "Total equity", data)
        # long_term_debt_to_equity_ratio = [long_term_debt_to_equity_ratio_Liability[0] if len(long_term_debt_to_equity_ratio_Liability) > 0 else {},long_term_debt_to_equity_ratio_Equity[0] if len(long_term_debt_to_equity_ratio_Equity) > 0 else {}]
        long_term_debt_to_equity_ratio = self._value_type(long_term_debt_to_equity_ratio_Liability,long_term_debt_to_equity_ratio_Equity, data)
        
        account_type_times_interest_earned_ratio = self._get_account_types(account_res, display_account, data, ['Income', 'Interest', 'Tax', 'Depreciation', 'Amortization', 'Intereset Expense'])
        earned_ratio_EBITDA = self._get_account_types_total(account_type_times_interest_earned_ratio, ['Income', 'Interest', 'Tax', 'Depreciation', 'Amortization'], "EBITDA", data)
        earned_ratio_Expense = self._get_account_types_total(account_type_times_interest_earned_ratio, ['Intereset Expense'], "Interest Expense", data)
        # times_interest_earned_ratio = [earned_ratio_EBITDA[0] if len(earned_ratio_EBITDA) > 0 else {},earned_ratio_Expense[0] if len(earned_ratio_Expense) > 0 else {}]
        times_interest_earned_ratio = self._value_type(earned_ratio_EBITDA,earned_ratio_Expense, data)

        account_type_EBITDA = self._get_account_types(account_res, display_account, data, ['Income', 'Interest', 'Tax', 'Depreciation', 'Amortization'])
        ratio_EBITDA = self._get_account_types_total(account_type_EBITDA, ['Income', 'Interest', 'Tax', 'Depreciation', 'Amortization'], "Net Income + Interst + Tax + Depreciation + Amortization", data)
        # EBITDA = [ratio_EBITDA[0] if len(ratio_EBITDA) > 0 else {}]
        EBITDA = self._value_type_one_row(ratio_EBITDA)

        account_type_return_on_asset = self._get_account_types(account_res, display_account, data, ['Income', 'Current Assets', 'Fixed Assets'])
        return_on_asset_Net_Income = self._get_account_types_total(account_type_return_on_asset, ['Income'], "Net Income", data)
        return_on_asset_Total_Asset = self._get_account_types_total(account_type_return_on_asset, ['Current Assets', 'Fixed Assets'], "Total Asset", data)
        # return_on_asset = [return_on_asset_Net_Income[0] if len(return_on_asset_Net_Income) > 0 else {},return_on_asset_Total_Asset[0] if len(return_on_asset_Total_Asset) > 0 else {}]
        return_on_asset = self._value_type(return_on_asset_Net_Income,return_on_asset_Total_Asset, data)
        
        account_type_return_on_equity = self._get_account_types(account_res, display_account, data, self._get_list_report(['Income', 'Equity']))
        return_on_equity_Net_Income = self._get_account_types_total(account_type_return_on_equity, self._get_list_report(['Income']), "Net Income", data)
        return_on_equity_Total_Equity = self._get_account_types_total(account_type_return_on_equity, self._get_list_report(['Equity']), "Total Asset", data)
        # return_on_equity = [return_on_equity_Net_Income[0] if len(return_on_equity_Net_Income) > 0 else {},return_on_equity_Total_Equity[0] if len(return_on_equity_Total_Equity) > 0 else {}]
        return_on_equity = self._value_type(return_on_equity_Net_Income,return_on_equity_Total_Equity, data)

        ##
        account_type_net_profit_margin = self._get_account_types(account_res, display_account, data, ['Income', 'Sales', 'Return Order', 'Discount Item'])
        profit_margin_Net_Profit = self._get_account_types_total(account_type_net_profit_margin, ['Income'], "Net Profit", data)
        profit_margin_Sales = self._get_account_types_total(account_type_net_profit_margin, ['Sales', 'Return Order', 'Discount Item'], "Sales", data)
        profit_margin = self._value_type(profit_margin_Net_Profit,profit_margin_Sales, data)

        ##
        account_type_gross_profit_margin = self._get_account_types(account_res, display_account, data, ['Income', 'Cost of Revenue', 'Sales', 'Return Order', 'Discount Item'])
        gross_profit_margin_Profit = self._get_account_types_total(account_type_gross_profit_margin, ['Income'], "Gross Profit", data)
        gross_profit_margin_Profit_cogs = self._get_account_types_total(account_type_gross_profit_margin, ['Cost of Revenue'], "Gross Profit COGS", data)
        gross_profit_margin_Sales = self._get_account_types_total(account_type_gross_profit_margin, ['Sales', 'Return Order', 'Discount Item'], "Sales", data)
        gross_profit_margin = self._value_type_div_minus(gross_profit_margin_Profit,gross_profit_margin_Sales,gross_profit_margin_Profit_cogs, data)
        
        ###
        account_type_ar_turnover_ratio = self._get_account_types(account_res, display_account, data, ['Income','Receivable'])
        ar_turnover_ratio_Revenue = self._get_account_types_total_minus(account_type_ar_turnover_ratio, ['Income'], "Revenue", data)
        ar_turnover_ratio_Receivable = self._get_account_types_total(account_type_ar_turnover_ratio, ['Receivable'], "Receivable", data)
        ar_turnover_ratio = self._value_type(ar_turnover_ratio_Revenue,ar_turnover_ratio_Receivable, data)

        ###
        account_type_merchandise_inventory = self._get_account_types(account_res, display_account, data, ['Cost of Revenue','Inventory'])
        merchandise_inventory_Cost_Of_Revenue = self._get_account_types_total(account_type_merchandise_inventory, ['Cost of Revenue'], "Cost of Revenue", data)
        merchandise_inventory_Inventory = self._get_account_types_total(account_type_merchandise_inventory, ['Inventory'], "Inventory", data)
        merchandise_inventory = self._value_type(merchandise_inventory_Cost_Of_Revenue,merchandise_inventory_Inventory, data)        

        ###
        account_type_total_assets = self._get_account_types(account_res, display_account, data, ['Income ', 'Receivable', 'Bank and Cash', 'Current Assetss', 'Fixed Assets'])
        total_assets_Revenue = self._get_account_types_total_minus(account_type_total_assets, ['Income '], "Revenue", data)
        total_assets_Total_Assets = self._get_account_types_total(account_type_total_assets, ['Receivable', 'Bank and Cash', 'Current Assetss', 'Fixed Assets'], "Total Assets", data)
        total_assets = self._value_type(total_assets_Revenue,total_assets_Total_Assets, data)
        
        ###
        account_type_net_fixed_assets = self._get_account_types(account_res, display_account, data, ['Income', 'Fixed Assets'])
        net_fixed_assets_Revenue = self._get_account_types_total_minus(account_type_net_fixed_assets, ['Income'], "Revenue", data)
        net_fixed_assets_Fixed_Asset = self._get_account_types_total(account_type_net_fixed_assets, ['Fixed Assets'], "Fixed Asset", data)
        net_fixed_assets = self._value_type(net_fixed_assets_Revenue,net_fixed_assets_Fixed_Asset, data)

        return {
            'doc_ids': self.ids,
            'debit_total': debit_total,
            'credit_total': credit_total,
            'docs': docs,
            'time': time,
            'Accounts': account_res,
            'current_ratio' : current_ratio,
            'quick_ratio' : quick_ratio,
            'capital_ratio' : capital_ratio,
            'cash_ratio' : cash_ratio,
            'debt_to_asset_ratio' : debt_to_asset_ratio,
            'debt_to_equity_ratio' : debt_to_equity_ratio,
            'long_term_debt_to_equity_ratio' : long_term_debt_to_equity_ratio,
            'times_interest_earned_ratio' : times_interest_earned_ratio,
            'EBITDA' : EBITDA,
            'return_on_asset' : return_on_asset,
            'return_on_equity' : return_on_equity,
            'profit_margin' : profit_margin,
            'gross_profit_margin' : gross_profit_margin,
            'ar_turnover_ratio' : ar_turnover_ratio,
            'merchandise_inventory' : merchandise_inventory,
            'total_assets' : total_assets,
            'net_fixed_assets' : net_fixed_assets,
        }

    def _value_type_div_minus(self,value1,value2,value3_minus,data):
        res_val1 = 0.0
        res_val2 = 0.0
        res_val3_minus = 0.0
        res_init_val1 = 0.0
        res_init_val2 = 0.0
        res_init_val3_minus = 0.0
        value = []
        
        if len(value1)>0:
            for x in value1:
                res_val1 = x['balance']
                if data.get('date_from'):
                    balance = x['Init_balance']
                    if balance != None:
                        res_init_val1 = balance['balance']
        
        if len(value3_minus)>0:
            for x in value3_minus:
                res_val3_minus = x['balance']
                if data.get('date_from'):
                    balance = x['Init_balance']
                    if balance != None:
                        res_init_val3_minus = balance['balance']   

        if len(value2)>0:
            for x in value2:
                res_val2 = x['balance']
                if data.get('date_from'):
                    balance = x['Init_balance']
                    if balance != None:
                        res_init_val2 = balance['balance']

        if res_val2 == 0:
            total_balance = 0
        else:
            total_balance = ((res_val1-res_val3_minus) / res_val2) * 100
        
        total_init_balance = 0.0        
        res = dict((fn, 0.0) for fn in ['balance'])
        if total_balance != 0:
            res['balance'] = total_balance
        if data.get('date_from'):
            if res_init_val2 == 0:
                total_init_balance = 0
            else:
                total_init_balance = ((res_init_val1-res_init_val3_minus) / res_init_val2) * 100
            if total_init_balance != 0:
                res['Init_balance'] = {'balance' : total_init_balance}
                if 'balance' not in res:
                    res['balance'] = 0
        value.append(res)
        return value

    def _value_type_minus(self,value1,value2,data):
        res_val1 = 0.0
        res_val2 = 0.0
        res_init_val1 = 0.0
        res_init_val2 = 0.0
        value = []
        if len(value1)>0:
            for x in value1:
                res_val1 = x['balance']
                if data.get('date_from'):
                    balance = x['Init_balance']
                    if balance != None:
                        res_init_val1 = balance['balance']
        if len(value2)>0:
            for x in value2:
                res_val2 = x['balance']
                if data.get('date_from'):
                    balance = x['Init_balance']
                    if balance != None:
                        res_init_val2 = balance['balance']                        
        total_balance = (res_val1 - res_val2) * 100
        total_init_balance = 0.0        
        res = dict((fn, 0.0) for fn in ['balance'])
        if total_balance != 0:
            res['balance'] = total_balance
        
        if data.get('date_from'):
            total_init_balance = (res_init_val1 - res_init_val2) * 100
            if total_init_balance != 0:
                res['Init_balance'] = {'balance' : total_init_balance}
                if 'balance' not in res:
                    res['balance'] = 0
        value.append(res)
        return value

    def _value_type_turnover_ratio(self,value1,value2,data):
        res_val1 = 0.0
        res_val2 = 0.0
        res_init_val1 = 0.0
        res_init_val2 = 0.0
        value = []
        if len(value1)>0:
            for x in value1:
                res_val1 = x['balance']
                if data.get('date_from'):
                    balance = x['Init_balance']
                    if balance != None:
                        res_init_val1 = balance['balance']
        if len(value2)>0:
            for x in value2:
                res_val2 = x['balance']
                if data.get('date_from'):
                    balance = x['Init_balance']
                    if balance != None:
                        res_init_val2 = balance['balance']                        
        if res_val2 == 0:
            total_balance = 0
        else:
            total_balance = (res_val1 / res_val2) * 100
        total_init_balance = 0.0        
        res = dict((fn, 0.0) for fn in ['balance'])
        if total_balance != 0:
            res['balance'] = total_balance
        
        if data.get('date_from'):
            if res_init_val2 == 0:
                total_init_balance = 0
            else:
                total_init_balance = (res_init_val1 / res_init_val2) * 100

            if res_val2 == 0:
                total_balance = 0
            else:
                total_balance = (res_val1 / ((res_init_val2/res_val2))/2) * 100
            
            res['balance'] = total_balance
            if total_init_balance != 0:
                res['Init_balance'] = {'balance' : total_init_balance}
                if 'balance' not in res:
                    res['balance'] = 0
        value.append(res)
        return value

    def _value_type(self,value1,value2,data):
        res_val1 = 0.0
        res_val2 = 0.0
        res_init_val1 = 0.0
        res_init_val2 = 0.0
        value = []
        if len(value1)>0:
            for x in value1:
                res_val1 = x['balance']
                if data.get('date_from'):
                    balance = x['Init_balance']
                    if balance != None:
                        res_init_val1 = balance['balance']
        if len(value2)>0:
            for x in value2:
                res_val2 = x['balance']
                if data.get('date_from'):
                    balance = x['Init_balance']
                    if balance != None:
                        res_init_val2 = balance['balance']                        
        if res_val2 == 0:
            total_balance = 0
        else:
            total_balance = (res_val1 / res_val2) * 100
        total_init_balance = 0.0        
        res = dict((fn, 0.0) for fn in ['balance'])
        if total_balance != 0:
            res['balance'] = total_balance
        
        if data.get('date_from'):
            if res_init_val2 == 0:
                total_init_balance = 0
            else:
                total_init_balance = (res_init_val1 / res_init_val2) * 100
            if total_init_balance != 0:
                res['Init_balance'] = {'balance' : total_init_balance}
                if 'balance' not in res:
                    res['balance'] = 0
        value.append(res)
        return value

    def _value_type_one_row(self,value1):
        value=[]
        if len(value1)>0:
            for x in value1:
                value.append(x)
        return value

    @api.model
    def create(self, vals):
        vals['target_move'] = 'posted'
        res = super(FinancialRatioView, self).create(vals)
        return res

    def write(self, vals):
        if vals.get('target_move'):
            vals.update({'target_move': vals.get('target_move').lower()})
        if vals.get('journal_ids'):
            vals.update({'journal_ids': [(6, 0, vals.get('journal_ids'))]})
        if vals.get('journal_ids') == []:
            vals.update({'journal_ids': [(5,)]})
        res = super(FinancialRatioView, self).write(vals)
        return res

    def _get_accounts(self, accounts, display_account, data):

        account_result = {}
        # Prepare sql query base on selected parameters from wizard
        tables, where_clause, where_params = self.env['account.move.line']._query_get()
        tables = tables.replace('"', '')
        if not tables:
            tables = 'account_move_line'
        wheres = [""]
        if where_clause.strip():
            wheres.append(where_clause.strip())
        filters = " AND ".join(wheres)
        if data['target_move'] == 'posted':
            filters += " AND account_move_line__move_id.state = 'posted'"
        else:
            filters += " AND account_move_line__move_id.state in ('draft','posted')"
        if data.get('date_from'):
            filters += " AND account_move_line.date >= '%s'" % data.get('date_from')
        if data.get('date_to'):
            filters += " AND account_move_line.date <= '%s'" % data.get('date_to')

        if data['journals']:
            filters += ' AND jrnl.id IN %s' % str(tuple(data['journals'].ids) + tuple([0]))
        tables += 'JOIN account_journal jrnl ON (account_move_line.journal_id=jrnl.id)'
        # compute the balance, debit and credit for the provided accounts
        request = (
                    "SELECT account_id AS id, SUM(debit) AS debit, SUM(credit) AS credit, (SUM(debit) - SUM(credit)) AS balance" + \
                    " FROM " + tables + " WHERE account_id IN %s " + filters + " GROUP BY account_id")
        params = (tuple(accounts.ids),) + tuple(where_params)
        self.env.cr.execute(request, params)

        value_fetch = self.env.cr.dictfetchall()        
        for row in value_fetch:        
            account_result[row.pop('id')] = row
        
        account_res = []
        for account in accounts:
            res = dict((fn, 0.0) for fn in ['credit', 'debit', 'balance'])
            currency = account.currency_id and account.currency_id or account.company_id.currency_id
            res['code'] = account.code
            res['name'] = account.name
            res['id'] = account.id
            res['type_id'] = account.user_type_id.id
            res['type_name'] = account.user_type_id.name
            if data.get('date_from'):

                res['Init_balance'] = self.get_init_bal(account, display_account, data)

            if account.id in account_result:
                res['debit'] = account_result[account.id].get('debit')
                res['credit'] = account_result[account.id].get('credit')
                res['balance'] = account_result[account.id].get('balance')
            if display_account == 'all':
                account_res.append(res)
            if display_account == 'not_zero' and not currency.is_zero(
                    res['balance']):
                account_res.append(res)
            if display_account == 'movement' and (
                    not currency.is_zero(res['debit']) or not currency.is_zero(
                    res['credit'])):
                account_res.append(res)
        return account_res

    def get_init_bal(self, account, display_account, data):            
        if data.get('date_from'):

            tables, where_clause, where_params = self.env[
                'account.move.line']._query_get()
            tables = tables.replace('"', '')
            if not tables:
                tables = 'account_move_line'
            wheres = [""]
            if where_clause.strip():
                wheres.append(where_clause.strip())
            filters = " AND ".join(wheres)
            if data['target_move'] == 'posted':
                filters += " AND account_move_line__move_id.state = 'posted'"
            else:
                filters += " AND account_move_line__move_id.state in ('draft','posted')"
            if data.get('date_from'):
                filters += " AND account_move_line.date < '%s'" % data.get('date_from')

            if data['journals']:
                filters += ' AND jrnl.id IN %s' % str(tuple(data['journals'].ids) + tuple([0]))
            tables += 'JOIN account_journal jrnl ON (account_move_line.journal_id=jrnl.id)'

            # compute the balance, debit and credit for the provided accounts
            request = (
                    "SELECT account_id AS id, SUM(debit) AS debit, SUM(credit) AS credit, (SUM(debit) - SUM(credit)) AS balance" + \
                    " FROM " + tables + " WHERE account_id = %s" % account.id + filters + " GROUP BY account_id")
            params = tuple(where_params)
            self.env.cr.execute(request, params)

            value_fetch = self.env.cr.dictfetchall()           
            for row in value_fetch:
                return row

    @api.model
    def _get_currency(self):
        journal = self.env['account.journal'].browse(
            self.env.context.get('default_journal_id', False))
        print("ini currency")
        print(journal.currency_id)
        if journal.currency_id:
            return journal.currency_id.id
        lang = self.env.user.lang
        if not lang:
            lang = 'en_US'
        lang = lang.replace("_", '-')
        currency_array = [self.env.company.currency_id.symbol,
                          self.env.company.currency_id.position,
                          lang]
        print(currency_array)
        print("akhir currency")
        return currency_array

    def _get_account_types(self, accounts, display_account, data, account_types):
        account_type_res = []
        for account in accounts:                        
            res = dict((fn, 0.0) for fn in ['balance'])
            if account['type_name'] in account_types:
                # print('account_types')
                # print(account_types)
                # print('account')
                # print(account)
                # print("account['type_name']")
                # print(account['type_name'])
                x = list(filter(lambda a: a['type_name'] == account['type_name'], account_type_res))
                if len(x) > 0:
                    x[0]['balance'] += account['balance']
                    if data.get('date_from'):
                        balance = account['Init_balance']
                        if balance != None:
                            if 'Init_balance' not in res:
                                res['Init_balance'] = 0         
                            res['Init_balance'] += balance['balance']
                        else:
                            res['Init_balance'] = balance
                else:
                    res['balance'] = account['balance']
                    res['type_id'] = account['type_id']
                    res['type_name'] = account['type_name']

                    if data.get('date_from'):
                        balance = account['Init_balance']
                        if balance != None:
                            res['Init_balance'] = {'balance' : balance['balance']}
                        else:
                            res['Init_balance'] = balance
                    account_type_res.append(res)
        return account_type_res

    def _get_account_types_total(self, account_types, types, name_type,data):
        account_type_res = []
        for account_detail in account_types:
            print('account_types')
            print(account_types)
            print('types')
            print(types)
            print('account_detail')
            print(account_detail)
            print("account_detail['type_name']")
            print(account_detail['type_name'])
            res = dict((fn, 0.0) for fn in ['balance'])
            if account_detail['type_name'] in types:
                x = list(filter(lambda a: a['type_name'] == name_type, account_type_res))
                if len(x) > 0:                                        
                    x[0]['balance'] += account_detail['balance']                    
                    if data.get('date_from'):
                        balance = account_detail['Init_balance']
                        if balance != None:
                            if 'Init_balance' not in res:
                                res['Init_balance'] = 0                       
                            res['Init_balance'] += balance['balance']
                        else:
                            res['Init_balance'] = balance
                else:
                    res['balance'] = account_detail['balance']
                    res['type_name'] = name_type
                    if data.get('date_from'):
                        balance = account_detail['Init_balance']
                        if balance != None:
                            res['Init_balance'] = {'balance' : balance['balance']}
                        else:
                            res['Init_balance'] = balance
                    account_type_res.append(res)
        return account_type_res


    def _get_account_types_total_minus(self, account_types, types, name_type,data):
        account_type_res = []
        for account_detail in account_types:
            res = dict((fn, 0.0) for fn in ['balance'])
            if account_detail['type_name'] in types:
                x = list(filter(lambda a: a['type_name'] == name_type, account_type_res))
                if len(x) > 0:
                    x[0]['balance'] -= account_detail['balance']
                    if data.get('date_from'):
                        balance = account_detail['Init_balance']
                        if balance != None:
                            if 'Init_balance' not in res:
                                res['Init_balance'] = 0         
                            res['Init_balance'] -= balance['balance']
                        else:
                            res['Init_balance'] = balance
                else:
                    res['balance'] = account_detail['balance']
                    res['type_name'] = name_type
                    if data.get('date_from'):
                        balance = account_detail['Init_balance']
                        if balance != None:
                            res['Init_balance'] = {'balance' : balance['balance']}
                        else:
                            res['Init_balance'] = balance
                    account_type_res.append(res)
        return account_type_res

        # account_result = {}
        # # Prepare sql query base on selected parameters from wizard
        # tables, where_clause, where_params = self.env['account.move.line']._query_get()
        # tables = tables.replace('"', '')
        # if not tables:
        #     tables = 'account_move_line'
        # wheres = [""]
        # if where_clause.strip():
        #     wheres.append(where_clause.strip())
        # filters = " AND ".join(wheres)
        # if data['target_move'] == 'posted':
        #     filters += " AND account_move_line__move_id.state = 'posted'"
        # else:
        #     filters += " AND account_move_line__move_id.state in ('draft','posted')"
        # if data.get('date_from'):
        #     filters += " AND account_move_line.date >= '%s'" % data.get('date_from')
        # if data.get('date_to'):
        #     filters += " AND account_move_line.date <= '%s'" % data.get('date_to')

        # if data['journals']:
        #     filters += ' AND jrnl.id IN %s' % str(tuple(data['journals'].ids) + tuple([0]))
        # tables += 'JOIN account_journal jrnl ON (account_move_line.journal_id=jrnl.id)'
        # # compute the balance, debit and credit for the provided accounts
        # request = (
        #             "SELECT account_id AS id, SUM(debit) AS debit, SUM(credit) AS credit, (SUM(debit) - SUM(credit)) AS balance" + \
        #             " FROM " + tables + " WHERE account_id IN %s " + filters + " GROUP BY account_id")
        # params = (tuple(accounts.ids),) + tuple(where_params)
        # self.env.cr.execute(request, params)
        # for row in self.env.cr.dictfetchall():
        #     account_result[row.pop('id')] = row

        # account_res = []
        # for account in accounts:
        #     res = dict((fn, 0.0) for fn in ['credit', 'debit', 'balance'])
        #     currency = account.currency_id and account.currency_id or account.company_id.currency_id
        #     res['code'] = account.code
        #     res['name'] = account.name
        #     res['id'] = account.id
        #     if data.get('date_from'):

        #         res['Init_balance'] = self.get_init_bal(account, display_account, data)

        #     if account.id in account_result:
        #         res['debit'] = account_result[account.id].get('debit')
        #         res['credit'] = account_result[account.id].get('credit')
        #         res['balance'] = account_result[account.id].get('balance')
        #     if display_account == 'all':
        #         account_res.append(res)
        #     if display_account == 'not_zero' and not currency.is_zero(
        #             res['balance']):
        #         account_res.append(res)
        #     if display_account == 'movement' and (
        #             not currency.is_zero(res['debit']) or not currency.is_zero(
        #             res['credit'])):
        #         account_res.append(res)
        # return account_res

    def get_dynamic_xlsx_report(self, data, response ,report_data, dfr_data):
        report_data_main = json.loads(report_data)
        output = io.BytesIO()
        total = json.loads(dfr_data)
        filters = json.loads(data)
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        sheet = workbook.add_worksheet()
        head = workbook.add_format({'align': 'center', 'bold': True,
                                    'font_size': '20px'})
        sub_heading = workbook.add_format(
            {'align': 'center', 'bold': True, 'font_size': '10px',
             'border': 1,
             'border_color': 'black'})
        txt = workbook.add_format({'font_size': '10px', 'border': 1})
        txt_l = workbook.add_format({'font_size': '10px', 'border': 1, 'bold': True})
        sheet.merge_range('A2:D3', filters.get('company_name') + ':' + ' Financial Ratio', head)
        date_head = workbook.add_format({'align': 'center', 'bold': True,
                                         'font_size': '10px'})
        date_style = workbook.add_format({'align': 'center',
                                          'font_size': '10px'})
        if filters.get('date_from'):
            sheet.merge_range('A4:B4', 'From: '+filters.get('date_from') , date_head)
        if filters.get('date_to'):
            sheet.merge_range('C4:D4', 'To: '+ filters.get('date_to'), date_head)
        sheet.merge_range('A5:D6', 'Journals: ' + ', '.join([ lt or '' for lt in filters['journals'] ]) + '  Target Moves: '+ filters.get('target_move'), date_head)
        sheet.write('A7', 'Code', sub_heading)
        sheet.write('B7', 'Amount', sub_heading)
        if filters.get('date_from'):
            sheet.write('C7', 'Initial Debit', sub_heading)
            sheet.write('D7', 'Initial Credit', sub_heading)
            sheet.write('E7', 'Debit', sub_heading)
            sheet.write('F7', 'Credit', sub_heading)
        else:
            sheet.write('C7', 'Debit', sub_heading)
            sheet.write('D7', 'Credit', sub_heading)

        row = 6
        col = 0
        sheet.set_column(5, 0, 15)
        sheet.set_column(6, 1, 15)
        sheet.set_column(7, 2, 26)
        if filters.get('date_from'):
            sheet.set_column(8, 3, 15)
            sheet.set_column(9, 4, 15)
            sheet.set_column(10, 5, 15)
            sheet.set_column(11, 6, 15)
        else:

            sheet.set_column(8, 3, 15)
            sheet.set_column(9, 4, 15)
        for rec_data in report_data_main:

            row += 1
            sheet.write(row, col, rec_data['code'], txt)
            sheet.write(row, col + 1, rec_data['name'], txt)
            if filters.get('date_from'):
                if rec_data.get('Init_balance'):
                    sheet.write(row, col + 2, rec_data['Init_balance']['debit'], txt)
                    sheet.write(row, col + 3, rec_data['Init_balance']['credit'], txt)
                else:
                    sheet.write(row, col + 2, 0, txt)
                    sheet.write(row, col + 3, 0, txt)

                sheet.write(row, col + 4, rec_data['debit'], txt)
                sheet.write(row, col + 5, rec_data['credit'], txt)

            else:
                sheet.write(row, col + 2, rec_data['debit'], txt)
                sheet.write(row, col + 3, rec_data['credit'], txt)
        sheet.write(row+1, col, 'Total', sub_heading)
        if filters.get('date_from'):
            sheet.write(row + 1, col + 4, total.get('debit_total'), txt_l)
            sheet.write(row + 1, col + 5, total.get('credit_total'), txt_l)
        else:
            sheet.write(row + 1, col + 2, total.get('debit_total'), txt_l)
            sheet.write(row + 1, col + 3, total.get('credit_total'), txt_l)

        workbook.close()
        output.seek(0)
        response.stream.write(output.read())
        output.close()
