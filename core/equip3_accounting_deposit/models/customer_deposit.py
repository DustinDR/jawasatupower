from odoo import api, fields, models, _
from datetime import datetime, date, timedelta
import pytz
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import UserError, ValidationError


class CustomerDeposit(models.Model):
    _name = 'customer.deposit'
    _description = 'Customer Deposit'
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']

    name = fields.Char(string="Name", readonly=True, tracking=True)
    is_deposit = fields.Boolean(string="Is Deposit")
    partner_id = fields.Many2one('res.partner', string="Customer/Vendor", tracking=True)
    amount = fields.Monetary(currency_field='currency_id', string="Amount", tracking=True)
    communication = fields.Char(string="Reference", tracking=True)
    payment_date = fields.Datetime(string="Payment Date", tracking=True)
    remaining_amount = fields.Monetary(string="Remaining Amount", tracking=True)
    return_deposit = fields.Many2one('account.move', string="Return Deposit", readonly=True, tracking=True)
    deposit_reconcile_journal_id = fields.Many2one('account.journal', string="Deposit Reconcile Journal", tracking=True)
    journal_id = fields.Many2one('account.journal', string="Payment Method", tracking=True)
    deposit_account_id = fields.Many2one('account.account', string="Deposit Account", domain=[('type', '=', 'payable')],
                                         tracking=True)
    filter_deposit_account_ids = fields.Many2many('account.account', string='Filter Depoist Account',
                                                  compute='_compute_deposit_account_ids')
    currency_id = fields.Many2one('res.currency', string='Currency', tracking=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('to_approve', 'Waiting For Approval'),
        ('rejected', 'Rejected'),
        ('post', 'Received'),
        ('posted', 'Posted'),
        ('returned', 'Returned'),
        ('converted', 'Converted to Revenue'),
        ('reconciled', 'Reconciled'),
        ('cancelled', 'Cancelled'),
    ],
        default='draft', track_visibility='onchange', copy=False, string="Status", tracking=True)
    deposit_move_id = fields.Many2one('account.move', string="Journal Entry", readonly=True, tracking=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company, tracking=True)
    suitable_journal_ids = fields.Many2many('account.journal', tracking=True)
    is_show_cancel = fields.Boolean(compute="_compute_show_cancel", string="Show Cancel")
    invoice_count = fields.Integer(compute="_compute_invoice", string='Invoices', copy=False, default=0, store=True)
    reconcile_count = fields.Integer(compute="_compute_reconcile", string='Invoices', copy=False, default=0, store=True)
    reconcile_deposit_ids = fields.Many2many('account.move', 'reconcile_customer_deposit_rel', 'deposit_id', 'move_id',
                                             string="Reconciled")
    invoice_deposit_ids = fields.Many2many('account.move', 'customer_deposit_invoice_rel', 'deposit_id', 'invoice_id',
                                           string="Invoiced")
    branch_id = fields.Many2one('res.branch', default=lambda self: self.env.user.branch_id, string="Branch")
    approval_matrix_id = fields.Many2one('approval.matrix.accounting', string="Approval Matrix",
                                         compute='_get_approval_matrix')
    is_customer_deposit_approval_matrix = fields.Boolean(string="Is Customer Deposite Approval Matrix",
                                                         compute='_get_approve_button_from_config')
    approved_matrix_ids = fields.One2many('approval.matrix.accounting.lines', 'customer_deposit_id',
                                          string="Approved Matrix")
    approval_matrix_line_id = fields.Many2one('approval.matrix.accounting.lines', string='Approval Matrix Line',
                                              compute='_get_approve_button', store=False)
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button', store=False)
    state1 = fields.Selection(related="state", tracking=False)
    state2 = fields.Selection(related="state", tracking=False)
    request_partner_id = fields.Many2one('res.partner', string="Requested Partner")

    def action_approve(self):
        for record in self:
            action_id = self.env.ref('equip3_accounting_deposit.action_customer_deposit')
            template_id = self.env.ref('equip3_accounting_deposit.email_template_customer_deposit_approval_matrix')
            template_id_submitter = self.env.ref('equip3_accounting_deposit.email_template_customer_deposit_submitter_approval_matrix')
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = base_url + '/web#id=' + str(record.id) + '&action='+ str(action_id.id) + '&view_type=form&model=customer.deposit'
            user = self.env.user
            currency = ''
            if record.currency_id.position == 'before':
               currency = record.currency_id.symbol + str(record.amount)
            else:
                currency = str(record.amount) + ' ' + record.currency_id.symbol
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_ids.ids and \
                        user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    utc_datetime = datetime.now()
                    local_timezone = pytz.timezone(self.env.user.tz)
                    local_datetime = utc_datetime.replace(tzinfo=pytz.utc)
                    local_datetime = local_datetime.astimezone(local_timezone).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                    if name != '':
                        name += "\n • %s: Approved - %s" % (self.env.user.name, local_datetime)
                    else:
                        name += "• %s: Approved - %s" % (self.env.user.name, local_datetime)

                    approval_matrix_line_id.write({
                        'last_approved': self.env.user.id, 'state_char': name,
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
                        next_approval_matrix_line_id = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
                        if next_approval_matrix_line_id and len(next_approval_matrix_line_id[0].user_ids) > 1:
                            for approving_matrix_line_user in next_approval_matrix_line_id[0].user_ids:
                                ctx = {
                                    'email_from' : self.env.user.company_id.email,
                                    'email_to' : approving_matrix_line_user.partner_id.email,
                                    'approver_name' : approving_matrix_line_user.name,
                                    'date': date.today(),
                                    'submitter' : self.env.user.name,
                                    'url' : url,
                                    "due_date": record.payment_date.strftime(DEFAULT_SERVER_DATE_FORMAT),
                                    "currency": currency,
                                }
                                template_id.sudo().with_context(ctx).send_mail(record.id, True)
                        else:
                            if next_approval_matrix_line_id and next_approval_matrix_line_id[0].user_ids:
                                ctx = {
                                    'email_from' : self.env.user.company_id.email,
                                    'email_to' : next_approval_matrix_line_id[0].user_ids[0].partner_id.email,
                                    'approver_name' : next_approval_matrix_line_id[0].user_ids[0].name,
                                    'date': date.today(),
                                    'submitter' : self.env.user.name,
                                    'url' : url,
                                    "due_date": record.payment_date.strftime(DEFAULT_SERVER_DATE_FORMAT),
                                    "currency": currency,
                                }
                                template_id.sudo().with_context(ctx).send_mail(record.id, True)
            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r: r.approved)):
                record.customer_deposit_post()
                ctx = {
                    'email_from' : self.env.user.company_id.email,
                    'email_to' : record.request_partner_id.email,
                    'approver_name' : record.name,
                    'date': date.today(),
                    'create_date': record.create_date.date(),
                    'submitter' : self.env.user.name,
                    'url' : url,
                    "due_date": record.payment_date.strftime(DEFAULT_SERVER_DATE_FORMAT),
                    "currency": currency,
                }
                template_id_submitter.sudo().with_context(ctx).send_mail(record.id, True)

    @api.depends('amount', 'company_id', 'branch_id')
    def _get_approval_matrix(self):
        for record in self:
            matrix_id = False
            matrix_id = self.env['approval.matrix.accounting'].search([
                ('company_id', '=', record.company_id.id),
                ('branch_id', '=', record.branch_id.id),
                ('min_amount', '<=', record.amount),
                ('max_amount', '>=', record.amount),
                ('approval_matrix_type', '=', 'customer_deposit_approval_matrix')
            ], limit=1)
            record.approval_matrix_id = matrix_id
            record._compute_approving_matrix_lines()

    def action_reject(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Customer Deposit ',
            'res_model': 'customer.deposit.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
        }

    @api.onchange('approval_matrix_id')
    def _compute_approving_matrix_lines(self):
        data = [(5, 0, 0)]
        for record in self:
            if record.state == 'draft' and record.is_customer_deposit_approval_matrix:
                record.approved_matrix_ids = []
                counter = 1
                record.approved_matrix_ids = []
                for rec in record.approval_matrix_id:
                    for line in rec.approval_matrix_line_ids:
                        data.append((0, 0, {
                            'sequence': counter,
                            'user_ids': [(6, 0, line.user_ids.ids)],
                            'minimum_approver': line.minimum_approver,
                        }))
                        counter += 1
                record.approved_matrix_ids = data

    def action_request_for_approval(self):
        for record in self:
            action_id = self.env.ref('equip3_accounting_deposit.action_customer_deposit')
            template_id = self.env.ref('equip3_accounting_deposit.email_template_customer_deposit_approval_matrix')
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = base_url + '/web#id=' + str(record.id) + '&action='+ str(action_id.id) + '&view_type=form&model=customer.deposit'
            currency = ''
            if record.currency_id.position == 'before':
               currency = record.currency_id.symbol + ' ' + str(record.amount)
            else:
                currency = str(record.amount) + ' ' + record.currency_id.symbol
            record.request_partner_id = self.env.user.partner_id.id
            if record.approved_matrix_ids and len(record.approved_matrix_ids[0].user_ids) > 1:
                for approved_matrix_id in record.approved_matrix_ids[0].user_ids:
                    approver = approved_matrix_id
                    ctx = {
                        'email_from' : self.env.user.company_id.email,
                        'email_to' : approver.partner_id.email,
                        'approver_name' : approver.name,
                        'date': date.today(),
                        'submitter' : self.env.user.name,
                        'url' : url,
                        "due_date": record.payment_date.strftime(DEFAULT_SERVER_DATE_FORMAT),
                        "currency": currency,
                    }
                    template_id.with_context(ctx).send_mail(record.id, True)
            else:
                approver = record.approved_matrix_ids[0].user_ids[0]
                ctx = {
                    'email_from' : self.env.user.company_id.email,
                    'email_to' : approver.partner_id.email,
                    'approver_name' : approver.name,
                    'date': date.today(),
                    'submitter' : self.env.user.name,
                    'url' : url,
                    "due_date": record.payment_date.strftime(DEFAULT_SERVER_DATE_FORMAT),
                    "currency": currency,
                }
                template_id.with_context(ctx).send_mail(record.id, True)
            record.write({'state': 'to_approve'})

    def _get_approve_button(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved),
                                 key=lambda r: r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False

    def _get_approve_button_from_config(self):
        for record in self:
            is_customer_deposit_approval_matrix = self.env['ir.config_parameter'].sudo().get_param(
                'is_customer_deposit_approval_matrix', False)
            record.is_customer_deposit_approval_matrix = is_customer_deposit_approval_matrix

    @api.onchange('journal_id')
    def currency(self):
        self._get_approve_button_from_config()
        for rec in self:
            if rec.journal_id:
                rec.currency_id = rec.journal_id.currency_id

    @api.onchange('name')
    def onchange_name(self):
        self._compute_deposit_account_ids()

    def _compute_deposit_account_ids(self):
        payable_type_id = self.env.ref('account.data_account_type_payable')
        account_ids = self.env['account.account'].search([('user_type_id', '=', payable_type_id.id)]).ids
        for record in self:
            record.filter_deposit_account_ids = [(6, 0, account_ids)]

    @api.depends('amount', 'remaining_amount')
    def _compute_show_cancel(self):
        for record in self:
            if record.amount == record.remaining_amount:
                record.is_show_cancel = True
            else:
                record.is_show_cancel = False

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('customer.deposit')
        return super(CustomerDeposit, self).create(vals)

    def customer_deposit_post(self):

        for record in self:
            record.write({'state': 'post'})
            ref = 'Customer Deposit ' + (record.name or '')
            name = 'Customer Deposit ' + (record.name or '')
            currency_rate = self.env['res.currency'].search([('id', '=', record.currency_id.id)], limit=1).rate
            debit_vals = {
                'debit': abs(record.amount / currency_rate) if record.currency_id else abs(record.amount),
                'date': record.payment_date,
                'name': name,
                'credit': 0.0,
                'account_id': record.journal_id.payment_debit_account_id.id,
            }
            credit_vals = {
                'debit': 0.0,
                'date': record.payment_date,
                'name': name,
                'credit': abs(record.amount / currency_rate) if record.currency_id else abs(record.amount),
                'account_id': record.deposit_account_id.id,
            }
            vals = {
                'ref': ref,
                'date': record.payment_date,
                'journal_id': record.journal_id.id,
                'line_ids': [(0, 0, debit_vals), (0, 0, credit_vals)]
            }
            move_id = self.env['account.move'].create(vals)
            move_id.post()
            record.deposit_move_id = move_id.id
            record.remaining_amount = record.amount

    def convert_revenue(self):
        context = dict(self.env.context) or {}
        context.update({'default_deposit_type': 'customer_deposit'})
        return {
            'name': 'Customer Deposit',
            'type': 'ir.actions.act_window',
            'res_model': 'convert.revenue',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'context': context,
        }

    def button_cancel_customer_deposit(self):
        for record in self:
            if record.amount != record.remaining_amount:
                raise ValidationError(
                    _("Cannot cancel Deposit! There are already transaction reconcile with this deposit."))
            else:
                record.deposit_move_id.button_draft()
                record.deposit_move_id.button_cancel()
                record.write({'state': 'cancelled'})

    def button_draft_customer_deposit(self):
        for payment in self:
            payment.write({'state': 'draft'})
        return True

    @api.constrains('amount')
    def _check_values_of_amount(self):
        if self.amount == 0:
            raise ValidationError("Deposit amount should bigger than 0!")

    @api.depends('invoice_deposit_ids')
    def _compute_invoice(self):
        for rec in self:
            rec.invoice_count = len(rec.invoice_deposit_ids)

    @api.depends('reconcile_deposit_ids')
    def _compute_reconcile(self):
        for rec in self:
            rec.reconcile_count = len(rec.reconcile_deposit_ids)

    def action_view_invoice(self):
        return {
            'name': _('Invoices'),
            'type': 'ir.actions.act_window',
            'res_model': 'account.move',
            'view_mode': 'tree,form',
            'views': [(self.env.ref('account.view_out_invoice_tree').id, 'tree'), (False, 'form')],
            'context': {'default_move_type': 'out_invoice'},
            'target': 'current',
            'domain': [('move_type', '=', 'out_invoice'), ('id', 'in', self.invoice_deposit_ids.ids)]
        }

    def action_view_reconcile(self):
        return {
            'name': _('Reconciles'),
            'type': 'ir.actions.act_window',
            'res_model': 'account.move',
            'view_mode': 'tree,form',
            'views': [(self.env.ref('account.view_move_tree').id, 'tree'), (False, 'form')],
            'context': {'default_move_type': 'entry', 'search_default_misc_filter': 1, 'view_no_maturity': True},
            'target': 'current',
            'domain': [('id', 'in', self.reconcile_deposit_ids.ids)]
        }


class AccountMove(models.Model):
    _inherit = 'account.move'

    customer_deposit_ids = fields.Many2many('customer.deposit', 'customer_deposit_invoice_rel', 'invoice_id',
                                            'deposit_id', string="Customer Deposit")
    vendor_deposit_ids = fields.Many2many('vendor.deposit', 'vendor_deposit_invoice_rel', 'invoice_id', 'deposit_id',
                                          string="Vendor Deposit")


class ApprovalMatrixAccountingLines(models.Model):
    _inherit = "approval.matrix.accounting.lines"

    customer_deposit_id = fields.Many2one('customer.deposit', string='Account Customer Deposit')
