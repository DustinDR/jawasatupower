
{
    'name': 'Accounting Deposit',
    'version': '1.1.11',
    'author': 'Hashmicro / Prince',
    'category' : 'Accounting',
    'depends': [
        'account',
        'analytic',
        'branch',
        'equip3_accounting_masterdata',
    ],
    'data': [
       'data/ir_sequence_data.xml',
       'data/customer_deposit_template.xml',
       'data/vendor_deposit_template.xml',
       'security/ir.model.access.csv',
       'wizard/convert_to_revenue_view.xml',
       'wizard/return_deposit_wizard_view.xml',
       'wizard/reconcile_deposit_wizard_view.xml',
       "wizard/reconcile_vendor_deposit_wizard_view.xml",
       'wizard/customer_deposit_reject.xml',
       'wizard/vendor_deposit_wizard.xml',
       'views/customer_deposit_views.xml',
       'views/vendor_deposit_views.xml',  
    ],
    'demo': [],
    'auto_install': False,
    'installable': True,
    'application': True
}