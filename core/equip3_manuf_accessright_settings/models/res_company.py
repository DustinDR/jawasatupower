from odoo import models, fields


class ResCompany(models.Model):
	_name = 'res.company'
	_inherit = 'res.company'

	sales_to_manufacturing = fields.Boolean('Sales To Manufacturing')
	manufacturing_plan_conf = fields.Boolean('Manufacturing Plan', default=True)
	manufacturing_order_conf = fields.Boolean('Manufacturing Order', default=True)
	mrp_plan_partial_availability = fields.Boolean('MP Reserve Materials Partially', default=False)
	production_record_conf = fields.Boolean('Production Record', default=True, store=True)
	mrp_production_partial_availability = fields.Boolean('Manufacturing Orders Reserve Materials Partially', default=False)

	manufacturing_mps = fields.Boolean(string='Master Production Schedule')
	manufacturing_period = fields.Selection([
		('month', 'Monthly'),
		('week', 'Weekly'),
		('day', 'Daily')], string="Manufacturing Period",
		default='month', required=True,
		help="Default value for the time ranges in Master Production Schedule report.")

	manufacturing_period_to_display = fields.Integer('Number of columns for the given period to display in Master Production Schedule', default=12)

	def write(self, vals):
		res = super(ResCompany, self).write(vals)
		manufacturing_menu = self.env.ref("mrp.menu_mrp_root")
		for company in self:
			if not company == self.env.company:
				continue
			manufacturing_menu.active = company.manufacturing
		return res
