from odoo import models, fields, api, _
from collections import defaultdict
import logging

_logger = logging.getLogger(__name__)

class GroupsView(models.Model):
    _inherit = 'res.groups'

    def get_application_groups(self, domain):
        manuf_domain = [('category_id', 'not in', [
            self.env.ref('base.module_category_manufacturing_manufacturing').id,
            self.env.ref('equip3_manuf_accessright_settings.module_category_manufacturing_equip3').id
        ])]
        if not self.env.company.manufacturing:
            domain += manuf_domain
        return super(GroupsView, self).get_application_groups(domain)
