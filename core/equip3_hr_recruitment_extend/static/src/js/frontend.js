$(document).ready(function () { 


	$('.table_past_experience_table_add_line').click(function(){
		var line = '<tr class="input_data">'
                +'<td style="padding: unset;"><input name="start_date_pe" readonly="1" class="form-control usedatepicker" type="text" style="padding: unset;padding-left: 5px;font-size: 14px;"></td>'
                +'<td style="padding: unset;"><input name="end_date_pe" readonly="1" class="form-control usedatepicker" type="text" style="padding: unset;padding-left: 5px;font-size: 14px;"></td>'
                +'<td style="padding: unset;" class="text-center"><input name="is_currently_work_here_pe" readonly="1"  type="checkbox" style="padding: unset;margin-top: 10px;font-size: 14px;"></td>'
                +'<td style="padding: unset;"><input name="company_name_pe" class="form-control" type="text" style="font-size: 14px;"></td>'
                +'<td style="padding: unset;"><input name="position_pe" class="form-control" type="text" style="font-size: 14px;"></td>'
                +'<td style="padding: unset;"><input name="reason_pe" class="form-control" type="text" style="font-size: 14px;"></td>'
                +'<td style="padding: unset;"><input name="salary_pe" class="form-control" type="number" style="font-size: 14px;"></td>'
                +'<td style="padding: unset;"><input name="company_phone_pe" class="form-control" type="text" style="font-size: 14px;"></td>'
                +'<td style="padding: unset;" class="text-center"><i class="fa fa-trash" style="cursor:pointer;"></i></td>'
            +'</tr>';

        $(line).insertBefore( '.table_past_experience_table_add_line');
        $('input[name="is_currently_work_here_pe"]').click(function(){
			if($(this).is(":checked")){
				$($($(this).parents()[1]).find('input[name="end_date_pe"]')).val('')
				$($($(this).parents()[1]).find('input[name="end_date_pe"]')).datepicker("destroy");
				$($($(this).parents()[1]).find('input[name="end_date_pe"]')).removeClass('usedatepicker')
			}
			else{
				$($($(this).parents()[1]).find('input[name="end_date_pe"]')).datepicker({ 
		            dateFormat:'dd/mm/yy',changeMonth: true,changeYear: true,
		             allowInputToggle: true 
		        }); 
			}
		});
        $('.usedatepicker').datepicker({ 
            dateFormat:'dd/mm/yy',changeMonth: true,changeYear: true,
             allowInputToggle: true 
        }); 
        $('#table_past_experience_table .fa-trash').click(function(){
        	$($(this).parents()[1]).remove()
        });

	});

	


	setTimeout(function(){ 
	           

	$('input[name="have_exp"]').click(function(){
		if($('input[name="have_exp"]').is(":checked")){
			$('.table_exp_question').hide()
		}
		else{
			$('.table_exp_question').show()
		}
	});
}, 2000);



	var pathname = window.location.pathname
	if(pathname.includes("/jobs")||pathname.includes("/job-thank-you")){
		if($('#gotojoinposition').length > 0){
			$('header nav').css("position", "absolute");
			$('header nav').css("width", "100%");
		}
		// $('#top_menu_container img').css("filter", "brightness(0) invert(1)");
		$('header nav').css("background-color", "white");
		$('header nav').css("padding", "20px 0");
		
		$('header').css("background", "transparent");
		$('ul#top_menu li').remove();
		$('#oe_structure_header_default_1').remove();
		$('#top_menu_container a').attr('href','/jobs')
		// $('#top_menu_container img').attr('src','/equip3_hr_recruitment_extend/static/src/img/logowhite.png')
		if ($( window ).width() > 865){
			$(".slick_testimoni_jobs").slick({
		        dots: true,
		        infinite: true,
		        centerMode: true,
		        slidesToShow: 1,
		        centerPadding:'200px',
		        slidesToScroll: 3
		      });
		}
		else if ($( window ).width() > 460){
			$(".slick_testimoni_jobs").slick({
		        dots: true,
		        infinite: true,
		        centerMode: true,
		        slidesToShow: 1,
		        centerPadding:'40px',
		        slidesToScroll: 3
		      });
		}
		else{
			$(".slick_testimoni_jobs").slick({
		        dots: true,
		        infinite: true,
		        centerMode: true,
		        slidesToShow: 1,
		        centerPadding:'30px',
		        slidesToScroll: 3
		      });
		}

		$('.job_header_flex select').select2({
		});


		$('#job_view_all_role').click(function(event) {
			window.location.href= '/jobs?all=1'+'#gotojoinposition'
		})		

		if(window.location.href.includes("/jobs?all=1#gotojoinposition")) {
	        setTimeout(function(){ 
	            $('.bottom_job_see_more').click()
	         }, 3000);
		}

		$('.bottom_job_see_more').click(function(event) {
			$('.col-box-job-recruit').removeClass('d-none');
			$($('.bottom_job_see_more').parent()).hide();
		})	

		$('.header1jobs .job_header1_flex_button').click(function(event) {
			var urllink = '/jobs'
			var location_id = $('.header1jobs .div_job_select_location select').val()
			var job_name = $('.header1jobs .div_job_select_role input').val()

			if (location_id!='0') {
				urllink+='/office/'+location_id
			}
			if (job_name!='') {
				urllink+='?jobname='+job_name
			}
			window.location.href= urllink+'#gotojoinposition'
		});
		$('.header3jobs .job_header1_flex_button').click(function(event) {
			var urllink = '/jobs'
			var location_id = $('.header3jobs .div_job_select_location select').val()
			var job_name = $('.header3jobs .div_job_select_role input').val()
			var departement_id = $('.header3jobs .div_job_select_departement select').val()
			if (departement_id!='0') {
				urllink+='/department/'+departement_id
			}

			if (location_id!='0') {
				urllink+='/office/'+location_id
			}
			if (job_name!='') {
				urllink+='?jobname='+job_name
			}
			window.location.href= urllink+'#gotojoinposition'

		});
	}



	odoo.define('mccoy_website.website_json', function(require) {
	    "use strict";
	    var ajax = require('web.ajax');

	    $('.table_emp_skill_table_add_line').click(function(){
	    	var $table = $(this)
			var selectskilltype = '<select name="skill_type_id_pe" class="selectskilltype form-control"><option value="0"></option>'
			ajax.jsonRpc("/get-skill-type", 'call', {

			}).then(function(result) {
				for (var i = 0; i < result.length; i++) {
                        selectskilltype +='<option value='+result[i]['id']+'>'+result[i]['name']+'</option>';
                    }
                selectskilltype+='</select>'
                var line = '<tr class="input_data">'
		                +'<td  style="padding: unset;">'+selectskilltype+'</td>'
		                +'<td id="selectskillonly" style="padding: unset;"></td>'
		                +'<td id="selectskilllevel" style="padding: unset;"></td>'
		                +'<td style="padding: unset;" class="text-center"><i class="fa fa-trash" style="cursor:pointer;"></i></td>'
		            +'</tr>';

		        $(line).insertBefore( '.table_emp_skill_table_add_line');
		        $( "select.selectskilltype" ).each(function( i ) {
		        	if($(this).css('display')!='none'){
		        		$(this).select2();
		        	}
		        		
				 });
		        $('#table_emp_skill_table .fa-trash').click(function(){
		        	$($(this).parents()[1]).remove()
		        });

		        $( "select.selectskilltype" ).change(function(){
		        	var $this = $(this)
		        	var idskilltype= $(this).val()
		        	if(idskilltype && idskilltype!='0' && idskilltype!=0){
		        		var selectskill = '<select name="skill_id_pe" class="selectskillonly form-control"><option value="0"></option>'
		        		var selectlevel = '<select name="skill_level_id_pe" class="selectskilllevel form-control"><option value="0"></option>'
						ajax.jsonRpc("/get-skill-other-list", 'call', {
							'skill_type_id':parseInt(idskilltype),
						}).then(function(result) {
							for (var i = 0; i < result['skill'].length; i++) {
		                        selectskill +='<option value='+result['skill'][i]['id']+'>'+result['skill'][i]['name']+'</option>';
		                    }
		                    for (var i = 0; i < result['level'].length; i++) {
		                        selectlevel +='<option value='+result['level'][i]['id']+'>'+result['level'][i]['name']+'</option>';
		                    }
		                    selectskill+='</select>'
		                    selectlevel+='</select>'
		                    var $tr = $($this.parents()[1])
		                    var checkfield = $tr.find('#selectskillonly')
		                    var checkfield1 = $tr.find('#selectskilllevel')
		                    if (checkfield.length>0){
		                    	$(checkfield).empty()
		                    }
		                    if (checkfield1.length>0){
		                    	$(checkfield1).empty()
		                    }

		                    $(checkfield).append(selectskill);
		                    $(checkfield1).append(selectlevel);
		                    $( "select.selectskillonly" ).each(function( i ) {
					        	if($(this).css('display')!='none'){
					        		$(this).select2();
					        	}
					        		
							 });
		                    $( "select.selectskilllevel" ).each(function( i ) {
					        	if($(this).css('display')!='none'){
					        		$(this).select2();
					        	}
					        		
							 });
						});
		        	}
		        });

			});

		});

	    



	});
})