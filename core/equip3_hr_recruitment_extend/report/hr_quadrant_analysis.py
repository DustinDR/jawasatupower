# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import tools
from odoo import api, fields, models


class HRQuadrantAnalysisReport(models.Model):
    _name = "hr.quadrant.analysis.report"
    _auto = False

    applicant_id = fields.Char("Applicant's ID")
    applicant_name = fields.Char("Applicant's Name")
    applicant_email = fields.Char("Email")
    job_id = fields.Many2one("hr.job", string="Applied Job")
    category_id = fields.Many2one("quadrant.category", string="Category")
    
    def _select(self):
        select_str = """
            min(qs.id) as id,
            qs.applicant_id,
            qs.applicant_name,
            qs.applicant_email,
            qs.job_id,
            qc.id as category_id"""
        return select_str

    def _from(self):
        from_str = """
                quadrant_score qs
                join quadrant_category qc on qc.id=qs.category_id
             """
        return from_str

    def _group_by(self):
        group_by_str = """group by qs.id,qs.applicant_id,qs.applicant_name,qs.applicant_email,qs.job_id,qc.id"""
        return group_by_str

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as ( SELECT
                   %s
                   FROM %s
                   %s
                   )""" % (self._table, self._select(), self._from(), self._group_by()))

