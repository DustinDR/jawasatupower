from odoo import fields, models, api


class HrManPlanWizard(models.TransientModel):
    _name = 'hr.man.plan.wizard'

    feedback = fields.Text()

    def submit(self):
        """ Prepare the Loan feedback and trigger Approve. """
        self.ensure_one()
        man_plan = self.env['manpower.planning'].browse(self._context.get('active_ids', []))
        man_plan.feedback_parent = self.feedback
        man_plan.action_approve()
