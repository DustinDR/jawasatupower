# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class Equip3HrRecruitmentExtendResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    is_sms = fields.Boolean(string="SMS", config_parameter='equip3_hr_recruitment_extend.is_sms')
    is_whatsapp = fields.Boolean(string="WA", config_parameter='equip3_hr_recruitment_extend.is_whatsapp')
    mpp = fields.Boolean(string="MPP", config_parameter='equip3_hr_recruitment_extend.mpp')
    man_power_type_approval = fields.Selection(
        [('employee_hierarchy', 'By Employee Hierarchy'), ('approval_matrix', 'By Approval Matrix')],
        config_parameter='equip3_hr_recruitment_extend.man_power_type_approval', default='employee_hierarchy')
    man_power_level = fields.Integer(config_parameter='equip3_hr_recruitment_extend.man_power_level', default=2)
    connector_id = fields.Many2one('acrux.chat.connector',config_parameter='equip3_hr_recruitment_extend.connector_id')
    send_by_wa = fields.Boolean(config_parameter='equip3_hr_recruitment_extend.send_by_wa')
    send_by_email_recruitment = fields.Boolean(config_parameter='equip3_hr_recruitment.send_by_email')
    

    @api.onchange("man_power_level")
    def _onchange_man_power_level(self):
        if self.man_power_level < 1:
            self.man_power_level = 1
