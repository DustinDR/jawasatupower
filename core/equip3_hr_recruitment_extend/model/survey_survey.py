import time
import werkzeug
from odoo import fields, models, api
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
import requests

headers = {'content-type': 'application/json'}
class hashMicroInheritSurveySurvey(models.Model):
    _inherit = 'survey.survey'
    is_application_form = fields.Boolean()
    is_follow_up = fields.Boolean(default=True)
    email_template_id = fields.Many2one('mail.template')
    wa_template_id = fields.Many2one('wa.template.message')
    interval_number = fields.Integer()
    interval_type = fields.Selection([('minutes','Minutes'),('hours','Hours'),('days','Days'),('weeks','Weeks'),('months','Months')])
    number_of_repetion = fields.Integer()
    # refuse_reason = fields.Many2one('hr.applicant.refuse.reason', 'Refuse Reason')
    
    
    
    @api.model
    def default_get(self, fields):
        res = super(hashMicroInheritSurveySurvey, self).default_get(fields)
        template_email =self.env.ref('equip3_hr_recruitment_extend.mail_template_reminder_invite_test').id
        template_wa =self.env.ref('equip3_hr_recruitment_extend.wa_template_2').id
        res.update({'email_template_id': template_email,
                    'wa_template_id':template_wa,
                    'interval_type':'hours',
                    'interval_number':1,
                    'number_of_repetion':1
                    })
        return res
    
    @api.onchange('number_of_repetion')
    def _onchange_number_of_repetion(self):
        for record in self:
            if record.number_of_repetion:
                if record.number_of_repetion < 0:
                    raise ValidationError("Number of Repetitions cannot less than 0")
                
    @api.onchange('interval_number')
    def _onchange_number_of_interval_number(self):
        for record in self:
            if record.interval_number:
                if record.interval_number < 0:
                    raise ValidationError("Interval Number cannot less than 0")
    
    
    def send_email_and_wa(self,email_from,partner_name,partner_mobile,applicant_id,job_id,survey_id):
        template = self.email_template_id
        survey=self.env['survey.invite'].create({'survey_id':self.id,'emails':str(email_from),'template_id':template.id})
        send_by_email = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment.send_by_email')
        context = self.env.context = dict(self.env.context)
        survey_url = survey.survey_start_url+f"?surveyId={self.id}&applicantId={applicant_id}&jobPosition={job_id}"
        survey_latest = self.env['survey.user_input'].search([('applicant_id','=',applicant_id),('survey_id','=',survey_id)],order='id desc',limit=1)
        if not survey_latest:
            if send_by_email:
                context.update({
                    'email_to': email_from,
                    'name': partner_name,
                    'url_test': survey_url,
                    'title':self.title
                })
                template.send_mail(applicant_id, force_send=True)
                template.with_context(context)
            send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment_extend.send_by_wa')
            if send_by_wa:
                # connector = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment_extend.connector_id')
                # if connector:
                #     connector_id = self.env['acrux.chat.connector'].search([('id','=',connector)])
                #     if connector_id.ca_status:
                template = self.wa_template_id
                if template:
                    string_test = str(template.message)
                    if partner_name:
                        if "${applicant_name}" in string_test:
                            string_test=string_test.replace("${applicant_name}",partner_name)
                    if "${survey_url}" in string_test:
                        string_test = string_test.replace("${survey_url}",survey_url)
                    if "${br}" in string_test:
                        string_test = string_test.replace("${br}",f"\n")
                    if partner_mobile:
                        phone_num = str(partner_mobile)
                        if "+" in phone_num:
                            phone_num =  int(phone_num.replace("+",""))
                        param = {'body': string_test, 'phone': phone_num}
                        domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                        token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                        try:
                            request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                        except ConnectionError:
                            raise ValidationError("Not connect to API Chat Server. Limit reached or not active")
                        # connector_id.sudo().ca_request('post','sendMessage',param)
    
    @api.model
    def _cron_auto_follow(self):
        job_stage_line = self.env['job.stage.line'].sudo().search([('survey_id','=',self.id)])
        if job_stage_line:
            for data_stage in job_stage_line:
                applicant_list = self.env['hr.applicant'].sudo().search([('shadow_stage_replace_id','=',data_stage.id),('job_id','=',data_stage.job_id.id)])
                if applicant_list:
                    for data_applicant in applicant_list:
                            if not data_applicant.is_auto_follow:
                                count = self.number_of_repetion - 1
                                query_statement = """UPDATE hr_applicant set  is_auto_follow  = TRUE, repetion_count = %s WHERE id  = %s """
                                self.env.cr.execute(query_statement, [count,data_applicant.id])
                                email_from = data_applicant.email_from if data_applicant.email_from else False
                                partner_name = data_applicant.partner_name if data_applicant.partner_name else False
                                partner_mobile = data_applicant.partner_mobile if data_applicant.partner_mobile else False
                                applicant = data_applicant.id if data_applicant.id else False
                                job = data_applicant.job_id.id if data_applicant.job_id.id else False
                                
                                self.send_email_and_wa(email_from,partner_name,partner_mobile,applicant,job,self.id)
                            
                            elif data_applicant.is_auto_follow:
                                if data_applicant.repetion_count > 0:
                                    count = data_applicant.repetion_count -1
                                    query_statement = """UPDATE hr_applicant set repetion_count = %s WHERE id  = %s """
                                    self.sudo().env.cr.execute(query_statement, [count,data_applicant.id])
                                    self.send_email_and_wa(data_applicant.email_from,data_applicant.partner_name,data_applicant.partner_mobile,data_applicant.id,data_applicant.job_id.id,self.id)
                           
                            
                    
    
    def unlink(self):
        for record in self:
            cron_to_delete = self.env['ir.cron'].search([('survey_id','=',record.id)])
            if cron_to_delete:
                cron_to_delete.unlink()
        res = super(hashMicroInheritSurveySurvey, self).unlink()
        return res
    
    @api.model
    def create(self, vals_list):
        res =  super(hashMicroInheritSurveySurvey,self).create(vals_list)
        if res.is_follow_up:
                interval = res.interval_number
                interval_type = res.interval_type
                delta_var = res.interval_type
                delta_var = 'month' if delta_var == 'months' else delta_var
                next_call = datetime.now() + eval(f'timedelta({delta_var}={interval})')
                model = self.env['ir.model'].search([('model','=',self._name)])
                user_bot = self.env['res.users'].search([('id','=',1)])
                if model and user_bot:
                    cron = self.env['ir.cron'].create({'name':f"Cron Auto Follow {self.title}",
                                                    'model_id':model.id,
                                                    'user_id':user_bot.id,
                                                    'interval_number': interval,
                                                    'interval_type':interval_type,
                                                    'active':True,
                                                    'code':f"model.search([('id','=',{self.id})])._cron_auto_follow()",
                                                    'nextcall':next_call,
                                                    'numbercall':-1,
                                                    'survey_id':res.id
                                                    
                                                    })
        return res
    
    def write(self, vals):
        for rec in self:
            interval = vals['interval_number'] if 'interval_number' in vals else rec.interval_number
            interval_type = vals['interval_type'] if 'interval_type' in vals else rec.interval_type
            number_of_repetion =  vals['number_of_repetion'] if 'number_of_repetion' in vals else rec.number_of_repetion
            delta_var = vals['interval_type'] if 'interval_type' in vals else rec.interval_type
            delta_var = 'month' if delta_var == 'months' else delta_var
            if delta_var and interval:
                next_call = datetime.now() + eval(f'timedelta({delta_var}={interval})')
            if 'is_follow_up' in vals:
                if vals['is_follow_up']:
                    model = self.env['ir.model'].search([('model','=',self._name)])
                    if model:
                        cron = self.env['ir.cron'].create({'name':f"Cron Auto Follow {rec.title}",
                                                        'model_id':model.id,
                                                        'user_id':1,
                                                        'interval_number': interval,
                                                        'interval_type':interval_type,
                                                        'active':True,
                                                        'code':f"model.search([('id','=',{rec.id})])._cron_auto_follow()",
                                                        'nextcall':next_call,
                                                        'numbercall':-1,
                                                        'survey_id':rec.id

                                                        })
                if not vals['is_follow_up']:
                    cron_to_delete = self.env['ir.cron'].search([('survey_id','=',rec.id)])
                    if cron_to_delete:
                        cron_to_delete.unlink()

            if rec.is_follow_up:
                cron_to_update = self.env['ir.cron'].search([('survey_id','=',rec.id)])
                if cron_to_update:
                    cron_to_update.write({'interval_number':interval,'interval_type':interval_type,'nextcall':next_call})
        res =  super(hashMicroInheritSurveySurvey,self).write(vals)
        return res
    

