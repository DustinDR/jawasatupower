import werkzeug
from odoo import fields, models, api


class hashMicroInheritSurveyUserInput(models.Model):
    _inherit = 'survey.user_input'
    applicant_id = fields.Many2one('hr.applicant',"Applicant")
    applicant_name = fields.Char(related='applicant_id.partner_name',string="Applicant Name")
    job_id = fields.Many2one('hr.job','Job')
    is_use = fields.Boolean()