from odoo import models,api
from lxml import etree





class HashmicrohrApplicantRefuseReason(models.Model):
    _inherit = 'hr.applicant.refuse.reason'
    
    
    @api.model
    def fields_view_get(self, view_id=None, view_type=None,
                        toolbar=False, submenu=False):
        res = super(HashmicrohrApplicantRefuseReason, self).fields_view_get(
            view_id=view_id, view_type=view_type)
        if  not self.env.user.has_group('hr_recruitment.group_hr_recruitment_manager'):
            root = etree.fromstring(res['arch'])
            root.set('create', 'false')
            root.set('edit', 'false')
            root.set('delete', 'false')
            res['arch'] = etree.tostring(root)
        return res