from odoo import fields,models,api

class HrApplicantPastExperience(models.Model):
    _name = 'hr.applicant.past.experience'
    start_date = fields.Date()
    end_date = fields.Date()
    company_name = fields.Char()
    position = fields.Char()
    reason_for_leaving = fields.Char()
    salary = fields.Float()
    company_telephone_number = fields.Char()
    applicant_id = fields.Many2one('hr.applicant')
    is_currently_work_here = fields.Boolean("Currently Work Here")


