from odoo import api, fields, models, _

class SurveyUserInput(models.Model):
	""" Metadata for a set of one user's answers to a particular survey """
	_inherit = "survey.user_input"
	
	
	score_by_amount = fields.Float("Score",compute="_get_score_value")

	def _get_score_value(self):
		for record in self:
			total = 0
			if record.user_input_line_ids:
				for line in record.user_input_line_ids:
					total +=  line.answer_score
				record.score_by_amount = total
			else:
				record.score_by_amount = 0

	
