from odoo import _, api, fields, models

class equip3WaMessageTemplate(models.Model):
    _name = 'wa.template.message'
    _inherit = ['mail.thread','mail.activity.mixin']
    
    name = fields.Char()
    message = fields.Text()
    wa_variable_ids = fields.One2many('wa.template.message.variable','wa_id')
    
    
    
    
    @api.model
    def default_get(self, fields):
        res = super(equip3WaMessageTemplate, self).default_get(fields)
        line_list = []
        line_list.append((0,0,{'name':"${applicant_name}",
                               'description':"Applicant Name"
                               }))
        
        line_list.append((0,0,{'name':"${survey_url}",
                               'description':"Survey or Test URL"
                               }))
        line_list.append((0,0,{'name':"${br}",
                               'description':"Break Line"
                               }))
        res.update({'wa_variable_ids': line_list})
        return res
    
    @api.model
    def create(self, vals_list):
        res = super(equip3WaMessageTemplate,self).create(vals_list)
        line_list = []
        line_list.append((0,0,{'name':"${applicant_name}",
                               'description':"Applicant Name"
                               }))
        
        line_list.append((0,0,{'name':"${survey_url}",
                               'description':"Survey or Test URL"
                               }))
        line_list.append((0,0,{'name':"${br}",
                               'description':"Break Line"
                               }))
        if not res.wa_variable_ids:
            res.wa_variable_ids = line_list
        return res
    
    


class equip3WaMessageTemplateVariables(models.Model):
    _name = 'wa.template.message.variable'
    
    wa_id = fields.Many2one('wa.template.message')
    name = fields.Char()
    description = fields.Char()