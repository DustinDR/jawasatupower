from datetime import datetime
from odoo import api, fields,models
from odoo.exceptions import ValidationError


class equip3ManPowerPlanningPeriod(models.Model):
    _name = "manpower.planning.period"
    _inherit = ['mail.thread','mail.activity.mixin']
    
    name = fields.Char()
    mpp_year = fields.Many2one('hr.years',"Year")
    mpp_type = fields.Many2one('manpower.planning.type',"MPP Type")
    start_period = fields.Date()
    end_period = fields.Date()
    
    
    
    @api.model
    def create(self,values):
        res= super(equip3ManPowerPlanningPeriod, self).create(values)
        start_period=datetime.strptime(str(res.start_period),"%Y-%m-%d")
        end_period=datetime.strptime(str(res.end_period),"%Y-%m-%d")
        start_period_string=datetime(start_period.year,start_period.month,start_period.day)
        end_period_string=datetime(end_period.year,end_period.month,end_period.day)
        res.name = f"{start_period_string.strftime('%d %b %Y')} - {end_period_string.strftime('%d %b %Y')}"
        return res  
    
    
    