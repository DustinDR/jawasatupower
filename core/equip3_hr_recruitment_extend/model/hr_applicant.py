
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, SUPERUSER_ID,_
from odoo.exceptions import ValidationError
from lxml import etree
from odoo.http import request
import werkzeug,requests



headers = {'content-type': 'application/json'}
class HrApplicant(models.Model):
    _inherit='hr.applicant'

    applicant_question_answer=fields.One2many('applicant.answer','applicant_id')
    applicant_question_answer_spesific=fields.One2many('applicant.answer','applicant_specific_id')
    stage_replace_id = fields.Many2one('job.stage.line', 'Stage',compute='compute_stage_replace_id')
    shadow_stage_replace_id = fields.Many2one('job.stage.line', 'Stage')
    stage_id = fields.Many2one('hr.recruitment.stage', 'Stage',domain=[])
    stage_domain_ids = fields.Many2many('hr.recruitment.stage',compute='_compute_stage_domain_ids')
    stage_replace_domain_ids = fields.Many2many('job.stage.line',compute='_compute_stage_domain_ids')
    aplicant_create_date = fields.Date(default=datetime.now())
    file_cv = fields.Binary("CV")
    cv_name = fields.Char("CV", compute='_compute_file_name', default="", store=True)
    response_id = fields.Many2one('survey.user_input', "Response", ondelete="set null")
    previous_score = fields.Float()
    past_experience_ids = fields.One2many('hr.applicant.past.experience','applicant_id',string="Past Experience")
    wa_url  = fields.Char(compute='_get_wa_url')
    wa_url_second  = fields.Char(compute='_get_wa_url_second')
    card_id_number = fields.Char()
    applicant_id = fields.Char("Applicant's ID")
    identification_no = fields.Char('Identification No')
    is_hide_assign = fields.Boolean()
    is_hide_pass_stage = fields.Boolean()
    is_hide_wa = fields.Boolean(compute='_is_hide_wa')
    is_hide_pass_next = fields.Boolean(compute='_is_hide_pass_next',default=True)
    apply_pass_stage = fields.Many2many('res.users')
    gender = fields.Selection([('male','Male'),('female','Female')],string="Gender")
    date_of_birth = fields.Date(string="Date Of Birth")
    address = fields.Text()
    marital_status = fields.Many2one('employee.marital.status')
    religion = fields.Many2one('employee.religion')
    birth_years = fields.Integer(string="Years of Service", compute='compute_birth_year')
    birth_months = fields.Integer(compute='compute_birth_year')
    birth_days = fields.Integer(compute='compute_birth_year')
    birth_year = fields.Char(string=' ', default='-year(s) -')
    birth_month = fields.Char(string=' ', default='month(s) -')
    birth_day = fields.Char(string=' ', default='day(s)')
    age = fields.Integer('Age', compute='compute_birth_year', store=True)
    participations_count = fields.Integer(compute="_get_participations_count")
    last_drawn_salary = fields.Float()
    is_auto_follow =  fields.Boolean()
    repetion_count = fields.Integer()
    in_college = fields.Boolean()
    employee_skill_ids = fields.One2many("hr.applicant.skill",'applicant_id',"Employee Skill")
    is_interview = fields.Boolean()
    interview_id = fields.Many2one('survey.user_input')
    refuse_is_sent = fields.Boolean()
    
    # def action_print_interview(self):
    #     base_url = self.env['ir.config_parameter'].get_param('web.base.url')
    #     return {
    #                'name'     : 'Interview Result',
    #               'res_model': 'ir.actions.act_url',
    #               'type'     : 'ir.actions.act_url',
    #               'target'   : 'new',
    #               'url'      : f"{base_url}/open_interview_result/{self.interview_id.id}"
    #            }
    
    
    def action_makeMeeting(self):
        """ This opens Meeting's calendar view to schedule meeting on current applicant
            @return: Dictionary value for created Meeting view
        """
        self.ensure_one()
        partners = self.partner_id | self.user_id.partner_id | self.department_id.manager_id.user_id.partner_id
        attende_ids = []
        if self.user_id.partner_id:
            attende_ids.append(self.user_id.partner_id.id)
        if self.partner_id:
            attende_ids.append(self.partner_id.id)
        if not self.partner_id:
            new_partner_id = self.env['res.partner'].create({
                    'is_company': False,
                    'type': 'private',
                    'name': self.partner_name if self.partner_name else False,
                    'email': self.email_from if self.email_from else False,
                    'phone': self.partner_phone if self.partner_phone else False,
                    'mobile': self.partner_mobile if self.partner_mobile else False
                })
            self.partner_id = new_partner_id
            attende_ids.append(new_partner_id.id)
        
            

        category = self.env.ref('hr_recruitment.categ_meet_interview')
        res = self.env['ir.actions.act_window']._for_xml_id('calendar.action_calendar_event')
        res['context'] = {
            'default_partner_ids': [(6,0,attende_ids)],
            'default_applicant_id':self.id,
            'default_user_id': self.env.uid,
            'default_name': self.name,
            'default_categ_ids': category and [category.id] or False,
            # 'default_attendee_ids':attende_ids
        }
        
        return res

    def action_print_interview(self):
        return {
                    'res_id': self.interview_id.id,
                    'name': 'Interview Result',
                    'res_model': 'survey.user_input',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'type': 'ir.actions.act_window',
                    'target': 'current',
                    'context': "{'create': False, 'edit': False}",
                }
    
    
    
    def action_start_survey(self):
        self.ensure_one()
        return self.action_start_survey_interview(self.stage_replace_id.interview_id)
    
    
    def action_start_survey_interview(self,survey):
        """ Open the website page with the survey form """
        self.ensure_one()
        survey_url = survey.get_start_url()+f"?surveyId={survey.id}&applicantId={self.id}&jobPosition={self.job_id.id}"
        return {
            'type': 'ir.actions.act_url',
            'name': "Start Survey",
            'target': 'self',
            'url': survey_url,
        }
    
    
    
    
    def re_do_current_test(self):
        for record in self:
            template = self.env.ref('survey.mail_template_user_input_invite', raise_if_not_found=False)
            survey_latest = self.env['survey.user_input'].search([('applicant_id','=',record.id)],order='id desc',limit=1)
            if survey_latest:
                survey_latest.is_use = False
                survey=self.env['survey.invite'].create({'survey_id':survey_latest.survey_id.id,'emails':str(self.email_from),'template_id':template.id})
                send_by_email = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment.send_by_email')
                context = self.env.context = dict(self.env.context)
                survey_url = survey.survey_start_url+f"?surveyId={survey_latest.survey_id.id}&applicantId={record.id}&jobPosition={record.job_id.id}"
                if send_by_email:
                    context.update({
                        'email_to': record.email_from,
                        'name': record.partner_name,
                        'url_test': survey_url,
                        'title':survey_latest.survey_id.title
                    })
                    template = self.env.ref('equip3_hr_recruitment_extend.mail_template_invite_test')
                    template.send_mail(record.id, force_send=True)
                    template.with_context(context)
                send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment_extend.send_by_wa')
                if send_by_wa:
                    # connector = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment_extend.connector_id')
                    # if connector:
                    #     connector_id = self.env['acrux.chat.connector'].search([('id','=',connector)])
                    #     if connector_id.ca_status:
                    template = self.env.ref('equip3_hr_recruitment_extend.wa_template_1')
                    if template:
                        string_test = str(template.message)
                        if "${applicant_name}" in string_test:
                            string_test=string_test.replace("${applicant_name}",self.partner_name)
                        if "${survey_url}" in string_test:
                            string_test = string_test.replace("${survey_url}",survey_url)
                        if "${br}" in string_test:
                            string_test = string_test.replace("${br}",f"\n")
                        if self.partner_mobile:
                            phone_num = str(self.partner_mobile)
                            if "+" in phone_num:
                                phone_num =  phone_num.replace("+","")
                            param = {'body': string_test, 'phone': int(phone_num)}
                            domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                            token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                            try:
                                request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                            except ConnectionError:
                                raise ValidationError("Not connect to API Chat Server. Limit reached or not active")
                                   
            
    
    
    
    
    def open_new_tab_record(self):
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        return {
                   'name'     : 'Applicant',
                  'res_model': 'ir.actions.act_url',
                  'type'     : 'ir.actions.act_url',
                  'target'   : '_blank',
                  'url'      : f"{base_url}/open_new_applicant/{self.env.context.get('active_id')}"
               }
    
    
    @api.depends('date_of_birth')
    def compute_birth_year(self):
        for record in self:
            if record.date_of_birth:
                current_day = date.today()
                d1 = record.date_of_birth
                d2 = current_day
                record.age = ((d2 - d1).days) / 365
                record.birth_years = ((d2 - d1).days) / 365
                d3 = record.date_of_birth + relativedelta(years=+record.birth_years)
                record.birth_months = ((d2 - d3).days) / 30
                d4 = d3 + relativedelta(months=+record.birth_months)
                record.birth_days = ((d2 - d4).days)
            else:
                record.age = 0
                record.birth_years = 0
                record.birth_months = 0
                record.birth_days = 0
    
    
    
    
    def _is_hide_pass_next(self):
        if self.env.user.has_group('equip3_hr_accessright_settings.equip3_group_hr_recruitment_user') and not self.env.user.has_group('hr_recruitment.group_hr_recruitment_user'):
            self.is_hide_pass_next = False
            if self.env.user.id in self.apply_pass_stage.ids:
                self.is_hide_pass_next = True
        elif self.env.user.has_group('hr_recruitment.group_hr_recruitment_user') :
            self.is_hide_pass_next = True
        else:
            self.is_hide_pass_next = True
            
    
    
    
    
    @api.depends('email_from')
    def _compute_application_count(self):
        count = 0
        ids =[]
        if self.email_from:
            email_from = self.search([('email_from','in',self.mapped('email_from')),('id','!=',self._origin.id )])
            for data in email_from:
                ids.append(data.id)
        if self.identification_no:
            identification_no = self.search([('identification_no','in',self.mapped('identification_no')),('id','!=',self._origin.id )])
            for data in identification_no:
                ids.append(data.id)
        if self.partner_phone:
            partner_phone = self.search([('partner_phone','in',self.mapped('partner_phone')),('id','!=',self._origin.id )])
            for data in partner_phone:
                ids.append(data.id)
        if self.partner_mobile:
            partner_mobile = self.search([('partner_mobile','in',self.mapped('partner_mobile')),('id','!=',self._origin.id)])
            for data in partner_mobile:
                ids.append(data.id)
        self.application_count = len(set(ids)) 
                
    def get_participations(self):
        return {
            'type': 'ir.actions.act_window',
            'name': _('Participations'),
            'res_model': 'survey.user_input',
            'view_mode': 'tree,form',
            'domain': [('applicant_id','=',self.id),('survey_type','!=','INTERVIEW')],
            'context':{'search_default_group_by_survey': True}
          
        }
    
    def _get_participations_count(self):
        for record in self:
            count =0
            survey_user_input = self.env['survey.user_input'].search([('applicant_id','=',record.id),('survey_type','!=','INTERVIEW')])
            if survey_user_input:
                for data in survey_user_input:
                    count+=1
            record.participations_count = count
        
            
            
    
    
    def action_applications_email(self):
        ids = []
        if self.email_from:
            email_from = self.search([('email_from','in',self.mapped('email_from')),('id','!=',self.id )])
            for data in email_from:
                ids.append(data.id)
        if self.identification_no:
            identification_no = self.search([('identification_no','in',self.mapped('identification_no')),('id','!=',self.id )])
            for data in identification_no:
                ids.append(data.id)
        if self.partner_phone:
            partner_phone = self.search([('partner_phone','in',self.mapped('partner_phone')),('id','!=',self.id )])
            for data in partner_phone:
                ids.append(data.id)
        if self.partner_mobile:
            partner_mobile = self.search([('partner_mobile','in',self.mapped('partner_mobile')),('id','!=',self.id)])
            for data in partner_mobile:
                ids.append(data.id)
        
        return {
            'type': 'ir.actions.act_window',
            'name': _('Applications'),
            'res_model': self._name,
            'view_mode': 'kanban,tree,form,pivot,graph,calendar,activity',
            'domain': [('id', 'in', ids)],
            'context': {
                'active_test': False
            },
        }
    
    def pass_to_next_stage(self):
        for record in self:
            if record.job_id:
                line = record.job_id.stage_ids.filtered(lambda line:line.sequence == record.stage_replace_id.sequence + 1)
                if line:
                    record.stage_id = line[0].stage_id.id
                    record.apply_pass_stage = [(4,self.env.user.id)]
                else:
                    raise ValidationError("Stage not found")
                        
    
    
    def get_all_menu(self):
        if  not self.env.user.has_group('hr_recruitment.group_hr_recruitment_user'):
            job_id = self.env['hr.job'].search([])
            search_view_id= self.env.ref('hr_recruitment.hr_applicant_view_search_bis')
            data_job = []
            for record in job_id:
                data_stage = [data.stage_id.id for data in record.stage_ids.filtered(lambda line: self.env.user.id in line.user_ids.ids )]
                data_job.extend(data_stage)
            return {
            'type': 'ir.actions.act_window',
            'name': 'Applications',
            'res_model': 'hr.applicant',
            # 'view_type': 'kanban',
            'search_view_id':search_view_id.id,
            'view_mode': 'kanban,tree,form,graph,calendar,pivot,activity',
            'context':{},
            'domain': [('job_id.real_second_user_ids','in',self.env.user.id),('stage_id','in',data_job)]
            }
        elif self.env.user.has_group('hr_recruitment.group_hr_recruitment_user') and not self.env.user.has_group('equip3_hr_accessright_settings.equip3_group_hr_recruitment_manager'):
            search_view_id= self.env.ref('hr_recruitment.hr_applicant_view_search_bis')
            return {
            'type': 'ir.actions.act_window',
            'name': 'Applications',
            'res_model': 'hr.applicant',
            # 'view_type': 'kanban',
            'search_view_id':search_view_id.id,
            'view_mode': 'kanban,tree,form,graph,calendar,pivot,activity',
            'context':{},
            'domain': [('job_id.user_ids','in',self.env.user.id)]
            }
        elif  self.env.user.has_group('equip3_hr_accessright_settings.equip3_group_hr_recruitment_manager') and not self.env.user.has_group('hr_recruitment.group_hr_recruitment_manager'):
            search_view_id= self.env.ref('hr_recruitment.hr_applicant_view_search_bis')
            return {
            'type': 'ir.actions.act_window',
            'name': 'Applications',
            'res_model': 'hr.applicant',
            # 'view_type': 'kanban',
            'search_view_id':search_view_id.id,
            'view_mode': 'kanban,tree,form,graph,calendar,pivot,activity',
            'context':{},
            'domain': [('job_id.user_ids','in',self.env.user.id)]
            }
        elif self.env.user.has_group('hr_recruitment.group_hr_recruitment_manager'):
            search_view_id= self.env.ref('hr_recruitment.hr_applicant_view_search_bis')
            return {
            'type': 'ir.actions.act_window',
            'name': 'Applications',
            'res_model': 'hr.applicant',
            # 'view_type': 'kanban',
            'search_view_id':search_view_id.id,
            'view_mode': 'kanban,tree,form,graph,calendar,pivot,activity',
            'context':{}
            }
        
    def get_report_menu(self):
        search_view_id= self.env.ref('hr_recruitment.hr_applicant_view_search')
        views = [(self.env.ref('hr_recruitment.hr_applicant_view_graph').id, 'graph'),
                         (self.env.ref('hr_recruitment.hr_applicant_view_pivot').id, 'pivot')]
        context = {'search_default_creation_month': 1, 'search_default_job': 2}
        if  not self.env.user.has_group('hr_recruitment.group_hr_recruitment_user'):
            job_id = self.env['hr.job'].search([])
            data_job = []
            
            for record in job_id:
                data_stage = [data.stage_id.id for data in record.stage_ids.filtered(lambda line: self.env.user.id in line.user_ids.ids )]
                data_job.extend(data_stage)
            return {
            'type': 'ir.actions.act_window',
            'name': 'Recruitment Analysis',
            'res_model': 'hr.applicant',
            'view_type': 'graph',
            'views':views,
            'search_view_id':search_view_id.id,
            'view_mode': 'graph,pivot',
            'context':context,
            'domain': [('job_id.real_second_user_ids','in',self.env.user.id),('stage_id','in',data_job)]
            }
        elif self.env.user.has_group('hr_recruitment.group_hr_recruitment_user') and not self.env.user.has_group('equip3_hr_accessright_settings.equip3_group_hr_recruitment_manager'):
            return {
            'type': 'ir.actions.act_window',
            'name': 'Recruitment Analysis',
            'res_model': 'hr.applicant',
            'view_type': 'graph',
            'views':views,
            'search_view_id':search_view_id.id,
            'view_mode': 'graph,pivot',
            'context':context,
            'domain': [('job_id.user_ids','in',self.env.user.id)]
            }
        elif  self.env.user.has_group('equip3_hr_accessright_settings.equip3_group_hr_recruitment_manager') and not self.env.user.has_group('hr_recruitment.group_hr_recruitment_manager'):
            return {
            'type': 'ir.actions.act_window',
            'name': 'Recruitment Analysis',
            'res_model': 'hr.applicant',
            'view_type': 'graph',
            'views':views,
            'search_view_id':search_view_id.id,
            'view_mode': 'graph,pivot',
            'context':context,
            'domain': [('job_id.user_ids','in',self.env.user.id)]
            }
        elif self.env.user.has_group('hr_recruitment.group_hr_recruitment_manager'):
            return {
            'type': 'ir.actions.act_window',
            'name': 'Recruitment Analysis',
            'res_model': 'hr.applicant',
            'view_type': 'graph',
            'views':views,
            'search_view_id':search_view_id.id,
            'view_mode': 'graph,pivot',
            'context':context
            }
        
    
    def get_menu(self):
        if  not self.env.user.has_group('hr_recruitment.group_hr_recruitment_user'):
            search_view_id= self.env.ref('hr_recruitment.hr_applicant_view_search_bis')
            job_id = self.env['hr.job'].search([])
            data_job = []
            for record in job_id:
                data_stage = [data.stage_id.id for data in record.stage_ids.filtered(lambda line: self.env.user.id in line.user_ids.ids )]
                data_job.extend(data_stage)
        
            return {
            'type': 'ir.actions.act_window',
            'name': 'Applications',
            'res_model': 'hr.applicant',
            # 'view_type': 'kanban',
            'search_view_id':search_view_id.id,
            'view_mode': 'kanban,tree,form,graph,calendar,pivot',
            'context':{'search_default_job_id': self.env.context.get('active_id'), 'default_job_id': self.env.context.get('active_id')},
            'domain': [('job_id.real_second_user_ids','in',self.env.user.id),('stage_id','in',data_job)]
            }
        elif self.env.user.has_group('hr_recruitment.group_hr_recruitment_user') and not self.env.user.has_group('equip3_hr_accessright_settings.equip3_group_hr_recruitment_manager'):
            search_view_id= self.env.ref('hr_recruitment.hr_applicant_view_search_bis')
            return {
            'type': 'ir.actions.act_window',
            'name': 'Applications',
            'res_model': 'hr.applicant',
            # 'view_type': 'kanban',
            'search_view_id':search_view_id.id,
            'view_mode': 'kanban,tree,form,graph,calendar,pivot',
            'context':{'search_default_job_id': self.env.context.get('active_id'), 'default_job_id': self.env.context.get('active_id')},
            'domain': [('job_id.user_ids','in',self.env.user.id)]
            }
        elif  self.env.user.has_group('equip3_hr_accessright_settings.equip3_group_hr_recruitment_manager'):
            print("hereee")
            print("hereee")
            search_view_id= self.env.ref('hr_recruitment.hr_applicant_view_search_bis')
            return {
            'type': 'ir.actions.act_window',
            'name': 'Applications',
            'res_model': 'hr.applicant',
            # 'view_type': 'kanban',
            'search_view_id':search_view_id.id,
            'view_mode': 'kanban,tree,form,graph,calendar,pivot',
            'context':{'search_default_job_id': self.env.context.get('active_id'), 'default_job_id': self.env.context.get('active_id')},
           
            }
            
    
    
    def _is_hide_wa(self):
        for record in self:
            hide_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment_extend.is_whatsapp')
            if hide_wa:
                record['is_hide_wa']= False
            else:
                record['is_hide_wa']= True
        
        
    
    
    
    @api.model
    def fields_view_get(self, view_id=None, view_type=None,
                        toolbar=True, submenu=True):
        res = super(HrApplicant, self).fields_view_get(
            view_id=view_id, view_type=view_type,toolbar=toolbar,submenu=submenu)
        
        # print(res)
        arch = str(res['arch'])
        if  self.env.user.has_group('hr_recruitment.group_hr_recruitment_user'):
            arch = str(arch).replace("""<field name="stage_replace_id" clickable="0" widget="statusbar" options="{\'clickable\': \'0\', \'fold_field\': \'fold\',\'sequence_field\':\'sequence\'}" domain="[(\'id\',\'in\',stage_replace_domain_ids)]" force_Save="1" on_change="1" can_create="true" can_write="true" modifiers="{&quot;readonly&quot;: true}"/>\n""","""<field name="stage_replace_id" clickable="1" widget="statusbar" options="{\'clickable\': \'1\', \'fold_field\': \'fold\',\'sequence_field\':\'sequence\'}" domain="[(\'id\',\'in\',stage_replace_domain_ids)]" force_Save="1" on_change="1" can_create="true" can_write="true" modifiers="{&quot;readonly&quot;: false}"/>\n""")
            root = etree.fromstring(arch)
            res['arch'] = etree.tostring(root)
        
        sms = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment_extend.is_sms')
        if not sms:
            #dont change or inherit the fields string
            arch = str(arch).replace("""<field name="partner_phone" nolabel="1" widget="phone" class="oe_inline" modifiers="{}"/>""","""<field name="partner_phone" class="oe_inline" modifiers="{}"/>""")
            arch = str(arch).replace("""<field name="partner_mobile" nolabel="1" widget="phone" class="oe_inline" modifiers="{}"/>""","""<field name="partner_mobile" nolabel="1" class="oe_inline" modifiers="{}"/>""")
            root = etree.fromstring(arch)
            res['arch'] = etree.tostring(root)
        return res
    
    def _get_wa_url(self):
        for record in self:
            if record.partner_phone:
                record.wa_url = f"https://api.whatsapp.com/send?phone={record.partner_phone}"
            else:
                record.wa_url = False
                
    def open_wa_phone(self):  
        for record in self:
            if record.partner_phone:
                phone =  str(record.partner_phone)
                if phone[0:3] != "+62":
                    phone = "+62"+phone[1:range]
                elif phone[0:3] == "+62":
                    phone = phone[1:]
                
                return {
                   'name'     : 'wa website',
                  'res_model': 'ir.actions.act_url',
                  'type'     : 'ir.actions.act_url',
                  'target'   : '_blank',
                  'url'      : f"https://api.whatsapp.com/send?phone={phone}"
               }
            else:
                pass
            
    def open_wa_mobile(self):  
        for record in self:
            if record.partner_mobile:
                phone =  str(record.partner_mobile)
                if phone[0:3] != "+62":
                    phone = "+62"+phone[1:range]
                elif phone[0:3] == "+62":
                    phone = phone[1:]
                return {
                   'name'     : 'wa website',
                  'res_model': 'ir.actions.act_url',
                  'type'     : 'ir.actions.act_url',
                  'target'   : '_blank',
                  'url'      : f"https://api.whatsapp.com/send?phone={phone}"
               }
            else:
                pass
                
    def _get_wa_url_second(self):
        for record in self:
            if record.partner_mobile:
                record.wa_url_second = f"https://api.whatsapp.com/send?phone={record.partner_mobile}"
            else:
                record.wa_url_second = False
    
    
    def assign_to_self(self):
        for record in self:
            record.user_id = self.env.user.id
            record.is_hide_assign = True



    def write(self, vals):
        if 'partner_phone' in vals:
            number = str(vals['partner_phone'])
            range = len(number)
            if number[0:3] != "+62":
                vals['partner_phone'] = "+62"+number[1:range]
        if 'partner_mobile' in vals:
            number = str(vals['partner_mobile'])
            range = len(number)
            if number[0:3] != "+62":
                vals['partner_mobile'] = "+62"+number[1:range]
        if 'stage_id' in vals:
            print("hereeee")
            stage_replace_id = self.env['job.stage.line'].search(
                [('job_id', '=', self.job_id.id), ('stage_id', '=', vals['stage_id'])], order='sequence asc')
            if stage_replace_id.interview_id:
                self.is_interview = True
            if stage_replace_id.survey_id:
                template = self.env.ref('survey.mail_template_user_input_invite', raise_if_not_found=False)
                survey=self.env['survey.invite'].create({'survey_id':stage_replace_id.survey_id.id,'emails':str(self.email_from),'template_id':template.id})
                context = self.env.context = dict(self.env.context)
                survey_url = survey.survey_start_url+f"?surveyId={stage_replace_id.survey_id.id}&applicantId={self.id}&jobPosition={self.job_id.id}"
                send_by_email = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment.send_by_email')
                if send_by_email:
                    context.update({
                        'email_to': self.email_from,
                        'name': self.partner_name,
                        'url_test': survey_url,
                        'title':stage_replace_id.survey_id.title
                    })
                    template = self.env.ref('equip3_hr_recruitment_extend.mail_template_invite_test')
                    if stage_replace_id.survey_id.is_application_form:
                        template = self.env.ref('equip3_hr_recruitment_extend.mail_application_invite', raise_if_not_found=True)
                    template.send_mail(self.id, force_send=True)
                    template.with_context(context)
                if stage_replace_id:
                    self.stage_replace_id = stage_replace_id.id
                    self.shadow_stage_replace_id = stage_replace_id.id
            
                    send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment_extend.send_by_wa')
                    if send_by_wa:
                        # connector = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment_extend.connector_id')
                        # if connector:
                        #     connector_id = self.env['acrux.chat.connector'].search([('id','=',connector)])
                            
                        #     if connector_id.ca_status:
                        template = self.env.ref('equip3_hr_recruitment_extend.wa_template_1')
                        if template:
                            string_test = str(template.message)
                            if self.partner_name:
                                if "${applicant_name}" in string_test:
                                    string_test=string_test.replace("${applicant_name}",self.partner_name)
                            if "${survey_url}" in string_test:
                                string_test = string_test.replace("${survey_url}",survey_url)
                            if "${br}" in string_test:
                                string_test = string_test.replace("${br}",f"\n")
                            if self.partner_mobile:
                                phone_num = str(self.partner_mobile)
                                if "+" in phone_num:
                                    phone_num =  phone_num.replace("+","")
                                param = {'body': string_test, 'phone': phone_num}
                                domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                                token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                                try:
                                    print("hereee")
                                    request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                                    print(request_server)
                                except ConnectionError:
                                    raise ValidationError("Not connect to API Chat Server. Limit reached or not active")
                            if stage_replace_id.stage_id.template_wa_id:
                                string_wa = str(stage_replace_id.stage_id.template_wa_id.message)
                                if self.partner_name:
                                    if "${applicant_name}" in string_wa:
                                        string_wa=string_wa.replace("${applicant_name}",self.partner_name)
                                if "${survey_url}" in string_wa:
                                    string_wa = string_wa.replace("${survey_url}",survey_url)
                                if "${br}" in string_wa:
                                    string_wa = string_wa.replace("${br}",f"\n")
                                phone_num = str(self.partner_mobile)
                                if self.partner_mobile:
                                    if "+" in phone_num:
                                        phone_num =  int(phone_num.replace("+",""))
                                    param = {'body': string_wa, 'phone': phone_num}
                                    domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                                    token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                                    try:
                                        print("hereee")
                                        request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                                        print(request_server)
                                    except ConnectionError:
                                        raise ValidationError("Not connect to API Chat Server. Limit reached or not active")
                                            # connector_id.ca_request('post','sendMessage',param)
                                        
        res=super(HrApplicant, self).write(vals)
        return res



    def qualification_answer(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Detailed Answers',
            'res_model': 'survey.user_input.line',
            'view_type': 'form',
            'view_mode': 'tree',
            'domain': [('applicant_id','=', self.id)]
        }
        
    
    @api.model
    def create(self, vals):
        result = []
        if 'partner_phone' in vals:
                number = str(vals['partner_phone'])
                range = len(number)
                if number[0:3] != "+62":
                    vals['partner_phone'] = "+62"+number[1:range]
                
        if 'partner_mobile' in vals:
            number = str(vals['partner_mobile'])
            range = len(number)
            if number[0:3] != "+62":
                vals['partner_mobile'] = "+62"+number[1:range]

        vals['applicant_id'] = self.env['ir.sequence'].next_by_code('applicant.sequence')
        res = super(HrApplicant, self).create(vals)
        
        
        
        if res.stage_id:
            stage_replace_id = self.env['job.stage.line'].search(
                [('job_id', '=', self.job_id.id), ('stage_id', '=', res.stage_id.id)], order='sequence asc')
            if stage_replace_id.survey_id:
                template = self.env.ref('survey.mail_template_user_input_invite', raise_if_not_found=False)

                survey=self.env['survey.invite'].create({'survey_id':stage_replace_id.survey_id.id,'emails':str(res.email_from),'template_id':template.id})
                context = self.env.context = dict(self.env.context)
                survey_url = survey.survey_start_url+f"?surveyId={stage_replace_id.survey_id.id}&applicantId={res.id}&jobPosition={res.job_id.id}"
                send_by_email = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment.send_by_email')
                if send_by_email:
                    context.update({
                        'email_to': res.email_from,
                        'name': res.partner_name,
                        'url_test': survey_url,
                        'title':stage_replace_id.survey_id.title
                    })
                    template = self.env.ref('equip3_hr_recruitment_extend.mail_template_invite_test')
                    if stage_replace_id.survey_id.is_application_form:
                        template = self.env.ref('equip3_hr_recruitment_extend.mail_application_invite', raise_if_not_found=True)
                    template.sudo().send_mail(self.id, force_send=True)
                    template.with_context(context)
                if stage_replace_id:
                    send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment_extend.send_by_wa')
                    if send_by_wa:
                        # connector = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_recruitment_extend.connector_id')
                        # if connector:
                        #     connector_id = self.env['acrux.chat.connector'].search([('id','=',connector)])
                            
                        #     if connector_id.ca_status:
                        template = self.env.ref('equip3_hr_recruitment_extend.wa_template_1')
                        if template:
                            string_test = str(template.message)
                            if "${applicant_name}" in string_test:
                                string_test=string_test.replace("${applicant_name}",res.partner_name)
                            if "${survey_url}" in string_test:
                                string_test = string_test.replace("${survey_url}",survey_url)
                            if "${br}" in string_test:
                                string_test = string_test.replace("${br}",f"\n")
                            phone_num = str(res.partner_mobile)
                            if "+" in phone_num:
                                phone_num =  int(phone_num.replace("+",""))
                            param = {'body': string_test, 'phone': phone_num}
                            domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                            token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                            try:
                                request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                            except ConnectionError:
                                raise ValidationError("Not connect to API Chat Server. Limit reached or not active")
                            if stage_replace_id.stage_id.template_wa_id:
                                string_wa = str(stage_replace_id.stage_id.template_wa_id.message)
                                if "${applicant_name}" in string_wa:
                                    string_wa=string_wa.replace("${applicant_name}",res.partner_name)
                                if "${survey_url}" in string_wa:
                                    string_wa = string_wa.replace("${survey_url}",survey_url)
                                if "${br}" in string_wa:
                                    string_wa = string_wa.replace("${br}",f"\n")
                                phone_num = str(res.partner_mobile)
                                if "+" in phone_num:
                                    phone_num =  int(phone_num.replace("+",""))
                                param = {'body': string_wa, 'phone': phone_num}
                                domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                                token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                                try:
                                    request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                                except ConnectionError:
                                    raise ValidationError("Not connect to API Chat Server. Limit reached or not active")
                                
        
        
        for record in res.applicant_question_answer:
            
               

            if record.question.lower() == "what is your name?":
                partner_name = record.answer
                res.write({
                    'partner_name': partner_name,
                    'name':f"{res.name}/{partner_name}"
                })
            elif record.question.lower() == "what is your email?":
                email_from = record.answer
                res.write({
                    'email_from': email_from
                })
            elif record.question.lower() == "what is your phone number?":
                partner_phone = record.answer
                res.write({
                    'partner_phone': partner_phone
                })
            elif record.question.lower() == "what is your mobile number?":
                partner_mobile = record.answer
                res.write({
                    'partner_mobile': partner_mobile
                })
            elif record.question.lower() == "upload your cv":
                file_cv = record.file
                res.write({
                    'file_cv': file_cv
                })
            elif record.question.lower() == "what is your degree?":
                degree_check = self.env['hr.recruitment.degree'].search([('name','=',record.answer)],limit=1)
                if degree_check:
                    degree = degree_check.id
                    res.write({
                        'type_id': degree
                    })
            elif record.question.lower() == "what is your address?":
                res.write({
                    'address': record.answer
                })
            elif record.question.lower() == "what is your expected salary?":
                res.write({
                    'salary_expected': record.answer
                })
                
            elif record.question.lower() == "what is your id card number?":
                res.write({
                    'identification_no': record.answer
                })
            elif record.question.lower() == "what is your gender?":
                res.write({
                    'gender': str(record.answer).lower()
                })
            elif record.question.lower() == "what is your date of birth?":
                date_of_birth = datetime.strptime(record.answer,"%Y-%m-%d")
                res.write({
                    'date_of_birth': date_of_birth,
                    
                })
                # result.extend(res.set_stage_applicant(res.applicant_question_answer,res,record.question.lower()))

            # elif record.question.lower() == "are you currently in college?":
            #     if str(record.answer).lower() == 'yes':
            #         college = True
            #     else:
            #         college = False
            #     res.write({
            #         'in_college': college
            #     })
            # elif record.question.lower() == "what is your marital status?":
                marital_status = self.env['employee.marital.status'].search([('name','=',record.answer)],limit=1)
                if marital_status:
                    res.write({
                        'marital_status': marital_status.id
                    })
            elif record.question.lower() == "what is your religion?":
                employee_religion = self.env['employee.religion'].search([('name','=',record.answer)],limit=1)
                if employee_religion:
                    res.write({
                        'religion': employee_religion.id
                    })
            elif record.question.lower() == "what is your last drawn salary?":
                res.write({
                    'last_drawn_salary': float(record.answer)
                })
        result.extend(self.set_stage_applicant(res.applicant_question_answer,res))  
        result.extend(self.set_stage_applicant(res.applicant_question_answer_spesific,res))
        
        
        if any(data == False for data in result):
            res.stage_id = res.job_id.default_reject_stage_id.id
        else:
            stage = [data.stage_id.id for data in res.job_id.stage_ids.filtered(lambda line: line.is_apply_stage)]
            if stage:
                res.stage_id = stage[0] 
        return res
    
    
    def set_stage_applicant(self, applicant_answers,res,question_string=None):
        list_answer = []
        for line_data in applicant_answers:
            question = res.job_id.question_job_position_ids.filtered(lambda line: str(line.question_string) == str(line_data.question))
            if question:
                for data in question:
                    list_answer.append(data.check_question(line_data.answer,res,question.question.question))
            question_global = res.job_id.question_job_position.filtered(
                lambda line: line.question.question == line_data.question)
            if question_global:
                list_answer.append(question_global.check_question(line_data.answer,res,question_global.question.question))
        return list_answer
        
        

    @api.onchange('stage_replace_id')
    def _onchange_stage_replace_id(self):
        for record in self:
                if record.stage_replace_id:
                    record.stage_id = record.stage_replace_id.stage_id.id
                    record.shadow_stage_replace_id = record.stage_replace_id.id
                
    @api.depends('file_cv')
    def _compute_file_name(self):
        for record in self:
            if record.file_cv:
                if record.partner_name:
                    record.cv_name = "CV_" + record.partner_name
                elif record.name:
                    record.cv_name = "CV_" + record.name
                else:
                    record.cv_name =  False


    @api.depends('job_id')
    def compute_stage_replace_id(self):
        for record in self:
            if record.stage_id and record.job_id:
                stage_replace_id = self.env['job.stage.line'].search([('job_id', '=', record.job_id.id),('stage_id','=',record.stage_id.id)], order='sequence asc',limit=1)
                if stage_replace_id:
                    record.stage_replace_id = stage_replace_id.id
                    record.shadow_stage_replace_id = stage_replace_id.id
                else:
                    record.stage_replace_id = False

            else:
                record.stage_replace_id = False


    @api.onchange('stage_replace_domain_ids')
    def _onchange_stage_replace_domain_ids(self):
        for record in self:
            if record.stage_replace_domain_ids:
                if not record.stage_id:
                    record.stage_replace_id = record.stage_replace_domain_ids.ids[0]
                    record.shadow_stage_replace_id = record.stage_replace_domain_ids.ids[0]
                    record.stage_id = record.stage_replace_id.stage_id.id
                else:
                    record.stage_replace_id = False
                    record.stage_id = False
            else:
                record.stage_replace_id = False
                record.stage_id = False


    #dont delete , it's matter function to write the base odoo stage
    @api.depends('job_id')
    def _compute_stage(self):
        pass


    @api.depends('job_id')
    def _compute_stage_domain_ids(self):
        for record in self:
            if  not self.env.user.has_group('hr_recruitment.group_hr_recruitment_user'):
                if record.job_id:
                    stage_list = []
                    stage_replace_list = []
                    stage_ids = self.env['job.stage.line'].search([('job_id','=',record.job_id.id),('user_ids','in',self.env.user.id)],order='sequence asc')
                    if stage_ids:
                        id = [data.stage_id.id for data in stage_ids]
                        second_id = [data.id for data in stage_ids]
                        stage_list.extend(id)
                        stage_replace_list.extend(second_id)
                        record.stage_domain_ids =  [(6,0, stage_list)]
                        record.stage_replace_domain_ids =  [(6,0, stage_replace_list)]
                    else:
                        record.stage_domain_ids = False
                        record.stage_replace_domain_ids = False
                else:
                    record.stage_domain_ids = False
                    record.stage_replace_domain_ids = False
            else:
                if record.job_id:
                    stage_list = []
                    stage_replace_list = []
                    stage_ids = self.env['job.stage.line'].search([('job_id','=',record.job_id.id)],order='sequence asc')
                    if stage_ids:
                        id = [data.stage_id.id for data in stage_ids]
                        second_id = [data.id for data in stage_ids]
                        stage_list.extend(id)
                        stage_replace_list.extend(second_id)
                        record.stage_domain_ids =  [(6,0, stage_list)]
                        record.stage_replace_domain_ids =  [(6,0, stage_replace_list)]
                    else:
                        record.stage_domain_ids = False
                        record.stage_replace_domain_ids = False
                else:
                    record.stage_domain_ids = False
                    record.stage_replace_domain_ids = False





    @api.model
    def _read_group_stage_ids(self, stages, domain, order):
        if  not self.env.user.has_group('hr_recruitment.group_hr_recruitment_user'):
            job_id = self._context.get('default_job_id')
            search_domain = []
            list_id = []
            if stages:
                if job_id:
                    stage_search_ids = self.env['job.stage.line'].search([('job_id', '=', job_id),('user_ids','in',self.env.user.id)], order='sequence asc')
                    if stage_search_ids:
                        id = [data.stage_id.id for data in stage_search_ids]
                        list_id.extend(id)
                search_domain = [('id', 'in', list_id)] + search_domain
            stage_ids = stages._search(search_domain, order=order, access_rights_uid=SUPERUSER_ID)
            if list_id:
                return stages.browse(list_id)
            else:
                return stages.browse(stage_ids)
        else:
            job_id = self._context.get('default_job_id')
            search_domain = []
            list_id = []
            if stages:
                if job_id:
                    stage_search_ids = self.env['job.stage.line'].search([('job_id', '=', job_id)], order='sequence asc')
                    if stage_search_ids:
                        id = [data.stage_id.id for data in stage_search_ids]
                        list_id.extend(id)
                search_domain = [('id', 'in', list_id)] + search_domain
            stage_ids = stages._search(search_domain, order=order, access_rights_uid=SUPERUSER_ID)
            if list_id:
                return stages.browse(list_id)
            else:
                return stages.browse(stage_ids)

    @api.model
    def send_mail_refuse(self):
        context = self.env.context = dict(self.env.context)
        applicant_refuse = self.sudo().search([('active', '=', False),('refuse_is_sent', '=', False)])
        for res in applicant_refuse:
            refuse_template = res.stage_id.refuse_template_id
            if refuse_template:
                res.write({'refuse_is_sent': True})
                context.update({
                    'email_to': res.email_from,
                })
                refuse_template.send_mail(res.id, force_send=True)
                refuse_template.with_context(context)
            
