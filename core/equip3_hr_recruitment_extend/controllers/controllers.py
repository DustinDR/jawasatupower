# -*- coding: utf-8 -*-
from datetime import timedelta
import datetime

import werkzeug
from dateutil.relativedelta import relativedelta

import odoo
from odoo import http, fields
from odoo.exceptions import UserError
from odoo.http import content_disposition, Controller, request, route
import json
from odoo.addons.survey.controllers.main import Survey
from odoo.addons.website_hr_recruitment.controllers.main import WebsiteHrRecruitment
from odoo.addons.survey.controllers.survey_session_manage import UserInputSession
import base64
import os
from odoo.exceptions import UserError, ValidationError

class WebsiteHrRecruitment(WebsiteHrRecruitment):

    def sitemap_jobs(env, rule, qs):
        if not qs or qs.lower() in '/jobs':
            yield {'loc': '/jobs'}


    @http.route('/get-skill-type', type='json', auth='public')
    def get_skill_type(self):
        skill_type_obj = request.env['hr.skill.type']
        result = []
        skill_types = skill_type_obj.sudo().search([]).read(['name'])
        return skill_types


    @http.route('/get-skill-other-list', type='json', auth='public')
    def get_skill_other_list(self,skill_type_id):
        skill_type_obj = request.env['hr.skill.type']
        result = {}
        skill_types = skill_type_obj.sudo().browse(skill_type_id)
        result['skill'] = skill_types.skill_ids.read(['name'])
        result['level'] = skill_types.skill_level_ids.read(['name'])
        return result

    @http.route([
        '/jobs',
        '/jobs/country/<model("res.country"):country>',
        '/jobs/department/<model("hr.department"):department>',
        '/jobs/country/<model("res.country"):country>/department/<model("hr.department"):department>',
        '/jobs/office/<int:office_id>',
        '/jobs/country/<model("res.country"):country>/office/<int:office_id>',
        '/jobs/department/<model("hr.department"):department>/office/<int:office_id>',
        '/jobs/country/<model("res.country"):country>/department/<model("hr.department"):department>/office/<int:office_id>',
    ], type='http', auth="public", website=True, sitemap=sitemap_jobs)

    def jobs(self, country=None, department=None, office_id=None, **kwargs):
        env = request.env(context=dict(request.env.context, show_address=True, no_tag_br=True))

        Country = env['res.country']
        Jobs = env['hr.job']
        dep_obj = env['hr.department']
        company_obj = env['res.company']
        partner_obj = env['res.partner']
        companies = company_obj.sudo().search([])
        offices = []
        work_locations = env['work.location.object'].sudo().search([])
        office_arr = partner_obj
        for c in companies:
            if c.partner_id not in office_arr and c.partner_id.city and c.partner_id.country_id:
                office_arr+=c.partner_id
        if office_arr:
            offices = office_arr

        # List jobs available to current UID
        domain = request.website.website_domain()
        domain+=[('website_published','=',True)]
        if kwargs.get('jobname'):
            domain+=[('name','ilike',kwargs['jobname'])]
        job_ids = Jobs.search(domain, order="is_published desc, no_of_recruitment desc").ids
        # Browse jobs as superuser, because address is restricted
        jobs = Jobs.sudo().browse(job_ids)

        # Default search by user country
        if not (country or department or office_id or kwargs.get('all_countries')):
            country_code = request.session['geoip'].get('country_code')
            if country_code:
                countries_ = Country.search([('code', '=', country_code)])
                country = countries_[0] if countries_ else None
                if not any(j for j in jobs if j.address_id and j.address_id.country_id == country):
                    country = False

        # Filter job / office for country
   

        # Deduce departments and countries offices of those jobs
        departments = set(j.department_id for j in jobs if j.department_id)
        countries = set(o.country_id for o in offices if o.country_id)

        if department:
            jobs = [j for j in jobs if j.department_id and j.department_id.id == department.id]
        if office_id:
            jobs = [j for j in jobs if j.custom_work_location_id and j.custom_work_location_id.id == office_id]
        else:
            office_id = False

        # Render page
        return request.render("website_hr_recruitment.index", {
            'jobs': jobs,
            'countries': countries,
            'departments': dep_obj.sudo().search([]),
            'offices': offices,
            'country_id': country,
            'department_id': department,
            'office_id': office_id,
            'work_locations':work_locations,
        })

class Recruiter(http.Controller):
    
    
    @http.route(["/open_new_applicant/<int:id>"], type='http', auth="public", website=True, csrf=False)
    def open_new_applicant(self, id, **kw):
        action_id = request.env.ref('equip3_hr_recruitment_extend.hr_applicant_new_tab')
        return request.redirect('/web?&#view_type=form&model=hr.applicant&action=%s&id=%s' % (action_id.id,id))
    
    @http.route(["/open_interview_result/<int:id>"], type='http', auth="public", website=True, csrf=False)
    def open_interview_result(self, id, **kw):
        action_id = request.env.ref('equip3_hr_recruitment_extend.survey_interview_result')
        return request.redirect('/web?&#view_type=form&model=survey.user_input&action=%s&id=%s' % (action_id.id,id))

    @http.route(["/recruiter-submit"], type='http', auth="public", website=True, csrf=False)
    def recruiter_submit(self, **kw):
        values = []
        values_specific =[]
        past_experience_ids = []
        employee_skill_ids = []
        if kw.get('past_experience'):
            past_experience = json.loads(kw['past_experience'])
            for data_pe in past_experience:
                start_date_pe = datetime.datetime.strptime(data_pe['start_date'], '%d/%m/%Y')
                start_date_pe = datetime.date.strftime(start_date_pe, "%Y-%m-%d")
                if data_pe.get('end_date'):
                    end_date_pe = datetime.datetime.strptime(data_pe['end_date'], '%d/%m/%Y')
                    end_date_pe = datetime.date.strftime(end_date_pe, "%Y-%m-%d")
                else:
                    end_date_pe = False
                is_currently_work_here = data_pe['is_currently_work_here']
                if is_currently_work_here == 'false':
                    is_currently_work_here  = False

                past_experience_ids.append(
                    (0, 0,{
                        'start_date':start_date_pe,
                        'end_date':end_date_pe,
                        'company_name':data_pe['company_name'],
                        'position':data_pe['position'],
                        'reason_for_leaving':data_pe['reason'],
                        'salary':data_pe['salary'],
                        'is_currently_work_here':is_currently_work_here,
                        'company_telephone_number':data_pe['company_phone'],
                    })
                )

        if kw.get('employee_skill'):
            employee_skill = json.loads(kw['employee_skill'])
            for data_pe in employee_skill:
                employee_skill_ids.append(
                    (0, 0,{
                        'skill_type_id':data_pe['skill_type_id'],
                        'skill_id':data_pe['skill_id'],
                        'skill_level_id':data_pe['skill_level_id'],
                    })
                )
            
        if kw.get('job_id'):
            job = request.env['hr.job'].sudo().search([('id','=',int(kw.get('job_id')))])
            for question in job.question_job_position:
                if question.question.type != 'file':
                    if question.question.type == 'decimal':
                        q_name =  str(question.id)
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question' : question.question.question,
                                'answer' : str(float(kw.get(q_name))),
                            })
                            values.append((0,0,dict))
                    elif question.question.type =='many2one':
                        q_name = str(question.id)
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question': question.question.question,
                                'answer': kw.get(q_name),
                            })
                            values.append((0, 0, dict))
                            
                    elif question.question.type =='drop_down_list':
                        q_name = str(question.id)
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question': question.question.question,
                                'answer': kw.get(q_name),
                            })
                            values.append((0, 0, dict))
                            
                    elif question.question.type =='date':
                        q_name = str(question.id)
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question': question.question.question,
                                'answer': str(kw.get(q_name)),
                            })
                            values.append((0, 0, dict))

                    elif question.question.type =='multiple_choice_one_answer':
                        q_name = str(question.id)
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question': question.question.question,
                                'answer': kw.get(q_name),
                            })
                            values.append((0, 0, dict))
                    elif question.question.type == 'multiple_choice_multiple_answer':
                        q_name = str(question.id)
                        q_name_replace = str(request.httprequest.form.getlist(q_name)).replace('[')
                        q_name_replace_final = str(request.httprequest.form.getlist(q_name_replace)).replace(']')
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question': question.question.question,
                                'answer': q_name_replace_final,
                            })
                            values.append((0, 0, dict))
                    else:
                        q_name = str(question.id)
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question': question.question.question,
                                'answer': kw.get(q_name),
                            })
                            values.append((0, 0, dict))
                if question.question.type == 'file':
                    q_name = str(question.id)
                    if kw.get(q_name):

                        dict = {}
                        dict.update({
                            'question' : question.question.question,
                            'file' : base64.b64encode(kw.get(q_name).read()),
                        })
                        values.append((0,0,dict))
            for question in job.question_job_position_ids:
                if question.question.type != 'file':
                    if question.question.type == 'decimal':
                        q_name = str(question.id)
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question' : question.question.question,
                                'answer' : str(float(kw.get(q_name))),
                            })
                            values_specific.append((0,0,dict))
                    elif question.question.type =='many2one':
                        q_name = str(question.id)
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question': question.question.question,
                                'answer': kw.get(q_name),
                            })
                        
                            values_specific.append((0,0,dict))
                    elif question.question.type =='drop_down_list':
                        q_name = str(question.id)
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question': question.question.question,
                                'answer': kw.get(q_name),
                            })
                        
                            values_specific.append((0,0,dict))
                            
                    elif question.question.type =='date':
                        q_name = str(question.id)
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question': question.question.question,
                                'answer': str(kw.get(q_name)),
                            })
                            values_specific.append((0,0,dict))
                    elif question.question.type == 'multiple_choice_one_answer':
                        q_name = str(question.id)
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question': question.question.question,
                                'answer': kw.get(q_name),
                            })
                            values_specific.append((0, 0, dict))
                    elif question.question.type == 'multiple_choice_multiple_answer':
                        q_name = str(question.id)
                        q_name_replace = str(request.httprequest.form.getlist(q_name)).replace('[', '')
                        q_name_replace_final = str(q_name_replace).replace(']', '')
                        q_name_replace_final_answer = str(q_name_replace_final).replace(']', '')
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question': question.question.question,
                                'answer': q_name_replace_final_answer,
                            })
                            values_specific.append((0, 0, dict))
                    else:
                        q_name = str(question.id)
                        if kw.get(q_name):
                            dict = {}
                            dict.update({
                                'question': question.question.question,
                                'answer': kw.get(q_name),
                            })
                            values_specific.append((0,0,dict))

                if question.question.type == 'file':
                    q_name =  str(question.id)
                    if kw.get(q_name):
                        dict = {}
                        dict.update({
                            'question' : question.question.question,
                            'file' : base64.b64encode(kw.get(q_name).read()),
                        })
                        values_specific.append((0, 0, dict))






            if values:
                applicant = request.env['hr.applicant'].sudo().create({
                    'name': job.name ,
                    'job_id':  int(kw.get('job_id')) if kw.get('job_id') else False,
                    'applicant_question_answer':  values,
                    'applicant_question_answer_spesific':  values_specific,
                    'past_experience_ids':past_experience_ids,
                    'employee_skill_ids':employee_skill_ids,
                })
        return request.redirect('/job-thank-you')

class Surveyedit(Survey):

    @http.route('/survey/submit/<string:survey_token>/<string:answer_token>', type='json', auth='public', website=True)
    def survey_submit(self, survey_token, answer_token, **post):

        """ Submit a page from the survey.
        This will take into account the validation errors and store the answers to the questions.
        If the time limit is reached, errors will be skipped, answers will be ignored and
        survey state will be forced to 'done'"""
        # Survey Validation

        access_data = self._get_access_data(survey_token, answer_token, ensure_token=True)
        if access_data['validity_code'] is not True:
            return {'error': access_data['validity_code']}
        survey_sudo, answer_sudo = access_data['survey_sudo'], access_data['answer_sudo']


        if answer_sudo.state == 'done':
            return {'error': 'unauthorized'}

        questions, page_or_question_id = survey_sudo._get_survey_questions(answer=answer_sudo,
                                                                           page_id=post.get('page_id'),
                                                                           question_id=post.get('question_id'))

        if not answer_sudo.test_entry and not survey_sudo._has_attempts_left(answer_sudo.partner_id, answer_sudo.email,
                                                                             answer_sudo.invite_token):
            # prevent cheating with users creating multiple 'user_input' before their last attempt
            return {'error': 'unauthorized'}

        if answer_sudo.survey_time_limit_reached or answer_sudo.question_time_limit_reached:
            if answer_sudo.question_time_limit_reached:
                time_limit = survey_sudo.session_question_start_time + relativedelta(
                    seconds=survey_sudo.session_question_id.time_limit
                )
                time_limit += timedelta(seconds=3)
            else:
                time_limit = answer_sudo.start_datetime + timedelta(minutes=survey_sudo.time_limit)
                time_limit += timedelta(seconds=10)
            if fields.Datetime.now() > time_limit:
                # prevent cheating with users blocking the JS timer and taking all their time to answer
                return {'error': 'unauthorized'}



        errors = {}
        # Prepare answers / comment by question, validate and save answers
        for question in questions:
            print("hereee")
            print(question.id)
            print(post)
            if question.question_type == 'video':
                q_name =  str(question.id)
                if post.get(q_name):
                    print("hereeee 2")
                    print(post)
                    # base64.b64encode(post.get(q_name).read())
                    # print(base64.b64encode(post.get(q_name).read()))
            inactive_questions = request.env[
                'survey.question'] if answer_sudo.is_session_answer else answer_sudo._get_inactive_conditional_questions()
            if question in inactive_questions:  # if question is inactive, skip validation and save
                continue
            answer, comment = self._extract_comment_from_answers(question, post.get(str(question.id)))
            errors.update(question.validate_question(answer, comment))
            if not errors.get(question.id):
                answer_sudo.save_lines(question, answer, comment)


        if errors and not (answer_sudo.survey_time_limit_reached or answer_sudo.question_time_limit_reached):
            return {'error': 'validation', 'fields': errors}


        if not answer_sudo.is_session_answer:
            answer_sudo._clear_inactive_conditional_answers()

        if answer_sudo.survey_time_limit_reached or survey_sudo.questions_layout == 'one_page':
            answer_sudo._mark_done()
        elif 'previous_page_id' in post:
            # Go back to specific page using the breadcrumb. Lines are saved and survey continues
            return self._prepare_question_html(survey_sudo, answer_sudo, **post)
        else:
            vals = {'last_displayed_page_id': page_or_question_id}
            if not answer_sudo.is_session_answer:
                next_page = survey_sudo._get_next_page_or_question(answer_sudo, page_or_question_id)
                if not next_page:
                    answer_sudo._mark_done()

            answer_sudo.write(vals)
        input_line = request.env['survey.user_input.line'].sudo().search([('user_input_id', '=', answer_sudo.id)])
        
                        
        paramater = json.loads(request.httprequest.data)['params']
        if paramater['survey_id'] and paramater['applicant_id'] and paramater['job_position']:
            next_page = survey_sudo._get_next_page_or_question(answer_sudo, page_or_question_id)
            answer_sudo.write({'applicant_id': paramater['applicant_id'],'job_id':paramater['job_position'],'is_use':True})
            applicant = request.env['hr.applicant'].sudo().search(
                [('id', '=', paramater['applicant_id']), ('job_id', '=', paramater['job_position'])])
            answer_sudo.email = applicant.email_from
            applicant.sudo().write({'previous_score': answer_sudo.score_by_amount})
            input_line = request.env['survey.user_input.line'].sudo().search([('user_input_id', '=', answer_sudo.id)])
            if input_line:
                for data in input_line:
                    if data.answer_type == 'numerical_box':
                        data.sudo().write({'applicant_id': paramater['applicant_id'],'value_numerical_box':data.value_numerical_box})

            # if  int(answer_sudo.score_by_amount)  < applicant.stage_replace_id.min_qualification and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_without_answers' or int(answer_sudo.score_by_amount)  < applicant.stage_replace_id.min_qualification and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_with_answers':
            #     applicant.sudo().write({'stage_id': applicant.stage_replace_id.stage_failed.id, 'active': False})
            # else:
            #     next_stage = applicant.job_id.stage_ids.filtered(lambda line:line.sequence == applicant.stage_replace_id.sequence + 1)
            #     if next_stage and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_without_answers' and  int(answer_sudo.score_by_amount) > applicant.stage_replace_id.min_qualification or next_stage and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_with_answers' and  int(answer_sudo.score_by_amount) > applicant.stage_replace_id.min_qualification:
            #         applicant.sudo().write({'stage_id': next_stage.stage_id.id})
            #         applicant.sudo().message_post()

            
            if applicant.stage_replace_id.survey_id and applicant.stage_replace_id.interview_id:
                prev_input = request.env['survey.user_input'].sudo().search([('survey_id', '=', applicant.stage_replace_id.survey_id.id),('applicant_id', '=', applicant.id),('job_id', '=', applicant.job_id.id),('survey_type', '!=', 'INTERVIEW')], limit=1)
                if survey_sudo.id == applicant.stage_replace_id.interview_id.id:
                    if int(prev_input.score_by_amount) >= applicant.stage_replace_id.min_qualification and int(answer_sudo.skill_score) >= applicant.stage_replace_id.min_skills_score and int(answer_sudo.personality_score) >= applicant.stage_replace_id.min_personality_score and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_without_answers' or int(prev_input.score_by_amount) >= applicant.stage_replace_id.min_qualification and int(answer_sudo.skill_score) >= applicant.stage_replace_id.min_skills_score and int(answer_sudo.personality_score) >= applicant.stage_replace_id.min_personality_score and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_with_answers':
                        next_stage = applicant.job_id.stage_ids.filtered(lambda line:line.sequence == applicant.stage_replace_id.sequence + 1)
                        if next_stage and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_without_answers' and  int(answer_sudo.score_by_amount) > applicant.stage_replace_id.min_qualification or next_stage and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_with_answers' and  int(answer_sudo.score_by_amount) > applicant.stage_replace_id.min_qualification:
                            applicant.sudo().write({'stage_id': next_stage.stage_id.id})
                            applicant.sudo().message_post()
                    else:
                        applicant.sudo().write({'refuse_reason_id': applicant.stage_id.refuse_reason_id.id,'active': False})
            elif applicant.stage_replace_id.interview_id and not applicant.stage_replace_id.survey_id:
                if int(answer_sudo.skill_score) >= applicant.stage_replace_id.min_skills_score and int(answer_sudo.personality_score) >= applicant.stage_replace_id.min_personality_score and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_without_answers' or int(answer_sudo.skill_score) >= applicant.stage_replace_id.min_skills_score and int(answer_sudo.personality_score) >= applicant.stage_replace_id.min_personality_score and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_with_answers':
                    next_stage = applicant.job_id.stage_ids.filtered(lambda line:line.sequence == applicant.stage_replace_id.sequence + 1)
                    if next_stage and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_without_answers' and  int(answer_sudo.score_by_amount) > applicant.stage_replace_id.min_qualification or next_stage and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_with_answers' and  int(answer_sudo.score_by_amount) > applicant.stage_replace_id.min_qualification:
                        applicant.sudo().write({'stage_id': next_stage.stage_id.id})
                        applicant.sudo().message_post()
                else:
                    applicant.sudo().write({'refuse_reason_id': applicant.stage_id.refuse_reason_id.id,'active': False})
            elif applicant.stage_replace_id.survey_id and not applicant.stage_replace_id.interview_id:
                if  int(answer_sudo.score_by_amount) < applicant.stage_replace_id.min_qualification and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_without_answers' or int(answer_sudo.score_by_amount)  < applicant.stage_replace_id.min_qualification and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_with_answers':
                    applicant.sudo().write({'refuse_reason_id': applicant.stage_id.refuse_reason_id.id, 'active': False})
                else:
                    next_stage = applicant.job_id.stage_ids.filtered(lambda line:line.sequence == applicant.stage_replace_id.sequence + 1)
                    if next_stage and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_without_answers' and  int(answer_sudo.score_by_amount) > applicant.stage_replace_id.min_qualification or next_stage and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_with_answers' and  int(answer_sudo.score_by_amount) > applicant.stage_replace_id.min_qualification:
                        applicant.sudo().write({'stage_id': next_stage.stage_id.id})
                        applicant.sudo().message_post()
            else:
                next_stage = applicant.job_id.stage_ids.filtered(lambda line:line.sequence == applicant.stage_replace_id.sequence + 1)
                if next_stage and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_without_answers' and  int(answer_sudo.score_by_amount) > applicant.stage_replace_id.min_qualification or next_stage and not next_page and answer_sudo.survey_id.scoring_type == 'scoring_with_answers' and  int(answer_sudo.score_by_amount) > applicant.stage_replace_id.min_qualification:
                    applicant.sudo().write({'stage_id': next_stage.stage_id.id})
                    applicant.sudo().message_post()

        if str(answer_sudo.survey_type).upper() == 'DISC':
            self.disc_add(answer_sudo)
                
        return self._prepare_question_html(survey_sudo, answer_sudo)

    
    
    def disc_add(self,answer_sudo):
        disc_row_id = []
        question_id = []
        question_m_id = []
        question_l_id = []
        for data in answer_sudo.survey_id.question_and_page_ids:
            disc_row_id.append(data.id)
        if disc_row_id:
            for page in disc_row_id:
                cek = [answer_line for answer_line in answer_sudo.user_input_line_ids.filtered(lambda line:line.question_id.id == page and line.suggested_answer_id.value == "*" )]
                cek_m = [answer_line for answer_line in answer_sudo.user_input_line_ids.filtered(lambda line:line.question_id.id == page and line.suggested_answer_id.value == "M" )]
                cek_l = [answer_line for answer_line in answer_sudo.user_input_line_ids.filtered(lambda line:line.question_id.id == page and line.suggested_answer_id.value == "L" )]
                if len(cek)>3:
                    for line in cek:
                        question_id.append(line.question_id.id)
                        line.unlink()

                else:
                    for line in cek:
                        line.unlink()
                if cek_m and len(cek) ==3:
                    for line in cek_m:
                        question_m_id.append(line.question_id.id)
                if cek_l and len(cek) ==3:
                    for line in cek_l:
                        question_l_id.append(line.question_id.id)
            if question_id:
                for id in set(question_id):
                    answer_q = request.env['survey.question.answer'].sudo().search([('value','=','M')],limit=1)
                    request.env['survey.user_input.line'].sudo().create({'user_input_id':answer_sudo.id,'suggested_answer_id':answer_q.id,'answer_type':'suggestion','question_id':id,'is_skip':True})
                    answer_ql = request.env['survey.question.answer'].sudo().search([('value', '=', 'L')], limit=1)
                    request.env['survey.user_input.line'].sudo().create(
                        {'user_input_id': answer_sudo.id, 'suggested_answer_id': answer_ql.id, 'answer_type': 'suggestion',
                            'question_id': id,'is_skip':True})
            if question_m_id:
                for id in set(question_m_id):
                    answer_ql = request.env['survey.question.answer'].sudo().search([('value', '=', 'L')], limit=1)
                    request.env['survey.user_input.line'].sudo().create(
                        {'user_input_id': answer_sudo.id, 'suggested_answer_id': answer_ql.id, 'answer_type': 'suggestion',
                            'question_id': id,'is_skip':True})
            if question_l_id:
                for id in set(question_l_id):
                    answer_ql = request.env['survey.question.answer'].sudo().search([('value', '=', 'M')], limit=1)
                    request.env['survey.user_input.line'].sudo().create(
                        {'user_input_id': answer_sudo.id, 'suggested_answer_id': answer_ql.id, 'answer_type': 'suggestion',
                            'question_id': id,'is_skip':True})


    @http.route('/survey/start/<string:survey_token>', type='http', auth='public', website=True)
    def survey_start(self, survey_token, answer_token=None, email=False, surveyId=None,applicantId=None,jobPosition=None,trainingId=None,employeeId=None, **post):
        """ Start a survey by providing
         * a token linked to a survey;
         * a token linked to an answer or generate a new token if access is allowed;
        """
        # Get the current answer token from cookie
        if not answer_token:
            answer_token = request.httprequest.cookies.get('survey_%s' % survey_token)

        access_data = self._get_access_data(survey_token, answer_token, ensure_token=False)
        if access_data['validity_code'] is not True:
            return self._redirect_with_error(access_data, access_data['validity_code'])

        survey_sudo, answer_sudo = access_data['survey_sudo'], access_data['answer_sudo']
        if not answer_sudo:
            try:
                answer_sudo = survey_sudo._create_answer(user=request.env.user, email=email)
            except UserError:
                answer_sudo = False

        if not answer_sudo:
            try:
                survey_sudo.with_user(request.env.user).check_access_rights('read')
                survey_sudo.with_user(request.env.user).check_access_rule('read')
            except:
                return werkzeug.utils.redirect("/")
            else:
                return request.render("survey.survey_403_page", {'survey': survey_sudo})
        if surveyId and applicantId and jobPosition:
            query_statement = """SELECT id FROM survey_user_input WHERE job_id = %s AND survey_id = %s and applicant_id = %s AND is_use IS TRUE"""
            request.env.cr.execute(query_statement, [jobPosition,surveyId,applicantId])
            hr_applicant_query = request._cr.dictfetchone()
            if hr_applicant_query:
                return request.render("equip3_hr_recruitment_extend.test_completed_page", {'survey': survey_sudo})
            return request.redirect('/survey/%s/%s?surveyId=%s&applicantId=%s&jobPosition=%s' % (survey_sudo.access_token, answer_sudo.access_token,surveyId,applicantId,jobPosition))
        # if surveyId and trainingId and employeeId:
        #     return request.redirect('/survey/%s/%s?surveyId=%s&trainingId=%s&employeeId=%s' % (survey_sudo.access_token, answer_sudo.access_token,surveyId,trainingId,employeeId))

        return request.redirect('/survey/%s/%s' % (survey_sudo.access_token, answer_sudo.access_token))




    