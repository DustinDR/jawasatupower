from odoo.addons.survey.controllers.main import Survey


class SurveyDisc(Survey):
    
    
    def _prepare_question_html(self, survey_sudo, answer_sudo, **post):
        res = super(SurveyDisc, self)._prepare_question_html(survey_sudo,answer_sudo,**post)
        if answer_sudo.state == 'done' and str(answer_sudo.survey_type).upper() == 'DISC':
            answer_sudo.generate()

        return res