# -*- coding: utf-8 -*-
{
    'name': "Equip3 Construction Sale Operation",

    'summary': """
        This module to manage all operation of crm and sales in construction""",

    'description': """
        This module manages these features :
        - CRM Leads
        - Job Estimates
        - Variable Estimation
        - Quotation
    """,

    'author': "Hashmicro",
    'website': "http://www.hashmicro.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Construction',
    'version': '1.1.31',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail', 'crm', 'branch', 'web_digital_sign', 'sale_crm', 'project', 'equip3_construction_masterdata', 
                'equip3_crm_operation', 'equip3_sale_operation','bi_job_cost_estimate_customer', 
                'sale', 'utm', 'account', 'payment','equip3_construction_accessright_setting' ],
    
    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/res_setting_config_view.xml',
        'views/project_opportunity_view.xml',
        'views/menu_item_const.xml',
        'views/job_estimates_view.xml',
        'views/approval_matrix_job_estimate_view.xml',
        'views/sale_order_const_view.xml',
        'data/job_sequence_data.xml',
        'data/sale_order_sequence.xml',
        'views/construction_sale_css_assets.xml',
        'views/job_estimates_css.xml',
        'views/approval_matrix_sale_order_view.xml',
        'wizard/variation_order_confirm_wiz_view.xml',
        'wizard/approval_matrix_sale_reject_view.xml',
        'wizard/approval_matrix_job_reject_view.xml',

    ],
}
