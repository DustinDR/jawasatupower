# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class ProjectProjectInheritx(models.Model):
    _inherit = 'project.project'
    _order = 'id DESC'
    
    project_scope_line_ids = fields.One2many('project.scope.line', 'project_id', 'sequence')
    section_ids = fields.One2many('section.line', 'project_id')
    lead_id = fields.Many2one('crm.lead', string='Lead ref')

class ProjectScopeInheritx(models.Model):
    _inherit = 'project.scope.line'

    @api.depends('project_id.project_scope_line_ids', 'project_id.project_scope_line_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.project_id.project_scope_line_ids:
                no += 1
                l.sr_no = no

class SectionInheritx(models.Model):
    _inherit = 'section.line'

    @api.depends('project_id.section_ids', 'project_id.section_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.project_id.section_ids:
                no += 1
                l.sr_no = no

    @api.depends('project_id.project_scope_line_ids')
    def get_project_scopes(self):
        for rec in self:
            if rec.project_id.project_scope_line_ids:
                rec.project_scope_computed = [(6,0,rec.project_id.project_scope_line_ids.ids)]
            else:
                rec.project_scope_computed = [(6,0,[])]

class Job(models.Model):
    _inherit = 'job.estimate'

    lead_id = fields.Many2one('crm.lead', string='Lead')


class OpportunityDetails(models.Model):
    _inherit = 'crm.lead'

    @api.constrains('project_id')
    def _check_existing_project(self):
        for record in self:
            name_id = self.env['project.project'].search(
                [('name', '=', record.project_id)])
            if len(name_id) > 1:
                raise ValidationError(
                    f'The Project name already exists. Please change the Project name.')

    def _compute_job_count(self):
        for rec in self:
            rec.job_count = len(self.env['job.estimate'].search([('lead_id', '=', self.id)]))

    def _compute_projects_count(self):
        for rec in self:
            rec.project_count = len(self.env['project.project'].search([('lead_id', '=', self.id)]))

    job_count = fields.Integer('Job Count', compute='_compute_job_count')
    project_count = fields.Integer('Job Count', compute='_compute_projects_count')

    def action_set_won_rainbowman(self):
        list_project = []

        for crm_line in self.project_scope_ids:
            list_project.append(
                (0, 0, {'name': crm_line.name, 'scope_description': crm_line.scope_description}))

        for rec in self:
            name = rec.project_id

        for part in self:
            customer = part.partner_id.id
            salesperson = part.user_id.id
            team = part.team_id.id

        vals = {
            'name': name,
            'project_scope_line_ids': list_project,
            'partner_id': customer,
            'sales_person' : salesperson,
            'sales_team' : team,
            'primary_states': 'progress',
            'lead_id': self.id,
        }
        res = super(OpportunityDetails, self).action_set_won_rainbowman()
        if self.project_count == 0:
            self.env['project.project'].create(vals)
        else:
            project = self.env['project.project'].search([('lead_id', '=', self.id)], limit=1)
            if project:
                project.primary_states='progress'


        return res

    def action_job_new(self):
        list_project = []
        project = False
        for crm_line in self.project_scope_ids:
            list_project.append(
                (0, 0, {'name': crm_line.name, 'scope_description': crm_line.scope_description})
            )
            
        for rec in self:
            project = rec.project_id
        for part in self:
            customer = part.partner_id.id
            salesperson = part.user_id.id
            team = part.team_id.id

        vals = {
            'name': project,
            'project_scope_line_ids': list_project,
            'partner_id': customer,
            'sales_person' : salesperson,
            'sales_team' : team,
            'lead_id': self.id
        }
        if self.project_count == 0:
            project = self.env['project.project'].create(vals)
        else:
            project = self.env['project.project'].search([('lead_id', '=', self.id)], limit=1)

        action = self.env["ir.actions.actions"]._for_xml_id(
            "equip3_construction_sales_operation.action_job_estimate_new")
        action['context'] = {
            'default_lead_id': self.id,
            'default_partner_id': self.partner_id.id,
            'default_project_id': project.id,
        }
        return action

    def action_job_estimate(self):
        action = self.env["ir.actions.actions"]._for_xml_id(
            "bi_job_cost_estimate_customer.action_job_estimate")
        job_ids = self.env['job.estimate'].search([('lead_id', '=', self.id)])
        action['domain'] = [('id', 'in', job_ids.ids)]

        return action

    def action_project_counts(self):
        action = self.env["ir.actions.actions"]._for_xml_id(
            "project.open_view_project_all")
        projs_ids = self.env['project.project'].search([('lead_id', '=', self.id)])
        action['domain'] = [('id', 'in', projs_ids.ids)]
        return action

    project_id = fields.Char(String='Project Name', tracking=True)
    project_scope_ids = fields.One2many('project.scope.line.ids', 'sequence', string='Project Scope', required=True,
                                    tracking=True, ondelete='cascade')

    @api.constrains('project_scope_ids')
    def _check_exist_project_scope(self):
        for scope in self:
            exist_scope_list = []
            for line in scope.project_scope_ids:
                if line.name in exist_scope_list:
                    raise ValidationError(_('The Project Scope "%s" already exists. Please change the Project Scope "%s" (must be unique).'%((line.name),(line.name))))
                exist_scope_list.append(line.name)
      

class ProjectScopeLine(models.Model):
    _name = 'project.scope.line.ids'
    _order = 'sequence'

    sequence = fields.Integer(string="Sequence", default=0, readonly=True)
    name = fields.Char(String='Project Scope', required=True)
    scope_description = fields.Text(String='Description')
