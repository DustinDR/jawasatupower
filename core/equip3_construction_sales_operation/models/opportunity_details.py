# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError

class Job(models.Model):
    _inherit = 'job.estimate'

    lead_id = fields.Many2one('crm.lead', string='Lead')


class OpportunityDetails(models.Model):
    # _name = 'opportunity.details'
    _inherit = 'crm.lead'

    def _compute_job_count(self):
        for rec in self:
            rec.job_count = len(self.env['job.estimate'].search([('lead_id', '=', self.id)]))

    job_count = fields.Integer('Job Count', compute='_compute_job_count')

    def action_set_won_rainbowman(self):
        list_project = []
        for crm_line in self.project_scope:
            list_project.append((0, 0, {'project_scope': crm_line.project_scope_name, 'description': crm_line.scope_description}))

        for rec in self:
            name = rec.project_name
        vals = {
            'name': name,
            'project_scope_handle_ids': list_project
        }
        res = super(OpportunityDetails, self).action_set_won_rainbowman()
        self.env['project.project'].create(vals)
        return res

    def action_job_new(self):
        action = self.env["ir.actions.actions"]._for_xml_id("equip3_construction_sales_operation.action_job_estimate_new")
        action['context'] = {
            'default_lead_id': self.id,
        }
        return action

    def action_job_estimate(self):
        action = self.env["ir.actions.actions"]._for_xml_id(
            "bi_job_cost_estimate_customer.action_job_estimate")
        job_ids = self.env['job.estimate'].search([('lead_id', '=', self.id)])
        action['domain'] = [('id', 'in', job_ids.ids)]
        return action


    project_name = fields.Char(String = 'Project Name', required=True)
    project_scope = fields.One2many(comodel_name = 'project.scope.line', inverse_name = 'psl_id', string='Project Scope', required=True, ondelete='cascade')
    
    
class ProjectScopeLine(models.Model):
    _name = 'project.scope.line'
    _order = 'sequence'

    psl_id = fields.Many2one(comodel_name = 'crm.lead', string='Project Scope Line ID', required=True, ondelete='cascade')
    project_scope_name = fields.Char(String = 'Name', required=True)
    scope_description = fields.Char(String = 'Description')
    sequence = fields.Integer(string="sequence", default=0)