# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class VariableTemplate(models.Model):
    _name = 'variable.template'
    _rec_name = "variable_name"

    @api.depends('material_variable_ids.subtotal', 'labour_variable_ids.subtotal', 'subcon_variable_ids.subtotal', 'overhead_variable_ids.subtotal', 'equipment_variable_ids.subtotal')
    def _calculate_total(self):
        total_job_cost = 0.0
        for order in self:
            if order.material_variable_ids : 
                for line in order.material_variable_ids:
                    material_price =  (line.quantity * line.unit_price)
                    order.total_variable_material += material_price
                    total_job_cost += material_price

            else : 

                order.total_variable_material = 0

            if order.labour_variable_ids :
                for line in order.labour_variable_ids:
                    labour_price =  (line.quantity * line.unit_price) 
                    order.total_variable_labour += labour_price
                    total_job_cost += labour_price
            else :

                order.total_variable_labour = 0

            if order.overhead_variable_ids: 
                for line in order.overhead_variable_ids:
                    overhead_price =  (line.quantity * line.unit_price) 
                    order.total_variable_overhead += overhead_price
                    total_job_cost += overhead_price

            else :

                order.total_variable_overhead = 0
            
            if order.subcon_variable_ids :
                for line in order.subcon_variable_ids:
                    subcon_price =  (line.quantity * line.unit_price) 
                    order.total_variable_subcon += subcon_price
                    total_job_cost += subcon_price
            else :

                order.total_variable_subcon = 0

            if order.equipment_variable_ids :
                for line in order.equipment_variable_ids:
                    equipment_price =  (line.quantity * line.unit_price) 
                    order.total_variable_equipment += equipment_price
                    total_job_cost += equipment_price
            else :

                order.total_variable_equipment = 0

            order.total_variable = total_job_cost

        return

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id
            line.company_id = res_user_id.company_id

    
    variable_name = fields.Char(string= "Variable Name", required=True,)
    variable_uom = fields.Many2one('uom.uom', required=True, string="Unit of Measure")
    total_variable = fields.Monetary(compute='_calculate_total', string="Total Variable Estimates", default=0.0, readonly=True)
    company_currency_id = fields.Many2one('res.currency', related='company_id.currency_id')
    material_variable_ids = fields.One2many('material.estimate.variable', 'material_id', string="Material Estimation")
    labour_variable_ids = fields.One2many('labour.estimate.variable', 'labour_id', string="Labour Estimation")
    subcon_variable_ids = fields.One2many('subcon.estimate.variable', 'subcon_id', string="Subcon Estimation")
    overhead_variable_ids = fields.One2many('overhead.estimate.variable', 'overhead_id', string="Overhead Estimation ")
    equipment_variable_ids = fields.One2many('equipment.estimate.variable', 'equipment_id', string="Equipment Estimation")
    total_variable_material = fields.Monetary(compute='_calculate_total', string="Total Material Estimate", default=0.0, readonly=True)
    total_variable_labour = fields.Monetary(compute='_calculate_total', string="Total Labour Estimate", default=0.0, readonly=True)
    total_variable_subcon = fields.Monetary(compute='_calculate_total', string="Total Subcon Estimate", default=0.0, readonly=True)
    total_variable_overhead = fields.Monetary(compute='_calculate_total', string="Total Overhead Estimate", default=0.0, readonly=True)
    total_variable_equipment = fields.Monetary(compute='_calculate_total', string="Total Equipment Estimate", default=0.0, readonly=True)
    company_id = fields.Many2one('res.company','Company', compute='get_currency_id')
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    

class MaterialEstimation(models.Model):
    _name = 'material.estimate.variable'
    _order = 'sequence'

    @api.onchange('product_id')
    def onchange_product_id(self):
        res = {}
        if not self.product_id:
            return res
        self.uom_id = self.product_id.uom_id.id
        self.quantity = 1.0
        self.unit_price = self.product_id.list_price
         
    @api.onchange('product_id')
    def compute_category(self):
        if self.product_id:
            self.categ_id = self.product_id.categ_id[0].id

    @api.depends('material_id.material_variable_ids', 'material_id.material_variable_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.material_id.material_variable_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price =  (line.quantity * line.unit_price) 
            line.subtotal = price

    @api.model
    def create(self, vals):
        vals['sr_no'] = self.env['ir.sequence'].next_by_code('material_variable_product') or _('New')
        res = super(MaterialEstimation, self).create(vals)
        return res
    

    material_id = fields.Many2one('variable.template', string="Material ID")
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    categ_id = fields.Many2one('product.category', 'Internal Category', compute='compute_category', readonly=True)
    product_id = fields.Many2one('product.template', required=True)
    description = fields.Text(string="Description")
    quantity = fields.Float(string="Quantity", required=True)
    uom_id = fields.Many2one('uom.uom', readonly=1, string="Unit of Measure")
    unit_price = fields.Float(string="Unit Price")
    subtotal = fields.Monetary(readonly=True, string="Subtotal")
    currency_id = fields.Many2one('res.currency', compute='get_currency_id', string="Currency")

class LabourEstimation(models.Model):
    _name = 'labour.estimate.variable'
    _order = 'sequence'

    @api.onchange('product_id')
    def onchange_product_id(self):
        res = {}
        if not self.product_id:
            return res
        self.uom_id = self.product_id.uom_id.id
        self.quantity = 1.0
        self.unit_price = self.product_id.list_price
         
    @api.onchange('product_id')
    def compute_category(self):
        if self.product_id:
            self.categ_id = self.product_id.categ_id[0].id

    @api.depends('labour_id.labour_variable_ids', 'labour_id.labour_variable_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.labour_id.labour_variable_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price =  (line.quantity * line.unit_price) 
            line.subtotal = price

    @api.model
    def create(self, vals):
        vals['sr_no'] = self.env['ir.sequence'].next_by_code('labour_variable_product') or _('New')
        res = super(LabourEstimation, self).create(vals)
        return res

    labour_id = fields.Many2one('variable.template', string="Labour ID")
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    categ_id = fields.Many2one('product.category', 'Internal Category', compute='compute_category', readonly=True)
    product_id = fields.Many2one('product.template', required=True)
    description = fields.Text(string="Description")
    quantity = fields.Float(string="Quantity")
    uom_id = fields.Many2one('uom.uom', readonly=1, string="Unit of Measure")
    unit_price = fields.Float(string="Unit Price")
    subtotal = fields.Monetary(readonly=True, string="Subtotal")
    currency_id = fields.Many2one('res.currency', compute='get_currency_id', string="Currency")
    
class SubconEstimation(models.Model):
    _name = 'subcon.estimate.variable'
    _order = 'sequence'

    @api.onchange('product_id')
    def onchange_product_id(self):
        res = {}
        if not self.product_id:
            return res
        self.uom_id = self.product_id.uom_id.id
        self.quantity = 1.0
        self.unit_price = self.product_id.list_price
         
    @api.onchange('product_id')
    def compute_category(self):
        if self.product_id:
            self.categ_id = self.product_id.categ_id[0].id

    @api.depends('subcon_id.subcon_variable_ids', 'subcon_id.subcon_variable_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.subcon_id.subcon_variable_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price =  (line.quantity * line.unit_price) 
            line.subtotal = price

    @api.model
    def create(self, vals):
        vals['sr_no'] = self.env['ir.sequence'].next_by_code('subcon_variable_product') or _('New')
        res = super(SubconEstimation, self).create(vals)
        return res

    subcon_id = fields.Many2one('variable.template', string="Subcon ID")
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    categ_id = fields.Many2one('product.category', 'Internal Category', compute='compute_category', readonly=True)
    product_id = fields.Many2one('product.template', required=True)
    description = fields.Text(string="Description")
    quantity = fields.Float(string="Quantity")
    uom_id = fields.Many2one('uom.uom', readonly=1, string="Unit of Measure")
    unit_price = fields.Float(string="Unit Price")
    subtotal = fields.Monetary(readonly=True, string="Subtotal")
    currency_id = fields.Many2one('res.currency', compute='get_currency_id', string="Currency")
    
class OverheadEstimation(models.Model):
    _name = 'overhead.estimate.variable'
    _order = 'sequence'

    @api.onchange('product_id')
    def onchange_product_id(self):
        res = {}
        if not self.product_id:
            return res
        self.uom_id = self.product_id.uom_id.id
        self.quantity = 1.0
        self.unit_price = self.product_id.list_price
         
    @api.onchange('product_id')
    def compute_category(self):
        if self.product_id:
            self.categ_id = self.product_id.categ_id[0].id

    @api.depends('overhead_id.overhead_variable_ids', 'overhead_id.overhead_variable_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.overhead_id.overhead_variable_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price =  (line.quantity * line.unit_price) 
            line.subtotal = price

    @api.model
    def create(self, vals):
        vals['sr_no'] = self.env['ir.sequence'].next_by_code('overhead_variable_product') or _('New')
        res = super(OverheadEstimation, self).create(vals)
        return res
    
    overhead_id = fields.Many2one('variable.template', string="Overhead ID")
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    categ_id = fields.Many2one('product.category', 'Internal Category', compute='compute_category', readonly=True)
    product_id = fields.Many2one('product.template', required=True)
    description = fields.Text(string="Description")
    quantity = fields.Float(string="Quantity")
    uom_id = fields.Many2one('uom.uom', readonly=1, string="Unit of Measure")
    unit_price = fields.Float(string="Unit Price")
    subtotal = fields.Monetary(readonly=True, string="Subtotal")
    currency_id = fields.Many2one('res.currency', compute='get_currency_id', string="Currency")
    
class EquipmentEstimation(models.Model):
    _name = 'equipment.estimate.variable'
    _order = 'sequence'

    @api.onchange('product_id')
    def onchange_product_id(self):
        res = {}
        if not self.product_id:
            return res
        self.uom_id = self.product_id.uom_id.id
        self.quantity = 1.0
        self.unit_price = self.product_id.list_price
         
    @api.onchange('product_id')
    def compute_category(self):
        if self.product_id:
            self.categ_id = self.product_id.categ_id[0].id

    @api.depends('equipment_id.equipment_variable_ids', 'equipment_id.equipment_variable_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.equipment_id.equipment_variable_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price =  (line.quantity * line.unit_price) 
            line.subtotal = price

    @api.model
    def create(self, vals):
        vals['sr_no'] = self.env['ir.sequence'].next_by_code('equipment_variable_product') or _('New')
        res = super(EquipmentEstimation, self).create(vals)
        return res
    
    equipment_id = fields.Many2one('variable.template', string="Equipment ID")
    sequence = fields.Integer(string="Sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    categ_id = fields.Many2one('product.category', 'Internal Category', compute='compute_category', readonly=True)
    product_id = fields.Many2one('product.template', required=True)
    description = fields.Text(string="Description")
    quantity = fields.Float(string="Quantity")
    uom_id = fields.Many2one('uom.uom', readonly=1, string="Unit of Measure")
    unit_price = fields.Float(string="Unit Price")
    subtotal = fields.Monetary(readonly=True, string="Subtotal")
    currency_id = fields.Many2one('res.currency', compute='get_currency_id', string="Currency")