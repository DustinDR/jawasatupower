from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning

class ApprovalMatrixJobEstimates(models.Model):
    _name = 'approval.matrix.job.estimates'
    _description = "Approval Matrix Job Estimates"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']

    name = fields.Char(string="Name", required=True, tracking=True)
    company_id = fields.Many2one('res.company', string="Company", required=True, tracking=True, readonly=True, default=lambda self: self.env.company.id)
    branch_id = fields.Many2one('res.branch', string="Branch", required=True, domain="[('company_id', '=', company_id)]", tracking=True, default=lambda self: self.env.user.branch_id.id)
    project_id = fields.Many2one('project.project', string='Project', required=True, domain="[('company_id', '=', company_id)]")
    minimum_amt = fields.Float(string="Minimum Amount", tracking=True, required=True)
    maximum_amt = fields.Float(string="Maximum Amount", tracking=True, required=True)
    approver_matrix_line_ids = fields.One2many('approval.matrix.job.estimates.line', 'approval_matrix', string="Approver Name")

    @api.constrains('project_id', 'branch_id', 'minimum_amt', 'maximum_amt')
    def _check_existing_record(self):
        for record in self:
            if record.project_id:
                if record.branch_id:
                    approval_matrix_id = self.search([('project_id', '=', record.project_id.id),
                                                      ('branch_id', '=', record.branch_id.id), 
                                                      ('id', '!=', record.id),
                                                      '|', '|',
                                                      '&', ('minimum_amt', '<=', record.minimum_amt), ('maximum_amt', '>=', record.minimum_amt),
                                                      '&', ('minimum_amt', '<=', record.maximum_amt), ('maximum_amt', '>=', record.maximum_amt),
                                                      '&', ('minimum_amt', '>=', record.minimum_amt), ('maximum_amt', '<=', record.maximum_amt)], limit=1)
                    if approval_matrix_id:
                        raise ValidationError("The minimum and maximum range of this approval matrix is intersects with other approval matrix %s in same branch. Please change the minimum and maximum range" % (approval_matrix_id.name))

    def _reset_sequence(self):
        for rec in self:
            current_sequence = 1
            for line in rec.approver_matrix_line_ids:
                line.sequence = current_sequence
                current_sequence += 1

    def copy(self, default=None):
        res = super(ApprovalMatrixJobEstimates, self.with_context(keep_line_sequence=True)).copy(default)
        return res


class ApprovalMatrixJobEstimateLines(models.Model):
    _name = 'approval.matrix.job.estimates.line'
    _description = "Approval Matrix Job Estimate Lines"

    @api.model
    def default_get(self, fields):
        res = super(ApprovalMatrixJobEstimateLines, self).default_get(fields)
        if self._context:
            context_keys = self._context.keys()
            next_sequence = 1
            if 'approver_matrix_line_ids' in context_keys:
                if len(self._context.get('approver_matrix_line_ids')) > 0:
                    next_sequence = len(self._context.get('approver_matrix_line_ids')) + 1
            res.update({'sequence': next_sequence})
        return res

    approval_matrix = fields.Many2one('approval.matrix.job.estimates', srting='Approval Marix')
    order_id = fields.Many2one('job.estimate', string="Job Estimate")
    user_name_ids = fields.Many2many('res.users', string="Users", required=True)
    sequence = fields.Integer(required=True, index=True, help='Use to arrange calculation sequence', tracking=True)
    sequence2 = fields.Integer(
        string="No.",
        related="sequence",
        readonly=True,
        store=True,
        tracking=True
    )
    minimum_approver = fields.Integer(string="Minimum Approver", required=True, default=1)
    state_char = fields.Text(string='Approval Status')
    time_stamp = fields.Datetime(string='Timestamp')
    feedback = fields.Char(string='Rejected Reason')
    last_approved = fields.Many2one('res.users', string='Users')
    approved = fields.Boolean('Approved')
    approved_users = fields.Many2many('res.users', 'approved_users_job_partner_rel', 'order_id', 'user_id', string='Users')
    signature = fields.Binary(related="last_approved.digital_signature", string="Signature", store=True)

    def unlink(self):
        approval = self.approval_matrix
        res = super(ApprovalMatrixJobEstimateLines, self).unlink()
        approval._reset_sequence()
        return res

    @api.model
    def create(self, vals):
        res = super(ApprovalMatrixJobEstimateLines, self).create(vals)
        if not self.env.context.get("keep-line_sequence", False):
            res.approval_matrix._reset_sequence()
        return res