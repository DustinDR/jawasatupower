
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError, Warning

class ApprovalMatrixSaleOrderConst(models.Model):
    _name = 'approval.matrix.sale.order.const'
    _description = "Approval Matrix Sale Order"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']

    name = fields.Char(string='Name', tracking=True, required=True)
    project_id = fields.Many2one('project.project', string='Project', required=True, domain="[('company_id', '=', company_id)]")
    company_id = fields.Many2one("res.company", string="Company", tracking=True, required=True, readonly=True, default=lambda self: self.env.company.id)
    branch_id = fields.Many2one("res.branch", string="Branch", domain="[('company_id', '=', company_id)]", tracking=True, required=True, default=lambda self: self.env.user.branch_id.id)
    config = fields.Selection([
        ('contract_amt', 'Contract Amount'),
        ('adjustment_amt', 'Adjustment Amount'),
        ('discount_amt', 'Discount Amount'),
    ], 'Configuration', store=True, required=True, default="contract_amt", tracking=True)
    minimum_amt = fields.Float(string='Minimum Amount', required=True, tracking=True)
    maximum_amt = fields.Float(string='Maximum Amount', required=True, tracking=True)
    approver_matrix_line_ids = fields.One2many('approval.matrix.sale.order.lines.const', 'approval_matrix', string="Approver Name")


    @api.model
    def default_get(self, fields):
        res = super(ApprovalMatrixSaleOrderConst, self).default_get(fields)
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        sale_matrix_config_const =  IrConfigParam.get_param('sale_matrix_config_const', 'contract_amt')
        res['config'] = sale_matrix_config_const
        return res

    @api.constrains('project_id','branch_id', 'minimum_amt', 'maximum_amt')
    def _check_existing_record(self):
        for record in self:
            if record.project_id:
                if record.branch_id:
                    approval_matrix_id = self.search([('project_id', '=', record.project_id.id),
                                                      ('branch_id', '=', record.branch_id.id), 
                                                      ('id', '!=', record.id), 
                                                      ('config', '=', record.config),
                                                      '|', '|',
                                                      '&', ('minimum_amt', '<=', record.minimum_amt), ('maximum_amt', '>=', record.minimum_amt),
                                                      '&', ('minimum_amt', '<=', record.maximum_amt), ('maximum_amt', '>=', record.maximum_amt),
                                                      '&', ('minimum_amt', '>=', record.minimum_amt), ('maximum_amt', '<=', record.maximum_amt)], limit=1)
                    if approval_matrix_id:
                        raise ValidationError("The minimum and maximum range of this approval matrix is intersects with other approval matrix [%s] in same branch. Please change the minimum and maximum range" % (approval_matrix_id.name))

    def _reset_sequence(self):
        for rec in self:
            current_sequence = 1
            for line in rec.approver_matrix_line_ids:
                line.sequence = current_sequence
                current_sequence += 1
    
    def copy(self, default=None):
        res = super(ApprovalMatrixSaleOrderConst, self.with_context(keep_line_sequence=True)).copy(default)
        return res


class ApprovalMatrixSaleOrderLinesConst(models.Model):
    _name = 'approval.matrix.sale.order.lines.const'
    _description = "Approval Matrix Sale Order Lines"

    @api.model
    def default_get(self, fields):
        res = super(ApprovalMatrixSaleOrderLinesConst, self).default_get(fields)
        if self._context:
            context_keys = self._context.keys()
            next_sequence = 1
            if 'approver_matrix_line_ids' in context_keys:
                if len(self._context.get('approver_matrix_line_ids')) > 0:
                    next_sequence = len(self._context.get('approver_matrix_line_ids')) + 1
            res.update({'sequence': next_sequence})
        return res

    approval_matrix = fields.Many2one('approval.matrix.sale.order.const', srting='Approval Marix')
    order_id = fields.Many2one('sale.order.const', string="Sale Order")
    user_name_ids = fields.Many2many('res.users', string="Users", required=True)
    sequence = fields.Integer(required=True, index=True, help='Use to arrange calculation sequence', tracking=True)
    sequence2 = fields.Integer(
        string="No.",
        related="sequence",
        readonly=True,
        store=True,
        tracking=True
    )
    minimum_approver = fields.Integer(string="Minimum Approver", required=True, default=1)
    state_char = fields.Text(string='Approval Status')
    time_stamp = fields.Datetime(string='Timestamp')
    feedback = fields.Char(string='Rejected Reason')
    last_approved = fields.Many2one('res.users', string='Users')
    approved = fields.Boolean('Approved')
    approved_users = fields.Many2many('res.users', 'approved_users_sale_partner_rel', 'order_id', 'user_id', string='Users')
    signature = fields.Binary(related="last_approved.digital_signature", string="Signature", store=True)
    approval_type = fields.Selection([('contract_amt', 'Contract Amount'), ('adjustment_amt', 'Adjustment Amount'), ('discount_amt', 'Discount Amount')])

    def unlink(self):
        approval = self.approval_matrix
        res = super(ApprovalMatrixSaleOrderLinesConst, self).unlink()
        approval._reset_sequence()
        return res

    @api.model
    def create(self, vals):
        res = super(ApprovalMatrixSaleOrderLinesConst, self).create(vals)
        if not self.env.context.get("keep-line_sequence", False):
            res.approval_matrix._reset_sequence()
        return res
