# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime, timedelta, date

import re


class JobEstimate(models.Model):
    _name = 'job.estimate'
    _inherit = ['job.estimate', 'portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _description = "Job Estimates"
    _rec_name = "name"
    _order = 'id DESC'
    _check_company_auto = True

    def quotation_button(self):
        self.ensure_one()
        return {
            'name': 'Quotation',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'sale.order.const',
            'domain': [('job_reference', '=', self.id)],
        }

    def _get_quotation_count(self):
        for sale in self:
            if sale:
                try:
                    sale_ids = self.env['sale.order.const'].search([('job_reference', '=', sale.id)])
                    sale.internal_quotation_count = len(sale_ids)

                except:
                    sale.internal_quotation_count = 0
        return True

    def reset_draft(self):
        res = self.write({
            'state': 'draft',
            'state_new': 'draft'
        })
        return res

    def approve_job_estimate(self):
        res = self.write({
            'state': 'approved',
            'state_new': 'approved'
        })
        return res

    def reject_job_estimate(self):
        res = self.write({
            'state': 'rejected',
            'state_new': 'rejected',
            'state_1': 'rejected'
        })
        return res

    def action_quotation_send(self):
        self.ensure_one()
        self.write({
            'state': 'sent',
            'state_new': 'sent'
        })
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = \
            ir_model_data.get_object_reference('bi_job_cost_estimate_customer', 'job_estimate_email_template')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False

        ctx = {
            'default_model': 'job.estimate',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'force_email': True
        }
        return {
            'name': 'Compose Email',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    def create_quotation(self):
        ctx = self._context.copy()
        ctx['default_job_reference'] = self.id
        return {
            'name': _('Quotation Const'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'sale.order.const',
            'target': 'current',
            'context': ctx,
        }

    def approve_new_job_estimate(self):
        res = self.write({
            'state': 'approved',
            'state_new': 'approved'
        })
        return res

    def create_variation_order(self):
        pass

    @api.depends('section_ids', 'section_ids.project_scope')
    def get_section_lines(self):
        sections = []
        # self.section_scope_computed=[(5, 0, 0)]
        latest = 1
        last_job = self.env['job.estimate'].search([], order="id desc", limit=1)
        if last_job:
            latest = last_job.id + 1

        for rec in self.section_ids:
            # for Sections
            res = self.section_scope_computed.create({
                'project_scope': rec.project_scope.id,
                'section_name': rec.section_name,
                'latest': latest,
                'project_id': self.project_id.id

            })
            sections.append(res.id)

        self.sections_text = str(sections)

    @api.onchange('variable_ids', 'variable_ids.variable_name', 'variable_ids.variable_quantity')
    def update_material(self):
        material = [(5, 0, 0)]
        labour = [(5, 0, 0)]
        subcon = [(5, 0, 0)]
        overhead = [(5, 0, 0)]
        equip = [(5, 0, 0)]

        if len(self.material_estimation_ids) > 0:
            if len(self.material_estimation_ids.filtered(lambda r: not r.variable_ref)) > 0:
                material = []
                for mm in self.material_estimation_ids.filtered(lambda r: r.variable_ref):
                    mm.material_id = False

        if len(self.labour_estimation_ids) > 0:
            if len(self.labour_estimation_ids.filtered(lambda r: not r.variable_ref)) > 0:
                labour = []
                for mm in self.labour_estimation_ids.filtered(lambda r: r.variable_ref):
                    mm.labour_id = False

        if len(self.subcon_estimation_ids) > 0:
            if len(self.subcon_estimation_ids.filtered(lambda r: not r.variable_ref)) > 0:
                subcon = []
                for mm in self.subcon_estimation_ids.filtered(lambda r: r.variable_ref):
                    mm.subcon_id = False

        if len(self.overhead_estimation_ids) > 0:
            if len(self.overhead_estimation_ids.filtered(lambda r: not r.variable_ref)) > 0:
                overhead = []
                for mm in self.overhead_estimation_ids.filtered(lambda r: r.variable_ref):
                    mm.overhead_id = False

        if len(self.equipment_estimation_ids) > 0:
            if len(self.equipment_estimation_ids.filtered(lambda r: not r.variable_ref)) > 0:
                equip = []
                for mm in self.equipment_estimation_ids.filtered(lambda r: r.variable_ref):
                    mm.equipment_id = False

        for rec in self.variable_ids:
            # for material
            for mat in rec.variable_name.material_variable_ids:
                res = (0, 0, {
                    'product_id': mat.product_id.id,
                    'quantity': rec.variable_quantity * mat.quantity,
                    'subtotal': mat.unit_price * (rec.variable_quantity * mat.quantity),
                    'unit_price': mat.unit_price,
                    'uom_id': mat.uom_id,
                    'project_scope': rec.project_scope.id,
                    'section_name': rec.section_name.id,
                    'variable_ref': rec.variable_name.id,
                    'description': mat.description,
                    'analytic_idz': rec.analytic_idz,
                    'group_of_product': mat.group_of_product.id
                })
                material.append(res)
            # for labor
            for lab in rec.variable_name.labour_variable_ids:
                labx = (0, 0, {
                    'product_id': lab.product_id.id,
                    'quantity': rec.variable_quantity * lab.quantity,
                    'subtotal': lab.unit_price * (rec.variable_quantity * lab.quantity),
                    'unit_price': lab.unit_price,
                    'uom_id': lab.uom_id,
                    'project_scope': rec.project_scope.id,
                    'section_name': rec.section_name.id,
                    'variable_ref': rec.variable_name.id,
                    'description': lab.description,
                    'analytic_idz': rec.analytic_idz,
                    'group_of_product': lab.group_of_product.id
                })
                labour.append(labx)
            # for subcon
            for sub in rec.variable_name.subcon_variable_ids:
                subx = (0, 0, {
                    'variable': sub.variable.id,
                    'quantity': rec.variable_quantity * sub.quantity,
                    'subtotal': sub.unit_price * (rec.variable_quantity * sub.quantity),
                    'unit_price': sub.unit_price,
                    'uom_id': sub.uom_id,
                    'project_scope': rec.project_scope.id,
                    'section_name': rec.section_name.id,
                    'variable_ref': rec.variable_name.id,
                    'description': sub.description,
                    'analytic_idz': rec.analytic_idz,
                })
                subcon.append(subx)

            # for over
            for over in rec.variable_name.overhead_variable_ids:
                overx = (0, 0, {
                    'product_id': over.product_id.id,
                    'quantity': rec.variable_quantity * over.quantity,
                    'subtotal': over.unit_price * (rec.variable_quantity * over.quantity),
                    'unit_price': over.unit_price,
                    'uom_id': over.uom_id,
                    'project_scope': rec.project_scope.id,
                    'section_name': rec.section_name.id,
                    'variable_ref': rec.variable_name.id,
                    'description': over.description,
                    'analytic_idz': rec.analytic_idz,
                    'group_of_product': over.group_of_product.id
                })
                overhead.append(overx)

            # for equip
            for eqp in rec.variable_name.equipment_variable_ids:
                eqpx = (0, 0, {
                    'product_id': eqp.product_id.id,
                    'quantity': rec.variable_quantity * eqp.quantity,
                    'subtotal': eqp.unit_price * (rec.variable_quantity * eqp.quantity),
                    'unit_price': eqp.unit_price,
                    'uom_id': eqp.uom_id,
                    'project_scope': rec.project_scope.id,
                    'section_name': rec.section_name.id,
                    'variable_ref': rec.variable_name.id,
                    'description': eqp.description,
                    'analytic_idz': rec.analytic_idz,
                    'group_of_product': eqp.group_of_product.id
                })
                equip.append(eqpx)

        self.material_estimation_ids = material
        self.labour_estimation_ids = labour
        self.subcon_estimation_ids = subcon
        self.overhead_estimation_ids = overhead
        self.equipment_estimation_ids = equip

    @api.onchange('analytic_idz')
    def set_account_group_lines(self):
        for res in self:
            for line1 in res.material_estimation_ids:
                line1.analytic_idz = res.analytic_idz
            for line2 in res.labour_estimation_ids:
                line2.analytic_idz = res.analytic_idz
            for line3 in res.subcon_estimation_ids:
                line3.analytic_idz = res.analytic_idz
            for line4 in res.overhead_estimation_ids:
                line4.analytic_idz = res.analytic_idz
            for line5 in res.equipment_estimation_ids:
                line5.analytic_idz = res.analytic_idz
            for line6 in res.variable_ids:
                line6.analytic_idz = res.analytic_idz

    @api.depends('material_estimation_ids.subtotal', 'labour_estimation_ids.subtotal', 'subcon_estimation_ids.subtotal',
                 'overhead_estimation_ids.subtotal', 'equipment_estimation_ids.subtotal')
    def _calculate_total(self):
        total_job_cost = 0.0
        for order in self:
            if order.material_estimation_ids:
                for line in order.material_estimation_ids:
                    material_price = (line.quantity * line.unit_price)
                    order.total_material_estimate += material_price
                    total_job_cost += material_price

            else:

                order.total_material_estimate = 0

            if order.labour_estimation_ids:
                for line in order.labour_estimation_ids:
                    labour_price = (line.quantity * line.unit_price)
                    order.total_labour_estimate += labour_price
                    total_job_cost += labour_price
            else:

                order.total_labour_estimate = 0

            if order.overhead_estimation_ids:
                for line in order.overhead_estimation_ids:
                    overhead_price = (line.quantity * line.unit_price)
                    order.total_overhead_estimate += overhead_price
                    total_job_cost += overhead_price

            else:

                order.total_overhead_estimate = 0

            if order.subcon_estimation_ids:
                for line in order.subcon_estimation_ids:
                    subcon_price = (line.quantity * line.unit_price)
                    order.total_subcon_estimate += subcon_price
                    total_job_cost += subcon_price
            else:

                order.total_subcon_estimate = 0

            if order.equipment_estimation_ids:
                for line in order.equipment_estimation_ids:
                    equipment_price = (line.quantity * line.unit_price)
                    order.total_equipment_estimate += equipment_price
                    total_job_cost += equipment_price
            else:

                order.total_equipment_estimate = 0

        # order.total_job_estimate = total_job_cost
        return

    @api.constrains('start_date', 'end_date')
    def constrains_date(self):
        for rec in self:
            if rec.start_date != False and rec.end_date != False:
                if rec.start_date > rec.end_date:
                    raise UserError(_('End date should be after start date.'))

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id
            line.company_id = res_user_id.company_id

    @api.onchange('project_id')
    def onchange_project_id(self):
        lines = []
        self.project_scope_ids = [(5, 0, 0)]
        self.material_estimation_ids = [(5, 0, 0)]
        self.labour_estimation_ids = [(5, 0, 0)]
        self.subcon_estimation_ids = [(5, 0, 0)]
        self.overhead_estimation_ids = [(5, 0, 0)]
        self.equipment_estimation_ids = [(5, 0, 0)]

        self.section_ids = [(5, 0, 0)]
        self.variable_ids = [(5, 0, 0)]
        self.main_contract_ref = False
        # self.contract_category = None
        self.customer_ref = False
        self.start_date = False
        self.end_date = False
        self.analytic_idz = False
        self.sales_person_id = False
        self.sales_team_id = False
        self.partner_id = False
        if self.project_id:
            # existed = self.check_job_duplicate()
            project = self.project_id
            self.partner_id = project.partner_id
            self.customer_ref = project.customer_ref
            self.start_date = project.start_date
            self.end_date = project.end_date
            self.analytic_idz = project.analytic_idz
            self.sales_person_id = project.sales_person
            self.sales_team_id = project.sales_team
            for scope in project.project_scope_line_ids:
                self.project_scope_ids = [(0, 0, {
                    'project_scope': scope.id,
                    'description': scope.scope_description,
                })]

            for section in project.section_ids:
                self.section_ids = [(0, 0, {
                    'project_scope': section.project_scope.id,
                    'section_name': section.name,
                    'description': section.description,
                })]

    @api.model
    def _get_default_team(self):
        return self.env['crm.team']._get_default_team_id()

    def action_cost_sheet(self):
        # views = self.env.ref('abs_construction_management.view_job_cost_sheet_menu_form').id
        job_sheet = self.env['job.cost.sheet'].search([('project_id', '=', self.project_id.id)], limit=1)
        action = job_sheet.get_formview_action()
        # action = self.env["ir.actions.actions"]._for_xml_id("abs_construction_management.action_view_job_cost_sheet_menu")
        return action

    @api.constrains('contract_category')
    def contract_category_cahnge(self):
        if self.contract_category:
            if self.contract_category == 'var':
                self.name = self.env['ir.sequence'].next_by_code('job.sequence.vo')
            elif self.contract_category == 'main':
                self.name = self.env['ir.sequence'].next_by_code('job.sequence.new')

    def job_confirm(self):
        is_existed = self.search([('project_id', '=', self.project_id.id), ('state', '=', 'sale')])
        if is_existed and self.contract_category != 'var':
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'variation.order.warning',
                'name': _("Create Variation Order"),
                'target': 'new',
                'view_type': 'form',
                'view_mode': 'form',
                'context': {'res_id': self.id}
            }
        else:
            res = self.write({'state': 'confirmed',
                              'state_new': 'confirmed'
                              })
            return res

    @api.model
    def create(self, vals):
        vals['name'] = _('New')
        res = super(JobEstimate, self).create(vals)
        return res

    name = fields.Char(string='Number', required=True, copy=False, readonly=True,
                       states={'draft': [('readonly', True)]}, index=True, default=lambda self: _('New'))

    state = fields.Selection(selection_add=[
        ('rejected', 'rejected'),
        ('sale', 'Sale Order Created'),
        ('waiting_for_approve', 'Waiting For Approval')
    ], string='Status', default='draft')
    state_new = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('sent', 'Estimation Sent'),
        ('waiting_for_approve', 'Waiting For Approval'),
        ('approved', 'Approved'),
        ('rejected', 'rejected'),
        ('done', 'Quotation Created')
    ], string='Status', readonly=True, copy=False, index=True, tracking=True, default='draft')
    state_1 = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('sent', 'Estimation Sent'),
        ('rejected', 'rejected'),
    ], string='Status', default='draft')

    start_date = fields.Date(string="Start Date", tracking=True, readonly=True,
                             states={'draft': [('readonly', False)], 'sent': [('readonly', False)],
                                     'confirmed': [('readonly', False)]})
    end_date = fields.Date(string="End Date", tracking=True, readonly=True,
                           states={'draft': [('readonly', False)], 'sent': [('readonly', False)],
                                   'confirmed': [('readonly', False)]})
    partner_id = fields.Many2one(
        'res.partner', string='Customer', readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        required=True, change_default=True, index=True, tracking=1,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]", )
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True,
                                 default=lambda self: self.env.company)
    branch_id = fields.Many2one('res.branch', domain="[('company_id', '=', company_id)]", readonly=True,
                                states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    company_currency_id = fields.Many2one('res.currency', string='Currency', related='company_id.currency_id')
    analytic_idz = fields.Many2many('account.analytic.tag', string='Analytic Group',
                                    domain="[('company_id', '=', company_id)]", readonly=True,
                                    states={'draft': [('readonly', False)], 'sent': [('readonly', False)],
                                            'confirmed': [('readonly', False)]})
    project_id = fields.Many2one('project.project', string='Project', required=True, readonly=True,
                                 states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    add_note = fields.Text('Additional Notes', readonly=True,
                           states={'draft': [('readonly', False)], 'sent': [('readonly', False)],
                                   'confirmed': [('readonly', False)]})
    subcon_estimation_ids = fields.One2many('subcon.estimate', 'subcon_id', 'Subcon Estimation')
    equipment_estimation_ids = fields.One2many('equipment.estimate', 'equipment_id', 'Equipment Estimation')
    project_scope_ids = fields.One2many('project.scope.estimate', 'scope_id', 'Project Scope Estimation')
    section_ids = fields.One2many('section.estimate', 'section_id', 'Section Estimation')
    variable_ids = fields.One2many('variable.estimate', 'variable_id', 'Variable Estimation')
    description = fields.Text('Description')
    total_material_estimate = fields.Monetary(compute='_calculate_total', string='Total Material Estimate', default=0.0,
                                              readonly=True, tracking=True)
    total_labour_estimate = fields.Monetary(compute='_calculate_total', string='Total Labour Estimate', default=0.0,
                                            readonly=True, tracking=True)
    total_subcon_estimate = fields.Monetary(compute='_calculate_total', string='Total Subcon Estimate', default=0.0,
                                            store=False,
                                            readonly=True, tracking=True)
    total_overhead_estimate = fields.Monetary(compute='_calculate_total', string='Total Overhead Estimate', default=0.0,
                                              readonly=True, tracking=True)
    total_equipment_estimate = fields.Monetary(compute='_calculate_total', string='Total Equipment Estimate',
                                               default=0.0, readonly=True, tracking=True)
    total_job_estimate = fields.Monetary(string='Total Job Estimate', default=0.0,
                                         readonly=True, tracking=True)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    contract_category = fields.Selection([
        ('main', 'Main Contract'),
        ('var', 'Variation Order')
    ], string="Contract Category", default='main')
    main_contract_ref = fields.Many2one('sale.order.const', 'Main Contract', tracking=True,
                                        domain="[('state', '=', 'sale'), ('company_id', '=', company_id)]")
    cost_sheet_ref = fields.Many2one('job.cost.sheet', 'Cost Sheet', tracking=True,
                                     domain="[('state', '=', 'approved'), ('company_id', '=', company_id)]")
    section_scope_computed = fields.Many2many('section.estimate', string="computed section lines")
    sections_text = fields.Char(string="Sections", compute='get_section_lines')

    # Other Info
    sales_person_id = fields.Many2one(
        'res.users', string='Salesperson', index=True, tracking=2, default=lambda self: self.env.user,
        domain=lambda self: [('groups_id', 'in', self.env.ref('sales_team.group_sale_salesman').id)])
    sales_team_id = fields.Many2one(
        'crm.team', 'Sales Team',
        change_default=True, default=_get_default_team, check_company=True,  # Unrequired company
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")

    approval_matrix_state = fields.Selection(related='state_new', tracking=False)
    approval_matrix_state_1 = fields.Selection(related='state_new', tracking=False)
    approving_matrix_sale_id = fields.Many2one('approval.matrix.job.estimates', string="Approval Matrix",
                                               compute='_compute_approving_customer_matrix', store=True)
    approved_matrix_ids = fields.One2many('approval.matrix.job.estimates.line', 'order_id', store=True,
                                          string="Approved Matrix", compute='_compute_approving_matrix_lines')
    is_job_estimate_approval_matrix = fields.Boolean(string="Custome Matrix", store=False,
                                                     compute='_compute_is_customer_approval_matrix')
    is_approval_matrix_filled = fields.Boolean(string="Custome Matrix", store=False,
                                               compute='_compute_approval_matrix_filled')
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button', store=False)
    approval_matrix_line_id = fields.Many2one('approval.matrix.job.estimates.line', string='Sale Approval Matrix Line',
                                              compute='_get_approve_button', store=False)

    @api.depends('approving_matrix_sale_id')
    def _compute_approval_matrix_filled(self):
        for record in self:
            record.is_approval_matrix_filled = False
            if record.approving_matrix_sale_id:
                record.is_approval_matrix_filled = True

    def _get_approve_button(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved),
                                 key=lambda r: r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_name_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False

    @api.onchange('partner_id')
    def onchange_partner_id_new(self):
        self._compute_is_customer_approval_matrix()
        self._compute_approval_matrix_filled()
        default_branch = self.env.user.branch_id.id,
        self.branch_id = default_branch

    @api.onchange('name')
    def _onchange_sale_name(self):
        self._compute_is_customer_approval_matrix()
        self._compute_approval_matrix_filled()

    @api.depends('approving_matrix_sale_id')
    def _compute_approving_matrix_lines(self):
        data = [(5, 0, 0)]
        for record in self:
            if record.state_new == 'draft' and record.is_job_estimate_approval_matrix:
                record.approved_matrix_ids = []
                counter = 1
                record.approved_matrix_ids = []
                for rec in record.approving_matrix_sale_id:
                    for line in rec.approver_matrix_line_ids:
                        data.append((0, 0, {
                            'sequence': counter,
                            'user_name_ids': [(6, 0, line.user_name_ids.ids)],
                            'minimum_approver': line.minimum_approver,
                        }))
                        counter += 1
                record.approved_matrix_ids = data

    @api.depends('partner_id')
    def _compute_is_customer_approval_matrix(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        is_job_estimate_approval_matrix = IrConfigParam.get_param('is_job_estimate_approval_matrix')
        for record in self:
            record.is_job_estimate_approval_matrix = is_job_estimate_approval_matrix

    @api.depends('project_id','branch_id','company_id','total_job_estimate')
    def _compute_approving_customer_matrix(self):
        for record in self:
            record.approving_matrix_sale_id = False
            if record.is_job_estimate_approval_matrix:
                approving_matrix_sale_id = self.env['approval.matrix.job.estimates'].search([
                    ('company_id', '=', record.company_id.id),
                    ('branch_id', '=', record.branch_id.id), 
                    ('project_id', '=', record.project_id.id),
                    ('minimum_amt', '<=', record.total_job_estimate),
                    ('maximum_amt', '>=', record.total_job_estimate)], limit=1)
                record.approving_matrix_sale_id = approving_matrix_sale_id and approving_matrix_sale_id.id or False

    def action_confirm_approving(self):
        for record in self:
            record.write({'state': 'approved',
                          'state_new': 'approved'
                          })

    def action_request_for_approving(self):
        for record in self:
            record.write({'state': 'waiting_for_approve',
                          'state_new': 'waiting_for_approve'
                          })

    def action_confirm_approving_matrix(self):
        for record in self:
            user = self.env.user
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_name_ids.ids and \
                        user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    if name != '':
                        name += "\n • %s: Approved" % (self.env.user.name)
                    else:
                        name += "• %s: Approved" % (self.env.user.name)

                    approval_matrix_line_id.write({
                        'last_approved': self.env.user.id, 'state_char': name,
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})

            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r: r.approved)):
                record.write({'state': 'approved',
                              'state_new': 'approved'
                              })

    def action_reject_approving_matrix(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Reject Reason',
            'res_model': 'approval.matrix.job.reject.const',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
        }

    def action_set_draft(self):
        for record in self:
            record.approved_matrix_ids.write({
                'approved_users': False,
                'last_approved': False,
                'approved': False,
                'feedback': False,
                'time_stamp': False,
                'state_char': False,
            })
            record.write({'state': 'draft',
                          'state_new': 'draft'
                          })

    @api.constrains('project_scope_ids')
    def _check_exist_project_scope(self):
        exist_scope_list = []
        for line in self.project_scope_ids:
            if line.project_scope.id in exist_scope_list:
                raise ValidationError(_('The Project Scope "%s" already exists. Please change the Project Scope "%s" (must be unique).'%((line.project_scope.name),(line.project_scope.name))))
            exist_scope_list.append(line.project_scope.id)
    
    @api.onchange('project_scope_ids')
    def _check_exist_project_scope(self):
        exist_scope_list = []
        for line in self.project_scope_ids:
            if line.project_scope.id in exist_scope_list:
                raise ValidationError(_('The Project Scope "%s" already exists. Please change the Project Scope "%s" (must be unique).'%((line.project_scope.name),(line.project_scope.name))))
            exist_scope_list.append(line.project_scope.id)

    @api.onchange('section_ids')
    def _check_exist_section(self):
        exist_section_list = []
        for line in self.section_ids:
            if line.section_name in exist_section_list:
                raise ValidationError(_('The Section "%s" already exists. Please change the Section "%s" (must be unique).'%((line.section_name),(line.section_name))))
            exist_section_list.append(line.section_name)

    @api.onchange('material_estimation_ids')
    def _check_exist_group_of_product_material(self):
        exist_group_list = []
        section_list =[]
        for line in self.material_estimation_ids:
            if (line.group_of_product.id in exist_group_list) and (line.section_name.section_name in section_list):
                raise ValidationError(_('The group of product "%s" already exists in the section "%s", please change the Product with different Group of Product.'%((line.group_of_product.name),(line.section_name.section_name))))
            exist_group_list.append(line.group_of_product.id)
            section_list.append(line.section_name.section_name)
                 
    
    @api.onchange('labour_estimation_ids')
    def _check_exist_group_of_product_labour(self):
        exist_group_list = []
        section_list =[]
        for line in self.labour_estimation_ids:
            if (line.group_of_product.id in exist_group_list) and (line.section_name.section_name in section_list):
                raise ValidationError(_('The group of product "%s" already exists in the section "%s", please change the Product with different Group of Product.'%((line.group_of_product.name),(line.section_name.section_name))))
            exist_group_list.append(line.group_of_product.id)
            section_list.append(line.section_name.section_name)

    @api.onchange('overhead_estimation_ids')
    def _check_exist_group_of_product_overhead(self):
        exist_group_list = []
        section_list =[]
        for line in self.overhead_estimation_ids:
            if (line.group_of_product.id in exist_group_list) and (line.section_name.section_name in section_list):
                raise ValidationError(_('The group of product "%s" already exists in the section "%s", please change the Product with different Group of Product.'%((line.group_of_product.name),(line.section_name.section_name))))
            exist_group_list.append(line.group_of_product.id)
            section_list.append(line.section_name.section_name)

    @api.onchange('equipment_estimation_ids')
    def _check_exist_group_of_product_equipment(self):
        exist_group_list = []
        section_list =[]
        for line in self.equipment_estimation_ids:
            if (line.group_of_product.id in exist_group_list) and (line.section_name.section_name in section_list):
                raise ValidationError(_('The group of product "%s" already exists in the section "%s", please change the Product with different Group of Product.'%((line.group_of_product.name),(line.section_name.section_name))))
            exist_group_list.append(line.group_of_product.id)
            section_list.append(line.section_name.section_name)

    @api.onchange('subcon_estimation_ids')
    def _check_exist_subcon(self):
        exist_subcon_list = []
        section_list =[]
        for line in self.subcon_estimation_ids:
            if (line.variable.name in exist_subcon_list) and (line.section_name.section_name in section_list):
                raise ValidationError(_('The subcon "%s" already exists in the section "%s", please change the Subcon.'%((line.variable.name),(line.section_name.section_name))))
            exist_subcon_list.append(line.variable.name)
            section_list.append(line.section_name.section_name)

class MaterialEstimateInherits(models.Model):
    _name = "material.estimate"
    _inherit = ['material.estimate', 'portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _order = 'sequence'
    _check_company_auto = True

    material_id = fields.Many2one('job.estimate', string="Job Estimates")
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    type = fields.Selection([
        ('material', 'Material'),
        ('labour', 'Labour'),
        ('subcon', 'Subcon'),
        ('overhead', 'Overhead'),
        ('equipment', 'Equipment')
    ], string="Type", default='material', readonly=1)
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product', required=True)
    analytic_idz = fields.Many2many('account.analytic.tag', string='Analytic Group',
                                    domain="[('company_id', '=', parent.company_id)]")
    subtotal = fields.Float('Subtotal', readonly=True)
    product_id = fields.Many2one('product.product', string='Product',
                                 domain="[('sale_ok', '=', True), '|', ('purchase_ok', '=', True), '|', ('company_id', '=', False), ('company_id', '=', parent.company_id), '|', ('multi_companies_all', '=', False), ('multi_companies_all', '=', parent.company_id)]",
                                 check_company=True, ondelete='restrict', tracking=True, required=True)
    company_id = fields.Many2one(related='material_id.company_id', string='Company', readonly=True)
    description = fields.Text('Description', tracking=True, required=True)
    contract_category = fields.Selection(string="Contract Category", related='material_id.contract_category')
    current_quantity = fields.Float('Current Quantity')
    quantity = fields.Float('Quantity', default=0.0, tracking=True)
    unit_price = fields.Float('Unit Price', defaut=0.0, tracking=True)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    section_scope_computed = fields.Many2many('section.estimate', string="computed section lines")
    project_scope_computed = fields.Many2many('project.scope.line', string="computed scope lines",
                                              compute='get_scope_lines')


    @api.onchange('quantity')
    def set_account_group(self):
        for res in self:
            res.analytic_idz = res.material_id.analytic_idz

    @api.depends('material_id.project_scope_ids')
    def get_scope_lines(self):
        for rec in self:
            if rec.material_id.project_scope_ids:
                rec.project_scope_computed = [(6, 0, rec.material_id.project_scope_ids.mapped("project_scope").ids)]
            else:
                rec.project_scope_computed = [(6, 0, [])]

    @api.onchange("section_name", "project_scope")
    def relate_section_project_scope(self):
        domain = {}
        # print("---------------------------- Section Project Scope", rec.building_name)
        if self.project_scope:
            # print("--------------------- If ", rec.building_name.id)
            sctions = [int(s) for s in re.findall(r'\b\d+\b', self.material_id.sections_text)]
            domain = {'section_name': [('project_scope', '=', self.project_scope.id), ('id', "in", sctions),
                                       ('project_id', "=", self.material_id.project_id.id)]}
        elif not self.project_scope:
            domain = {'section_name': [('id', '<', 1)]}
        return {'domain': domain}

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.unit_price = self.product_id.list_price
            self.description = self.product_id.display_name
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False
            self.unit_price = False
            self.group_of_product = False

    @api.onchange('project_scope')
    def onchange_project_scope(self):
        if not self.material_id.project_id:
            raise ValidationError(_("Select Project First"))

    @api.depends('material_id.material_estimation_ids', 'material_id.material_estimation_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.material_id.material_estimation_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price = (line.quantity * line.unit_price)
            line.subtotal = price


class LabourEstimateInherits(models.Model):
    _name = "labour.estimate"
    _inherit = ['labour.estimate', 'portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _order = 'sequence'
    _check_company_auto = True

    labour_id = fields.Many2one('job.estimate', string="Job Estimates")
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    type = fields.Selection([
        ('material', 'Material'),
        ('labour', 'Labour'),
        ('subcon', 'Subcon'),
        ('overhead', 'Overhead'),
        ('equipment', 'Equipment')
    ], string="Type", default='labour', readonly=1)
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product', required=True)
    analytic_idz = fields.Many2many('account.analytic.tag', string='Analytic Group',
                                    domain="[('company_id', '=', parent.company_id)]")
    subtotal = fields.Float('Subtotal', readonly=True)
    subtotal = fields.Float('Subtotal', readonly=True)
    product_id = fields.Many2one('product.product', string='Product',
                                 domain="[('sale_ok', '=', True), '|', ('purchase_ok', '=', True), '|', ('company_id', '=', False), ('company_id', '=', parent.company_id), '|', ('multi_companies_all', '=', False), ('multi_companies_all', '=', parent.company_id)]",
                                 check_company=True, ondelete='restrict', tracking=True, required=True)
    company_id = fields.Many2one(related='labour_id.company_id', string='Company', readonly=True)
    description = fields.Text('Description', tracking=True, required=True)
    contract_category = fields.Selection(string="Contract Category", related='labour_id.contract_category')
    current_quantity = fields.Float('Current Quantity')
    quantity = fields.Float('Quantity', default=0.0, tracking=True)
    unit_price = fields.Float('Unit Price', defaut=0.0, tracking=True)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    section_scope_computed = fields.Many2many('section.estimate', string="computed section lines")
    project_scope_computed = fields.Many2many('project.scope.line', string="computed scope lines",
                                              compute='get_scope_lines')

    @api.onchange('quantity')
    def set_account_group(self):
        for res in self:
            res.analytic_idz = res.labour_id.analytic_idz

    @api.depends('labour_id.project_scope_ids')
    def get_scope_lines(self):
        for rec in self:
            if rec.labour_id.project_scope_ids:
                rec.project_scope_computed = [(6, 0, rec.labour_id.project_scope_ids.mapped("project_scope").ids)]
            else:
                rec.project_scope_computed = [(6, 0, [])]

    @api.onchange("section_name", "project_scope")
    def relate_section_project_scope(self):
        domain = {}
        # print("---------------------------- Section Project Scope", rec.building_name)
        if self.project_scope:
            # print("--------------------- If ", rec.building_name.id)
            sctions = [int(s) for s in re.findall(r'\b\d+\b', self.labour_id.sections_text)]
            domain = {'section_name': [('project_scope', '=', self.project_scope.id), ('id', "in", sctions),
                                       ('project_id', "=", self.labour_id.project_id.id)]}
        elif not self.project_scope:
            domain = {'section_name': [('id', '<', 1)]}
        return {'domain': domain}

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.unit_price = self.product_id.list_price
            self.description = self.product_id.display_name
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False
            self.unit_price = False
            self.group_of_product = False

    @api.onchange('project_scope')
    def onchange_project_scope(self):
        if not self.labour_id.project_id:
            raise ValidationError(_("Select Project First"))

    @api.depends('labour_id.labour_estimation_ids', 'labour_id.labour_estimation_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.labour_id.labour_estimation_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price = (line.quantity * line.unit_price)
            line.subtotal = price


class OverheadEstimateInherits(models.Model):
    _name = "overhead.estimate"
    _inherit = ['overhead.estimate', 'portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _order = 'sequence'
    _check_company_auto = True

    overhead_id = fields.Many2one('job.estimate', string="Job Estimates")
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    type = fields.Selection([
        ('material', 'Material'),
        ('labour', 'Labour'),
        ('subcon', 'Subcon'),
        ('overhead', 'Overhead'),
        ('equipment', 'Equipment')
    ], string="Type", default='overhead', readonly=1)
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product', required=True)
    description = fields.Text(string="Description", required=True)
    analytic_idz = fields.Many2many('account.analytic.tag', string='Analytic Group',
                                    domain="[('company_id', '=', parent.company_id)]")
    subtotal = fields.Float('Subtotal', readonly=True)
    subtotal = fields.Float('Subtotal', readonly=True)
    product_id = fields.Many2one('product.product', string='Product',
                                 domain="[('sale_ok', '=', True), '|', ('purchase_ok', '=', True), '|', ('company_id', '=', False), ('company_id', '=', parent.company_id), '|', ('multi_companies_all', '=', False), ('multi_companies_all', '=', parent.company_id)]",
                                 check_company=True, ondelete='restrict', tracking=True, required=True)
    company_id = fields.Many2one(related='overhead_id.company_id', string='Company', readonly=True)
    contract_category = fields.Selection(string="Contract Category", related='overhead_id.contract_category')
    current_quantity = fields.Float('Current Quantity')
    quantity = fields.Float('Quantity', default=0.0, tracking=True)
    unit_price = fields.Float('Unit Price', defaut=0.0, tracking=True)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    section_scope_computed = fields.Many2many('section.estimate', string="computed section lines")
    project_scope_computed = fields.Many2many('project.scope.line', string="computed scope lines",
                                              compute='get_scope_lines')

    @api.onchange('quantity')
    def set_account_group(self):
        for res in self:
            res.analytic_idz = res.overhead_id.analytic_idz

    @api.depends('overhead_id.project_scope_ids')
    def get_scope_lines(self):
        for rec in self:
            if rec.overhead_id.project_scope_ids:
                rec.project_scope_computed = [(6, 0, rec.overhead_id.project_scope_ids.mapped("project_scope").ids)]
            else:
                rec.project_scope_computed = [(6, 0, [])]

    @api.onchange("section_name", "project_scope")
    def relate_section_project_scope(self):
        domain = {}
        # print("---------------------------- Section Project Scope", rec.building_name)
        if self.project_scope:
            # print("--------------------- If ", rec.building_name.id)
            sctions = [int(s) for s in re.findall(r'\b\d+\b', self.overhead_id.sections_text)]
            domain = {'section_name': [('project_scope', '=', self.project_scope.id), ('id', "in", sctions),
                                       ('project_id', "=", self.overhead_id.project_id.id)]}
        elif not self.project_scope:
            domain = {'section_name': [('id', '<', 1)]}
        return {'domain': domain}

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.unit_price = self.product_id.list_price
            self.description = self.product_id.display_name
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False
            self.unit_price = False
            self.group_of_product = False

    @api.onchange('project_scope')
    def onchange_project_scope(self):
        if not self.overhead_id.project_id:
            raise ValidationError(_("Select Project First"))

    @api.depends('overhead_id.overhead_estimation_ids', 'overhead_id.overhead_estimation_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.overhead_id.overhead_estimation_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price = (line.quantity * line.unit_price)
            line.subtotal = price


class SubconEstimate(models.Model):
    _name = "subcon.estimate"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _order = 'sequence'
    _check_company_auto = True

    @api.onchange('variable')
    def onchange_variable(self):
        if self.variable:
            self.uom_id = self.variable.variable_uom.id
            self.quantity = 1.0
            self.unit_price = self.variable.total_variable
            self.description = self.variable.name
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False
            self.unit_price = False

    @api.onchange('project_scope')
    def onchange_project_scope(self):
        if not self.subcon_id.project_id:
            raise ValidationError(_("Select Project First"))

    @api.depends('subcon_id.subcon_estimation_ids', 'subcon_id.subcon_estimation_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.subcon_id.subcon_estimation_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price = (line.quantity * line.unit_price)
            line.subtotal = price

    @api.onchange('quantity')
    def set_account_group(self):
        for res in self:
            res.analytic_idz = res.subcon_id.analytic_idz

    sequence = fields.Integer(string="sequence", default=0)
    subcon_id = fields.Many2one('job.estimate', string="Job Estimates")
    type = fields.Selection([
        ('material', 'Material'),
        ('labour', 'Labour'),
        ('subcon', 'Subcon'),
        ('overhead', 'Overhead'),
        ('equipment', 'Equipment')
    ], string="Type", default='subcon', readonly=1)
    subtotal = fields.Float('Subtotal', readonly=True)
    company_id = fields.Many2one(related='subcon_id.company_id', string='Company', readonly=True)
    description = fields.Text(string="Description", tracking=True, required=True)
    contract_category = fields.Selection(string="Contract Category", related='subcon_id.contract_category')
    current_quantity = fields.Float('Current Quantity')
    quantity = fields.Float('Quantity', default=0.0, tracking=True)
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure', )
    unit_price = fields.Float('Unit Price', defaut=0.0, tracking=True)
    discount = fields.Float('Discount (%)', default=0.0)
    analytic_idz = fields.Many2many('account.analytic.tag', string='Analytic Group',
                                    domain="[('company_id', '=', parent.company_id)]")
    subtotal = fields.Float('Subtotal', readonly=True)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    variable = fields.Many2one('variable.template', string='Subcon',
                               domain="[('variable_subcon', '=', True), ('company_id', '=', parent.company_id)]",
                               check_company=True, ondelete='restrict', tracking=True, required=True)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    section_scope_computed = fields.Many2many('section.estimate', string="computed section lines")
    project_scope_computed = fields.Many2many('project.scope.line', string="computed scope lines",
                                              compute='get_scope_lines')

    @api.depends('subcon_id.project_scope_ids')
    def get_scope_lines(self):
        for rec in self:
            if rec.subcon_id.project_scope_ids:
                rec.project_scope_computed = [(6, 0, rec.subcon_id.project_scope_ids.mapped("project_scope").ids)]
            else:
                rec.project_scope_computed = [(6, 0, [])]

    @api.onchange("section_name", "project_scope")
    def relate_section_project_scope(self):
        domain = {}
        # print("---------------------------- Section Project Scope", rec.building_name)
        if self.project_scope:
            # print("--------------------- If ", rec.building_name.id)
            sctions = [int(s) for s in re.findall(r'\b\d+\b', self.subcon_id.sections_text)]
            domain = {'section_name': [('project_scope', '=', self.project_scope.id), ('id', "in", sctions),
                                       ('project_id', "=", self.subcon_id.project_id.id)]}
        elif not self.project_scope:
            domain = {'section_name': [('id', '<', 1)]}
        return {'domain': domain}


class EquipmentEstimate(models.Model):
    _name = "equipment.estimate"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _order = 'sequence'
    _check_company_auto = True

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.uom_id = self.product_id.uom_id.id
            self.quantity = 1.0
            self.unit_price = self.product_id.list_price
            self.description = self.product_id.display_name
            self.group_of_product = self.product_id.group_of_product.id
        else:
            self.description = False
            self.uom_id = False
            self.quantity = False
            self.unit_price = False
            self.group_of_product = False

    @api.onchange('project_scope')
    def onchange_project_scope(self):
        if not self.equipment_id.project_id:
            raise ValidationError(_("Select Project First"))

    @api.depends('equipment_id.equipment_estimation_ids', 'equipment_id.equipment_estimation_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.equipment_id.equipment_estimation_ids:
                no += 1
                l.sr_no = no

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('quantity', 'unit_price')
    def onchange_quantity(self):
        price = 0.0
        for line in self:
            price = (line.quantity * line.unit_price)
            line.subtotal = price

    @api.onchange('quantity')
    def set_account_group(self):
        for res in self:
            res.analytic_idz = res.equipment_id.analytic_idz

    sequence = fields.Integer(string="sequence", default=0)
    equipment_id = fields.Many2one('job.estimate', string="Job Estimates")
    type = fields.Selection([
        ('material', 'Material'),
        ('labour', 'Labour'),
        ('subcon', 'Subcon'),
        ('overhead', 'Overhead'),
        ('equipment', 'Equipment')
    ], string="Type", default='equipment', readonly=1)
    subtotal = fields.Float('Subtotal', readonly=True)
    product_id = fields.Many2one('product.product', string='Product',
                                 domain="[('sale_ok', '=', True), '|', ('purchase_ok', '=', True), '|', ('company_id', '=', False), ('company_id', '=', parent.company_id), '|', ('multi_companies_all', '=', False), ('multi_companies_all', '=', parent.company_id)]",
                                 check_company=True, ondelete='restrict', tracking=True, required=True)
    company_id = fields.Many2one(related='equipment_id.company_id', string='Company', readonly=True)
    description = fields.Text(string="Description", tracking=True, required=True)
    contract_category = fields.Selection(string="Contract Category", related='equipment_id.contract_category')
    current_quantity = fields.Float('Current Quantity')
    quantity = fields.Float('Quantity', default=0.0, tracking=True)
    uom_id = fields.Many2one('uom.uom', 'Unit Of Measure', )
    unit_price = fields.Float('Unit Price', defaut=0.0, tracking=True)
    analytic_idz = fields.Many2many('account.analytic.tag', string='Analytic Group',
                                    domain="[('company_id', '=', parent.company_id)]")
    discount = fields.Float('Discount (%)', default=0.0)
    subtotal = fields.Float('Subtotal', readonly=True)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    section_name = fields.Many2one('section.estimate', string='Section', required=True)
    variable_ref = fields.Many2one('variable.template', string='Variable')
    group_of_product = fields.Many2one('group.of.product', 'Group of Product', required=True)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    section_scope_computed = fields.Many2many('section.estimate', string="computed section lines")
    project_scope_computed = fields.Many2many('project.scope.line', string="computed scope lines",
                                              compute='get_scope_lines')

    @api.depends('equipment_id.project_scope_ids')
    def get_scope_lines(self):
        for rec in self:
            if rec.equipment_id.project_scope_ids:
                rec.project_scope_computed = [(6, 0, rec.equipment_id.project_scope_ids.mapped("project_scope").ids)]
            else:
                rec.project_scope_computed = [(6, 0, [])]

    @api.onchange("section_name", "project_scope")
    def relate_section_project_scope(self):
        domain = {}
        # print("---------------------------- Section Project Scope", rec.building_name)
        if self.project_scope:
            # print("--------------------- If ", rec.building_name.id)
            sctions = [int(s) for s in re.findall(r'\b\d+\b', self.equipment_id.sections_text)]
            domain = {'section_name': [('project_scope', '=', self.project_scope.id), ('id', "in", sctions),
                                       ('project_id', "=", self.equipment_id.project_id.id)]}
        elif not self.project_scope:
            domain = {'section_name': [('id', '<', 1)]}
        return {'domain': domain}


class ProjectScopeLine(models.Model):
    _inherit = "project.scope.line"

    project_id = fields.Many2one('project.project', readonly=True)


class ProjectScopeEstimate(models.Model):
    _name = "project.scope.estimate"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _rec_name = 'project_scope'
    _order = 'sequence'

    @api.depends('scope_id.material_estimation_ids', 'scope_id.labour_estimation_ids',
                 'scope_id.overhead_estimation_ids', 'scope_id.subcon_estimation_ids',
                 'scope_id.equipment_estimation_ids')
    def _amount_total(self):
        for scope in self.scope_id.project_scope_ids:
            total_subtotal = 0.0
            material_ids = self.scope_id.material_estimation_ids.filtered(
                lambda m: m.project_scope.id == scope.project_scope.id)
            for mat in material_ids:
                total_subtotal += mat.subtotal
            labour_ids = self.scope_id.labour_estimation_ids.filtered(
                lambda l: l.project_scope.id == scope.project_scope.id)
            for lab in labour_ids:
                total_subtotal += lab.subtotal
            overhead_ids = self.scope_id.overhead_estimation_ids.filtered(
                lambda o: o.project_scope.id == scope.project_scope.id)
            for ove in overhead_ids:
                total_subtotal += ove.subtotal
            subcon_ids = self.scope_id.subcon_estimation_ids.filtered(
                lambda s: s.project_scope.id == scope.project_scope.id)
            for sub in subcon_ids:
                total_subtotal += sub.subtotal
            equipment_ids = self.scope_id.equipment_estimation_ids.filtered(
                lambda e: e.project_scope.id == scope.project_scope.id)
            for equ in equipment_ids:
                total_subtotal += equ.subtotal
            scope.subtotal = total_subtotal

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    @api.onchange('project_scope')
    def onchange_project_scope(self):
        if not self.scope_id.project_id:
            raise ValidationError(_("Select Project First"))

    @api.depends('scope_id.project_scope_ids', 'scope_id.project_scope_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.scope_id.project_scope_ids:
                no += 1
                l.sr_no = no

    @api.onchange('project_scope')
    def project_scope_ref(self):
        if self.project_scope:
            self.description = self.project_scope.scope_description
        else:
            self.description = False

    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    scope_id = fields.Many2one('job.estimate', string="Job Estimates")
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True)
    description = fields.Text(string='Description')
    subtotal = fields.Float(string='Subtotal', compute='_amount_total', store=True, readonly=True)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    project_scope_computed = fields.Many2many('project.scope.line', string="computed scope lines",
                                              compute='get_scope_lines')

    @api.depends('scope_id')
    def get_scope_lines(self):
        for rec in self:
            if rec.scope_id.project_id:
                rec.project_scope_computed = [(6, 0, rec.scope_id.project_id.mapped("project_scope_line_ids").ids)]
            else:
                rec.project_scope_computed = [(6, 0, [])]




class SectionEstimate(models.Model):
    _name = "section.estimate"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _order = 'sequence'
    _rec_name = 'section_name'

    @api.depends('section_id.material_estimation_ids', 'section_id.labour_estimation_ids',
                 'section_id.overhead_estimation_ids', 'section_id.subcon_estimation_ids',
                 'section_id.equipment_estimation_ids')
    def _amount_total_section(self):
        total_job_estimate = 0.0
        for section in self.section_id.section_ids:
            total_subtotal = 0.0
            material_ids = self.section_id.material_estimation_ids.filtered(
                lambda m: m.section_name.section_name == section.section_name)
            for mat in material_ids:
                total_subtotal += mat.subtotal
            labour_ids = self.section_id.labour_estimation_ids.filtered(
                lambda m: m.section_name.section_name == section.section_name)
            for lab in labour_ids:
                total_subtotal += lab.subtotal
            overhead_ids = self.section_id.overhead_estimation_ids.filtered(
                lambda m: m.section_name.section_name == section.section_name)
            for ove in overhead_ids:
                total_subtotal += ove.subtotal
            subcon_ids = self.section_id.subcon_estimation_ids.filtered(
                lambda m: m.section_name.section_name == section.section_name)
            for sub in subcon_ids:
                total_subtotal += sub.subtotal
            equipment_ids = self.section_id.equipment_estimation_ids.filtered(
                lambda m: m.section_name.section_name == section.section_name)
            for equ in equipment_ids:
                total_subtotal += equ.subtotal

            total_job_estimate += total_subtotal
            section.subtotal = total_subtotal

            self.section_id.total_job_estimate = total_job_estimate

    @api.depends('section_id.section_ids', 'section_id.section_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.section_id.section_ids:
                no += 1
                l.sr_no = no

    @api.onchange('project_scope')
    def onchange_project_scope(self):
        if not self.section_id.project_id:
            raise ValidationError(_("Select Project First"))

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    section_id = fields.Many2one('job.estimate', string="Job Estimates")
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True, ondelete="cascade")
    section_name = fields.Char(string='Section', required=True)
    description = fields.Text(string='Description')
    subtotal = fields.Float(string='Subtotal', compute='_amount_total_section', store=True, readonly=True)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    project_scope_computed = fields.Many2many('project.scope.line', string="computed scope lines",
                                              compute='get_scope_lines')
    latest = fields.Integer("latest job id")
    project_id = fields.Many2one('project.project', "Project")

    @api.depends('section_id.project_scope_ids')
    def get_scope_lines(self):
        for rec in self:
            if rec.section_id.project_scope_ids:
                rec.project_scope_computed = [(6, 0, rec.section_id.project_scope_ids.mapped("project_scope").ids)]
            else:
                rec.project_scope_computed = [(6, 0, [])]


class VariableEstimate(models.Model):
    _name = "variable.estimate"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _order = 'sequence'
    _rec_name = 'variable_name'

    @api.depends('variable_id.variable_ids', 'variable_id.variable_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.variable_id.variable_ids:
                no += 1
                l.sr_no = no

    @api.onchange('project_scope')
    def onchange_project_scope(self):
        if not self.variable_id.project_id:
            raise ValidationError(_("Select Project First"))

    @api.onchange('variable_name')
    def onchange_variable_name(self):
        res = {}
        if not self.variable_name:
            return res
        self.variable_uom = self.variable_name.variable_uom.id
        self.variable_quantity = 1.0
        self.total_variable = self.variable_name.total_variable

    @api.onchange('variable_quantity', 'total_variable')
    def onchange_variable_quantity(self):
        total = 0.0
        for line in self:
            total = (line.variable_quantity * line.variable_name.total_variable)
            line.subtotal = total

    def get_currency_id(self):
        user_id = self.env.uid
        res_user_id = self.env['res.users'].browse(user_id)
        for line in self:
            line.currency_id = res_user_id.company_id.currency_id

    variable_id = fields.Many2one('job.estimate', string="Job Estimates")
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    project_scope = fields.Many2one('project.scope.line', string='Project Scope', required=True, ondelete="cascade")
    section_name = fields.Many2one('section.estimate', string='Section', required=True, ondelete="cascade")
    variable_name = fields.Many2one('variable.template', string='Variable', required=True, ondelete="cascade",
                                    domain="[('variable_subcon', '=', False), ('company_id', '=', parent.company_id)]")
    variable_quantity = fields.Float(string='Quantity', default=1.0, tracking=True)
    variable_uom = fields.Many2one('uom.uom', string="Unit Of Measure")
    total_variable = fields.Float(string='Total Variable', readonly=True)
    subtotal = fields.Float(string='Subtotal', readonly=True, store=True)
    currency_id = fields.Many2one("res.currency", compute='get_currency_id', string="Currency")
    section_scope_computed = fields.Many2many('section.estimate', string="computed section lines")
    project_scope_computed = fields.Many2many('project.scope.line', string="computed scope lines",
                                              compute='get_scope_lines')
    company_id = fields.Many2one(related='variable_id.company_id', string='Company', readonly=True)
    analytic_idz = fields.Many2many('account.analytic.tag', string='Analytic Group',
                                    domain="[('company_id', '=', parent.company_id)]")

    @api.onchange('variable_quantity')
    def set_account_group(self):
        for res in self:
            res.analytic_idz = res.variable_id.analytic_idz

    def unlink(self):
        # self.clear_all(self.variable_id)
        res = super(VariableEstimate, self).unlink()
        self.clear_caches()
        return res

    @api.depends('variable_id.project_scope_ids')
    def get_scope_lines(self):
        for rec in self:
            if rec.variable_id.project_scope_ids:
                rec.project_scope_computed = [(6, 0, rec.variable_id.project_scope_ids.mapped("project_scope").ids)]
            else:
                rec.project_scope_computed = [(6, 0, [])]

    @api.onchange("section_name", "project_scope")
    def relate_section_project_scope(self):
        domain = {}
        # print("---------------------------- Section Project Scope", rec.building_name)
        if self.project_scope:
            # print("--------------------- If ", rec.building_name.id)
            sctions = [int(s) for s in re.findall(r'\b\d+\b', self.variable_id.sections_text)]
            domain = {'section_name': [('project_scope', '=', self.project_scope.id), ('id', "in", sctions),
                                       ('project_id', "=", self.variable_id.project_id.id)]}
        elif not self.project_scope:
            domain = {'section_name': [('id', '<', 1)]}
        return {'domain': domain}
