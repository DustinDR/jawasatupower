# -*- coding: utf-8 -*-
from dataclasses import field
from email.policy import default
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, timedelta, date

class JobCostSheet(models.Model):
    _inherit = 'job.cost.sheet'

    sale_const = fields.Many2one('sale.order.const','Sale Const')

class SaleOrderConst(models.Model):
    _name = 'sale.order.const'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin', 'utm.mixin']
    _rec_name = "name"
    _description = 'Quotation'
    _order = 'date_order desc, id desc'
    _check_company_auto = True

    def _default_validity_date(self):
        if self.env['ir.config_parameter'].sudo().get_param('sale.use_quotation_validity_days'):
            days = self.env.company.quotation_validity_days
            if days > 0:
                return fields.Date.to_string(datetime.now() + timedelta(days))
        return False

    @api.onchange('job_reference')
    def _onchange_job_reference(self):
        if self.job_reference:
            job = self.job_reference
            self.project_id = job.project_id.id
            self.partner_id = job.partner_id.id
            self.client_order_ref = job.customer_ref
            self.analytic_idz = job.analytic_idz
            self.start_date = job.start_date
            self.end_date = job.end_date
            self.contract_category = job.contract_category
            self.main_contract_ref = job.main_contract_ref
            self.cost_sheet_ref = job.cost_sheet_ref

            scope = []
            self.scope_adjustment_ids = False
            self.scope_discount_ids = False
            if self.job_reference.project_scope_ids:
                for sco in self.job_reference.project_scope_ids:
                    scope.append((0, 0, {
                        'project_scope': sco.project_scope and sco.project_scope.id or False,
                        'subtotal_scope': sco.subtotal,
                    }))
            
            if len(scope) > 0:
                self.scope_adjustment_ids = scope
                self.scope_discount_ids = scope
            
            section = []
            self.section_adjustment_ids = False
            self.section_discount_ids = False
            if self.job_reference.section_ids:
                for sec in self.job_reference.section_ids:
                    section.append((0, 0, {
                        'section_name': sec.section_name or False,
                        'subtotal_section': sec.subtotal,
                    }))
            
            if len(section) > 0:
                self.section_adjustment_ids = section
                self.section_discount_ids = section
            
            order_lines = []
            self.order_line_ids = False
            if self.job_reference.material_estimation_ids:
                for material in self.job_reference.material_estimation_ids:
                    order_lines.append((0, 0, {
                        'project_scope': material.project_scope and material.project_scope.id or False,
                        'section_name': material.section_name and material.section_name.id or False,
                        'variable_ref': material.variable_ref and material.variable_ref.id or False,
                        'type': 'material',
                        'group_of_product': material.group_of_product and material.group_of_product.id or False,
                        'product_id': material.product_id and material.product_id.id or False,
                        'description': material.description,
                        'analytic_idz': material.analytic_idz and [(6, 0, material.analytic_idz.ids)] or False,
                        'quantity': material.quantity,
                        'uom_id': material.uom_id and material.uom_id.id or False,
                        'unit_price': material.unit_price,
                        'subtotal': material.subtotal,
                    }))
            if self.job_reference.labour_estimation_ids:
                for labour in self.job_reference.labour_estimation_ids:
                    order_lines.append((0, 0, {
                        'project_scope': labour.project_scope and labour.project_scope.id or False,
                        'section_name': labour.section_name and labour.section_name.id or False,
                        'variable_ref': labour.variable_ref and labour.variable_ref.id or False,
                        'type': 'labour',
                        'group_of_product': labour.group_of_product and labour.group_of_product.id or False,
                        'product_id': labour.product_id and labour.product_id.id or False,
                        'description': labour.description,
                        'analytic_idz': labour.analytic_idz and [(6, 0, labour.analytic_idz.ids)] or False,
                        'quantity': labour.quantity,
                        'uom_id': labour.uom_id and labour.uom_id.id or False,
                        'unit_price': labour.unit_price,
                        'subtotal': labour.subtotal,
                    }))
            if self.job_reference.overhead_estimation_ids:
                for overhead in self.job_reference.overhead_estimation_ids:
                    order_lines.append((0, 0, {
                        'project_scope': overhead.project_scope and overhead.project_scope.id or False,
                        'section_name': overhead.section_name and overhead.section_name.id or False,
                        'variable_ref': overhead.variable_ref and overhead.variable_ref.id or False,
                        'type': 'overhead',
                        'group_of_product': overhead.group_of_product and overhead.group_of_product.id or False,
                        'product_id': overhead.product_id and overhead.product_id.id or False,
                        'description': overhead.description,
                        'analytic_idz': overhead.analytic_idz and [(6, 0, overhead.analytic_idz.ids)] or False,
                        'quantity': overhead.quantity,
                        'uom_id': overhead.uom_id and overhead.uom_id.id or False,
                        'unit_price': overhead.unit_price,
                        'subtotal': overhead.subtotal,
                    }))
            if self.job_reference.equipment_estimation_ids:
                for equipment in self.job_reference.equipment_estimation_ids:
                    order_lines.append((0, 0, {
                        'project_scope': equipment.project_scope and equipment.project_scope.id or False,
                        'section_name': equipment.section_name and equipment.section_name.id or False,
                        'variable_ref': equipment.variable_ref and equipment.variable_ref.id or False,
                        'type': 'equipment',
                        'group_of_product': equipment.group_of_product and equipment.group_of_product.id or False,
                        'product_id': equipment.product_id and equipment.product_id.id or False,
                        'description': equipment.description,
                        'analytic_idz': equipment.analytic_idz and [(6, 0, equipment.analytic_idz.ids)] or False,
                        'quantity': equipment.quantity,
                        'uom_id': equipment.uom_id and equipment.uom_id.id or False,
                        'unit_price': equipment.unit_price,
                        'subtotal': equipment.subtotal,
                    }))
            if self.job_reference.subcon_estimation_ids:
                for subcon in self.job_reference.subcon_estimation_ids:
                    order_lines.append((0, 0, {
                        'project_scope': subcon.project_scope and subcon.project_scope.id or False,
                        'section_name': subcon.section_name and subcon.section_name.id or False,
                        'variable_ref': subcon.variable_ref and subcon.variable_ref.id or False,
                        'type': 'subcon',
                        'variable': subcon.variable and subcon.variable.id or False,
                        'description': subcon.description,
                        'analytic_idz': subcon.analytic_idz and [(6, 0, subcon.analytic_idz.ids)] or False,
                        'quantity': subcon.quantity,
                        'uom_id': subcon.uom_id and subcon.uom_id.id or False,
                        'unit_price': subcon.unit_price,
                        'subtotal': subcon.subtotal,
                    }))
                    
            if len(order_lines) > 0:
                self.order_line_ids = order_lines
                
    
    @api.onchange('contract_category')
    def onchange_contract_category(self):
        if self.project_id:
            pro = self.project_id
            if self.contract_category == 'main':
                self.dp_method = pro.dp_method
                self.dp_amount = pro.dp_amount
                self.dp_total = pro.down_payment
                self.retention1 = pro.retention1
                self.retention1_date = pro.retention1_date
                self.retention2 = pro.retention2
                self.retention2_date = pro.retention2_date
                self.tax_id = [(6, 0, [v.id for v in pro.tax_id])]
                self.opportunity_id = pro.lead_id.id
            
            else:
                self.contract_project = pro.contract_amount
                self.tax_id = [(6, 0, [v.id for v in pro.tax_id])]
                self.payment_term_id = pro.payment_term.id
    
    @api.onchange('vo_payment_type')
    def onchange_vo_payment_type(self):
        if self.project_id:
            pro = self.project_id
            if self.vo_payment_type == 'join':
                self.dp_method = pro.dp_method
                self.dp_amount = pro.dp_amount
                self.dp_total = pro.down_payment
                self.retention1 = pro.retention1
                self.retention1_date = pro.retention1_date
                self.retention2 = pro.retention2
                self.retention2_date = pro.retention2_date

            else:
                self.dp_method = 'fix'
                self.dp_amount = False
                self.dp_total = False
                self.retention1 = False
                self.retention1_date = False
                self.retention2 = False
                self.retention2_date = False

    @api.onchange('analytic_idz')
    def set_account_group_lines(self):
        for res in self:
            for line in res.order_line_ids:
                line.analytic_idz = res.analytic_idz
    
    @api.onchange('tax_id')
    def set_tax_id_lines(self):
        for res in self:
            for line in res.order_line_ids:
                line.line_tax_id = res.tax_id

    @api.onchange('contract_category')
    def set_vo_payment_type(self):
        self.vo_payment_type = False
        if self.contract_category == 'main':
            self.vo_payment_type = False
        else:
            self.vo_payment_type = 'join'
    
    @api.constrains('state')
    def contract_category_cahnge(self):
        if self.state:
            if self.state != 'sale':
                self.name = self.env['ir.sequence'].next_by_code('sale.order.quotation.const')
            else:
                self.name = self.env['ir.sequence'].next_by_code('sale.order.quotation.order.const')

    @api.constrains('start_date', 'end_date')
    def constrains_date(self):
        for rec in self:
            if rec.start_date != False and rec.end_date != False:
                if rec.start_date > rec.end_date:
                    raise UserError(_('End date should be after start date.'))

    # def _find_mail_template(self, force_confirmation_template=False):
    #     template_id = False

    #     if force_confirmation_template or (self.state == 'sale' and not self.env.context.get('proforma', False)):
    #         template_id = int(self.env['ir.config_parameter'].sudo().get_param('sale.default_confirmation_template'))
    #         template_id = self.env['mail.template'].search([('id', '=', template_id)]).id
    #         if not template_id:
    #             template_id = self.env['ir.model.data'].xmlid_to_res_id('sale.mail_template_sale_confirmation', raise_if_not_found=False)
    #     if not template_id:
    #         template_id = self.env['ir.model.data'].xmlid_to_res_id('sale.email_template_edi_sale', raise_if_not_found=False)

    #     return template_id
    
    # def button_revision(self):
    #     pass
    
    # def action_quotation_send(self):
    #     template_id = self._find_mail_template()
    #     lang = self.env.context.get('lang')
    #     template = self.env['mail.template'].browse(template_id)
    #     if template.lang:
    #         lang = template._render_lang(self.ids)[self.id]

    #     if self.state != 'sale':
    #         subject = '%s Quotation (Ref %s)'%(self.company_id.name, self.name)
    #     else:
    #         subject = '%s Order (Ref %s)'%(self.company_id.name, self.name)

    #     body = self.env['mail.render.mixin']._render_template(template.body_html, 'sale.order.const', self.ids)[self.id]
    #     ctx = {
    #         'default_body': body,
    #         'default_subject': subject,
    #         'default_model': 'sale.order.const',
    #         'default_partner_ids': self.partner_id.ids,
    #         'default_res_id': self.ids[0],
    #         'default_use_template': bool(template_id),
    #         'default_template_id': template_id,
    #         'default_composition_mode': 'comment',
    #         'mark_so_as_sent': True,
    #         'custom_layout': "mail.mail_notification_paynow",
    #         'proforma': self.env.context.get('proforma', False),
    #         'force_email': True,
    #         'model_description': self.with_context(lang=lang).type_name,
    #     }
    #     mail_compose_message_id = self.env['mail.compose.message'].with_context(ctx).create({})
    #     values = mail_compose_message_id.generate_email_for_composer(
    #         template.id, [self.id],
    #         ['subject', 'body_html', 'email_from', 'email_to', 'partner_to', 'email_cc',  'reply_to', 'attachment_ids', 'mail_server_id']
    #     )[self.id]
    #     attachment_ids = []
    #     Attachment = self.env['ir.attachment']
    #     for attach_fname, attach_datas in values.pop('attachments', []):
    #         data_attach = {
    #             'name': attach_fname,
    #             'datas': attach_datas,
    #             'res_model': 'mail.compose.message',
    #             'res_id': 0,
    #             'type': 'binary',  # override default_type from context, possibly meant for another model!
    #         }
    #         attachment_ids.append(Attachment.create(data_attach).id)
    #     mail_compose_message_id.attachment_ids = [(6, 0, attachment_ids)]
    #     # mail_compose_message_id.send_mail()
    #     mail_message_id = self.env['mail.message'].search([('res_id', '=', self.id), ('model', '=', 'sale.order')], limit=1)
    #     mail_message_id.write({'subject': subject})
    #     mail_message_id.res_id = 0
    #     if self.state != 'sale':
    #         self.message_post(body=_("Email has been sent to %s for Quotation information") % self.partner_id.name)
    #     else:
    #         self.message_post(body=_("Email has been sent to %s for Sale Order confirmation") % self.partner_id.name)

    #     return {
    #         'type': 'ir.actions.act_window',
    #         'view_mode': 'form',
    #         'res_model': 'mail.compose.message',
    #         'views': [(False, 'form')],
    #         'view_id': False,
    #         'target': 'new',
    #         'context': ctx,
    #     }
    
    # @api.returns('mail.message', lambda value: value.id)
    # def message_post(self, **kwargs):
    #     if self.env.context.get('mark_so_as_sent'):
    #         self.filtered(lambda o: o.state == 'draft').with_context(tracking_disable=True).write({'state': 'sent'})
    #     return super(SaleOrderConst, self.with_context(mail_post_autofollow=True)).message_post(**kwargs)

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('sale.order.quotation.const')
        res =  super(SaleOrderConst, self).create(vals)
        if res.job_reference:
            res.job_reference.write({
                'state':'done',
                'state_new':'done',
                'sale_quotation_id' : self.id
            })
        return res
    
    name = fields.Char(string='Number', required=True, copy=False, readonly=True,
                        index=True, default=lambda self: _('New'))
    job_reference = fields.Many2one('job.estimate', required=True, tracking=True, string="Job Estimates", ondelete='cascade', readonly=True,
                    states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
                    domain="[('state', 'in', ['approved','done'])]")
    partner_id = fields.Many2one(
        'res.partner', string='Customer', readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        required=True, change_default=True, index=True, tracking=1,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]",)
    partner_invoice_id = fields.Many2one(
        'res.partner', string='Invoice Address',
        readonly=True, required=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'waiting_for_approval': [('readonly', False)], 
                           'quotation_approved': [('readonly', False)], 'sale': [('readonly', False)]}, 
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")
    analytic_idz = fields.Many2many('account.analytic.tag', string='Analytic Group', domain="[('company_id', '=', company_id)]", readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}) 
    project_id = fields.Many2one('project.project', required=True, tracking=True, string="Project", readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    report_template_id = fields.Many2one('ir.actions.report', string="Sale Order Template")
    date_order = fields.Datetime(string='Order Date', required=True, readonly=True, index=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, copy=False, default=fields.Datetime.now, help="Creation date of draft/sent orders,\nConfirmation date of confirmed orders.",tracking=True)
    validity_date = fields.Date(string='Expiration Date', readonly=True, copy=False, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
                                default=_default_validity_date, required=True)
    is_expired = fields.Boolean(compute='_compute_is_expired', string="Is expired")
    pricelist_id = fields.Many2one(
        'product.pricelist', string='Pricelist', check_company=True,  # Unrequired company
        required=True, readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]", tracking=1,
        help="If you change the pricelist, only newly added lines will be affected.")
    currency_id = fields.Many2one(related='pricelist_id.currency_id', depends=["pricelist_id"], store=True, string="Sale Order Currency")
    company_currency_id = fields.Many2one('res.currency', string='Currency', related='company_id.currency_id')
    type_name = fields.Char('Type Name', compute='_compute_type_name')
    adjustment_type = fields.Selection([
                        ('scope', 'Project Scope'),
                        ('section', 'Section'),
                        ('line', 'Order Line'), 
                        ('global', 'Global')
                        ],string='Adjustment Applies to',default='global', readonly=True,
                        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    discount_type = fields.Selection([
                        ('scope', 'Project Scope'),
                        ('section', 'Section'),
                        ('line', 'Order Line'), 
                        ('global', 'Global')
                        ],string='Discount Applies to', default='global', readonly=True,
                        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    state = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('waiting_for_approval', 'Waiting For Approval'),
        ('quotation_approved', 'Quotation Approved'),
        ('sale', 'Sales Order'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
        ('reject', 'Quotation Rejected')
        ], string='Status', readonly=True, copy=False, index=True, tracking=True, default='draft')
    
    signature = fields.Image('Signature', help='Signature received through the portal.', copy=False, attachment=True, max_width=1024, max_height=1024)
    signed_by = fields.Char('Signed By', help='Name of the person that signed the SO.', copy=False)
    signed_on = fields.Datetime('Signed On', help='Date of the signature.', copy=False)

    # tab project details

    contract_category = fields.Selection([
                        ('main', 'Main Contract'),
                        ('var', 'Variation Order')
                        ],string="Contract Category", default='main', required=True)
    main_contract_ref = fields.Many2one('sale.order.const', string="Main Contract")
    cost_sheet_ref = fields.Many2one('job.cost.sheet', 'Cost Sheet', tracking=True,
                     domain="[('state', '=', 'approved'), ('company_id', '=', company_id)]")
    vo_payment_type = fields.Selection([
                        ('join', 'Join Payment'),
                        ('split', 'Split Payment')
                        ], string="Payment Method")
    start_date = fields.Date(string="Start Date", readonly=True,
                    states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    end_date = fields.Date(string="End Date", readonly=True,
                    states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    contract_amount = fields.Float(string="Contract Amount", compute="_compute_amount") 
    contract_project = fields.Float(string="Contract Amount")
    total_contract_amount = fields.Float(string="Total Contract Amount", compute="_compute_total_contract")
    dp_method = fields.Selection([
                        ('fix', 'Fixed'),
                        ('per', 'Percentage')
                        ],string="Down Payment Method", default='fix')
    dp_amount = fields.Float(string="Down Payment Amount")
    dp_total = fields.Float(string="Total Down Payment", compute="_compute_total_downpayment")
    retention1 = fields.Float(string="Retention 1")
    retention1_amount = fields.Float(string="Retention 1 Amount", compute="_compute_total_retention1")
    retention1_date = fields.Date(string="Retention 1 Date")
    retention2 = fields.Float(string="Retention 2")
    retention2_amount = fields.Float(string="Retention 2 Amount", compute="_compute_total_retention2")
    retention2_date = fields.Date(string="Retention 2 Date")
    tax_id = fields.Many2many('account.tax', string='Taxes', domain=['|', ('active', '=', False), ('active', '=', True)], required=True, readonly=True,
                states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    payment_term_id = fields.Many2one(
        'account.payment.term', string='Payment Terms', check_company=True,  # Unrequired company
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]",)
    
    # tab order lines

    order_line_ids = fields.One2many('sale.order.line.const', 'order_id', string='Order Lines', states={'cancel': [('readonly', True)], 'done': [('readonly', True)]}, copy=True, auto_join=True)
    scope_adjustment_ids = fields.One2many('scope.adjustment', 'order_id')
    section_adjustment_ids = fields.One2many('section.adjustment', 'order_id')
    scope_discount_ids = fields.One2many('scope.discount', 'order_id')
    section_discount_ids = fields.One2many('section.discount', 'order_id')
    note = fields.Text(string="Term and Conditions...")
    adjustment_method_global = fields.Selection([
                        ('fix', 'Fixed'),
                        ('per', 'Percentage')
                        ],string="Adjustment Method", readonly=True,
                        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    adjustment_amount_global = fields.Float(string="Adjustment Amount", readonly=True,
                                states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    discount_method_global = fields.Selection([
                        ('fix', 'Fixed'),
                        ('per', 'Percentage')
                        ],string="Discount Method", readonly=True,
                        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    discount_amount_global = fields.Float(string="Discount Amount", readonly=True,
                            states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    amount_untaxed = fields.Float(string="Amount Before Adjustment", compute="_compute_amount")
    adjustment_sub = fields.Float(string="Global Adjustment (+)", compute="_compute_amount")
    discount_sub = fields.Float(string="Global Discount (-)", compute="_compute_amount")
    line_adjustment = fields.Float(string="LIne Adjustment (+)", compute="_compute_amount")
    line_discount = fields.Float(string="Line Discount (-)", compute="_compute_amount")
    
    adjustment_scope = fields.Float(string="Scope Adjustment (+)", compute="_compute_amount")
    discount_scope = fields.Float(string="Scope Discount (-)", compute="_compute_amount")
    adjustment_section = fields.Float(string="Section Adjustment (+)", compute="_compute_amount")
    discount_section = fields.Float(string="Section Discount (-)", compute="_compute_amount")

    amount_tax = fields.Float(string="Taxes", compute="_compute_amount")
    amount_total = fields.Float(string="Total", compute="_compute_amount")
    invoice_count = fields.Integer(string='Invoices', compute='_compute_invoice_count')

    #sale term and condition
    terms_conditions_id = fields.Many2one('sale.terms.and.conditions', string='Terms and Conditions', readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}) 

    state_1 = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('waiting_for_approval', 'Waiting For Approval'),
        ('quotation_approved', 'Quotation Approved'),
        ('sale', 'Sales Order'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
        ('reject', 'Quotation Rejected')
        ], string='Status', readonly=True, copy=False, index=True, default='draft')
    sale_state = fields.Selection([
        ('pending', 'Pending'),
        ('progress', 'In Progress'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ] ,string="Sale State", default='pending')
    sale_state_1 = fields.Selection(related='sale_state')


    def _compute_invoice_count(self):
        for order in self:
            order.invoice_count = self.env['account.move'].sudo().search_count([('sale_order_ref', '=', order.id)])

    def action_view_invoices(self):
        action = {
            'name': _('Invoices'),
            'type': 'ir.actions.act_window',
            'res_model': 'account.move',
            'target': 'current',
        }
        invoices = self.env['account.move'].sudo().search([('sale_order_ref', '=', self.id)])
        invoice_ids = invoices.ids
        if len(invoice_ids) == 1:
            invoice = invoice_ids[0]
            action['res_id'] = invoice
            action['view_mode'] = 'form'
            form_view = [(self.env.ref('account.view_move_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [(state,view) for state,view in action['views'] if view != 'form']
            else:
                action['views'] = form_view
        else:
            action['view_mode'] = 'tree,form'
            action['domain'] = [('id', 'in', invoice_ids)]
        return action

    def button_confirm(self):
        self.write({'date_order' : datetime.now()})
        self.state = 'sale'
        self.state_1 = 'sale'
        self.sale_state = 'progress'
        self.sale_state_1 = 'progress'
        
        self.job_reference.write({
                'state':'sale',  
                })
        
        self.project_id.write({
                'section_ids' : False,
                })

        if self.contract_category == 'main':
            
            res = self.job_reference
            list_section = []
            for section in res.section_ids:
                list_section.append(
                    (0, 0, {'project_scope': section.project_scope.id, 'name': section.section_name, 'description': section.description}))
            
            self.project_id.write({
                'start_date' : self.start_date,
                'end_date' : self.end_date,
                'partner_id' : self.partner_id.id,
                'customer_ref' : self.client_order_ref,
                'analytic_idz' : [(6, 0, [v.id for v in self.analytic_idz])],
                'primary_states' : 'progress',
                'retention2_date': self.retention2_date,
                'retention1_date': self.retention1_date,
                'tax_id':[(6, 0, [v.id for v in self.tax_id])],
                'payment_term': self.payment_term_id.id,
                'contract_amount': self.total_contract_amount,
                'dp_method': self.dp_method,
                'dp_amount': self.dp_amount,
                'down_payment': self.dp_total,
                'retention1': self.retention1,
                'retention2': self.retention2,
                #'project_scope_line_ids' : list_scope,
                'section_ids' : list_section,
            })
            

            line = self.env['job.cost.sheet'].create({
                # 'sale_const': self.id,
                'cost_sheet_name': self.project_id.name + ' - ' + self.partner_id.name,
                'partner_id': self.partner_id.id,
                'project_id': self.project_id.id,
                'sale_order_ref':  [(4, self.id)],
                'job_reference': [(4, self.job_reference.id)]
            })
            if line:
                line.sudo()._onchange_sale_order_ref()
                line.sudo()._onchange_job_reference()


        elif self.contract_category == 'var':
            res = self.job_reference
            if self.vo_payment_type == 'join':
                list_section = []
                for section in res.section_ids:
                    list_section.append(
                        (0, 0, {'project_scope': section.project_scope.id, 'name': section.section_name, 'description': section.description}))
                
                
                list_var = []
                for sub in self:
                    list_var.append(
                        (0, 0, {'sale_order_ref': self.id and self.id or False,
                                'contract_amount': sub.contract_amount,
                                'down_payment' : sub.dp_total,
                                'retention_1': sub.retention1,
                                'retention_1_date': sub.retention1_date,                                    
                                'retention_2': sub.retention2,
                                'retention_2_date': sub.retention2_date,
                                'payment_term': sub.payment_term_id.id,
                                'vo_payment_type': sub.vo_payment_type, 
                                }
                        ))
                    
                self.project_id.write({
                    'section_ids' : list_section,
                    'addendum_line_ids' : list_var,
                })

                self.cost_sheet_ref.write({
                    'sale_order_ref':  [(4, self.id)],
                    'job_reference': [(4, self.job_reference.id)]
                })

                self.cost_sheet_ref.sudo()._onchange_sale_order_ref()
                self.cost_sheet_ref.sudo()._onchange_job_reference()
                
                # self.cost_sheet_ref.write({
                #     'sale_order_ref':  [(4, self.id)],
                #     'job_reference': [(4, self.job_reference.id)]
                # })

                # if line in self.cost_sheet_ref:
                #     line.sudo()._onchange_sale_order_ref()
                #     line.sudo()._onchange_job_reference()
            
            elif self.vo_payment_type == 'split':
                list_section = []
                for section in res.section_ids:
                    list_section.append(
                        (0, 0, {'project_scope': section.project_scope.id, 'name': section.section_name, 'description': section.description}))
                
                
                list_var = []
                for sub in self:
                    list_var.append(
                        (0, 0, {'sale_order_ref': self.id and self.id or False,
                                'contract_amount': sub.contract_amount,
                                'down_payment' : sub.dp_total,
                                'retention_1': sub.retention1,
                                'retention_1_date': sub.retention1_date,                                    
                                'retention_2': sub.retention2,
                                'retention_2_date': sub.retention2_date,
                                'payment_term': sub.payment_term_id.id,
                                'vo_payment_type': sub.vo_payment_type, 
                                }
                        ))
                
                self.project_id.write({
                    'section_ids' : list_section,
                    'addendum_line_ids' : list_var,
                })

                self.cost_sheet_ref.write({
                    'sale_order_ref':  [(4, self.id)],
                    'job_reference': [(4, self.job_reference.id)]
                })

                self.cost_sheet_ref.sudo()._onchange_sale_order_ref()
                self.cost_sheet_ref.sudo()._onchange_job_reference()
                
                # self.cost_sheet_ref.write({
                #     'sale_order_ref':  [(4, self.id)],
                #     'job_reference': [(4, self.job_reference.id)]
                # })

                # if line in self.cost_sheet_ref:
                #     line.sudo()._onchange_sale_order_ref()
                #     line.sudo()._onchange_job_reference()
        

        IrConfigParam = self.env['ir.config_parameter'].sudo()
        for record in self:
            record.write({'sale_state': 'progress'})
            keep_name_so = IrConfigParam.get_param('keep_name_so', False)
            if not keep_name_so:
                if record.origin:
                    record.origin += "," + record.name
                else:
                    record.origin = record.name
                record.name = self.env['ir.sequence'].next_by_code('sale.order.quotation.order.const')
        return


    def action_project(self):
        action = self.project_id.get_formview_action()
        # action = self.env["ir.actions.actions"]._for_xml_id("abs_construction_management.action_view_project")
        action['domain'] = [('id', '=', self.project_id.id)]
        return action

    def button_cancel(self):
        res = self.write({'state': 'cancel',
                          'state_1': 'cancel', 
                          'sale_state': 'cancel', 
                          'sale_state_1': 'cancel' 
                         })
        return res

    def button_set_quotation(self):
        res = self.write({'state': 'draft',
                          'state_1': 'draft', 
                          'sale_state': 'pending', 
                          'sale_state_1': 'pending' 
                         })
        return res

    def action_cost_sheet(self):
        # views = self.env.ref('abs_construction_management.view_job_cost_sheet_menu_form').id
        job_sheet = self.env['job.cost.sheet'].search([('sale_order_ref', '=', self.id)], limit=1)
        action = job_sheet.get_formview_action()
        # action = self.env["ir.actions.actions"]._for_xml_id("abs_construction_management.action_view_job_cost_sheet_menu")
        return action

    def button_create_invoice(self):
        return

    def button_done(self):
        if self.sale_state == 'done':
            self.state='done'
        else:
            raise ValidationError("You have an unfinished invoice")

    @api.depends('contract_category', 'contract_project', 'contract_amount')
    def _compute_total_contract(self):
        for res in self:
            if res.contract_category == 'main':
                res.total_contract_amount = res.contract_amount
            else:
                res.total_contract_amount = res.contract_project + res.contract_amount
    
    @api.depends('dp_method', 'dp_amount', 'contract_amount')
    def _compute_total_downpayment(self):
        for res in self:
            if res.dp_method == 'fix':
                res.dp_total = res.dp_amount
            else:
                res.dp_total = res.contract_amount * (res.dp_amount / 100)

    @api.depends('retention1', 'contract_amount')
    def _compute_total_retention1(self):
        for res in self:
            res.retention1_amount = res.contract_amount * (res.retention1 / 100)

    @api.depends('retention2', 'contract_amount')
    def _compute_total_retention2(self):
        for res in self:
            res.retention2_amount = res.contract_amount * (res.retention2 / 100)

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        """
        Update the following fields when the partner is changed:
        - Pricelist
        - Payment terms
        - Invoice address
        - Sales Team
        """
        if not self.partner_id:
            self.update({
                'partner_invoice_id': False,
                'fiscal_position_id': False,
            })
            return

        self = self.with_company(self.company_id)

        addr = self.partner_id.address_get(['invoice'])
        partner_user = self.partner_id.user_id or self.partner_id.commercial_partner_id.user_id
        values = {
            'pricelist_id': self.partner_id.property_product_pricelist and self.partner_id.property_product_pricelist.id or False,
            'payment_term_id': self.partner_id.property_payment_term_id and self.partner_id.property_payment_term_id.id or False,
            'partner_invoice_id': addr['invoice'],
        }
        user_id = partner_user.id
        if not self.env.context.get('not_self_saleperson'):
            user_id = user_id or self.env.uid
        if user_id and self.user_id.id != user_id:
            values['user_id'] = user_id

        if self.env['ir.config_parameter'].sudo().get_param('account.use_invoice_terms') and self.env.company.invoice_terms:
            values['note'] = self.with_context(lang=self.partner_id.lang).env.company.invoice_terms
        if not self.env.context.get('not_self_saleperson') or not self.team_id:
            values['team_id'] = self.env['crm.team'].with_context(
                default_team_id=self.partner_id.team_id.id
            )._get_default_team_id(domain=['|', ('company_id', '=', self.company_id.id), ('company_id', '=', False)], user_id=user_id)
        self.update(values)

    @api.onchange('user_id')
    def onchange_user_id(self):
        if self.user_id:
            self.team_id = self.env['crm.team'].with_context(
                default_team_id=self.team_id.id
            )._get_default_team_id(user_id=self.user_id.id)

    @api.depends('state')
    def _compute_type_name(self):
        for record in self:
            record.type_name = _('Quotation') if record.state in ('draft', 'sent', 'cancel') else _('Sales Order')

    def unlink(self):
        for order in self:
            if order.state not in ('draft', 'cancel'):
                raise UserError(_('You can not delete a sent quotation or a confirmed sales order. You must first cancel it.'))
        return super(SaleOrderConst, self).unlink()


    def _get_default_require_signature(self):
        return self.env.company.portal_confirmation_sign

    def _get_default_require_payment(self):
        return self.env.company.portal_confirmation_pay
    
    def _compute_is_expired(self):
        today = fields.Date.today()
        for order in self:
            order.is_expired = order.state == 'sent' and order.validity_date and order.validity_date < today

    @api.depends('order_line_ids.subtotal', 'order_line_ids.adjustment_method_line', 'order_line_ids.adjustment_amount_line', 
                 'order_line_ids.adjustment_line', 'order_line_ids.discount_method_line', 'order_line_ids.discount_amount_line', 
                 'order_line_ids.discount_line', 'adjustment_type', 'adjustment_method_global', 'adjustment_amount_global', 
                 'discount_method_global', 'discount_amount_global', 'discount_type', 'order_line_ids.amount_tax_line', 
                 'order_line_ids.project_scope', 'scope_adjustment_ids.project_scope', 'order_line_ids.section_name', 
                 'section_adjustment_ids.section_name', 'scope_discount_ids.project_scope', 'section_discount_ids.section_name', 
                 'scope_adjustment_ids.subtotal_scope', 'scope_adjustment_ids.adjustment_method_scope', 'scope_adjustment_ids.adjustment_amount_scope',
                 'scope_adjustment_ids.adjustment_subtotal_scope', 'section_adjustment_ids.subtotal_section', 'section_adjustment_ids.adjustment_method_section',
                 'section_adjustment_ids.adjustment_amount_section', 'section_adjustment_ids.adjustment_subtotal_section',
                 'scope_discount_ids.subtotal_scope', 'scope_discount_ids.adjustment_amount', 'scope_discount_ids.discount_method_scope', 'scope_discount_ids.discount_amount_scope',
                 'scope_discount_ids.discount_subtotal_scope', 'section_discount_ids.subtotal_section', 'section_discount_ids.adjustment_amount', 
                 'section_discount_ids.discount_method_section', 'section_discount_ids.discount_amount_section', 'section_discount_ids.discount_subtotal_section')
    def _compute_amount(self):
        for order in self:
            order.contract_amount = 0
            order.amount_untaxed = sum(order.order_line_ids.mapped('subtotal'))
            order.amount_tax = sum(order.order_line_ids.mapped('amount_tax_line'))
            if order.adjustment_type == 'global':
                order.adjustment_scope = 0
                order.adjustment_section = 0
                order.line_adjustment = 0
                if order.adjustment_method_global == 'fix':
                    order.adjustment_sub = order.adjustment_amount_global
                    number_of_lines = len(order.order_line_ids)
                    for line in order.order_line_ids:
                        line_adjustment_line = order.adjustment_amount_global / number_of_lines
                        line.sudo().write({'adjustment_line': line_adjustment_line})

                else:
                    order.adjustment_sub = order.amount_untaxed * (order.adjustment_amount_global/100)
                    for line in order.order_line_ids:
                        line_adjustment_line = line.subtotal * (order.adjustment_amount_global/100)
                        line.sudo().write({'adjustment_line': line_adjustment_line})
                   
            elif order.adjustment_type == 'line':
                order.adjustment_scope = 0
                order.adjustment_section = 0
                order.adjustment_sub = 0
                for line in order.order_line_ids:
                    if line.adjustment_method_line == 'fix':
                        line_adjustment_subtotal_line = line.adjustment_amount_line
                        line.sudo().write({'adjustment_line': line_adjustment_subtotal_line})
                        order.line_adjustment = sum(order.order_line_ids.mapped('adjustment_line'))
                    else:
                        line_adjustment_subtotal_line = line.subtotal * (line.adjustment_amount_line / 100)
                        line.sudo().write({'adjustment_line': line_adjustment_subtotal_line})
                        order.line_adjustment = sum(order.order_line_ids.mapped('adjustment_line'))

            elif order.adjustment_type == 'scope':
                order.adjustment_section = 0
                order.adjustment_sub = 0
                order.line_adjustment = 0
                for scope in order.scope_adjustment_ids:
                    if scope.adjustment_method_scope == 'fix':
                        scope.adjustment_subtotal_scope = scope.adjustment_amount_scope
                        order.adjustment_scope = sum(order.scope_adjustment_ids.mapped('adjustment_subtotal_scope'))
                        same_scope = []
                        for scope1 in order.order_line_ids:
                            if scope.project_scope.id == scope1.project_scope.id:
                                same_scope.append(scope1.project_scope.id)
                                number_of_lines = len(same_scope)
                        for scope1 in order.order_line_ids:
                            if scope.project_scope.id == scope1.project_scope.id:
                                line_adjustment_line = scope.adjustment_subtotal_scope / float(number_of_lines)
                                scope1.sudo().write({'adjustment_line': line_adjustment_line})

                    else:
                        scope.adjustment_subtotal_scope = scope.subtotal_scope * (scope.adjustment_amount_scope / 100)
                        order.adjustment_scope = sum(order.scope_adjustment_ids.mapped('adjustment_subtotal_scope'))
                        for scope1 in order.order_line_ids:
                            if scope.project_scope.id == scope1.project_scope.id:
                                line_adjustment_line = scope1.subtotal * (scope.adjustment_amount_scope/100)
                                scope1.sudo().write({'adjustment_line': line_adjustment_line})
            
            elif order.adjustment_type == 'section':
                order.adjustment_scope = 0
                order.adjustment_sub = 0
                order.line_adjustment = 0
                for section in order.section_adjustment_ids:
                    if section.adjustment_method_section == 'fix':
                        section.adjustment_subtotal_section = section.adjustment_amount_section
                        order.adjustment_section = sum(order.section_adjustment_ids.mapped('adjustment_subtotal_section'))
                        same_section = []
                        for section1 in order.order_line_ids:
                            if section.section_name == section1.section_name.section_name:
                                same_section.append(section1.section_name.section_name)
                                number_of_lines = len(same_section)
                        for section1 in order.order_line_ids:
                            if section.section_name == section1.section_name.section_name:
                                line_adjustment_line = section.adjustment_subtotal_section / number_of_lines
                                section1.sudo().write({'adjustment_line': line_adjustment_line})

                    else:
                        section.adjustment_subtotal_section = section.subtotal_section * (section.adjustment_amount_section / 100)
                        order.adjustment_section = sum(order.section_adjustment_ids.mapped('adjustment_subtotal_section'))
                        for section1 in order.order_line_ids:
                            if section.section_name == section1.section_name.section_name:
                                line_adjustment_line = section1.subtotal * (section.adjustment_amount_section/100)
                                section1.sudo().write({'adjustment_line': line_adjustment_line})
            
            else:
                order.adjustment_scope = 0
                order.adjustment_section = 0
                order.adjustment_sub = 0
                order.line_adjustment = 0

            if order.discount_type == 'global':
                order.discount_scope = 0
                order.discount_section = 0
                order.line_discount = 0
                if order.discount_method_global == 'fix':
                    order.discount_sub = order.discount_amount_global
                    number_of_lines = len(order.order_line_ids)
                    for line in order.order_line_ids:
                        line_discount_line = order.discount_amount_global / number_of_lines
                        line.sudo().write({'discount_line': line_discount_line})

                else:
                    order.discount_sub = (order.amount_untaxed + order.adjustment_scope + order.adjustment_section + order.adjustment_sub + order.line_adjustment) * (order.discount_amount_global / 100)
                    for line in order.order_line_ids:
                        line_discount_line = (line.subtotal + line.adjustment_line) * (order.discount_amount_global/100)
                        line.sudo().write({'discount_line': line_discount_line})

            elif order.discount_type == 'line':
                order.discount_scope = 0
                order.discount_section = 0
                order.discount_sub = 0
                for line in order.order_line_ids:
                    if line.discount_method_line == 'fix':
                        line_discount_line = line.discount_amount_line
                        line.sudo().write({'discount_line': line_discount_line})
                        order.line_discount = sum(order.order_line_ids.mapped('discount_line'))
                    else:
                        line_discount_line = (line.subtotal + line.adjustment_line) * (line.discount_amount_line / 100)
                        line.sudo().write({'discount_line': line_discount_line})
                        order.line_discount = sum(order.order_line_ids.mapped('discount_line'))

            elif order.discount_type == 'scope':
                order.discount_section = 0
                order.discount_sub = 0
                order.line_discount = 0
                for scope in order.scope_discount_ids:
                    for scope1 in order.order_line_ids:
                        prd = order.order_line_ids.filtered(lambda p: p.project_scope and p.project_scope.id == scope.project_scope.id)
                        if scope1.project_scope.id == scope.project_scope.id:
                            adjustment_temp = sum(prd.mapped('adjustment_line'))
                            scope.sudo().write({'adjustment_amount': adjustment_temp})

                    if scope.discount_method_scope == 'fix':
                        scope.discount_subtotal_scope = scope.discount_amount_scope
                        order.discount_scope = sum(order.scope_discount_ids.mapped('discount_subtotal_scope'))
                        same_scope = []
                        for scope1 in order.order_line_ids:
                            if scope.project_scope.id == scope1.project_scope.id:
                                same_scope.append(scope1.project_scope.id)
                                number_of_lines = len(same_scope)
                        for scope1 in order.order_line_ids:        
                            if scope.project_scope.id == scope1.project_scope.id:
                                line_discount_line = scope.discount_subtotal_scope / number_of_lines
                                scope1.sudo().write({'discount_line': line_discount_line})
                                
                    else:
                        scope.discount_subtotal_scope = (scope.subtotal_scope + scope.adjustment_amount) * (scope.discount_amount_scope / 100)
                        order.discount_scope = sum(order.scope_discount_ids.mapped('discount_subtotal_scope'))
                        for scope1 in order.order_line_ids:
                            if scope.project_scope.id == scope1.project_scope.id:
                                line_discount_line = (scope1.subtotal + scope1.adjustment_line) * (scope.discount_amount_scope/100)
                                scope1.sudo().write({'discount_line': line_discount_line})


            elif order.discount_type == 'section':
                order.discount_scope = 0
                order.discount_sub = 0
                order.line_discount = 0
                for section in order.section_discount_ids:
                    for section1 in order.order_line_ids:
                        prd = order.order_line_ids.filtered(lambda p: p.section_name.section_name == section.section_name)
                        if section1.section_name.section_name == section.section_name:
                            adjustment_temp = sum(prd.mapped('adjustment_line'))
                            section.sudo().write({'adjustment_amount': adjustment_temp})

                    if section.discount_method_section == 'fix':
                        section.discount_subtotal_section = section.discount_amount_section
                        order.discount_section = sum(order.section_discount_ids.mapped('discount_subtotal_section'))
                        same_section = []
                        for section1 in order.order_line_ids:
                            if section.section_name == section1.section_name.section_name:
                                same_section.append(section1.section_name.section_name)
                                number_of_lines = len(same_section)
                        for section1 in order.order_line_ids:
                            if section.section_name == section1.section_name.section_name:
                                line_discount_line = section.discount_subtotal_section / number_of_lines
                                section1.sudo().write({'discount_line': line_discount_line})

                    else:
                        section.discount_subtotal_section = (section.subtotal_section + section.adjustment_amount) * (section.discount_amount_section / 100)
                        order.discount_section = sum(order.section_discount_ids.mapped('discount_subtotal_section'))
                        for section1 in order.order_line_ids:
                            if section.section_name == section1.section_name.section_name:
                                line_discount_line = (section1.subtotal + section1.adjustment_line) * (section.discount_amount_section/100)
                                section1.sudo().write({'discount_line': line_discount_line})

            else:
                order.discount_scope = 0
                order.discount_section = 0
                order.discount_sub = 0
                order.line_discount = 0

            for line in order.order_line_ids:
                 line_amount_line = (line.subtotal + line.adjustment_line) - line.discount_line
                 line.sudo().write({'amount_line': line_amount_line})

            order.contract_amount = order.amount_untaxed + order.adjustment_scope + order.adjustment_section + order.adjustment_sub + order.line_adjustment - order.discount_scope - order.discount_section - order.discount_sub - order.line_discount 
            order.amount_total = order.contract_amount + order.amount_tax

            return order.amount_total
        

    @api.onchange('terms_conditions_id')
    def _onchange_terms_conditions_id(self):
        if self.terms_conditions_id:
            self.note = self.terms_conditions_id.terms_and_conditions


    @api.model
    def _get_default_team(self):
        return self.env['crm.team']._get_default_team_id()
    
    # sales
    user_id = fields.Many2one(
        'res.users', string='Salesperson', index=True, tracking=2, default=lambda self: self.env.user,
        domain=lambda self: [('groups_id', 'in', self.env.ref('sales_team.group_sale_salesman').id)], readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    team_id = fields.Many2one(
        'crm.team', 'Sales Team',
        change_default=True, default=_get_default_team, check_company=True,  # Unrequired company
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]", readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True, default=lambda self: self.env.company, readonly=True)
    require_signature = fields.Boolean('Online Signature', default=_get_default_require_signature, readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        help='Request a online signature to the customer in order to confirm orders automatically.')
    require_payment = fields.Boolean('Online Payment', default=_get_default_require_payment, readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        help='Request an online payment to the customer in order to confirm orders automatically.')
    client_order_ref = fields.Char(string='Customer Reference', copy=False)
    tag_ids = fields.Many2many('crm.tag', string='Tags')
    report_grids = fields.Boolean(string="Print Variant Grids", default=True)

    # invoicing
    fiscal_position_id = fields.Many2one(
        'account.fiscal.position', string='Fiscal Position',
        domain="[('company_id', '=', company_id)]", check_company=True,
        help="Fiscal positions are used to adapt taxes and accounts for particular customers or sales orders/invoices."
        "The default value comes from the customer.")
    analytic_account_id = fields.Many2one(
        'account.analytic.account', 'Analytic Account',
        readonly=True, copy=False, check_company=True,  # Unrequired company
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]",
        help="The analytic account related to a sales order.")

    # branch
    branch_id = fields.Many2one('res.branch', string="Branch", readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})

    # reporting
    origin = fields.Char(string='Source Document', help="Reference of the document that generated this sales order request.")
    opportunity_id = fields.Many2one('crm.lead', string="Opportunity")
    campaign_id = fields.Many2one('utm.campaign', string="Campaign")
    medium_id = fields.Many2one('utm.medium', string="Medium")
    source_id = fields.Many2one('utm.source', string="Source")
    
    # tab customer signature
    signature = fields.Image('Signature', help='Signature received through the portal.', copy=False, attachment=True, max_width=1024, max_height=1024)
    signed_by = fields.Char('Signed By', help='Name of the person that signed the SO.', copy=False)
    signed_on = fields.Datetime('Signed On', help='Date of the signature.', copy=False)

    #tab sale order approval matrix line
    approval_matrix_state = fields.Selection(related='state', tracking=False)
    approval_matrix_state_1 = fields.Selection(related='state', tracking=False)
    approving_matrix_sale_id = fields.Many2many('approval.matrix.sale.order.const', string="Approval Matrix", compute='_compute_approving_customer_matrix', store=True)
    approved_matrix_ids = fields.One2many('approval.matrix.sale.order.lines.const', 'order_id', store=True, string="Approved Matrix", compute='_compute_approving_matrix_lines')
    is_customer_approval_matrix_const = fields.Boolean(string="Custome Matrix", store=False, compute='_compute_is_customer_approval_matrix')
    is_approval_matrix_filled = fields.Boolean(string="Custome Matrix", store=False, compute='_compute_approval_matrix_filled')
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button', store=False)
    approval_matrix_line_id = fields.Many2one('approval.matrix.sale.order.lines.const', string='Sale Approval Matrix Line', compute='_get_approve_button', store=False)
    is_quotation_cancel = fields.Boolean(string='Is Quotation Cancel', default=False)

    @api.depends('approving_matrix_sale_id')
    def _compute_approval_matrix_filled(self):
        for record in self:
            record.is_approval_matrix_filled = False
            if record.approving_matrix_sale_id:
                record.is_approval_matrix_filled = True

    def _get_approve_button(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_name_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False

    @api.onchange('partner_id')
    def onchange_partner_id_new(self):
        self._compute_is_customer_approval_matrix()
        self._compute_approval_matrix_filled()
        default_branch = self.env.user.branch_id.id,
        self.branch_id = default_branch

    @api.onchange('partner_id')
    def _set_domain_partner_invoice_id(self):
        b = {}
        if self.partner_id:
            partner_inv_ids = self.env['res.partner'].search([('parent_id', '=', self.partner_id.id), ('type', '=', 'invoice')]).ids
            b = {'domain': {'partner_invoice_id': [('id', 'in', partner_inv_ids)]}}
        return b

    @api.onchange('name')
    def _onchange_sale_name(self):
        self._compute_is_customer_approval_matrix()
        self._compute_approval_matrix_filled()


    @api.depends('approving_matrix_sale_id')
    def _compute_approving_matrix_lines(self):
        data = [(5, 0, 0)]
        for record in self:
            if record.state == 'draft' and record.is_customer_approval_matrix_const:
                record.approved_matrix_ids = []
                counter = 1
                record.approved_matrix_ids = []
                for rec in record.approving_matrix_sale_id: 
                    for line in rec.approver_matrix_line_ids:
                        data.append((0, 0, {
                            'sequence' : counter,
                            'user_name_ids' : [(6, 0, line.user_name_ids.ids)],
                            'minimum_approver' : line.minimum_approver,
                            'approval_type': rec.config,
                        }))
                        counter += 1
                record.approved_matrix_ids = data

    @api.depends('partner_id')
    def _compute_is_customer_approval_matrix(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        is_customer_approval_matrix_const = IrConfigParam.get_param('is_customer_approval_matrix_const')
        for record in self:
            record.is_customer_approval_matrix_const = is_customer_approval_matrix_const

    @api.depends('project_id','branch_id','company_id','contract_amount', 'adjustment_sub', 'line_adjustment', 'discount_sub', 'line_discount')
    def _compute_approving_customer_matrix(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        is_contract_amount = IrConfigParam.get_param('is_contract_amount', False)
        is_adjustment_amount = IrConfigParam.get_param('is_adjustment_amount', False)
        is_discount_amount_const = IrConfigParam.get_param('is_discount_amount_const', False)
        contract_sequence = IrConfigParam.get_param('contract_sequence', 0)
        adjustment_sequence = IrConfigParam.get_param('adjustment_sequence', 0)
        discount_sequence_const = IrConfigParam.get_param('discount_sequence_const', 0)
        data = []
        if is_contract_amount:
            data.insert(int(contract_sequence) - 1, 'contract_amt')
        if is_adjustment_amount:
            data.insert(int(adjustment_sequence) - 1, 'adjustment_amt')
        if is_discount_amount_const:
            data.insert(int(discount_sequence_const) - 1, 'discount_amt')
        for record in self:
            matrix_ids = []
            if record.is_customer_approval_matrix_const:
                record.approving_matrix_sale_id = False
                for sale_matrix_config in data: 
                    if sale_matrix_config == 'contract_amt':
                        matrix_id = self.env['approval.matrix.sale.order.const'].search([('company_id', '=', record.company_id.id),
                                                                                         ('branch_id', '=', record.branch_id.id),
                                                                                         ('project_id', '=', record.project_id.id),
                                                                                         ('config', '=', 'contract_amt'), 
                                                                                         ('minimum_amt', '<=', record.contract_amount), 
                                                                                         ('maximum_amt', '>=', record.contract_amount)], limit=1)
                        if matrix_id:
                            matrix_ids.append(matrix_id.id)

                    elif sale_matrix_config == 'adjustment_amt':
                        if record.adjustment_type == 'line':
                            matrix_id = self.env['approval.matrix.sale.order.const'].search([('company_id', '=', record.company_id.id),
                                                                                             ('branch_id', '=', record.branch_id.id),
                                                                                             ('project_id', '=', record.project_id.id),
                                                                                             ('config', '=', 'adjustment_amt'), 
                                                                                             ('minimum_amt', '<=', record.line_adjustment), 
                                                                                             ('maximum_amt', '>=', record.line_adjustment)], limit=1)
                            if matrix_id:
                                matrix_ids.append(matrix_id.id)
                        elif record.adjustment_type == 'global':
                            matrix_id = self.env['approval.matrix.sale.order.const'].search([('company_id', '=', record.company_id.id),
                                                                                             ('branch_id', '=', record.branch_id.id),
                                                                                             ('project_id', '=', record.project_id.id),
                                                                                             ('config', '=', 'adjustment_amt'), 
                                                                                             ('minimum_amt', '<=', record.adjustment_sub), 
                                                                                             ('maximum_amt', '>=', record.adjustment_sub)], limit=1)
                            if matrix_id:
                                matrix_ids.append(matrix_id.id)
                        elif record.adjustment_type == 'scope':
                            matrix_id = self.env['approval.matrix.sale.order.const'].search([('company_id', '=', record.company_id.id),
                                                                                             ('branch_id', '=', record.branch_id.id),
                                                                                             ('project_id', '=', record.project_id.id),
                                                                                             ('config', '=', 'adjustment_amt'), 
                                                                                             ('minimum_amt', '<=', record.adjustment_scope), 
                                                                                             ('maximum_amt', '>=', record.adjustment_scope)], limit=1)
                            if matrix_id:
                                matrix_ids.append(matrix_id.id)
                        elif record.adjustment_type == 'section':
                            matrix_id = self.env['approval.matrix.sale.order.const'].search([('company_id', '=', record.company_id.id),
                                                                                             ('branch_id', '=', record.branch_id.id),
                                                                                             ('project_id', '=', record.project_id.id),
                                                                                             ('config', '=', 'adjustment_amt'), 
                                                                                             ('minimum_amt', '<=', record.adjustment_section), 
                                                                                             ('maximum_amt', '>=', record.adjustment_section)], limit=1)
                            if matrix_id:
                                matrix_ids.append(matrix_id.id)
                        else:
                            matrix_ids = False

                    elif sale_matrix_config == 'discount_amt':
                        if record.discount_type == 'line':
                            matrix_id = self.env['approval.matrix.sale.order.const'].search([('company_id', '=', record.company_id.id),
                                                                                             ('branch_id', '=', record.branch_id.id),
                                                                                             ('project_id', '=', record.project_id.id),
                                                                                             ('config', '=', 'discount_amt'), 
                                                                                             ('minimum_amt', '<=', record.line_discount), 
                                                                                             ('maximum_amt', '>=', record.line_discount)], limit=1)
                            if matrix_id:
                                matrix_ids.append(matrix_id.id)
                        elif record.discount_type == 'global':
                            matrix_id = self.env['approval.matrix.sale.order.const'].search([('company_id', '=', record.company_id.id),
                                                                                             ('branch_id', '=', record.branch_id.id),
                                                                                             ('project_id', '=', record.project_id.id),
                                                                                             ('config', '=', 'discount_amt'), 
                                                                                             ('minimum_amt', '<=', record.discount_sub), 
                                                                                             ('maximum_amt', '>=', record.discount_sub)], limit=1)
                            if matrix_id:
                                matrix_ids.append(matrix_id.id)
                        elif record.discount_type == 'scope':
                            matrix_id = self.env['approval.matrix.sale.order.const'].search([('company_id', '=', record.company_id.id),
                                                                                             ('branch_id', '=', record.branch_id.id),
                                                                                             ('project_id', '=', record.project_id.id),
                                                                                             ('config', '=', 'discount_amt'), 
                                                                                             ('minimum_amt', '<=', record.discount_scope), 
                                                                                             ('maximum_amt', '>=', record.discount_scope)], limit=1)
                            if matrix_id:
                                matrix_ids.append(matrix_id.id)
                        elif record.discount_type == 'section':
                            matrix_id = self.env['approval.matrix.sale.order.const'].search([('company_id', '=', record.company_id.id),
                                                                                             ('branch_id', '=', record.branch_id.id),
                                                                                             ('project_id', '=', record.project_id.id),
                                                                                             ('config', '=', 'discount_amt'), 
                                                                                             ('minimum_amt', '<=', record.discount_section), 
                                                                                             ('maximum_amt', '>=', record.discount_section)], limit=1)
                            if matrix_id:
                                matrix_ids.append(matrix_id.id)
                        else:
                            matrix_ids = False

                record.approving_matrix_sale_id = [(6 ,0, matrix_ids)]
            else:
                record.approving_matrix_sale_id = False

    def action_confirm_approving(self):
        for record in self:
            record.write({'state': 'quotation_approved'})

    def action_request_for_approving(self):
        for record in self:
            record.write({'state': 'waiting_for_approval'})

    def action_confirm_approving_matrix(self):
        for record in self:
            user = self.env.user
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_name_ids.ids and \
                    user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    if name != '':
                        name += "\n • %s: Approved" % (self.env.user.name)
                    else:
                        name += "• %s: Approved" % (self.env.user.name)

                    approval_matrix_line_id.write({ 
                        'last_approved': self.env.user.id, 'state_char': name, 
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})

            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r:r.approved)):
                record.write({'state': 'quotation_approved'})

    def action_reject_approving_matrix(self):
        return {
                'type': 'ir.actions.act_window',
                'name': 'Reject Reason',
                'res_model': 'approval.matrix.sale.reject.const',
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
                }

    def action_set_quotation(self):
        for record in self:
            record.approved_matrix_ids.write({
                            'approved_users': False, 
                            'last_approved': False, 
                            'approved': False, 
                            'feedback': False,
                            'time_stamp': False,
                            'state_char': False,
                            })
            record.write({'state': 'draft'})

    @api.model
    def _action_sale_order_cancel(self):
        today_date = date.today()
        sale_order_ids = self.search([('validity_date', '<', today_date), ('state', 'in', ('draft', 'sent'))])
        sale_order_ids.write({'sale_state': 'cancel', 'state': 'cancel'})

    def action_draft(self):
        orders = self.filtered(lambda s: s.state in ['cancel', 
                            'sent', 'reject'])
        return orders.write({
            'state': 'draft',
            'signature': False,
            'signed_by': False,
            'signed_on': False,
        })

    def action_done(self):
        for record in self:
            if all(invoice_id.payment_state == 'paid' for invoice_id in record.invoice_ids):
               record.write({'sale_state': 'done'})
            else:
                raise ValidationError(_("You Have an Unfinished Invoice."))

class OrderLines(models.Model):
    _name = 'sale.order.line.const'
    _description = 'Sales Order Line'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin', 'utm.mixin']
    _rec_name = "type"
    _order = 'order_id, sequence, id'
    _check_company_auto = True

    order_id = fields.Many2one('sale.order.const', string='Order Reference', required=True, ondelete='cascade', index=True, copy=False)
    
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    project_scope = fields.Many2one('project.scope.line', string='Project Scope')
    section_name = fields.Many2one('section.estimate',string='Section')
    variable_ref = fields.Many2one('variable.template',string='Variable')
    type = fields.Selection([
            ('material', 'Material'),
            ('labour', 'Labour'),
            ('subcon', 'Subcon'),
            ('overhead', 'Overhead'),
            ('equipment', 'Equipment')
            ],string="Estimation Type")
    group_of_product = fields.Many2one('group.of.product', 'Group of Product')
    product_id = fields.Many2one('product.product', string="Product", tracking=True)
    variable = fields.Many2one('variable.template', string='Subcon', 
               domain="[('variable_subcon', '=', True), ('company_id', '=', parent.company_id)]")
    description = fields.Text(string="Description", tracking=True)
    quantity = fields.Float(string="Quantity", tracking=True)
    company_id = fields.Many2one(related='order_id.company_id', string='Company', readonly=True)
    analytic_idz = fields.Many2many('account.analytic.tag', string='Analytic Group', domain="[('company_id', '=', parent.company_id)]", tracking=True)
    uom_id = fields.Many2one('uom.uom', string="Unit Of Measure")
    line_tax_id = fields.Many2many('account.tax', string="Taxes", tracking=True)
    unit_price = fields.Float(string="Unit Price", tracking=True)
    subtotal = fields.Float(string="Subtotal")
    adjustment_method_line = fields.Selection([
                        ('fix', 'Fixed'),
                        ('per', 'Percentage')
                        ],string="Adjustment Method")
    adjustment_amount_line = fields.Float(string="Adjustment Amount", tracking=True)
    adjustment_subtotal_line = fields.Float(string="Adjustment Subtotal")
    adjustment_line = fields.Float(string="Adjustment Line")
    discount_method_line = fields.Selection([
                        ('fix', 'Fixed'),
                        ('per', 'Percentage')
                        ],string="Discount Method")
    discount_amount_line = fields.Float(string="Discount Amount", tracking=True)
    discount_subtotal_line = fields.Float(string="Discount Subtotal")
    discount_line = fields.Float(string="Discount Line")
    discount_type = fields.Selection(related='order_id.discount_type', string="Discount Applies to")
    adjustment_type = fields.Selection(related='order_id.adjustment_type', string="Adjustment Applies to")
    amount_line = fields.Float(string="Amount Before Tax")
    amount_tax_line = fields.Float(string="Amount Tax", compute="_compute_amount_tax_line")
    total_amount = fields.Float(string="Total Amount", compute="_compute_total_amount")

    currency_id = fields.Many2one(related='order_id.currency_id', depends=['order_id.currency_id'], store=True, string='Currency', readonly=True)
    company_id = fields.Many2one(related='order_id.company_id', string='Company', store=True, readonly=True, index=True)
    state_line = fields.Selection(related='order_id.state', string='Order Status')
    project_id = fields.Many2one(related='order_id.project_id', string="Project")
    job_reference = fields.Many2one(related='order_id.job_reference', string="Job Estimates Reference")
    customer_id = fields.Many2one(related='order_id.partner_id', string='Customer')
    user_id = fields.Many2one(related='order_id.user_id', string='Salesperson')
    sequence_no = fields.Char(related='order_id.name', string="Sequence Number")
    analytic_account_id = fields.Many2one('account.analytic.account', string="Analytic Account")

    # def _compute_adjustment_subtotal_line(self):
    #     for line in self:
    #         if line.adjustment_method_line == 'fix':
    #             line.adjustment_subtotal_line = line.adjustment_amount_line
    #         else:
    #             line.adjustment_subtotal_line = line.subtotal * (line.adjustment_amount_line / 100)

    # def _compute_discount_line(self):
    #     print("<<<<<<<316>>>>>>>")
    #     for line in self:
    #         line.discount_line = (line.subtotal + line.adjustment_line) * (line.order_id.discount_amount_global / 100)
    #         print("<<<<<<<<line.discount_line<<<<<<<",line.discount_line)

    # def _compute_discount_subtotal_line(self):
    #     for line in self:
    #         if line.discount_method_line == 'fix':
    #             line.discount_subtotal_line = line.discount_amount_line
    #         else:
    #             line.discount_subtotal_line = (line.subtotal + line.adjustment_line) * (line.discount_amount_line / 100)

    # def _compute_amount_line(self):
    #     for line in self:
    #         line.amount_line = (line.subtotal + line.adjustment_line) - line.discount_line

    def _compute_amount_tax_line(self):
        for line in self:
            line_tax_id_amount = 0
            for tax_line in line.line_tax_id:
                line_tax_id_amount += tax_line.amount
            line.amount_tax_line = line.amount_line * (line_tax_id_amount / 100)
            return line_tax_id_amount

    def _compute_total_amount(self):
        for line in self:
            line.total_amount = line.amount_line + line.amount_tax_line

    @api.depends('order_id.order_line_ids', 'order_id.order_line_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.order_id.order_line_ids:
                no += 1
                l.sr_no = no

    @api.onchange('quantity')
    def set_account_group(self):
        for res in self:
            res.analytic_idz = res.order_id.analytic_idz
            res.line_tax_id = res.order_id.tax_id



class AddendumLineinherit(models.Model):
	_inherit = "addendum.line"

	sale_order_ref = fields.Many2one('sale.order.const', string="Sales Order Reference", ondelete='cascade')


class ScopeAdjustment(models.Model):
    _name = "scope.adjustment"
    _order = 'sequence'

    @api.depends('order_id.scope_adjustment_ids', 'order_id.scope_adjustment_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.order_id.scope_adjustment_ids:
                no += 1
                l.sr_no = no

    order_id = fields.Many2one('sale.order.const', string='Order Reference')
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    project_scope = fields.Many2one('project.scope.line', string='Project Scope')
    adjustment_type = fields.Selection(related='order_id.adjustment_type', string="Adjustment Applies to")
    adjustment_method_scope = fields.Selection([
                        ('fix', 'Fixed'),
                        ('per', 'Percentage')
                        ],string="Adjustment Method")
    adjustment_amount_scope = fields.Float(string="Adjustment Amount")
    adjustment_subtotal_scope = fields.Float(string="Adjustment Subtotal")
    subtotal_scope = fields.Float(string='Subtotal')


class ScopeDiscount(models.Model):
    _name = "scope.discount"
    _order = 'sequence'

    @api.depends('order_id.scope_discount_ids', 'order_id.scope_discount_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.order_id.scope_discount_ids:
                no += 1
                l.sr_no = no

    order_id = fields.Many2one('sale.order.const', string='Order Reference')
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    project_scope = fields.Many2one('project.scope.line', string='Project Scope')
    discount_type = fields.Selection(related='order_id.discount_type', string="Discount Applies to")
    discount_method_scope = fields.Selection([
                        ('fix', 'Fixed'),
                        ('per', 'Percentage')
                        ],string="Discount Method")
    discount_amount_scope = fields.Float(string="Discount Amount")
    discount_subtotal_scope = fields.Float(string="Discount Subtotal")
    subtotal_scope = fields.Float(string='Subtotal')
    adjustment_amount = fields.Float(string='Adjustment')

class SectionAdjustment(models.Model):
    _name = "section.adjustment"
    _order = 'sequence'

    @api.depends('order_id.section_adjustment_ids', 'order_id.section_adjustment_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.order_id.section_adjustment_ids:
                no += 1
                l.sr_no = no

    order_id = fields.Many2one('sale.order.const', string='Order Reference')
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    section_name = fields.Char(string='Section')
    adjustment_type = fields.Selection(related='order_id.adjustment_type', string="Adjustment Applies to")
    adjustment_method_section = fields.Selection([
                        ('fix', 'Fixed'),
                        ('per', 'Percentage')
                        ],string="Adjustment Method")
    adjustment_amount_section = fields.Float(string="Adjustment Amount")
    adjustment_subtotal_section = fields.Float(string="Adjustment Subtotal")
    subtotal_section = fields.Float(string='Subtotal')
            

class SectionDiscount(models.Model):
    _name = "section.discount"
    _order = 'sequence'

    @api.depends('order_id.section_discount_ids', 'order_id.section_discount_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.order_id.section_discount_ids:
                no += 1
                l.sr_no = no

    order_id = fields.Many2one('sale.order.const', string='Order Reference')
    sequence = fields.Integer(string="sequence", default=0)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    section_name = fields.Char(string='Section')
    discount_type = fields.Selection(related='order_id.discount_type', string="Discount Applies to")
    discount_method_section = fields.Selection([
                        ('fix', 'Fixed'),
                        ('per', 'Percentage')
                        ],string="Discount Method")
    discount_amount_section = fields.Float(string="Discount Amount")
    discount_subtotal_section = fields.Float(string="Discount Subtotal")
    subtotal_section = fields.Float(string='Subtotal')
    adjustment_amount = fields.Float(string='Adjustment')
    
    
    