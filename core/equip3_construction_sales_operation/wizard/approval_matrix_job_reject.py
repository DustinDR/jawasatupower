
from odoo import api , models, fields 
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

class ApprovalMatrixsaleReject(models.TransientModel):
    _name = 'approval.matrix.job.reject.const'

    reason = fields.Text(string="Reason")

    def action_cancel(self):
        job_id = self.env['job.estimate'].browse([self._context.get('active_id')])
        job_id.action_set_draft()
        job_id.action_request_for_approving()

    def action_confirm(self):
        job_id = self.env['job.estimate'].browse([self._context.get('active_id')])
        user = self.env.user
        approving_matrix_line = sorted(job_id.approved_matrix_ids.filtered(lambda r:not r.approved), key=lambda r:r.sequence)
        if approving_matrix_line:
            matrix_line = approving_matrix_line[0]
            name = matrix_line.state_char or ''
            if name != '':
                name += "\n • %s: Rejected" % (user.name)
            else:
                name += "• %s: Rejected" % (user.name)
            matrix_line.write({'state_char': name, 'time_stamp': datetime.now(), 'feedback': self.reason, 'last_approved': self.env.user})
            job_id.state = 'rejected'
            job_id.state_new = 'rejected'
            
