# -*- coding: utf-8 -*-

from odoo import _, api, fields, models


class variationOrderWarningWiz(models.TransientModel):
    _name = 'variation.order.warning'
    _description = 'Variation Order Wizard Warning'


    txt = fields.Text(string="Information",default="The main contract for this project has been created.\nDo you want to create job estimates for variation order?")

    def confirm_job(self):
        res = self._context.get('res_id', False)
        if res:
            job_id = self.env['job.estimate'].browse(res)
            sale_ordere_const = self.env['sale.order.const'].search([('project_id', '=', job_id.project_id.id), ('state', '=', 'sale')])
            job_cost_sheet = self.env['job.cost.sheet'].search([('project_id', '=', job_id.project_id.id)])
            if job_id:
                is_existed = job_id.search([('project_id', '=', job_id.project_id.id), ('state', '=', 'sale')])
                for rec in sale_ordere_const:
                    job_id.main_contract_ref = rec[0].id
                for rec in job_cost_sheet:
                    job_id.cost_sheet_ref = rec[0].id
                job_id.contract_category = 'var'
                
                job_id.write({'state': 'confirmed',
                              'state_new':'confirmed'
                             })
            
            #res_id=self.env['job.estimate'].browse(res)
            #if res_id:
            #    is_existed = res_id.search([('project_id', '=', res_id.project_id.id), ('state', '=', 'sale')])
            #    res_id.main_contract_ref = is_existed.sale_quotation_id.id
            #    res_id.contract_category = 'var'


    def clear_job_est(self):
        res = self._context.get('res_id', False)
        if res:
            res_id = self.env['job.estimate'].browse(res)
            if res_id:
                res_id.main_contract_ref = False
                res_id.contract_category = 'main'





