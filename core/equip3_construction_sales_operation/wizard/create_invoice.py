from odoo import _, api, fields, models
from datetime import datetime


class CreateinvoiceWiz(models.TransientModel):
    _name = 'create.invoice.wiz'
    _description = 'Create Invoice Wizard'

    invoice_method = fields.Selection([
            ('regular', 'Regular'),
            ('progressive', 'Progressive'),
            ],string="Create Invoice", default='progressive')
    regular_method = fields.Selection([
            ('regular', 'Regular Invoice'),
            ('down_payment', 'Down Payment'),
            ('retention1', 'Retention 1'),
            ('retention2', 'Retention 2')
            ],string="Regular", default='regular')

    def create_invoice(self):
        order = self.env['sale.order.const'].browse(self._context.get('active_ids'))
        if self.invoice_method == 'regular':
            form_view = self.env.ref('account.view_move_form')
            search_account = self.env['account.account'].sudo().search([('code', '=', '400000')], limit=1)
            invoice_line_ids = []
            if order.order_line_ids:
                unique_products = []
                for product_line in order.order_line_ids:
                    if product_line.product_id and product_line.product_id.id not in unique_products:
                        unique_products.append(product_line.product_id.id)
                if len(unique_products) > 0:
                    for product in unique_products:
                        lines = order.order_line_ids.filtered(lambda p: p.product_id and p.product_id.id == product)
                        unique_desc = []
                        for desc_line in lines:
                            if desc_line.description not in unique_desc:
                                unique_desc.append(desc_line.description)
                        if len(unique_desc) > 0:
                            for descp in unique_desc:
                                prd = order.order_line_ids.filtered(lambda p: p.product_id and p.product_id.id == product and p.description == descp)
                                line = prd[0]
                                invoice_line_ids.append((0, 0, {
                                    'product_id': line.product_id and line.product_id.id or False,
                                    'variable': line.variable and line.variable.id or False,
                                    'name': line.description,
                                    'analytic_tag_ids': line.analytic_idz and [(6, 0, line.analytic_idz.ids)] or False,
                                    'quantity': sum(prd.mapped('quantity')),
                                    'product_uom_id': line.uom_id and line.uom_id.id or False,
                                    'price_unit': line.unit_price,
                                    'tax_ids': line.line_tax_id and [(6, 0, line.line_tax_id.ids)] or False,
                                    'price_tax': sum(prd.mapped('amount_tax_line')),
                                    'price_subtotal': sum(prd.mapped('subtotal')),
                                    'adjustment_line': sum(prd.mapped('adjustment_line')),
                                    'discount_line': sum(prd.mapped('discount_line')),
                                    'account_id': search_account and search_account.id or False
                                }))
                                
            invoice_values = {
                'project_invoice': 'regular',
                'ref': order.client_order_ref,
                'move_type': 'out_invoice',
                'invoice_origin': order.name,
                'invoice_user_id': order.user_id.id,
                'partner_id': order.partner_invoice_id and order.partner_invoice_id.id or False,
                'project_id': order.project_id and order.project_id.id or False,
                'sale_order_ref': order.id,
                'fiscal_position_id': (order.fiscal_position_id or order.fiscal_position_id.get_fiscal_position(order.partner_id.id)).id,
                'analytic_group_ids': order.analytic_idz and [(6, 0, order.analytic_idz.ids)] or False,
                'invoice_payment_term_id': order.payment_term_id and order.payment_term_id.id or False,
                'adjustment_type_ref': order.adjustment_type,
                'discount_type_ref': order.discount_type,
                'adjustment_scope': order.adjustment_scope,
                'discount_scope': order.discount_scope,
                'adjustment_section': order.adjustment_section,
                'discount_section': order.discount_section,
                'adjustment_global': order.adjustment_sub,
                'discount_global': order.discount_sub,
                'adjustment_subline': order.line_adjustment,
                'discount_subline': order.line_discount,
                'partner_bank_id': order.company_id.partner_id.bank_ids[:1].id,
                'team_id': order.team_id.id,
                'campaign_id': order.campaign_id.id,
                'medium_id': order.medium_id.id,
                'source_id': order.source_id.id,
                'invoice_line_ids': invoice_line_ids
            }
            move = self.env['account.move'].sudo().create(invoice_values)

            return {
                "name": "Invoice",
                "type": "ir.actions.act_window",
                "res_model": "account.move",
                "res_id": move.id,
                'views': [(form_view.id, 'form')],
                "target": "current",
            }
        else:
            form_view = self.env.ref('equip3_construction_accounting_operation.progressive_claim_view_form')
            return {
                "name": "Progressive Claim",
                "type": "ir.actions.act_window",
                "res_model": "progressive.claim",
                "context": {'default_project_id': order.project_id and order.project_id.id or False},
                'views': [(form_view.id, 'form')],
                "target": "current",
            }

        