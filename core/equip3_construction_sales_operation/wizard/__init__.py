# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import variation_order_confirm_wiz
from . import approval_matrix_sale_reject
from . import approval_matrix_job_reject