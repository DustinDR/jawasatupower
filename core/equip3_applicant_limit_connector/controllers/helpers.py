# -*- coding: utf-8 -*-

STATUS_CODE = {
    200: "OK", 201: "Created", 202: "Accepted", 301: "Moved Permanently", 304: "Not Modified",
    307: "Temporary Redirect", 400: "Bad Request", 401: "Unauthorized", 403: "Forbidden", 404: "Not Found",
    405: "Method Not Acceptable", 406: "Unacceptable", 413: "Payload too large", 415: "Unsupported Media Type",
    429: "Too Many Requests", 500: "Internal Server Error", 501: "Not Implemented", 503: "Service Unavailable"
}

PAGE_DATA_LIMIT = 5
