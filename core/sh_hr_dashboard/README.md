About
============
Do you want to show all the employee details on the dashboard? So here it is. we have made a materialize dashboard for employees and managers. The dashboard is a powerful way to keep all details in a single bucket. An HR dashboard is a type of graphical user interface that often provides a glance view relevant to a particular objective or business process of HR. HR dashboard displays all information related to human resources in different menus. "HRMS dashboard" is the most advanced HR management system. You can quickly access all information about the employees. This includes different analysis and very helpful for the managers. Here you can easily manage your employees with various services such as Leave details, Attendance analysis, Contracts report, Employee expenses, Payslip counter, Greetings Menu (Birthday & Anniversary), Announcement menu.

User Guide
============
Blog: https://www.softhealer.com/blog/odoo-2/post/hr-dashboard-597

Installation
============
1) Copy module files to addon folder.
2) Restart odoo service (sudo service odoo-server restart).
3) Go to your odoo instance and open apps (make sure to activate debug mode).
4) click on update app list.
5) search module name and hit install button.

Any Problem with module?
=====================================
Please create your ticket here https://softhealer.com/support

Softhealer Technologies Doubt/Inquiry/Sales/Customization Team
=====================================
Skype: live:softhealertechnologies
What's app: +917984575681
E-Mail: support@softhealer.com
Website: https://softhealer.com
