from odoo import api , fields , models, _

class ResCompany(models.Model):
    _inherit = 'res.company'

    accounting = fields.Boolean(string="Accounting")

class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    is_invoice_approval_matrix = fields.Boolean(string="Invoice Approval Matrix")
    is_bill_approval_matrix = fields.Boolean(string="Bill Approval Matrix")
    is_other_income_approval_matrix = fields.Boolean(string="Other Income Approval Matrix")
    is_other_expense_approval_matrix = fields.Boolean(string="Other Expense Approval Matrix")

    is_customer_multi_receipt_approval_matrix = fields.Boolean(string="Customer Multi Receipt Approval Matrix")
    is_vendor_multipayment_approval_matrix = fields.Boolean(string="Vendor Multi Payment Approval Matrix")
    is_receipt_approval_matrix = fields.Boolean(string="Receipt Approval Matrix")
    is_payment_approval_matrix = fields.Boolean(string="Payment Approval Matrix")
    is_payment_voucher_approval_matrix = fields.Boolean(string="Payment Voucher Approval Matrix")

    accounting = fields.Boolean(string="Acccounting", related='company_id.accounting', readonly=False)

    is_cost_price_per_warehouse = fields.Boolean(string="Cost Price Per Warehouse")

    group_is_invoice_approval_matrix = fields.Boolean(string="Invoice Approval Matrix",
        implied_group='equip3_accounting_accessright_setting.group_is_invoice_approval_matrix')
    group_is_bill_approval_matrix = fields.Boolean(string="Bill Approval Matrix",
        implied_group='equip3_accounting_accessright_setting.group_is_bill_approval_matrix')
    group_is_other_income_approval_matrix = fields.Boolean(string="Other Income Approval Matrix",
        implied_group='equip3_accounting_accessright_setting.group_is_other_income_approval_matrix')
    group_is_other_expense_approval_matrix = fields.Boolean(string="Other Expense Approval Matrix",
        implied_group='equip3_accounting_accessright_setting.group_is_other_expense_approval_matrix')
    group_is_customer_multi_receipt_approval_matrix = fields.Boolean(string="Customer Multi Receipt Approval Matrix",
        implied_group='equip3_accounting_accessright_setting.group_is_customer_multi_receipt_approval_matrix')
    group_is_vendor_multipayment_approval_matrix = fields.Boolean(string="Vendor Multi Payment Approval Matrix",
        implied_group='equip3_accounting_accessright_setting.group_is_vendor_multipayment_approval_matrix')
    group_is_receipt_approval_matrix = fields.Boolean(string="Receipt Approval Matrix",
        implied_group='equip3_accounting_accessright_setting.group_is_receipt_approval_matrix')
    group_is_payment_approval_matrix = fields.Boolean(string="Payment Approval Matrix",
        implied_group='equip3_accounting_accessright_setting.group_is_payment_approval_matrix')
    group_is_payment_voucher_approval_matrix = fields.Boolean(string="Payment Voucher Approval Matrix",
        implied_group='equip3_accounting_accessright_setting.group_is_payment_voucher_approval_matrix')
        
    module_om_account_budget = fields.Boolean(string='Budget Management')
    
    
    @api.onchange('module_om_account_budget')
    def onchange_module_om_account_budget(self):
        if self.module_om_account_budget:
            self.group_analytic_accounting = True

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        res.update({
            'is_invoice_approval_matrix': IrConfigParam.get_param('is_invoice_approval_matrix', False),
            'is_bill_approval_matrix': IrConfigParam.get_param('is_bill_approval_matrix', False),
            'is_other_income_approval_matrix': IrConfigParam.get_param('is_other_income_approval_matrix', False),
            'is_other_expense_approval_matrix': IrConfigParam.get_param('is_other_expense_approval_matrix', False),
            'is_payment_voucher_approval_matrix': IrConfigParam.get_param('is_payment_voucher_approval_matrix', False),
            'is_cost_price_per_warehouse': IrConfigParam.get_param('is_cost_price_per_warehouse', False),

            'is_customer_multi_receipt_approval_matrix': IrConfigParam.get_param('is_customer_multi_receipt_approval_matrix', False),
            'is_vendor_multipayment_approval_matrix': IrConfigParam.get_param('is_vendor_multipayment_approval_matrix', False),
            'is_receipt_approval_matrix': IrConfigParam.get_param('is_receipt_approval_matrix', False),
            'is_payment_approval_matrix': IrConfigParam.get_param('is_payment_approval_matrix', False),
            'accounting': IrConfigParam.get_param('accounting', False),

            # 'group_is_invoice_approval_matrix': IrConfigParam.get_param('is_invoice_approval_matrix', False),
            # 'group_is_bill_approval_matrix': IrConfigParam.get_param('is_bill_approval_matrix', False),
            # 'group_is_other_income_approval_matrix': IrConfigParam.get_param('is_other_income_approval_matrix', False),
            # 'group_is_other_expense_approval_matrix': IrConfigParam.get_param('is_other_expense_approval_matrix', False),
            # 'group_is_payment_voucher_approval_matrix': IrConfigParam.get_param('is_payment_voucher_approval_matrix', False),
            # 'group_is_customer_multi_receipt_approval_matrix': IrConfigParam.get_param('is_customer_multi_receipt_approval_matrix', False),
            # 'group_is_vendor_multipayment_approval_matrix': IrConfigParam.get_param('is_vendor_multipayment_approval_matrix', False),
            # 'group_is_receipt_approval_matrix': IrConfigParam.get_param('is_receipt_approval_matrix', False),
            # 'group_is_payment_approval_matrix': IrConfigParam.get_param('is_payment_approval_matrix', False),
        })
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param('is_invoice_approval_matrix', self.is_invoice_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_bill_approval_matrix', self.is_bill_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_other_income_approval_matrix', self.is_other_income_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_other_expense_approval_matrix', self.is_other_expense_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_payment_voucher_approval_matrix', self.is_payment_voucher_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_cost_price_per_warehouse', self.is_cost_price_per_warehouse)

        self.env['ir.config_parameter'].sudo().set_param('is_customer_multi_receipt_approval_matrix', self.is_customer_multi_receipt_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_vendor_multipayment_approval_matrix', self.is_vendor_multipayment_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_receipt_approval_matrix', self.is_receipt_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('is_payment_approval_matrix', self.is_payment_approval_matrix)
        self.env['ir.config_parameter'].sudo().set_param('accounting', self.accounting)

        # self.env['ir.config_parameter'].sudo().set_param('group_is_invoice_approval_matrix', self.is_invoice_approval_matrix)
        # self.env['ir.config_parameter'].sudo().set_param('group_is_bill_approval_matrix', self.is_bill_approval_matrix)
        # self.env['ir.config_parameter'].sudo().set_param('group_is_other_income_approval_matrix', self.is_other_income_approval_matrix)
        # self.env['ir.config_parameter'].sudo().set_param('group_is_other_expense_approval_matrix', self.is_other_expense_approval_matrix)
        # self.env['ir.config_parameter'].sudo().set_param('group_is_payment_voucher_approval_matrix', self.is_payment_voucher_approval_matrix)
        # self.env['ir.config_parameter'].sudo().set_param('group_is_customer_multi_receipt_approval_matrix', self.is_customer_multi_receipt_approval_matrix)
        # self.env['ir.config_parameter'].sudo().set_param('group_is_vendor_multipayment_approval_matrix', self.is_vendor_multipayment_approval_matrix)
        # self.env['ir.config_parameter'].sudo().set_param('group_is_receipt_approval_matrix', self.is_receipt_approval_matrix)
        # self.env['ir.config_parameter'].sudo().set_param('group_is_payment_approval_matrix', self.is_payment_approval_matrix)

    
    @api.onchange('is_invoice_approval_matrix',
                  'is_bill_approval_matrix',
                  'is_other_income_approval_matrix',
                  'is_other_expense_approval_matrix',
                  'is_customer_multi_receipt_approval_matrix',
                  'is_vendor_multipayment_approval_matrix',
                  'is_receipt_approval_matrix',
                  'is_payment_approval_matrix',
                  'is_payment_voucher_approval_matrix')
    def onchange_aproval_matrix(self):
        self.group_is_invoice_approval_matrix = self.is_invoice_approval_matrix
        self.group_is_bill_approval_matrix = self.is_bill_approval_matrix
        self.group_is_other_income_approval_matrix = self.is_other_income_approval_matrix
        self.group_is_other_expense_approval_matrix = self.is_other_expense_approval_matrix
        self.group_is_customer_multi_receipt_approval_matrix = self.is_customer_multi_receipt_approval_matrix
        self.group_is_vendor_multipayment_approval_matrix = self.is_vendor_multipayment_approval_matrix
        self.group_is_receipt_approval_matrix = self.is_receipt_approval_matrix
        self.group_is_payment_approval_matrix = self.is_payment_approval_matrix
        self.group_is_payment_voucher_approval_matrix = self.is_payment_voucher_approval_matrix

