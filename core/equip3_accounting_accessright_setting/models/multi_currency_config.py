from odoo import fields, models

class ResCompany(models.Model):
    _inherit = "res.company"

    unrealized_exchange_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string="Unrealized Exchange Journal",
        domain="[('company_id', '=', company_id), ('type', '=', 'general')]")
    income_unrealized_exchange_account_id = fields.Many2one(
        comodel_name="account.account",
        string="Unrealized Gain Account",
        domain=lambda self: "[('internal_type', '=', 'other'), ('deprecated', '=', False), ('company_id', '=', company_id),\
                             ('user_type_id', 'in', %s)]" % [self.env.ref('account.data_account_type_revenue').id,
                                                             self.env.ref('account.data_account_type_other_income').id])
    expense_unrealized_exchange_account_id = fields.Many2one(
        comodel_name="account.account",
        string="Unrealized Loss Account",
        domain=lambda self: "[('internal_type', '=', 'other'), ('deprecated', '=', False), ('company_id', '=', company_id),\
                             ('user_type_id', '=', %s)]" % self.env.ref('account.data_account_type_expenses').id)


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'
    
    unrealized_exchange_journal_id = fields.Many2one(
        comodel_name='account.journal',
        related='company_id.unrealized_exchange_journal_id', 
        readonly=False,
        string="Unrealized Exchange Journal",
        domain="[('company_id', '=', company_id), ('type', '=', 'general')]")
    income_unrealized_exchange_account_id = fields.Many2one(
        comodel_name="account.account",
        related="company_id.income_unrealized_exchange_account_id",
        string="Unrealized Gain Account",
        readonly=False,
        domain=lambda self: "[('internal_type', '=', 'other'), ('deprecated', '=', False), ('company_id', '=', company_id),\
                             ('user_type_id', 'in', %s)]" % [self.env.ref('account.data_account_type_revenue').id,
                                                             self.env.ref('account.data_account_type_other_income').id])
    expense_unrealized_exchange_account_id = fields.Many2one(
        comodel_name="account.account",
        related="company_id.expense_unrealized_exchange_account_id",
        string="Unrealized Loss Account",
        readonly=False,
        domain=lambda self: "[('internal_type', '=', 'other'), ('deprecated', '=', False), ('company_id', '=', company_id),\
                             ('user_type_id', '=', %s)]" % self.env.ref('account.data_account_type_expenses').id)