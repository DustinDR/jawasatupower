from odoo import api, fields, models, modules, _


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    deposit_reconcile_journal_id = fields.Many2one('account.journal', string="Reconcile Journal")
    journal_id = fields.Many2one('account.journal', string="Payment Method")
    deposit_account_id = fields.Many2one('account.account', string="Advance Account")
    petty_cash_expense_account_id = fields.Many2one('account.account', string="Petty Cash Expense Account", domain=[
        ('user_type_id.name', 'in', ('Expenses', 'Cost of Revenue'))])
    overdue_template = fields.Text(string='Overdue Payments Message')
    tax_rounding_type = fields.Selection([
        ('round_type_up', 'Tax Round Up'),('round_type_down','Tax Round Down'),('round_type_normal','Tax Round Half-up')
    ], string='Tax Rounding Type')

    is_credit_note_approval_matrix = fields.Boolean(string='Credit Note Approval Matrix')
    is_refund_approval_matrix = fields.Boolean(string='Refund Approval Matrix')
    is_customer_deposit_approval_matrix = fields.Boolean(string='Customer Deposit Approval Matrix')
    is_vendor_deposit_approval_matrix = fields.Boolean(string='Vendor Deposit Approval Matrix')
    is_receipt_giro_approval_matrix = fields.Boolean(string='Receipt Giro Approval Matrix')
    is_payment_giro_approval_matrix = fields.Boolean(string='Payment Giro Approval Matrix')
    is_internal_transfer_approval_matrix = fields.Boolean(string='Internal Bank/Cash Approval Matrix')
    is_purchase_currency_approval_matrix = fields.Boolean(string='Purchase Currency Approval Matrix')
    is_internal_transaction = fields.Boolean(string="Use Internal Company Transaction")

    reminder_interval_before = fields.Integer(string='Before Due Date')
    reminder_interval_before_unit = fields.Selection([
        ('days', 'Days'),
        ('weeks', 'Weeks')
    ], string='')
    reminder_notification_before = fields.Integer(string='')
    
    reminder_interval_after = fields.Integer(string='After Maturity')
    reminder_interval_after_unit = fields.Selection([
        ('days', 'Days'),
        ('weeks', 'Weeks')
    ], string='')
    reminder_notification_after = fields.Integer(string='')

    group_is_credit_note_approval_matrix = fields.Boolean(string='Credit Note Approval Matrix',
                                                          implied_group='equip3_accounting_accessright_setting.group_is_credit_note_approval_matrix')
    group_is_refund_approval_matrix = fields.Boolean(string='Refund Approval Matrix',
                                                     implied_group='equip3_accounting_accessright_setting.group_is_refund_approval_matrix')
    group_is_customer_deposit_approval_matrix = fields.Boolean(string='Customer Deposit Approval Matrix',
                                                               implied_group='equip3_accounting_accessright_setting.group_is_customer_deposit_approval_matrix')
    group_is_vendor_deposit_approval_matrix = fields.Boolean(string='Vendor Deposit Approval Matrix',
                                                             implied_group='equip3_accounting_accessright_setting.group_is_vendor_deposit_approval_matrix')
    group_is_receipt_giro_approval_matrix = fields.Boolean(string='Receipt Giro Approval Matrix',
                                                           implied_group='equip3_accounting_accessright_setting.group_is_receipt_giro_approval_matrix')
    group_is_payment_giro_approval_matrix = fields.Boolean(string='Payment Giro Approval Matrix',
                                                           implied_group='equip3_accounting_accessright_setting.group_is_payment_giro_approval_matrix')
    group_is_internal_transfer_approval_matrix = fields.Boolean(string='Internal Bank/Cash Approval Matrix',
                                                                implied_group='equip3_accounting_accessright_setting.group_is_internal_transfer_approval_matrix')
    group_is_purchase_currency_approval_matrix = fields.Boolean(string='Purchase Currency Approval Matrix',
                                                                implied_group='equip3_accounting_accessright_setting.group_is_purchase_currency_approval_matrix')
    group_account_asset_category_fiscal = fields.Boolean(string='Fiscal Asset Group',
                                                                implied_group='equip3_accounting_accessright_setting.group_account_asset_category_fiscal')

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        ICP = self.env['ir.config_parameter'].sudo()
        res.update(
            deposit_reconcile_journal_id=int(ICP.get_param('deposit_reconcile_journal_id')),
            journal_id=int(ICP.get_param('journal_id')),
            deposit_account_id=int(ICP.get_param('deposit_account_id'))

        )

        res.update({
            'overdue_template': ICP.get_param('overdue_template',
                                              'Dear Sir/Madam,\n\nOur records indicate that some payments on your account are still due. Please find details below. If the amount has already been paid, please disregard this notice. Otherwise, please forward us the total amount stated below. If you have any queries regarding your account, please contact us.\n\nThank you in advance for your cooperation.\nBest Regards,'),
            'is_credit_note_approval_matrix': ICP.get_param('is_credit_note_approval_matrix', False),
            'is_refund_approval_matrix': ICP.get_param('is_refund_approval_matrix', False),
            'is_customer_deposit_approval_matrix': ICP.get_param('is_customer_deposit_approval_matrix', False),
            'is_vendor_deposit_approval_matrix': ICP.get_param('is_vendor_deposit_approval_matrix', False),
            'is_receipt_giro_approval_matrix': ICP.get_param('is_receipt_giro_approval_matrix', False),
            'is_payment_giro_approval_matrix': ICP.get_param('is_payment_giro_approval_matrix', False),
            'is_internal_transfer_approval_matrix': ICP.get_param('is_internal_transfer_approval_matrix', False),
            'is_purchase_currency_approval_matrix': ICP.get_param('is_purchase_currency_approval_matrix', False),
            'is_internal_transaction': ICP.get_param('is_internal_transaction', False),
            'reminder_interval_before': ICP.get_param('reminder_interval_before', 1),
            'reminder_interval_after': ICP.get_param('reminder_interval_after', 1),
            'reminder_interval_before_unit': ICP.get_param('reminder_interval_before_unit', 'days'),
            'reminder_interval_after_unit': ICP.get_param('reminder_interval_after_unit', 'days'),
            'reminder_notification_before': ICP.get_param('reminder_notification_before', 1),
            'reminder_notification_after': ICP.get_param('reminder_notification_after', 1),
            'petty_cash_expense_account_id': int(ICP.get_param('petty_cash_expense_account_id'))

            # 'group_is_credit_note_approval_matrix':ICP.get_param('is_credit_note_approval_matrix', False),
            # 'group_is_refund_approval_matrix':ICP.get_param('is_refund_approval_matrix', False),
            # 'group_is_customer_deposit_approval_matrix':ICP.get_param('is_customer_deposit_approval_matrix', False),
            # 'group_is_vendor_deposit_approval_matrix':ICP.get_param('is_vendor_deposit_approval_matrix', False),
            # 'group_is_receipt_giro_approval_matrix':ICP.get_param('is_receipt_giro_approval_matrix', False),
            # 'group_is_payment_giro_approval_matrix':ICP.get_param('is_payment_giro_approval_matrix', False),
            # 'group_is_internal_transfer_approval_matrix':ICP.get_param('is_internal_transfer_approval_matrix', False),
            # 'group_is_purchase_currency_approval_matrix':ICP.get_param('is_purchase_currency_approval_matrix', False),
        })
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        ISP = self.env['ir.config_parameter'].sudo()
        ISP.set_param('deposit_reconcile_journal_id', self.deposit_reconcile_journal_id.id)
        ISP.set_param('journal_id', self.journal_id.id)
        ISP.set_param('deposit_account_id', self.deposit_account_id.id)
        ISP.set_param('petty_cash_expense_account_id', int(self.petty_cash_expense_account_id.id))
        ISP.set_param('overdue_template', self.overdue_template)

        ISP.set_param('is_credit_note_approval_matrix', self.is_credit_note_approval_matrix)
        ISP.set_param('is_refund_approval_matrix', self.is_refund_approval_matrix)
        ISP.set_param('is_customer_deposit_approval_matrix', self.is_customer_deposit_approval_matrix)
        ISP.set_param('is_vendor_deposit_approval_matrix', self.is_vendor_deposit_approval_matrix)
        ISP.set_param('is_receipt_giro_approval_matrix', self.is_receipt_giro_approval_matrix)
        ISP.set_param('is_payment_giro_approval_matrix', self.is_payment_giro_approval_matrix)
        ISP.set_param('is_internal_transfer_approval_matrix', self.is_internal_transfer_approval_matrix)
        ISP.set_param('is_purchase_currency_approval_matrix', self.is_purchase_currency_approval_matrix)
        ISP.set_param('is_internal_transaction', self.is_internal_transaction)
        ISP.set_param('reminder_interval_before', self.reminder_interval_before)
        ISP.set_param('reminder_interval_before_unit', self.reminder_interval_before_unit)
        ISP.set_param('reminder_notification_before', self.reminder_notification_before)
        ISP.set_param('reminder_interval_after', self.reminder_interval_after)
        ISP.set_param('reminder_interval_after_unit', self.reminder_interval_after_unit)
        ISP.set_param('reminder_notification_after', self.reminder_notification_after)

        # ISP.set_param('group_is_credit_note_approval_matrix', self.is_credit_note_approval_matrix)
        # ISP.set_param('group_is_refund_approval_matrix', self.is_refund_approval_matrix)
        # ISP.set_param('group_is_customer_deposit_approval_matrix', self.is_customer_deposit_approval_matrix)
        # ISP.set_param('group_is_vendor_deposit_approval_matrix', self.is_vendor_deposit_approval_matrix)
        # ISP.set_param('group_is_receipt_giro_approval_matrix', self.is_receipt_giro_approval_matrix)
        # ISP.set_param('group_is_payment_giro_approval_matrix', self.is_payment_giro_approval_matrix)
        # ISP.set_param('group_is_internal_transfer_approval_matrix', self.is_internal_transfer_approval_matrix)
        # ISP.set_param('group_is_purchase_currency_approval_matrix', self.is_purchase_currency_approval_matrix)


    @api.model
    def default_get(self, rec):
        res = super(ResConfigSettings, self).default_get(rec)

        if res['deposit_reconcile_journal_id']:
            val1 = res['deposit_reconcile_journal_id']
        else:
            val1 = self.env['account.journal'].search([('code', '=', 'CA')], limit=1).id

        if res['deposit_account_id']:
            val2 = res['deposit_account_id']
        else:
            val2 = self.env['account.account'].search([('code', '=', '11210090')], limit=1).id

        if res['journal_id']:
            val3 = res['journal_id']
        else:
            val3 = self.env['account.journal'].search([('code', '=', 'CSH1')], limit=1).id

        res.update({
            'deposit_reconcile_journal_id': val1,
            'deposit_account_id': val2,
            'journal_id': val3
        })
        return res

    @api.onchange('is_credit_note_approval_matrix',
                  'is_refund_approval_matrix',
                  'is_customer_deposit_approval_matrix',
                  'is_vendor_deposit_approval_matrix',
                  'is_receipt_giro_approval_matrix',
                  'is_payment_giro_approval_matrix',
                  'is_internal_transfer_approval_matrix',
                  'is_purchase_currency_approval_matrix', )
    def onchange_matrix(self):
        self.group_is_credit_note_approval_matrix = self.is_credit_note_approval_matrix
        self.group_is_refund_approval_matrix = self.is_refund_approval_matrix
        self.group_is_customer_deposit_approval_matrix = self.is_customer_deposit_approval_matrix
        self.group_is_vendor_deposit_approval_matrix = self.is_vendor_deposit_approval_matrix
        self.group_is_receipt_giro_approval_matrix = self.is_receipt_giro_approval_matrix
        self.group_is_payment_giro_approval_matrix = self.is_payment_giro_approval_matrix
        self.group_is_internal_transfer_approval_matrix = self.is_internal_transfer_approval_matrix
        self.group_is_purchase_currency_approval_matrix = self.is_purchase_currency_approval_matrix
