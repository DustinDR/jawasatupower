{
    'name': 'Accounting Access Right Setting',
    'version': '1.1.20',
    'category': 'Accounting',
    'author': 'Mochamad Rezki',
    'depends': [
        'account',
        'bi_sale_purchase_discount_with_tax',
        'equip3_general_setting',
        'app_account_superbar',
        'om_account_accountant'
    ],
    'data': [        
        'security/accounting_security.xml',
        'views/config_views.xml',
        'views/multi_currency_config_views.xml',
        'views/sales_discount_config_settings.xml',
        'views/accounting_setting.xml',
        'views/res_config_settings_views.xml',
        'data/cash_advance_config.xml'
    ],
    'demo': [],
    'auto_install': False,
    'installable': True,
    'application': True
}