# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from . import fleet_vehicle_book
from . import fleet_vehicle
from . import fleet_vehicle_contract
from . import fleet_vehicle_move
from . import fleet_vehicle_inquiry
from . import fleet_vehicle_inspection
from . import fleet_vehicle_order
from . import customer_feedback_system
from . import res_config_settings