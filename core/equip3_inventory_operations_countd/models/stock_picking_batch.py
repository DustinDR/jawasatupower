# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import _, api, fields, models
from datetime import date
from odoo import tools


class StockPickingBatch(models.Model):
    _inherit = 'stock.picking.batch'

    location_ids = fields.Many2many('stock.location', 'stock_location_picking_batch_rel', 'location_id', 'batch_id', string='Locations', required=True, tracking=True, states={'draft': [('readonly', False)]})
    warehouse_id = fields.Many2one('stock.warehouse', string="Warehouse")
    company_id = fields.Many2one(related='warehouse_id.company_id', store=True)
    branch_id = fields.Many2one('res.branch', related="warehouse_id.branch_id", string='Branch',
                                tracking=True,
                                readonly=True)
    filter_location_ids = fields.Many2many('stock.location', string='Location', compute='_get_filter_locations', store=False)
    picking_type_id = fields.Many2one(
        'stock.picking.type', 'Operation Type', check_company=True, copy=False,
        readonly=True, states={'draft': [('readonly', False)]}, tracking=True, 
        domain="['|', '|', ('default_location_src_id', 'in', location_ids), \
                ('default_location_dest_id', 'in', location_ids), \
                '&', ('default_location_src_id', 'in', location_ids), \
                ('default_location_dest_id', 'in', location_ids)]")
    scheduled_date = fields.Datetime(
        'Scheduled Date', tracking=True, copy=False, store=True, readonly=False, compute="_compute_scheduled_date",
        states={'done': [('readonly', True)], 'cancel': [('readonly', True)]},
        help="""Scheduled date for the transfers to be processed.
              - If manually set then scheduled date for all transfers in batch will automatically update to this date.
              - If not manually changed and transfers are added/removed/updated then this will be their earliest scheduled date
                but this scheduled date will not be set for all transfers in batch.""")
    partner_id = fields.Many2one(related="user_id.partner_id", string='Partner')
    stock_picking_batch_ids = fields.One2many('stock.picking.batch.line', 'picking_batch_id', string="Stock Picking Batch Line", compute='_product_by_same_locations', store=True)
    
    @api.model
    def create(self, vals):
        seq1 = self.env.ref('stock_picking_batch.seq_picking_batch').sudo()
        record_id = self.search([], limit=1, order='id desc')
        check_today = False
        if record_id and record_id.create_date.date() == date.today():
            check_today = True
        if check_today == True:
            seq = self.env['ir.sequence'].next_by_code('picking.batch')
            vals['name'] = seq
        else:
            seq1.sudo().write({'number_next_actual': 1})
            seq = self.env['ir.sequence'].next_by_code('picking.batch')
            vals['name'] = seq
        return super(StockPickingBatch, self).create(vals)

    @api.depends('warehouse_id')
    def _get_filter_locations(self):
        location_ids = []
        for record in self:
            if record.warehouse_id:
                location_obj = self.env['stock.location']
                store_location_id = record.warehouse_id.view_location_id.id
                addtional_ids = location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')])
                for location in addtional_ids:
                    if location.location_id.id not in addtional_ids.ids:
                        location_ids.append(location.id)
                child_location_ids = self.env['stock.location'].search([('id', 'child_of', location_ids), ('id', 'not in', location_ids)]).ids
                final_location = child_location_ids + location_ids
                record.filter_location_ids = [(6, 0, final_location)]
            else:
                record.filter_location_ids = [(6, 0, [])]

    @api.depends('move_ids', 'move_ids.product_uom_qty', 'move_ids.reserved_availability', 'move_ids.quantity_done')
    def _product_by_same_locations(self):
        for record in self:
            product_line_data = [(5, 0, 0)]
            temp_list = []
            line_list_vals = []
            for line in record.move_ids:
                if {'product_id': line.product_id.id, 'location_id': line.location_id.id} in temp_list:
                    filter_list = list(filter(lambda r: r.get('product_id') == line.product_id.id and r.get('location_id') == line.location_id.id,  line_list_vals))
                    if filter_list:
                        filter_list[0]['demand_qty'].append(line.product_uom_qty)
                        filter_list[0]['reserved_qty'].append(line.reserved_availability)
                        filter_list[0]['done_qty'].append(line.quantity_done)
                else:
                    temp_list.append({'product_id': line.product_id.id, 'location_id': line.location_id.id})
                    line_list_vals.append({
                        'product_id' : line.product_id.id,
                        'name' : line.product_id.name,
                        'location_id': line.location_id.id,
                        'demand_qty' : [line.product_uom_qty],
                        'reserved_qty' : [line.reserved_availability],
                        'done_qty' : [line.quantity_done],
                        'uom_id' : line.product_id.uom_id.id,
                    })
            for final_line in line_list_vals:
                final_line['demand_qty'] = sum(final_line['demand_qty'])
                final_line['reserved_qty'] = sum(final_line['reserved_qty'])
                final_line['done_qty'] = sum(final_line['done_qty'])
                product_line_data.append((0, 0, final_line))
            record.stock_picking_batch_ids = product_line_data

    @api.depends('company_id', 'location_ids', 'state')
    def _compute_allowed_picking_ids(self):
        allowed_picking_states = ['waiting', 'confirmed', 'assigned']
        # cancelled_batchs = self.env['stock.picking.batch'].search_read(
        #     [('state', '=', 'cancel')], ['id']
        # )
        # cancelled_batch_ids = [batch['id'] for batch in cancelled_batchs]
        for batch in self:
            domain_states = list(allowed_picking_states)
            # Allows to add draft pickings only if batch is in draft as well.
            # if batch.state == 'draft':
            #     domain_states.append('draft')
            domain = [
                ('company_id', '=', batch.company_id.id),
                # ('immediate_transfer', '=', False),
                ('state', 'in', domain_states),
                # '|',
                # '|',
                # ('batch_id', '=', False),
                # ('batch_id', '=', batch.id),
                # ('batch_id', 'in', cancelled_batch_ids),
            ]
            if batch.location_ids:
                domain += [('location_id', 'in', batch.location_ids.ids), ('picking_type_code', 'in', ('outgoing', 'internal'))]
            batch.allowed_picking_ids = self.env['stock.picking'].search(domain)

    def _get_street(self, partner):
        self.ensure_one()
        res = {}
        address = ''
        if partner.street:
            address = "%s" % (partner.street)
        if partner.street2:
            address += ", %s" % (partner.street2)
        # reload(sys)
        html_text = str(tools.plaintext2html(address, container_tag=True))
        data = html_text.split('p>')
        if data:
            return data[1][:-2]
        return False

    def _get_address_details(self, partner):
        self.ensure_one()
        res = {}
        address = ''
        if partner.city:
            address = "%s" % (partner.city)
        if partner.state_id.name:
            address += ", %s" % (partner.state_id.name)
        if partner.zip:
            address += ", %s" % (partner.zip)
        if partner.country_id.name:
            address += ", %s" % (partner.country_id.name)
        # reload(sys)
        html_text = str(tools.plaintext2html(address, container_tag=True))
        data = html_text.split('p>')
        if data:
            return data[1][:-2]
        return False

    def action_force_validate(self):
        for record in self:
            for line in record.move_ids:
                line.quantity_done = line.product_uom_qty
            record.action_assign()
            return record.action_done()

class StockMove(models.Model):
    _inherit = 'stock.move'

    description = fields.Char(related='product_id.display_name', string="Description")

class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'

    description = fields.Char(related='product_id.display_name', string="Description")

class StockMove(models.Model):
    _name = 'stock.picking.batch.line'

    product_id = fields.Many2one('product.product', string="Product")
    name = fields.Char(string="Description")
    location_id = fields.Many2one('stock.location', string="Source Location")
    demand_qty = fields.Float(string="Demand")
    reserved_qty = fields.Float(string="Reserved")
    done_qty = fields.Float(string="Done")
    uom_id = fields.Many2one('uom.uom', string="Unit of Measure")
    picking_batch_id = fields.Many2one('stock.picking.batch', string="Stock Picking Batch")


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    def _get_without_quantities_error_message(self):
        """ Returns the error message raised in validation if no quantities are reserved or done.
        The purpose of this method is to be overridden in case we want to adapt this message.

        :return: Translated error message
        :rtype: str
        """
        return _(
            'You cannot validate a transfer if no quantities are reserved nor done.'
            'To force the transfer, click Force Validate or switch in edit mode and encode the done quantities.'
        )