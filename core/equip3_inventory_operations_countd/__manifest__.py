# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Equip3 Operations Countd',
    'author': 'Hashmicro / Prince',
    'version': '1.1.7',
    'summary': 'Manage your Inventory tracking.',
    'depends': ['stock_picking_batch', 'branch', 'general_template'],
    'category': 'Inventory/Inventory',
    'data': [
        "data/ir_sequence.xml",
        'security/ir.model.access.csv',
        'views/batch_transfer_view.xml',
        'report/stock_picking_batch.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
