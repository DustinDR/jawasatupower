from odoo import models, fields, api


class MrpBomLine(models.Model):
    _inherit = 'mrp.bom.line'

    quality_point_ids = fields.Many2many('sh.qc.point', string='Quality Points')


class MrpRoutingWorkcenter(models.Model):
    _inherit = 'mrp.routing.workcenter'

    quality_point_ids = fields.Many2many('sh.qc.point', string='Quality Points')

