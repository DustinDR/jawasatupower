from odoo import models, _
from odoo.exceptions import ValidationError


class MrpPlan(models.Model):
    _name = 'mrp.plan'
    _inherit = ['mrp.plan', 'sh.mrp.qc.reuse']

    def action_done(self):
        for record in self:
            record.check_mandatory_qc()
        return super(MrpPlan, self).action_done()
