from odoo import models, _
from odoo.exceptions import ValidationError


class MrpProduction(models.Model):
    _name = 'mrp.production'
    _inherit = ['mrp.production', 'sh.mrp.qc.reuse']

    def _get_move_raw_values(self, product_id, product_uom_qty, product_uom, operation_id=False, bom_line=False):
        values = super(MrpProduction, self)._get_move_raw_values(product_id, product_uom_qty, product_uom, operation_id, bom_line)
        if bom_line is False:
            return values
        values['qc_point_ids'] = [(6, 0, bom_line.quality_point_ids.ids)]
        return values

    def _get_move_finished_values(self, product_id, product_uom_qty, product_uom, operation_id=False, byproduct_id=False):
        values = super(MrpProduction, self)._get_move_finished_values(product_id, product_uom_qty, product_uom, operation_id, byproduct_id)
        if product_id != self.product_id.id:
            return values
        operation_ids = self.bom_id.operation_ids
        values['qc_point_ids'] = [(6, 0, operation_ids.mapped('quality_point_ids').ids)]
        return values

    def button_mark_done(self):
        for record in self:
            record.check_mandatory_qc()
        return super(MrpProduction, self).button_mark_done()
