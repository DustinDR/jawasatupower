from odoo import models


class MrpConsumption(models.Model):
    _name = 'mrp.consumption'
    _inherit = ['mrp.consumption', 'sh.mrp.qc.reuse']
