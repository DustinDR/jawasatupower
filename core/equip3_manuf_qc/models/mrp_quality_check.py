from odoo import models, fields, api


class ShMrpQualityCheck(models.Model):
    _inherit = 'sh.mrp.quality.check'

    @api.model
    def create(self, values):
        records = super(ShMrpQualityCheck, self).create(values)
        records.set_fields()
        return records

    def write(self, values):
        result = super(ShMrpQualityCheck, self).write(values)
        if not self.env.context.get('bypass_set_fields'):
            self.set_fields()
        return result

    move_id = fields.Many2one('stock.move', string='Move')
    sh_consumption_id = fields.Many2one('mrp.consumption', string='Production Record')
    sh_workorder_id = fields.Many2one('mrp.workorder', string='Work Order')
    sh_mrp = fields.Many2one('mrp.production', string='Manufacturing Order')
    sh_plan_id = fields.Many2one('mrp.plan', string='Manufacturing Plan')

    product_type = fields.Selection(
        selection=[
            ('material', 'Material'),
            ('finished', 'Finished Good'),
            ('wip', 'WIP')
        ],
        string='Product Type',
        default='material'
    )
    description = fields.Char()
    wizard_id = fields.Many2one('mrp.qc.wizard', string='Wizard')

    def set_fields_from_move(self):
        self.ensure_one()
        move = self.move_id
        return self.with_context(bypass_set_fields=True).write({
            'sh_consumption_id': (move.mrp_consumption_id or move.mrp_consumption_finished_id).id,
            'sh_workorder_id': (move.mrp_workorder_component_id or move.workorder_id).id,
            'sh_mrp': (move.raw_material_production_id or move.production_id).id,
            'sh_plan_id': (move.raw_material_production_id or move.production_id).mrp_plan_id.id
        })

    def set_fields_from_consumption(self):
        self.ensure_one()
        cons = self.sh_consumption_id
        return self.with_context(bypass_set_fields=True).write({
            'sh_workorder_id': cons.workorder_id.id,
            'sh_mrp': cons.manufacturing_order_id.id,
            'sh_plan_id': cons.manufacturing_plan.id
        })

    def set_fields(self):
        for record in self:
            if record.move_id:
                record.set_fields_from_move()
            else:
                record.set_fields_from_consumption()

    def action_inspect(self, mode='main', message=False, sh_norm=0.0):
        self.ensure_one()
        if not message:
            message = self.control_point_id.sh_instruction

        context = self.env.context.copy()
        context.update({
            'default_mode': mode,
            'default_check_id': self.id,
            'default_point_id': self.control_point_id.id,
            'default_message': message,
            'default_measure': sh_norm
        })

        action = self.env.ref('equip3_manuf_qc.action_view_mrp_quality_inspect_wizard').read()[0]
        action['context'] = context
        return action
