from odoo import models, _


class MrpWorkorder(models.Model):
    _name = 'mrp.workorder'
    _inherit = ['mrp.workorder', 'sh.mrp.qc.reuse']

    def create_consumption(self):
        consumption_id = super(MrpWorkorder, self).create_consumption()
        for move in consumption_id.move_raw_ids | consumption_id.byproduct_ids | consumption_id.move_finished_ids:
            if move.qc_check_ids:
                move.qc_check_ids.write({'sh_consumption_id': consumption_id.id})
            if move.qc_alert_ids:
                move.qc_alert_ids.write({'consumption_id': consumption_id.id})
        return consumption_id
