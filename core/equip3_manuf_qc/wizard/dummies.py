from odoo import models


class Dummy1(models.TransientModel):
    _name = 'mrp.quality.alert.wizard'
    _description = 'MRP Quality Alert Wizard'


class Dummy2(models.TransientModel):
    _name = 'mrp.quality.alert.line.wizard'
    _description = 'MRP Quality Alert Line Wizard'
