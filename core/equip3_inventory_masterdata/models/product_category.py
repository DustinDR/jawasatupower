
from odoo import models, fields, api, _


class ProductCategory(models.Model):
    _inherit = 'product.category'

    product_ids = fields.One2many('product.product', 'categ_id', string="Products Category")
    stock_transfer_transit_account_id = fields.Many2one('account.account', string="Stock Transfer Transit Account",
        domain=[('user_type_id.name', 'ilike', 'Inventory')])
