from odoo import models
from odoo import models, fields, api, _


class StockMove(models.Model):
    _inherit = "stock.move"

    def _prepare_move_line_vals(self, quantity=None, reserved_quant=None):
        ctx = dict(self.env.context)
        if not self.env.context.get("not_create_interwarehouse_transfer"):
            ctx.update({'stock_move_id': self.id})
        else:
            ctx.pop("stock_move_id")
        ctx.pop("not_create_interwarehouse_transfer", None)
        self = self.with_context(ctx)

        res = super(StockMove, self)._prepare_move_line_vals(quantity=quantity, reserved_quant=reserved_quant)
        return res

    length = fields.Float(string="Length", compute='_cal_move_length', store=False)
    width = fields.Float(string="Width", compute='_cal_move_width', store=False)
    height = fields.Float(string="Height", compute='_cal_move_height', store=False)

    @api.depends('product_id', 'product_uom_qty', 'product_uom')
    def _cal_move_length(self):
        moves_with_length = self.filtered(lambda moves: moves.product_id.length > 0.00)
        for move in moves_with_length:
            move.length = (move.product_qty * move.product_id.length)
        (self - moves_with_length).length = 0

    @api.depends('product_id', 'product_uom_qty', 'product_uom')
    def _cal_move_width(self):
        moves_with_width = self.filtered(lambda moves: moves.product_id.width > 0.00)
        for move in moves_with_width:
            move.width = (move.product_qty * move.product_id.width)
        (self - moves_with_width).width = 0
    
    @api.depends('product_id', 'product_uom_qty', 'product_uom')
    def _cal_move_height(self):
        moves_with_height = self.filtered(lambda moves: moves.product_id.height > 0.00)
        for move in moves_with_height:
            move.height = (move.product_qty * move.product_id.height)
        (self - moves_with_height).height = 0