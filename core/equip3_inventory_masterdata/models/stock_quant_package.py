
import re
from odoo import models, fields, api, _


class StockQuantPackage(models.Model):
    _inherit = 'stock.quant.package'

    color = fields.Integer('Color Index')
    package_status = fields.Selection([
        ('packed', 'Packed'),
        ('partial', 'Partial'),
        ('empty', 'Empty')
    ], 'Status', compute="_compute_package_staus", store=True)

    def change_color_on_kanban(self):
        for record in self:
            color = 0
            if record.package_status == 'packed':
                color = 2
            elif record.package_status == 'partial':
                color = 5
            elif record.package_status == 'empty':
                color = 7
            record.color = color

    @api.model
    def action_package_unpack(self, vals):
        pass
