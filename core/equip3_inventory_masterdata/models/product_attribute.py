from odoo import models, fields, api, _

class StockProductAttribute(models.Model):
    _inherit = 'product.attribute'

    is_short_name = fields.Boolean(compute='compute_short_name')

    def compute_short_name(self):
        for sh in self.value_ids:
            s = ''.join(sh.name.split())
            sh.short_name = (s[:2]).upper()
        self.is_short_name = True

    # @api.onchange('value_ids')
    # def get_short_name(self):
    #     for record in self:
    #         if len(record.short_name) >= 2:
    #             record.short_name = (record.name[:2]).upper()

class ProductAttributeValue(models.Model):
    _inherit = 'product.attribute.value'

    @api.onchange('name')
    def get_short_name(self):
        for record in self:
            if record.name != False:
                s = ''.join(record.name.split())
                # s.replace(" ", "")
                print('s',s)
                record.short_name = (s[:2]).upper()
