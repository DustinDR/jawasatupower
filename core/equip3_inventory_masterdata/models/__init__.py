# -*- coding: utf-8 -*-

# from . import models
from . import product
from . import product_brand
from . import res_config
from . import warehouse
from . import stock_location
from . import product_category
from . import internal_transfer_approval_matrix
from . import material_approval_matrix
from . import stock_inventory_approval_matrix
from . import stock_scrap_approval_matrix
from . import barcode_labels
from . import product_attribute
from . import stock_putaway_rule
from . import stock_move
from . import res_users
from . import stock_quant_package