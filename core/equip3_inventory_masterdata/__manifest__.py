# -*- coding: utf-8 -*-
{
    'name': "Equip3 - Inventory Masterdata",

    'summary': """
        Manage your Inventory Master Data""",

    'description': """
        Manage your Inventory Master Data
    """,

    'author': "Hashmicro",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Inventory/Inventory',
    'version': '1.1.18',

    # any module necessary for this one to work correctly
    'depends': [
        'stock_account',
        'equip3_general_features',
        'equip3_inventory_accessright_setting',
        'app_stock_location_capacity',
        'sh_product_multi_barcode',
        'stock_3dview',
        'dynamic_product_bundle',
        'warehouse_stock_restrictions',
        'pragmatic_delivery_control_app'
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        # 'views/views.xml',
        "wizard/product_template_create_variant_wizard.xml",
        'views/product.xml',
        # 'views/templates.xml',
        'views/material_approval_matrix_view.xml',
        "views/internal_transfer_approval_matrix_view.xml",
        'views/stock_inventory_approval_matrix_view.xml',
        'views/stock_scrap_approval_matrix_view.xml',
        'views/stock_location_views.xml',
        'views/stock_quant_package_views.xml',
        'data/create_op_type.xml',
        # 'data/ir_sequence_data.xml',
        "views/assets.xml",
        "views/stock_warehouse_view.xml",
        "views/barcode_menu_views.xml",
        "views/barcode_label_report.xml",
        "views/product_attribute.xml",
        "views/stock_picking.xml",
        "wizard/barcode_labels.xml",
        "views/stock_putaway_rule_view.xml",
        "views/product_category_views.xml",
        "views/res_users_views.xml",
        "views/hide_menu.xml",
        "views/product_view.xml",
        "views/barcode_labels_views.xml",
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    "qweb": [
        "static/src/xml/templates.xml",
        "static/src/xml/location_removal_priority.xml",
        "static/src/xml/stock_quant_package.xml",
    ],
}
