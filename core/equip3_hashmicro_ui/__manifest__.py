# -*- coding: utf-8 -*-
{
   'name': 'Hashmicro Theme',
   'version': '1.2.11',
   'author': 'Hashmicro / Prince',
   'depends': ['sh_backmate_theme_adv'],
   'data': [
      "views/assets.xml",
   ],
   'qweb': [
      'static/src/xml/*.xml',
   ],
   'auto_install': True,
   'installable': True,
   'application': True
}
