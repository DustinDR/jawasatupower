odoo.define('equip3_hashmicro_ui.AppsMenu', function (require) {
    "use strict";

    var AppsMenu = require('web.AppsMenu');

    AppsMenu.include({
        start: function () {
            this._super.apply(this, arguments);
            var self = this;
            $('.parent_menu_select').find("option:selected").change();
        },
    });

});
