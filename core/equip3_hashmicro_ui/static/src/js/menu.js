odoo.define('equip3_hashmicro_ui.Menu', function (require) {
    "use strict";

    var Menu = require('web.Menu');
    var session = require('web.session');

    Menu.include({
        events: _.extend({}, Menu.prototype.events, {
            'change .parent_menu_option': '_onParentMenuChange',
            'click .action_menu': '_onMenuitemClick',
            'click .parent_menu_autocomplete': '_onInputClick',
            'click .search_dropdown': '_onInputClick',
            'input #appDrawerSearchInput': '_showFoundMenus',
        }),
        init: function (parent, menu_data) {
            var self = this;
            this._super.apply(this, arguments);
            this.currentMenu = false;
            this.lastrecordID = false;
            this.autoCompleteChange = false;
        },
        start: function () {
            this._super.apply(this, arguments);
            var self = this;
            var parentmenu = $('.parent_menu_select');
            var select = $("<select class='parent_menu_option'></select>");
            _.each(self._appsMenu._apps, function(value) {
                const newOption = document.createElement('option');
                $(newOption).appendTo(select);
                $(newOption).text(value.name);
                newOption.setAttribute('value', value.menuID);
                newOption.setAttribute('data-menu-id', value.menuID);
                newOption.setAttribute('data-menu-xmlid', value.xmlID);
                newOption.setAttribute('data-action-id', value.actionID);
            })
            select.appendTo(parentmenu);
            var $input = $('.parent_menu_autocomplete');
            $input.autocomplete({
                classes: {
                    "ui-autocomplete": "parent_menu_autocomplete_ui",
                },
                source: function (req, resp) {
                    const search = req.term.trim();
                    var filter_apps = self._appsMenu._apps.filter(k => k.name.toLowerCase().includes(search.toLowerCase()));
                    if (filter_apps.length) {
                        resp($.map(filter_apps, function (value, key) {
                           return {
                               label: value.name,
                               value: value.menuID
                           }
                       }));
                    } else {
                         resp($.map(self._appsMenu._apps, function (value, key) {
                            return {
                                label: value.name,
                                value: value.menuID
                            }
                        }));
                    }
                },
                select: function (event, ui) {
                    event.stopImmediatePropagation();
                    event.preventDefault();
                    $input.val('');
                    $input.val(ui.item.label);
                    self.autoCompleteChange = true;
                    $('.parent_menu_select').find("option:selected").removeAttr("selected");
                    $('.parent_menu_select option[data-menu-id='+ ui.item.value +']').attr('selected', 'selected');
                    $('.parent_menu_option').trigger('change');
                    return false;
                },
                focus: function (event) {
                    event.preventDefault();
                },
                maxResults: 5,
                minLength: 0
            }).autocomplete( "instance" )._renderItem = function( ul, item ) {
              return $( "<li><div><img src=/web/image?model=ir.ui.menu&amp;field=web_icon_data&amp;id="+item.value+"><span>"+item.label+"</span></div></li>" ).appendTo( ul );
            };
            $input.autocomplete("option", "position", { my : "left top", at: "left bottom" });
            $('.close_sidebar').on('click', this._onCloseSidebar.bind(this));
        },
        _onCloseSidebar: function(event) {
            if ($('body').hasClass('show_full_screen')) {
                $('body').removeClass('show_full_screen');
                $('.close_sidebar').css('left', '270px');
                $('.menu_image').css('left', '295px');
                $('.parent_menu_autocomplete_dropdown').css('left', '325px');
                $('.o_action_manager').removeClass('o_theme_manager');
                $('body').find('.hover_menuitem > .dropdown-menu:not(.ag_sidebar_submenu)').addClass('sh_backmate_theme_appmenu_div');
            }
            else {
                $('body').addClass('show_full_screen');
                $('.close_sidebar').css('left', '20px');
                $('.menu_image').css('left', '45px');
                $('.parent_menu_autocomplete_dropdown').css('left', '75px');
                $('.o_action_manager').addClass('o_theme_manager');
                $('body').find('.hover_menuitem > .dropdown-menu:not(.ag_sidebar_submenu)').removeClass('sh_backmate_theme_appmenu_div');
            }
        },
        _showFoundMenus: function(event) {
            var self = this;
            var search_term = $(event.target).val();
            var $current_ul = $('.show_ul');
            if ($current_ul.length  > 1) {
                $current_ul = $($current_ul[0])
            }
            var $hide_ul = $('.hide_menu');
            if (search_term != '') {
                if ($current_ul.length) {
                    $current_ul.addClass('hide_menu');
                    $current_ul.removeClass('show_ul');
                }
                else {
                    $current_ul = $hide_ul;
                }
                $current_ul.parent().find('.search_ul').removeClass('d-none');
                $current_ul.parent().find('.search_ul').html('');
                _.each($current_ul.find('a.action_menu > span:not(.fa)'), function(element){
                    var $element = $(element);
                    var menu_name = $element.text().trim().toLowerCase();
                    if (menu_name.includes(search_term.toLowerCase())) {
                        let menu_item = $(element).closest('a').clone(true);
                        let dropdown_header = $('<div class="dropdown-header">');
                        menu_item.appendTo(dropdown_header);
                        $current_ul.parent().find('.search_ul').append(dropdown_header);
                    }
                });
            } else {
                $hide_ul.addClass('show_ul');
                $hide_ul.removeClass('hide_menu');
                $hide_ul.parent().find('.search_ul').addClass('d-none');
                $hide_ul.parent().find('.search_ul').html('');
            }
        },
        _onInputClick: function(event) {
            $('.parent_menu_autocomplete').autocomplete("search", '');
        },
        _onParentMenuChange: function(event) {
            $('#appDrawerSearchInput').val('');
            $('#appDrawerSearchInput').trigger('input');
            var lastactionID = false;
            var lastmenuactionID = false;
            var lastmenuID = false;
            var lastrecordID = false;
            var lastMenu = false;
            var filter_menu = this._appsMenu._apps.filter(k => k.actionID !== actionID && k.menuID !== menuID);
            _.each(filter_menu, function(value) {
                $('a[data-menu-id=' + value.menuID + ']').addClass('d-none');
            });
            if (window.location.href.includes('#action=')) {
                var substring = window.location.href.split('#action=');
                if (substring.length && substring.length >= 1) {
                    let previous_action = substring[1].split('&');
                    let previous_record = substring[1].split('id=');
                    if (previous_action.length && previous_action.length >= 1) {
                        lastactionID = parseInt(previous_action[0]);
                    }
                    if (previous_record.length && previous_record.length >= 1) {
                        let previous_record_id = previous_record[1].split('&');
                        if (previous_record_id.length && previous_record_id.length >= 1) {
                            this.lastrecordID = previous_record_id[0];
                        }
                    }
                }
            }
            if (!window.location.href.includes('#action=') && !this.autoCompleteChange) {
                if ($('.parent_menu_select option').length) {
                    var first_option = $($('.parent_menu_select option')[0]);
                    first_option.attr('selected', 'selected');
                    var actionID = first_option.data('action-id');
                    var menuID = first_option.data('menu-id');
                    const menu = $('a[data-menu-id=' + menuID + ']');
                    menu.click();
                }
            }
            if (lastactionID && !this.autoCompleteChange) {
                lastMenu = $('a[data-action-id=' + lastactionID + ']');
                lastmenuactionID = lastMenu.data('action-id');
                lastmenuID = lastMenu.data('menu-id');
                if (!lastMenu.hasClass('direct_menu')) {
                    var parent_menu = lastMenu.parents('.cssmenu').find('.o_app2');
                    $('.parent_menu_select').find("option:selected").removeAttr("selected");
                    $('.parent_menu_select option[data-menu-id='+ parent_menu.data('menu-id') +']').attr('selected', 'selected');
                }
                else if (lastMenu.hasClass('direct_menu')) {
                    $('.parent_menu_select').find("option:selected").removeAttr("selected");
                    $('.parent_menu_select option[data-menu-id='+ lastMenu.data('menu-id') +']').attr('selected', 'selected');
                }
            }
            if (window.location.href.includes('&menu_id=') && !this.autoCompleteChange) {
                var substring = window.location.href.split('&menu_id=');
                if (substring.length && substring.length >= 1) { 
                    var Menu = parseInt(substring[1]);
                    if (!isNaN(Menu)) {
                        const menu = $('a[data-menu-id=' + Menu + ']');
                        var option_menu = $('.parent_menu_select option[data-menu-id='+ menu.data('menu-id') +']');
                        option_menu.attr('selected', 'selected');
                        var currentMenu = option_menu;
                    }
                    else {
                        var currentMenu = $('.parent_menu_select').find("option:selected");
                    }
                }
                else {
                    var currentMenu = $('.parent_menu_select').find("option:selected");
                }
            }
            else {
                var currentMenu = $('.parent_menu_select').find("option:selected");
            }
            var actionID = currentMenu.data('action-id');
            var menuID = currentMenu.data('menu-id');
            const menu = $('a[data-menu-id=' + menuID + ']');
            if (this.currentMenu) {
                $('a[data-menu-id=' + this.currentMenu.data('menu-id') + ']').click();
                this.currentMenu.click();
            }
            menu.click();
            var app = _.findWhere(this._appsMenu._apps, { actionID: actionID, menuID: menuID });
            if (lastmenuID && lastmenuactionID && 
                (event.originalEvent === undefined || 
                lastMenu.hasClass('action_menu')
                )) {
                this._trigger_menu_clicked(lastmenuID, lastmenuactionID);
                $(lastMenu).addClass('active');
            }
            else if (app !== undefined &&
                app.children !== undefined &&
                app.children.length && (event.originalEvent !== undefined || 
                event.isTrigger)) {
                if (menu.next().hasClass('show_ul')) {
                    var child_app = menu.next().find('.action_menu')[0];
                    var childactionID = $(child_app).data('action-id');
                    var childmenuID = $(child_app).data('menu-id');
                    this._trigger_menu_clicked(childmenuID, childactionID);
                    $(child_app).addClass('active');
                }
            }
            if (menu.hasClass('child_app')) {
                menu.addClass('d-none');
            }
            if (menu.hasClass('direct_menu')) {
                menu.removeClass('d-none');
            }
            this.currentMenu = currentMenu;
            session.lastmenuId = menuID;

            var xml_id = this.currentMenu.data('menu-xmlid');
            var img = $('a[data-menu-xmlid="' + xml_id + '"]').find('img')
            if (img.length) {
                var new_img = img.clone();
                $('.parent_menu_image').remove();
                new_img.addClass('parent_menu_image');
                new_img.insertBefore($('.parent_menu_autocomplete_dropdown'));
            }
            else if (img.length == 0 && $('.parent_menu_image').length) {
                $('.parent_menu_image').remove();
            }
            var text_length = currentMenu.text().length;
            var space_count = currentMenu.text().split(" ").length - 1;
            var final_length = text_length + space_count;

            if ($('.parent_menu_autocomplete').val() == '') {
                $('.parent_menu_autocomplete').val(currentMenu.text());
                $('.parent_menu_autocomplete').css('width',((final_length + 5) * 11) + 'px');
            }
            else{
                $('.parent_menu_autocomplete').css('width',((final_length + 5) * 11) + 'px');
            }
            $('.parent_menu_autocomplete').css('padding-left', '15px');
        },
        _onMenuitemClick: function(event) {
            if (event.originalEvent !== undefined) {
                var actionID = $(event.target).closest('.action_menu').data('action-id');
                var menuID = $(event.target).closest('.action_menu').data('menu-id');
                return this._trigger_menu_clicked(menuID, actionID);
            }
        },
    });



});
