odoo.define('equip3_hashmicro_ui.ListRenderer', function (require) {
    "use strict";

    var ListRenderer = require('web.ListRenderer');

    ListRenderer.include({
        _renderView: function() {
            var self = this;
            return this._super.apply(this, arguments).then(function () {
                if (self.getParent() !== undefined &&
                    self.getParent().getParent() !== undefined &&
                    self.getParent().getParent().getParent() !== undefined &&
                    self.getParent().getParent().getParent().menu !== undefined &&
                    self.state !== undefined && self.state.data !== undefined) {
                    var menu = self.getParent().getParent().getParent().menu;
                    var lastrecordID = menu.lastrecordID;
                    if (lastrecordID !== undefined && lastrecordID !== '') {
                        var record = self.state.data.filter(k => k.res_id == parseInt(lastrecordID));
                        if (record && record.length) {
                            var row = self.$el.find('tr.o_data_row[data-id="' + record[0].id + '"]');
                            if (row && row.length) {
                                setTimeout(
                                function() {
                                    menu.lastrecordID = '';
                                    row.click();
                                }, 1000)
                            }
                        }
                    }
                }
            });
        }
    });
});