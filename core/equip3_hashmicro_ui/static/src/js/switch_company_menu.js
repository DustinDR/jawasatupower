odoo.define('equip3_hashmicro_ui.SwitchCompanyMenu', function (require) {
    "use strict";

    var config = require('web.config');
    var core = require('web.core');
    var session = require('web.session');
    var SystrayMenu = require('web.SystrayMenu');
    var Widget = require('web.Widget');

    var _t = core._t;

    // if (session.display_switch_company_menu) {
    //     var Items = SystrayMenu.Items.filter(k => k.prototype.template !== "SwitchCompanyMenu");
    //     SystrayMenu.Items = Items;
    // }

    var AppsMenu = require('web.AppsMenu');
    var config = require('web.config');

    var AppsMenu = AppsMenu.include({

        events: _.extend({}, AppsMenu.prototype.events, {
            'click .dropdown-item[data-menu] span.switch_company_click': '_SwitchCompanyClick'
        }),

        init: function (parent, menuData) {
            this._super.apply(this, arguments);
            var self = this;

            this.isMobile = config.device.isMobile;

            this._allowed_company_ids = String(session.user_context.allowed_company_ids).split(',')
                                                         .map(function (id) {return parseInt(id);});

            this._user_companies = session.user_companies.allowed_companies;

            this._current_company = this._allowed_company_ids[0];

            this._current_company_name = _.find(session.user_companies.allowed_companies, function (company) {
                return company[0] === self._current_company;
            })[1];

            this._SwitchCompanyClick = _.debounce(this._SwitchCompanyClick, 1500, true);
        },

         _SwitchCompanyClick: function (ev) {
              var dropdownItem = $(ev.currentTarget).parent();
              var dropdownMenu = dropdownItem.parent();
              var companyID = dropdownItem.data('company-id');

              if (this.isMobile) {
                 dropdownMenu = dropdownMenu.parent();
              }
              var allowed_company_ids = [companyID];

              $(ev.currentTarget).attr('aria-pressed', 'true');
              session.setCompanies(companyID, allowed_company_ids);
         },


    });
    return AppsMenu;

});