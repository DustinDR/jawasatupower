# -*- coding: utf-8 -*-
{
    'name': "Equip3 CRM WhatsApp",
    'summary': """
        Posting message to Sale Quotation/CRM Chatter from ChatRoom """,
    'description': """
        This module manages these features :
        1. Post message to Sale Quotation oe_chatter from Chat Room
        2. Post message to CRM oe_chatter from Chat Room
    """,
    'author': "HashMicro / Muhammad Saleem",
    'website': 'www.hashmicro.com',
    'category': 'Sales/CRM',
    'version': '1.1.3',
    'depends': ['equip3_sale_operation', 'product', 'acrux_chat_sale'],
    'data': [
        'wizard/MessageWizard.xml',
        'views/sale_order_view.xml',
        'views/chatroom_view.xml',
        ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
