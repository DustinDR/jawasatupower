# -*- coding: UTF-8 -*-

{
    'name': 'Persistent and propagated view cache',
    'summary': '''This module will make persistent cache 
from Odoo generated views and 
propagetes them between odoo workers.''',
    'version': '1.0.2',
    'author': "Advanced Accountancy OÜ",
    'website': "https://www.advancedaccountancy.org/r/WBR",
    'license': 'OPL-1',
    'category': 'Extra Tools',
    'description': '''This module will make persistent cache 
from Odoo generated views and 
propagetes them between odoo workers.''',
    'installable': True,
    'auto_install': False,
    'price': 49.0,
    'currency': 'EUR',
    'images': [
        'static/description/banner.png',
        'static/description/icon.png',
    ],
}
