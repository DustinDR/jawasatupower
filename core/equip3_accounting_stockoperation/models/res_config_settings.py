from odoo import api, fields, models, modules, _


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    is_cost_price_per_warehouse = fields.Boolean(string="Is cost price per warehouse")

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        ICP = self.env['ir.config_parameter'].sudo()

        res.update({
            'is_cost_price_per_warehouse': ICP.get_param('is_cost_price_per_warehouse', False),
        })
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        ISP = self.env['ir.config_parameter'].sudo()
        ISP.set_param('is_cost_price_per_warehouse', self.is_cost_price_per_warehouse)
