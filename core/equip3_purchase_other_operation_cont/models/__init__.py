# -*- coding: utf-8 -*-

from . import models
from . import approval_matrix_direct_purchase
from . import res_config_settings
from . import product_template