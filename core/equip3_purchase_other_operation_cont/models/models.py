# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning
from datetime import datetime,timedelta

class Purchase(models.Model):
	_inherit = 'purchase.order'
	
	dp = fields.Boolean("Direct Purchase")
	journal_id = fields.Many2one('account.journal', 'Journal', domain=[('type', 'in', ['bank','cash'])])
	name_dp = fields.Char("DP")
	direct_approval_matrix_id = fields.Many2one('approval.matrix.direct.purchase',string="Direct Purchase Approval Matrix")
	is_approval_matrix_direct = fields.Boolean(compute="_compute_approval_matrix_direct", string="Approving Matrix")
	approved_matrix_direct_ids = fields.One2many('approval.matrix.direct.purchase.line', 'order_id', compute="_compute_approving_direct_matrix_lines", store=True, string="Approved Matrix")
	approved_matrix_direct_id = fields.Many2one('approval.matrix.direct.purchase.line', string="Approved Matrix")
	state_direct_on = fields.Selection(related="state", tracking=False)
	state_direct_off = fields.Selection(related="state", tracking=False)
	is_approve_button_direct = fields.Boolean("Show Approve Button")
	control = fields.Boolean("Control")
	qty_limit = fields.Float("Qty Limit")
	budget_limit = fields.Float("Budget Limit")

	@api.constrains('state')
	def set_inv_move(self):
		for rec in self:
			rec._compute_invoice()

	def action_approve_direct(self):
		for record in self:
			user = self.env.user
			if record.is_approve_button_direct and record.approved_matrix_direct_id:
				approval_matrix_line_id = record.approved_matrix_direct_id
				if user.id in approval_matrix_line_id.user_ids.ids and \
						user.id not in approval_matrix_line_id.approved_users.ids:
					name = approval_matrix_line_id.state_char or ''
					if name != '':
						name += "\n • %s: Approved" % (self.env.user.name)
					else:
						name += "• %s: Approved" % (self.env.user.name)

					approval_matrix_line_id.write({
						'last_approved': self.env.user.id, 'state_char': name,
						'approved_users': [(4, user.id)]})
					if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
						approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
						# next_approval_matrix_line_id = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
						# if next_approval_matrix_line_id and len(next_approval_matrix_line_id[0].approver) > 1:
						#     pass
			if len(record.approved_matrix_direct_ids) == len(record.approved_matrix_direct_ids.filtered(lambda r:r.approved)):
				record.write({'state': 'rfq_approved'})

	def set_is_approve_button(self):
		for record in self:
			matrix_line = sorted(record.approved_matrix_direct_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
			if len(matrix_line) == 0:
				record.is_approve_button_direct = False
				record.approved_matrix_direct_id = False
			elif len(matrix_line) > 0:
				matrix_line_id = matrix_line[0]
				if self.env.user.id in matrix_line_id.user_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
					record.is_approve_button_direct = True
					record.approved_matrix_direct_id = matrix_line_id.id
				else:
					record.is_approve_button_direct = False
					record.approved_matrix_direct_id = False
			else:
				record.is_approve_button_direct = False
				record.approved_matrix_direct_id = False

	def action_request_direct(self):
		for res in self:
			res.check_control()
			res.state = 'waiting_for_approve'

	def button_confirm(self):
		self.check_control()
		res = super(Purchase, self).button_confirm()
		return res

	def check_control(self):
		for rec in self:
			if rec.dp:
				IrConfigParam = self.env['ir.config_parameter'].sudo()
				control = IrConfigParam.get_param('equip3_purchase_other_operation_cont.direct_control')
				if control:
					budget_limit = IrConfigParam.get_param('equip3_purchase_other_operation_cont.budget_limit')
					if rec.amount_total > float(budget_limit):
						raise ValidationError(_("The amount total is cannot greater than %s") % budget_limit)


	@api.depends('direct_approval_matrix_id')
	def _compute_approving_direct_matrix_lines(self):
		if self.direct_approval_matrix_id and self.dp:
			data = [(5, 0, 0)]
			for record in self:
				if record.state in ('draft', 'sent'):
					counter = 1
					record.approved_matrix_direct_ids = []
					for line in record.direct_approval_matrix_id.approval_matrix_direct_purchase_line_ids:
						data.append((0, 0, {
							'sequence' : counter,
							'user_ids' : [(6, 0, line.user_ids.ids)],
							'minimum_approver' : line.minimum_approver,
						}))
						counter += 1
					record.approved_matrix_direct_ids = data
					record.set_is_approve_button()

	def approval_matrix_direct(self):
		IrConfigParam = self.env['ir.config_parameter'].sudo()
		approval = IrConfigParam.get_param('is_direct_purchase_approval_matrix')
		control = IrConfigParam.get_param('equip3_purchase_other_operation_cont.direct_control')
		qty_limit = IrConfigParam.get_param('equip3_purchase_other_operation_cont.qty_limit')
		budget_limit = IrConfigParam.get_param('equip3_purchase_other_operation_cont.budget_limit')
		for record in self:
			record.write({
				'is_approval_matrix_direct': approval,
				'control': control,
				'qty_limit': qty_limit,
				'budget_limit': budget_limit
			})

	@api.onchange('name')
	def onchange_purchase_name(self):
		self._compute_approval_matrix_direct()
		res = super(Purchase, self).onchange_purchase_name()
		return res

	@api.depends('amount_untaxed','approval_matrix_id')
	def _compute_approval_matrix_direct(self):
		for record in self:
			record.direct_approval_matrix_id = False
			record.approval_matrix_direct()
			if record.dp:
				record.write({
					'approval_matrix_id': False,
					'is_approval_matrix': False,
					'approved_matrix_ids': [(6, 0, [])]
				})
			if record.is_approval_matrix_direct and record.dp:
				approval_matrix_id = self.env['approval.matrix.direct.purchase'].search([
					('minimum_amt', '<=', record.amount_untaxed),
					('maximum_amt', '>=', record.amount_untaxed),
					('company_id', '=', record.company_id.id)], limit=1)
				record.direct_approval_matrix_id = approval_matrix_id and approval_matrix_id.id or False

	@api.model
	def create(self, vals):
		if 'dp' in vals:
			if vals['dp']:
				vals['name'] = self.env['ir.sequence'].next_by_code('direct.purchase.sequence.rfq')
		res = super(Purchase, self).create(vals)
		if res.dp:
			name = False
			if self.env['ir.config_parameter'].sudo().get_param('is_good_services_order') == 'True':
				if res.is_goods_orders:
					name = self.env['ir.sequence'].next_by_code('direct.purchase.sequence.dp.new.goods')
				elif res.is_services_orders:
					name = self.env['ir.sequence'].next_by_code('direct.purchase.sequence.dp.new.services')
			else:
				name = self.env['ir.sequence'].next_by_code('direct.purchase.sequence.dp.new')
			res.name = name
			res.name_dp = name
			res.approved_matrix_ids = [(6, 0, [])]
		return res

	@api.constrains('state')
	def set_direct_purch(self):
		if self.dp and self.state in ('purchase', 'done'):
			if self.name != self.name_dp and self.name_dp:
				self.name = self.name_dp
			if not self.sh_fully_ship and self.picking_ids:
				for picking in self.picking_ids:
					if picking.state != 'done':
						for line in picking.move_ids_without_package:
							line.quantity_done = line.product_uom_qty
						picking.button_validate()
		if not self.invoice_ids and self.sh_fully_ship:
			invoice = self.action_create_invoice()
			inv = self.invoice_ids
			inv.invoice_date = datetime.now()
			inv.action_post()
			payments = self.env['account.payment.register'].with_context(active_model='account.move', active_ids=self.invoice_ids.ids).create({
				'journal_id': self.journal_id.id,
				'payment_date': datetime.now().date(),
				'amount': inv.amount_total,
				'branch_id': self.env['res.branch'].search([('company_id', '=', self.env.company.id)], limit=1).id,
			})
			payments._create_payments()
			self = self.with_context(check_invoice_count=True)
		context = dict(self.env.context) or {}
		if self.invoice_ids and context.get('check_invoice_count'):
			self.with_context(check_invoice_count=False)._compute_invoice()

		
	
	def write(self, vals):
		if 'state' in vals and self.dp and 'name' in vals:
			if vals['state'] in ('purchase', 'done'):
				vals['name'] = self.name_dp
		res = super(Purchase, self).write(vals)
		# if self.dp and self.state in ('purchase', 'done'):
		# 	if self.name != self.name_dp and self.name_dp:
		# 		self.name = self.name_dp
		# 	if not self.sh_fully_ship and self.picking_ids:
		# 		for picking in self.picking_ids:
		# 			if picking.state != 'done':
		# 				for line in picking.move_ids_without_package:
		# 					line.quantity_done = line.product_uom_qty
		# 				picking.button_validate()
			# if not self.invoice_ids and self.sh_fully_ship:
			# 	invoice = self.action_create_invoice()
			# 	inv = self.invoice_ids
			# 	inv.invoice_date = datetime.now()
			# 	inv.action_post()
			# 	payments = self.env['account.payment.register'].with_context(active_model='account.move', active_ids=self.invoice_ids.ids).create({
			# 		'journal_id': self.journal_id.id,
			# 		'payment_date': datetime.now().date(),
			# 		'amount': inv.amount_total,
			# 		'branch_id': self.env['res.branch'].search([('company_id', '=', self.env.company.id)], limit=1).id,
			# 	})
			# 	payments._create_payments()
			# 	self = self.with_context(check_invoice_count=True)
			# context = dict(self.env.context) or {}
			# if self.invoice_ids and context.get('check_invoice_count'):
			# 	self.with_context(check_invoice_count=False)._compute_invoice()
		return res

class PurchaseOrderLine(models.Model):
	_inherit = "purchase.order.line"

	dp_line = fields.Boolean(related="order_id.dp")
	
	@api.onchange('dp_line')
	def onchange_product_type(self):
		product_domain = {}
		if self.env['ir.config_parameter'].sudo().get_param('is_good_services_order'):
			if self.dp_line and self.order_id.is_goods_orders:
				product_domain = {'domain': {'product_template_id': [('tracking', '=', 'none'), ('can_be_direct','=',True), ('type', 'in', ['consu', 'product']), ('purchase_ok', '=', True), '|', ('company_id', '=', False), ('company_id', '=', self.order_id.company_id.id)]}}
			elif self.dp_line and self.order_id.is_services_orders:
				product_domain = {'domain': {'product_template_id': [('tracking', '=', 'none'), ('can_be_direct','=',True), ('type', 'in', ['service']), ('purchase_ok', '=', True), '|', ('company_id', '=', False), ('company_id', '=', self.order_id.company_id.id)]}}
		else:
			if not self.dp_line:
				product_domain = {'domain': {'product_template_id': [('purchase_ok', '=', True), '|', ('company_id', '=', False), ('company_id', '=', self.order_id.company_id.id)]}}
			else:
				product_domain = {'domain': {'product_template_id': [('tracking', '=', 'none'), ('can_be_direct','=',True), ('purchase_ok', '=', True), '|', ('company_id', '=', False), ('company_id', '=', self.order_id.company_id.id)]}}
		return product_domain

	@api.onchange('product_qty')
	def check_qty_and_limit(self):
		for rec in self:
			if rec.order_id.dp:
				IrConfigParam = self.env['ir.config_parameter'].sudo()
				control = IrConfigParam.get_param('equip3_purchase_other_operation_cont.direct_control')
				if control:
					qty_limit = IrConfigParam.get_param('equip3_purchase_other_operation_cont.qty_limit')
					if rec.product_qty > float(qty_limit):
						raise ValidationError(_("The quantity is cannot greater than %s") % qty_limit)

class AccountPaymentRegister(models.TransientModel):
	_inherit = 'account.payment.register'
	
	branch_id = fields.Many2one('res.branch')