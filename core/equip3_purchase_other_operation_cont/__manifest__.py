# -*- coding: utf-8 -*-
{
    'name': "equip3_purchase_other_operation_cont",

    'summary': """
            Manage your Direct Purchase Activities""",

    'description': """
        equip3 purchase other operation cont
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Purchase/Purchase',
    'version': '1.1.12',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'purchase',
        'purchase_product_matrix',
        'equip3_purchase_operation',
        'purchase_last_price_info'
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/sequence_dp.xml',
        'report/report_quotation_template.xml',
        'views/views.xml',
        'views/templates.xml',
        "views/purchase_order_views.xml",
        'views/approval_matrix_direct_purchase_view.xml',
        'views/res_config_setting_view.xml',
        'views/direct_purchase_views.xml',
        'views/product_template_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}