# -*- coding: utf-8 -*-
{
    'name': "Equip3 Construction Purchase Operation",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Muhammad Andi Laksamana",
    'website': "http://www.hashmicro.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '1.1.12',

    # any module necessary for this one to work correctly
    'depends': ['base', 
                'purchase', 
                'equip3_purchase_operation',   
                'equip3_purchase_other_operation', 
                'sh_po_tender_management', 
                'equip3_inventory_operation', 
                'equip3_construction_sales_operation', 
                'equip3_construction_operation', 
                'purchase_request'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'wizard/purchase_agreement_views.xml',
        'wizard/create_bill_views.xml',
        'wizard/s_curve_view.xml',
        'views/orders_menu.xml',
        'views/material_request.xml',
        'views/material_request_line.xml',
        'views/material_request_approval_matrix_view.xml',
        'views/variable_estimate_view.xml',
        'views/purchase_request_view.xml',
        'views/rfq_view.xml',
        'views/line_assets.xml',
        'views/purchase_order_view.xml',
        'views/purchase_agreement_view.xml',
        'views/s_curve_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
