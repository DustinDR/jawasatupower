from odoo import _, api, fields, models
from datetime import datetime

class SCurve(models.TransientModel):
    _name = "s.curve"
    _description = "S-Curve"

    project = fields.Many2one("project.project", string="Project")
    work_orders = fields.Many2many("project.task", string="Work Orders")

    def create_scurve(self):
        return{
            'type': 'ir.actions.act_window',
            'res_model': 'construction.scurve',
            'view_mode': 'graph',
            'view_type': 'graph',
        }