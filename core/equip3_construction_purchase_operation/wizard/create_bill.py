from odoo import api, fields, models

class CreateBillWizard(models.TransientModel):
    _name = 'create.bill.wizard'
    _description = 'Create Bill'

    create_bill = fields.Selection(string='Create Bill', selection=[('regular', 'Regular'), ('progressive', 'Progressive'),])
    regular = fields.Selection(string='', 
                               selection=[
                               ('bill', 'Regular Bill'), 
                               ('down', 'Down Payment'), 
                               ('retention', 'Retention Rate'),])
    
    def button_create_bill(self):
        pass

    
