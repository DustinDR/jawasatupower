# -*- coding: utf-8 -*-

from typing_extensions import Required
from odoo import _, api, fields, models


class ProductUsageGenerate(models.TransientModel):
    _name = 'product.usage.generate.wizard'
    _description = 'Generate Product Usage'

    @api.model
    def default_get(self, fields):
        rec = super(ProductUsageGenerate, self).default_get(fields)
        lines = []
        ctx = self.env.context.copy()
        active_id = ctx.get('active_ids', [])
        material_id = self.env['material.request'].browse(active_id)
        for material in material_id.product_line:
            lines.append((0, 0, {
                'no': material.no,
                'product_id': material.product.id,
                'description': material.description,
                'uom_id': material.product_unit_measure.id,
                'source_location_id': material.destination_warehouse_id.id,
                'use_qty': material.quantity,
                'product_usage_id': material_id.id
            }))
        rec.update({'name': material_id.name, 'product_usage_line_ids': lines, })
        return rec

    name = fields.Char (string='Name', required=True)
    warehouse = fields.Many2one('stock.warehouse', string='Warehouse', required=True)
    responsible = fields.Many2one('res.users', string='Responsible', required=True)
    product_usage_line_ids = fields.One2many(
        'product.usage.generate.wizard.line', 'product_usage_id')

    def generate_product_usage(self):
        lines = []
        for line in self.product_usage_line_ids:
            lines.append((0, 0, {
                'location_id': line.source_location_id.id,
                'product_id': line.product_id.id,
                'scrap_qty': line.use_qty,
                'product_uom_id': line.uom_id.id,
            }))
        product_usage_id = self.env['stock.scrap.request'].create({
            'scrap_request_name': self.name,
            'warehouse_id' : self.warehouse.id,
            'responsible_id': self.responsible.id,
            'scrap_ids': lines,
        })
        return {
            'name': _('Product Usage'),
            'domain': [],
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'stock.scrap.request',
            'res_id': product_usage_id.id,
            'view_id': False,
            'views': [(self.env.ref('equip3_inventory_control.action_stock_product_usage').id, 'form')],
            'type': 'ir.actions.act_window'
        }


class ProductUsagetGenerateLine(models.TransientModel):
    _name = 'product.usage.generate.wizard.line'

    no = fields.Integer('No')
    product_id = fields.Many2one('product.product', string='Product')
    description = fields.Text(string='Description')
    uom_id = fields.Many2one('uom.uom', string='Unit of Measure')
    source_location_id = fields.Many2one('stock.location', string='Source Location')
    use_qty = fields.Float("Quantity To Use")
    product_usage_id = fields.Many2one('product.usage.generate.wizard')