from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.exceptions import ValidationError

import re

class MaterialRequest(models.Model):
    _inherit = 'material.request'

    type_of_mr = fields.Selection([('assets','Assets'),('material','Material'),('labour','Labour'),('overhead','Overhead'),('equipment','Equipment'),('vehicle','Vehicle')],
                                  string = "Type Of MR")
    request_to_departement = fields.Many2one(comodel_name = 'hr.department', string='Request to Department')
    project = fields.Many2one(comodel_name = 'project.project', string='Project')
    job_cost_sheet = fields.Many2one('job.cost.sheet', 'Job Cost Sheet')
    agreement_count = fields.Integer(compute='_compute_agreement_count',
                                     string='Agreement', compute_sudo=True)

    def _compute_agreement_count(self):
        for rec in self:
            dom = [('sh_source', '=', self.name)]
            rec.agreement_count = self.env['purchase.agreement'].search_count(
                dom)

    @api.onchange('project', 'department', 'type_of_mr', 'destination_warehouse_id','product_line')
    def onchange_approval_matrix(self):
        quntity_val =[]
        for rec in self.product_line:
            quntity_val.append(rec.quantity)
        total_quntity = sum(quntity_val)
        for rec in self:
            matrix_id = self.env['mr.approval.matrix'].search([('project', '=', self.project.id),
                                                               ('department', '=', self.request_to_departement.id),
                                                               ('type_of_mr', '=', self.type_of_mr),
                                                               ('warehouse_id', '=', self.destination_warehouse_id.id),
                                                               ('minimum_amount','<=',total_quntity),
                                                               ('maximum_amount','>=',total_quntity)])
            if matrix_id:
                rec.mr_approval_matrix_id = matrix_id[0].id
            else:
                rec.mr_approval_matrix_id = False                

class OrdersMaterialRequestLine(models.Model):
    _inherit = 'material.request.line'

    
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    budget_quantity = fields.Float('Budget Quantity')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product', compute='compute_groupofproduct', readonly=True)
    project_scope_computed = fields.Many2many('project.scope.line', string="computed scope lines", compute='get_scope_lines')

    #@api.onchange('project_scope')
    #def onchange_project_scope(self):
    #    if not self.material_request_id.project:
    #        raise ValidationError(_("Select Project First"))

    @api.onchange('product')
    def compute_groupofproduct(self):
        if self.product:
            self.group_of_product = self.product.group_of_product.id
    
    @api.depends('material_request_id')
    def get_scope_lines(self):
        for rec in self:
            if rec.material_request_id.project:
                rec.project_scope_computed = [(6, 0, rec.material_request_id.project.mapped("project_scope_line_ids").ids)]
                rec.project_scope_computed = [(6, 0, rec.material_request_id.project.mapped("section_ids").ids)]
            else:
                rec.project_scope_computed = [(6, 0, [])]

class MaterialRequestApprovalMatrix(models.Model):
    _inherit = 'mr.approval.matrix'

    project = fields.Many2one(comodel_name = 'project.project', string='Project', required=True)
    department = fields.Many2one(comodel_name = 'hr.department', string='Department', required=True)
    type_of_mr = fields.Selection([('material','Material'),('labour','Labour'),('assets','Assets'),('overhead','Overhead')],
                                  string = "Type Of MR", required=True)
    minimum_amount = fields.Float(string='Minimum Amount', required=True)
    maximum_amount = fields.Float(string='Maximum Amount', required=True)

    @api.constrains('project', 'department', 'type_of_mr', 'minimum_amount', 'maximum_amount',
                    'branch_id', 'warehouse_id')
    def _constraint_unique(self):
        rec = self.env['mr.approval.matrix'].search([('project', '=', self.project.id),
                                                     ('department', '=', self.department.id),
                                                     ('type_of_mr', '=', self.type_of_mr),
                                                     ('branch_id', '=', self.branch_id.id),
                                                     ('warehouse_id', '=', self.warehouse_id.id)])
        rec_minimum = rec.filtered(lambda r: r.minimum_amount <= self.minimum_amount and r.maximum_amount >= self.minimum_amount)
        rec_maximum = rec.filtered(lambda r: r.minimum_amount <= self.maximum_amount and r.maximum_amount >= self.maximum_amount)
        if len(rec_minimum) > 1 or len(rec_maximum) > 1:
            raise UserError(_("Record Already exist!"))