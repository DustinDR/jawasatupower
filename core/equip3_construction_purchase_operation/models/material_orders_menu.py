from odoo import models, fields, api, _
from odoo.exceptions import UserError

class RFQMaterialOrdersMenu(models.Model):
    _inherit = 'purchase.order'


    is_orders = fields.Boolean('Is Orders', default=False)

    @api.onchange('is_orders')
    def _onchange_is_orders(self):
        context = dict(self.env.context) or {}
        if context.get('orders'):
            self.is_orders = True


class RFQMaterialOrdersMenuLines(models.Model):
    _inherit = 'purchase.order.line'


    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    budget_quantity = fields.Float('Budget Quantity')
    budget_unit_price = fields.Float('Budget Unit Price')
    remining_budget_amount = fields.Float('Budget Amount Left')
    is_orders = fields.Boolean('Is Orders', default=False)

    @api.onchange('is_orders')
    def _onchange_is_orders(self):
        context = dict(self.env.context) or {}
        if context.get('orders'):
            self.is_orders = True