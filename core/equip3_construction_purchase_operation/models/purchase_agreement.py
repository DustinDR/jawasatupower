from odoo import models, fields, api, _
from datetime import datetime, timedelta
from odoo.exceptions import UserError , ValidationError
from odoo import tools

class PurchaseAgreement(models.Model):
    _inherit = 'purchase.agreement'

    def action_new_quotation3(self):
        for rec in self:
            po_obj = self.env['purchase.order']
            line_ids = []
            is_goods_orders = False
            is_subcontracting = False
            if rec.is_goods_orders:
                is_goods_orders = True
            elif rec.is_subcontracting:
                is_subcontracting = True
            for rec_line in rec.variable_line_ids:
                for ref in rec_line.cs_subcon_id:
                    line_vals = {
                        'cs_subcon_id': rec_line.cs_subcon_id.id,
                        'project_scope': rec_line.project_scope.id,
                        'section': rec_line.section.id,
                        'variable': rec_line.variable.id,
                        'quantity': rec_line.quantity,
                        'budget_quantity': rec_line.budget_qty,
                        'uom': rec_line.uom.id,
                        'sub_total': rec_line.reference_price,
                        'budget_amount': ref.price_unit,
                        'budget_amount_total': ref.budgeted_amt_left,
                    }
                line_ids.append((0, 0, line_vals))
            i = 0
            for vendor in self.partner_ids:
                vals = {
                    'partner_id': vendor.id,
                    'project': rec.project.id,
                    'cost_sheet': rec.cost_sheet.id,
                    'agreement_id': rec.id,
                    'origin': rec.name,
                    'is_goods_orders' : is_goods_orders,
                    'is_subcontracting': is_subcontracting,
                    'analytic_account_group_ids': rec.account_tag_ids.ids,
                    'branch_id': rec.branch_id.id,
                    'user_id': rec.sh_purchase_user_id.id,
                    'variable_line_ids': line_ids
                }
                po_obj.create(vals)
                i += 1
            if i and rec.state2 == 'pending':
                rec.state = 'bid_submission'
    