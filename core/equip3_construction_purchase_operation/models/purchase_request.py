from dataclasses import field
from email.policy import default
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime


class PurchaseRequest(models.Model):
    _inherit = 'purchase.request'

    project = fields.Many2one('project.project', 'Project')
    sub_contracting = fields.Selection([('main_contract', 'Main Contract'), ('addendum', 'Addendum')],
                                       string="Contract Type", required=True, default='main_contract')
    main_po_reference = fields.Many2one('purchase.order', 'Main PO Reference')
    material_request = fields.Many2one('material.request', 'Material Request')

    is_subcontracting = fields.Boolean('Is Subcontracting', default=False)
    is_material_orders = fields.Boolean('Is Material Orders', default=False)
    is_orders = fields.Boolean('Is Orders', default=False)

    @api.onchange('is_subcontracting')
    def _onchange_is_subcontracting(self):
        context = dict(self.env.context) or {}
        if context.get('services_good'):
            self.is_subcontracting = True

    @api.onchange('is_material_orders')
    def _onchange_is_material_orders(self):
        context = dict(self.env.context) or {}
        if context.get('goods_order'):
            self.is_material_orders = True

    @api.onchange('is_orders')
    def _onchange_is_orders(self):
        context = dict(self.env.context) or {}
        if context.get('orders'):
            self.is_orders = True

    variable_line_ids = fields.One2many('pr.variable.line', 'variable_id', string='Variable Line')
    material_line_ids = fields.One2many('pr.material.line', 'material_id', string='Material Line')
    service_line_ids = fields.One2many('pr.service.line', 'service_id', string='Service Line')
    equipment_line_ids = fields.One2many('pr.equipment.line', 'equipment_id')
    labour_line_ids = fields.One2many('pr.labour.line', 'labour_id', string='Labour Line')
    overhead_line_ids = fields.One2many('pr.overhead.line', 'overhead_id', string='Overhead Line')
    cost_sheet = fields.Many2one('job.cost.sheet', string='Cost Sheet')

    @api.onchange('variable_line_ids', 'variable_line_ids.variable')
    def _get_material_line(self):
        material = [(5, 0, 0)]
        services = [(5, 0, 0)]
        equipment = [(5, 0, 0)]
        labour = [(5, 0, 0)]
        overhead = [(5, 0, 0)]
        for rec in self.variable_line_ids:
            for mat in rec.variable.material_variable_ids:
                res = (0, 0, {
                    'project_scope': rec.project_scope.id,
                    'section': rec.section.id,
                    'variable': rec.variable.id,
                    'product': mat.product_id.id,
                    'description': mat.description,
                    'quantity': rec.quantity * mat.quantity,
                    'analytic_group': self.analytic_account_group_ids,
                    'uom': mat.uom_id,
                })
                material.append(res)

            for serv in rec.variable.service_variable_ids:
                res = (0, 0, {
                    'project_scope': rec.project_scope.id,
                    'section': rec.section.id,
                    'variable': rec.variable.id,
                    'product': serv.product_id.id,
                    'description': serv.description,
                    'quantity': rec.quantity * serv.quantity,
                    'analytic_group': self.analytic_account_group_ids,
                    'uom': serv.uom_id,
                })
                services.append(res)

            for equi in rec.variable.equipment_variable_ids:
                res = (0, 0, {
                    'project_scope': rec.project_scope.id,
                    'section': rec.section.id,
                    'variable': rec.variable.id,
                    'product': equi.product_id.id,
                    'description': equi.description,
                    'quantity': rec.quantity * equi.quantity,
                    'analytic_group': self.analytic_account_group_ids,
                    'uom': equi.uom_id,
                })
                equipment.append(res)

            for lab in rec.variable.labour_variable_ids:
                res = (0, 0, {
                    'project_scope': rec.project_scope.id,
                    'section': rec.section.id,
                    'variable': rec.variable.id,
                    'product': lab.product_id.id,
                    'description': lab.description,
                    'quantity': rec.quantity * lab.quantity,
                    'analytic_group': self.analytic_account_group_ids,
                    'uom': lab.uom_id,
                })
                labour.append(res)

            for over in rec.variable.overhead_variable_ids:
                res = (0, 0, {
                    'project_scope': rec.project_scope.id,
                    'section': rec.section.id,
                    'variable': rec.variable.id,
                    'product': over.product_id.id,
                    'description': over.description,
                    'quantity': rec.quantity * over.quantity,
                    'analytic_group': self.analytic_account_group_ids,
                    'uom': over.uom_id,
                })
                overhead.append(res)

        self.material_line_ids = material
        self.service_line_ids = services
        self.equipment_line_ids = equipment
        self.labour_line_ids = labour
        self.overhead_line_ids = overhead

    def button_to_approve(self):
        res = super(PurchaseRequest, self).action_confirm_purchase_request()
        for rec in self.variable_line_ids:
            reserved = 0.00
            for bud in rec.cs_subcon_id:
                if rec.quantity > bud.budgeted_qty_left:
                    raise ValidationError(_("The quantity is over the remaining budget"))
                else:
                    reserved = (bud.reserved_qty + rec.quantity)
                    for sub in self.cost_sheet:
                        sub.material_subcon_ids = [(1, rec.cs_subcon_id.id, {
                                 'reserved_qty': reserved,
                            })]
        return res

    def action_confirm_purchase_request(self):
        res = super(PurchaseRequest, self).action_confirm_purchase_request()
        for rec in self.variable_line_ids:
            reserved = 0.00
            for bud in rec.cs_subcon_id:
                if rec.quantity > bud.budgeted_qty_left:
                    raise ValidationError(_("The quantity is over the remaining budget"))
                else:
                    reserved = (bud.reserved_qty + rec.quantity)
                    for sub in self.cost_sheet:
                        sub.material_subcon_ids = [(1, rec.cs_subcon_id.id, {
                                 'reserved_qty': reserved,
                            })]
        return res

    def action_confirm_purchase_request_2(self):
        if not self.variable_line_ids:
                raise ValidationError("Please Enter Variable Data!")
        else:
            for rec in self.variable_line_ids:
                reserved = 0.00
                for bud in rec.cs_subcon_id:
                    if rec.quantity > bud.budgeted_qty_left:
                        raise ValidationError(_("The quantity is over the remaining budget"))
                    else:
                        reserved = (bud.reserved_qty + rec.quantity)
                        for record in self:
                            for sub in self.cost_sheet:
                                record.write({'state': 'approved', 'purchase_req_state': 'pending'})
                                sub.material_subcon_ids = [(1, rec.cs_subcon_id.id, {
                                        'reserved_qty': reserved,
                                    })]

    
    def button_cancel_pr(self):
        res = super(PurchaseRequest, self).button_cancel_pr()
        for rec in self.variable_line_ids:
            reserved = 0.00
            for bud in rec.cs_subcon_id:
                reserved = (bud.reserved_qty - rec.quantity)
                for sub in self.cost_sheet:
                    sub.material_subcon_ids = [(1, rec.cs_subcon_id.id, {
                            'reserved_qty': reserved,
                        })]
        return res
    
    def button_draft(self):
        res = super(PurchaseRequest, self).button_draft()
        for rec in self.variable_line_ids:
            reserved = 0.00
            for bud in rec.cs_subcon_id:
                reserved = (bud.reserved_qty - rec.quantity)
                for sub in self.cost_sheet:
                    sub.material_subcon_ids = [(1, rec.cs_subcon_id.id, {
                        'reserved_qty': reserved,
                    })]

        return res  
    
    def button_draft_cancel(self):
        res = super(PurchaseRequest, self).button_draft()
        return res       

    @api.onchange('cost_sheet')
    def _onchange_cost_sheet(self):
        self.variable_line_ids = [(5, 0, 0)] 
        self.analytic_account_group_ids = False
        if self.cost_sheet:
            cost_sheet = self.cost_sheet
            self.analytic_account_group_ids = cost_sheet.account_tag_ids.ids
        for rec in self.cost_sheet:
            for section in rec.material_subcon_ids:
                self.variable_line_ids = [(0, 0, {
                    'cs_subcon_id': section.id,
                    'project_scope': section.project_scope,
                    'section': section.section_name,
                    'variable': section.variable,
                    'budget_quantity': section.budgeted_qty_left,
                    'uom': section.uom_id
                })]


class VariableLine(models.Model):
    _name = 'pr.variable.line'
    _description = "Variable Line Purchase Request"
    _order = "sequence"

    sequence = fields.Integer('Sequence', default=1)
    cs_subcon_id = fields.Many2one('material.subcon', 'Subcon ID')
    sr_no = fields.Char('No.', compute="_sequence_ref")
    variable_id = fields.Many2one('purchase.request', 'Variable ID')
    variable = fields.Many2one('variable.template', 'Variable')
    budget_quantity = fields.Float('Budget Quantity')
    quantity = fields.Float('Quantity', default=1)
    uom = fields.Many2one('uom.uom', string='UoM', readonly=True, force_save="1")
    project_scope = fields.Many2one('project.scope.line', 'Project Scope', required=True, )
    section = fields.Many2one('section.estimate', 'Section', required=True)

    @api.onchange('variable')
    def onchange_variable(self):
        res = {}
        if not self.variable:
            return res
        self.uom = self.variable.variable_uom.id

    @api.onchange('project_scope')
    def onchange_project_scope(self):
        for rec in self:
            if self.project_scope:
                return {
                    'domain': {'variable': [('id', 'in', rec.variable_id.cost_sheet.material_subcon_ids.variable.ids)]}}
            else:
                return {
                    'domain': {'variable': [('id', '=', False)]}}

    @api.depends('variable_id.variable_line_ids', 'variable_id.variable_line_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.variable_id.variable_line_ids:
                no += 1
                l.sr_no = no

    @api.model
    def create(self, vals):
        vals['sr_no'] = self.env['ir.sequence'].next_by_code('variable_purchase_request') or ('New')
        res = super(VariableLine, self).create(vals)
        return res
        
class PurchaseRequestLine(models.Model):
    _inherit = 'purchase.request.line'

    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product', compute='compute_groupofproduct', readonly=True)
    product = fields.Many2one('product.product', 'Product', required=True)
    budget_quantity = fields.Float('Budget Quantity')
    is_orders = fields.Boolean('Is Orders', default=False)

    @api.onchange('is_orders')
    def _onchange_is_orders(self):
        context = dict(self.env.context) or {}
        if context.get('orders'):
            self.is_orders = True

    @api.onchange('product')
    def compute_groupofproduct(self):
        if self.product:
            self.group_of_product = self.product.group_of_product.id

class PRMaterialLine(models.Model):
    _name = 'pr.material.line'
    _description = "Purchase Request Material Line"

    material_id = fields.Many2one('purchase.request', 'Material ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product', compute='compute_groupofproduct', readonly=True)
    product = fields.Many2one('product.product', 'Product', required=True)
    description = fields.Char('Description')
    quantity = fields.Float('Quantity')
    budget_quantity = fields.Float('Budget Quantity')
    analytic_group = fields.Many2many('account.analytic.tag', string='Analytic Group', required=True)
    uom = fields.Many2one('uom.uom', 'UoM')
    request_date = fields.Date('Request Date', default=datetime.now(), required=True)
    estimated_cost = fields.Monetary(
        string="Estimated Cost",
        currency_field="currency_id",
        default=0.0,
        help="Estimated cost of Purchase Request Line, not propagated to PO.",
    )
    currency_id = fields.Many2one(related="company_id.currency_id", readonly=True)
    company_id = fields.Many2one('res.company', related='material_id.company_id',
                                 string='Company', store=True, readonly=True, default=lambda self: self.env.company)
    destination_warehouse = fields.Many2one('stock.warehouse', 'Destination Warehouse')
    purchased_qty = fields.Float('RFQ/PO Qty', readonly=True)
    purchase_status = fields.Selection(
        [('request_for_amendment', 'Request for Amendemnt'), ('draft', 'RFQ'), ('sent', 'RFQ Sent'),
         ('to_approve', 'To Approve'), ('waiting_for_approve', 'Waiting for Approval'),
         ('rfq_approved', 'RFQ Approved'),
         ('purchase', 'Purchase Order'), ('reject', 'Rejected'), ('done', 'Locked'),
         ('on_hold', 'On Hold'), ('cancel', 'Cancelled')],
        string="Purchase Status", readonly=True)

    @api.onchange('product')
    def onchange_product(self):
        res = {}
        if not self.product:
            return res
        self.quantity = 1.0
        self.uom = self.product.uom_id.id
    
    @api.onchange('product')
    def compute_groupofproduct(self):
        if self.product:
            self.group_of_product = self.product.group_of_product.id


class PRServiceLine(models.Model):
    _name = 'pr.service.line'
    _description = "Purchase Request Service Line"

    service_id = fields.Many2one('purchase.request', 'Service ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product', compute='compute_groupofproduct', readonly=True)
    product = fields.Many2one('product.product', 'Product', required=True)
    description = fields.Char('Description')
    quantity = fields.Float('Quantity')
    budget_quantity = fields.Float('Budget Quantity')
    analytic_group = fields.Many2many('account.analytic.tag', string='Analytic Group', required=True)
    uom = fields.Many2one('uom.uom', 'UoM')
    request_date = fields.Date('Request Date', default=datetime.now(),required=True)
    estimated_cost = fields.Monetary(
        string="Estimated Cost",
        currency_field="currency_id",
        default=0.0,
        help="Estimated cost of Purchase Request Line, not propagated to PO.",
    )
    currency_id = fields.Many2one(related="company_id.currency_id", readonly=True)
    company_id = fields.Many2one('res.company', related='service_id.company_id',
                                 string='Company', store=True, readonly=True, default=lambda self: self.env.company)
    destination_warehouse = fields.Many2one('stock.warehouse', 'Destination Warehouse')
    purchased_qty = fields.Float('RFQ/PO Qty', readonly=True)
    purchase_status = fields.Selection(
        [('request_for_amendment', 'Request for Amendemnt'), ('draft', 'RFQ'), ('sent', 'RFQ Sent'),
         ('to_approve', 'To Approve'), ('waiting_for_approve', 'Waiting for Approval'),
         ('rfq_approved', 'RFQ Approved'),
         ('purchase', 'Purchase Order'), ('reject', 'Rejected'), ('done', 'Locked'),
         ('on_hold', 'On Hold'), ('cancel', 'Cancelled')],
        string="Purchase Status", readonly=True)

    @api.onchange('product')
    def onchange_product(self):
        res = {}
        if not self.product:
            return res
        self.quantity = 1.0
        self.uom = self.product.uom_id.id
    
    @api.onchange('product')
    def compute_groupofproduct(self):
        if self.product:
            self.group_of_product = self.product.group_of_product.id


class PREquipmentLine(models.Model):
    _name = 'pr.equipment.line'
    _description = "Purchase Request Equipment Line"

    equipment_id = fields.Many2one('purchase.request', 'Equipment ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product', compute='compute_groupofproduct', readonly=True)
    product = fields.Many2one('product.product', 'Product', required=True)
    description = fields.Char('Description')
    quantity = fields.Float('Quantity')
    budget_quantity = fields.Float('Budget Quantity')
    analytic_group = fields.Many2many('account.analytic.tag', string='Analytic Group', required=True)
    uom = fields.Many2one('uom.uom', 'UoM')
    request_date = fields.Date('Request Date', default=datetime.now(), required=True)
    estimated_cost = fields.Monetary(
        string="Estimated Cost",
        currency_field="currency_id",
        default=0.0,
        help="Estimated cost of Purchase Request Line, not propagated to PO.",
    )
    currency_id = fields.Many2one(related="company_id.currency_id", readonly=True)
    company_id = fields.Many2one('res.company', related='equipment_id.company_id',
                                 string='Company', store=True, readonly=True, default=lambda self: self.env.company)
    destination_warehouse = fields.Many2one('stock.warehouse', 'Destination Warehouse')
    purchased_qty = fields.Float('RFQ/PO Qty', readonly=True)
    purchase_status = fields.Selection(
        [('request_for_amendment', 'Request for Amendemnt'), ('draft', 'RFQ'), ('sent', 'RFQ Sent'),
         ('to_approve', 'To Approve'), ('waiting_for_approve', 'Waiting for Approval'),
         ('rfq_approved', 'RFQ Approved'),
         ('purchase', 'Purchase Order'), ('reject', 'Rejected'), ('done', 'Locked'),
         ('on_hold', 'On Hold'), ('cancel', 'Cancelled')],
        string="Purchase Status", readonly=True)

    @api.onchange('product')
    def onchange_product(self):
        res = {}
        if not self.product:
            return res
        self.quantity = 1.0
        self.uom = self.product.uom_id.id
    
    @api.onchange('product')
    def compute_groupofproduct(self):
        if self.product:
            self.group_of_product = self.product.group_of_product.id


class PRLabourLine(models.Model):
    _name = 'pr.labour.line'
    _description = "Purchase Request Labour Line"

    labour_id = fields.Many2one('purchase.request', 'Labour ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product', compute='compute_groupofproduct', readonly=True)
    product = fields.Many2one('product.product', 'Product', required=True)
    description = fields.Char('Description')
    quantity = fields.Float('Quantity')
    budget_quantity = fields.Float('Budget Quantity')
    analytic_group = fields.Many2many('account.analytic.tag', string='Analytic Group', required=True)
    uom = fields.Many2one('uom.uom', 'UoM')
    request_date = fields.Date('Request Date', default=datetime.now(), required=True)
    estimated_cost = fields.Monetary(
        string="Estimated Cost",
        currency_field="currency_id",
        default=0.0,
        help="Estimated cost of Purchase Request Line, not propagated to PO.",
    )
    currency_id = fields.Many2one(related="company_id.currency_id", readonly=True)
    company_id = fields.Many2one('res.company', related='labour_id.company_id',
                                 string='Company', store=True, readonly=True, default=lambda self: self.env.company)
    destination_warehouse = fields.Many2one('stock.warehouse', 'Destination Warehouse')
    purchased_qty = fields.Float('RFQ/PO Qty', readonly=True)
    purchase_status = fields.Selection(
        [('request_for_amendment', 'Request for Amendemnt'), ('draft', 'RFQ'), ('sent', 'RFQ Sent'),
         ('to_approve', 'To Approve'), ('waiting_for_approve', 'Waiting for Approval'),
         ('rfq_approved', 'RFQ Approved'),
         ('purchase', 'Purchase Order'), ('reject', 'Rejected'), ('done', 'Locked'),
         ('on_hold', 'On Hold'), ('cancel', 'Cancelled')],
        string="Purchase Status", readonly=True)

    @api.onchange('product')
    def onchange_product(self):
        res = {}
        if not self.product:
            return res
        self.quantity = 1.0
        self.uom = self.product.uom_id.id
    
    @api.onchange('product')
    def compute_groupofproduct(self):
        if self.product:
            self.group_of_product = self.product.group_of_product.id


class PROverheadLine(models.Model):
    _name = 'pr.overhead.line'
    _description = "Purchase Request Overhead Line"

    overhead_id = fields.Many2one('purchase.request', 'Overhead ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    group_of_product = fields.Many2one('group.of.product', string='Group of Product', compute='compute_groupofproduct', readonly=True)
    product = fields.Many2one('product.product', 'Product', required=True)
    description = fields.Char('Description')
    quantity = fields.Float('Quantity')
    budget_quantity = fields.Float('Budget Quantity')
    analytic_group = fields.Many2many('account.analytic.tag', string='Analytic Group', required=True)
    uom = fields.Many2one('uom.uom', 'UoM')
    request_date = fields.Date('Request Date', default=datetime.now(), required=True)
    estimated_cost = fields.Monetary(
        string="Estimated Cost",
        currency_field="currency_id",
        default=0.0,
        help="Estimated cost of Purchase Request Line, not propagated to PO.",
    )
    currency_id = fields.Many2one(related="company_id.currency_id", readonly=True)
    company_id = fields.Many2one('res.company', related='overhead_id.company_id',
                                 string='Company', store=True, readonly=True, default=lambda self: self.env.company)
    destination_warehouse = fields.Many2one('stock.warehouse', 'Destination Warehouse')
    purchased_qty = fields.Float('RFQ/PO Qty', readonly=True)
    purchase_status = fields.Selection(
        [('request_for_amendment', 'Request for Amendemnt'), ('draft', 'RFQ'), ('sent', 'RFQ Sent'),
         ('to_approve', 'To Approve'), ('waiting_for_approve', 'Waiting for Approval'),
         ('rfq_approved', 'RFQ Approved'),
         ('purchase', 'Purchase Order'), ('reject', 'Rejected'), ('done', 'Locked'),
         ('on_hold', 'On Hold'), ('cancel', 'Cancelled')],
        string="Purchase Status", readonly=True)

    @api.onchange('product')
    def onchange_product(self):
        res = {}
        if not self.product:
            return res
        self.quantity = 1.0
        self.uom = self.product.uom_id.id
    
    @api.onchange('product')
    def compute_groupofproduct(self):
        if self.product:
            self.group_of_product = self.product.group_of_product.id
