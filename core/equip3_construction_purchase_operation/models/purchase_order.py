from dataclasses import field
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class PurchaseRequest(models.Model):
    _inherit = 'purchase.order'


    main_po_reference = fields.Many2one('purchase.order', 'Main PO Reference')
    addendum_payment_method = fields.Selection([('join_payment','Join Payment'), ('split_payment','Split Payment')], string = "Addendum Payment Method", default='join_payment')

    def button_confirm2(self):
        for rec in self.variable_line_ids:
            reserved = 0.00
            for bud in rec.cs_subcon_id:
                if rec.sub_total > bud.price_unit:
                    raise ValidationError(_("The unit price is over the budget unit price"))
                elif rec.total > bud.budgeted_amt_left:
                    raise ValidationError(_("The total amount is over the budget amount left"))
                elif rec.quantity > rec.budget_quantity:
                    raise ValidationError(_("The quantity is over the remaining budget"))
                else:
                    reserved = (bud.reserved_amt + rec.total)
                    for sub in self.cost_sheet:
                        sub.material_subcon_ids = [(1, rec.cs_subcon_id.id, {
                                 'reserved_amt': reserved,
                            })]
    
    def action_create_bill_2(self):
        pass