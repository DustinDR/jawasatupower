from http.client import TOO_MANY_REQUESTS
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
from odoo.tools.misc import formatLang, get_lang
import odoo.addons.decimal_precision as dp

class RequestForQuotations(models.Model):
    _inherit =  'purchase.order'


    project = fields.Many2one('project.project', 'Project')
    cost_sheet = fields.Many2one('job.cost.sheet', string="Cost Sheet")
    down_payment_method = fields.Selection([('fixed','Fixed'), ('percentage','Percentage')], string = "Down Payment Method", default='fixed')
    down_payment = fields.Float('Down Payment')
    retention_1 = fields.Float('Retention 1 (%)')
    retention_1_date = fields.Date('Retention 1 Date')
    retention_2 = fields.Float('Retention 2 (%)')
    retention_2_date = fields.Date('Retention 2 Date')
    sub_contracting = fields.Selection([('main_contract','Main Contract'), ('addendum','Addendum')], string = "Sub Contracting", default='main_contract')
    is_subcontracting = fields.Boolean('Is Subcontracting', default=False)
    is_material_orders = fields.Boolean('Is Material Orders', default=False)

    main_po_reference = fields.Many2one('purchase.order', 'Main PO Reference')
    addendum_payment_method = fields.Selection([('join_payment','Join Payment'), ('split_payment','Split Payment')], string = "Addendum Payment Method", default='join_payment')

    def button_confirm2(self):
        for rec in self.variable_line_ids:
            if rec.total > rec.budget_amount_total:
                raise ValidationError(("The total amount is over the budget amount left"))
            else:
                self.write({'state': 'purchase'})
                for bud in rec.cs_subcon_id:
                    reserved = 0.00
                    reserved = (bud.reserved_amt + rec.total)
                    for sub in self.cost_sheet:
                        sub.material_subcon_ids = [(1, rec.cs_subcon_id.id, {
                                'reserved_amt': reserved,
                            })]
    
    def action_create_bill_2(self):
        pass

    @api.onchange('is_subcontracting')
    def _onchange_is_subcontracting(self):
        context = dict(self.env.context) or {}
        if context.get('services_good'):
            self.is_subcontracting = True

    @api.onchange('is_material_orders')
    def _onchange_is_material_orders(self):
        context = dict(self.env.context) or {}
        if context.get('goods_order'):
            self.is_material_orders = True
    
    @api.onchange('cost_sheet')
    def _onchange_cost_sheet(self):
        self.analytic_account_group_ids = False
        self.variable_line_ids = [(5,0,0)]
        if self.cost_sheet:
            cost_sheet = self.cost_sheet
            self.analytic_account_group_ids = cost_sheet.account_tag_ids.ids
        for rec in self.cost_sheet:
            for section in rec.material_subcon_ids:
                self.variable_line_ids = [(0, 0, {
                    'cs_subcon_id': section.id,
                    'project_scope': section.project_scope,
                    'section': section.section_name,
                    'variable': section.variable,
                    'uom': section.uom_id,
                    'budget_amount': section.price_unit,
                    'budget_amount_total': section.budgeted_amt_left
                })]

    # def button_confirm(self):
    #     res = super(RequestForQuotations, self).button_confirm()
    #     for rec in self.variable_line_ids:
    #         reserved = 0.00
    #         for bud in rec.cs_subcon_id:
    #             if rec.sub_total > bud.price_unit:
    #                 raise ValidationError(_("The unit price is over the budget unit price"))
    #             elif rec.total > bud.budgeted_amt_left:
    #                 raise ValidationError(_("The total amount is over the budget amount left"))
    #             elif rec.quantity > rec.budget_quantity:
    #                 raise ValidationError(_("The quantity is over the remaining budget"))
    #             else:
    #                 reserved = (bud.reserved_amt + rec.total)
    #                 for sub in self.cost_sheet:
    #                     sub.material_subcon_ids = [(1, rec.cs_subcon_id.id, {
    #                              'reserved_amt': reserved,
    #                         })]
    #     return res


    variable_line_ids = fields.One2many('rfq.variable.line', 'variable_id', string='Variable Line')


    # Service Line Start
    service_line_ids = fields.One2many('rfq.service.line', 'service_id', string='Service Line')
    sl_amount_untaxed = fields.Monetary(string='Untaxed Amount', store=True, readonly=True, compute='_sl_amount_all', tracking=True)
    sl_discount_amt = fields.Monetary(string='- Discount', store=True, readonly=True, compute='_sl_amount_all')
    sl_discount_amt_line = fields.Float(compute='_sl_amount_all', string='- Line Discount', digits_compute=dp.get_precision('Line Discount'), store=True, readonly=True)
    sl_amount_tax = fields.Monetary(string='Taxes', store=True, readonly=True, compute='_sl_amount_all')
    sl_amount_total = fields.Monetary(string='Total Service', store=True, readonly=True, compute='_sl_amount_all')

    discount_method_sl = fields.Selection([('fixed', 'Fixed'), ('percentage', 'Percentage')], 'Discount Method', default='fixed')
    discount_amount_sl = fields.Float('Discount Amount',default=0.0)

    @api.depends('service_line_ids.total')
    def _sl_amount_all(self):
        for order in self:
            sl_amount_untaxed = sl_amount_tax = 0.0
            for line in order.service_line_ids:
                line._compute_sl_amount()
                sl_amount_untaxed += line.subtotal
                sl_amount_tax += line.tax
            order.update({
                'sl_amount_untaxed': order.currency_id.round(sl_amount_untaxed),
                'sl_amount_tax': order.currency_id.round(sl_amount_tax),
                'sl_amount_total': sl_amount_untaxed + sl_amount_tax,
            })

    @api.onchange('discount_type', 'discount_method_ml', 'discount_amount_ml')
    def set_disc_ml(self):
        for res in self:
            if res.discount_type == 'global':
                for line in res.material_line_ids:
                    line.update({
                        'discount_method': res.discount_method_ml,
                        'discount_amount': res.discount_amount_ml
                    })

    # Service Line End


    # Material Line Start  
    material_line_ids = fields.One2many('rfq.material.line', 'material_id', string='Material Line')
    ml_amount_untaxed = fields.Monetary(string='Untaxed Amount', store=True, readonly=True, compute='_ml_amount_all', tracking=True)
    ml_discount_amt = fields.Monetary(string='- Discount', store=True, readonly=True, compute='_ml_amount_all')
    ml_discount_amt_line = fields.Float(compute='_ml_amount_all', string='- Line Discount', digits_compute=dp.get_precision('Line Discount'), store=True, readonly=True)
    ml_amount_tax = fields.Monetary(string='Taxes', store=True, readonly=True, compute='_ml_amount_all')
    ml_amount_total = fields.Monetary(string='Total Material', store=True, readonly=True, compute='_ml_amount_all')

    discount_method_ml = fields.Selection([('fixed', 'Fixed'), ('percentage', 'Percentage')], 'Discount Method', default='fixed')
    discount_amount_ml = fields.Float('Discount Amount',default=0.0)

    @api.depends('material_line_ids.total')
    def _ml_amount_all(self):
        for order in self:
            ml_amount_untaxed = ml_amount_tax = 0.0
            for line in order.material_line_ids:
                line._compute_ml_amount()
                ml_amount_untaxed += line.subtotal
                ml_amount_tax += line.tax
            order.update({
                'ml_amount_untaxed': order.currency_id.round(ml_amount_untaxed),
                'ml_amount_tax': order.currency_id.round(ml_amount_tax),
                'ml_amount_total': ml_amount_untaxed + ml_amount_tax,
            })

    @api.onchange('discount_type', 'discount_method_ml', 'discount_amount_ml')
    def set_disc_ml(self):
        for res in self:
            if res.discount_type == 'global':
                for line in res.material_line_ids:
                    line.update({
                        'discount_method': res.discount_method_ml,
                        'discount_amount': res.discount_amount_ml
                    })
    
    # Material Line End

    # Equipment Line Start  
    equipment_line_ids = fields.One2many('rfq.equipment.line', 'equipment_id', string='Equipment Line')
    el_amount_untaxed = fields.Monetary(string='Untaxed Amount', store=True, readonly=True, compute='_el_amount_all', tracking=True)
    el_discount_amt = fields.Monetary(string='- Discount', store=True, readonly=True, compute='_el_amount_all')
    el_discount_amt_line = fields.Float(compute='_el_amount_all', string='- Line Discount', digits_compute=dp.get_precision('Line Discount'), store=True, readonly=True)
    el_amount_tax = fields.Monetary(string='Taxes', store=True, readonly=True, compute='_el_amount_all')
    el_amount_total = fields.Monetary(string='Total Equipment', store=True, readonly=True, compute='_el_amount_all')

    discount_method_el = fields.Selection([('fixed', 'Fixed'), ('percentage', 'Percentage')], 'Discount Method', default='fixed')
    discount_amount_el = fields.Float('Discount Amount',default=0.0)

    @api.depends('equipment_line_ids.total')
    def _el_amount_all(self):
        for order in self:
            el_amount_untaxed = el_amount_tax = 0.0
            for line in order.equipment_line_ids:
                line._compute_el_amount()
                el_amount_untaxed += line.subtotal
                el_amount_tax += line.tax
            order.update({
                'el_amount_untaxed': order.currency_id.round(el_amount_untaxed),
                'el_amount_tax': order.currency_id.round(el_amount_tax),
                'el_amount_total': el_amount_untaxed + el_amount_tax,
            })

    @api.onchange('discount_type', 'discount_method_el', 'discount_amount_el')
    def set_disc_el(self):
        for res in self:
            if res.discount_type == 'global':
                for line in res.equipment_line_ids:
                    line.update({
                        'discount_method': res.discount_method_el,
                        'discount_amount': res.discount_amount_el
                    })
    
    # Equipment Line End

    # Labour Line Start  
    labour_line_ids = fields.One2many('rfq.labour.line', 'labour_id', string='Labour Line')
    ll_amount_untaxed = fields.Monetary(string='Untaxed Amount', store=True, readonly=True, compute='_ll_amount_all', tracking=True)
    ll_discount_amt = fields.Monetary(string='- Discount', store=True, readonly=True, compute='_ll_amount_all')
    ll_discount_amt_line = fields.Float(compute='_ll_amount_all', string='- Line Discount', digits_compute=dp.get_precision('Line Discount'), store=True, readonly=True)
    ll_amount_tax = fields.Monetary(string='Taxes', store=True, readonly=True, compute='_ll_amount_all')
    ll_amount_total = fields.Monetary(string='Total Labour', store=True, readonly=True, compute='_ll_amount_all')

    discount_method_ll = fields.Selection([('fixed', 'Fixed'), ('percentage', 'Percentage')], 'Discount Method', default='fixed')
    discount_amount_ll = fields.Float('Discount Amount',default=0.0)

    @api.depends('labour_line_ids.total')
    def _ll_amount_all(self):
        for order in self:
            ll_amount_untaxed = ll_amount_tax = 0.0
            for line in order.labour_line_ids:
                line._compute_ll_amount()
                ll_amount_untaxed += line.subtotal
                ll_amount_tax += line.tax
            order.update({
                'll_amount_untaxed': order.currency_id.round(ll_amount_untaxed),
                'll_amount_tax': order.currency_id.round(ll_amount_tax),
                'll_amount_total': ll_amount_untaxed + ll_amount_tax,
            })

    @api.onchange('discount_type', 'discount_method_ll', 'discount_amount_ll')
    def set_disc_ll(self):
        for res in self:
            if res.discount_type == 'global':
                for line in res.labour_line_ids:
                    line.update({
                        'discount_method': res.discount_method_ll,
                        'discount_amount': res.discount_amount_ll
                    })
    
    # Labour Line End

    # Overhead Line Start  
    overhead_line_ids = fields.One2many('rfq.overhead.line', 'overhead_id', string='Overhead Line')
    ol_amount_untaxed = fields.Monetary(string='Untaxed Amount', store=True, readonly=True, compute='_ol_amount_all', tracking=True)
    ol_discount_amt = fields.Monetary(string='- Discount', store=True, readonly=True, compute='_ol_amount_all')
    ol_discount_amt_line = fields.Float(compute='_ol_amount_all', string='- Line Discount', digits_compute=dp.get_precision('Line Discount'), store=True, readonly=True)
    ol_amount_tax = fields.Monetary(string='Taxes', store=True, readonly=True, compute='_ol_amount_all')
    ol_amount_total = fields.Monetary(string='Total Overhead', store=True, readonly=True, compute='_ol_amount_all')

    discount_method_ol = fields.Selection([('fixed', 'Fixed'), ('percentage', 'Percentage')], 'Discount Method', default='fixed')
    discount_amount_ol = fields.Float('Discount Amount',default=0.0)

    @api.depends('overhead_line_ids.total')
    def _ol_amount_all(self):
        for order in self:
            ol_amount_untaxed = ol_amount_tax = 0.0
            for line in order.overhead_line_ids:
                line._compute_ol_amount()
                ol_amount_untaxed += line.subtotal
                ol_amount_tax += line.tax
            order.update({
                'ol_amount_untaxed': order.currency_id.round(ol_amount_untaxed),
                'ol_amount_tax': order.currency_id.round(ol_amount_tax),
                'ol_amount_total': ol_amount_untaxed + ol_amount_tax,
            })

    @api.onchange('discount_type', 'discount_method_ol', 'discount_amount_ol')
    def set_disc_ol(self):
        for res in self:
            if res.discount_type == 'global':
                for line in res.overhead_line_ids:
                    line.update({
                        'discount_method': res.discount_method_ol,
                        'discount_amount': res.discount_amount_ol
                    })
    
    # Overhead Line End


    # Total All Start
    material_total = fields.Monetary(string='Material Total', store=True, readonly=True, compute='_get_material_total')
    service_total = fields.Monetary(string='Service Total', store=True, readonly=True, compute='_get_service_total')
    equipment_total = fields.Monetary(string='Equipment Total', store=True, readonly=True, compute='_get_equipment_total')
    labour_total = fields.Monetary(string='Labour Total', store=True, readonly=True, compute='_get_labour_total')
    overhead_total = fields.Monetary(string='Overhead Total', store=True, readonly=True, compute='_get_overhead_total')
    total_all = fields.Monetary(string='Total', store=True, readonly=True, compute='_get_total_all')

    @api.depends('ml_amount_total')
    def _get_material_total(self):
        for order in self:
            order.update({
                'material_total': order.ml_amount_total,
                'service_total': order.sl_amount_total,
                'equipment_total': order.el_amount_total,
                'labour_total': order.ll_amount_total,
                'overhead_total': order.ol_amount_total,
            })

    @api.depends('sl_amount_total')
    def _get_service_total(self):
        for order in self:
            order.update({
                'material_total': order.ml_amount_total,
                'service_total': order.sl_amount_total,
                'equipment_total': order.el_amount_total,
                'labour_total': order.ll_amount_total,
                'overhead_total': order.ol_amount_total,
            })

    @api.depends('el_amount_total')
    def _get_equipment_total(self):
        for order in self:
            order.update({
                'material_total': order.ml_amount_total,
                'service_total': order.sl_amount_total,
                'equipment_total': order.el_amount_total,
                'labour_total': order.ll_amount_total,
                'overhead_total': order.ol_amount_total,
            })

    @api.depends('ll_amount_total')
    def _get_labour_total(self):
        for order in self:
            order.update({
                'material_total': order.ml_amount_total,
                'service_total': order.sl_amount_total,
                'equipment_total': order.el_amount_total,
                'labour_total': order.ll_amount_total,
                'overhead_total': order.ol_amount_total,
            })

    @api.depends('ol_amount_total')
    def _get_overhead_total(self):
        for order in self:
            order.update({
                'material_total': order.ml_amount_total,
                'service_total': order.sl_amount_total,
                'equipment_total': order.el_amount_total,
                'labour_total': order.ll_amount_total,
                'overhead_total': order.ol_amount_total,
            })

    @api.depends('material_total','service_total','equipment_total','labour_total','overhead_total')
    def _get_total_all(self):
        for order in self:
            order.update({
                'total_all': order.material_total + order.service_total + order.equipment_total + order.labour_total + order.overhead_total,
            })

    all_total = fields.Monetary('Total', compute='_compute_all_total')

    @api.depends('all_total')
    def _compute_all_total(self):        
        for record in self:
            record.all_total = record.ml_amount_total + record.sl_amount_total +  record.el_amount_total + record.ll_amount_total + record.ol_amount_total + record.total_all
    
    # Total All End

class RFQVariableLine(models.Model):
    _name = 'rfq.variable.line'
    _description = "Variable Line Request for Quotations"
    _order = "sequence"


    sequence = fields.Integer('Sequence', default=1)
    sr_no = fields.Integer('No.', compute="_sequence_ref")
    cs_subcon_id = fields.Many2one('material.subcon', 'Subcon ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope', required=True, )
    section = fields.Many2one('section.estimate', 'Section', required=True)
    variable_id = fields.Many2one('purchase.order', 'Variable ID')
    variable = fields.Many2one('variable.template', 'Variable', required=True)
    quantity = fields.Float('Quantity', default="1")
    budget_quantity = fields.Float('Budget Quantity')
    uom = fields.Many2one('uom.uom', string='UoM', readonly=True)
    sub_total = fields.Float('Unit Price')
    budget_amount = fields.Float('Budget Unit Price')
    total = fields.Float('Total', readonly=True)
    budget_amount_total = fields.Float('Budget Amount')

    @api.onchange('variable')
    def onchange_variable(self):
        res = {}
        if not self.variable:
            return res
        self.uom = self.variable.variable_uom.id
        self.quantity = 1.0
        self.sub_total = self.variable.total_variable

    @api.onchange('quantity', 'sub_total')
    def onchange_total(self):
        price = 0.0
        for line in self:
            price =  (line.quantity * line.sub_total) 
            line.total = price

    @api.depends('variable_id.variable_line_ids', 'variable_id.variable_line_ids.sequence')
    def _sequence_ref(self):
        for line in self:
            no = 0
            line.sr_no = no
            for l in line.variable_id.variable_line_ids:
                no += 1
                l.sr_no = no

    @api.model
    def create(self, vals):
        vals['sr_no'] = self.env['ir.sequence'].next_by_code('rfq_variable_line') or ('New')
        res = super(RFQVariableLine, self).create(vals)
        return res


class RFQMaterialLine(models.Model):
    _name = 'rfq.material.line'
    _description = "Material Line Request for Quotations"
    _order = "sequence"


    @api.model
    def _default_domain_ml(self):
        context = dict(self.env.context) or {}
        if context.get('goods_order'):
            return [('type', 'in', ('consu','product'))]
        elif context.get('services_good'):
            return [('type', '=', 'service')]

    sequence = fields.Integer('Sequence', default=1)
    material_id = fields.Many2one('purchase.order', 'Material ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    product = fields.Many2one('product.product', 'Product', required=True, domain=_default_domain_ml)
    description = fields.Text(string='Description', required=True)
    purchase_tender = fields.Many2one('purchase.agreement', 'Purchase Tender')
    receipt_date = fields.Datetime('Receipt Date')
    destination_warehouse = fields.Many2one('stock.warehouse', 'Destination Warehouse', required=True)
    analytic_group = fields.Many2many('account.analytic.tag', store=True, string='Analytic Group')
    discount_method = fields.Selection([('fixed','Fixed'), ('percentage','Percentage')], string = "Discount Method")
    discount_amount = fields.Float('Discount Amount')
    note_comment = fields.Char('Note/Comment')
    quantity = fields.Float('Quantity', required=True)
    budget_quantity = fields.Float('Budget Quantity')
    uom = fields.Many2one('uom.uom', 'UoM', required=True)
    secondary_quantity = fields.Float('Secondary Qty')
    secondary_uom = fields.Many2one('uom.uom', 'Secondary UOM')
    unit_price = fields.Float('Unit Price', required=True)
    budget_unit_price = fields.Float('Budget Unit Price')
    taxes = fields.Many2many('account.tax', string='Taxes', domain=['|', ('active', '=', False), ('active', '=', True)])
    lpp = fields.Float('Last Purchased Price')
    lpp_of_vendor = fields.Float('Last Purchased Price of Vendor')
    remining_budget_amount = fields.Float('Budget Amount Left')

    subtotal = fields.Monetary(compute='_compute_ml_amount', string='Subtotal', store=True)
    total = fields.Monetary(compute='_compute_ml_amount', string='Total', store=True)
    tax = fields.Float(compute='_compute_ml_amount', string='Tax', store=True)

    partner_id = fields.Many2one('res.partner', related='material_id.partner_id', string='Partner', readonly=True, store=True)
    currency_id = fields.Many2one(related='material_id.currency_id', store=True, string='Currency', readonly=True)
    date_order = fields.Datetime(related='material_id.date_order', string='Order Date', readonly=True)

    discount_amt = fields.Float('Discount Final Amount')
    discount_type = fields.Selection(related='material_id.discount_type', string="Discount Applies to")

    company_id = fields.Many2one('res.company', related='material_id.company_id', string='Company', store=True, readonly=True)

    @api.onchange('product')
    def onchange_product_ml(self):
        res = {}
        if not self.product:
            return res
        self.receipt_date = datetime.today()
        product_lang = self.product.with_context(lang=get_lang(self.env).code)
        self.description = self._get_rfq_material_description(product_lang)
        self.quantity = 1.0
        self.uom = self.product.uom_id.id
        self.unit_price = self.product.standard_price
        self.taxes = self.product.taxes_id
        self.lpp = self.product.last_purchase_price

    def _get_rfq_material_description(self, product_lang):
        self.ensure_one()
        name = product_lang.display_name
        if product_lang.description_purchase:
            name += '\n' + product_lang.description_purchase
        return name

    @api.depends('quantity', 'unit_price', 'taxes')
    def _compute_ml_amount(self):
        for line in self:
            vals = line.ml_prepare_compute_all_values()
            taxes = line.taxes.compute_all(
                vals['unit_price'],
                vals['currency_id'],
                vals['quantity'],
                vals['product'],
                vals['partner'])
            line.update({
                'tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'total': taxes['total_included'],
                'subtotal': taxes['total_excluded'],
            })

    def ml_prepare_compute_all_values(self):
        # Hook method to returns the different argument values for the
        # compute_all method, due to the fact that discounts mechanism
        # is not implemented yet on the purchase orders.
        # This method should disappear as soon as this feature is
        # also introduced like in the sales module.
        self.ensure_one()
        return {
            'unit_price': self.unit_price,
            'currency_id': self.material_id.currency_id,
            'quantity': self.quantity,
            'product': self.product,
            'partner': self.material_id.partner_id,
        }


class RFQServiceLine(models.Model):
    _name = 'rfq.service.line'
    _description = "Service Line Request for Quotations"
    _order = "sequence"


    @api.model
    def _default_domain_sl(self):
        context = dict(self.env.context) or {}
        if context.get('goods_order'):
            return [('type', 'in', ('consu','product'))]
        elif context.get('services_good'):
            return [('type', '=', 'service')]

    sequence = fields.Integer('Sequence', default=1)
    service_id = fields.Many2one('purchase.order', 'Service ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    product = fields.Many2one('product.product', 'Product', required=True, domain=_default_domain_sl)
    description = fields.Text(string='Description', required=True)
    purchase_tender = fields.Many2one('purchase.agreement', 'Purchase Tender')
    receipt_date = fields.Datetime('Receipt Date')
    destination_warehouse = fields.Many2one('stock.warehouse', 'Destination Warehouse', required=True)
    analytic_group = fields.Many2many('account.analytic.tag', store=True, string='Analytic Group')
    discount_method = fields.Selection([('fixed','Fixed'), ('percentage','Percentage')], string = "Discount Method")
    discount_amount = fields.Float('Discount Amount')
    note_comment = fields.Char('Note/Comment')
    quantity = fields.Float('Quantity', required=True)
    budget_quantity = fields.Float('Budget Quantity')
    uom = fields.Many2one('uom.uom', 'UoM', required=True)
    secondary_quantity = fields.Float('Secondary Qty')
    secondary_uom = fields.Many2one('uom.uom', 'Secondary UOM')
    unit_price = fields.Float('Unit Price', required=True)
    budget_unit_price = fields.Float('Budget Unit Price')
    taxes = fields.Many2many('account.tax', string='Taxes', domain=['|', ('active', '=', False), ('active', '=', True)])
    lpp = fields.Float('Last Purchased Price')
    lpp_of_vendor = fields.Float('Last Purchased Price of Vendor')
    remining_budget_amount = fields.Float('Budget Amount Left')

    subtotal = fields.Monetary(compute='_compute_sl_amount', string='Subtotal', store=True)
    total = fields.Monetary(compute='_compute_sl_amount', string='Total', store=True)
    tax = fields.Float(compute='_compute_sl_amount', string='Tax', store=True)

    partner_id = fields.Many2one('res.partner', related='service_id.partner_id', string='Partner', readonly=True, store=True)
    currency_id = fields.Many2one(related='service_id.currency_id', store=True, string='Currency', readonly=True)
    date_order = fields.Datetime(related='service_id.date_order', string='Order Date', readonly=True)

    discount_amt = fields.Float('Discount Final Amount')
    discount_type = fields.Selection(related='service_id.discount_type', string="Discount Applies to")

    company_id = fields.Many2one('res.company', related='service_id.company_id', string='Company', store=True, readonly=True)

    @api.onchange('product')
    def onchange_product_sl(self):
        res = {}
        if not self.product:
            return res
        self.receipt_date = datetime.today()
        product_lang = self.product.with_context(lang=get_lang(self.env).code)
        self.description = self._get_rfq_service_description(product_lang)
        self.quantity = 1.0
        self.uom = self.product.uom_id.id
        self.unit_price = self.product.standard_price
        self.taxes = self.product.taxes_id
        self.lpp = self.product.last_purchase_price

    def _get_rfq_service_description(self, product_lang):
        self.ensure_one()
        name = product_lang.display_name
        if product_lang.description_purchase:
            name += '\n' + product_lang.description_purchase
        return name

    @api.depends('quantity', 'unit_price', 'taxes')
    def _compute_sl_amount(self):
        for line in self:
            vals = line.sl_prepare_compute_all_values()
            taxes = line.taxes.compute_all(
                vals['unit_price'],
                vals['currency_id'],
                vals['quantity'],
                vals['product'],
                vals['partner'])
            line.update({
                'tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'total': taxes['total_included'],
                'subtotal': taxes['total_excluded'],
            })

    def sl_prepare_compute_all_values(self):
        # Hook method to returns the different argument values for the
        # compute_all method, due to the fact that discounts mechanism
        # is not implemented yet on the purchase orders.
        # This method should disappear as soon as this feature is
        # also introduced like in the sales module.
        self.ensure_one()
        return {
            'unit_price': self.unit_price,
            'currency_id': self.service_id.currency_id,
            'quantity': self.quantity,
            'product': self.product,
            'partner': self.service_id.partner_id,
        }


class RFQEquipmentLine(models.Model):
    _name = 'rfq.equipment.line'
    _description = "Equipment Line Request for Quotations"
    _order = "sequence"


    @api.model
    def _default_domain_el(self):
        context = dict(self.env.context) or {}
        if context.get('goods_order'):
            return [('type', 'in', ('consu','product'))]
        elif context.get('equipments_good'):
            return [('type', '=', 'equipment')]

    sequence = fields.Integer('Sequence', default=1)
    equipment_id = fields.Many2one('purchase.order', 'Equipment ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    product = fields.Many2one('product.product', 'Product', required=True, domain=_default_domain_el)
    description = fields.Text(string='Description', required=True)
    purchase_tender = fields.Many2one('purchase.agreement', 'Purchase Tender')
    receipt_date = fields.Datetime('Receipt Date')
    destination_warehouse = fields.Many2one('stock.warehouse', 'Destination Warehouse', required=True)
    analytic_group = fields.Many2many('account.analytic.tag', store=True, string='Analytic Group')
    discount_method = fields.Selection([('fixed','Fixed'), ('percentage','Percentage')], string = "Discount Method")
    discount_amount = fields.Float('Discount Amount')
    note_comment = fields.Char('Note/Comment')
    quantity = fields.Float('Quantity', required=True)
    budget_quantity = fields.Float('Budget Quantity')
    uom = fields.Many2one('uom.uom', 'UoM', required=True)
    secondary_quantity = fields.Float('Secondary Qty')
    secondary_uom = fields.Many2one('uom.uom', 'Secondary UOM')
    unit_price = fields.Float('Unit Price', required=True)
    budget_unit_price = fields.Float('Budget Unit Price')
    taxes = fields.Many2many('account.tax', string='Taxes', domain=['|', ('active', '=', False), ('active', '=', True)])
    lpp = fields.Float('Last Purchased Price')
    lpp_of_vendor = fields.Float('Last Purchased Price of Vendor')
    remining_budget_amount = fields.Float('Budget Amount Left')

    subtotal = fields.Monetary(compute='_compute_el_amount', string='Subtotal', store=True)
    total = fields.Monetary(compute='_compute_el_amount', string='Total', store=True)
    tax = fields.Float(compute='_compute_el_amount', string='Tax', store=True)

    partner_id = fields.Many2one('res.partner', related='equipment_id.partner_id', string='Partner', readonly=True, store=True)
    currency_id = fields.Many2one(related='equipment_id.currency_id', store=True, string='Currency', readonly=True)
    date_order = fields.Datetime(related='equipment_id.date_order', string='Order Date', readonly=True)

    discount_amt = fields.Float('Discount Final Amount')
    discount_type = fields.Selection(related='equipment_id.discount_type', string="Discount Applies to")

    company_id = fields.Many2one('res.company', related='equipment_id.company_id', string='Company', store=True, readonly=True)

    @api.onchange('product')
    def onchange_product_el(self):
        res = {}
        if not self.product:
            return res
        self.receipt_date = datetime.today()
        product_lang = self.product.with_context(lang=get_lang(self.env).code)
        self.description = self._get_rfq_equipment_description(product_lang)
        self.quantity = 1.0
        self.uom = self.product.uom_id.id
        self.unit_price = self.product.standard_price
        self.taxes = self.product.taxes_id
        self.lpp = self.product.last_purchase_price

    def _get_rfq_equipment_description(self, product_lang):
        self.ensure_one()
        name = product_lang.display_name
        if product_lang.description_purchase:
            name += '\n' + product_lang.description_purchase
        return name

    @api.depends('quantity', 'unit_price', 'taxes')
    def _compute_el_amount(self):
        for line in self:
            vals = line.el_prepare_compute_all_values()
            taxes = line.taxes.compute_all(
                vals['unit_price'],
                vals['currency_id'],
                vals['quantity'],
                vals['product'],
                vals['partner'])
            line.update({
                'tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'total': taxes['total_included'],
                'subtotal': taxes['total_excluded'],
            })

    def el_prepare_compute_all_values(self):
        # Hook method to returns the different argument values for the
        # compute_all method, due to the fact that discounts mechanism
        # is not implemented yet on the purchase orders.
        # This method should disappear as soon as this feature is
        # also introduced like in the sales module.
        self.ensure_one()
        return {
            'unit_price': self.unit_price,
            'currency_id': self.equipment_id.currency_id,
            'quantity': self.quantity,
            'product': self.product,
            'partner': self.equipment_id.partner_id,
        }


class RFQLabourLine(models.Model):
    _name = 'rfq.labour.line'
    _description = "Labour Line Request for Quotations"
    _order = "sequence"


    @api.model
    def _default_domain_ll(self):
        context = dict(self.env.context) or {}
        if context.get('goods_order'):
            return [('type', 'in', ('consu','product'))]
        elif context.get('labours_good'):
            return [('type', '=', 'labour')]

    sequence = fields.Integer('Sequence', default=1)
    labour_id = fields.Many2one('purchase.order', 'Labour ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    product = fields.Many2one('product.product', 'Product', required=True, domain=_default_domain_ll)
    description = fields.Text(string='Description', required=True)
    purchase_tender = fields.Many2one('purchase.agreement', 'Purchase Tender')
    receipt_date = fields.Datetime('Receipt Date')
    destination_warehouse = fields.Many2one('stock.warehouse', 'Destination Warehouse', required=True)
    analytic_group = fields.Many2many('account.analytic.tag', store=True, string='Analytic Group')
    discount_method = fields.Selection([('fixed','Fixed'), ('percentage','Percentage')], string = "Discount Method")
    discount_amount = fields.Float('Discount Amount')
    note_comment = fields.Char('Note/Comment')
    quantity = fields.Float('Quantity', required=True)
    budget_quantity = fields.Float('Budget Quantity')
    uom = fields.Many2one('uom.uom', 'UoM', required=True)
    secondary_quantity = fields.Float('Secondary Qty')
    secondary_uom = fields.Many2one('uom.uom', 'Secondary UOM')
    unit_price = fields.Float('Unit Price', required=True)
    budget_unit_price = fields.Float('Budget Unit Price')
    taxes = fields.Many2many('account.tax', string='Taxes', domain=['|', ('active', '=', False), ('active', '=', True)])
    lpp = fields.Float('Last Purchased Price')
    lpp_of_vendor = fields.Float('Last Purchased Price of Vendor')
    remining_budget_amount = fields.Float('Budget Amount Left')

    subtotal = fields.Monetary(compute='_compute_ll_amount', string='Subtotal', store=True)
    total = fields.Monetary(compute='_compute_ll_amount', string='Total', store=True)
    tax = fields.Float(compute='_compute_ll_amount', string='Tax', store=True)

    partner_id = fields.Many2one('res.partner', related='labour_id.partner_id', string='Partner', readonly=True, store=True)
    currency_id = fields.Many2one(related='labour_id.currency_id', store=True, string='Currency', readonly=True)
    date_order = fields.Datetime(related='labour_id.date_order', string='Order Date', readonly=True)

    discount_amt = fields.Float('Discount Final Amount')
    discount_type = fields.Selection(related='labour_id.discount_type', string="Discount Applies to")

    company_id = fields.Many2one('res.company', related='labour_id.company_id', string='Company', store=True, readonly=True)

    @api.onchange('product')
    def onchange_product_ll(self):
        res = {}
        if not self.product:
            return res
        self.receipt_date = datetime.today()
        product_lang = self.product.with_context(lang=get_lang(self.env).code)
        self.description = self._get_rfq_labour_description(product_lang)
        self.quantity = 1.0
        self.uom = self.product.uom_id.id
        self.unit_price = self.product.standard_price
        self.taxes = self.product.taxes_id
        self.lpp = self.product.last_purchase_price

    def _get_rfq_labour_description(self, product_lang):
        self.ensure_one()
        name = product_lang.display_name
        if product_lang.description_purchase:
            name += '\n' + product_lang.description_purchase
        return name

    @api.depends('quantity', 'unit_price', 'taxes')
    def _compute_ll_amount(self):
        for line in self:
            vals = line.ll_prepare_compute_all_values()
            taxes = line.taxes.compute_all(
                vals['unit_price'],
                vals['currency_id'],
                vals['quantity'],
                vals['product'],
                vals['partner'])
            line.update({
                'tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'total': taxes['total_included'],
                'subtotal': taxes['total_excluded'],
            })

    def ll_prepare_compute_all_values(self):
        # Hook method to returns the different argument values for the
        # compute_all method, due to the fact that discounts mechanism
        # is not implemented yet on the purchase orders.
        # This method should disappear as soon as this feature is
        # also introduced like in the sales module.
        self.ensure_one()
        return {
            'unit_price': self.unit_price,
            'currency_id': self.labour_id.currency_id,
            'quantity': self.quantity,
            'product': self.product,
            'partner': self.labour_id.partner_id,
        }


class RFQOverheadLine(models.Model):
    _name = 'rfq.overhead.line'
    _description = "Overhead Line Request for Quotations"
    _order = "sequence"


    @api.model
    def _default_domain_ol(self):
        context = dict(self.env.context) or {}
        if context.get('goods_order'):
            return [('type', 'in', ('consu','product'))]
        elif context.get('services_good'):
            return [('type', '=', 'service')]

    sequence = fields.Integer('Sequence', default=1)
    overhead_id = fields.Many2one('purchase.order', 'Overhead ID')
    project_scope = fields.Many2one('project.scope.line', 'Project Scope')
    section = fields.Many2one('section.estimate', 'Section')
    variable = fields.Many2one('variable.template', 'Variable')
    product = fields.Many2one('product.product', 'Product', required=True, domain=_default_domain_ol)
    description = fields.Text(string='Description', required=True)
    purchase_tender = fields.Many2one('purchase.agreement', 'Purchase Tender')
    receipt_date = fields.Datetime('Receipt Date')
    destination_warehouse = fields.Many2one('stock.warehouse', 'Destination Warehouse', required=True)
    analytic_group = fields.Many2many('account.analytic.tag', store=True, string='Analytic Group')
    discount_method = fields.Selection([('fixed','Fixed'), ('percentage','Percentage')], string = "Discount Method")
    discount_amount = fields.Float('Discount Amount')
    note_comment = fields.Char('Note/Comment')
    quantity = fields.Float('Quantity', required=True)
    budget_quantity = fields.Float('Budget Quantity')
    uom = fields.Many2one('uom.uom', 'UoM', required=True)
    secondary_quantity = fields.Float('Secondary Qty')
    secondary_uom = fields.Many2one('uom.uom', 'Secondary UOM')
    unit_price = fields.Float('Unit Price', required=True)
    budget_unit_price = fields.Float('Budget Unit Price')
    taxes = fields.Many2many('account.tax', string='Taxes', domain=['|', ('active', '=', False), ('active', '=', True)])
    lpp = fields.Float('Last Purchased Price')
    lpp_of_vendor = fields.Float('Last Purchased Price of Vendor')
    remining_budget_amount = fields.Float('Budget Amount Left')

    subtotal = fields.Monetary(compute='_compute_ol_amount', string='Subtotal', store=True)
    total = fields.Monetary(compute='_compute_ol_amount', string='Total', store=True)
    tax = fields.Float(compute='_compute_ol_amount', string='Tax', store=True)

    partner_id = fields.Many2one('res.partner', related='overhead_id.partner_id', string='Partner', readonly=True, store=True)
    currency_id = fields.Many2one(related='overhead_id.currency_id', store=True, string='Currency', readonly=True)
    date_order = fields.Datetime(related='overhead_id.date_order', string='Order Date', readonly=True)

    discount_amt = fields.Float('Discount Final Amount')
    discount_type = fields.Selection(related='overhead_id.discount_type', string="Discount Applies to")

    company_id = fields.Many2one('res.company', related='overhead_id.company_id', string='Company', store=True, readonly=True)

    @api.onchange('product')
    def onchange_product_ol(self):
        res = {}
        if not self.product:
            return res
        self.receipt_date = datetime.today()
        product_lang = self.product.with_context(lang=get_lang(self.env).code)
        self.description = self._get_rfq_overhead_description(product_lang)
        self.quantity = 1.0
        self.uom = self.product.uom_id.id
        self.unit_price = self.product.standard_price
        self.taxes = self.product.taxes_id
        self.lpp = self.product.last_purchase_price

    def _get_rfq_overhead_description(self, product_lang):
        self.ensure_one()
        name = product_lang.display_name
        if product_lang.description_purchase:
            name += '\n' + product_lang.description_purchase
        return name

    @api.depends('quantity', 'unit_price', 'taxes')
    def _compute_ol_amount(self):
        for line in self:
            vals = line.ol_prepare_compute_all_values()
            taxes = line.taxes.compute_all(
                vals['unit_price'],
                vals['currency_id'],
                vals['quantity'],
                vals['product'],
                vals['partner'])
            line.update({
                'tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'total': taxes['total_included'],
                'subtotal': taxes['total_excluded'],
            })

    def ol_prepare_compute_all_values(self):
        # Hook method to returns the different argument values for the
        # compute_all method, due to the fact that discounts mechanism
        # is not implemented yet on the purchase orders.
        # This method should disappear as soon as this feature is
        # also introduced like in the sales module.
        self.ensure_one()
        return {
            'unit_price': self.unit_price,
            'currency_id': self.overhead_id.currency_id,
            'quantity': self.quantity,
            'product': self.product,
            'partner': self.overhead_id.partner_id,
        }