# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Equip3 HR Master data Tax ID',
    'author': 'Hashmicro / Kumar',
    'website': "https://www.hashmicro.com",
    'version': '1.1.14',
    'summary': 'Manage your Tax master.',
    'depends': ['equip3_general_features', 'hr', 'equip3_hr_masterdata_employee', 'hr_payroll_community'],
    'category': 'Human Resources',
    'data': [
        'data/country_domicile_code_data.xml',
        'security/ir.model.access.csv',
        'views/res_company_views.xml',
        'views/hr_tax_bracket.xml',
        'views/hr_employee.xml',
        'views/hr_tax_kpp.xml',
        'views/hr_tax_ptkp.xml',
        'views/hr_tax_pktp_harian_lepas.xml',
        'views/country_domicile_code.xml',
        'views/res_country.xml',
        'views/hr_ptkp_change_status.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
