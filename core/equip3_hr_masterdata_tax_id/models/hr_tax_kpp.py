from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class HashmicroKppMasterData(models.Model):
    _name = 'hr.tax.kpp'
    _description = 'Hr Tax Kpp'
    _inherit = ['mail.thread']

    company_id = fields.Many2one("res.company", string="Company", default=lambda self: self.env.company,
                                 tracking=True)
    branch_id = fields.Many2one("res.branch", string="Branch", domain="[('company_id', '=', company_id)]",
                                tracking=True)
    name = fields.Char(required=True, tracking=True)
    code = fields.Char()
    address = fields.Text(required=True, tracking=True)
    phone = fields.Char()
    fax = fields.Char()

    @api.constrains("name")
    def name_constraimn(self):
        for record in self:
            if record.name:
                name = self.env['hr.tax.kpp'].search([('name', '=', record.name), ('id', '!=', record.id)])
                if name:
                    raise ValidationError("name already use")
