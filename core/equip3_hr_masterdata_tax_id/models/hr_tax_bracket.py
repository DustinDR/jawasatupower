from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class HashmicroHRTaxBracket(models.Model):
    _name = 'hr.tax.bracket'
    _inherit = ['mail.thread']
    _description = 'HR Tax Bracket'

    name = fields.Char()
    sequence = fields.Integer()
    taxable_income_from = fields.Float()
    taxable_income_to = fields.Float()
    tax_rate = fields.Float()
    tax_penalty_rate = fields.Float()
    company_id = fields.Many2one('res.company',default=lambda self:self.env.company.id)
    branch_id = fields.Many2one('res.branch',domain=[('company_id','=',company_id)])

    @api.model
    def default_get(self, fields):
        res = super(HashmicroHRTaxBracket, self).default_get(fields)
        num_list = []
        tax_bracket = self.search([])
        if not tax_bracket:
            res['sequence'] = 1
        if tax_bracket:
            num_list.extend([data.sequence for data in tax_bracket])
            res['sequence'] = max(num_list) + 1


        return res




