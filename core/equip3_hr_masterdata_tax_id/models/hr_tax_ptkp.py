from odoo import _, api, fields, models
from datetime import datetime
from odoo.exceptions import ValidationError


class HashmicroPtkpMasterData(models.Model):
    _name = 'hr.tax.ptkp'
    _description = 'Hr Tax Ptkp'
    _rec_name = 'ptkp_name'
    _inherit = ['mail.thread']

    ptkp_name = fields.Char(required=True, string="PTKP Name")
    ptkp_amount = fields.Float(required=True, string="PTKP Amount")
    company_id = fields.Many2one("res.company", string="Company", default=lambda self: self.env.company,
                                 tracking=True)
    branch_id = fields.Many2one("res.branch", string="Branch", domain="[('company_id', '=', company_id)]",
                                tracking=True)
