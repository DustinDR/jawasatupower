# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class Company(models.Model):
    _inherit = "res.company"

    company_npwp = fields.Char('Company NPWP')
    tax_cutter_name = fields.Many2one('res.users', string='Tax Cutter Name')
    tax_cutter_npwp = fields.Char('Tax Cutter NPWP')
    work_unit = fields.Char('Work Unit')
    corporate_id = fields.Char('Company ID')