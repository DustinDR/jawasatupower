# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from . import hr_tax_kpp
from . import country_domicile_code
from . import res_company
from . import hr_employee
from . import hr_tax_ptkp
from . import hr_tax_bracket
from . import hr_tax_pktp_harian_lepas
from . import res_country
from . import hr_ptkp_change_status