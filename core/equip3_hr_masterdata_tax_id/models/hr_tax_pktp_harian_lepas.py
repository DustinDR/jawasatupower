from odoo import _, api, fields, models


class HRTaxPktpHarianLepas(models.TransientModel):
    _name = 'hr.tax.ptkp.harian.lepas'
    _inherit = 'res.config.settings'
    _description = 'HR Tax Pktp Harian Lepas'

    daily_income_ptkp = fields.Float(string='Daily Income (PTKP)',
                                     config_parameter='equip3_hr_masterdata_tax_id.daily_income_ptkp', default=450000)
    cummulative_income_start = fields.Float(string='Cummulative Income Start',
                                            config_parameter='equip3_hr_masterdata_tax_id.cummulative_income_start', default=4500000)
    cummulative_income_end = fields.Float(string='Cummulative Income End',
                                          config_parameter='equip3_hr_masterdata_tax_id.cummulative_income_end', default=10200000)
    tax_rate = fields.Float(string='Tax Rate (%)', config_parameter='equip3_hr_masterdata_tax_id.tax_rate', default=5)
    tax_penalty_rate = fields.Float(string='Tax Penalty Rate (%)', config_parameter='equip3_hr_masterdata_tax_id.tax_penalty_rate', default=6)
