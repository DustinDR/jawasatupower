# -*- coding: utf-8 -*-
{
    'name': "Equip3 Sale Other Operation Cont",

    'summary': """
        Manage your blanket orders activities""",

    'description': """
        This module manages these features :
        1. Blanket Order
        2. Blanket Order Approval Matrix
        3. Blanket Order to Sale Order
        4. Sale Agreement Type
    """,

    'author': "Hashmicro",
    'category': 'Sales',
    'version': '1.3.7',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'sale',
        'account',
        'stock',
        'blanket_sale_order_app',
        'branch',
        'equip3_sale_operation',
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence.xml',
        'data/ir_cron_data.xml',
        'report/blanket_template.xml',
        'report/blanket_report.xml',
        'data/mail_template_bo.xml',
        'wizards/bo_approval_matrix_sale_reject_views.xml',
        'views/sale_blanket_views.xml',
        'views/force_done_memory_view.xml',
        'views/res_config_setting_views.xml',
        'views/bo_approval_matrix_view.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}