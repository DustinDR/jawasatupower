# -*- coding: utf-8 -*-

from . import sale_blanket
from . import res_config_setting
from . import sale_order
from . import blanket_sale_wizard
from . import force_done_memory
from . import bo_approval_matrix