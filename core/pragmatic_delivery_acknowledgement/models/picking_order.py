from odoo import api, models, fields, tools
import string, random
from datetime import datetime


class picking_order(models.Model):
    _inherit = "picking.order"

    def _compute_broadcast(self):
        Param = self.env['ir.config_parameter'].sudo()
        if Param.get_param('pragmatic_delivery_control_app.is_broadcast_order'):
            self.broadcast = True
        else:
            self.broadcast = False

    customer_code = fields.Char(string="Customer Acknoledgement Code")
    delivery_boy_code = fields.Char(string="Delivery Boy Acknoledgement Code")
    picking_id = fields.Many2one('stock.picking',string="Deliveryboy Picking")
    is_broadcast_order = fields.Boolean(string="Broadcast Order")
    broadcast = fields.Boolean(string="Broadcast",compute="_compute_broadcast")

    @api.model
    def create(self,vals):
        res = super(picking_order, self).create(vals)
        # ack_code = ''.join(random.choices(string.ascii_uppercase + string.digits, k=5))
        Param = self.env['ir.config_parameter'].sudo()
        if Param.get_param('pragmatic_delivery_control_app.is_delivery_acknowledgement'):
            ack_code = ''.join(random.choices(string.digits, k=5))
            res.customer_code = ack_code

            msg = ("<p> Dear Sir / Madam,</p> <p>Your Order delivery acknowledge code is %s</p> <p>Thank You</p>" % ack_code)
            template = self.env['ir.model.data'].get_object('pragmatic_delivery_acknowledgement', 'delivery_acknowledgement_email_template')

            template.body_html = msg
            template.send_mail(res.id, force_send=True)
        return res

    def assign_driver(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Assign Delivery Boy',
            'res_model': 'picking.order.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'context':{'default_sale_order':[(4,self.sale_order.id)]},
            'view_id': self.env.ref('pragmatic_odoo_delivery_boy.assign_driver_wizard', False).id,
            'target': 'new',
        }

    def pick_delivery(self):
        """
        this process happen after the delivery of order completed by delivery boy from the website.
        it is make done picking if 'Enable Delivery Boy Location' is not enable
        it is create a new picking for delivery boy and make transfer like
            store location -> delivery boy location
            delivery boy location -> customer
        """
        # delivery_boy_store_configuration = self.user_has_groups('pragmatic_delivery_control_app.group_delivery_boy_store_configuration')
        picking = self.picking
        picking.action_assign()
        # if not delivery_boy_store_configuration:
        #     if self.picking.state == 'assigned':
        #         for move in picking.move_lines.filtered(lambda m: m.state not in ['done', 'cancel']):
        #             for move_line in move.move_line_ids:
        #                 move_line.qty_done = move_line.product_uom_qty
        #         picking.with_context(skip_immediate=True).button_validate()
        # else:
        picking_type_obj = self.env['stock.picking.type']
        delivery_boy_location = self.env['delivery.boy.store'].search([('delivery_boy_id.partner_id','=',self.delivery_boy.id)])
        deliveryboy_picking_id = picking.copy()
        picking_type_id = picking_type_obj.search([('sequence_code', '=', 'TDB')])
        deliveryboy_picking_id.picking_type_id = picking_type_id.id
        deliveryboy_picking_id.action_assign()
        deliveryboy_picking_id.location_dest_id = delivery_boy_location.location_id.id
        for move in deliveryboy_picking_id.move_lines.filtered(lambda m: m.state not in ['done', 'cancel']):
            for move_line in move.move_line_ids:
                move_line.qty_done = move_line.product_uom_qty
                move_line.location_dest_id = delivery_boy_location.location_id.id
        deliveryboy_picking_id.with_context(skip_immediate=True).button_validate()
        # self.sale_order.write({'state':'picked'})
        self.picking_id = deliveryboy_picking_id.id

        picking.location_id = deliveryboy_picking_id.location_dest_id
        for move in picking.move_lines.filtered(lambda m: m.state not in ['done', 'cancel']):
            for move_line in move.move_line_ids:
                move_line.location_id = deliveryboy_picking_id.location_dest_id
                move_line.qty_done = move_line.product_uom_qty
        picking.with_context(skip_immediate=True).button_validate()
        self.state = 'picked'
        order_stage_id = self.env['order.stage'].search([('action_type', '=', 'picked')])
        if order_stage_id:
            self.stage_id = order_stage_id.id

        """
            reconsile invoice
        """

        move_obj = self.env['account.move'].sudo()
        invoice_id = self.sale_order.invoice_ids
        store_config_id = self.env['store.configuration'].sudo().search(
            [('location_id', '=', self.picking.location_id.id)])
        if store_config_id and invoice_id:
            line_ids = [(0, 0, {
                'partner_id': self.delivery_boy.id,  # delivery boy id,
                'account_id': store_config_id.delivery_boy_account_id.id,
                'journal_id': store_config_id.delivery_boy_journal_id.id,
                'name': 'Cash received by delivery boy from order Number {0}'.format(self.sale_order.name),
                'amount_currency': 0.0,
                'debit': invoice_id.amount_total,
                'credit': 0.0,
            }),
                        (0, 0, {
                            'partner_id': invoice_id.partner_id.id,
                            'account_id': invoice_id.partner_id.property_account_receivable_id.id,
                            'journal_id': store_config_id.delivery_boy_journal_id.id,
                            'name': 'Cash received by delivery boy from order Number {0}'.format(self.sale_order.name),
                            'amount_currency': 0.0,
                            'debit': 0.0,
                            'credit': invoice_id.amount_total,
                        })]

            vals = {
                'journal_id': store_config_id.delivery_boy_journal_id.id,
                'ref': 'Cash received by delivery boy from order Number {0}'.format(self.sale_order.name),
                'narration': 'Delivery Boy Receipt',
                'date': datetime.now(),
                'line_ids': line_ids,
            }
            journal_id = move_obj.create(vals)
            journal_id.sudo().action_post()
            self.delivery_boy_move_id = journal_id.id

            line_1 = journal_id.line_ids.filtered(lambda line: line.account_id.user_type_id.type == 'receivable')
            line_2 = invoice_id.line_ids.filtered(lambda line: (line.account_id.user_type_id.type == 'receivable'))
            (line_1 + line_2).reconcile()

        if invoice_id:
            self.action_picking_order_paid(invoice_id.id)

        """
            create invoice for order
        """
        if not self.sale_order.invoice_count:
            invoice_obj = self.sale_order._create_invoices()
            invoice_obj.action_post()
            self.invoice = invoice_obj.id

    def print_invoice(self):
        self.ensure_one()
        if self.sale_order.invoice_ids:
            action = self.sale_order.invoice_ids.action_invoice_print()
            # action.update({'close_on_report_download': True})
            return action

    def check_deliveryboy_code(self,deliveryboy_code):
        if self.customer_code == deliveryboy_code:
            self.delivery_boy_code = deliveryboy_code
            return True
        else:
            return False

    def delivery_order_ready(self):
        self.is_broadcast_order = True
