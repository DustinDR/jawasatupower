# -*- coding: utf-8 -*-
import base64
from io import BytesIO
from datetime import date, datetime, timedelta
import qrcode
from odoo import models, fields, api, _


class modulequip(models.Model):
    _inherit = 'maintenance.equipment'

    qr_code = fields.Binary("QR Code", attachment=True, store=True)
    purchase_value = fields.Many2one(string='Purchase Value', default=False)
    category_id = fields.Many2one(string='Asset Category', required=True)
    company_id_asset = fields.Many2one('res.partner', string='Owner')
    maintenance_teams_id = fields.Many2one('maintenance.teams', string='Assigned To', ondelete='restrict')
    fac_area = fields.Many2one('maintenance.facilities.area', string='Facilities Area', tracking=True)
    maintenance_hm = fields.One2many('maintenance.hour.meter', 'maintenance_asset', string='Hour Meter')
    scrap_date = fields.Date(string='Scrap Date')
    purchase_value = fields.Many2one(string='Purchase Value')
    active = fields.Boolean(default=True)
    state = fields.Selection([('operative','Operative'), ('breakdown','Breakdown'),('maintenance','Maintenance'), ('scrapped','Scrapped')], default="operative", string="Status")
    work_order = fields.Integer(string="Work Order")
    repair = fields.Integer (string="Repair")
    maintenance_f_id = fields.Many2one(string="Maintenance Facilities Area", comodel_name="maintenance.facilities.area", ondelete="restrict")
    hm_count = fields.Integer(string='Hour Meter Count', compute='_compute_hm_count')
    wo_count  = fields.Integer(string='Work Order Count', compute='_compute_work_order_count', readonly=True)
    repair_count = fields.Integer(string='Repair Count', compute='_compute_repair_count')
    request_count = fields.Integer(string='Requests Count', compute='_compute_request_count')
    odometer_mp_count = fields.Integer(string="Odometer Plans", compute='_compute_plan_count')
    odometer_mp_ids = fields.Many2many('maintenance.plan', compute="_compute_plan_count")
    hourmeter_mp_count = fields.Integer(string="Hourmeter Plans", compute='_compute_plan_count')
    hourmeter_mp_ids = fields.Many2many('maintenance.plan', compute="_compute_plan_count")
    preventive_mp_count = fields.Integer(string="Preventive Plans", compute='_compute_plan_count')
    preventive_mp_ids = fields.Many2many('maintenance.plan', compute="_compute_plan_count")
    am_count = fields.Integer(string='Asset Moves Count', compute='_compute_am_count', readonly=True)
    account_asset_id = fields.Many2one('account.asset.asset', string="Account Asset")

    @api.model
    def get_import_templates(self):
        return [{
            'label': _('Import Template for Assets/Vehicles'),
            'template': '/equip3_asset_fms_masterdata/static/xls/asset_vehicle_template.xls'
        }]

    @api.model
    def create(self, vals):
        result = super().create(vals)

        base_url = self.env['ir.config_parameter'].get_param(
            'web.base.url')
        base_url += '/page/maintenance_request/?asset=%d&facility_area=%d'%(result.id,result.fac_area.id)

        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=4,
        )
        qr.add_data(base_url)
        qr.make(fit=True)
        img = qr.make_image()
        temp = BytesIO()
        img.save(temp, format="PNG")
        qr_image = base64.b64encode(temp.getvalue())
        result.qr_code = qr_image
        result.create_account_asset()
        return result

    def name_get(self):
        res = super(modulequip, self).name_get()
        result = []
        for record in self:
            name = record.name
            if record.serial_no:
                name += ' ' + '[' + record.serial_no + ']'
            result.append((record.id, name))
        return result

    def create_account_asset(self):
        vals = {
            'name' : self.name,
            'company_id' : self.company_id.id,
            'date' : date.today(),
            'first_depreciation_manual_date' : date.today(),
            'equipment_id' : self.id,
            'value' : 0,
        }
        asset_id = self.env['account.asset.asset'].create(vals)
        self.account_asset_id = asset_id.id
    
    def action_account_asset(self):
        context = dict(self.env.context) or {}
        context.update({'default_equipment_id': self.id})
        return{
            'type': 'ir.actions.act_window',
            'view_type' : 'form',
            'view_mode' : 'form',
            'res_model': 'account.asset.asset',
            'target': 'current',
            'context': context,
            'res_id': self.account_asset_id.id,
        }

    def breakdown_action(self):
        self.write({'state': 'breakdown'})

    def scrapped_action(self):
        self.write({'state': 'scrapped'})

    def _compute_plan_count(self):
        plan_data = self.env['maintenance.plan'].search([])
        self.odometer_mp_count = None
        self.odometer_mp_ids = None
        self.hourmeter_mp_count = None
        self.hourmeter_mp_ids = None
        self.preventive_mp_count = None
        self.preventive_mp_ids = None
        eq_id = None
        odometer_mps = None
        odometer_mps_lst = []
        hourmeter_mps = None
        hourmeter_mps_lst = []
        prev_mps = None
        prev_mps_lst = []
        for rec in self:
            for plan in plan_data:
                if plan.is_odometer_m_plan:
                    odometer_mps = plan.filtered(lambda odo: rec.category_id in odo.maintenance_category_ids)
                    if not odometer_mps:
                        for eq_id in plan.task_check_list_ids:
                            if eq_id.equipment_id.id == rec.id:
                                odometer_mps = plan
                    if odometer_mps:
                        odometer_mps_lst.append(odometer_mps.id)
                if plan.is_hourmeter_m_plan:
                    hourmeter_mps = plan.filtered(lambda odo: rec.category_id in odo.maintenance_category_ids)
                    if not hourmeter_mps:
                        for eq_id in plan.task_check_list_ids:
                            if eq_id.equipment_id.id == rec.id:
                                hourmeter_mps += plan
                    if hourmeter_mps:
                        hourmeter_mps_lst.append(hourmeter_mps.id)
                if plan.is_preventive_m_plan:
                    prev_mps = plan.filtered(lambda odo: rec.category_id in odo.maintenance_category_ids)
                    if not prev_mps:
                        for eq_id in plan.task_check_list_ids:
                            if eq_id.equipment_id.id == rec.id:
                                prev_mps += plan
                    if prev_mps:
                        prev_mps_lst.append(prev_mps.id)
               
            rec.odometer_mp_ids = [(4, x, None) for x in odometer_mps_lst]
            rec.odometer_mp_count = len(odometer_mps_lst)

            rec.hourmeter_mp_ids = [(4, x, None) for x in hourmeter_mps_lst]
            rec.hourmeter_mp_count = len(hourmeter_mps_lst)
            
            rec.preventive_mp_ids = [(4, x, None) for x in prev_mps_lst]
            rec.preventive_mp_count = len(prev_mps_lst)
    
    def action_view_odometer_mp(self):
        self.ensure_one()
        plan_data = self.env['maintenance.plan'].search([])
        main_main_plan_lst = self.env['maintenance.plan'].search([('is_odometer_m_plan', '=', True)]).mapped('id')
        check_main_plan_lst = self.env['plan.task.check.list'].search([('equipment_id', '=', self.id)]).mapped('maintenance_plan_id.id')
        equi_main_plan_lst = plan_data.filtered(lambda odo: self.category_id in odo.maintenance_category_ids).mapped('id')
        
        action_lst = []
        for eq in equi_main_plan_lst:
            if eq in main_main_plan_lst:
                action_lst.append(eq)
        for eq1 in check_main_plan_lst:
            if eq1 in main_main_plan_lst:
                action_lst.append(eq1)
        action_lst = list(set(action_lst))
        view_form_id = self.env.ref('maintenance_plan.maintenance_plan_view_form').id
        view_tree_id = self.env.ref('maintenance_plan.maintenance_plan_view_tree').id
        action = {
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', action_lst)],
            'view_mode': 'tree,form',
            'name': _('Odometer Maintenance Plan'),
            'res_model': 'maintenance.plan',
            'context': {'default_equipment_id': self.id, 'default_is_odometer_m_plan': True}
        }
        if len(self.odometer_mp_ids) == 1:
            action.update({'views': [(view_form_id, 'form')], 'res_id': self.odometer_mp_ids.id})
        else:
            action['views'] = [(view_tree_id, 'tree'), (view_form_id, 'form')]
        return action

    def action_view_hourmeter_mp(self):
        self.ensure_one()
        plan_data = self.env['maintenance.plan'].search([])
        main_main_plan_lst = self.env['maintenance.plan'].search([('is_hourmeter_m_plan', '=', True)]).mapped('id')
        check_main_plan_lst = self.env['plan.task.check.list'].search([('equipment_id', '=', self.id)]).mapped('maintenance_plan_id.id')
        equi_main_plan_lst = plan_data.filtered(lambda odo: self.category_id in odo.maintenance_category_ids).mapped('id')
        action_lst = []
        for eq in equi_main_plan_lst:
            if eq in main_main_plan_lst:
                action_lst.append(eq)
        for eq1 in check_main_plan_lst:
            if eq1 in main_main_plan_lst:
                action_lst.append(eq1)
        action_lst = list(set(action_lst))
        view_form_id = self.env.ref('maintenance_plan.maintenance_plan_view_form').id
        view_tree_id = self.env.ref('maintenance_plan.maintenance_plan_view_tree').id
        action = {
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', action_lst)],
            'view_mode': 'tree,form',
            'name': _('Hourmeter Maintenance Plan'),
            'res_model': 'maintenance.plan',
            'context': {'default_equipment_id': self.id}
        }
        if len(self.hourmeter_mp_ids) == 1:
            action.update({'views': [(view_form_id, 'form')], 'res_id': self.hourmeter_mp_ids.id})
        else:
            action['views'] = [(view_tree_id, 'tree'), (view_form_id, 'form')]
        return action

    def action_view_preventive_mp(self):
        self.ensure_one()
        plan_data = self.env['maintenance.plan'].search([])
        main_main_plan_lst = self.env['maintenance.plan'].search([('is_preventive_m_plan', '=', True)]).mapped('id')
        check_main_plan_lst = self.env['plan.task.check.list'].search([('equipment_id', '=', self.id)]).mapped('maintenance_plan_id.id')
        equi_main_plan_lst = plan_data.filtered(lambda odo: self.category_id in odo.maintenance_category_ids).mapped('id')
        action_lst = []
        for eq in equi_main_plan_lst:
            if eq in main_main_plan_lst:
                action_lst.append(eq)
        for eq1 in check_main_plan_lst:
            if eq1 in main_main_plan_lst:
                action_lst.append(eq1)
        action_lst = list(set(action_lst))
        view_form_id = self.env.ref('maintenance_plan.maintenance_plan_view_form').id
        view_tree_id = self.env.ref('maintenance_plan.maintenance_plan_view_tree').id
        action = {
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', action_lst)],
            'view_mode': 'tree,form',
            'name': _('Preventive Maintenance Plan'),
            'res_model': 'maintenance.plan',
            'context': {'default_equipment_id': self.id}
        }
        if len(self.preventive_mp_ids) == 1:
            action.update({'views': [(view_form_id, 'form')], 'res_id': self.preventive_mp_ids.id})
        else:
            action['views'] = [(view_tree_id, 'tree'), (view_form_id, 'form')]
        return action

    hm_unit = fields.Selection([
        ('hour', 'hours'),
        ('jam', 'j')
        ], 'Hour meter Unit', default='hour', help='Unit of the hourmeter ', required=True)

    def _compute_hm_count(self):
        for rec in self:
            hm_count = self.env['maintenance.hour.meter'].search_count([('maintenance_asset', '=', rec.id)])
            rec.hm_count = hm_count

    def _compute_repair_count(self):
        repair_obj = self.env['maintenance.repair.order']
        repair_ids = repair_obj.search([('facilities_area','=', self.id)])
        for book in self:
            book.update({
                'repair_count' : len(repair_ids)
                })

    def _compute_request_count(self):
        for rec in self:
            request_count = self.env['maintenance.request'].search_count([('equipment_id', '=', rec.id)])
            rec.request_count = request_count

    def _compute_wo_count(self):
        for rec in self:
            wo_count = self.env['maintenance.work.order'].search_count([('facility', '=', rec.id)])
            rec.wo_count = wo_count

    def _compute_work_order_count(self):
        work_order_obj = self.env['maintenance.work.order']
        work_order_ids = work_order_obj.search([('facility','=', self.id)])
        for book in self:
            book.update({
                'wo_count' : len(work_order_ids)
                })
    def _compute_am_count(self):
        for rec in self:
            am_count = self.env['inter.asset.transfer'].search_count([('asset_ids.asset_id', '=', rec.id)])
            rec.am_count = am_count

    def wo_action_link(self):
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'list',
            'view_mode': 'list,form',
            'name': 'Work Order',
            'res_model': 'maintenance.work.order',
            'domain': [('facility','=',self.id)]}

    def asset_moves_link(self):
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'list',
            'view_mode': 'list,form',
            'name': 'Asset Moves',
            'res_model': 'inter.asset.transfer',
            'domain': [('asset_ids.asset_id','=',self.id)]}

    def repair_action_link(self):
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'list',
            'view_mode': 'list,form',
            'name': 'Repair',
            'res_model': 'maintenance.repair.order',
            'domain': [('facilities_area','=',self.id)]}

class MaintenanceEquipmentCategory(models.Model):
    _inherit = 'maintenance.equipment.category'

    odometer_mp_count = fields.Integer(string="Odometer Plans", compute='_compute_plan_count')
    hourmeter_mp_count = fields.Integer(string="Hourmeter Plans", compute='_compute_plan_count')
    preventive_mp_count = fields.Integer(string="Preventive Plans", compute='_compute_plan_count')


    def _compute_plan_count(self):
        self.odometer_mp_count = None
        self.hourmeter_mp_count = None
        self.preventive_mp_count = None
        plan_date = self.env['maintenance.plan'].search([])
        for rec in self:
            rec.odometer_mp_count = len(plan_date.filtered(lambda odo: rec.id in odo.maintenance_category_ids.ids and odo.is_odometer_m_plan == True))
            rec.hourmeter_mp_count = len(plan_date.filtered(lambda odo: rec.id in odo.maintenance_category_ids.ids and odo.is_hourmeter_m_plan == True))
            rec.preventive_mp_count = len(plan_date.filtered(lambda odo: rec.id in odo.maintenance_category_ids.ids and odo.is_preventive_m_plan == True))

    @api.model
    def get_import_templates(self):
        return [{
            'label': _('Import Template for Asset Categories'),
            'template': '/equip3_asset_fms_masterdata/static/xls/asset_categories_template.xls'
        }]

class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    category_id = fields.Many2one(required=False)
    equipment_id = fields.Many2one('maintenance.equipment', string="Reference")

