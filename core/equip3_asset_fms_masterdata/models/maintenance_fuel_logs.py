from odoo import models, fields, api, _
from datetime import datetime, date, timedelta

class MaintenanceFuelLogs(models.Model):
    _name = 'maintenance.fuel.logs'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'name'

    name = fields.Char("Fuel Logs",default='New')
    vehicle = fields.Many2one('maintenance.equipment', string='Vehicle', domain="[('vehicle_checkbox', '=', True)]", required=True)
    date = fields.Datetime(string='Date', required=True)
    createon = fields.Date('Created On', tracking=True, default=fields.Date.context_today,
                               help="Date requested for the maintenance to happen")
    createby = fields.Many2one(comodel_name='res.users', string='Created By', default=lambda self: self.env.user)
    liter = fields.Float(string='Liter', required=True)
    total_price = fields.Float(string='Total Price', required=True)
    fuel_type = fields.Many2one('product.product', string='Fuel Type', related='vehicle.fuel_type')
    analytic_group = fields.Many2many('account.analytic.tag', string='Analytic Group')

    @api.onchange('vehicle')
    def set_code(self):
        self.fuel_type = self.vehicle.fuel_type

    # def name_get(self):
    #     res = []
    #     for rec in self:
    #         res.append((rec.id, 'FUL / %s' % (rec.date)))
    #     return res

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('maintenance.fuel.logs.sequence')
        return super(MaintenanceFuelLogs, self).create(vals)

    @api.model
    def get_import_templates(self):
        return [{
            'label': _('Import Template for Fuel Logs'),
            'template': '/equip3_asset_fms_masterdata/static/xls/fuel_logs_template.xls'
        }]