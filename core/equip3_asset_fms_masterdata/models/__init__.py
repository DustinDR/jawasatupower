# -*- coding: utf-8 -*-

from . import maintenance_asset
from . import maintenance_facilities
from . import maintenance_assignation
from . import maintenance_teams
from . import maintenance_inherit
from . import maintenance_approval
from . import maintenance_vehicle
from . import maintenance_fuel_logs