from pickle import TRUE
from odoo import api, fields, models, _
from odoo.osv import expression
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, date

class MaintenanceHourMeter(models.Model):
    _name = 'maintenance.hour.meter'

    name = fields.Char(compute='_compute_asset_log_name', store=True)
    maintenance_asset = fields.Many2one('maintenance.equipment', string='Asset', readonly=True)
    unit = fields.Selection(related='maintenance_asset.hm_unit', string="Unit", readonly=True)
    date = fields.Date(string='Date', required=True)
    value = fields.Float(string='Hour Meter Value',required=True)

    @api.depends('maintenance_asset', 'date')
    def _compute_asset_log_name(self):
        for record in self:
            name = record.maintenance_asset.name
            if not name:
                name = str(record.date)
            elif record.date:
                name += ' / ' + str(record.date)
            record.name = name

    @api.onchange('maintenance_asset')
    def _onchange_asset(self):
        if self.maintenance_asset:
            self.unit = self.maintenance_asset.hm_unit

    @api.constrains('date')
    def _check_date(self):
        today = date.today()
        if self.date > today:
            raise ValidationError(_('You are not allowed to register a date later than today\'s date \n Please register a date prior to or on today.'))

class MaintenanceVehicleOdometer(models.Model):
    _name = 'maintenance.vehicle'

    name = fields.Char(compute='_compute_vehicle_log_name', store=True)
    date = fields.Date(string='Date', required=True)
    maintenance_vehicle = fields.Many2one('maintenance.equipment', string='Vehicle',  readonly=True)
    value = fields.Float('Odometer Value', group_operator="max", required=True)
    unit = fields.Selection(related='maintenance_vehicle.odometer_unit', string="Unit", readonly=True)

    @api.depends('maintenance_vehicle', 'date')
    def _compute_vehicle_log_name(self):
        for record in self:
            name = record.maintenance_vehicle.name
            if not name:
                name = str(record.date)
            elif record.date:
                name += ' / ' + str(record.date)
            record.name = name

    @api.onchange('maintenance_vehicle')
    def _onchange_vehicle(self):
        if self.maintenance_vehicle:
            self.unit = self.maintenance_vehicle.odometer_unit


    @api.constrains('date')
    def _check_date(self):
        today = date.today()
        if self.date > today:
            raise ValidationError(_('You are not allowed to register a date later than today\'s date \n Please register a date prior to or on today.'))

class MaintenanceVehicle(models.Model):
    _inherit = 'maintenance.equipment'

    vehicle_checkbox = fields.Boolean(string="Check box")
    maintenance_v = fields.One2many('maintenance.vehicle', 'maintenance_vehicle', string='Vehicle')
    driver_1 = fields.Many2one('res.partner', string='Driver 1')
    driver_2 = fields.Many2one('res.partner', string='Driver 2')
    engine_number = fields.Char('Engine Number')
    chassis_number = fields.Char('Chassis Number')
    transmission =  fields.Selection(string='Transmission', selection=[('manual', 'Manual'), ('automatic', 'Automatic')])
    fuel_type = fields.Many2one('product.product', string='Fuel Type')
    horsepower = fields.Integer(string="Horsepower")
    manufacture_year = fields.Integer(string="Manufacture Year")
    model_year = fields.Integer(string="Model Year")
    capacity = fields.Float(string='Capacity (ton)')
    volume = fields.Float(string='Volume (m3)')
    frequency_hourmeter_ids = fields.One2many(comodel_name='request.line', inverse_name='hourmeter_id', string='Frequency Hour')
    frequency_odoometer_ids = fields.One2many(comodel_name='request.line', inverse_name='odoometer_id', string='Frequency Odoo')
    threshold_hourmeter_ids = fields.One2many(comodel_name='threshold.line', inverse_name='thresholdhourmeter_id', string='Frequency Hour')
    threshold_odoometer_ids = fields.One2many(comodel_name='threshold.line', inverse_name='thresholdodometer_id', string='Frequency Odoo')
    
    odometer_unit = fields.Selection([
        ('kilometers', 'km'),
        ('miles', 'mi')
        ], 'Odometer Unit', default='kilometers', help='Unit of the odometer ', required=True)

    odometer_count = fields.Integer(string='Odometer Count', compute='_compute_odometer_count')

    def vehicle_moves_link(self):
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'list',
            'view_mode': 'list,form',
            'name': 'Asset Moves',
            'res_model': 'inter.asset.transfer',
            'domain': [('asset_ids.asset_id','=',self.id)]}

    def _compute_odometer_count(self):
        for rec in self:
            odometer_count = self.env['maintenance.vehicle'].search_count([('maintenance_vehicle', '=', rec.id)])
            rec.odometer_count = odometer_count

    fuel_logs_count = fields.Integer(string='Odometer Count', compute='_compute_fuel_logs_count')

    def _compute_fuel_logs_count(self):
        for rec in self:
            fuel_logs_count = self.env['maintenance.fuel.logs'].search_count([('vehicle', '=', rec.id)])
            rec.fuel_logs_count = fuel_logs_count

class RequestLine(models.Model):
    _name = 'request.line'
    
    hourmeter_id = fields.Many2one(comodel_name='maintenance.equipment', string='Hour Meter')
    is_hourmeter_m_plan = fields.Many2one(comodel_name='maintenance.plan', string='Hour Meter Maintenance', domain=[('is_hourmeter_m_plan', '=', True)])
    floorhour_value = fields.Integer(string='Floor Value')
    
    odoometer_id = fields.Many2one(comodel_name='maintenance.equipment', string='Odoo Meter')
    is_odometer_m_plan = fields.Many2one(comodel_name='maintenance.plan', string='Odo Meter Maintenance', domain=[('is_odometer_m_plan', '=', True)])
    floorodoo_value = fields.Integer(string='Floor Value')
    
class ThresholdLine(models.Model):
    _name = 'threshold.line'
    
    thresholdhourmeter_id = fields.Many2one(comodel_name='maintenance.equipment', string='Hour Meter')
    is_hourmeter = fields.Many2one(comodel_name='maintenance.plan', string='Hour Meter Maintenance', domain=[('is_hourmeter_m_plan', '=', True)])
    last_threshold = fields.Float(string='Last Threshold')
    
    thresholdodometer_id = fields.Many2one(comodel_name='maintenance.equipment', string='Odoo Meter')
    is_odometer = fields.Many2one(comodel_name='maintenance.plan', string='Odo Meter Maintenance', domain=[('is_odometer_m_plan', '=', True)])    
    last_threshold = fields.Float(string='Last Threshold')