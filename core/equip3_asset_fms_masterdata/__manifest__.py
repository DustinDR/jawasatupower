# -*- coding: utf-8 -*-
{
    'name': "Equip3 Asset Fms Masterdata",

    'summary': """
        a Modul for Masterdata""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '1.2.4',
    'application': True,

    # any module necessary for this one to work correctly
    'depends': ['base', 'maintenance', 'mail', 
                'stock', 'maintenance_plan',
                'hr_maintenance', 
                'website_job_workorder_request',
                'equipment_print_pdf_image',
                'material_purchase_requisitions'],

    # always loaded
    'data': [
        # 'views/security.xml',
        'security/ir.model.access.csv',
        'views/asset.xml',
        'views/view.xml',
        'views/views.xml',
        'data/sequence.xml',
        'views/maintenance_teams.xml',
        'views/maintenance_vehicle.xml',
        'views/maintenance_fuel_logs.xml',
        'views/account_asset_asset_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
