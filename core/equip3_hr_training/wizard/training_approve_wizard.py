from odoo import fields, models, api


class TrainingRequestWizard(models.TransientModel):
    _name = 'training.request.wizard'

    feedback = fields.Text()

    def submit(self):
        """ Prepare the training feedback and trigger Approve. """
        self.ensure_one()
        training_req = self.env['training.request'].browse(self._context.get('active_ids', []))
        training_req.feedback_parent = self.feedback
        training_req.action_approve()


class TrainingConductWizard(models.TransientModel):
    _name = 'training.conduct.wizard'

    feedback = fields.Text()

    def submit(self):
        """ Prepare the training feedback and trigger Approve. """
        self.ensure_one()
        training_req = self.env['training.conduct'].browse(self._context.get('active_ids', []))
        training_req.feedback_parent = self.feedback
        training_req.action_approve()
