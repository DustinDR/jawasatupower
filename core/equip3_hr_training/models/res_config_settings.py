from odoo import api, fields, models
from odoo.exceptions import ValidationError


class Equip3HrTrainingResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'
    training_type_approval = fields.Selection(
        [('employee_hierarchy', 'By Employee Hierarchy'), ('approval_matrix', 'By Approval Matrix')],
        config_parameter='equip3_hr_training.training_type_approval', default='employee_hierarchy')
    training_level = fields.Integer(config_parameter='equip3_hr_training.training_level', default=1)
    send_by_wa_training = fields.Boolean(config_parameter='equip3_hr_training.send_by_wa_training')
    send_by_email_training = fields.Boolean(config_parameter='equip3_hr_training.send_by_email_training', default=True)

    @api.onchange("training_level")
    def _onchange_level(self):
        if self.training_level < 1:
            self.training_level = 1
