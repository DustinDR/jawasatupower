from odoo import api, fields, models
from odoo.exceptions import ValidationError


class TrainingHistories(models.Model):
    _name = 'training.histories'
    _description = 'Training Histories'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'id desc'

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('training.histories') or 'New'
        return super(TrainingHistories, self).create(vals)

    def _compute_training_required(self):
        for rec in self:
            if rec.course_ids in rec.job_id.course_ids:
                rec.training_required = 'yes'
            else:
                rec.training_required = 'no'
            if rec.state != 'expired':
                rec.update_state()
            else:
                rec.state = rec.state

    def update_state(self):
        for rec in self:
            if not rec.stage_course_id.stage_id:
                rec.state = 'to_do'
            elif rec.stage_course_id.stage_id.name == 'Approved' or rec.stage_course_id.stage_id.name == 'On Progress':
                rec.state = 'on_progress'
            elif rec.training_conduct_line_id.status == 'Success':
                rec.state = 'success'
            elif rec.training_conduct_line_id.status == 'Failed':
                rec.state = 'failed'

    # def required_update(self):
    #     for rec in self:
    #         print ('ttttttttttttttt')
    #         if rec.course_ids in rec.job_id.course_ids:
    #             print ('ddddddddddddd')
    #             rec.training_required = 'yes'
    #         else:
    #             print ('qqqqqqqqqqqqqqqqqq')
    #             rec.training_required = 'no'

    name = fields.Char(string='Name')
    employee_id = fields.Many2one('hr.employee', 'Employee')
    job_id = fields.Many2one('hr.job', string='Job Position', related='employee_id.job_id')
    course_ids = fields.Many2many('training.courses', string='Courses')
    state = fields.Selection(
        [('to_do', 'To Do'), ('on_progress', 'On Progress'), ('success', 'Success'), ('failed', 'Failed'),
         ('expired', 'Expired')], string='State', tracking=True, default='to_do')
    training_required = fields.Selection([('no', 'No'), ('yes', 'Yes')], default='no', string='Training Required', compute='_compute_training_required')
    company_id = fields.Many2one('res.company', string='Company', tracking=True, default=lambda self: self.env.company)
    training_conduct_id = fields.Many2one('training.conduct', string='Training Conduct Origin')
    expiry_date = fields.Date(string='Expiry Date')
    start_date = fields.Date('Date Start', related='training_conduct_id.start_date')
    end_date = fields.Date('Date Completed', related='training_conduct_id.end_date')
    stage_course_id = fields.Many2one('training.courses.stages', related='training_conduct_id.stage_course_id')
    stage_course_domain_ids = fields.Many2many('training.courses.stages',
                                               related='training_conduct_id.stage_course_domain_ids')
    training_conduct_line_id = fields.Many2one('training.conduct.line', string='Training Conduct Line Origin')
    training_req_id = fields.Many2one('training.request', 'Training Request Id')
    created_by_model = fields.Selection([('by_job', 'By Job'), ('by_conduct', 'By Conduct'), ('by_request', 'By Request'), ('by_update_from_conduct', 'By Update from conduct')])
    
    
    
    def custom_menu(self):
        # search_view_id = self.env.ref("hr_contract.hr_contract_view_search")
        if  self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_training_supervisor') and not self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_reward_warning_hr_manager'):
            employee_ids = []
            my_employee = self.env['hr.employee'].sudo().search([('user_id','=',self.env.user.id)])
            if my_employee:
                for child_record in my_employee.child_ids:
                    employee_ids.append(my_employee.id)
                    employee_ids.append(child_record.id)
                    child_record._get_amployee_hierarchy(employee_ids,child_record.child_ids,my_employee.id)
            return {
                'type': 'ir.actions.act_window',
                'name': 'Training Histories',
                'res_model': 'training.histories',
                'target':'current',
                'view_mode': 'tree,form',
                'domain': [('employee_id', 'in', employee_ids)],
                'context':{'search_default_group_employee_id':1},
                'help':"""<p class="o_view_nocontent_smiling_face">
                    Create a new Training 
                </p>"""
                # 'context':{'search_default_current':1, 'search_default_group_by_state': 1},
                # 'search_view_id':search_view_id.id,
                
            }
        else:
            return {
                'type': 'ir.actions.act_window',
                'name': 'Training Histories',
                'res_model': 'training.histories',
                'target':'current',
                'view_mode': 'tree,form',
                # 'domain': [('state','=','to_approve')],
                'context':{'search_default_group_employee_id':1,'is_approve_manager':True},
                'help':"""<p class="o_view_nocontent_smiling_face">
                    Create a new Training 
                </p>""",
                # 'context':{'is_approve_manager':True},
                # 'search_view_id':search_view_id.id,
            }