from odoo import api, fields, models
from odoo.exceptions import ValidationError



class TrainingCourses(models.Model):
    _name = 'training.courses'
    _description = 'Training Courses for Employee'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(string='Course Name', tracking=True, required=True)
    estimated_cost = fields.Selection(
        [('idr', 'IDR'), ('eur', 'EUR'), ('usd', 'USD')], string='Estimated Cost', tracking=True, default='idr')
    renewable = fields.Integer(string="Renewable Every")
    minimal_score = fields.Float(string='Minimal Score', tracking=True)
    description = fields.Text(string='Description', tracking=True)
    create_by = fields.Many2one('res.users', 'Created by', default=lambda self: self.env.user)
    create_date = fields.Date('Created on', default=fields.date.today())
    company_id = fields.Many2one('res.company', string='Company', tracking=True, default=lambda self: self.env.company)
    stage_ids = fields.One2many('training.courses.stages','course_id')
    color = fields.Integer()
    
    
    @api.onchange('name')
    def _onchange_name(self):
        for record in self:
            if record.name:
                line_ids = []
                if not record.stage_ids:
                    stage_ids = self.env['training.stages'].search([])
                    for record_stage in stage_ids:
                        line_ids.append((0,0,{'sequence':record_stage.sequence,
                                              'stage_id':record_stage.id
                                              }))
                    record.stage_ids = line_ids
                    
    @api.onchange('stage_ids')
    def _onchange_stage_ids(self):
        for record in self:
            if record.stage_ids:
                if len(record.stage_ids.filtered(lambda line:line.pre_test)) >1:
                    raise ValidationError("Pre-Test Cannot more than one")
                if len(record.stage_ids.filtered(lambda line:line.post_test)) >1:
                    raise ValidationError("Post-Test Cannot more than one")
                if len(record.stage_ids.filtered(lambda line:line.start)) >1:
                    raise ValidationError("Start Stage Cannot more than one")
                if len(record.stage_ids.filtered(lambda line:line.completed)) >1:
                    raise ValidationError("End Stage Cannot more than one")
            
                    
    
    
class TrainingCourseStages(models.Model):
    _name = 'training.courses.stages'
    _rec_name = 'stage_id'
    _order = 'sequence'
    
    sequence = fields.Integer()
    course_id = fields.Many2one('training.courses',ondelete='cascade')
    stage_id = fields.Many2one('training.stages')
    is_default_stages = fields.Boolean('Cannot be delete or edit', related='stage_id.is_default_stages')
    survey_pre_test_id = fields.Many2one('survey.survey')
    survey_post_test_id = fields.Many2one('survey.survey')
    pre_test = fields.Boolean()
    post_test = fields.Boolean()
    start = fields.Boolean()
    completed = fields.Boolean() 
    fold = fields.Boolean(related='stage_id.fold')    
    remark =  fields.Text()
    
    
    
    @api.onchange('pre_test','post_test')
    def _onchange_pre_post_test_(self):
        for record in self:
            if record.pre_test:
                if record.post_test:
                    record.post_test = False
                    record.pre_test = False
