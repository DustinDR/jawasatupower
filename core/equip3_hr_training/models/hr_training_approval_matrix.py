# -*- coding: utf-8 -*-

from odoo import api, fields, models, _, tools


class TrainingApprovalMatrix(models.Model):
    _name = 'hr.training.approval.matrix'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Training Approval Matrix"
    _order = 'create_date desc'

    name = fields.Char('Name')
    company_id = fields.Many2one('res.company', default=lambda self: self.env.company.id)
    apply_to = fields.Selection(
        [('by_employee', 'By Employee'), ('by_job_position', 'By Job Position'), ('by_department', 'By Department')])
    employee_ids = fields.Many2many('hr.employee')
    department_ids = fields.Many2many('hr.department')
    job_ids = fields.Many2many('hr.job')
    level = fields.Integer(compute="_get_level")
    approval_matrix_ids = fields.One2many('hr.training.approval.matrix.line', 'approval_matrix_id')
    applicable_to = fields.Selection(
        [('training_request', 'Training Request'), ('training_conduct', 'Training Conduct')],
        default='training_request', string="Applicable To")

    @api.depends('approval_matrix_ids')
    def _get_level(self):
        for record in self:
            if record.approval_matrix_ids:
                record.level = len(record.approval_matrix_ids)
            else:
                record.level = 0


class TrainingApprovalMatrixline(models.Model):
    _name = 'hr.training.approval.matrix.line'

    approval_matrix_id = fields.Many2one('hr.training.approval.matrix')
    sequence = fields.Integer()
    approvers = fields.Many2many('res.users')
    minimum_approver = fields.Integer(default=1)

    @api.model
    def default_get(self, fields):
        res = super(TrainingApprovalMatrixline, self).default_get(fields)
        if self.env.context:
            context_keys = self.env.context.keys()
            next_sequence = 1
            if 'approval_matrix_ids' in context_keys:
                if len(self.env.context.get('approval_matrix_ids')) > 0:
                    next_sequence = len(self.env.context.get('approval_matrix_ids')) + 1
        res.update({'sequence': next_sequence})
        return res
