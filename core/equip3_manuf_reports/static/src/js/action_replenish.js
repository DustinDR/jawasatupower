odoo.define('equip3_manuf_reports.ActionMrpMpsReplenish', function (require) {
'use strict';

const { ComponentWrapper } = require('web.OwlCompatibility');

var concurrency = require('web.concurrency');
var core = require('web.core');
var Pager = require('web.Pager');
var AbstractAction = require('web.AbstractAction');
var Dialog = require('web.Dialog');
var field_utils = require('web.field_utils');
var session = require('web.session');

var QWeb = core.qweb;
var _t = core._t;

const defaultPagerSize = 20;

var ClientAction = AbstractAction.extend({
    contentTemplate: 'equip3_mrp_mps_replenish_wizard',

    events: {
        'change .o_mrp_mps_input_scheduled_date': '_onChangeCell',
        'change .o_mrp_mps_input_to_replenish': '_onChangeCell',
        'click .o_mrp_mps_produce': '_onClickProduce',
        'click .o_mrp_mps_reset': '_onClickReset'
    },

    init: function (parent, action) {
        this._super.apply(this, arguments);
        this.actionManager = parent;
        this.action = action;
        this.context = action.context;
        this.domain = [];

        this.companyId = false;
        this.date_range = [];
        this.formatFloat = field_utils.format.float;
        this.manufacturingPeriod = false;
        this.manufacturingPeriods = [];
        this.state = false;
        this.warehouseId = false;
        this.warehouseIds = [];

        this.active_ids = [];
        this.pager = false;
        this.recordsPager = false;
        this.mutex = new concurrency.Mutex();

        this.searchModelConfig.modelName = 'equip.mrp.production.schedule';
    },

    willStart: function () {
        var self = this;
        var _super = this._super.bind(this);
        var args = arguments;

        var def_control_panel = this._rpc({
            model: 'ir.model.data',
            method: 'get_object_reference',
            args: ['equip3_manuf_reports', 'equip3_mrp_mps_search_view'],
            kwargs: {context: session.user_context},
        })
        .then(function (viewId) {
            self.viewId = viewId[1];
        });

        var def_content = this._getRecordIds();

        return Promise.all([def_content, def_control_panel]).then(function () {
            return self._getState().then(function () {
                return _super.apply(self, args);
            });
        });
    },

    start: async function () {
        await this._super(...arguments);
        if (this.state.length == 0) {
            this.$el.find('.o_mrp_mps_wizard').append($(QWeb.render('equip3_mrp_mps_nocontent_helper')));
        }
    },

    //--------------------------------------------------------------------------
    // Private
    //--------------------------------------------------------------------------

    _getRecordIds: function () {
        var self = this;
        return this._rpc({
            model: 'equip.mrp.production.schedule',
            method: 'search_read',
            domain: this.domain,
            fields: ['id'],
        }).then(function (ids) {
            self.recordsPager = ids;
            self.active_ids = ids.slice(0, defaultPagerSize).map(i => i.id);
        });
    },

    /**
     * Make an rpc to get the state and afterwards set the company, the
     * manufacturing period, the groups in order to display/hide the differents
     * rows and the state that contains all the informations
     * about production schedules and their forecast for each period.
     *
     * @private
     * @return {Promise}
     */
    _getState: function () {
        var self = this;
        var domain = this.domain.concat([['id', 'in', this.active_ids]]);
        return this._rpc({
            model: 'equip.mrp.production.schedule',
            method: 'get_mps_view_state',
            args: [domain],
        }).then(function (state) {
            self.companyId = state.company_id;
            self.manufacturingPeriods = state.dates;
            self.state = state.production_schedule_ids;
            self.manufacturingPeriod = state.manufacturing_period;
//            self.groups = state.groups[0];
            return state;
        });
    },

    _getProductionScheduleState: function (productionScheduleId) {
        var self = this;
        return self._rpc({
            model: 'equip.mrp.production.schedule',
            method: 'get_impacted_schedule',
            args: [productionScheduleId, self.domain],
        }).then(function (productionScheduleIds) {
            productionScheduleIds.push(productionScheduleId);
            return self._rpc({
                model: 'equip.mrp.production.schedule',
                method: 'get_production_schedule_view_state',
                args: [productionScheduleIds],
            }).then(function (states) {
                for (var i = 0; i < states.length; i++) {
                    var state = states[i];
                    var index =  _.findIndex(self.state, {id: state.id});
                    if (index >= 0) {
                        self.state[index] = state;
                    }
                    else {
                        self.state.push(state);
                    }
                }
                return states;
            });
        });
    },

    /**
     * reload all the production schedules inside content. Make an rpc to the
     * server in order to get the updated state and render it.
     *
     * @private
     * @return {Promise}
     */
    _reloadContent: function () {
        var self = this;
        return this._getState().then(function () {
            var $content = $(QWeb.render('equip3_mrp_mps_replenish_wizard', {
                widget: {
                    manufacturingPeriods: self.manufacturingPeriods,
                    state: self.state,
                    formatFloat: self.formatFloat,
                    warehouseIds: self.warehouseIds,
                    warehouseId: self.warehouseId
                }
            }));
            $('.o_mrp_mps_wizard').replaceWith($content);
        });
    },

    /**
     * Get the state with an rpc and render it with qweb. If the production
     * schedule is already present in the view replace it. Else append it at the
     * end of the table.
     *
     * @private
     * @param {Array} [productionScheduleIds] equip.mrp.production.schedule ids to render
     * @return {Promise}
     */
    _renderProductionSchedule: function (productionScheduleId) {
        var self = this;
        return this._getProductionScheduleState(productionScheduleId).then(function (states) {
            return self._renderState(states);
        });
    },

    _renderState: function (states) {
        for (var i = 0; i < states.length; i++) {
            var state = states[i];

            var $table = $(QWeb.render('equip3_mrp_mps_production_replenish', {
                manufacturingPeriods: this.manufacturingPeriods,
                state: [state],
                formatFloat: this.formatFloat,
            }));
            var $tbody = $('.o_mps_content_replenish[data-id='+ state.id +']');
            if ($tbody.length) {
                $tbody.replaceWith($table);
            } else {
                var $warehouse = false;
                if ('warehouse_id' in state) {
                    $warehouse = $('.o_mps_content_replenish[data-warehouse_id='+ state.warehouse_id[0] +']');
                }
                if ($warehouse.length) {
                    $warehouse.last().append($table);
                } else {
                    $('.o_mps_product_table_replenish').append($table);
                }
            }
        }
        return Promise.resolve();
    },

    /**
     * Save the company settings and hide or display the rows. It will not
     * reload the whole page but just add/remove the o_hidden class.
     *
     * @private
     * @param {Object} [values] {field_name: field_value}
     * @return {Promise}
     */
    _saveCompanySettings: function (values) {
        var self = this;
        this.mutex.exec(function () {
            return self._rpc({
                model: 'res.company',
                method: 'write',
                args: [[self.companyId], values],
            }).then(function () {
                self._reloadContent();
            });
        });
    },

    //--------------------------------------------------------------------------
    // Handlers
    //--------------------------------------------------------------------------

    _actionProduce: function (ids, replenishOption, bomIds, now) {
        var self = this;
        this.mutex.exec(function () {
            return self._rpc({
                model: 'equip.mrp.production.schedule',
                method: 'action_replenish',
                args: [ids, replenishOption, bomIds],
            }).then(function() {
                return self.do_action({type: 'ir.actions.act_window_close'});
            });
        });
    },

    _onClickProduce: function (ev) {
        ev.stopPropagation();
        var $manufOrder = this.$el.find('#manufacturing_order');
        var replenishOption = 'MP';
        if ($manufOrder[0].checked){
            replenishOption = 'MO'
        }

        var $selectedBom = this.$el.find('select.o_mps_select_inline');

        var bomIds = {}
        $selectedBom.each(function() {
            bomIds[$(this).data('mps_id')] = $(this).val();
        });

        var $datetimeNow = this.$el.find('span.o_scheduled_now');

        var ids = this.active_ids;
        this._actionProduce(ids, replenishOption, bomIds);
    },

    _writeForecast: function (forecastId, fieldName, fieldEdited, fieldValue) {
        var self = this;
        function doIt() {
            self.mutex.exec(function () {
                return self._rpc({
                    model: 'equip.mrp.product.forecast',
                    method: 'write',
                    args: [[forecastId], {
                        [fieldName]: fieldValue,
                        [fieldEdited]: false
                    }],
                }).then(function () {
                    self._reloadContent();
                });
            });
        }
        Dialog.confirm(this, _t("Are you sure you want to reset this record ?"), {
            confirm_callback: doIt,
        });
    },

    _onClickReset: function (ev) {
        ev.preventDefault();
        var $target = $(ev.target);
        var resetField = $target.data('reset');
        var forecastId = $target.data('forecast_id');
        var isEdited = $target.data(resetField + '_edited');

        if (forecastId && isEdited){
            var fieldName = resetField;
            var fieldValue = false;
            var fieldEdited = resetField + '_edited';
            if (resetField === 'forecasted_demand' || resetField == 'to_replenish'){
                fieldName = fieldName + '_qty';
                fieldValue = 0.0;
            }
            this._writeForecast(forecastId, fieldName, fieldEdited, fieldValue);
        }
    },

    /**
     * Handles the change on a cell.
     *
     * @private
     * @param {jQuery.Event} ev
     */
    _onChangeCell: function (ev) {
        ev.stopPropagation();
        var $target = $(ev.target);
        var dateIndex = $target.data('date_index');
        var field = $target.data('field');
        var productionScheduleId = $target.closest('.o_mps_content_replenish').data('id');

        var newValue = $target.val();
        var isValid;
        if (field === 'forecasted_demand' || field === 'to_replenish'){
            newValue = parseFloat(newValue);
            isValid = !isNaN(newValue);
        } else {
            isValid = Date.parse(newValue);
            newValue = newValue.replace('T', ' ');
        }

        if (!isValid){
            this._backToState(productionScheduleId);
        } else {
            this._saveQuantity(productionScheduleId, dateIndex, newValue, field);
        }
    },

    _backToState: function (productionScheduleId) {
        var state = _.where(_.flatten(_.values(this.state)), {id: productionScheduleId});
        return this._renderState(state);
    },

    /**
     * Save the forecasted quantity and reload the current schedule in order
     * to update its To Replenish quantity and its safety stock (current and
     * future period). Also update the other schedules linked by BoM in order
     * to update them depending the indirect demand.
     *
     * @private
     * @param {Object} [productionScheduleId] equip.mrp.production.schedule Id.
     * @param {Integer} [dateIndex] period to save (column number)
     * @param {Float} [forecastQty] The new forecasted quantity
     * @return {Promise}
     */
    _saveQuantity: function (productionScheduleId, dateIndex, newValue, field) {
        console.log(productionScheduleId +' '+ dateIndex + ' ' + newValue +' '+ field)
        var self = this;
        this.mutex.exec(function () {
            return self._rpc({
                model: 'equip.mrp.production.schedule',
                method: 'save_changes',
                args: [productionScheduleId, dateIndex, newValue, field],
            }).then(function () {
                return self._renderProductionSchedule(productionScheduleId).then(function () {
                    return self._focusNextInput(productionScheduleId, dateIndex, 'demand_forecast');
                });
            });
        });
    },

    _focusNextInput: function (productionScheduleId, dateIndex, inputName) {
        var tableSelector = '.o_mps_content_replenish[data-id=' + productionScheduleId + ']';
        var rowSelector = 'tr[name=' + inputName + ']';
        var inputSelector = 'input[data-date_index=' + (dateIndex + 1) + ']';
        return $([tableSelector, rowSelector, inputSelector].join(' ')).select();
    },
});

core.action_registry.add('equip3_mrp_mps_action_replenish', ClientAction);

return ClientAction;

});
