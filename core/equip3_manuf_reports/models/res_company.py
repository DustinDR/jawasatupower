from odoo import models, fields, _
from odoo.tools.date_utils import start_of, end_of, add, subtract
from odoo.tools.misc import format_date


class ResCompany(models.Model):
	_inherit = 'res.company'

	def _get_date_range(self):
		""" Return the date range for a production schedude depending the
        manufacturing period and the number of columns to display specify by the
        user. It returns a list of tuple that contains the timestamp for each
        column.
        """
		self.ensure_one()
		date_range = []
		first_day = start_of(fields.Date.today(), self.manufacturing_period)
		for columns in range(self.manufacturing_period_to_display):
			last_day = end_of(first_day, self.manufacturing_period)
			date_range.append((first_day, last_day))
			first_day = add(last_day, days=1)
		return date_range

	def _date_range_to_str(self):
		date_range = self._get_date_range()
		dates_as_str = []
		lang = self.env.context.get('lang')
		for date_start, date_stop in date_range:
			if self.manufacturing_period == 'month':
				dates_as_str.append(format_date(self.env, date_start, date_format='MMM yyyy'))
			elif self.manufacturing_period == 'week':
				dates_as_str.append(_('Week %s', format_date(self.env, date_start, date_format='w')))
			else:
				dates_as_str.append(format_date(self.env, date_start, date_format='MMM d'))
		return dates_as_str

	def write(self, vals):
		res = super(ResCompany, self).write(vals)
		for company in self:
			if company.id == self.env.company.id and not company.manufacturing:
				manufacturing_cost_analysis_menu = self.env.ref("equip3_manuf_reports.menu_mrp_cost_analysis")
				if manufacturing_cost_analysis_menu:
					manufacturing_cost_analysis_menu.active = False
			else:
				manufacturing_cost_analysis_menu = self.env.ref("equip3_manuf_reports.menu_mrp_cost_analysis")
				if manufacturing_cost_analysis_menu:
					manufacturing_cost_analysis_menu.active = True

			if company.id == self.env.company.id:
				mrp_mps_menu = self.env.ref('equip3_manuf_reports.mrp_mps_menu_planning')
				mrp_mps_menu.active = company.manufacturing_mps

		return res
