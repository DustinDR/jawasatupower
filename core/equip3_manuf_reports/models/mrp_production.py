from odoo import models, fields, api


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    @api.depends('location_dest_id')
    def _compute_warehouse_id(self):
        for record in self:
            record.warehouse_id = False
            warehouse_id = record.location_dest_id.get_warehouse()
            if warehouse_id:
                record.warehouse_id = warehouse_id.id

    mps_production_id = fields.Many2one('equip.mps.production', string='MPS Production')
    mps_start_date = fields.Date(string='MPS Start Date')
    mps_end_date = fields.Date(string='MPS End Date')
    warehouse_id = fields.Many2one('stock.warehouse', compute=_compute_warehouse_id, store=True)
