from odoo import models, fields, api


class MrpConsumption(models.Model):
    _inherit = 'mrp.consumption'

    @api.depends('location_dest_id')
    def _compute_warehouse_id(self):
        for record in self:
            record.warehouse_id = False
            location_dest_id = record.location_dest_id
            if location_dest_id:
                record.warehouse_id = location_dest_id.get_warehouse().id

    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', compute=_compute_warehouse_id, store=True)
