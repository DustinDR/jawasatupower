from odoo import models, fields, api


class MrpWorkorder(models.Model):
    _inherit = 'mrp.workorder'

    @api.depends('location_id')
    def _compute_warehouse_id(self):
        for record in self:
            record.warehouse_id = False
            location_id = record.location_id
            if location_id:
                record.warehouse_id = location_id.get_warehouse().id

    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', compute=_compute_warehouse_id, store=True)