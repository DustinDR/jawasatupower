# -*- coding: utf-8 -*-
import datetime
import pytz
from math import log10

from odoo import api, fields, models
from odoo.tools import float_round
from odoo.osv.expression import AND
from datetime import timedelta
from calendar import monthrange


def format_dt(dt, tz):
    new_dt = dt.replace(microsecond=0).astimezone(pytz.timezone(tz))
    return 'T'.join(str(new_dt.replace(tzinfo=None)).split(' '))


def to_utc(dt_string, tz):
    tz_obj = pytz.timezone(tz)
    new_dt = tz_obj.localize(fields.Datetime.from_string(dt_string))
    new_dt = new_dt.astimezone(pytz.utc)
    return new_dt.replace(tzinfo=None)


class MrpProductionSchedule(models.Model):
    _name = 'equip.mrp.production.schedule'
    _order = 'warehouse_id, sequence'
    _description = 'Schedule the production of Product in a warehouse'

    @api.model
    def _default_warehouse_id(self):
        return self.env['stock.warehouse'].search([('company_id', '=', self.env.company.id)], limit=1)

    forecast_ids = fields.One2many('equip.mrp.product.forecast', 'production_schedule_id', 'Forecasted quantity at date')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.company)
    product_id = fields.Many2one('product.product', string='Product', required=True, domain="[('bom_ids', '!=', False)]")
    product_tmpl_id = fields.Many2one('product.template', related="product_id.product_tmpl_id", readonly=True)
    product_uom_id = fields.Many2one('uom.uom', string='Product UoM', related='product_id.uom_id')
    sequence = fields.Integer(related='product_id.sequence', store=True)
    warehouse_id = fields.Many2one('stock.warehouse', 'Production Warehouse', required=True, default=_default_warehouse_id)

    _sql_constraints = [
        ('warehouse_product_ref_uniq', 'unique (warehouse_id, product_id)', 'The combination of warehouse and product must be unique !'),
    ]

    def action_replenish(self, replenish_option, bom_ids):
        company_id = self.env.company
        date_range = company_id._get_date_range()

        current_datetime = fields.Datetime.now()
        mps_values = self.get_production_schedule_view_state()

        plan = self.env['mrp.plan']
        order = self.env['mrp.production']

        mps_plan_ids = self.env['mrp.plan']
        mps_order_ids = self.env['mrp.production']

        for mps in mps_values:
            mps_id = str(mps['id'])
            if not bom_ids[mps_id]:
                continue

            product_id = self.env['product.product'].browse(mps['product_id'][0])
            bom_id = int(bom_ids[mps_id])

            order_ids = self.env['mrp.production']
            total_product_qty = 0.0
            ranges = []
            for (forecast, (date_start, date_stop)) in zip(mps['forecast_ids'], date_range):
                product_qty = forecast['to_replenish_qty']

                if product_qty <= 0.0:
                    continue

                scheduled_date = forecast['scheduled_date'].replace('T', ' ')
                date_planned_start = to_utc(scheduled_date, self.env.user.tz)
                date_planned_finished = date_planned_start + timedelta(hours=1)

                order_values = {
                    'product_id': product_id.id,
                    'product_qty': product_qty,
                    'bom_id': bom_id,
                    'user_id': self.env.user.id,
                    'product_uom_id': product_id.uom_id.id,
                    'company_id': self.env.company.id,
                    'date_planned_start': date_planned_start,
                    'date_planned_finished': date_planned_finished,
                    'mps_start_date': date_start,
                    'mps_end_date': date_stop
                }

                total_product_qty += product_qty
                ranges.append((date_start, date_stop))

                order_id = order.create(order_values)
                order_id.sudo().onchange_product_id()
                order_id.sudo().onchange_branch()
                order_id.sudo()._onchange_workorder_ids()
                order_id.sudo()._onchange_move_raw()
                order_id.sudo()._onchange_move_finished()
                order_id.sudo().onchange_workorder_ids()
                order_ids |= order_id

            if not order_ids:
                continue

            mps_order_ids |= order_ids

            if replenish_option == 'MP':
                plan_name = format_dt(current_datetime, self.env.user.tz).replace('T', ' ')
                plan_id = plan.create({
                    'name': 'Replenish %s' % plan_name,
                    'branch': self.env.user.branch_id.id,
                    'mps_product_id': product_id.id,
                    'mps_product_qty': total_product_qty,
                    'mps_bom_id': bom_id,
                    'mps_start_date': ranges[0][0],
                    'mps_end_date': ranges[-1][1]
                })
                plan_id.mrp_order_ids = [(6, 0, order_ids.ids)]
                plan_id.generate_mrp_order_ids()
                mps_plan_ids |= plan_id

        self.env['equip.mps.production'].create({
            'date': current_datetime,
            'plan_ids': [(6, 0, mps_plan_ids.ids)],
            'production_ids': [(6, 0, mps_order_ids.ids)],
        })

    @api.model
    def get_mps_view_state(self, domain=False):
        productions_schedules = self.env['equip.mrp.production.schedule'].search(domain or [])
        productions_schedules_states = productions_schedules.get_production_schedule_view_state()
        values = {
            'dates': self.env.company._date_range_to_str(),
            'production_schedule_ids': productions_schedules_states,
            'manufacturing_period': self.env.company.manufacturing_period,
            'company_id': self.env.company.id,
            'warehouse_ids': [{'id': w.id, 'name': w.name} for w in self.env['stock.warehouse'].search([])],
            'warehouse_id': self.search([], limit=1).warehouse_id.id
        }
        return values

    def get_production_schedule_view_state(self):
        company_id = self.env.company
        date_range = company_id._get_date_range()
        advance_stock_days = 1
        if company_id.manufacturing_period == 'week':
            advance_stock_days = 7
        elif company_id.manufacturing_period == 'month':
            today = fields.Date.today()
            advance_stock_days = monthrange(year=today.year, month=today.month)[1]

        read_fields = ['product_id']
        if self.env.user.has_group('stock.group_stock_multi_warehouses'):
            read_fields.append('warehouse_id')
        if self.env.user.has_group('uom.group_uom'):
            read_fields.append('product_uom_id')

        production_schedule_states = self.search([]).read(read_fields)
        production_schedule_states_by_id = {mps['id']: mps for mps in production_schedule_states}

        for production_schedule in self.search([]):

            production_schedule_state = production_schedule_states_by_id[production_schedule['id']]

            bom_ids = self.env['mrp.bom'].search([
                '&',
                '|',
                ('company_id', '=', False),
                ('company_id', '=', company_id.id),
                '&',
                '|',
                ('product_id', '=', production_schedule.product_id.id),
                '&',
                ('product_tmpl_id.product_variant_ids', '=', production_schedule.product_id.id),
                ('product_id', '=', False),
                ('type', '=', 'normal')
            ])

            production_schedule_state['bom_ids'] = []
            for bom in bom_ids:
                production_schedule_state['bom_ids'].append((bom.id, '[%s] %s' % (bom.code and bom.code or '', bom.product_tmpl_id.name)))

            precision_digits = max(0, int(-(log10(production_schedule.product_uom_id.rounding))))
            production_schedule_state['precision_digits'] = precision_digits
            production_schedule_state['forecast_ids'] = []
            production_schedule_state['len_range'] = len(date_range)

            next_on_hand = False
            previous_to_replenish_qty = False
            for date_start, date_stop in date_range:
                forecast_at_date = production_schedule.forecast_ids.filtered(lambda f: f.date == date_stop)

                forecasted_demand_edited = False
                to_replenish_edited = False
                scheduled_date_edited = False
                forecast_at_date_id = False
                if forecast_at_date:
                    forecasted_demand_edited = forecast_at_date.forecasted_demand_edited
                    to_replenish_edited = forecast_at_date.to_replenish_edited
                    scheduled_date_edited = forecast_at_date.scheduled_date_edited
                    forecast_at_date_id = forecast_at_date.id

                forecast_values = {}

                product_id = production_schedule.product_id.with_context(
                    warehouse=production_schedule.warehouse_id.id,
                    location=False
                )

                if next_on_hand is False:
                    res = product_id._compute_quantities_dict(False, False, False, to_date=date_stop)
                    on_hand_qty = res[product_id.id]['qty_available']
                else:
                    on_hand_qty = next_on_hand

                if previous_to_replenish_qty is not False:
                    on_hand_qty += previous_to_replenish_qty

                stock_order_point = self.env['stock.warehouse.orderpoint'].search([
                    ('product_id', '=', production_schedule.product_id.id),
                ])

                run_rate_days = 1
                if stock_order_point:
                    run_rate_days = stock_order_point[0].run_rate_days or 1

                low_stock = sum(stock_order_point.mapped('product_min_qty'))
                max_stock = sum(stock_order_point.mapped('product_max_qty'))

                domain = [
                    ('product_id', '=', production_schedule.product_id.id),
                    ('order_id.sale_state', '=', 'in_progress'),
                    ('multiple_do_date', '>=', date_start),
                    ('multiple_do_date', '<=', date_stop),
                    ('location_id', '=', production_schedule.warehouse_id.lot_stock_id.id)
                ]

                sales = sum(self.env['sale.order.line'].search(domain).mapped('product_uom_qty'))

                if forecasted_demand_edited:
                    forecast_demand = forecast_at_date.forecasted_demand_qty
                else:
                    forecast_demand = (low_stock / run_rate_days) * advance_stock_days

                forecast_stock = on_hand_qty - forecast_demand

                if to_replenish_edited:
                    to_replenish_qty = forecast_at_date.to_replenish_qty
                else:
                    to_replenish_qty = 0.0
                    if forecast_stock < low_stock:
                        to_replenish_qty = max_stock - forecast_stock

                previous_to_replenish_qty = to_replenish_qty

                scheduled_date = str(datetime.datetime.combine(date_start, datetime.datetime.min.time())).replace(' ', 'T')
                if scheduled_date_edited:
                    scheduled_date = format_dt(forecast_at_date.scheduled_date, self.env.user.tz)

                forecast_values['on_hand_qty'] = on_hand_qty
                forecast_values['low_stock'] = low_stock
                forecast_values['max_stock'] = max_stock
                forecast_values['sales'] = sales
                forecast_values['forecasted_demand'] = forecast_demand
                forecast_values['forecasted_stock'] = forecast_stock
                forecast_values['to_replenish_qty'] = to_replenish_qty
                forecast_values['forecast_at_date_id'] = forecast_at_date_id
                forecast_values['run_rate_days'] = run_rate_days
                forecast_values['advanced_stock_days'] = advance_stock_days
                forecast_values['scheduled_date'] = scheduled_date

                forecast_values['forecasted_demand_edited'] = forecasted_demand_edited
                forecast_values['to_replenish_edited'] = to_replenish_edited
                forecast_values['scheduled_date_edited'] = scheduled_date_edited

                production_schedule_state['forecast_ids'].append(forecast_values)
                next_on_hand = forecast_values['forecasted_stock']

        return production_schedule_states

    def get_impacted_schedule(self, domain=False):
        """ When the user modify the demand forecast on a schedule. The new
        replenish quantity is computed from schedules that use the product in
        self as component (no matter at which BoM level). It will also modify
        the replenish quantity on self that will impact the schedule that use
        the product in self as a finished product.

        :param domain: filter supplied and supplying schedules with the domain
        :return ids of supplied and supplying schedules
        :rtype list
        """
        if not domain:
            domain = []

        def _used_in_bom(products, related_products):
            """ Bottom up from bom line to finished products in order to get
            all the finished products that use 'products' as component.
            """
            if not products:
                return related_products
            boms = products.bom_line_ids.mapped('bom_id')
            products = boms.mapped('product_id') | boms.mapped('product_tmpl_id.product_variant_ids')
            products -= related_products
            related_products |= products
            return _used_in_bom(products, related_products)

        supplying_mps = self.env['equip.mrp.production.schedule'].search(
            AND([domain, [
                ('warehouse_id', 'in', self.mapped('warehouse_id').ids),
                ('product_id', 'in', _used_in_bom(self.mapped('product_id'), self.env['product.product']).ids)
            ]]))

        def _use_boms(products, related_products):
            """ Explore bom line from products's BoMs in order to get components
            used.
            """
            if not products:
                return related_products
            boms = products.bom_ids | products.mapped('product_variant_ids.bom_ids')
            products = boms.mapped('bom_line_ids.product_id')
            products -= related_products
            related_products |= products
            return _use_boms(products, related_products)

        supplied_mps = self.env['equip.mrp.production.schedule'].search(
            AND([domain, [
                ('warehouse_id', 'in', self.mapped('warehouse_id').ids),
                ('product_id', 'in', _use_boms(self.mapped('product_id'), self.env['product.product']).ids)
            ]]))
        return (supplying_mps | supplied_mps).ids

    def set_forecasted_demand_qty(self, date_index, quantity):
        """ Save the to replenish quantity:

        params quantity: The new total to replenish quantity
        params date_index: The manufacturing period
        """
        # Get the last date of current period
        self.ensure_one()
        date_start, date_stop = self.company_id._get_date_range()[date_index]
        existing_forecast = self.forecast_ids.filtered(
            lambda f: f.date == date_stop
        )
        quantity = float_round(float(quantity), precision_rounding=self.product_uom_id.rounding)
        quantity_to_add = quantity - sum(existing_forecast.mapped('forecasted_demand_qty'))
        if existing_forecast:
            new_qty = existing_forecast[0].forecasted_demand_qty + quantity_to_add
            new_qty = float_round(new_qty, precision_rounding=self.product_uom_id.rounding)
            existing_forecast[0].write({
                'forecasted_demand_qty': new_qty,
                'forecasted_demand_edited': True
            })
        else:
            existing_forecast.create({
                'forecasted_demand_qty': quantity,
                'forecasted_demand_edited': True,
                'date': date_stop,
                'production_schedule_id': self.id
            })
        return True

    def set_to_replenish_qty(self, date_index, quantity):
        """ Save the to replenish quantity:

        params quantity: The new total to replenish quantity
        params date_index: The manufacturing period
        """
        # Get the last date of current period
        self.ensure_one()
        date_start, date_stop = self.company_id._get_date_range()[date_index]
        existing_forecast = self.forecast_ids.filtered(
            lambda f: f.date == date_stop
        )
        quantity = float_round(float(quantity), precision_rounding=self.product_uom_id.rounding)
        quantity_to_add = quantity - sum(existing_forecast.mapped('to_replenish_qty'))
        if existing_forecast:
            new_qty = existing_forecast[0].to_replenish_qty + quantity_to_add
            new_qty = float_round(new_qty, precision_rounding=self.product_uom_id.rounding)
            existing_forecast[0].write({
                'to_replenish_qty': new_qty,
                'to_replenish_edited': True
            })
        else:
            existing_forecast.create({
                'to_replenish_qty': quantity,
                'to_replenish_edited': True,
                'date': date_stop,
                'production_schedule_id': self.id
            })
        return True

    def save_changes(self, date_index, new_value, field):
        field_edited = field + '_edited'
        if field in ('forecasted_demand', 'to_replenish'):
            field_name = field + '_qty'
        else:
            new_value = to_utc(new_value, self.env.user.tz)
            field_name = field

        # Get the last date of current period
        self.ensure_one()
        date_start, date_stop = self.company_id._get_date_range()[date_index]
        existing_forecast = self.forecast_ids.filtered(
            lambda f: f.date == date_stop
        )

        if existing_forecast:
            existing_forecast[0].write({
                field_name: new_value,
                field_edited: True
            })
        else:
            existing_forecast.create({
                field_name: new_value,
                field_edited: True,
                'date': date_stop,
                'production_schedule_id': self.id
            })
        return True


class MrpProductForecast(models.Model):
    _name = 'equip.mrp.product.forecast'
    _order = 'date'
    _description = 'Product Forecast at Date'

    production_schedule_id = fields.Many2one('equip.mrp.production.schedule', required=True, ondelete='cascade')
    date = fields.Date('Date', required=True)

    forecasted_demand_qty = fields.Float('Forecasted Demand')
    to_replenish_qty = fields.Float('To Replenish')
    scheduled_date = fields.Datetime('Scheduled Date')

    forecasted_demand_edited = fields.Boolean('Forecasted Demand Edited Manually')
    to_replenish_edited = fields.Boolean('To Replenish Edited Manually')
    scheduled_date_edited = fields.Boolean('Scheduled Date Edited Manually')

