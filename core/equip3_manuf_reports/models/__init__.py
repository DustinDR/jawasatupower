# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import mrp_consumption
from . import mrp_mps
from . import purchase_order
from . import res_company
from . import stock_rule
from . import mrp_cost_analysis
from . import material_usage_report
from . import ks_dashboard_ninja_board
from . import mps_production
from . import mrp_plan
from . import mrp_production
from . import mrp_workorder
from . import mrp_workcenter_productivity
from . import stock_quant
