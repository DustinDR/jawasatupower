# -*- coding: utf-8 -*-

{
    'name': 'Equip 3 - Manufacturing Reports',
    'version': '1.1.21',
    'category': 'Manufacturing',
    'summary': 'Manufacturing Reports',
    'description': '''
    i. Manufacturing Plan Report
    ii. Manufacturing Order Report
    iii. Work Order Report
    iv. Production Record Report
    v. Dashboard
    vi. Gantt Chart Report for Manufacturing Plan/ Manufacturing Order/ Work Order
    ''',
    'author': 'HashMicro',
    'website': 'www.hashmicro.com',
    'depends': [
        "web",
        "mrp",
        "stock",
        "purchase",
        "equip3_manuf_masterdata",
        'stock_account',
        'equip3_manuf_operations_contd',
        'equip3_manuf_other_operations',
        'ks_gantt_view_base',
        'ks_gantt_view_mrp',
        'ks_dashboard_ninja',
        'ks_dn_advance'
    ],
    'data': [
        'data/ir_sequence_data.xml',
        'data/ks_dashboard_data.xml',
        'security/ir.model.access.csv',
        'templates/templates.xml',
        'views/mrp_report.xml',
        'views/material_usage_report_view.xml',
        'views/work_center_gantt_chart.xml',
        'views/mrp_production_gantt_views.xml',
        'views/mrp_mps_views.xml',
        'views/mrp_mps_menu_views.xml',
        'views/mps_production_views.xml',
        'views/mrp_plan_views.xml',
        'views/mrp_production_views.xml',
        'views/mrp_workcenter_views.xml',
        'views/stock_quant_view.xml',
        'reports/report_mrp_production.xml',
        # 'reports/report_mrp_plan.xml',
        # 'reports/report_work_order.xml',
        'reports/mrp_cost_analysis.xml',
    ],
    'qweb': [
        "static/src/xml/custom_template.xml",
        "static/src/xml/mrp_mps.xml",
        "static/src/xml/mrp_mps_replenish.xml",
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
