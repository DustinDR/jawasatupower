14.0.1
-----------
Initial Release.

14.0.2 (Date : 5th Feb 2021)
---------------------------
[FIX] fix table class.

14.0.3 (Date : 19th Feb 2021)
------------------------------
[FIX] fix issue in day wise sale report.

14.0.4 (Date : 28th May 2021)
-------------------------------
[UPDATE] update some payment calculation of group payments.

14.0.5 (Date : 28th June 2021)
-------------------------------
[ADDED] sh_customer_sales_analysis,sh_product_sales_indent,sh_sale_by_category,sh_sale_invoice_summary,sh_sale_product_profit  Added.

14.0.6 (Date : 16th July 2021)
-------------------------------
Bug Fixed

14.0.7 (Date : 31st July 2021)
---------------------------------
[ADD] datetime field is added in all reports and filter data with date and time.

14.0.8 (Date : 10th August 2021)
-----------------------------------
[FIX] small bug fixed.

14.0.9 (Date : 20th August 2021)
-------------------------------
[UPDATE] update journal multi company wise and filtered and get data with that in print.

14.0.10 (Date : 24th August 2021)
------------------------------------
[FIX] small bug fixed.
