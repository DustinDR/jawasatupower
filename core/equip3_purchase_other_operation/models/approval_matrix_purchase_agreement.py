
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning

class ApprovalMatrixPurchaseAgreement(models.Model):
	_name = "purchase.agreement.approval.matrix"
	_description = "Approval Matrix Purchase Agreement"
	_inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
	
	name = fields.Char(string="Name", required=True, tracking=True)
	company_id = fields.Many2one('res.company', string="Company", required=True, tracking=True, readonly=True, default=lambda self: self.env.company.id)
	branch_id = fields.Many2one('res.branch', string="Branch", domain="[('company_id', '=', company_id)]", tracking=True)
	minimum_amt = fields.Float(string="Minimum Amount", tracking=True)
	maximum_amt = fields.Float(string="Maximum Amount", tracking=True)
	approval_matrix_purchase_agreement_line_ids = fields.One2many('approval.matrix.purchase.agreement.line', 'approval_matrix_purchase_agreement', string='Approving Matrix Purchase Agreement', tracking=True)
	
	@api.constrains('branch_id')
	def _check_existing_record(self):
		for record in self:
			if record.branch_id:
				approval_matrix_id = self.search([('branch_id', '=', record.branch_id.id), ('id', '!=', record.id)], limit=1)
				# approval_matrix_id = self.search([('branch_id', '=', record.branch_id.id), ('id', '!=', record.id),
				# 	'|', '|',
				# 	'&', ('minimum_amt', '<=', record.minimum_amt), ('maximum_amt', '>=', record.minimum_amt),
				# 	'&', ('minimum_amt', '<=', record.maximum_amt), ('maximum_amt', '>=', record.maximum_amt),
				# 	'&', ('minimum_amt', '>=', record.minimum_amt), ('maximum_amt', '<=', record.maximum_amt)], limit=1)
				if approval_matrix_id:
					raise ValidationError("The minimum and maximum range of this approval matrix is intersects with other approval matrix %s in same branch. Please change the minimum and maximum range" % (approval_matrix_id.name))
	
	def _reset_sequence(self):
		for rec in self:
			current_sequence = 1
			for line in rec.approval_matrix_purchase_agreement_line_ids:
				line.sequence = current_sequence
				current_sequence += 1

class ApprovalMatrixPurchaseAgreementLine(models.Model):
	_name = "approval.matrix.purchase.agreement.line"
	_description = "Approval Matrix Purchase Agreement Line"
	
	@api.model
	def default_get(self, fields):
		res = super(ApprovalMatrixPurchaseAgreementLine, self).default_get(fields)
		if self._context:
			context_keys = self._context.keys()
			next_sequence = 1
			if 'approval_matrix_purchase_agreement_line_ids' in context_keys:
				if len(self._context.get('approval_matrix_purchase_agreement_line_ids')) > 0:
					next_sequence = len(self._context.get('approval_matrix_purchase_agreement_line_ids')) + 1
			res.update({'sequence': next_sequence})
		return res
	
	sequence = fields.Integer(string="Sequence", tracking=True)
	user_ids = fields.Many2many('res.users', string="User", required=True, tracking=True)
	minimum_approver = fields.Integer(string="Minimum Approver", default=1, required=True, tracking=True)
	approval_matrix_purchase_agreement = fields.Many2one('purchase.agreement.approval.matrix', string="Approval Matrix")
	
	sequence2 = fields.Integer(
		string="No.",
		related="sequence",
		readonly=True,
		store=True,
	)
	
	def unlink(self):
		approval = self.approval_matrix_purchase_order
		res = super(ApprovalMatrixPurchaseAgreementLine, self).unlink()
		approval._reset_sequence()
		return res
	
	@api.model
	def create(self, vals):
		res = super(ApprovalMatrixPurchaseAgreementLine, self).create(vals)
		if not self.env.context.get("keep-line_sequence", False):
			res.approval_matrix_purchase_agreement._reset_sequence()
		return res