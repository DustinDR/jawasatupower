from odoo import api, fields, models, _

class WizardQuotationAgreement(models.TransientModel):
    _name = "wizard.quotation.agreement"
    
    agreement_id = fields.Many2one('purchase.agreement', string="Tender")
    partner_ids = fields.Many2many('res.partner', string='Vendors')
    
    def action_new_quotation_wizard(self):
        for res in self:
            res.agreement_id.create_new_rfq(res.partner_ids)

class ShPurchaseOrderWizard(models.TransientModel):
    _inherit = 'purchase.order.wizard'

    partner_id = fields.Many2one(required=False)
    date_planned = fields.Datetime(required=False)
    order_selection = fields.Selection(required=False)
    sh_group_by_partner = fields.Boolean("Group By")
    sh_cancel_old_rfqs = fields.Selection([('none', 'None'), ('cancel_all_old', "Cancel Old RFQ'S of Tender"), ('cancel_old_partner', "Cancel Old RFQ'S of Selected Tender of Partners")],
                                          default='none',
                                          string="Cancel Old RFQ'S"
                                          )
    sh_create_po = fields.Selection([
        ('rfq', 'RFQ'),
        ('po', 'Purchase Order')
    ], default='rfq', string="Create RFQ/Purchase Order")

    def action_create_po(self):
        context = dict(self._context or {})
        purchase_order_line = self.env['purchase.order.line'].sudo().search(
            [('id', 'in', context.get('active_ids'))])
        active_id = self.env['purchase.order.line'].sudo().search([('id', 'in', dict(self._context or {}).get('active_ids'))],limit=1)
        if self.env['ir.config_parameter'].sudo().get_param('is_good_services_order') == 'True':       
            is_goods_orders = active_id.mapped('is_goods_orders')[0]
            is_services_orders = active_id.mapped('is_services_orders')[0]
            is_assets_orders = active_id.mapped('is_assets_orders')[0]
            if is_goods_orders:
                context.update({
                    'default_is_goods_orders': is_goods_orders,
                    "goods_order": True,
                })
            elif is_services_orders:
                context.update({
                    'default_is_services_orders': is_services_orders,
                    'services_good': True,
                    })
            elif is_assets_orders:
                context.update({
                    'default_is_assets_orders': is_assets_orders,
                    'assets_orders': True,
                    })
        else:
            context.update({'default_is_goods_orders': False})
        if self.sh_cancel_old_rfqs == 'cancel_all_old':
            for line in purchase_order_line:
                purchase_orders = self.env['purchase.order'].sudo().search(
                    [('agreement_id', '=', line.agreement_id.id), ('state', 'in', ['draft'])])
                if purchase_orders:
                    for order in purchase_orders:
                        order.button_cancel()
        elif self.sh_cancel_old_rfqs == 'cancel_old_partner':
            partner_list = []
            agreement_list = []
            for order_line in purchase_order_line:
                if order_line.partner_id and order_line.partner_id not in partner_list:
                    partner_list.append(order_line.partner_id.id)
                if order_line.agreement_id and order_line.agreement_id not in agreement_list:
                    agreement_list.append(order_line.agreement_id.id)
            purchase_orders = self.env['purchase.order'].sudo().search(
                [('state', 'in', ['draft'])])
            if purchase_orders:
                for order in purchase_orders:
                    if order.partner_id.id in partner_list and order.agreement_id.id in agreement_list:
                        order.button_cancel()
        if purchase_order_line:
            if not self.sh_group_by_partner:
                order_ids = []
                for order_line in purchase_order_line:
                    vals = {
                        'partner_id': order_line.partner_id.id,
                        'agreement_id': order_line.agreement_id.id,
                        'user_id': self.env.user.id,
                        'date_planned': order_line.date_planned,
                        'picking_type_id': order_line.order_id.picking_type_id.id or False,
                        'branch_id': order_line.order_id.branch_id.id,
                        'analytic_account_group_ids': order_line.order_id.analytic_account_group_ids.ids,
                        'not_editable': order_line.order_id.not_editable,
                        'is_editable': order_line.order_id.is_editable,
                    }
                    if context.get('goods_order'):
                        vals.update({'is_goods_orders': True})
                    elif context.get('services_good'):
                        vals.update({'is_services_orders': True})
                    elif context.get('assets_orders'):
                        vals.update({'is_assets_orders': True})
                    purchase_order_id = self.env['purchase.order'].with_context(context).create(vals)
                    order_ids.append(purchase_order_id.id)
                    line_vals = {
                        'order_id': purchase_order_id.id,
                        'product_id': order_line.product_id.id,
                        'name': order_line.product_id.name,
                        'status': 'draft',
                        'product_uom': order_line.product_uom.id,
                        'product_qty': order_line.product_qty,
                        'price_unit': order_line.price_unit,
                        'picking_type_id': order_line.order_id.picking_type_id.id,
                        'destination_warehouse_id': order_line.destination_warehouse_id.id,
                        'analytic_tag_ids': order_line.analytic_tag_ids.ids,
                        'taxes_id': [(6, 0, order_line.taxes_id.ids)],
                    }
                    purchase_order_line = self.env['purchase.order.line'].sudo().create(
                        line_vals)
                    
                    if active_id.is_goods_orders:
                        purchase_order_id.is_goods_orders = active_id.is_goods_orders
                        for line in purchase_order_id.order_line:
                            line.is_goods_orders = purchase_order_id.is_goods_orders
                            
                    if self.sh_create_po == 'po':
                        purchase_order_id.selected_order = True
                        purchase_order_id.button_confirm()
                    else:
                        purchase_order_id.selected_order = False
                return {
                    'name': _("Purchase Orders/RFQ's"),
                    'type': 'ir.actions.act_window',
                    'res_model': 'purchase.order',
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'domain': [('id', 'in', order_ids)],
                    'target': 'current'
                }
            else:
                partner_list = []
                agreement_id = None
                picking_id = None
                order_ids = []
                for order_line in purchase_order_line:
                    if order_line.partner_id and order_line.partner_id not in partner_list:
                        partner_list.append(order_line.partner_id)
                    agreement_id = order_line.agreement_id
                    picking_id = order_line.order_id.picking_type_id.id,
                for partner in partner_list:
                    order_vals = {}
                    order_vals = {
                        'partner_id': partner.id,
                        'user_id': self.env.user.id,
                        'agreement_id': agreement_id.id,
                        'picking_type_id': picking_id,
                        'branch_id': agreement_id.branch_id.id,
                        'analytic_account_group_ids': agreement_id.account_tag_ids.ids,
                        'not_editable': True,
                        'is_editable': True,
                    }
                    order_id = self.env['purchase.order'].with_context(context).create(order_vals)
                    order_ids.append(order_id.id)
                    line_ids = []
                    
                    for order_line in purchase_order_line:
                        if order_line.partner_id.id == partner.id:
                            order_line_vals = {
                                'order_id': order_id.id,
                                'product_id': order_line.product_id.id,
                                'name': order_line.product_id.name,
                                'status': 'draft',
                                'product_uom': order_line.product_uom.id,
                                'product_qty': order_line.product_qty,
                                'price_unit': order_line.price_unit,
                                'destination_warehouse_id': order_line.destination_warehouse_id.id,
                                'analytic_tag_ids': order_line.analytic_tag_ids.ids,
                                'picking_type_id': order_line.order_id.picking_type_id.id,
                                'taxes_id': [(6, 0, order_line.taxes_id.ids)]
                            }
                            line_ids.append((0, 0, order_line_vals))
                    order_id.order_line = line_ids
                    
                    if active_id.is_goods_orders:
                        order_id.is_goods_orders = active_id.is_goods_orders
                        for line in order_id.order_line:
                            line.is_goods_orders = order_id.is_goods_orders
                    
                    if self.sh_create_po == 'po':
                        order_id.selected_order = True
                        order_id.button_confirm()
                    else:
                        order_id.selected_order = False
                return {
                    'name': _("Purchase Orders/RFQ's"),
                    'type': 'ir.actions.act_window',
                    'res_model': 'purchase.order',
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'domain': [('id', 'in', order_ids)],
                    'target': 'current'
                }