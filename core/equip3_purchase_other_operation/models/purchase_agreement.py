
from odoo import api, fields, models, _, tools
from datetime import timedelta, datetime, date
from odoo.http import request
from odoo.exceptions import UserError


class PurchaseAgreement(models.Model):
    _inherit = 'purchase.agreement'

    purchase_request_id = fields.Many2one('purchase.request', string='Purchase Tender')
    sh_purchase_user_id = fields.Many2one(
        'res.users', 'Purchase Representative', tracking=True, default=lambda self: self.env.user)
    sh_agreement_type = fields.Many2one(
        'purchase.agreement.type', 'Tender Type', required=False, tracking=True)
    partner_ids = fields.Many2many(
        'res.partner', string='Vendors', tracking=True, required=True)
    days_left = fields.Integer("Days Left")
    analytic_accounting = fields.Boolean("Analyic Account", compute="get_analytic_accounting", store=True)
    account_tag_ids = fields.Many2many('account.analytic.tag', 'account_analytic_tag_pt_rel', 'pt_id', 'tag_id', string="Analytic Group")
    branch_id = fields.Many2one('res.branch', domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]", string="Branch", tracking=True)
    destination_warehouse_id = fields.Many2one('stock.warehouse', string="Destination", domain="[('company_id', '=', company_id)]")
    set_single_delivery_destination = fields.Boolean("Single Delivery Destination")
    set_single_delivery_date = fields.Boolean("Single Delivery Date")
    comparison_ids = fields.One2many('purchase.agreement.comparison', 'agreement_id', string='Vendor Comparison')
    partner_id = fields.Many2one(related='user_id.partner_id')

    @api.onchange('destination_warehouse_id')
    def _onchange_destination_warehouse(self):
        for res in self:
            for line in res.sh_purchase_agreement_line_ids:
                if res.set_single_delivery_destination:
                    line.dest_warehouse_id = res.destination_warehouse_id.id
                    
    @api.onchange('set_single_delivery_destination', 'set_single_delivery_date')
    def set_single_date_destination(self):
        for res in self:
            if res.set_single_delivery_destination:
                stock_warehouse = res.env['stock.warehouse'].search([], order="id", limit=1)
                res.destination_warehouse_id = stock_warehouse[0]
            if res.set_single_delivery_date:
                res.sh_delivery_date = datetime.now().date() + timedelta(days=14)
            for line in res.sh_purchase_agreement_line_ids:
                if res.set_single_delivery_date:
                    line.schedule_date = res.sh_delivery_date

    def action_confirm(self):
        res = super(PurchaseAgreement, self).action_confirm()
        for vals in self.sh_purchase_agreement_line_ids:
            if vals.sh_qty <= 0 :
                raise UserError("You cannot confirm purchase tender without quantity.")
        return res

    @api.onchange('account_tag_ids')
    def set_analytic(self):
        for res in self:
            for line in res.sh_purchase_agreement_line_ids:
                line.analytic_tag_ids = res.account_tag_ids

    @api.depends('sh_purchase_user_id')
    def get_analytic_accounting(self):
        for res in self:
            res.analytic_accounting = self.user_has_groups('analytic.group_analytic_accounting')
    
    def action_new_quotation(self):
        res = super(PurchaseAgreement, self).action_new_quotation()
        context = dict(self.env.context) or {}
        if context.get('goods_order'):
            res['context'].update({'default_is_goods_orders' : True})
        return res
    
    @api.onchange('company_id')
    def set_expiry_date(self):
        pt_expiry_date = self.env['ir.config_parameter'].get_param('equip3_purchase_other_operation.pt_expiry_date')
        pt_goods_order_expiry_date = self.env['ir.config_parameter'].get_param('equip3_purchase_other_operation.pt_goods_order_expiry_date')
        pt_service_order_expiry_date = self.env['ir.config_parameter'].get_param('equip3_purchase_other_operation.pt_service_order_expiry_date')
        context = dict(self.env.context) or {}
        if context.get('goods_order') and pt_goods_order_expiry_date:
            for res in self:
                expiry_date = datetime.now() + timedelta(days=int(pt_goods_order_expiry_date))
                res.write({
                    'sh_agreement_deadline': expiry_date,
                })
        elif context.get('services_good') and pt_service_order_expiry_date:
            for res in self:
                expiry_date = datetime.now() + timedelta(days=int(pt_service_order_expiry_date))
                res.write({
                    'sh_agreement_deadline': expiry_date,
                })
        else:
            for res in self:
                if pt_expiry_date:
                    expiry_date = datetime.now() + timedelta(days=int(pt_expiry_date))
                    res.write({
                        'sh_agreement_deadline': expiry_date,
                    })

    def auto_cancel_pr(self):
        pr = self.env['purchase.agreement'].search(['|',('state', '=', 'draft'), ('state2', 'in', ('pending', 'bid_selection'))])
        for res in pr:
            if res.sh_agreement_deadline:
                if datetime.strftime(res.sh_agreement_deadline, tools.DEFAULT_SERVER_DATE_FORMAT) == datetime.strftime(datetime.now(), tools.DEFAULT_SERVER_DATE_FORMAT):
                    po = self.env['purchase.order'].search([('agreement_id', '=', res.id),('state1', '=', 'purchase')])
                    if not po:
                        res.update({
                            'state': "cancel"
                        })
                        

    def send_email(self):
        template_before = self.env.ref('equip3_purchase_other_operation.email_template_pt_expiry_reminder')
        template_after = self.env.ref('equip3_purchase_other_operation.email_template_pt_expiry_reminder_after')

        notif = self.env['ir.config_parameter'].get_param('equip3_purchase_other_operation.pt_expiry_notification')
        expiry = self.env['ir.config_parameter'].get_param('equip3_purchase_other_operation.pt_expiry_date')
        on_date = self.env['ir.config_parameter'].get_param('equip3_purchase_other_operation.pt_on_date_notify')
        before_exp = self.env['ir.config_parameter'].get_param('equip3_purchase_other_operation.pt_enter_before_first_notify') or 3
        after_exp = self.env['ir.config_parameter'].get_param('equip3_purchase_other_operation.pt_enter_after_first_notify') or 1
        pr = self.env['purchase.agreement'].search([('state', '!=', 'closed')])

        if notif:
            for res in pr:
                if res.sh_agreement_deadline:
                    expiry_date = datetime.strftime(res.sh_agreement_deadline, tools.DEFAULT_SERVER_DATE_FORMAT)
                    if expiry_date == datetime.strftime(datetime.now() + timedelta(days=int(before_exp)), tools.DEFAULT_SERVER_DATE_FORMAT):
                        # Before Expiry Date
                        res.days_left = int(before_exp)
                        template_before.send_mail(
                            res.id, force_send=True)
                    elif expiry_date == datetime.strftime(datetime.now() - timedelta(days=int(after_exp)), tools.DEFAULT_SERVER_DATE_FORMAT):
                        # After Expiry Date
                        template_after.send_mail(
                            res.id, force_send=True)
                    elif on_date:
                        if expiry_date == datetime.strftime(datetime.now(), tools.DEFAULT_SERVER_DATE_FORMAT):
                        # On Date
                            template_after.send_mail(
                                res.id, force_send=True)

    def get_full_url(self):
        for res in self:
            base_url = request.env['ir.config_parameter'].get_param('web.base.url')
            base_url += '/web#id=%d&view_type=form&model=%s' % (res.id, res._name)
            return base_url

    @api.onchange('partner_ids')
    def set_comparison(self):
        for res in self:
            res.comparison_ids = [(6, 0, [])]
            for vendor in res.partner_ids:
                self.env['purchase.agreement.comparison'].create({
                    'partner_id': vendor.id.origin,
                    'agreement_id': res.id
                })

    @api.model
    def default_get(self, fields):
        res = super(PurchaseAgreement, self).default_get(fields)
        analytic_priority_ids = self.env['analytic.priority'].search([], order="priority")
        for analytic_priority in analytic_priority_ids:
            if analytic_priority.object_id == 'user' and self.env.user.analytic_tag_ids:
                res.update({
                    'account_tag_ids': [(6, 0, self.env.user.analytic_tag_ids.ids)]
                })
                break
            elif analytic_priority.object_id == 'branch' and self.env.user.branch_id.analytic_tag_ids:
                res.update({
                    'account_tag_ids': [(6, 0, self.env.user.branch_id.analytic_tag_ids.ids)]
                })
                break
        return res

    def print_purchase_tender(self):
        context = dict(self.env.context) or {}
        return {
            'type': 'ir.actions.act_window',
            'name': 'Print RFQ Comparison',
            'view_mode': 'form',
            'res_model': 'print.purchase.tender.report',
            'domain' : [],
            'context': context,
            'target': 'new'
        }

    def _get_vendors(self):
        purchase_order_lines = self.env['purchase.order.line'].search([('agreement_id', '=', self.id), ('state', 'not in', ['cancel']), ('order_id.selected_order', '=', False)])
        vendor_ids = purchase_order_lines.mapped('partner_id')
        counter = 0
        vendors_data = []
        temp_vendor = []
        for vendor in vendor_ids:
            counter += 1
            temp_vendor.append(vendor)
            if counter == 5:
                counter = 0
                vendors_data.append(temp_vendor)
                temp_vendor = []
        if temp_vendor:
            vendors_data.append(temp_vendor)
        return vendors_data

    def _get_vendors_name(self):
        purchase_order_lines = self.env['purchase.order.line'].search([('agreement_id', '=', self.id), ('state', 'not in', ['cancel']), ('order_id.selected_order', '=', False)])
        vendor_ids = purchase_order_lines.mapped('partner_id')
        return ', '.join(vendor_ids.mapped('name'))

    def _get_purchase_vendor_lines(self, vendors):
        vendor_ids = [vendor_id.id for vendor_id in vendors]
        purchase_order_lines = self.env['purchase.order.line'].search([('agreement_id', '=', self.id), ('state', 'not in', ['cancel']), ('order_id.selected_order', '=', False), ('partner_id', 'in', vendor_ids)])
        temp_data = []
        final_line = []
        for line in purchase_order_lines:
            if {'product_id': line.product_id.id} in temp_data:
                filter_product_line = list(filter(lambda r:r.get('product_id') == line.product_id.id, final_line))
                if filter_product_line:
                    final_vendor_line = list(filter(lambda r:r.get('vendor_id') == line.partner_id.id, filter_product_line[0]['vendor_lines']))
                    if final_vendor_line:
                        final_vendor_line[0]['quantity'] += line.product_qty
                        final_vendor_line[0]['unit_price'] += line.price_unit
                    else:
                        filter_product_line[0]['vendor_lines'].append({
                            'vendor_id': line.partner_id.id,
                            'vendor_name': line.partner_id.name,
                            'quantity': line.product_qty,
                            'unit_price': line.price_unit, 
                        })
            else:
                temp_data.append({'product_id': line.product_id.id})
                final_line.append({
                    'product_id': line.product_id.id,
                    'product_name': line.product_id.name,
                    'vendor_lines': [{
                        'vendor_id': line.partner_id.id,
                        'vendor_name': line.partner_id.name,
                        'quantity': line.product_qty,
                        'unit_price': line.price_unit,
                    }]
                    })
        return final_line

class PurchaseAgreementComparison(models.Model):
    _name = 'purchase.agreement.comparison'

    point = [
        ('0', 'Not Use'),
        ('1', 'Poor'),
        ('2', 'Fair'),
        ('3', 'Satisfied'),
        ('4', 'Good'),
        ('5', 'Excellent')
    ]

    agreement_id = fields.Many2one('purchase.agreement', string="Tender")
    partner_id = fields.Many2one('res.partner', string='Vendor')
    on_time_rate = fields.Float(string='Delivery on Schedule (%)', compute='_get_fulfillment', store=True)
    fulfillment = fields.Float(string="Fulfillment (%)", compute='_get_fulfillment', store=True)
    final_point = fields.Float(readonly=True, store=True, string="Final Point")
    final_star = fields.Selection(point, string="Vendor Rate", readonly=True)

    @api.depends('partner_id')
    def _get_fulfillment(self):
        for rec in self:
            rec.on_time_rate = 0
            rec.fulfillment = 0
            rec.final_point = 0
            end_date = date.today()
            start_date = end_date - timedelta(days=365)
            vendor_eval = self.env['vendor.evaluation'].search([
                        ('vendor', '=', rec.partner_id.id),
                        ('period_start', '>=', start_date),
                        ('period_end', '<=', end_date),
                        ('state', '=', 'approved')
                    ])
            if len(vendor_eval) > 0:
                total_fullfillment = sum(vendor_eval.mapped('fulfillment')) / len(vendor_eval)
                total_on_time_rate = sum(vendor_eval.mapped('on_time_rate')) / len(vendor_eval)
                total_final_point = sum(vendor_eval.mapped('final_point')) / len(vendor_eval)
                rec.fulfillment = total_fullfillment if total_fullfillment > 0 else 0
                rec.on_time_rate = total_on_time_rate if total_on_time_rate > 0 else 0
                rec.final_point = total_final_point if total_final_point > 0 else 0
                rec.final_star = str(round(rec.final_point))
