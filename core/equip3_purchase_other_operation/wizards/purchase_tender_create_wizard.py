
from odoo import api, fields, models, _


class PurchaseTenderCreateWizard(models.TransientModel):
    _name = 'purchase.tender.create.wizard'

    vendor_ids = fields.Many2many('res.partner', string='Vendor')
    product_line_ids = fields.One2many('purchase.tender.create.lines.wizard', 'purchse_tender_lines', string="Product Lines")
    sh_source = fields.Char(string="Source")

    def _create_tender_vals(self):
        data = []
        context = dict(self.env.context) or {}
        for record in self:
            for product_line_id in record.product_line_ids:
                product_line_id.pr_line_id.tender_qty += product_line_id.tender_qty
                data.append((0, 0, {'sh_product_id' : product_line_id.product_id.id,
                                'sh_product_description': product_line_id.product_description,
                                'sh_qty' : product_line_id.tender_qty,
                                'request_line_id': product_line_id.pr_line_id.id,
                                'sh_ordered_qty' : product_line_id.remaning_qty,
                                'sh_product_uom_id': product_line_id.uom.id,
                                'dest_warehouse_id': product_line_id.destination_warehouse.id,
                                'analytic_tag_ids': product_line_id.analytics_tag_ids.ids,
                                'schedule_date': product_line_id.schedule_date,
                }))
        vals = {
            'partner_ids' : self.vendor_ids.ids,
            'sh_source': self.sh_source,
            'sh_purchase_agreement_line_ids' : data,
        }
        return vals

    def action_confirm(self):
        res = []
        vals = self._create_tender_vals()
        context = dict(self.env.context) or {}
        purchase_tender = self.env['purchase.agreement'].create(vals)
        res.append(purchase_tender.id)
        if context.get('goods_order'):
            purchase_tender.is_goods_orders = True
        elif context.get('services_good'):
            purchase_tender.is_services_orders = True
        elif context.get('assets_orders'):
            purchase_tender.is_assets_orders = True

        return {
                "domain": [("id", "in", res)],
                'type': 'ir.actions.act_window',
                'name': 'Purchaes Tender',
                'view_mode': 'tree,form',
                'res_model': 'purchase.agreement',
                'res_id' : purchase_tender.id,
                "view_id": False,
                "context": False,
                # 'target': 'current'
            }

class PurchaseTenderCreateLinesWizard(models.TransientModel):
    _name = 'purchase.tender.create.lines.wizard'

    purchse_tender_lines = fields.Many2one('purchase.tender.create.wizard', string='Purchase Tender Lines')
    product_id = fields.Many2one('product.product', string="Product")
    product_description = fields.Char(string="Description")
    remaning_qty = fields.Float(string="Remaning Qty")
    tender_qty = fields.Float(string="Quantity To Tender")
    uom = fields.Many2one('uom.uom', string="Uom")
    pr_line_id = fields.Many2one('purchase.request.line', string="Purchase Line")
    request_id = fields.Many2one('purchase.request', related='pr_line_id.request_id', string="Purchase Request")
    destination_warehouse = fields.Many2one('stock.warehouse', string="Warehouse")
    analytics_tag_ids = fields.Many2many('account.analytic.tag', string="Analytic Tags")
    schedule_date = fields.Date(string="Schedule Date")