
{
    'name': 'Accounting Cash Advance',
    'version': '1.1.4',
    'author': 'Hashmicro / Prince',
    'category' : 'Accounting',
    'depends': ['equip3_accounting_deposit', 'hr', 'equip3_accounting_accessright_setting'],
    'data': [
        'wizard/return_deposit_wizard_view.xml',
        'views/vendor_deposit_views.xml'
    ],
    'demo': [],
    'auto_install': False,
    'installable': True,
    'application': True
}