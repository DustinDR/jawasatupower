# -*- coding: utf-8 -*-
{
    "name": "Joint Calendar",
    "version": "14.0.1.0.4",
    "category": "Extra Tools",
    "author": "faOtools",
    "website": "https://faotools.com/apps/14.0/joint-calendar-493",
    "license": "Other proprietary",
    "application": True,
    "installable": True,
    "auto_install": False,
    "depends": [
        "calendar",
        "mail"
    ],
    "data": [
        "security/security.xml",
        "views/configurations.xml",
        "views/event_rule_view.xml",
        "views/joint_calendar_view.xml",
        "views/joint_event_view.xml",
        "views/menu.xml",
        "data/cron.xml",
        "data/data.xml",
        "security/ir.model.access.csv"
    ],
    "qweb": [
        
    ],
    "js": [
        
    ],
    "demo": [
        
    ],
    "external_dependencies": {},
    "summary": "The tool to combine different Odoo events in a few configurable super calendars. Shared calendar. Common calendar.",
    "description": """
For the full details look at static/description/index.html

* Features * 

- Super calendars for any business area
- Flexible rules to merge Odoo documents



#odootools_proprietary

    """,
    "images": [
        "static/description/main.png"
    ],
    "price": "74.0",
    "currency": "EUR",
    "live_test_url": "https://faotools.com/my/tickets/newticket?&url_app_id=27&ticket_version=14.0&url_type_id=3",
}