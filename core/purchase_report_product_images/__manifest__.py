# -*- coding: utf-8 -*-
#╔══════════════════════════════════════════════════════════════════════╗
#║                                                                      ║
#║                  ╔═══╦╗       ╔╗  ╔╗     ╔═══╦═══╗                   ║
#║                  ║╔═╗║║       ║║ ╔╝╚╗    ║╔═╗║╔═╗║                   ║
#║                  ║║ ║║║╔╗╔╦╦══╣╚═╬╗╔╬╗ ╔╗║║ ╚╣╚══╗                   ║
#║                  ║╚═╝║║║╚╝╠╣╔╗║╔╗║║║║║ ║║║║ ╔╬══╗║                   ║
#║                  ║╔═╗║╚╣║║║║╚╝║║║║║╚╣╚═╝║║╚═╝║╚═╝║                   ║
#║                  ╚╝ ╚╩═╩╩╩╩╩═╗╠╝╚╝╚═╩═╗╔╝╚═══╩═══╝                   ║
#║                            ╔═╝║     ╔═╝║                             ║
#║                            ╚══╝     ╚══╝                             ║
#║                  SOFTWARE DEVELOPED AND SUPPORTED BY                 ║
#║                ALMIGHTY CONSULTING SOLUTIONS PVT. LTD.               ║
#║                      COPYRIGHT (C) 2016 - TODAY                      ║
#║                      https://www.almightycs.com                      ║
#║                                                                      ║
#╚══════════════════════════════════════════════════════════════════════╝
{
    'name': 'Product Image and line sequence on Purchase Reports',
    'version': '1.0',
    'author': 'Almighty Consulting Solutions Pvt. Ltd.',
    'support': 'info@almightycs.com',
    'category': 'Purchase Management',
    'summary': 'Product Image and line sequence on Purchase Reports',
    'description': """Product Image and line sequence on Purchase Reports
    Print Image in Purchase report
    Purchase Report
    Image in Report
    product image
    line sequence in report
    line number in report
    sequence number in report
    row number in report
    """,
    'author': 'Almighty Consulting Solutions Pvt. Ltd.',
    'depends': ['purchase'],
    'website': 'https://www.almightycs.com',
    'license': 'OPL-1',
    'data': [
        "views/purchase_product_view.xml",
        "views/report_purchaseorder.xml",
    ],
    'images': [
        'static/description/purchase_report_print_image_medium_almightycs.png',
    ],
    'application': False,
    'price': 14,
    'currency': 'EUR',
}
