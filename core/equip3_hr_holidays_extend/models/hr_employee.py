from odoo import api, fields, models, _


class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    _description = "Hr Employee"

    leave_struct_id = fields.Many2one('hr.leave.structure', string='Leave Structure')
