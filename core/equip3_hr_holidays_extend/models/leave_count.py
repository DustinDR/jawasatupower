from odoo import api, fields, models, _
from datetime import date, timedelta
from dateutil import relativedelta as rd
from dateutil.relativedelta import relativedelta


class HrLeaveCount(models.Model):
    _name = 'hr.leave.count'
    _description = 'Hr Leave Count'
    _order = 'id desc'

    @api.depends('count')
    def compute_active(self):
        for count in self:
            if count.count > 0:
                count.active = True
            else:
                count.active = False

    name = fields.Char('Name', copy=False, related="employee_id.name")
    active = fields.Boolean(String='Active', compute='compute_active', store=True)
    employee_id = fields.Many2one('hr.employee', string='Employee')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company)
    holiday_status_id = fields.Many2one("hr.leave.type", string="Leave Type")
    start_date = fields.Date('Start Valid Date', required="1")
    expired_date = fields.Date('Valid Until', required="1")
    hr_months = fields.Integer('Hr Months')
    hr_years_monthly = fields.Integer('Hr Years Monthly')
    hr_years = fields.Integer('Hr Years', group_operator='avg')
    count = fields.Float('Count')
    is_expired = fields.Boolean(String='Expired', compute='compute_current_period', store=True)
    description = fields.Char('Description')
    holiday_type = fields.Selection([
        ('employee', 'By Employee'),
        ('category', 'By Employee Tag')
    ], string='Allocation Mode', readonly=True)
    check_date = fields.Date('Current Year', store=True, compute='compute_current_period')

    @api.depends('employee_id', 'hr_years', 'start_date')
    def compute_current_period(self):
        current_date = fields.Date.today()
        for balance in self:
            if balance.start_date and balance.expired_date:
                if balance.expired_date >= current_date >= balance.start_date:
                    balance.is_expired = False
                else:
                    balance.is_expired = True
            if balance.hr_years:
                balance.check_date = date(balance.hr_years, balance.start_date.month, balance.start_date.day)
            else:
                balance.check_date = 0

    def create_allocation_leave_count(self):
        current_date = fields.Date.today()
        current_year = current_date.year
        for emp in self.env['hr.employee'].search([('active', '=', True)]):
            for leave_type in self.env['hr.leave.type'].search([]):
                current_balance = self.env['hr.leave.balance'].search(
                    [('employee_id', '=', emp.id), ('holiday_status_id', '=', leave_type.id)], order='id desc', limit=1)
                for count in self.env['hr.leave.count'].with_context(active_test=False).search(
                        [('employee_id', '=', emp.id), ('holiday_status_id', '=', leave_type.id),
                         ('description', '=', 'Allocation')], order='id desc',
                        limit=1):
                    start_date = False
                    end_date = False
                    if count.holiday_status_id.leave_method == 'monthly':
                        start_date = count.start_date + relativedelta(months=1)
                        start_date_month = date(current_date.year, current_date.month, start_date.day)
                        if count.start_date != start_date_month:
                            if count.expired_date > current_date:
                                end_date = count.expired_date
                                if int(current_balance.current_period) == current_date.year:
                                    current_balance.assigned = current_balance.assigned + leave_type.leave_entitlement
                            else:
                                end_date = count.expired_date + relativedelta(years=1)
                                self.env['hr.leave.balance'].create({
                                    'employee_id': emp.id,
                                    'holiday_status_id': leave_type.id,
                                    'leave_entitlement': leave_type.leave_entitlement,
                                    'assigned': leave_type.leave_entitlement,
                                    'hr_years': current_date.year,
                                    'hr_years_monthly': current_date.year,
                                    'start_date': start_date,
                                    'current_period': current_year,
                                })
                                current_balance.active = False
                    elif count.holiday_status_id.leave_method != 'monthly':
                        start_date = date(current_date.year, count.start_date.month, count.start_date.day)
                        if count.start_date != start_date:
                            self.env['hr.leave.balance'].create({
                                'employee_id': emp.id,
                                'holiday_status_id': leave_type.id,
                                'start_date': start_date,
                                'leave_entitlement': leave_type.leave_entitlement,
                                'assigned': leave_type.leave_entitlement,
                                'hr_years': current_date.year,
                                'hr_years_monthly': current_date.year,
                                'current_period': current_year,
                            })
                            current_balance.active = False
                            end_date = count.expired_date + relativedelta(years=1)
                    if end_date:
                        self.env['hr.leave.count'].create({
                            'employee_id': emp.id,
                            'holiday_status_id': leave_type.id,
                            'count': leave_type.leave_entitlement,
                            'start_date': start_date,
                            'expired_date': end_date,
                            'hr_years': current_date.year,
                            'hr_months': current_date.month,
                            'description': 'Allocation',
                        })

    def expired_carry_forward(self):
        for count in self.env['hr.leave.count'].with_context(active_test=False).search(
                [('is_expired', '=', True), ('description', '=', 'Carry Forward')]):
            for balance in self.env['hr.leave.balance'].search(
                    [('employee_id', '=', count.employee_id.id), ('holiday_status_id', '=', count.holiday_status_id.id),
                     ('current_period', '=', count.hr_years), ('count_ids', 'not in', count.id)]):
                if balance:
                    balance.bring_forward = balance.bring_forward - count.count
                    balance.count_ids = [(4, count.id)]

    @api.model
    def _cron_unlink_expire_leave_count(self):
        self.expired_carry_forward()
        current_date = fields.Date.today()
        self.create_allocation_leave_count()
        leave_balance = self.env['hr.leave.balance'].search([('current_period', '<', current_date.year)])
        if leave_balance:
            for lb in leave_balance:
                lb.active = False
        leave_count = self.env['hr.leave.count'].search(
            [('expired_date', '<', current_date), ('active', '=', True)])
        if leave_count:
            for lc in leave_count:
                # leave_balance_reduce = self.env['hr.leave.balance'].search(
                #     [('employee_id', '=', lc.employee_id.id), ('holiday_status_id', '=', lc.holiday_status_id.id),
                #      ('current_period', '<', current_date.year)])
                # leave_balance_reduce.active = False

                leave_balance_active = self.env['hr.leave.balance'].search([('employee_id', '=', lc.employee_id.id),
                                                                        ('holiday_status_id', '=', lc.holiday_status_id.id),
                                                                        ('current_period', '=', current_date.year),
                                                                        ('active', '=', True)], limit=1)
                assigned = leave_balance_active.assigned - lc.count
                leave_balance_active.assigned = assigned
                lc.active = False
