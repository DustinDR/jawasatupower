from odoo import api, fields, models, _


class HrLeaveReportCalendarInherit(models.Model):
    _inherit = 'hr.leave.report.calendar'

    def custom_menu(self):
        # views = [(self.env.ref('bi_employee_travel_managment.view_travel_req_tree').id, 'tree'),
        #              (self.env.ref('bi_employee_travel_managment.view_travel_req_form').id, 'form')]
    # search_view_id = self.env.ref("hr_contract.hr_contract_view_search")
        if  self.env.user.has_group('equip3_hr_employee_access_right_setting.group_responsible') and not self.env.user.has_group('hr_holidays.group_hr_holidays_user'):
            employee_ids = []
            my_employee = self.env['hr.employee'].sudo().search([('user_id','=',self.env.user.id)])
            if my_employee:
                for child_record in my_employee.child_ids:
                    employee_ids.append(my_employee.id)
                    employee_ids.append(child_record.id)
                    child_record._get_amployee_hierarchy(employee_ids,child_record.child_ids,my_employee.id)
                return {
                    'type': 'ir.actions.act_window',
                    'name': 'All Time Off',
                    'res_model': 'hr.leave.report.calendar',
                    'target':'current',
                    'view_mode': 'calendar',
                    # 'views':views,
                    'domain': [('employee_id', 'in', employee_ids),('employee_id.active','=',True)],
                    'context':{'hide_employee_name': 1},
                    'help':"""<p class="o_view_nocontent_smiling_face">
                        Create a new All Time Off
                    </p>"""
                    # 'context':{'search_default_current':1, 'search_default_group_by_state': 1},
                    # 'search_view_id':search_view_id.id,
                    
                }
        
        else:
            return {
                'type': 'ir.actions.act_window',
                'name': 'All Time Off',
                'res_model': 'hr.leave.report.calendar',
                'target':'current',
                'view_mode': 'calendar',
                'domain': [('employee_id.active','=',True)],
                'help':"""<p class="o_view_nocontent_smiling_face">
                    Create a new All Time Off
                </p>""",
                'context':{'hide_employee_name': 1},
                # 'views':views,
                # 'search_view_id':search_view_id.id,
            }
