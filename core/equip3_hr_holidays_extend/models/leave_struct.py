from odoo import api, fields, models, _
from lxml import etree

class HrLeaveStructure(models.Model):
    _name = 'hr.leave.structure'
    _description = "Hr Leave Structure"
    _inherit = ['mail.thread']

    name = fields.Char('Name', required=True, tracking=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company, tracking=True)
    branch_id = fields.Many2one("res.branch", string="Branch", domain="[('company_id', '=', company_id)]",
                                tracking=True)
    leaves_ids = fields.Many2many('hr.leave.type', string='Leave Types', copy=True)
    
    
    @api.model
    def fields_view_get(self, view_id=None, view_type=None,
                        toolbar=True, submenu=True):
        res = super(HrLeaveStructure, self).fields_view_get(
            view_id=view_id, view_type=view_type,toolbar=toolbar,submenu=submenu)
        if  not self.env.user.has_group('hr_holidays.group_hr_holidays_user'):
            root = etree.fromstring(res['arch'])
            root.set('create', 'false')
            root.set('edit', 'false')
            root.set('delete', 'false')
            res['arch'] = etree.tostring(root)
            
        return res


class HrLeaveType(models.Model):
    _inherit = 'hr.leave.type'

    parent_leave_id = fields.Many2one('hr.leave.type', string='Leave Type', index=True)
    child_leaves_ids = fields.One2many('hr.leave.type', 'parent_leave_id', string='Leave Types', copy=True)
