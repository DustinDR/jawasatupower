from odoo import api, fields, models, _
from odoo.tools.float_utils import float_round
from datetime import date
from odoo.exceptions import ValidationError
from lxml import etree


class HrLeaveType(models.Model):
    _name = 'hr.leave.type'
    _description = "Hr Leave Type"
    _inherit = ['hr.leave.type', 'mail.thread']

    name = fields.Char('Leave Type', required=True, translate=True, tracking=True)
    gender = fields.Selection([('male', 'Male'), ('female', 'Female')], string='Gender')
    is_dashboard = fields.Boolean('Show on Dashboard', default=False, tracking=True)
    leave_entitlement = fields.Float('Entitlement Leave', required=True, tracking=True)
    extra_leave_after = fields.Integer('Extra Leave After', tracking=True)
    extra_leave_frequency = fields.Integer('Extra Leave per Frequency', tracking=True)
    interval_unit = fields.Selection([('yearly', 'Yearly'), ('monthly', 'Monthly')], string='Interval Unit',
                                     tracking=True)
    maximum_extra_leave = fields.Integer('Maximum Extra Leave', tracking=True)
    months = fields.Integer(tracking=True)
    days = fields.Integer(tracking=True)
    year = fields.Char(string=' ', default='year(s) -')
    month = fields.Char(string=' ', default='month(s) -')
    day = fields.Char(string=' ', default='day(s)')
    allow_minus = fields.Boolean('Allow Minus', default=False, tracking=True)
    maximum_minus = fields.Integer('Maximum Minus', tracking=True)
    allow_past_date = fields.Boolean('Allow Past Date', default=False, tracking=True)
    past_days = fields.Integer('Past Dated Days', tracking=True)
    carry_forward = fields.Selection(
        [('none', 'None'), ('remaining_amount', 'Remaining Amount'), ('specific_days', 'Specific Days')],
        string='Carry Forward', tracking=True)
    no_of_days = fields.Integer('Number of Days', required=True, tracking=True)
    carry_forward_expiry = fields.Integer('Carry Forward Expiry', tracking=True)
    rounding = fields.Selection([
        ('no_rounding', 'No Rounding'), ('rounding_up', 'Rounding Up'), ('rounding_down', 'Rounding Down')],
        string='Rounding', tracking=True)
    request_unit = fields.Selection([
        ('day', 'Day'), ('half_day', 'Half Day'), ('hour', 'Hours')],
        default='day', string='Take Leave in', required=True, tracking=True)
    leave_method = fields.Selection(
        [('annually', 'Annually'), ('anniversary', 'Anniversary'), ('monthly', 'Monthly'), ('none', 'None')],
        string='Leave Method', tracking=True)
    allocation_validation_type = fields.Selection([
        ('hr', 'By Leaves Officer'),
        ('manager', "By Employee's Manager"),
        ('both', "By Employee's Manager and Leaves Officer")], default='manager', string='Allocation Validation')
    leave_months_appear = fields.Selection([
        ('1', 'January'),
        ('2', 'February'),
        ('3', 'March'),
        ('4', 'April'),
        ('5', 'May'),
        ('6', 'June'),
        ('7', 'July'),
        ('8', 'August'),
        ('9', 'September'),
        ('10', 'October'),
        ('11', 'November'),
        ('12', 'December')], default='1', string='Leave Date Appear', tracking=True)
    leave_date_appear = fields.Selection([
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
        ('11', '11'),
        ('12', '12'),
        ('13', '13'),
        ('14', '14'),
        ('15', '15'),
        ('16', '16'),
        ('17', '17'),
        ('18', '18'),
        ('19', '19'),
        ('20', '20'),
        ('21', '21'),
        ('22', '22'),
        ('23', '23'),
        ('24', '24'),
        ('25', '25'),
        ('26', '26'),
        ('27', '27'),
        ('28', '28'),
        ('29', '29'),
        ('30', '30'),
        ('31', '31')
    ], default='1', string='Leave Date Appear', tracking=True)
    valid_leave = fields.Selection(
        [('one_year', '1 Year After Allocation'), ('end_year', 'End of the Running Year')],
        string='Valid until', tracking=True)
    maximum_leave = fields.Integer(string="Maximum Leave")
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company, tracking=True)
    branch_id = fields.Many2one("res.branch", string="Branch", domain="[('company_id', '=', company_id)]",
                                tracking=True)
    limit_days = fields.Integer(string="Limit Days to Apply", tracking=True)
    minimum_days_before = fields.Integer('Minimum Days Before Application', tracking=True)
    is_required = fields.Boolean(string="Required Attachment", default=True, tracking=True)
    day_count = fields.Selection([('work_day', 'Work Day'), ('calendar_day', "Calendar Day")], default='work_day',
                                 string='Day Count By', tracking=True)
    set_by = fields.Selection([('duration', "Duration"), ('times', 'Times')], default='duration',
                              string='Set By', tracking=True, required=True)
    allocation_type = fields.Selection([
        ('no', 'No Limit'),
        ('fixed_allocation', 'Allow Employees Requests'),
        ('fixed', 'Set by Leave Officer')],
        default='no', string='Mode', tracking=True,
        help='\tNo Limit: no allocation by default, users can freely request time off; '
             '\tAllow Employees Requests: allocated by HR and users can request time off and allocations; '
             '\tSet by Time Off Officer: allocated by HR and cannot be bypassed; users can request time off;')
    leave_validation_type = fields.Selection(selection=[
        ('no_validation', 'No Validation'),
        ('by_employee_hierarchy', "By Employee Hierarchy"),
        ('by_approval_matrix', "By Approval Matrix"), ('both', "By Employee's Manager and Leave Officer")],
        default=False, string='Leave Validation')
    leave_validation_type_clone = fields.Selection(selection=[
        ('no_validation', 'No Validation'),
        ('by_employee_hierarchy', "By Employee Hierarchy"),
        ('by_approval_matrix', "By Approval Matrix"), ('both', "By Employee's Manager and Leave Officer")],
        default=False, string='Leave Validation')
    leave_notif_subtype_id = fields.Many2one('mail.message.subtype', string='Leave Notification Subtype',
                                             default=lambda self: self.env.ref('hr_holidays.mt_leave',
                                                                               raise_if_not_found=False))
    approval_level = fields.Integer(string="Approval Levels", default=1)
    create_calendar_meeting = fields.Boolean(string="Display Leave in Calendar", default=True)
    employee_id = fields.Many2one('hr.employee', string='Attendance Employee')
    expiry_days = fields.Integer()
    repeated_allocation = fields.Boolean("Repeated Allocation")
    overtime_extra_leave = fields.Boolean("Overtime Extra Leave")
    min_days_before_alloc = fields.Integer('Minimum Days Before Application')
    total_actual_hours = fields.Boolean("Total Actual Hours")
    formula = fields.Text(string='Formula', 
        default='''if actual_hours >= 4 and actual_hours < 8:
    duration = 0.5
elif actual_hours >= 8:
    duration = 1.0''')

    @api.model
    def fields_view_get(self, view_id=None, view_type=None,
                        toolbar=True, submenu=True):
        res = super(HrLeaveType, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if not self.env.user.has_group('hr_holidays.group_hr_holidays_user'):
            root = etree.fromstring(res['arch'])
            root.set('create', 'false')
            root.set('edit', 'false')
            root.set('delete', 'false')
            res['arch'] = etree.tostring(root)

        return res

    @api.onchange('leave_validation_type_clone')
    def _onchange_leave_validation_type_clone(self):
        for record in self:
            if record.leave_validation_type_clone:
                record.leave_validation_type = record.leave_validation_type_clone

    @api.constrains('leave_validation_type')
    def _check_validation_type(self):
        if self.leave_validation_type in ('hr', 'manager'):
            raise ValidationError(
                _('You can not choose empty space.'))

    def name_get(self):
        if not self.employee_id:
            # leave counts is based on employee_id, would be inaccurate if not based on correct employee
            return super(HrLeaveType, self).name_get()
        res = []
        for record in self:
            name = record.name
            employee_id = record.employee_id
            check_period = date.today().year
            leave_balance = record.env['hr.leave.balance'].search(
                [('employee_id', '=', employee_id.id), ('holiday_status_id', '=', record.id),
                 ('current_period', '=', str(check_period))], limit=1)
            if record.allocation_type and leave_balance:
                name = "%(name)s (%(count)s)" % {
                    'name': name,
                    'count': _('%g remaining out of %g') % (
                        float_round(float(leave_balance.remaining), precision_digits=2) or 0.0,
                        float_round(float(leave_balance.assigned), precision_digits=2) or 0.0,
                    ) + (_(' hours') if record.request_unit == 'hour' else _(' days'))
                }
            res.append((record.id, name))
            record.update({
                'employee_id': False,
            })
        return res
