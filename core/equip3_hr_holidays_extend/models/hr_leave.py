from odoo import api, fields, models, _
from datetime import date, timedelta
from odoo.exceptions import ValidationError, UserError, AccessError
from odoo.osv import expression
from dateutil.relativedelta import relativedelta
import requests

headers = {'content-type': 'application/json'}

class HrLeave(models.Model):
    _inherit = 'hr.leave'
    _description = "Hr Leave"
    _rec_name = "seq_name"

    def custom_menu(self):
        # views = [(self.env.ref('bi_employee_travel_managment.view_travel_req_tree').id, 'tree'),
        #              (self.env.ref('bi_employee_travel_managment.view_travel_req_form').id, 'form')]
        # search_view_id = self.env.ref("hr_contract.hr_contract_view_search")
        if self.env.user.has_group(
                'equip3_hr_employee_access_right_setting.group_responsible') and not self.env.user.has_group(
                'hr_holidays.group_hr_holidays_user'):
            employee_ids = []
            my_employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)])
            if my_employee:
                for child_record in my_employee.child_ids:
                    employee_ids.append(my_employee.id)
                    employee_ids.append(child_record.id)
                    child_record._get_amployee_hierarchy(employee_ids, child_record.child_ids, my_employee.id)
                return {
                    'type': 'ir.actions.act_window',
                    'name': 'Leave Cancelation',
                    'res_model': 'hr.leave',
                    'target': 'current',
                    'view_mode': 'tree,kanban,form,calendar,activity',
                    # 'views':views,
                    'domain': [('employee_id', 'in', employee_ids)],
                    'context': {'hide_employee_name': 1},
                    'help': """<p class="o_view_nocontent_smiling_face">
                        Create a new All Time Off
                    </p>"""
                    # 'context':{'search_default_current':1, 'search_default_group_by_state': 1},
                    # 'search_view_id':search_view_id.id,

                }

        else:
            return {
                'type': 'ir.actions.act_window',
                'name': 'Leave Cancelation',
                'res_model': 'hr.leave',
                'target': 'current',
                'view_mode': 'tree,kanban,form,calendar,activity',
                'domain': [],
                'help': """<p class="o_view_nocontent_smiling_face">
                    Create a new All Time Off
                </p>""",
                'context': {'hide_employee_name': 1},
                # 'views':views,
                # 'search_view_id':search_view_id.id,
            }

    @api.model
    def create(self, vals):
        sequence_no = self.env['ir.sequence'].next_by_code('hr.leave')
        vals.update({'seq_name': sequence_no})
        result = super(HrLeave, self).create(vals)
        return result

    @api.onchange('employee_id')
    def _onchange_holiday_status_id(self):
        res = {}
        balance_list = []
        if self.holiday_type == 'employee':
            for leave_balance in self.env['hr.leave.balance'].search(
                    [('employee_id', '=', self.employee_id.id),
                     ('active', '=', True)]):
                balance_list.append(leave_balance.holiday_status_id.id)
            res['domain'] = {'holiday_status_id': [('id', 'in', balance_list)]}
        else:
            res['domain'] = {'holiday_status_id': []}
        return res

    seq_name = fields.Char('Name', default='New', copy=False)
    state = fields.Selection([
        ('draft', 'To Submit'),
        ('confirm', 'To Approve'),
        ('refuse', 'Refused'),
        ('validate1', 'Second Approval'),
        ('validate', 'Approved'),
        ('cancel', 'Cancelled')
    ], string='Status', compute='_compute_state', store=True, tracking=True, copy=False, readonly=False,
        help="The status is set to 'To Submit', when a time off request is created." +
             "\nThe status is 'To Approve', when time off request is confirmed by user." +
             "\nThe status is 'Refused', when time off request is refused by manager." +
             "\nThe status is 'Approved', when time off request is approved by manager.")
    is_urgent = fields.Boolean(string="Is Urgent", default=False)
    is_required = fields.Boolean(string="Required Attachment", related="holiday_status_id.is_required")
    attachment = fields.Binary('Attachment')
    holiday_status_id = fields.Many2one("hr.leave.type", string="Leave Type", required=True, readonly=False,
                                        states={'cancel': [('readonly', True)], 'refuse': [('readonly', True)],
                                                'validate1': [('readonly', True)],
                                                'validate': [('readonly', True)]}, domain=[])
    # domain=lambda self: self._domain_holiday_status_id())
    cancel_id = fields.Many2one('hr.leave.cancelation', string='Leave cancel', compute='_compute_cancel')
    approvers_ids = fields.Many2many('res.users', 'approver_users_leave_rel', string='Approvers',
                                     compute="_compute_approvers", store=True)
    approved_user = fields.Text(string="Approved User", tracking=True)
    is_approver = fields.Boolean(string="Is Approver", compute="_compute_can_approve")
    approver_user_ids = fields.One2many('leave.approver.user', 'leave_id', string='Approver')
    approved_user_ids = fields.Many2many('res.users', string='Approved User')
    line_item_visible = fields.Boolean(string="Line item visible", compute="_compute_line_items")
    leave_count_ids = fields.One2many('hr.leave.line', 'leave_id', string='Leave Count')
    leave_balance_id = fields.Many2one('hr.leave.balance', string='Leave Balance')
    current_period = fields.Integer('Current Period', copy=False, store=True, compute='compute_current_period')
    current_year = fields.Integer('Current Year', store=True, compute='compute_current_period')
    is_readonly_mode = fields.Boolean(compute='_compute_readonly_mode')
    is_invisible_mode = fields.Boolean(compute='_compute_is_invisible_mode')
    domain_employee_ids = fields.Many2many('hr.employee', string="Employee Domain", compute='_compute_employee_ids')
    is_refused_by_leave_cancel_form = fields.Boolean('Refused by Leave Cancel Form', default=False)

    def _check_approval_update(self, state):
        """ Check if target state is achievable. """
        if self.env.is_superuser():
            return


    def no_validation(self):
        if self.holiday_status_id.leave_validation_type == 'no_validation':
            self.write({'state': 'draft'})

    @api.model_create_multi
    def create(self, vals_list):
        """ Override to avoid automatic logging of creation """
        if not self._context.get('leave_fast_create'):
            leave_types = self.env['hr.leave.type'].browse(
                [values.get('holiday_status_id') for values in vals_list if values.get('holiday_status_id')])
            mapped_validation_type = {leave_type.id: leave_type.leave_validation_type for leave_type in leave_types}

            for values in vals_list:
                employee_id = values.get('employee_id', False)
                leave_type_id = values.get('holiday_status_id')
                # Handle automatic department_id
                if not values.get('department_id'):
                    values.update({'department_id': self.env['hr.employee'].browse(employee_id).department_id.id})

                # Handle no_validation
                if mapped_validation_type[leave_type_id] == 'no_validation':
                    values.update({'state': 'confirm'})

                if 'state' not in values:
                    # To mimic the behavior of compute_state that was always triggered, as the field was readonly
                    values['state'] = 'confirm' if mapped_validation_type[leave_type_id] != 'no_validation' else 'draft'

                # Handle double validation
                if mapped_validation_type[leave_type_id] == 'both':
                    self._check_double_validation_rules(employee_id, values.get('state', False))

        holidays = super(HrLeave, self.with_context(mail_create_nosubscribe=True)).create(vals_list)
        holidays.no_validation()
        for holiday in holidays:
            if not self._context.get('leave_fast_create'):
                # Everything that is done here must be done using sudo because we might
                # have different create and write rights
                # eg : holidays_user can create a leave request with validation_type = 'manager' for someone else
                # but they can only write on it if they are leave_manager_id
                holiday_sudo = holiday.sudo()
                holiday_sudo.add_follower(employee_id)
                if holiday.validation_type == 'manager':
                    holiday_sudo.message_subscribe(partner_ids=holiday.employee_id.leave_manager_id.partner_id.ids)
                if holiday.validation_type == 'no_validation':
                    # Automatic validation should be done in sudo, because user might not have the rights to do it by himself
                    holiday_sudo.action_validate()
                    holiday_sudo.message_subscribe(partner_ids=[holiday._get_responsible_for_approval().partner_id.id])
                    holiday_sudo.message_post(body=_("The time off has been automatically approved"),
                                              subtype_xmlid="mail.mt_comment")  # Message from OdooBot (sudo)
                elif not self._context.get('import_file'):
                    holiday_sudo.activity_update()
        return holidays

    @api.depends('holiday_type')
    def _compute_employee_ids(self):
        for record in self:
            employee_ids = []
            if self.env.user.has_group(
                    'equip3_hr_employee_access_right_setting.group_responsible') and not self.env.user.has_group(
                    'hr_holidays.group_hr_holidays_user'):
                my_employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.env.user.id)])
                if my_employee:
                    for child_record in my_employee.child_ids:
                        employee_ids.append(my_employee.id)
                        employee_ids.append(child_record.id)
                        child_record._get_amployee_hierarchy(employee_ids, child_record.child_ids, my_employee.id)
                record.domain_employee_ids = [(6, 0, employee_ids)]
            else:
                all_employee = self.env['hr.employee'].sudo().search([])
                for data_employee in all_employee:
                    employee_ids.append(data_employee.id)
                record.domain_employee_ids = [(6, 0, employee_ids)]

    @api.depends('holiday_type')
    def _compute_readonly_mode(self):
        for record in self:
            if self.env.user.has_group(
                    'equip3_hr_employee_access_right_setting.group_responsible') and not self.env.user.has_group(
                    'hr_holidays.group_hr_holidays_user'):
                record.is_readonly_mode = True
            else:
                record.is_readonly_mode = False

    @api.depends('holiday_type')
    def _compute_is_invisible_mode(self):
        for record in self:
            if self.env.user.has_group('hr_holidays.group_hr_holidays_user'):
                record.is_invisible_mode = True
            else:
                record.is_invisible_mode = False

    @api.depends('holiday_status_id')
    def _compute_state(self):
        for holiday in self:
            if self.env.context.get('unlink') and holiday.state == 'draft':
                # Otherwise the record in draft with validation_type in (hr, manager, both) will be set to confirm
                # and a simple internal user will not be able to delete his own draft record
                holiday.state = 'draft'
            else:
                holiday.state = 'draft' if holiday.validation_type != 'no_validation' else 'draft'

    @api.depends('employee_id', 'request_date_to')
    def compute_current_period(self):
        for leave in self:
            if leave.request_date_to:
                leave.current_period = leave.request_date_to.year
                leave.current_year = date.today().year
            else:
                leave.current_period = 0
                leave.current_year = 0

    @api.depends('state', 'employee_id', 'department_id')
    def _compute_can_approve(self):
        for holiday in self:
            # try:
            #     if holiday.state == 'confirm' and holiday.validation_type == 'both':
            #         holiday._check_approval_update('validate1')
            #     else:
            #         holiday._check_approval_update('validate')
            # except (AccessError, UserError):
            #     holiday.can_approve = False
            # else:
            #     holiday.can_approve = True
            if holiday.approvers_ids:
                if holiday.holiday_status_id.leave_validation_type == 'by_employee_hierarchy':
                    current_user = holiday.env.user
                    matrix_line = sorted(holiday.approver_user_ids.filtered(lambda r: r.is_approve == True))
                    app = len(matrix_line)
                    a = len(holiday.approver_user_ids)
                    if app < holiday.holiday_status_id.approval_level and app < a:
                        if current_user in holiday.approver_user_ids[len(matrix_line)].employee_id:
                            holiday.is_approver = True
                            holiday.can_approve = True
                        else:
                            holiday.is_approver = False
                            holiday.can_approve = False
                    else:
                        holiday.is_approver = False
                        holiday.can_approve = False
                elif holiday.holiday_status_id.leave_validation_type == 'by_approval_matrix':
                    current_user = holiday.env.user
                    matrix_line = sorted(holiday.approver_user_ids.filtered(lambda r: r.is_approve == True))
                    app = len(matrix_line)
                    a = len(holiday.approver_user_ids)
                    if app < a:
                        for line in holiday.approver_user_ids[app]:
                            if current_user in holiday.approved_user_ids:
                                holiday.is_approver = False
                                holiday.can_approve = False
                            elif current_user in line.employee_id:
                                holiday.is_approver = True
                                holiday.can_approve = True
                            else:
                                holiday.is_approver = False
                                holiday.can_approve = False
                    else:
                        holiday.is_approver = False
                        holiday.can_approve = False
                elif holiday.holiday_status_id.leave_validation_type == 'hr':
                    current_user = holiday.env.user
                    if current_user in holiday.approvers_ids:
                        holiday.is_approver = True
                        holiday.can_approve = True
                    else:
                        holiday.is_approver = False
                        holiday.can_approve = False
                elif holiday.holiday_status_id.leave_validation_type == 'manager':
                    current_user = holiday.env.user
                    if current_user in holiday.approvers_ids:
                        holiday.is_approver = True
                        holiday.can_approve = True
                    else:
                        holiday.is_approver = False
                        holiday.can_approve = False
                elif holiday.holiday_status_id.leave_validation_type == 'both':
                    current_user = holiday.env.user
                    matrix_line = sorted(holiday.approver_user_ids.filtered(lambda r: r.is_approve == True))
                    app = len(matrix_line)
                    a = len(holiday.approver_user_ids)
                    if app < 2 and app < a:
                        if current_user in holiday.approver_user_ids[len(matrix_line)].employee_id:
                            holiday.is_approver = True
                            holiday.can_approve = True
                        else:
                            holiday.is_approver = False
                            holiday.can_approve = False
                    else:
                        holiday.is_approver = False
                        holiday.can_approve = False
                else:
                    holiday.is_approver = False
                    holiday.can_approve = False
            else:
                holiday.is_approver = False
                holiday.can_approve = False

    @api.depends('employee_id', 'holiday_status_id', 'department_id')
    def _compute_approvers(self):
        for leave in self:
            app_list = []
            if leave.employee_id and leave.holiday_status_id.leave_validation_type == 'by_employee_hierarchy':
                app_level = leave.holiday_status_id.approval_level
                employee = leave.employee_id
                for i in range(app_level):
                    emp = self.env['hr.employee'].search([('id', '=', employee.id)])
                    if emp:
                        parent = self.env['hr.employee'].search([('id', '=', emp.parent_id.id)])
                        employee = parent
                        app_list.append(employee.user_id.id)
            if leave.employee_id and leave.holiday_status_id.leave_validation_type == 'hr':
                responsible = leave.holiday_status_id.responsible_id
                # emp_responsible = self.env['hr.employee'].search([('user_id', '=', responsible.id)],
                #                                                  limit=1)
                if responsible:
                    app_list.append(responsible.id)
            if leave.employee_id and leave.holiday_status_id.leave_validation_type == 'manager':
                emp_manager = leave.employee_id.parent_id.user_id
                if emp_manager:
                    app_list.append(emp_manager.id)
            if leave.employee_id and leave.holiday_status_id.leave_validation_type == 'both':
                both_emp_manager = leave.employee_id.parent_id.user_id
                both_responsible = leave.holiday_status_id.responsible_id
                # both_emp_responsible = self.env['hr.employee'].search([('user_id', '=', both_responsible.id)],
                #                                                       limit=1)
                if both_emp_manager:
                    app_list.append(both_emp_manager.id)
                if both_responsible:
                    app_list.append(both_responsible.id)
            if leave.holiday_status_id.leave_validation_type == 'by_approval_matrix':
                employee_matrix = self.env['hr.leave.approval'].search([('employee_ids', 'in', leave.employee_id.id)],
                                                                       limit=1)
                job_position_matrix = self.env['hr.leave.approval'].search(
                    [('job_ids', 'in', leave.employee_id.job_id.id)], limit=1)
                department_matrix = self.env['hr.leave.approval'].search(
                    [('department_ids', 'in', leave.employee_id.department_id.id)], limit=1)
                if employee_matrix:
                    for line in employee_matrix.leave_approvel_matrix_ids:
                        for approvers in line.approver_ids:
                            app_list.append(approvers.id)
                elif job_position_matrix:
                    for line in job_position_matrix.leave_approvel_matrix_ids:
                        for approvers in line.approver_ids:
                            app_list.append(approvers.id)
                elif department_matrix:
                    for line in department_matrix.leave_approvel_matrix_ids:
                        for approvers in line.approver_ids:
                            app_list.append(approvers.id)
            leave.approvers_ids = app_list

    @api.onchange('holiday_status_id', 'employee_id', 'department_id', )
    def onchange_approver_user(self):
        for leave in self:
            if leave.approver_user_ids:
                leave.approver_user_ids.unlink()
                leave.approved_user_ids = False
            if leave.employee_id and leave.holiday_status_id.leave_validation_type == 'by_employee_hierarchy':
                app_level = leave.holiday_status_id.approval_level
                employee = leave.employee_id
                for i in range(app_level):
                    emp = self.env['hr.employee'].search([('id', '=', employee.id)])
                    if emp:
                        parent = self.env['hr.employee'].search([('id', '=', emp.parent_id.id)])
                        employee = parent
                        if employee:
                            vals = [(0, 0, {'employee_id': employee.user_id.ids, 'leave_id': self.id})]
                            leave.approver_user_ids = vals
            elif leave.employee_id and leave.holiday_status_id.leave_validation_type == 'hr':
                responsible = leave.holiday_status_id.responsible_id
                # emp_responsible = self.env['hr.employee'].search([('user_id', '=', responsible.id)],
                #                                                  limit=1)
                hr_vals = [(0, 0, {'employee_id': responsible, 'leave_id': self.id})]
                leave.approver_user_ids = hr_vals
            elif leave.employee_id and leave.holiday_status_id.leave_validation_type == 'manager':
                emp_manager = leave.employee_id.parent_id
                manager_vals = [(0, 0, {'employee_id': emp_manager.user_id.ids, 'leave_id': self.id})]
                leave.approver_user_ids = manager_vals
            elif leave.employee_id and leave.holiday_status_id.leave_validation_type == 'both':
                emp_manager = leave.employee_id.parent_id
                both_manager_vals = [(0, 0, {'employee_id': emp_manager.user_id.ids, 'leave_id': self.id})]
                leave.approver_user_ids = both_manager_vals
                responsible = leave.holiday_status_id.responsible_id
                # emp_responsible = self.env['hr.employee'].search([('user_id', '=', responsible.id)],
                #                                                  limit=1)
                both_hr_vals = [(0, 0, {'employee_id': responsible, 'leave_id': self.id})]
                leave.approver_user_ids = both_hr_vals
            elif leave.holiday_status_id.leave_validation_type == 'by_approval_matrix':
                employee_matrix = self.env['hr.leave.approval'].search([('employee_ids', 'in', leave.employee_id.id)],
                                                                       limit=1)
                job_position_matrix = self.env['hr.leave.approval'].search(
                    [('job_ids', 'in', leave.employee_id.job_id.id)], limit=1)
                department_matrix = self.env['hr.leave.approval'].search(
                    [('department_ids', 'in', leave.employee_id.department_id.id)], limit=1)
                if employee_matrix:
                    for line in employee_matrix.leave_approvel_matrix_ids:
                        emp = [(0, 0, {'employee_id': line.approver_ids.ids, 'minimum_approver': line.minimum_approver,
                                       'leave_id': self.id})]
                        leave.approver_user_ids = emp
                elif job_position_matrix:
                    for line in job_position_matrix.leave_approvel_matrix_ids:
                        job = [(0, 0, {'employee_id': line.approver_ids.ids, 'minimum_approver': line.minimum_approver,
                                       'leave_id': self.id})]
                        leave.approver_user_ids = job
                elif department_matrix:
                    for line in department_matrix.leave_approvel_matrix_ids:
                        department = [
                            (0, 0, {'employee_id': line.approver_ids.ids, 'minimum_approver': line.minimum_approver,
                                    'leave_id': self.id})]
                        leave.approver_user_ids = department
            else:
                leave.approver_user_ids.unlink()
                leave.approved_user_ids = False

    @api.constrains('state', 'number_of_days', 'holiday_status_id')
    def _check_holidays(self):
        mapped_days = self.mapped('holiday_status_id').get_employees_days(self.mapped('employee_id').ids)
        for holiday in self:
            if holiday.holiday_type != 'employee' or not holiday.employee_id or holiday.holiday_status_id.allocation_type == 'no':
                continue
            leave_days = mapped_days[holiday.employee_id.id][holiday.holiday_status_id.id]
            # if float_compare(leave_days['remaining_leaves'], 0, precision_digits=2) == -1 or float_compare(
            #         leave_days['virtual_remaining_leaves'], 0, precision_digits=2) == -1:
            #     raise ValidationError(_('The number of remaining time off is not sufficient for this time off type.\n'
            #                             'Please also check the time off waiting for validation.'))

    def get_url(self, obj):
        url = ''
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        menu_id = self.env['ir.model.data'].get_object_reference(
            'hr_holidays', 'menu_open_department_leave_approve')[1]
        action_id = self.env['ir.model.data'].get_object_reference(
            'hr_holidays', 'hr_leave_action_action_approve_department')[1]
        url = base_url + "/web?db=" + str(self._cr.dbname) + "#id=" + str(
            obj.id) + "&view_type=form&model=hr.holidays&menu_id=" + str(
            menu_id) + "&action=" + str(action_id)
        return url

    def approver_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.approver_user_ids:
                matrix_line = sorted(rec.approver_user_ids.filtered(lambda r: r.is_approve == True))
                approver = rec.approver_user_ids[len(matrix_line)]
                for user in approver.employee_id:
                    try:
                        template_id = ir_model_data.get_object_reference(
                            'equip3_hr_holidays_extend',
                            'email_template_leave_approval_request')[1]
                    except ValueError:
                        template_id = False
                    ctx = self._context.copy()
                    url = self.get_url(self)
                    ctx.update({
                        'email_from': self.env.user.email,
                        'email_to': user.email,
                        'url': url,
                        'approver_name': user.name,
                        'emp_name': self.employee_id.name,
                        'holiday_status_name': self.holiday_status_id.name,
                    })
                    if self.request_date_from:
                        ctx.update(
                            {'date_from': fields.Datetime.from_string(self.request_date_from).strftime('%d/%m/%Y')})
                    if self.request_date_to:
                        ctx.update(
                            {'date_to': fields.Datetime.from_string(self.request_date_to).strftime('%d/%m/%Y')})
                    self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                              force_send=True)
                break

    def approved_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.approver_user_ids:
                for rec in rec.approver_user_ids.sorted(key=lambda r: r.name):
                    for user in rec.employee_id:
                        try:
                            template_id = ir_model_data.get_object_reference(
                                'equip3_hr_holidays_extend',
                                'email_template_edi_leave_request_approved')[1]
                        except ValueError:
                            template_id = False
                        ctx = self._context.copy()
                        url = self.get_url(self)
                        ctx.update({
                            'email_from': self.env.user.email,
                            'email_to': self.employee_id.user_id.email,
                            'url': url,
                            'approver_name': user.name,
                            'emp_name': self.employee_id.name,
                            'holiday_status_name': self.holiday_status_id.name,
                        })
                        if self.request_date_from:
                            ctx.update(
                                {'date_from': fields.Datetime.from_string(self.request_date_from).strftime('%d/%m/%Y')})
                        if self.request_date_to:
                            ctx.update(
                                {'date_to': fields.Datetime.from_string(self.request_date_to).strftime('%d/%m/%Y')})
                        self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                                  force_send=True)
                    break

    def reject_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.approver_user_ids:
                for rec in rec.approver_user_ids.sorted(key=lambda r: r.name):
                    for user in rec.employee_id:
                        try:
                            template_id = ir_model_data.get_object_reference(
                                'equip3_hr_holidays_extend',
                                'email_template_edi_leave_request_reject')[1]
                        except ValueError:
                            template_id = False
                        ctx = self._context.copy()
                        url = self.get_url(self)
                        ctx.update({
                            'email_from': self.env.user.email,
                            'email_to': self.employee_id.user_id.email,
                            'url': url,
                            'approver_name': user.name,
                            'emp_name': self.employee_id.name,
                            'holiday_status_name': self.holiday_status_id.name,
                        })
                        if self.request_date_from:
                            ctx.update(
                                {'date_from': fields.Datetime.from_string(self.request_date_from).strftime('%d/%m/%Y')})
                        if self.request_date_to:
                            ctx.update(
                                {'date_to': fields.Datetime.from_string(self.request_date_to).strftime('%d/%m/%Y')})
                        self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                                  force_send=True)
                    break

    def approver_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_holidays_extend.send_by_wa')
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        if send_by_wa:
            # connector = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_holidays_extend.connector_id')
            # if connector:
            #     connector_id = self.env['acrux.chat.connector'].search([('id', '=', connector)])
            #     if connector_id.ca_status:
            template = self.env.ref('equip3_hr_holidays_extend.leave_approver_wa_template')
            # url = self.get_url(self)
            if template:
                if self.approver_user_ids:
                    matrix_line = sorted(self.approver_user_ids.filtered(lambda r: r.is_approve == True))
                    approver = self.approver_user_ids[len(matrix_line)]
                    for user in approver.employee_id:
                        string_test = str(template.message)
                        if "${employee_name}" in string_test:
                            string_test = string_test.replace("${employee_name}", self.employee_id.name)
                        if "${leave_name}" in string_test:
                            string_test = string_test.replace("${leave_name}", self.holiday_status_id.name)
                        if "${start_date}" in string_test:
                            string_test = string_test.replace("${start_date}", fields.Datetime.from_string(
                                self.request_date_from).strftime('%d/%m/%Y'))
                        if "${end_date}" in string_test:
                            string_test = string_test.replace("${end_date}", fields.Datetime.from_string(
                                self.request_date_to).strftime('%d/%m/%Y'))
                        if "${approver_name}" in string_test:
                            string_test = string_test.replace("${approver_name}", user.name)
                        if "${name}" in string_test:
                            string_test = string_test.replace("${name}", self.seq_name)
                        # if "${survey_url}" in string_test:
                        #     string_test = string_test.replace("${survey_url}", url)
                        if "${br}" in string_test:
                            string_test = string_test.replace("${br}", f"\n")
                        if "${url}" in string_test:
                            string_test = string_test.replace("${url}", f"{base_url}/leave/{self.id}")
                        phone_num = str(user.mobile_phone)
                        if "+" in phone_num:
                            phone_num = int(phone_num.replace("+", ""))
                        param = {'body': string_test, 'phone': phone_num}
                        domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                        token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                        try:
                            request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                        except ConnectionError:
                            raise ValidationError("Not connect to API Chat Server. Limit reached or not active")
                                # connector_id.ca_request('post', 'sendMessage', param)

    def approved_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_holidays_extend.send_by_wa')
        if send_by_wa:
            # connector = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_holidays_extend.connector_id')
            # if connector:
            #     connector_id = self.env['acrux.chat.connector'].search([('id', '=', connector)])
            #     if connector_id.ca_status:
            template = self.env.ref('equip3_hr_holidays_extend.leave_approved_wa_template')
            # url = self.get_url(self)
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            if template:
                if self.approver_user_ids:
                    string_test = str(template.message)
                    if "${employee_name}" in string_test:
                        string_test = string_test.replace("${employee_name}", self.employee_id.name)
                    if "${leave_name}" in string_test:
                        string_test = string_test.replace("${leave_name}", self.holiday_status_id.name)
                    if "${start_date}" in string_test:
                        string_test = string_test.replace("${start_date}", fields.Datetime.from_string(
                            self.request_date_from).strftime('%d/%m/%Y'))
                    if "${end_date}" in string_test:
                        string_test = string_test.replace("${end_date}", fields.Datetime.from_string(
                            self.request_date_to).strftime('%d/%m/%Y'))
                    if "${name}" in string_test:
                        string_test = string_test.replace("${name}", self.seq_name)
                    # if "${survey_url}" in string_test:
                    #     string_test = string_test.replace("${survey_url}", url)
                    if "${br}" in string_test:
                        string_test = string_test.replace("${br}", f"\n")
                    phone_num = str(self.employee_id.mobile_phone)
                    if "+" in phone_num:
                        phone_num = int(phone_num.replace("+", ""))
                    if "${url}" in string_test:
                        string_test = string_test.replace("${url}", f"{base_url}/leave/{self.id}")
                    param = {'body': string_test, 'phone': phone_num}
                    domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                    token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                    try:
                        request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                    except ConnectionError:
                        raise ValidationError("Not connect to API Chat Server. Limit reached or not active")
                            

    def rejected_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_holidays_extend.send_by_wa')
        if send_by_wa:
            # connector = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_holidays_extend.connector_id')
            # if connector:
            #     connector_id = self.env['acrux.chat.connector'].search([('id', '=', connector)])
            #     if connector_id.ca_status:
            template = self.env.ref('equip3_hr_holidays_extend.leave_rejected_wa_template')
            # url = self.get_url(self)
            if template:
                if self.approver_user_ids:
                    string_test = str(template.message)
                    if "${employee_name}" in string_test:
                        string_test = string_test.replace("${employee_name}", self.employee_id.name)
                    if "${leave_name}" in string_test:
                        string_test = string_test.replace("${leave_name}", self.holiday_status_id.name)
                    if "${start_date}" in string_test:
                        string_test = string_test.replace("${start_date}", fields.Datetime.from_string(
                            self.request_date_from).strftime('%d/%m/%Y'))
                    if "${end_date}" in string_test:
                        string_test = string_test.replace("${end_date}", fields.Datetime.from_string(
                            self.request_date_to).strftime('%d/%m/%Y'))
                    if "${name}" in string_test:
                        string_test = string_test.replace("${name}", self.seq_name)
                    # if "${survey_url}" in string_test:
                    #     string_test = string_test.replace("${survey_url}", url)
                    if "${br}" in string_test:
                        string_test = string_test.replace("${br}", f"\n")
                    phone_num = str(self.employee_id.mobile_phone)
                    # if "+" in phone_num:
                    #     phone_num = int(phone_num.replace("+", ""))
                    param = {'body': string_test, 'phone': phone_num}
                    domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                    token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                    try:
                        request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                    except ConnectionError:
                        raise ValidationError("Not connect to API Chat Server. Limit reached or not active")
                            # connector_id.ca_request('post', 'sendMessage', param)

    def action_confirm(self):
        if self.holiday_type == 'employee':
            self.approver_wa_template()
            self.approver_mail()
            holidays = self.filtered(lambda leave: leave.validation_type == 'no_validation')
            self.write({'state': 'confirm'})
            if holidays:
                # Automatic validation should be done in sudo, because user might not have the rights to do it by himself
                holidays.sudo().action_validate()
                self.activity_update()
        else:
            self.write({'state': 'validate'})
        return True

    def action_approve(self):
        for record in self:
            current_user = self.env.uid
            if record.holiday_status_id.leave_validation_type == 'by_employee_hierarchy':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for user in record.approver_user_ids:
                            if current_user == user.employee_id.id:
                                user.is_approve = True
                                user.timestamp = fields.Datetime.now()
                                user.approver_state = 'approved'
                                if user.approval_status:
                                    app_state = user.approval_status + ',' + self.env.user.name + ':' + 'Approved'
                                    app_time = user.approved_time + ',' + self.env.user.name + ':' + str(user.timestamp)
                                else:
                                    app_state = self.env.user.name + ':' + 'Approved'
                                    app_time = self.env.user.name + ':' + str(user.timestamp)
                                user.approval_status = app_state
                                user.approved_time = app_time
                                record.approved_user_ids = [(4, current_user)]
                        matrix_line = sorted(record.approver_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            self.approved_wa_template()
                            self.approved_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Leave Request!'
                            super(HrLeave, self).action_approve()
                        else:
                            self.approver_wa_template()
                            self.approver_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Leave Request!'
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved for this Leave!'
                    ))
            elif record.holiday_status_id.leave_validation_type == 'hr':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for user in record.approver_user_ids:
                            if current_user == user.employee_id.id:
                                user.is_approve = True
                                user.timestamp = fields.Datetime.now()
                                user.approver_state = 'approved'
                                if user.approval_status:
                                    app_state = user.approval_status + ',' + self.env.user.name + ':' + 'Approved'
                                    app_time = user.approved_time + ',' + self.env.user.name + ':' + str(user.timestamp)
                                else:
                                    app_state = self.env.user.name + ':' + 'Approved'
                                    app_time = self.env.user.name + ':' + str(user.timestamp)
                                user.approval_status = app_state
                                user.approved_time = app_time
                        matrix_line = sorted(record.approver_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            self.approved_wa_template()
                            self.approved_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Leave Request!'
                            super(HrLeave, self).action_approve()
                        else:
                            self.approver_wa_template()
                            self.approver_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Leave Request!'
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved for this Leave!'
                    ))
            elif record.holiday_status_id.leave_validation_type == 'manager':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for user in record.approver_user_ids:
                            if current_user == user.employee_id.id:
                                user.is_approve = True
                                user.timestamp = fields.Datetime.now()
                                user.approver_state = 'approved'
                                if user.approval_status:
                                    app_state = user.approval_status + ',' + self.env.user.name + ':' + 'Approved'
                                    app_time = user.approved_time + ',' + self.env.user.name + ':' + str(user.timestamp)
                                else:
                                    app_state = self.env.user.name + ':' + 'Approved'
                                    app_time = self.env.user.name + ':' + str(user.timestamp)
                                user.approval_status = app_state
                                user.approved_time = app_time
                                record.approved_user_ids = [(4, current_user)]
                        matrix_line = sorted(record.approver_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            self.approved_wa_template()
                            self.approved_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Leave Request!'
                            super(HrLeave, self).action_approve()
                        else:
                            self.approver_wa_template()
                            self.approver_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Leave Request!'
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved for this Leave!'
                    ))
            elif record.holiday_status_id.leave_validation_type == 'both':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for user in record.approver_user_ids:
                            if current_user == user.employee_id.id:
                                user.is_approve = True
                                user.timestamp = fields.Datetime.now()
                                user.approver_state = 'approved'
                                if user.approval_status:
                                    app_state = user.approval_status + ',' + self.env.user.name + ':' + 'Approved'
                                    app_time = user.approved_time + ',' + self.env.user.name + ':' + str(user.timestamp)
                                else:
                                    app_state = self.env.user.name + ':' + 'Approved'
                                    app_time = self.env.user.name + ':' + str(user.timestamp)
                                user.approval_status = app_state
                                user.approved_time = app_time
                                record.approved_user_ids = [(4, current_user)]
                        matrix_line = sorted(record.approver_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            self.approved_wa_template()
                            self.approved_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Leave Request!'
                            record.write({'state': 'confirm'})
                            record.action_validate()
                        else:
                            self.approver_wa_template()
                            self.approver_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Leave Request!'
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved for this Leave!'
                    ))
            elif record.holiday_status_id.leave_validation_type == 'by_approval_matrix':
                if self.env.user not in record.approved_user_ids:
                    if record.is_approver:
                        for user in record.approver_user_ids:
                            for employee in user.employee_id:
                                if current_user == employee.id:
                                    user.timestamp = fields.Datetime.now()
                                    record.approved_user_ids = [(4, current_user)]
                                    var = len(user.approved_employee_ids) + 1
                                    if user.minimum_approver <= var:
                                        user.approver_state = 'approved'
                                        if user.approval_status:
                                            app_state = user.approval_status + ',' + self.env.user.name + ':' + 'Approved'
                                            app_time = user.approved_time + ',' + self.env.user.name + ':' + str(
                                                user.timestamp)
                                        else:
                                            app_state = self.env.user.name + ':' + 'Approved'
                                            app_time = self.env.user.name + ':' + str(user.timestamp)
                                        user.approval_status = app_state
                                        user.approved_time = app_time
                                        user.is_approve = True
                                    else:
                                        user.approver_state = 'pending'
                                        if user.approval_status:
                                            app_state = user.approval_status + ',' + self.env.user.name + ':' + 'Approved'
                                            app_time = user.approved_time + ',' + self.env.user.name + ':' + str(
                                                user.timestamp)
                                        else:
                                            app_state = self.env.user.name + ':' + 'Approved'
                                            app_time = self.env.user.name + ':' + str(user.timestamp)
                                        user.approval_status = app_state
                                        user.approved_time = app_time
                                    user.approved_employee_ids = [(4, current_user)]

                        matrix_line = sorted(record.approver_user_ids.filtered(lambda r: r.is_approve == False))
                        if len(matrix_line) == 0:
                            self.approved_wa_template()
                            self.approved_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Leave Request!'
                            record.write({'state': 'confirm'})
                            record.action_validate()
                        else:
                            self.approver_wa_template()
                            self.approver_mail()
                            record.approved_user = self.env.user.name + ' ' + 'has approved the Leave Request!'
                    else:
                        raise ValidationError(_(
                            'You are not allowed to perform this action!'
                        ))
                else:
                    raise ValidationError(_(
                        'Already approved for this Leave!'
                    ))
            else:
                super(HrLeave, self).action_approve()

    def action_refuse(self):
        for record in self:
            record.leave_balance_id.used = record.leave_balance_id.used - record.number_of_days
            for count in record.leave_count_ids:
                count.count_id.count = count.count_id.count + count.count
            record.leave_count_ids.unlink()
            for user in record.approver_user_ids:
                for employee in user.employee_id:
                    if self.env.uid == employee.id:
                        user.timestamp = fields.Datetime.now()
                        user.approver_state = 'refuse'
                        if user.approval_status:
                            app_state = user.approval_status + ',' + self.env.user.name + ':' + 'Refused'
                            app_time = user.approved_time + ',' + self.env.user.name + ':' + str(user.timestamp)
                        else:
                            app_state = self.env.user.name + ':' + 'Refused'
                            app_time = self.env.user.name + ':' + str(user.timestamp)
                        user.approval_status = app_state
                        user.approved_time = app_time
            self.rejected_wa_template()
            if not record.is_refused_by_leave_cancel_form:
                self.reject_mail()
            record.approved_user = self.env.user.name + ' ' + 'has Refused the Leave Request!'
            super(HrLeave, self).action_refuse()

    def action_cancel(self):
        for vals in self:
            return {
                "type": "ir.actions.act_window",
                "res_model": "hr.leave.cancelation",
                "view_mode": 'form',
                "view_type": 'form',
                'view_id': self.env.ref("equip3_hr_holidays_extend.view_my_leave_cancel_form").id,
                "name": "HR Leave Cancelation",
                "context": {
                    'default_employee_id': self.employee_id.id,
                    'default_leave_id': self.id,
                    'default_request_date_from': self.request_date_from,
                    'default_request_date_to': self.request_date_to,
                    'default_holiday_status_id': self.holiday_status_id.id,
                    'default_number_of_days': self.number_of_days,
                    'default_holiday_type': self.holiday_type
                },
                "target": "self",
            }

    def action_validate(self):
        leave_count = self.env['hr.leave.count'].search([('employee_id', '=', self.employee_id.id),
                                                         ('holiday_status_id', '=', self.holiday_status_id.id),
                                                         ('count', '>', 0), ('expired_date', '>=', date.today())],
                                                        order='expired_date asc')
        var_count = 0
        remaining_leaves = self.number_of_days
        for count_line in leave_count:
            if self.number_of_days > var_count:
                var_count += count_line.count
                taken_leaves = abs(remaining_leaves)
                remaining_leaves = count_line.count - abs(remaining_leaves)
                if remaining_leaves < 0:
                    count = 0
                    taken_count = count_line.count
                else:
                    count = remaining_leaves
                    taken_count = taken_leaves
                self.leave_count_ids.create({
                    'leave_id': self.id,
                    'count_id': count_line.id,
                    'count': taken_count
                })
                count_line.count = count
        leave_balance = self.env['hr.leave.balance'].search([('employee_id', '=', self.employee_id.id),
                                                             ('holiday_status_id', '=', self.holiday_status_id.id),
                                                             ('current_period', '=', self.request_date_to.year)],
                                                            limit=1)
        if leave_balance:
            self.leave_balance_id = leave_balance.id
            leave_balance.used = leave_balance.used + self.number_of_days
            result = super(HrLeave, self).action_validate()
        return True

    @api.constrains('number_of_days', 'request_date_from', 'request_date_to')
    def _check_limit_days(self):
        if self.holiday_status_id.limit_days > 0 and self.request_date_from and self.request_date_to:
            if self.holiday_status_id.limit_days < self.number_of_days:
                raise ValidationError(
                    _('You can not apply %(type)s more than %(count)s for a single request.',
                      count=self.holiday_status_id.limit_days,
                      type=self.holiday_status_id.name))

    @api.depends('date_from', 'date_to', 'employee_id')
    def _compute_number_of_days(self):
        for holiday in self:
            if holiday.date_from and holiday.date_to and holiday.holiday_status_id.day_count == 'calendar_day':
                delta = (holiday.date_to - holiday.date_from).days  # as timedelta
                holiday.number_of_days = delta + 1
            elif holiday.request_date_from and holiday.request_date_to and holiday.holiday_status_id.day_count == 'work_day':
                # if holiday.employee_id.contract_id.resource_calendar_id.schedule != 'fixed_schedule':
                start_leave = holiday.request_date_from
                duration_list = []
                holiday_list = []
                while start_leave <= holiday.request_date_to:
                    for leave in holiday.employee_id.contract_id.resource_calendar_id.global_leave_ids:
                        holiday_start_date = leave.date_from
                        holiday_end_date = leave.date_to
                        while holiday_start_date <= holiday_end_date:
                            holiday_list.append(holiday_start_date)
                            holiday_start_date += relativedelta(days=1)
                    if start_leave not in holiday_list and start_leave.weekday() not in (5, 6):
                        duration_list.append(start_leave)
                    start_leave += relativedelta(days=1)
                holiday.number_of_days = len(duration_list)
            # else:
            #     holiday.number_of_days = \
            #         holiday._get_number_of_days(holiday.date_from, holiday.date_to, holiday.employee_id.id)['days']

            else:
                holiday.number_of_days = 0

    @api.depends('employee_id')
    def _compute_cancel(self):
        for leave in self:
            cancel = self.env['hr.leave.cancelation'].search([('leave_id', '=', leave.id)], limit=1)
            if cancel:
                leave.cancel_id = cancel.id
            else:
                leave.cancel_id = False

    def name_get(self):
        res = []
        for leave in self:
            res.append((
                leave.id,
                _("%(sequence)s",
                  sequence=leave.seq_name,
                  )
            ))
        return res

    @api.constrains('number_of_days', 'request_date_from', 'request_date_to')
    def _check_leave_balance(self):
        for leave in self:
            if leave.holiday_type == 'employee':
                check_period = leave.request_date_from.strftime("%Y")
                leave_balance = self.env['hr.leave.balance'].search(
                    [('employee_id', '=', leave.employee_id.id), ('holiday_status_id', '=', leave.holiday_status_id.id),
                     ('current_period', '=', str(check_period))], limit=1)
                available_leave = 0
                if leave.holiday_status_id.allow_minus:
                    available_leave = available_leave + float(
                        leave_balance.remaining) + leave.holiday_status_id.maximum_minus
                else:
                    available_leave = available_leave + float(leave_balance.remaining)
                if not leave_balance:
                    raise ValidationError(
                        _('Please ask the responsible user in order to allocate your leaves!'))
                elif leave_balance and leave.number_of_days > available_leave:
                    raise ValidationError(
                        _('You can not request leave more than leave balance.'))
            else:
                continue

    @api.constrains('holiday_status_id')
    def _check_leave_minimum_days(self):
        for leave in self:
            if leave.holiday_status_id.minimum_days_before and leave.holiday_status_id.day_count == 'calendar_day':
                minimum_days = date.today() + relativedelta(days=leave.holiday_status_id.minimum_days_before)
                if leave.request_date_from < minimum_days:
                    raise ValidationError(
                        _('You only able to request leave minimum %s days before the leave date') % (
                            leave.holiday_status_id.minimum_days_before))
            elif leave.holiday_status_id.minimum_days_before and leave.holiday_status_id.day_count == 'work_day':
                start_leave = date.today()
                weekend = 0
                while start_leave <= leave.request_date_to:
                    if start_leave.weekday() in (5, 6):
                        weekend += 1
                    start_leave += relativedelta(days=1)
                minimum_days_weekend = date.today() + relativedelta(days=leave.holiday_status_id.minimum_days_before)
                minimum_days = minimum_days_weekend + relativedelta(days=weekend)
                if leave.request_date_from < minimum_days:
                    raise ValidationError(
                        _('You only able to request leave minimum %s days before the leave date') % (
                            leave.holiday_status_id.minimum_days_before))
            if leave.holiday_status_id.allow_past_date and leave.holiday_status_id.day_count == 'calendar_day':
                past_days = date.today() - relativedelta(days=leave.holiday_status_id.past_days)
                if leave.request_date_from < past_days:
                    raise ValidationError(
                        _('You can only able for request leave for the last %s days') % (
                            leave.holiday_status_id.past_days))
            elif leave.holiday_status_id.allow_past_date and leave.holiday_status_id.day_count == 'work_day':
                start_leave = leave.request_date_from
                weekend = 0
                while start_leave <= date.today():
                    if start_leave.weekday() in (5, 6):
                        weekend += 1
                    start_leave += relativedelta(days=1)
                minimum_days_weekend = date.today() - relativedelta(days=leave.holiday_status_id.past_days)
                past_days = minimum_days_weekend - relativedelta(days=weekend)
                if leave.request_date_from < past_days:
                    raise ValidationError(
                        _('You can only able for request leave for the last %s days ') % (
                            leave.holiday_status_id.past_days))
            elif not leave.holiday_status_id.allow_past_date:
                if leave.request_date_from < date.today():
                    raise ValidationError(_('You cannot request leave for past dated'))
            if leave.holiday_status_id.gender and leave.holiday_status_id.gender != leave.employee_id.gender:
                raise ValidationError(_('Selected Employee and Leave Type Gender is different'))

    @api.model
    def default_get(self, fields_list):
        defaults = super(HrLeave, self).default_get(fields_list)
        defaults = self._default_get_request_parameters(defaults)
        defaults['holiday_status_id'] = False
        if 'holiday_status_id' in fields_list and not defaults.get('holiday_status_id'):
            for lt in self.employee_id.leave_struct_id.leaves_ids:
                if lt:
                    defaults['holiday_status_id'] = lt.id
        if 'state' in fields_list and not defaults.get('state'):
            lt = self.env['hr.leave.type'].browse(defaults.get('holiday_status_id'))
            defaults['state'] = 'confirm' if lt and lt.leave_validation_type != 'no_validation' else 'draft'
        now = fields.Datetime.now()
        if 'date_from' not in defaults:
            defaults.update({'date_from': now})
        if 'date_to' not in defaults:
            defaults.update({'date_to': now})
        return defaults

    # @api.onchange('holiday_type', 'employee_id')
    # def onchange_domain_holiday_type(self):
    #     res = {}
    #     holiday_list = []
    #     if self.holiday_type == 'employee':
    #         for vals in self.employee_id.leave_struct_id.leaves_ids:
    #             holiday_list.append(vals.id)
    #             res['domain'] = {'holiday_status_id': [('id', 'in', holiday_list)]}
    #     else:
    #         res['domain'] = {'holiday_status_id': []}
    #     return res

    @api.onchange('employee_id', 'holiday_status_id')
    def onchange_emp_in_type(self):
        for rec in self:
            if rec.holiday_type == 'employee':
                leave_type_master = self.env['hr.leave.type'].search([])
                leave_type_master.update({
                    'employee_id': rec.employee_id,
                })
            else:
                leave_type_master = self.env['hr.leave.type'].search([])
                leave_type_master.update({
                    'employee_id': False, })

    @api.depends('employee_id', 'holiday_status_id')
    def _compute_line_items(self):
        for rec in self:
            if rec.holiday_status_id.leave_validation_type == 'no_validation':
                rec.line_item_visible = True
            else:
                rec.line_item_visible = False


class HrLeaveLine(models.Model):
    _name = 'hr.leave.line'

    leave_id = fields.Many2one('hr.leave', string="Leave")
    count_id = fields.Many2one('hr.leave.count', string="Leave Count")
    count = fields.Float(string="Count")


class LeaveApproverUser(models.Model):
    _name = 'leave.approver.user'

    leave_id = fields.Many2one('hr.leave', string="Leave")
    name = fields.Integer('Sequence', compute="fetch_sl_no")
    employee_id = fields.Many2many('res.users', string="Approvers")
    approved_employee_ids = fields.Many2many('res.users', 'approved_users_rel', string="Approved user")
    minimum_approver = fields.Integer(string="Minimum Approver", default=1)
    timestamp = fields.Datetime(string="Timestamp")
    approved_time = fields.Char(string="Timestamp")
    approver_state = fields.Selection([('draft', 'Draft'), ('pending', 'Pending'), ('approved', 'Approved'),
                                       ('refuse', 'Refused')], default='draft', string="Approval Status")
    approval_status = fields.Text(string="Approval Status")
    is_approve = fields.Boolean(string="Is Approve", default=False)

    @api.depends('leave_id')
    def fetch_sl_no(self):
        sl = 0
        for line in self.leave_id.approver_user_ids:
            sl = sl + 1
            line.name = sl


class LeaveReport(models.Model):
    _inherit = 'hr.leave.report'

    @api.model
    def action_leave_analysis(self):
        domain = [('holiday_type', '=', 'employee')]

        if self.env.context.get('active_ids'):
            domain = expression.AND([
                domain,
                [('employee_id', 'in', self.env.context.get('active_ids', []))]
            ])

        return {
            'name': _('Leave Analysis'),
            'type': 'ir.actions.act_window',
            'res_model': 'hr.leave.report',
            'view_mode': 'tree,pivot,form',
            'search_view_id': self.env.ref('hr_holidays.view_hr_holidays_filter_report').id,
            'domain': domain,
            'context': {
                'search_default_group_type': True,
                'search_default_year': True,
                'search_default_validated': True,
            }
        }
