# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import hr_employee
from . import leave_allocation
from . import leave_type
from . import leave_struct
from . import hr_leave
from . import leave_cancel
from . import leave_balance
from . import generate_allocations
from . import leave_count
from . import approval_matrix
from . import res_config_settings
from . import hr_leave_report_calendar
from . import hr_leave_replace_write
