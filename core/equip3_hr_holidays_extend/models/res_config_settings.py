# -*- coding: utf-8 -*-

from odoo import fields, models, api


class ResConfigSettingsInherit(models.TransientModel):
    _inherit = 'res.config.settings'

    connector_id = fields.Many2one('acrux.chat.connector', config_parameter='equip3_hr_holidays_extend.connector_id')
    send_by_wa = fields.Boolean(config_parameter='equip3_hr_holidays_extend.send_by_wa')
    send_by_email = fields.Boolean(config_parameter='equip3_hr_holidays_extend.send_by_email', default=True)
