# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from lxml import etree


class HrLeaveApprovalMatrix(models.Model):
    _name = "hr.leave.approval"

    name = fields.Char(string='Name')
    # sequence = fields.Integer('Sequence')
    level = fields.Integer('Level', compute='_compute_get_level')
    mode_type = fields.Selection([
        ('employee', 'By Employee'),
        ('job_position', 'By Job Position'),
        ('department', 'By Department')],
        string='Mode', help='Leave Request /Leave Cancel', tracking=True)
    employee_ids = fields.Many2many('hr.employee', string='Employee', tracking=True)
    department_ids = fields.Many2many('hr.department', string='Department', tracking=True)
    job_ids = fields.Many2many('hr.job', string='Job Position', tracking=True)
    leave_approvel_matrix_ids = fields.One2many('hr.leave.approval.line', 'leave_approvel_matrix_line_id',
                                                string='Approver')
    company_id = fields.Many2one('res.company', string='Company',
                                 default=lambda self: self.env.company)

    def _compute_get_level(self):
        if self:
            for rec in self:
                approval_line = self.env['hr.leave.approval.line'].search(
                    [('leave_approvel_matrix_line_id', '=', rec.id)])
                rec.level = len(approval_line.ids)
    @api.model
    def fields_view_get(self, view_id=None, view_type=None,
                        toolbar=True, submenu=True):
        res = super(HrLeaveApprovalMatrix, self).fields_view_get(
            view_id=view_id, view_type=view_type,toolbar=toolbar,submenu=submenu)
        if  not self.env.user.has_group('hr_holidays.group_hr_holidays_user'):
            root = etree.fromstring(res['arch'])
            root.set('create', 'false')
            root.set('edit', 'false')
            root.set('delete', 'false')
            res['arch'] = etree.tostring(root)
            
        return res

class HrLeaveApprovalMatrixLine(models.Model):
    _name = 'hr.leave.approval.line'

    leave_approvel_matrix_line_id = fields.Many2one('hr.leave.approval', string="Matrix Line")
    name = fields.Integer('Sequence', compute="fetch_sl_no")
    approver_ids = fields.Many2many('res.users', string="Approvers")
    minimum_approver = fields.Integer(string="Minimum Approver", default="1")

    @api.depends('leave_approvel_matrix_line_id')
    def fetch_sl_no(self):
        sl = 0
        for line in self.leave_approvel_matrix_line_id.leave_approvel_matrix_ids:
            sl = sl + 1
            line.name = sl
