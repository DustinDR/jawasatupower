from odoo import api, fields, models, _


class MyLeaveCancelation(models.Model):
    _name = 'hr.leave.balance'
    _description = 'My Leave Balance'
    _inherit = ['mail.thread']

    employee_id = fields.Many2one('hr.employee', string='Employee', tracking=True)
    state = fields.Selection([('draft', 'To Submit'), ('confirm', 'To Approve'), ('validate', 'Approved')],
                             string='Status', default='draft', tracking=True)
    holiday_status_id = fields.Many2one("hr.leave.type", string="Leave Type")
    code = fields.Char('Code', tracking=True)
    leave_entitlement = fields.Float('Entitlement', tracking=True)
    assigned = fields.Char('Assigned', tracking=True)
    used = fields.Char('Used', tracking=True)
    carry_forward = fields.Selection(
        [('none', 'None'), ('remaining_amount', 'Remaining Amount'), ('specific_days', 'Specific Days')],
        string='Carry Forward', tracking=True)
    bring_forward = fields.Char('Bring Forward', tracking=True)
    carry_forward_expired = fields.Selection([
        ('january', 'January'),
        ('february', 'February'),
        ('march', 'March'),
        ('april', 'April'),
        ('may', 'May'),
        ('june', 'June'),
        ('july', 'July'),
        ('august', 'August'),
        ('september', 'September'),
        ('october', 'October'),
        ('november', 'November'),
        ('december', 'December')], 'Carry Forward Expired', tracking=True)
    request_date_from = fields.Date('Start Valid Date', tracking=True)
    request_date_to = fields.Date('End Valid Date', tracking=True)
    current_period = fields.Char('Current Period', tracking=True)
    remaining = fields.Char('Remaining', tracking=True)
