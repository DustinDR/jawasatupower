from datetime import datetime, timedelta
from datetime import date
from attr import field
from pytz import timezone
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError
from dateutil.relativedelta import relativedelta
from odoo.tools.safe_eval import safe_eval
import requests
headers = {'content-type': 'application/json'}

class HrLeaveAllocation(models.Model):
    _inherit = 'hr.leave.allocation'
    _description = "Hr Leave Allocation"

    state = fields.Selection([
        ('draft', 'To Submit'),
        ('cancel', 'Cancelled'),
        ('confirm', 'To Approve'),
        ('refuse', 'Refused'),
        ('validate1', 'Second Approval'),
        ('validate', 'Approved')
    ], string='Status', readonly=True, tracking=True, copy=False, default='draft',
        help="The status is set to 'To Submit', when an allocation request is created." +
             "\nThe status is 'To Approve', when an allocation request is confirmed by user." +
             "\nThe status is 'Refused', when an allocation request is refused by manager." +
             "\nThe status is 'Approved', when an allocation request is approved by manager.")
    holiday_status_id = fields.Many2one(
        "hr.leave.type", string="Leave Type", required=True,
        readonly=False,
        default=False,
        states={'cancel': [('readonly', True)], 'refuse': [('readonly', True)], 'validate1': [('readonly', True)],
                'validate': [('readonly', True)]},
        domain="[('leave_method', '=', 'none'),('valid', '=', True)]")
    approver_user_ids = fields.One2many('allocation.approver.user', 'leave_id', string='Approver')
    is_hide_approve = fields.Boolean(compute='_compute_approver', default=True)
    approvers_ids = fields.Many2many('res.users', 'allocation_approver_users_rel', string='Approvers')
    allocation_type_by = fields.Selection([
        ('overtime', 'By Overtime'),
        ('dates', 'By Dates')
    ], string='Allocation Type', default='dates')
    overtime_id = fields.Many2one('hr.overtime.actual', string='Overtime')
    allocation_date_from = fields.Date('Allocation Start Date')
    allocation_date_to = fields.Date('Allocation End Date')
    allocation_date_from_period = fields.Selection([
        ('morning', 'Morning'), ('afternoon', 'Afternoon'), ('evening', 'Evening')],
        string="Date Period Start", default='morning')
    allocation_half_day = fields.Boolean('Half Day')
    effective_date = fields.Date('Effective Date', default=fields.Date.context_today)
    sequence = fields.Char('Name')

    @api.model
    def create(self, vals):
        sequence = self.env['ir.sequence'].next_by_code('hr.leave.allocation')
        vals.update({'sequence': sequence})
        result = super(HrLeaveAllocation, self).create(vals)
        if result.allocation_type_by == 'overtime' and result.holiday_status_id.overtime_extra_leave == False:
            raise ValidationError(_("Selected leave types not allow to overtime extra leave"))
        return result
    
    def unlink(self):
        for res in self:
            if res.state in ['validate']:
                raise UserError(_('You can not delete if already approve'))
        return super(HrLeaveAllocation, self).unlink()

    def _check_approval_update(self, state):
        """ Check if target state is achievable. """
        if self.env.is_superuser():
            return
        current_employee = self.env.user.employee_id
        if not current_employee:
            return
        is_officer = self.env.user.has_group('hr_holidays.group_hr_holidays_user')
        is_manager = self.env.user.has_group('hr_holidays.group_hr_holidays_manager')
        for holiday in self:
            val_type = holiday.holiday_status_id.sudo().allocation_validation_type
            if state == 'confirm':
                continue

            if state == 'draft':
                if holiday.employee_id != current_employee and not is_manager:
                    raise UserError(_('Only a time off Manager can reset other people allocation.'))
                continue

            # if not is_officer and self.env.user != holiday.employee_id.leave_manager_id:
            #     raise UserError(_('Only a time off Officer/Responsible or Manager can approve or refuse time off requests.'))

            if is_officer or self.env.user == holiday.employee_id.leave_manager_id:
                # use ir.rule based first access check: department, members, ... (see security.xml)
                holiday.check_access_rule('write')

            # if holiday.employee_id == current_employee and not is_manager:
            #     raise UserError(_('Only a time off Manager can approve its own requests.'))

            # if (state == 'validate1' and val_type == 'both') or (state == 'validate' and val_type == 'manager'):
            #     if self.env.user == holiday.employee_id.leave_manager_id and self.env.user != holiday.employee_id.user_id:
            #         continue
            #     manager = holiday.employee_id.parent_id or holiday.employee_id.department_id.manager_id
            #     if (manager != current_employee) and not is_manager:
            #         raise UserError(_('You must be either %s\'s manager or time off manager to approve this time off') % (holiday.employee_id.name))
            #
            # if state == 'validate' and val_type == 'both':
            #     if not is_officer:
            #         raise UserError(_('Only a Time off Approver can apply the second approval on allocation requests.'))


    @api.constrains('employee_id', 'holiday_status_id')
    def _check_leave_gender(self):
        for leave in self:
            if leave.holiday_status_id.gender:
                if leave.employee_id.gender != leave.holiday_status_id.gender:
                    raise ValidationError(_('Selected Employee and Leave Type Gender is different!'))
            else:
                continue

    def get_url(self, obj):
        url = ''
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        menu_id = self.env['ir.model.data'].get_object_reference(
            'hr_holidays', 'hr_holidays_menu_manager_approve_allocations')[1]
        action_id = self.env['ir.model.data'].get_object_reference(
            'hr_holidays', 'hr_leave_allocation_action_approve_department')[1]
        url = base_url + "/web?db=" + str(self._cr.dbname) + "#id=" + str(
            obj.id) + "&view_type=form&model=hr.holidays&menu_id=" + str(
            menu_id) + "&action=" + str(action_id)
        return url

    def approver_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.approver_user_ids:
                matrix_line = sorted(rec.approver_user_ids.filtered(lambda r: r.is_approve == True))
                approver = rec.approver_user_ids[len(matrix_line)]
                for user in approver.user_ids:
                    try:
                        template_id = ir_model_data.get_object_reference(
                            'equip3_hr_holidays_extend',
                            'email_template_leave_allocation_request')[1]
                    except ValueError:
                        template_id = False
                    ctx = self._context.copy()
                    url = self.get_url(self)
                    ctx.update({
                        'email_from': self.env.user.email,
                        'email_to': user.email,
                        'url': url,
                        'approver_name': user.name,
                        'emp_name': self.employee_id.name,
                        'holiday_status_name': self.holiday_status_id.name,
                        'duration': self.number_of_days_display,
                    })
                    self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                              force_send=True)
                break

    def approved_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.approver_user_ids:
                for rec in rec.approver_user_ids.sorted(key=lambda r: r.name):
                    for user in rec.user_ids:
                        try:
                            template_id = ir_model_data.get_object_reference(
                                'equip3_hr_holidays_extend',
                                'email_template_edi_leave_allocation_approved')[1]
                        except ValueError:
                            template_id = False
                        ctx = self._context.copy()
                        url = self.get_url(self)
                        ctx.update({
                            'email_from': self.env.user.email,
                            'email_to': self.employee_id.user_id.email,
                            'url': url,
                            'approver_name': user.name,
                            'emp_name': self.employee_id.name,
                            'holiday_status_name': self.holiday_status_id.name,
                            'duration': self.number_of_days_display,
                        })
                        self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                                  force_send=True)
                    break

    def reject_mail(self):
        ir_model_data = self.env['ir.model.data']
        for rec in self:
            if rec.approver_user_ids:
                for rec in rec.approver_user_ids.sorted(key=lambda r: r.name):
                    for user in rec.user_ids:
                        try:
                            template_id = ir_model_data.get_object_reference(
                                'equip3_hr_holidays_extend',
                                'email_template_edi_leave_allocation_reject')[1]
                        except ValueError:
                            template_id = False
                        ctx = self._context.copy()
                        url = self.get_url(self)
                        ctx.update({
                            'email_from': self.env.user.email,
                            'email_to': self.employee_id.user_id.email,
                            'url': url,
                            'approver_name': user.name,
                            'emp_name': self.employee_id.name,
                            'holiday_status_name': self.holiday_status_id.name,
                            'duration': self.number_of_days_display,
                        })
                        self.env['mail.template'].browse(template_id).with_context(ctx).send_mail(self.id,
                                                                                                  force_send=True)
                    break

    def approver_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_holidays_extend.send_by_wa')
        if send_by_wa:
            # connector = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_holidays_extend.connector_id')
            # if connector:
            #     connector_id = self.env['acrux.chat.connector'].search([('id', '=', connector)])
            #     if connector_id.ca_status:
            template = self.env.ref('equip3_hr_holidays_extend.leave_allocation_approver_wa_template')
            # url = self.get_url(self)
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            if template:
                if self.approver_user_ids:
                    matrix_line = sorted(self.approver_user_ids.filtered(lambda r: r.is_approve == True))
                    approver = self.approver_user_ids[len(matrix_line)]
                    for user in approver.user_ids:
                        string_test = str(template.message)
                        if "${employee_name}" in string_test:
                            string_test = string_test.replace("${employee_name}", self.employee_id.name)
                        if "${leave_name}" in string_test:
                            string_test = string_test.replace("${leave_name}", self.holiday_status_id.name)
                        if "${duration}" in string_test:
                            string_test = string_test.replace("${duration}", str(self.number_of_days_display))
                        if "${approver_name}" in string_test:
                            string_test = string_test.replace("${approver_name}", user.name)
                        if "${name}" in string_test:
                            string_test = string_test.replace("${name}", self.name)
                        # if "${survey_url}" in string_test:
                        #     string_test = string_test.replace("${survey_url}", url)
                        if "${br}" in string_test:
                            string_test = string_test.replace("${br}", f"\n")
                        phone_num = str(user.mobile_phone)
                        if "+" in phone_num:
                            phone_num = int(phone_num.replace("+", ""))
                        if "${url}" in string_test:
                            string_test = string_test.replace("${url}", f"{base_url}/allocation/{self.id}")
                        param = {'body': string_test, 'phone': phone_num}
                        domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                        token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                        try:
                            request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                        except ConnectionError:
                            raise ValidationError("Not connect to API Chat Server. Limit reached or not active")
                        

    def approved_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_holidays_extend.send_by_wa')
        if send_by_wa:
            # connector = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_holidays_extend.connector_id')
            # if connector:
            #     connector_id = self.env['acrux.chat.connector'].search([('id', '=', connector)])
            #     if connector_id.ca_status:
            template = self.env.ref('equip3_hr_holidays_extend.leave_allocation_approved_wa_template')
            # url = self.get_url(self)
            if template:
                if self.approver_user_ids:
                    string_test = str(template.message)
                    if "${employee_name}" in string_test:
                        string_test = string_test.replace("${employee_name}", self.employee_id.name)
                    if "${leave_name}" in string_test:
                        string_test = string_test.replace("${leave_name}", self.holiday_status_id.name)
                    if "${start_date}" in string_test:
                        string_test = string_test.replace("${start_date}", fields.Datetime.from_string(
                            self.request_date_from).strftime('%d/%m/%Y'))
                    if "${end_date}" in string_test:
                        string_test = string_test.replace("${end_date}", fields.Datetime.from_string(
                            self.request_date_to).strftime('%d/%m/%Y'))
                    if "${name}" in string_test:
                        string_test = string_test.replace("${name}", self.name)
                    # if "${survey_url}" in string_test:
                    #     string_test = string_test.replace("${survey_url}", url)
                    if "${br}" in string_test:
                        string_test = string_test.replace("${br}", f"\n")
                    phone_num = str(self.employee_id.mobile_phone)
                    # if "+" in phone_num:
                    #     phone_num = int(phone_num.replace("+", ""))
                    param = {'body': string_test, 'phone': phone_num}
                    domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                    token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                    try:
                        request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                    except ConnectionError:
                        raise ValidationError("Not connect to API Chat Server. Limit reached or not active")

    def rejected_wa_template(self):
        send_by_wa = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_holidays_extend.send_by_wa')
        if send_by_wa:
            # connector = self.env['ir.config_parameter'].sudo().get_param('equip3_hr_holidays_extend.connector_id')
            # if connector:
            #     connector_id = self.env['acrux.chat.connector'].search([('id', '=', connector)])
            #     if connector_id.ca_status:
            template = self.env.ref('equip3_hr_holidays_extend.leave_allocation_rejected_wa_template')
            # url = self.get_url(self)
            if template:
                if self.approver_user_ids:
                    string_test = str(template.message)
                    if "${employee_name}" in string_test:
                        string_test = string_test.replace("${employee_name}", self.employee_id.name)
                    if "${leave_name}" in string_test:
                        string_test = string_test.replace("${leave_name}", self.holiday_status_id.name)
                    if "${start_date}" in string_test:
                        string_test = string_test.replace("${start_date}", fields.Datetime.from_string(
                            self.request_date_from).strftime('%d/%m/%Y'))
                    if "${end_date}" in string_test:
                        string_test = string_test.replace("${end_date}", fields.Datetime.from_string(
                            self.request_date_to).strftime('%d/%m/%Y'))
                    if "${name}" in string_test:
                        string_test = string_test.replace("${name}", self.name)
                    # if "${survey_url}" in string_test:
                    #     string_test = string_test.replace("${survey_url}", url)
                    if "${br}" in string_test:
                        string_test = string_test.replace("${br}", f"\n")
                    phone_num = str(self.employee_id.mobile_phone)
                    # if "+" in phone_num:
                    #     phone_num = int(phone_num.replace("+", ""))
                    param = {'body': string_test, 'phone': phone_num}
                    domain = self.env['ir.config_parameter'].sudo().get_param('chat.api.url')
                    token = self.env['ir.config_parameter'].sudo().get_param('chat.api.token')
                    try:
                        request_server = requests.post(f'{domain}/sendMessage?token={token}', params=param,headers=headers,verify=True)
                    except ConnectionError:
                        raise ValidationError("Not connect to API Chat Server. Limit reached or not active")

    def action_confirm(self):
        if self.filtered(lambda holiday: holiday.state != 'draft'):
            raise UserError(_('Allocation request must be in Draft state ("To Submit") in order to confirm it.'))
        self.approver_wa_template()
        self.approver_mail()
        res = self.write({'state': 'confirm'})
        self.activity_update()
        return res

    def action_refuse(self):
        now = datetime.now(timezone(self.env.user.tz))
        lines = self.approver_user_ids.filtered(lambda line: self.env.user.id in line.user_ids.ids)
        for approval_line in lines:
            if approval_line:
                format_timestamp = f"{self.env.user.name}:{now.day}/{now.month}/{now.year} {now.hour}:{now.minute}:{now.second}"
                format_approve = f"{self.env.user.name}:Refuse"
                approval_line.approved_user_ids = [(4, self.env.user.id)]
                if not approval_line.approved_time:
                    approval_line.approved_time = format_timestamp
                else:
                    timestamp_list = []
                    timestamp_list.append(approval_line.timestamp)
                    approval_line.approved_time = "\n".join(format_timestamp)
                if not approval_line.approval_status:
                    approval_line.approval_status = format_approve
                else:
                    approve_list = []
                    approve_list.append(approval_line.approval_status)
                    approval_line.approval_status = "\n".join(format_approve)

        current_employee = self.env.user.employee_id
        if any(holiday.state not in ['confirm', 'validate', 'validate1'] for holiday in self):
            raise UserError(_('Allocation request must be confirmed or validated in order to refuse it.'))
        validated_holidays = self.filtered(lambda hol: hol.state == 'validate1')
        validated_holidays.write({'state': 'refuse', 'first_approver_id': current_employee.id})
        (self - validated_holidays).write({'state': 'refuse', 'second_approver_id': current_employee.id})
        # If a category that created several holidays, cancel all related
        linked_requests = self.mapped('linked_request_ids')
        if linked_requests:
            linked_requests.action_refuse()
        self.rejected_wa_template()
        self.reject_mail()
        self.activity_update()
        return True

    def action_approve(self):
        now = datetime.now(timezone(self.env.user.tz))
        for record in self:
            seq_approve = [seq.name for seq in record.approver_user_ids]
            max_seq = max(seq_approve)
            lines = record.approver_user_ids.filtered(lambda line: self.env.user.id in line.user_ids.ids)
            for approval_line in lines:
                if approval_line:
                    format_timestamp = f"{self.env.user.name}:{now.day}/{now.month}/{now.year} {now.hour}:{now.minute}:{now.second}"
                    format_approve = f"{self.env.user.name}:Approved"
                    approval_line.approved_user_ids = [(4, self.env.user.id)]
                    approval_line.is_approve = True
                    if not approval_line.approved_time:
                        approval_line.approved_time = format_timestamp
                    else:
                        timestamp_list = []
                        timestamp_list.append(approval_line.timestamp)
                        approval_line.approved_time = "\n".join(format_timestamp)
                    if not approval_line.approval_status:
                        approval_line.approval_status = format_approve
                    else:
                        approve_list = []
                        approve_list.append(approval_line.approval_status)
                        approval_line.approval_status = "\n".join(format_approve)

            if any(holiday.state != 'confirm' for holiday in record):
                raise UserError(_('Allocation request must be confirmed ("To Approve") in order to approve it.'))
            current_employee = self.env.user.employee_id
            if record.holiday_status_id.allocation_type == 'fixed_allocation' \
                    and record.holiday_status_id.allocation_validation_type == 'both':
                record.write({'state': 'validate1', 'first_approver_id': current_employee.id})
                self.approver_wa_template()
                self.approver_mail()
            else:
                record.action_validate()
            record.activity_update()

    def action_validate(self):
        current_employee = self.env.user.employee_id
        for holiday in self:
            if holiday.state not in ['confirm', 'validate1']:
                raise UserError(_('Allocation request must be confirmed in order to approve it.'))

            holiday.write({'state': 'validate'})
            if holiday.validation_type == 'both':
                holiday.write({'second_approver_id': current_employee.id})
            else:
                holiday.write({'first_approver_id': current_employee.id})

            holiday._action_validate_create_childs()
            leave_balance_active = self.env['hr.leave.balance'].search([('employee_id', '=', holiday.employee_id.id),
                                                                        ('holiday_status_id', '=', holiday.holiday_status_id.id),
                                                                        ('current_period', '=', date.today().year),
                                                                        ('active', '=', True)], limit=1)
            if leave_balance_active:
                if holiday.holiday_status_id.repeated_allocation == True:
                    assigned = leave_balance_active.assigned + holiday.number_of_days_display
                    leave_balance_active.write({'assigned': assigned})
            else:
                self.env['hr.leave.balance'].create({'employee_id': holiday.employee_id.id,
                                                    'holiday_status_id': holiday.holiday_status_id.id,
                                                    'assigned': holiday.number_of_days_display,
                                                    'current_period': date.today().year,
                                                    'start_date': date.today(),
                                                    'hr_years': date.today().year,
                                                    'description': "Leave Allocation Request"
                                                    })
            self.env['hr.leave.count'].create({'employee_id': holiday.employee_id.id,
                                            'holiday_status_id': holiday.holiday_status_id.id,
                                            'count': holiday.number_of_days_display,
                                            'expired_date': date.today() + timedelta(
                                                days=int(holiday.holiday_status_id.expiry_days)),
                                            'start_date': holiday.effective_date,
                                            'hr_years': date.today().year,
                                            'description': "Leave Allocation Request"
                                            })
            if holiday.allocation_type_by == 'overtime' and holiday.overtime_id:
                holiday.overtime_id.state = 'convert_as_leave'
        self.approved_wa_template()
        self.approved_mail()
        self.activity_update()
        return True

    def _compute_approver(self):
        for holiday in self:
            current_user = holiday.env.user
            matrix_line = sorted(holiday.approver_user_ids.filtered(lambda r: r.is_approve == True))
            app = len(matrix_line)
            a = len(holiday.approver_user_ids)
            if app < 2 and app < a:
                if current_user in holiday.approver_user_ids[len(matrix_line)].user_ids:
                    holiday.is_hide_approve = False
                else:
                    holiday.is_hide_approve = True
            else:
                holiday.is_hide_approve = True

    def approve_by_matrix(self):
        pass

    @api.onchange('employee_id', 'holiday_status_id')
    def onchange_approver(self):
        for leave_cancel in self:
            app_list = []
            if leave_cancel.employee_id and leave_cancel.holiday_status_id.allocation_type != 'fixed_allocation':
                responsible = leave_cancel.holiday_status_id.responsible_id
                if responsible:
                    app_list.append(responsible.id)
            else:
                if leave_cancel.employee_id and leave_cancel.holiday_status_id.allocation_validation_type == 'hr':
                    responsible = leave_cancel.holiday_status_id.responsible_id
                    if responsible:
                        app_list.append(responsible.id)
                elif leave_cancel.employee_id and leave_cancel.holiday_status_id.allocation_validation_type == 'manager':
                    emp_manager = leave_cancel.employee_id.parent_id.user_id
                    if emp_manager:
                        app_list.append(emp_manager.id)
                elif leave_cancel.employee_id and leave_cancel.holiday_status_id.allocation_validation_type == 'both':
                    both_emp_manager = leave_cancel.employee_id.parent_id.user_id
                    both_responsible = leave_cancel.holiday_status_id.responsible_id
                    if both_emp_manager:
                        app_list.append(both_emp_manager.id)
                    if both_responsible:
                        app_list.append(both_responsible.id)
            leave_cancel.approvers_ids = app_list

    @api.onchange('holiday_status_id', 'employee_id')
    def _onchange_holiday_status_id(self):
        for record in self:
            if record.holiday_status_id:
                if record.approver_user_ids:
                    line_remove = []
                    for line in record.approver_user_ids:
                        line_remove.append((2, line.id))
                    record.approver_user_ids = line_remove
                if record.holiday_status_id.allocation_type != 'fixed_allocation':
                    if not record.holiday_status_id.responsible_id:
                        raise ValidationError(f"Responsible not set in leave types {record.holiday_status_id.name}")
                    line_data = [(0, 0, {'user_ids': [(4, record.holiday_status_id.responsible_id.id)]})]
                    record.approver_user_ids = line_data
                elif record.holiday_status_id.allocation_type == 'fixed_allocation':
                    if record.holiday_status_id.allocation_validation_type == 'hr':
                        if not record.holiday_status_id.responsible_id:
                            raise ValidationError(f"Responsible not set in leave types {record.holiday_status_id.name}")
                        line_data = [(0, 0, {'user_ids': [(4, record.holiday_status_id.responsible_id.id)]})]
                        record.approver_user_ids = line_data
                    if record.holiday_status_id.allocation_validation_type == 'manager':
                        if not record.employee_id.parent_id:
                            raise ValidationError(f"Manager not set in Employee {record.employee_id.name}")
                        if not record.employee_id.parent_id.user_id:
                            raise ValidationError(f"User not set in Employee {record.employee_id.parent_id.name}")
                        line_data = [(0, 0, {'user_ids': [(4, record.employee_id.parent_id.user_id.id)]})]
                        record.approver_user_ids = line_data
                    if record.holiday_status_id.allocation_validation_type == 'both':
                        if not record.employee_id.parent_id:
                            raise ValidationError(f"Manager not set in Employee {record.employee_id.name}")
                        if not record.employee_id.parent_id.user_id:
                            raise ValidationError(f"User not set in Employee {record.employee_id.parent_id.name}")
                        if not record.holiday_status_id.responsible_id:
                            raise ValidationError(f"Responsible not set in leave types {record.holiday_status_id.name}")
                        line_data = [(0, 0, {'user_ids': [(4, record.employee_id.parent_id.user_id.id)]})]
                        line_data.append((0, 0, {'user_ids': [(4, record.holiday_status_id.responsible_id.id)]}))
                        record.approver_user_ids = line_data
        # for record in self:
        #     if record.holiday_status_id:
        #         if record.approver_user_ids:
        #             line_remove = []
        #             for line in record.approver_user_ids:
        #                 line_remove.append((2, line.id))
        #             record.approver_user_ids = line_remove
        #         if record.holiday_status_id.allocation_type != 'fixed_allocation':
        #             if not record.employee_id.parent_id:
        #                 raise ValidationError(f"Manager not set in Employee {record.employee_id.name}")
        #             if not record.employee_id.parent_id.user_id:
        #                 raise ValidationError(f"User not set in Employee {record.employee_id.parent_id.name}")
        #             if not record.holiday_status_id.responsible_id:
        #                 raise ValidationError(f"Responsible not set in leave types {record.holiday_status_id.name}")
        #             line_data = [(0, 0, {'user_ids': [(4, record.employee_id.parent_id.user_id.id)]})]
        #             line_data.append((0, 0, {'user_ids': [(4, record.holiday_status_id.responsible_id.id)]}))
        #             record.approver_user_ids = line_data
                leave_balance_active = self.env['hr.leave.balance'].search([('employee_id', '=', record.employee_id.id),
                                                                            ('holiday_status_id', '=',
                                                                             record.holiday_status_id.id),
                                                                            ('active', '=', True)])
                if leave_balance_active and record.holiday_status_id.repeated_allocation == False:
                    raise ValidationError(_('Already allocated for this leave type, please choose another leave type.'))

    @api.onchange('allocation_half_day', 'allocation_date_from', 'allocation_date_to')
    def onchange_allocation_half_day(self):
        for res in self:
            if res.allocation_half_day:
                res.number_of_days_display = 0.5
            else:
                if res.allocation_date_from and res.allocation_date_to:
                    d1 = datetime.strptime(str(res.allocation_date_from), "%Y-%m-%d")
                    d2 = datetime.strptime(str(res.allocation_date_to), "%Y-%m-%d")
                    diff_day =  abs((d2 - d1).days)
                    res.number_of_days_display = diff_day + 1
                else:
                    res.number_of_days_display = 1.0

    @api.onchange('holiday_status_id', 'employee_id')
    def _onchange_domain_overtime(self):
        if self.holiday_status_id:
            period = date.today() + relativedelta(days=self.holiday_status_id.min_days_before_alloc)
        else:
            period = date.today()
        if self.employee_id:
            employee = self.employee_id.id
        else:
            employee = -1
        domain = {'domain': {'overtime_id': [('period_start', '>=', period),('employee_id', '=', employee),('applied_to','=','extra_leave')]}}
        return domain

    @api.onchange('overtime_id', 'holiday_status_id')
    def onchange_overtime_id(self):
        for res in self:
            if res.holiday_status_id and res.overtime_id:
                res.number_of_days_display = 0.0
                formula = res.holiday_status_id.formula
                if res.holiday_status_id.total_actual_hours:
                    localdict = {"actual_hours": res.overtime_id.total_actual_hours,"duration": 0.0}
                    safe_eval(formula, localdict, mode='exec', nocopy=True)
                    res.number_of_days_display = localdict['duration']
                else:
                    total_duration = 0.0
                    for line in res.overtime_id.actual_line_ids:
                        localdict = {"actual_hours": line.actual_hours,"duration": 0.0}
                        safe_eval(formula, localdict, mode='exec', nocopy=True)
                        total_duration += localdict['duration']
                    res.number_of_days_display = total_duration

class AllocationApproverUser(models.Model):
    _name = 'allocation.approver.user'
    leave_id = fields.Many2one('hr.leave.allocation', string="Leave")
    name = fields.Integer('Sequence', compute="fetch_sl_no")
    user_ids = fields.Many2many('res.users', string="Approvers")
    approved_user_ids = fields.Many2many('res.users', 'approved_users_allocation_rel', string="Approved user")
    minimum_approver = fields.Integer(string="Minimum Approver", default=1)
    timestamp = fields.Datetime(string="Timestamp")
    approved_time = fields.Text(string="Timestamp")
    approver_state = fields.Selection([('draft', 'Draft'), ('pending', 'Pending'), ('approved', 'Approved'),
                                       ('refuse', 'Refused')], default='draft', string="Approval Status")
    approval_status = fields.Text(string="Approval Status")
    is_approve = fields.Boolean(string="Is Approve", default=False)

    @api.depends('leave_id')
    def fetch_sl_no(self):
        sl = 0
        for line in self.leave_id.approver_user_ids:
            sl = sl + 1
            line.name = sl
