About
============
You can make a recurring order for your regular customers using this module. For example, a consumer could set up an order to have particular goods in every three months. you can make recurring orders using this module would let this purchase happen automatically on a regular schedule. You can also make recurring orders manually from recurring orders. You can set the scheduled time.



User Guide
============
Blog: https://softhealer.com/blog/odoo-2/post/sale-order-recurring-287


Installation
============
1) Copy module files to addon folder.
2) Restart odoo service (sudo service odoo-server restart).
3) Go to your odoo instance and open apps (make sure to activate debug mode).
4) click on update app list. 
5) search module name and hit install button.

Any Problem with module?
=====================================
Please create your ticket here https://softhealer.com/support

Softhealer Technologies Doubt/Inquiry/Sales/Customization Team
=====================================
Skype: live:softhealertechnologies
What's app: +917984575681
E-Mail: support@softhealer.com
Website: https://softhealer.com
