# -*- coding: utf-8 -*-

{
    "name": "Equip 3  - Manufacturing Kiosk Mode",
    "version": "1.1.16",
    "category": "Manufacturing",
    "summary": "Manufacturing Kiosk Mode",
    "description": """
    i. Work Center Kanban
    ii. Work Order Kanban
    iii. Kiosk Mode
    """,
    "author": "HashMicro",
    "website": "www.hashmicro.com",
    "depends": ["base", "mrp", "equip3_manuf_operations", "equip3_hr_masterdata_employee"],
    "data": [
        "views/mrp_workcenter_view.xml",
        # "views/mrp_workorder_view.xml",
        "views/kiosk_assets.xml",
        "views/res_config_settings_views.xml"
    ],
    "qweb": [
        "static/src/xml/kiosk_template.xml",
    ],
    "installable": True,
    "application": True,
    "auto_install": False,
}
