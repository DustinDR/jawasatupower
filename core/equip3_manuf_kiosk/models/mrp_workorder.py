# -*- coding: utf-8 -*-


from odoo import models, fields, api, _
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)

class StockMoveInherit(models.Model):
    _inherit = "stock.move"

    lot_name = fields.Char(
        string='Lot Name',
    )
    
    is_scan = fields.Boolean(
        string='Is Scan', default=False,
    )
    
    

class MrpWorkorderInheritKiosk(models.Model):
    _inherit = "mrp.workorder"


    manuf_kiosk_barcode_mobile = fields.Char(string="Mobile Barcode")

    manuf_kiosk_bm_is_cont_scan = fields.Boolean(
        related='company_id.manuf_kiosk_bm_is_cont_scan', string='Continuously Scan? (KIOSK)', readonly=False)
    
    manuf_kiosk_barcode_mobile_type = fields.Selection(
        related='company_id.manuf_kiosk_barcode_mobile_type', string='Product Scan Options In Mobile (KIOSK)', translate=True, readonly=False)
    
    manuf_kiosk_bm_is_notify_on_success = fields.Boolean(
        related='company_id.manuf_kiosk_bm_is_notify_on_success', string='Notification On Product Succeed? (KIOSK)', readonly=False)

    manuf_kiosk_bm_is_notify_on_fail = fields.Boolean(
        related='company_id.manuf_kiosk_bm_is_notify_on_fail', string='Notification On Product Failed? (KIOSK)', readonly=False)

    manuf_kiosk_bm_is_sound_on_success = fields.Boolean(
        related='company_id.manuf_kiosk_bm_is_sound_on_success', string='Play Sound On Product Succeed? (KIOSK)', readonly=False)

    manuf_kiosk_bm_is_sound_on_fail = fields.Boolean(
        related='company_id.manuf_kiosk_bm_is_sound_on_fail', string='Play Sound On Product Failed? (KIOSK)', readonly=False)
    
    # cofig for employee scan
    manuf_kiosk_att_is_cont_scan = fields.Boolean(
        related='company_id.manuf_kiosk_att_is_cont_scan', string='Continuously Scan? (KIOSK)', readonly=False)

    manuf_kiosk_att_is_notify_on_success = fields.Boolean(
        related='company_id.manuf_kiosk_att_is_notify_on_success', string='Notification On Attendance Succeed? (KIOSK)', readonly=False)

    manuf_kiosk_att_is_notify_on_fail = fields.Boolean(
        related='company_id.manuf_kiosk_att_is_notify_on_fail', string='Notification On Attendance Failed? (KIOSK)', readonly=False)

    manuf_kiosk_att_is_sound_on_success = fields.Boolean(
        related='company_id.manuf_kiosk_att_is_sound_on_success', string='Play Sound On Attendance Succeed? (KIOSK)', readonly=False)

    manuf_kiosk_att_is_sound_on_fail = fields.Boolean(
        related='company_id.manuf_kiosk_att_is_sound_on_fail', string='Play Sound On Attendance Failed? (KIOSK)', readonly=False)
    
    produced_finished_product = fields.Float('Produced Finished Goods', digits='Product Unit of Measure')
    produced_finished_product_uom_id = fields.Many2one('uom.uom', related='product_uom_id', string='Unit of Measure', readonly=True)
    produced_rejected_product = fields.Float('Produced Rejected Goods', digits='Product Unit of Measure')
    produced_rejected_product_uom_id = fields.Many2one('uom.uom', related='product_uom_id', string='Unit of Measure', readonly=True)
    digits_value = fields.Integer(
        compute='_compute_digits_value' )
    
    @api.model
    def _compute_digits_value(self):
        for record in self:
            record.digits_value = self.env['decimal.precision'].precision_get('Product Unit of Measure')
    
    
    is_show_barcode_scanner = fields.Boolean(string='Show Scan Barcode feature', compute='_compute_get_kiosk_cofig')
    is_qty_editable = fields.Boolean(string='Qty Editable', compute='_compute_get_kiosk_cofig')
    consumption_type = fields.Selection([
        ('flexible', 'Allowed'),
        ('warning', 'Allowed with warning'),
        ('strict', 'Blocked'),
    ], default='warning', string='Flexible Consumption', compute='_compute_get_consumption')
    
    employee_id = fields.Many2one('hr.employee', string='Employee')
        
    @api.model
    def _compute_get_consumption(self):
        for rec in self:
            rec.consumption_type = rec.production_id.bom_id.consumption
    
    @api.model
    def _compute_get_kiosk_cofig(self):
        for record in self:
            if record.state in ['pending', 'ready']:
                record.is_show_barcode_scanner = False
                record.is_qty_editable = False
            elif record.state in ['progress']:
                record.is_show_barcode_scanner = True
                record.is_qty_editable = True
            elif record.state in ['done', 'cancel']:
                record.is_show_barcode_scanner = False
                record.is_qty_editable = False
    
    
    @api.model            
    def get_kiosk_action(self, workorder_id):
        wo_id = self.env['mrp.workorder'].browse(workorder_id)
        kanban_view_ref = self.env.ref("equip3_manuf_kiosk.workcenter_line_kanban_kiosk", False)
        action = {
            "name": _("Work Orders"),
            "type": "ir.actions.act_window",
            "view_mode": "kanban",
            "res_model": "mrp.workorder",
            "domain": [
                ["state", "in", ["ready", "progress", "pending"]],
                ["workcenter_id.id", "=", wo_id.workcenter_id.id],
            ],
            "views": [(kanban_view_ref.id, "kanban")],
            "target": "main",
            "res_id": wo_id.workcenter_id.id,
        }
        return {'action': action}
    
    @api.model
    def kiosk_mobile_scan(self, barcode, wo_id):
        wo_id = self.env['mrp.workorder'].browse(wo_id)
        err_msg = ""
        product = self.env['product.product'].sudo().search(['|',('barcode', '=', barcode),('sh_qr_code', '=', barcode)], limit=1)
        if product:
            raw_id = self.env['stock.move'].search([('product_id', '=', product.id), ('mrp_workorder_component_id', '=', wo_id.id)], limit=1)
            if raw_id:
                raw_id.quantity_done = raw_id.quantity_done + 1
                raw_id.product_uom_qty = raw_id.quantity_done
            else:
                lot = self.env['stock.production.lot'].search([('product_id', '=', product.id)], limit=1)
                
                self.env['stock.move'].create({
                    'product_id': product.id,
                    'name': product.name,
                    'product_uom': product.uom_id.id,
                    'location_id': wo_id.workcenter_id.wc_location.id,
                    'location_dest_id': wo_id.workcenter_id.wc_location.id,
                    'product_uom_qty': 1,
                    'mrp_workorder_component_id': wo_id.id,
                    'workorder_id': wo_id.id,
                    'lot_ids': lot,
                    'lot_name': lot.name
                })
            return {'action': _("%(product)s.'") % {'product': wo_id.move_raw_ids}}
            # employee._attendance_action('hr_attendance.hr_attendance_action_kiosk_mode')
        else:
            err_msg = {'warning': _("No product corresponding to Barcode '%(barcode)s.'") % {'barcode': barcode}}
            return err_msg
    
    @api.model
    def kiosk_online_sync(self, workorder_id, component, produced_finished_goods, produced_rejected_goods, done_method_sync):
        # Update data
        wo_id = self.env['mrp.workorder'].browse(workorder_id)
        # check the wo status done or not
        if wo_id.state in ['progress']:
            for comp in component:
                _logger.info("APPLE %s: %s" % (comp['product_id'], comp['sync']))
                if comp['sync'] == False:
                    _logger.info("APPLE IF %s: %s" % (comp['product_id'], comp['sync']))
                    product = self.env['product.product'].browse(comp['product_id'])
                    if product:
                        # Update MO component consumed qty
                        m_comp = self.env['stock.move'].search([('product_id', '=', product.id), ('raw_material_production_id', '=', wo_id.production_id.id)], limit=1)
                        if m_comp:
                            m_comp.quantity_done = comp['product_uom_qty']
                        
                        if comp['move_id']:
                            raw_id = self.env['stock.move'].browse(comp['move_id'])
                            lot = self.env['stock.production.lot'].browse(comp['lot_id'])
                            if raw_id:
                                raw_id.product_uom_qty = comp['product_uom_qty']
                                raw_id.lot_name = lot.name
                                raw_id.lot_ids = lot
                                raw_id.is_scan = True
                            
                        else:
                            lot = self.env['stock.production.lot'].browse(comp['lot_id'])
                            
                            self.env['stock.move'].create({
                                'product_id': product.id,
                                'name': product.name,
                                'product_uom': product.uom_id.id,
                                'location_id': wo_id.workcenter_id.wc_location.id,
                                'location_dest_id': wo_id.workcenter_id.wc_location.id,
                                'product_uom_qty': comp['product_uom_qty'],
                                'mrp_workorder_component_id': wo_id.id,
                                'workorder_id': wo_id.id,
                                'lot_ids': lot,
                                'lot_name': lot.name,
                                'is_scan': True
                            })
            # Create MRP records
            done_method_sync = done_method_sync
            t_type = "create"
            cons_id = False
            if len(wo_id.consumption_ids) == 0:
                t_type = "create"
            
            for cons in wo_id.consumption_ids:
                if cons.state == 'draft':
                    t_type = "update"
                    cons_id = cons.id
                elif cons.state == 'confirm':
                    t_type = "create_new"
            
            approval_matrix = self.env['mrp.approval.matrix'].sudo()
            if self.env.user.branch_id:
                approval_matrix = approval_matrix.search([('company', '=', self.env.company.id), ('branch', '=', self.env.user.branch_id.id), ('matrix_type', '=', 'pr')], limit=1)

            all_workorder_ids = sorted(wo_id.production_id.workorder_ids.ids)
            move_finished_product = all_workorder_ids[-1] == wo_id.id
            move_finished_ids = self.env['stock.move']
            if move_finished_product:
                move_finished_ids = wo_id.production_id.move_finished_ids.filtered(
                    lambda m: m.state not in ('done', 'cancel') and m.product_id == wo_id.production_id.product_id
                )
            byproduct_ids = wo_id.byproduct_ids.filtered(lambda m: m.state not in ('done', 'cancel'))
                    
            # check consumption_ids and create consumption records
            if t_type == "create" or t_type == "create_new":
                consumption = self.env['mrp.consumption'].with_env(self.env(user=wo_id.employee_id.user_id.id)).create({
                    'manufacturing_plan': wo_id.mrp_plan_id.id,
                    'name': _('New'),
                    'create_date': fields.Datetime.now(),
                    'create_uid': wo_id.employee_id.user_id.id or self.env.user.id,
                    'manufacturing_order_id': wo_id.production_id.id,
                    'workorder_id': wo_id.id,
                    'product_id': wo_id.product_id.id,
                    'date_finished': fields.Datetime.now(),
                    'branch_id': self.env.user.branch_id.id,
                    'approval_matrix': approval_matrix.id,
                    'finished_product': produced_finished_goods or 0,
                    'rejected_product': produced_rejected_goods or 0,
                    'move_finished_product': move_finished_product,
                    'move_finished_ids': [(6, 0, move_finished_ids.ids)],
                    'byproduct_ids': [(6, 0, byproduct_ids.ids)],
                    'move_raw_ids': [(0, 0, {
                            "name": "New",
                            "product_id": line["product_id"],
                            "quantity_done": line["product_uom_qty"],
                            "product_uom_qty": line["product_uom_qty"],
                            "mpr_quantity_done": line["product_uom_qty"],
                            "product_uom": self.env['product.product'].browse(line['product_id']).uom_id.id,
                            "location_id": wo_id.workcenter_id.wc_location.id,
                            "location_dest_id": wo_id.workcenter_id.wc_location.id,
                            "lot_ids": [(6, 0, [line["lot_id"]])],
                    }) for line in component]
                })
                # consumption.oc_finished_product_rejected_product()  
            elif t_type == "update":
                if cons_id:
                    self.env['mrp.consumption'].browse(cons_id).write({
                                'finished_product': produced_finished_goods or 0,
                                'rejected_product': produced_rejected_goods or 0,
                    })
                    # self.env['mrp.consumption'].browse(cons_id).move_raw_ids.unlink()
                    for comp in component:
                        move_id = self.env['stock.move'].search([('product_id', '=', comp["product_id"]), ('mrp_consumption_id', '=', cons_id)], limit=1)
                        if move_id:
                            move_id.quantity_done = move_id.quantity_done + comp["new_qty"]
                            move_id.product_uom_qty = move_id.product_uom_qty + comp["new_qty"]
                            move_id.mpr_quantity_done = move_id.mpr_quantity_done + comp["new_qty"]
                            move_id.state = "done"
                        else:
                            if comp["new_qty"] > 0:
                                self.env['mrp.consumption'].browse(cons_id).write({
                                    'move_raw_ids': [(0, 0, {
                                            "name": "New",
                                            "product_id": comp["product_id"],
                                            "quantity_done": comp["new_qty"],
                                            "product_uom_qty": comp["new_qty"],
                                            "mpr_quantity_done": comp["new_qty"],
                                            "product_uom": self.env['product.product'].browse(comp['product_id']).uom_id.id,
                                            "location_id": wo_id.workcenter_id.wc_location.id,
                                            "location_dest_id": wo_id.workcenter_id.wc_location.id,
                                            "lot_ids": [(6, 0, [comp["lot_id"]])],
                                    })]
                                })

        return True
    
    @api.model
    def kiosk_online_sync_mrp(self, workorder_id, done_method_sync):
        """
        This method is used to update mrp record
        @param workorder_id: workorder id
        @return: True
        """
        wo_id = self.env['mrp.workorder'].browse(workorder_id)
        if done_method_sync:
                if wo_id:
                    wo_id.with_context(bypass_consumption=True).button_finish()
                    done_method_sync = False
                con_rec = self.env['mrp.consumption'].search([('workorder_id', '=', wo_id.id), ('state', '=', 'draft')], limit=1)
                if con_rec:
                    # Update employee id 
                    con_rec.write({
                        'create_uid': wo_id.employee_id.user_id.id or self.env.user.id
                    })
                    if wo_id.company_id.production_record_conf:
                        con_rec.action_approval()
                        # _logger.info("APPLE action_approval(): %s" % (con_rec.approval_matrix.id))
                    else:
                        con_rec.move_raw_done_ids = con_rec.move_raw_ids.filtered(lambda m: m.state == 'done')
                        # con_rec.button_confirm()
                        action = con_rec.button_confirm()
                        if action:
                            return action
                        # _logger.info("APPLE button_confirm(): %s" % (con_rec.approval_matrix.id))
                return True
            
    @api.model
    def kiosk_employee_scan(self, barcode, wo_id):
        wo_id = self.env['mrp.workorder'].browse(wo_id)
        err_msg = ""
        employee = self.env['hr.employee'].sudo().search([('sequence_code', '=', barcode)], limit=1)
        if employee:
            wo_id.write({'employee_id': employee.id})
            employee = {
                "name": employee.name,
                "id": employee.id,
                "sequence_code": employee.sequence_code,
            }
            return {'action': employee}
        else:
            err_msg = {'warning': _("No employee corresponding to Barcode '%(barcode)s.'") % {'barcode': barcode}}
            return err_msg