{
    'name': 'General Setting',
    'version': '1.1.8',
    'author': 'Hashmicro / Prince',
    'category' : '',
    'depends': ['base_setup',
    ],
    'data': [
        'data/ir_config_parameter.xml',
       'view/equip3_general_setting.xml',
       "view/menu.xml",
    ],
    'demo': [],
    'auto_install': False,
    'installable': True,
    'application': True
}