from odoo import models,api,_,fields


class Equip3SurveyInheritSurveyQuestion(models.Model):
    _inherit = 'survey.question'
    question_type = fields.Selection(selection_add=[
        ('disc', 'DISC'),('epps','EPPS'),('video','Video')])
    is_read_only = fields.Boolean(default=False)
    is_primary_master_data = fields.Boolean()
    matrix_subtype = fields.Selection([
        ('simple', 'One choice per row'),
        ('multiple', 'Multiple choices per row')], string='Matrix Type', default='simple')
    disc_subtype = fields.Selection([
        ('simple', 'One choice per row'),
        ('multiple', 'Multiple choices per row')], string='Disc Type', default='simple')
    
    interview_category = fields.Selection([('skills','Skills'),('personality','Personality')],string="Category")
    is_interview = fields.Boolean(default=False)
    comment_parent_id = fields.Many2one('survey.question',string="Parent Question")

    @api.model
    def default_get(self, fields_list):
        res = super().default_get(fields_list)
        question_type = self.env.context.get('default_question_type', False)
        is_read_only = self.env.context.get('default_is_read_only')
        if (not question_type):
            res.update({
                'question_type': 'text_box'
            })
        else:
            res.update({
                'question_type': question_type.lower() if question_type != 'general' else False,
                'is_read_only': is_read_only if question_type not in ['general','interview'] else False,
                'is_interview': True if question_type == 'interview' else False,
            })
        return res


class Equip3SurveyInheritSurveyQuestionAnswers(models.Model):
    _inherit = 'survey.question.answer'
    code_m = fields.Selection([
        ('D', 'D'),('I','I'),('S','S'),('C','C'),('*','*')],string="Code (M)")
    code_l = fields.Selection([('D', 'D'),('I','I'),('S','S'),('C','C'),('*','*')],string="Code (L)")
    code = fields.Selection([('a','A'),('b','B')],string="Code")
    epps_code = fields.Integer()


