from lxml import etree

from odoo import models,fields,api
from odoo.exceptions import ValidationError

class SurveyExtendInherit(models.Model):
    _inherit = 'survey.survey'
    survey_type = fields.Selection([('general','General'),('disc','DISC'),('epps','EPPS'),('interview','Interview')],default='general',readonly=True)
    is_read_only_type = fields.Boolean(default=False)


    # def unlink(self):
    #     for rec in self:
    #         if rec.survey_type in ('disc','epps'):
    #             raise ValidationError(f"Cannot delete survey type {str(rec.survey_type).upper()}")
    #     res = super(SurveyExtendInherit, self).unlink()
    #     return res
        

    @api.model
    def fields_view_get(self, view_id=None, view_type='form',
                        toolbar=False, submenu=False):
        res = super(SurveyExtendInherit, self).fields_view_get(
            view_id=view_id, view_type=view_type,
            toolbar=toolbar, submenu=submenu)
        if  self.env.context.get('default_survey_type') in ['disc']:
            root = etree.fromstring(res['arch'])
            root.set('create', 'false')
            res['arch'] = etree.tostring(root)

        return res

    @api.onchange('survey_type')
    def _onchange_survey_type(self):
        for record in self:
            if record.survey_type == 'disc':
                master_list = []
                if not record.question_and_page_ids:
                    master_data = self.env['survey.question'].search([('is_primary_master_data', '=', True)])
                    data = [line.id for line in master_data]
                    master_list.extend(data)
                    record.question_and_page_ids = [(6, 0, master_list)]
