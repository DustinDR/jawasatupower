from logging import PercentStyle
from odoo import models,api,_,fields
import plotly

class Equip3SurveyInheritSurveyUserInput(models.Model):
    _inherit = 'survey.user_input'
    survey_type = fields.Char(compute='_get_survey_type',store=True)
    disc_result_ids = fields.One2many('survey.disc_result','survey_user_input')
    disc_result_score2_ids = fields.One2many('survey.disc_result.score2','survey_user_input')
    disc_result_score3_ids = fields.One2many('survey.disc_result.score3','survey_user_input')
    epps_result_score_ids = fields.One2many('survey.consistency.result','survey_user_input')
    epps_peronality_result_score_ids = fields.One2many('survey.personality.result','survey_user_input')
    interview_result_skill_ids = fields.One2many('survey.interview.skill.result','survey_user_input')
    interview_result_personality_ids = fields.One2many('survey.interview.personality.result','survey_user_input')
    mask_public_self = fields.Many2one('survey.disc.variables')
    core_private_self = fields.Many2one('survey.disc.variables')
    mirror_perceived_self = fields.Many2one('survey.disc.variables')
    mask_public_self_ids = fields.One2many('survey.input.personality.line','mask_public_self')
    core_private_self_ids = fields.One2many('survey.input.personality.line','core_private_self')
    mirror_perceived_self_ids = fields.One2many('survey.input.personality.line','mirror_perceived_self')
    personal_description = fields.Text()
    job_match = fields.Text()
    job_suggestion = fields.Many2many('hr.job',string="Job Suggestion")
    is_hide_generate = fields.Boolean(default=False)
    is_hide_generate_score2 = fields.Boolean(default=True)
    is_hide_generate_score3 = fields.Boolean(default=True)
    is_hide_generate_final_score = fields.Boolean(default=True)
    shadow_field_mask_public_self = fields.Char(compute='_get_mask_public_self')
    shadow_field_core_private_self = fields.Char(compute='_get_core_private_self')
    shadow_field_mirror_perceived_self = fields.Char(compute='_get_mirror_perceived_self')
    skill_score = fields.Integer(compute='_compute_skill_score')
    personality_score = fields.Integer(compute='_compute_personality_score')
    chart_epps_result_score = fields.Text(
        string='EPPS Result Score Chart',
        compute='_compute_chart_epps_result_score',
    )
    chart_disc_result_score21 = fields.Text(
        string='Disc Result Score 2 Chart 1',
        compute='_compute_chart_disc_result_score2_1',
    )
    chart_disc_result_score22 = fields.Text(
        string='Disc Result Score 2 Chart 2',
        compute='_compute_chart_disc_result_score2_2',
    )
    chart_disc_result_score23 = fields.Text(
        string='Disc Result Score 2 Chart 3',
        compute='_compute_chart_disc_result_score2_3',
    )

    def _compute_chart_disc_result_score2_1(self):
        for data in self:
            chart_disc_result_score2_1 = False
            if data.disc_result_score2_ids:
                x_arr = []
                y_arr = []
                for l in data.disc_result_score2_ids:
                    if l.line == 1:
                        x_arr.append('D')
                        y_arr.append(l.d_field)
                    if l.line == 1:
                        x_arr.append('I')
                        y_arr.append(l.i_field)
                    if l.line == 1:
                        x_arr.append('S')
                        y_arr.append(l.s_field)
                    if l.line == 1:
                        x_arr.append('C')
                        y_arr.append(l.c_field)
                    if l.line == 1 and x_arr and y_arr:
                        break
                if x_arr and y_arr:
                    coor = [{'x': x_arr, 'y': y_arr}]
                    chart_disc_result_score2_1 = plotly.offline.plot(coor,
                                             include_plotlyjs=False,
                                             output_type='div')
            data.chart_disc_result_score21 = chart_disc_result_score2_1


    def _compute_chart_disc_result_score2_2(self):
        for data in self:
            chart_disc_result_score2_2 = False
            if data.disc_result_score2_ids:
                x_arr = []
                y_arr = []
                for l in data.disc_result_score2_ids:
                    if l.line == 2 :
                        x_arr.append('D')
                        y_arr.append(l.d_field)
                    if l.line == 2 :
                        x_arr.append('I')
                        y_arr.append(l.i_field)
                    if l.line == 2 :
                        x_arr.append('S')
                        y_arr.append(l.s_field)
                    if l.line == 2 :
                        x_arr.append('C')
                        y_arr.append(l.c_field)
                    if l.line == 2 and x_arr and y_arr:
                        break
                if x_arr and y_arr:
                    coor = [{'x': x_arr, 'y': y_arr}]
                    chart_disc_result_score2_2 = plotly.offline.plot(coor,
                                             include_plotlyjs=False,
                                             output_type='div')
            data.chart_disc_result_score22 = chart_disc_result_score2_2

    def _compute_chart_disc_result_score2_3(self):
        for data in self:
            chart_disc_result_score2_3 = False
            if data.disc_result_score2_ids:
                x_arr = []
                y_arr = []
                for l in data.disc_result_score2_ids:
                    if l.line == 3 :
                        x_arr.append('D')
                        y_arr.append(l.d_field)
                    if l.line == 3 :
                        x_arr.append('I')
                        y_arr.append(l.i_field)
                    if l.line == 3 :
                        x_arr.append('S')
                        y_arr.append(l.s_field)
                    if l.line == 3 :
                        x_arr.append('C')
                        y_arr.append(l.c_field)
                    if l.line == 3 and x_arr and y_arr:
                        break
                if x_arr and y_arr:
                    coor = [{'x': x_arr, 'y': y_arr}]
                    chart_disc_result_score2_3 = plotly.offline.plot(coor,
                                             include_plotlyjs=False,
                                             output_type='div')
            data.chart_disc_result_score23 = chart_disc_result_score2_3

    def _compute_chart_epps_result_score(self):
        for data in self:
            chart_epps_result_score = False
            if data.epps_peronality_result_score_ids:
                x_arr = []
                y_arr = []
                for l in data.epps_peronality_result_score_ids:
                    if l.percentile and l.factor:
                        x_arr.append(l.factor)
                        y_arr.append(l.percentile)
                if x_arr and y_arr:
                    coor = [{'x': x_arr, 'y': y_arr}]
                    chart_epps_result_score = plotly.offline.plot(coor,
                                             include_plotlyjs=False,
                                             output_type='div')
            data.chart_epps_result_score = chart_epps_result_score
    
    @api.depends('interview_result_skill_ids')
    def _compute_skill_score(self):
        for record in self:
            if record.interview_result_skill_ids:
                score = 0
                for data_score in record.interview_result_skill_ids:
                    if data_score.score == '1':
                        score += 2
                    if data_score.score == '2':
                        score += 4
                    if data_score.score == '3':
                        score += 6
                    if data_score.score == '4':
                        score += 8
                    if data_score.score == '5':
                        score += 10
                jml_data = len(record.interview_result_skill_ids)
                avg_score = score/jml_data
                record.skill_score = avg_score * 10
            else:
                record.skill_score = 0
                
    @api.depends('interview_result_personality_ids')
    def _compute_personality_score(self):
        for record in self:
            if record.interview_result_personality_ids:
                score = 0
                for data_score in record.interview_result_personality_ids:
                    if data_score.score == '1':
                        score += 2
                    if data_score.score == '2':
                        score += 4
                    if data_score.score == '3':
                        score += 6
                    if data_score.score == '4':
                        score += 8
                    if data_score.score == '5':
                        score += 10
                jml_data = len(record.interview_result_skill_ids)
                avg_score = score/jml_data
                record.personality_score = avg_score * 10
            else:
                record.personality_score = 0  
                
                            
                        
    
    
    
    
    
    def generate_interview_score(self):
         for record in self:
             skill_ids = []
             personality_ids = []
             for question_skills in record.user_input_line_ids.filtered(lambda line:line.question_id.interview_category == 'skills' and not line.question_id.comment_parent_id):
                 score_str = str(int(question_skills.answer_score))
                 comment = record.user_input_line_ids.filtered(lambda line:line.question_id.comment_parent_id.id == question_skills.question_id.id)
                 skill_ids.append((0,0,{'question':question_skills.question_id.title,'score':score_str,'comment':comment.value_char_box if comment else False}))
             record.interview_result_skill_ids = skill_ids
             for question in record.user_input_line_ids.filtered(lambda line:line.question_id.interview_category == 'personality' and not line.question_id.comment_parent_id):
                score_str = str(int(question.answer_score)) if question.answer_score else '0'
                comment = record.user_input_line_ids.filtered(lambda line:line.question_id.comment_parent_id.id == question.question_id.id)
                personality_ids.append((0,0,{'question':question.question_id.title,'score':score_str,'comment':comment.value_char_box if comment else False}))
             record.interview_result_personality_ids = personality_ids
             record.applicant_id.interview_id = record.id
             
    
    def epps_category(self,percentile):
        category = ""
        if percentile <= 3:
            category = "Sangat Pembohong"
        elif percentile <= 16:
            category = "Pembohong"
        elif percentile <= 84:
            category = "Cukup Jujur"
        elif percentile <= 96:
            category = "Jujur"
        elif percentile > 96:
            category = "Sangat Jujur"
        return category
    
    def epps_personality_category(self,percentile):
        category = ""
        if percentile <= 3:
            category = "Sangat Rendah"
        elif percentile <= 16:
            category = "Rendah"
        elif percentile <= 84:
            category = "Sedang"
        elif percentile <= 96:
            category = "Tinggi"
        elif percentile > 96:
            category = "Sangat Tinggi"
        return category
            
    def has_numbers(self,inputString):
        return any(char.isdigit() for char in inputString)
    
    def generate_epps(self):
        for record in self:
    
            if record.user_input_line_ids:
                if record.applicant_id:
                    code = 1
                    for data_epps_code in record.user_input_line_ids:
                        if data_epps_code.suggested_answer_id:
                            if data_epps_code.suggested_answer_id.epps_code == 1:
                                code = 1
                            elif data_epps_code.suggested_answer_id.epps_code == 2:
                                code = 2
                            elif data_epps_code.suggested_answer_id.epps_code == 3:
                                code = 3
                            elif data_epps_code.suggested_answer_id.epps_code == 4:
                                code = 4
                            
                                       
                    C1 = 0
                    C2 = 0
                    C3 = 0
                    C4 = 0
                    C5 = 0
                    C6 = 0
                    C7 = 0
                    C8 = 0
                    C9 = 0
                    C10 = 0
                    C11 = 0
                    C12 = 0
                    C13 = 0
                    C14 = 0
                    C15 = 0
                    score_len = []
                   
                    no_1 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "1" )
                    no_2 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "2"  )
                    no_3 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "3" )
                    no_4 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "4" )
                    no_5 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "5" )
                    no_6 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "6")
                    no_7 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "7")
                    no_8 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "8")
                    no_9 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "9")
                    no_10 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "10")
                    no_11 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "11")
                    no_12 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "12")
                    no_13 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "13")
                    no_14 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "14")
                    no_15 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "15")
                    no_16 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "16")
                    no_17 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "17")
                    no_18 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "18")
                    no_19 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "19")
                    no_20 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "20")
                    no_21 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "21")
                    no_22 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "22")
                    no_23 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "23")
                    no_24 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "24")
                    no_25 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "25")
                    no_26 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "26")
                    no_27 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "27")
                    no_28 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "28")
                    no_29 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "29")
                    no_30 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "30")
                    no_31 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "31")
                    no_32 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "32")
                    no_33 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "33")
                    no_34 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "34")
                    no_35 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "35")
                    no_36 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "36")
                    no_37 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "37")
                    no_38 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "38")
                    no_39 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "39")
                    no_40 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "40")
                    no_41 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "41")
                    no_42 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "42")
                    no_43 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "43")
                    no_44 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "44")
                    no_45 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "45")
                    no_46 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "46")
                    no_47 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "47")
                    no_48 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "48")
                    no_49 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "49")
                    no_50 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "50")
                    no_51 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "51" )
                    no_52 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "52" )
                    no_53 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "53" )
                    no_54 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "54" )
                    no_55 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "55" )
                    no_56 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "56" )
                    no_57 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "57" )
                    no_58 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "58" )
                    no_59 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "59" )
                    no_60 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "60" )
                    no_61 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "61" )
                    no_62 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "62" )
                    no_63 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "63" )
                    no_64 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "64" )
                    no_65 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "65" )
                    no_66 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "66" )
                    no_67 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "67" )
                    no_68 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "68" )
                    no_69 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "69" )
                    no_70 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "70" )
                    no_71 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "71" )
                    no_72 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "72" )
                    no_73 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "73" )
                    no_74 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "74" )
                    no_75 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "75" )
                    no_76 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "76" )
                    no_77 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "77" )
                    no_78 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "78")
                    no_79 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "79" )
                    no_80 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "80")
                    no_81 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "81")
                    no_82 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "82")
                    no_83 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "83")
                    no_84 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "84")
                    no_85 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "85")
                    no_86 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "86")
                    no_87 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "87")
                    no_88 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "88")
                    no_89 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "89")
                    no_90 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "90")
                    no_91 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "91")
                    no_92 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "92")
                    no_93 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "93")
                    no_94 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "94")
                    no_95 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "95")
                    no_96 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "96")
                    no_97 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "97")
                    no_98 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "98")
                    no_99 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "99")
                    no_100 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "100")
                    no_101 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "101")
                    no_102 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "102")
                    no_103 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "103")
                    no_104 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "104")
                    no_105 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "105")
                    no_106 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "106")
                    no_107 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "107")
                    no_108 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "108")
                    no_109 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "109")
                    no_110 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "110")
                    no_111 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "111")
                    no_112 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "112")
                    no_113 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "113")
                    no_114 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "114")
                    no_115 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "115")
                    no_116 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "116")
                    no_117 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "117")
                    no_118 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "118")
                    no_119 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "119")
                    no_120 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "120")
                    no_121 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "121")
                    no_122 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "122")
                    no_123 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "123")
                    no_124 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "124")
                    no_125 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "125")
                    no_126 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "126")
                    no_127 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "127")
                    no_128 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "128")
                    no_129 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "129")
                    no_130 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "130")
                    no_131 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "131")
                    no_132 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "132")
                    no_133 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "133")
                    no_134 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "134")
                    no_135 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "135")
                    no_136 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "136")
                    no_137 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "137")
                    no_138 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "138")
                    no_139 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "139")
                    no_140 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "140")
                    no_141 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "141")
                    no_142 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "142")
                    no_143 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "143")
                    no_144 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "144")
                    no_145 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "145")
                    no_146 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "146")
                    no_147 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "147")
                    no_148 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "148")
                    no_149 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "149")
                    no_150 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "150")
                    no_151 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "151")
                    no_152 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "152")
                    no_153 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "153")
                    no_154 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "154")
                    no_155 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "155")
                    no_156 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "156")
                    no_157 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "157")
                    no_158 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "158")
                    no_159 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "159")
                    no_160 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "160")
                    no_161 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "161")
                    no_162 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "162")
                    no_163 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "163")
                    no_164 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "164")
                    no_165 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "165")
                    no_166 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "166")
                    no_167 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "167")
                    no_168 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "168")
                    no_169 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "169")
                    no_170 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "170")
                    no_171 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "171")
                    no_172 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "172")
                    no_173 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "173")
                    no_174 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "174")
                    no_175 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "175")
                    no_176 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "176")
                    no_177 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "177")
                    no_178 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "178")
                    no_179 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "179")
                    no_180 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "180")
                    no_181 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "181")
                    no_182 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "182")
                    no_183 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "183")
                    no_184 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "184")
                    no_185 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "185")
                    no_186 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "186")
                    no_187 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "187")
                    no_188 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "188")
                    no_189 = record.user_input_line_ids.filtered(lambda line: line.question_id.title  == "189")
                    no_190 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "190")
                    no_191 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "191")
                    no_192 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "192")
                    no_193 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "193")
                    no_194 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "194")
                    no_195 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "195")
                    no_196 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "196")
                    no_197 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "197")
                    no_198 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "198")
                    no_199 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "199")
                    no_200 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "200")
                    no_201 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "201")
                    no_202 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "202")
                    no_203 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "203")
                    no_204 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "204")
                    no_205 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "205")
                    no_206 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "206")
                    no_207 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "207")
                    no_208 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "208")
                    no_209 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "209")
                    no_210 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "210")
                    no_211 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "211")
                    no_212 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "212")
                    no_213 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "213")
                    no_214 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "214")
                    no_215 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "215")
                    no_216 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "216")
                    no_217 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "217")
                    no_218 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "218")
                    no_219 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "219")
                    no_220 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "220")
                    no_221 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "221")
                    no_222 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "222")
                    no_223 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "223")
                    no_224 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "224")
                    no_225 = record.user_input_line_ids.filtered(lambda line: line.question_id.title == "225")
                    
                        
                    
                   
                    if no_1 and no_151:
                        if no_1.suggested_answer_id.code == no_151.suggested_answer_id.code:
                            C1 = 1
                            score_len.append(C1)
                    if no_7 and no_157:
                        if no_7.suggested_answer_id.code == no_157.suggested_answer_id.code:
                            C2 = 1
                            score_len.append(C2)
                    if no_13 and no_163:
                        if no_13.suggested_answer_id.code == no_163.suggested_answer_id.code:
                            C3 = 1
                            score_len.append(C3)
                    if no_19 and no_169:
                        if no_19.suggested_answer_id.code == no_169.suggested_answer_id.code:
                            C4 = 1
                            score_len.append(C4)
                    if no_25 and no_175:
                        if no_25.suggested_answer_id.code == no_175.suggested_answer_id.code:
                            C5 = 1
                            score_len.append(C5)
                    if no_26 and no_101:
                        if no_26.suggested_answer_id.code == no_101.suggested_answer_id.code:
                            C6 = 1
                            score_len.append(C6)
                    if no_32 and no_107:
                        if no_32.suggested_answer_id.code == no_107.suggested_answer_id.code:
                            C7 = 1
                            score_len.append(C7)
                    if no_38 and no_113:
                        if no_38.suggested_answer_id.code == no_113.suggested_answer_id.code:
                            C8 = 1
                            score_len.append(C8)
                    if no_44 and no_119:
                        if no_44.suggested_answer_id.code == no_119.suggested_answer_id.code:
                            C9 = 1
                            score_len.append(C9)
                    if no_50 and no_125:
                        if no_50.suggested_answer_id.code == no_125.suggested_answer_id.code:
                            C10 = 1
                            score_len.append(C10)
                    if no_51 and no_201:
                        if no_51.suggested_answer_id.code == no_201.suggested_answer_id.code:
                            C11 = 1
                            score_len.append(C11)
                    if no_57 and no_207:
                        if no_57.suggested_answer_id.code == no_207.suggested_answer_id.code:
                            C12 = 1
                            score_len.append(C12)
                    if no_63 and no_213:
                        if no_63.suggested_answer_id.code == no_213.suggested_answer_id.code:
                            C13 = 1
                            score_len.append(C13)
                    if no_69 and no_219:
                        if no_69.suggested_answer_id.code == no_219.suggested_answer_id.code:
                            C14 = 1
                            score_len.append(C14)
                    if no_75 and no_225:
                        if no_75.suggested_answer_id.code == no_225.suggested_answer_id.code:
                            C15 = 1
                            score_len.append(C15)
                    percentile = 0
                    category = ""
                    scoring_matrix = self.env['survey.epps.scoring.matrix'].search([('code','=',code)],limit=1)
                    if scoring_matrix:
                        line_data = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == len(score_len))
                        percentile = line_data.consistency
                        category = self.epps_category(percentile)
                        
                    record.epps_result_score_ids = [(0,0,{
                                                        'factor':"Con",
                                                        'c1':C1,
                                                        'c2':C2,
                                                        'c3':C3,
                                                        'c4':C4,
                                                        'c5':C5,
                                                        'c6':C6,
                                                        'c7':C7,
                                                        'c8':C8,
                                                        'c9':C9,
                                                        'c10':C10,
                                                        'c11':C11,
                                                        'c12':C12,
                                                        'c13':C13,
                                                        'c14':C14,
                                                        'c15':C15,
                                                        'score':len(score_len),
                                                        'percentile':percentile,
                                                        'category':category
                                                        
                                                        })]
                ach_r1 = 0                 
                ach_r2 = 0                 
                ach_r3 = 0                 
                ach_r4 = 0                 
                ach_r5 = 0                 
                ach_r6 = 0                 
                ach_r7 = 0                 
                ach_r8 = 0                 
                ach_r9 = 0                 
                ach_r10 = 0                 
                ach_r11 = 0                 
                ach_r12 = 0                 
                ach_r13 = 0                 
                ach_r14 = 0                 
                ach_r = 0
                ach_c = 0
                ach_r_score_len = []
                ach_c_score_len = []
                personality_ids = []
                ach_c1 = 0                                  
                ach_c2 = 0                                  
                ach_c3 = 0                                  
                ach_c4 = 0                                  
                ach_c5 = 0                                  
                ach_c6 = 0                                  
                ach_c7 = 0                                  
                ach_c8 = 0                                  
                ach_c9 = 0                                  
                ach_c10 = 0                                  
                ach_c11 = 0                                  
                ach_c12 = 0                                  
                ach_c13 = 0                                  
                ach_c14 = 0                                  
                ach_c = 0
                if no_6:
                    if no_6.suggested_answer_id.code == "a":
                        ach_r1 = 1
                        ach_r_score_len.append(ach_r1)
                if no_11:
                    if no_11.suggested_answer_id.code == "a":
                        ach_r2 = 1
                        ach_r_score_len.append(ach_r2)
                if no_16:
                    if no_16.suggested_answer_id.code == "a":
                        ach_r3 = 1
                        ach_r_score_len.append(ach_r3)
                if no_21:
                    if no_21.suggested_answer_id.code == "a":
                        ach_r4 = 1
                        ach_r_score_len.append(ach_r4)
                if no_26:
                    if no_26.suggested_answer_id.code == "a":
                        ach_r5 = 1
                        ach_r_score_len.append(ach_r5)
                if no_31:
                       if no_31.suggested_answer_id.code == "a":
                            ach_r6 = 1
                            ach_r_score_len.append(ach_r6) 
                if no_36:
                       if no_36.suggested_answer_id.code == "a":
                           ach_r7 = 1
                           ach_r_score_len.append(ach_r7) 
                if no_41:
                       if no_41.suggested_answer_id.code == "a":
                            ach_r8 = 1
                            ach_r_score_len.append(ach_r8) 
                if no_46:
                       if no_46.suggested_answer_id.code == "a":
                            ach_r9 = 1
                            ach_r_score_len.append(ach_r9) 
                if no_51:
                       if no_51.suggested_answer_id.code == "a":
                           ach_r10 = 1
                           ach_r_score_len.append(ach_r10) 
                if no_56:
                       if no_56.suggested_answer_id.code == "a":
                           ach_r11 = 1
                           ach_r_score_len.append(ach_r11) 
                if no_61:
                   if no_61.suggested_answer_id.code == "a":
                       ach_r12 = 1
                       ach_r_score_len.append(ach_r12)
                if no_66:
                       if no_66.suggested_answer_id.code == "a":
                           ach_r13 = 1
                           ach_r_score_len.append(ach_r13)
                if no_71:
                       if no_71.suggested_answer_id.code == "a":
                           ach_r14 = 1
                           ach_r_score_len.append(ach_r14)
                ach_r = len(ach_r_score_len)
                
                if no_2:
                      if no_2.suggested_answer_id.code == "b":
                          ach_c1 = 1
                          ach_c_score_len.append(ach_c1)
                if no_3:
                      if no_3.suggested_answer_id.code == "b":
                          ach_c2 = 1
                          ach_c_score_len.append(ach_c2)
                if no_4:
                      if no_4.suggested_answer_id.code == "b":
                          ach_c3 = 1
                          ach_c_score_len.append(ach_c3)
                if no_5:
                      if no_5.suggested_answer_id.code == "b":
                          ach_c4 = 1
                          ach_c_score_len.append(ach_c4)
                if no_76:
                      if no_76.suggested_answer_id.code == "b":
                          ach_c5 = 1
                          ach_c_score_len.append(ach_c5)
                if no_77:
                      if no_77.suggested_answer_id.code == "b":
                          ach_c6 = 1
                          ach_c_score_len.append(ach_c6)
                if no_78:
                      if no_78.suggested_answer_id.code == "b":
                          ach_c7 = 1
                          ach_c_score_len.append(ach_c7)
                if no_79:
                      if no_79.suggested_answer_id.code == "b":
                          ach_c8 = 1
                          ach_c_score_len.append(ach_c8)
                if no_80:
                      if no_80.suggested_answer_id.code == "b":
                          ach_c9 = 1
                          ach_c_score_len.append(ach_c9)
                if no_151:
                      if no_151.suggested_answer_id.code == "b":
                          ach_c10 = 1
                          ach_c_score_len.append(ach_c10)
                if no_152:
                      if no_152.suggested_answer_id.code == "b":
                          ach_c11 = 1
                          ach_c_score_len.append(ach_c11)
                if no_153:
                      if no_153.suggested_answer_id.code == "b":
                          ach_c12 = 1
                          ach_c_score_len.append(ach_c12)
                if no_154:
                      if no_154.suggested_answer_id.code == "b":
                          ach_c13 = 1
                          ach_c_score_len.append(ach_c13)
                if no_155:
                      if no_155.suggested_answer_id.code == "b":
                          ach_c14 = 1
                          ach_c_score_len.append(ach_c14)
                ach_c =  len(ach_c_score_len)
                          
                ach_rs = ach_r + ach_c
                ach_category = ""
                if scoring_matrix:
                        line_data_ach = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == ach_rs)
                        epps_personality =  self.env['survey.epps_personality'].search([('sequence','=',1)])
                        percentile_ach = line_data_ach.achievement
                        ach_category = self.epps_personality_category(percentile_ach)
                        personality_ids.append((0,0,{
                                                    'factor':"ach",
                                                    'r1':ach_r1,
                                                     'r2':ach_r2,
                                                     'r3':ach_r3,
                                                     'r4':ach_r4,
                                                     'r5':ach_r5,
                                                     'r6':ach_r6,
                                                     'r7':ach_r7,
                                                     'r8':ach_r8,
                                                     'r9':ach_r9,
                                                     'r10':ach_r10,
                                                     'r11':ach_r11,
                                                     'r12':ach_r12,
                                                     'r13':ach_r13,
                                                     'r14':ach_r14,
                                                     'r':ach_r,
                                                     'c1':ach_c1,
                                                     'c2':ach_c2,
                                                     'c3':ach_c3,
                                                     'c4':ach_c4,
                                                     'c5':ach_c5,
                                                     'c6':ach_c6,
                                                     'c7':ach_c7,
                                                     'c8':ach_c8,
                                                     'c9':ach_c9,
                                                     'c10':ach_c10,
                                                     'c11':ach_c11,
                                                     'c12':ach_c12,
                                                     'c13':ach_c13,
                                                     'c14':ach_c14,
                                                     'c':ach_c,
                                                     'rs':ach_rs,
                                                     'percentile':percentile_ach,
                                                     'category':ach_category,
                                                     'description':epps_personality.description if epps_personality else False,
                                                    
                                                     }))
                
                def_answer = []
                def_answer_c = []
                def_r1 = 0
                def_r2 = 0
                def_r3 = 0
                def_r4 = 0
                def_r5 = 0
                def_r6 = 0
                def_r7 = 0
                def_r8 = 0
                def_r9 = 0
                def_r10 = 0
                def_r11 = 0
                def_r12 = 0
                def_r13 = 0
                def_r14 = 0
                def_r = 0
                
                def_c1 = 0
                def_c2 = 0
                def_c3 = 0
                def_c4 = 0
                def_c5 = 0
                def_c6 = 0
                def_c7 = 0
                def_c8 = 0
                def_c9 = 0
                def_c10 = 0
                def_c11 = 0
                def_c12 = 0
                def_c13 = 0
                def_c14 = 0
                def_c = 0
                
                def_rs = 0
                
                if no_2:
                    if no_2.suggested_answer_id.code == "a":
                        def_r1 = 1
                        def_answer.append(def_r1)
                if no_12:
                    if no_12.suggested_answer_id.code == "a":
                        def_r2 = 1
                        def_answer.append(def_r2)
                if no_17:
                    if no_17.suggested_answer_id.code == "a":
                        def_r3 = 1
                        def_answer.append(def_r3)
                if no_22:
                    if no_22.suggested_answer_id.code == "a":
                        def_r4 = 1
                        def_answer.append(def_r4)
                if no_27:
                    if no_27.suggested_answer_id.code == "a":
                        def_r5 = 1
                        def_answer.append(def_r5)
                if no_32:
                    if no_32.suggested_answer_id.code == "a":
                        def_r6 = 1
                        def_answer.append(def_r6)
                if no_37:
                    if no_37.suggested_answer_id.code == "a":
                        def_r7 = 1
                        def_answer.append(def_r7)
                if no_42:
                    if no_42.suggested_answer_id.code == "a":
                        def_r8 = 1
                        def_answer.append(def_r8)
                if no_47:
                    if no_47.suggested_answer_id.code == "a":
                        def_r9 = 1
                        def_answer.append(def_r9)
                if no_52:
                    if no_52.suggested_answer_id.code == "a":
                        def_r10 = 1
                        def_answer.append(def_r10)
                if no_57:
                    if no_57.suggested_answer_id.code == "a":
                        def_r11 = 1
                        def_answer.append(def_r11)
                if no_62:
                    if no_62.suggested_answer_id.code == "a":
                        def_r12 = 1
                        def_answer.append(def_r12)
                if no_67:
                    if no_67.suggested_answer_id.code == "a":
                        def_r13 = 1
                        def_answer.append(def_r13)
                if no_72:
                    if no_72.suggested_answer_id.code == "a":
                        def_r14 = 1
                        def_answer.append(def_r14)
                def_r = len(def_answer)
                
                if no_6:
                    if no_6.suggested_answer_id.code == "b":
                        def_c1 = 1
                        def_answer_c.append(def_c1)
                if no_8:
                    if no_8.suggested_answer_id.code == "b":
                        def_c2 = 1
                        def_answer_c.append(def_c2)
                if no_9:
                    if no_9.suggested_answer_id.code == "b":
                        def_c3 = 1
                        def_answer_c.append(def_c3)
                if no_10:
                    if no_10.suggested_answer_id.code == "b":
                        def_c4 = 1
                        def_answer_c.append(def_c4)
                if no_81:
                    if no_81.suggested_answer_id.code == "b":
                        def_c5 = 1
                        def_answer_c.append(def_c5)
                if no_82:
                    if no_82.suggested_answer_id.code == "b":
                        def_c6 = 1
                        def_answer_c.append(def_c6)
                if no_83:
                    if no_83.suggested_answer_id.code == "b":
                        def_c7 = 1
                        def_answer_c.append(def_c7)
                if no_84:
                    if no_84.suggested_answer_id.code == "b":
                        def_c8 = 1
                        def_answer_c.append(def_c8)
                if no_85:
                    if no_85.suggested_answer_id.code == "b":
                        def_c9 = 1
                        def_answer_c.append(def_c9)
                if no_156:
                    if no_156.suggested_answer_id.code == "b":
                        def_c10 = 1
                        def_answer_c.append(def_c10)
                if no_157:
                    if no_157.suggested_answer_id.code == "b":
                        def_c11 = 1
                        def_answer_c.append(def_c11)
                if no_158:
                    if no_158.suggested_answer_id.code == "b":
                        def_c12 = 1
                        def_answer_c.append(def_c12)
                if no_159:
                    if no_159.suggested_answer_id.code == "b":
                        def_c13 = 1
                        def_answer_c.append(def_c13)
                if no_160:
                    if no_160.suggested_answer_id.code == "b":
                        def_c14 = 1
                        def_answer_c.append(def_c14)
                def_c = len(def_answer_c)
                
                
                def_rs = def_r + def_c
                def_category = ""
                if scoring_matrix:
                        line_data_def = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == def_rs)
                        percentile_def = line_data_def.deference
                        def_category = self.epps_personality_category(percentile_def)
                        epps_personality_def =  self.env['survey.epps_personality'].search([('sequence','=',2)])
                        personality_ids.append((0,0,{
                                                    'factor':"def",
                                                    'r1':def_r1,
                                                     'r2':def_r2,
                                                     'r3':def_r3,
                                                     'r4':def_r4,
                                                     'r5':def_r5,
                                                     'r6':def_r6,
                                                     'r7':def_r7,
                                                     'r8':def_r8,
                                                     'r9':def_r9,
                                                     'r10':def_r10,
                                                     'r11':def_r11,
                                                     'r12':def_r12,
                                                     'r13':def_r13,
                                                     'r14':def_r14,
                                                     'r':def_r,
                                                     'c1':def_c1,
                                                     'c2':def_c2,
                                                     'c3':def_c3,
                                                     'c4':def_c4,
                                                     'c5':def_c5,
                                                     'c6':def_c6,
                                                     'c7':def_c7,
                                                     'c8':def_c8,
                                                     'c9':def_c9,
                                                     'c10':def_c10,
                                                     'c11':def_c11,
                                                     'c12':def_c12,
                                                     'c13':def_c13,
                                                     'c14':def_c14,
                                                     'c':def_c,
                                                     'rs':def_rs,
                                                     'percentile':percentile_def,
                                                     'category':def_category,
                                                     'description':epps_personality_def.description if epps_personality_def else False,
                                                     
                                                    
                                                     }))
                
                ord_answer_r = []
                ord_answer_c = []
                ord_r1 = 0
                ord_r2 = 0
                ord_r3 = 0
                ord_r4 = 0
                ord_r5 = 0
                ord_r6 = 0
                ord_r7 = 0
                ord_r8 = 0
                ord_r9 = 0
                ord_r10 = 0
                ord_r11 = 0
                ord_r12 = 0
                ord_r13 = 0
                ord_r14 = 0
                
                ord_r = 0
                ord_rs = 0
                
                
                ord_c1 = 0
                ord_c2 = 0
                ord_c3 = 0
                ord_c4 = 0
                ord_c5 = 0
                ord_c6 = 0
                ord_c7 = 0
                ord_c8 = 0
                ord_c9 = 0
                ord_c10 = 0
                ord_c11 = 0
                ord_c12 = 0
                ord_c13 = 0
                ord_c14 = 0
                
                ord_c = 0
                
                if no_3:
                    if no_3.suggested_answer_id.code == "a":
                        ord_r1 = 1
                        ord_answer_r.append(ord_r1)
                if no_8:
                    if no_8.suggested_answer_id.code == "a":
                        ord_r2 = 1
                        ord_answer_r.append(ord_r2)
                if no_18:
                    if no_18.suggested_answer_id.code == "a":
                        ord_r3 = 1
                        ord_answer_r.append(ord_r3)
                if no_23:
                    if no_23.suggested_answer_id.code == "a":
                        ord_r4 = 1
                        ord_answer_r.append(ord_r4)
                if no_28:
                    if no_28.suggested_answer_id.code == "a":
                        ord_r5 = 1
                        ord_answer_r.append(ord_r5)
                if no_33:
                    if no_33.suggested_answer_id.code == "a":
                        ord_r6 = 1
                        ord_answer_r.append(ord_r6)
                if no_38:
                    if no_38.suggested_answer_id.code == "a":
                        ord_r7 = 1
                        ord_answer_r.append(ord_r7)
                if no_43:
                    if no_43.suggested_answer_id.code == "a":
                        ord_r8 = 1
                        ord_answer_r.append(ord_r8)
                if no_48:
                    if no_48.suggested_answer_id.code == "a":
                        ord_r9 = 1
                        ord_answer_r.append(ord_r9)
                if no_53:
                    if no_53.suggested_answer_id.code == "a":
                        ord_r10 = 1
                        ord_answer_r.append(ord_r10)
                if no_58:
                    if no_58.suggested_answer_id.code == "a":
                        ord_r11 = 1
                        ord_answer_r.append(ord_r11)
                if no_63:
                    if no_63.suggested_answer_id.code == "a":
                        ord_r12 = 1
                        ord_answer_r.append(ord_r12)
                if no_68:
                    if no_68.suggested_answer_id.code == "a":
                        ord_r13 = 1
                        ord_answer_r.append(ord_r13)
                if no_73:
                    if no_73.suggested_answer_id.code == "a":
                        ord_r14 = 1
                        ord_answer_r.append(ord_r14)
                ord_r = len(ord_answer_r)
                
                
                
                if no_11:
                    if no_11.suggested_answer_id.code == "b":
                        ord_c1 = 1
                        ord_answer_c.append(ord_c1)
                if no_12:
                    if no_12.suggested_answer_id.code == "b":
                        ord_c2 = 1
                        ord_answer_c.append(ord_c2)
                if no_14:
                    if no_14.suggested_answer_id.code == "b":
                        ord_c3 = 1
                        ord_answer_c.append(ord_c3)
                if no_15:
                    if no_15.suggested_answer_id.code == "b":
                        ord_c4 = 1
                        ord_answer_c.append(ord_c4)
                if no_86:
                    if no_86.suggested_answer_id.code == "b":
                        ord_c5 = 1
                        ord_answer_c.append(ord_c5)
                if no_87:
                    if no_87.suggested_answer_id.code == "b":
                        ord_c6 = 1
                        ord_answer_c.append(ord_c6)
                if no_88:
                    if no_88.suggested_answer_id.code == "b":
                        ord_c7 = 1
                        ord_answer_c.append(ord_c7)
                if no_89:
                    if no_89.suggested_answer_id.code == "b":
                        ord_c8 = 1
                        ord_answer_c.append(ord_c8)
                if no_90:
                    if no_90.suggested_answer_id.code == "b":
                        ord_c9 = 1
                        ord_answer_c.append(ord_c9)
                if no_161:
                    if no_161.suggested_answer_id.code == "b":
                        ord_c10 = 1
                        ord_answer_c.append(ord_c10)
                if no_162:
                    if no_162.suggested_answer_id.code == "b":
                        ord_c11 = 1
                        ord_answer_c.append(ord_c11)
                if no_163:
                    if no_163.suggested_answer_id.code == "b":
                        ord_c12 = 1
                        ord_answer_c.append(ord_c12)
                if no_164:
                    if no_164.suggested_answer_id.code == "b":
                        ord_c13 = 1
                        ord_answer_c.append(ord_c13)
                if no_165:
                    if no_165.suggested_answer_id.code == "b":
                        ord_c14 = 1
                        ord_answer_c.append(ord_c14)
                ord_c = len(ord_answer_c)
                ord_rs = ord_r + ord_c
                
                
                ord_category = ""
                if scoring_matrix:
                        line_data_ord = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == ord_rs)
                        percentile_ord = line_data_ord.order
                        ord_category = self.epps_personality_category(percentile_ord)
                        epps_personality_ord =  self.env['survey.epps_personality'].search([('sequence','=',3)])
                        personality_ids.append((0,0,{
                                                    'factor':"ord",
                                                    'r1':ord_r1,
                                                     'r2':ord_r2,
                                                     'r3':ord_r3,
                                                     'r4':ord_r4,
                                                     'r5':ord_r5,
                                                     'r6':ord_r6,
                                                     'r7':ord_r7,
                                                     'r8':ord_r8,
                                                     'r9':ord_r9,
                                                     'r10':ord_r10,
                                                     'r11':ord_r11,
                                                     'r12':ord_r12,
                                                     'r13':ord_r13,
                                                     'r14':ord_r14,
                                                     'r':ord_r,
                                                     'c1':ord_c1,
                                                     'c2':ord_c2,
                                                     'c3':ord_c3,
                                                     'c4':ord_c4,
                                                     'c5':ord_c5,
                                                     'c6':ord_c6,
                                                     'c7':ord_c7,
                                                     'c8':ord_c8,
                                                     'c9':ord_c9,
                                                     'c10':ord_c10,
                                                     'c11':ord_c11,
                                                     'c12':ord_c12,
                                                     'c13':ord_c13,
                                                     'c14':ord_c14,
                                                     'c':ord_c,
                                                     'rs':ord_rs,
                                                     'percentile':percentile_ord,
                                                     'category':ord_category,
                                                     'description':epps_personality_ord.description if epps_personality_ord else False
                                                    
                                                     }))
                        
                
                exh_answer_r = []
                exh_answer_c = []
                exh_r1 = 0
                exh_r2 = 0
                exh_r3 = 0
                exh_r4 = 0
                exh_r5 = 0
                exh_r6 = 0
                exh_r7 = 0
                exh_r8 = 0
                exh_r9 = 0
                exh_r10 = 0
                exh_r11 = 0
                exh_r12 = 0
                exh_r13 = 0
                exh_r14 = 0
                exh_r = 0
                
                exh_c1 = 0
                exh_c2 = 0
                exh_c3 = 0
                exh_c4 = 0
                exh_c5 = 0
                exh_c6 = 0
                exh_c7 = 0
                exh_c8 = 0
                exh_c9 = 0
                exh_c10 = 0
                exh_c11 = 0
                exh_c12 = 0
                exh_c13 = 0
                exh_c14 = 0
                exh_c = 0
                
                exh_rs = 0
                
                
                if no_4:
                    if no_4.suggested_answer_id.code == "a":
                        exh_r1 = 1
                        exh_answer_r.append(exh_r1)
                if no_9:
                    if no_9.suggested_answer_id.code == "a":
                        exh_r2 = 1
                        exh_answer_r.append(exh_r2)
                if no_14:
                    if no_14.suggested_answer_id.code == "a":
                        exh_r3 = 1
                        exh_answer_r.append(exh_r3)
                if no_24:
                    if no_24.suggested_answer_id.code == "a":
                        exh_r4 = 1
                        exh_answer_r.append(exh_r4)
                if no_29:
                    if no_29.suggested_answer_id.code == "a":
                        exh_r5 = 1
                        exh_answer_r.append(exh_r5)
                if no_34:
                    if no_34.suggested_answer_id.code == "a":
                        exh_r6 = 1
                        exh_answer_r.append(exh_r6)
                if no_39:
                    if no_39.suggested_answer_id.code == "a":
                        exh_r7 = 1
                        exh_answer_r.append(exh_r7)
                if no_44:
                    if no_44.suggested_answer_id.code == "a":
                        exh_r8 = 1
                        exh_answer_r.append(exh_r8)
                if no_49:
                    if no_49.suggested_answer_id.code == "a":
                        exh_r9 = 1
                        exh_answer_r.append(exh_r9)
                if no_54:
                    if no_54.suggested_answer_id.code == "a":
                        exh_r10 = 1
                        exh_answer_r.append(exh_r10)
                if no_59:
                    if no_59.suggested_answer_id.code == "a":
                        exh_r11 = 1
                        exh_answer_r.append(exh_r11)
                if no_64:
                    if no_64.suggested_answer_id.code == "a":
                        exh_r12 = 1
                        exh_answer_r.append(exh_r12)
                if no_69:
                    if no_69.suggested_answer_id.code == "a":
                        exh_r13 = 1
                        exh_answer_r.append(exh_r13)
                if no_74:
                    if no_74.suggested_answer_id.code == "a":
                        exh_r14 = 1
                        exh_answer_r.append(exh_r14)
                        
                exh_r = len(exh_answer_r)
                
                if no_16:
                    if no_16.suggested_answer_id.code == "b":
                        exh_c1 = 1
                        exh_answer_c.append(exh_c1)
                if no_17:
                    if no_17.suggested_answer_id.code == "b":
                        exh_c2 = 1
                        exh_answer_c.append(exh_c2)
                if no_18:
                    if no_18.suggested_answer_id.code == "b":
                        exh_c3 = 1
                        exh_answer_c.append(exh_c3)
                if no_20:
                    if no_20.suggested_answer_id.code == "b":
                        exh_c4 = 1
                        exh_answer_c.append(exh_c4)
                if no_91:
                    if no_91.suggested_answer_id.code == "b":
                        exh_c5 = 1
                        exh_answer_c.append(exh_c5)
                if no_92:
                    if no_92.suggested_answer_id.code == "b":
                        exh_c6 = 1
                        exh_answer_c.append(exh_c6)
                if no_93:
                    if no_93.suggested_answer_id.code == "b":
                        exh_c7 = 1
                        exh_answer_c.append(exh_c7)
                if no_94:
                    if no_94.suggested_answer_id.code == "b":
                        exh_c8 = 1
                        exh_answer_c.append(exh_c8)
                if no_95:
                    if no_95.suggested_answer_id.code == "b":
                        exh_c9 = 1
                        exh_answer_c.append(exh_c9)
                if no_166:
                    if no_166.suggested_answer_id.code == "b":
                        exh_c10 = 1
                        exh_answer_c.append(exh_c10)
                if no_167:
                    if no_167.suggested_answer_id.code == "b":
                        exh_c11 = 1
                        exh_answer_c.append(exh_c11)
                if no_168:
                    if no_168.suggested_answer_id.code == "b":
                        exh_c12 = 1
                        exh_answer_c.append(exh_c12)
                if no_169:
                    if no_169.suggested_answer_id.code == "b":
                        exh_c13 = 1
                        exh_answer_c.append(exh_c13)
                if no_170:
                    if no_170.suggested_answer_id.code == "b":
                        exh_c14 = 1
                        exh_answer_c.append(exh_c14)
                exh_c = len(exh_answer_c)
                exh_rs = exh_r + exh_c
                
                
                exh_category = ""
                if scoring_matrix:
                        line_data_exh = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == exh_rs)
                        percentile_exh = line_data_exh.exhibition
                        exh_category = self.epps_personality_category(percentile_exh)
                        epps_personality_exh =  self.env['survey.epps_personality'].search([('sequence','=',4)])
                        personality_ids.append((0,0,{
                                                    'factor':"exh",
                                                    'r1':exh_r1,
                                                     'r2':exh_r2,
                                                     'r3':exh_r3,
                                                     'r4':exh_r4,
                                                     'r5':exh_r5,
                                                     'r6':exh_r6,
                                                     'r7':exh_r7,
                                                     'r8':exh_r8,
                                                     'r9':exh_r9,
                                                     'r10':exh_r10,
                                                     'r11':exh_r11,
                                                     'r12':exh_r12,
                                                     'r13':exh_r13,
                                                     'r14':exh_r14,
                                                     'r':exh_r,
                                                     'c1':exh_c1,
                                                     'c2':exh_c2,
                                                     'c3':exh_c3,
                                                     'c4':exh_c4,
                                                     'c5':exh_c5,
                                                     'c6':exh_c6,
                                                     'c7':exh_c7,
                                                     'c8':exh_c8,
                                                     'c9':exh_c9,
                                                     'c10':exh_c10,
                                                     'c11':exh_c11,
                                                     'c12':exh_c12,
                                                     'c13':exh_c13,
                                                     'c14':exh_c14,
                                                     'c':exh_c,
                                                     'rs':exh_rs,
                                                     'percentile':percentile_exh,
                                                     'category':exh_category,
                                                     'description':epps_personality_exh.description if epps_personality_exh else False
                                                    
                                                     }))             
                aut_answer_r = []
                aut_answer_c = []
                aut_r1 = 0
                aut_r2 = 0
                aut_r3 = 0
                aut_r4 = 0
                aut_r5 = 0
                aut_r6 = 0
                aut_r7 = 0
                aut_r8 = 0
                aut_r9 = 0
                aut_r10 = 0
                aut_r11 = 0
                aut_r12 = 0
                aut_r13 = 0
                aut_r14 = 0
                aut_r = 0
                
                aut_c1 = 0
                aut_c2 = 0
                aut_c3 = 0
                aut_c4 = 0
                aut_c5 = 0
                aut_c6 = 0
                aut_c7 = 0
                aut_c8 = 0
                aut_c9 = 0
                aut_c10 = 0
                aut_c11 = 0
                aut_c12 = 0
                aut_c13 = 0
                aut_c14 = 0
                aut_c = 0
                
                
                if no_5:
                    if no_5.suggested_answer_id.code == "a":
                        aut_r1 = 1
                        aut_answer_r.append(aut_r1)
                if no_10:
                    if no_10.suggested_answer_id.code == "a":
                        aut_r2 = 1
                        aut_answer_r.append(aut_r2)
                if no_15:
                    if no_15.suggested_answer_id.code == "a":
                        aut_r3 = 1
                        aut_answer_r.append(aut_r3)
                if no_20:
                    if no_20.suggested_answer_id.code == "a":
                        aut_r4 = 1
                        aut_answer_r.append(aut_r4)
                if no_30:
                    if no_30.suggested_answer_id.code == "a":
                        aut_r5 = 1
                        aut_answer_r.append(aut_r5)
                if no_35:
                    if no_35.suggested_answer_id.code == "a":
                        aut_r6 = 1
                        aut_answer_r.append(aut_r6)
                if no_40:
                    if no_40.suggested_answer_id.code == "a":
                        aut_r7 = 1
                        aut_answer_r.append(aut_r7)
                if no_45:
                    if no_45.suggested_answer_id.code == "a":
                        aut_r8 = 1
                        aut_answer_r.append(aut_r8)
                if no_50:
                    if no_50.suggested_answer_id.code == "a":
                        aut_r9 = 1
                        aut_answer_r.append(aut_r9)
                if no_55:
                    if no_55.suggested_answer_id.code == "a":
                        aut_r10 = 1
                        aut_answer_r.append(aut_r10)
                if no_60:
                    if no_60.suggested_answer_id.code == "a":
                        aut_r11 = 1
                        aut_answer_r.append(aut_r11)
                if no_65:
                    if no_65.suggested_answer_id.code == "a":
                        aut_r12 = 1
                        aut_answer_r.append(aut_r12)
                if no_70:
                    if no_70.suggested_answer_id.code == "a":
                        aut_r13 = 1
                        aut_answer_r.append(aut_r13)
                if no_75:
                    if no_75.suggested_answer_id.code == "a":
                        aut_r14 = 1
                        aut_answer_r.append(aut_r14)
                
                aut_r =len(aut_answer_r)
                
                if no_21:
                    if no_21.suggested_answer_id.code == "b":
                        aut_c1 = 1
                        aut_answer_c.append(aut_c1)
                if no_22:
                    if no_22.suggested_answer_id.code == "b":
                        aut_c2 = 1
                        aut_answer_c.append(aut_c2)
                if no_23:
                    if no_23.suggested_answer_id.code == "b":
                        aut_c3 = 1
                        aut_answer_c.append(aut_c3)
                if no_24:
                    if no_24.suggested_answer_id.code == "b":
                        aut_c4 = 1
                        aut_answer_c.append(aut_c4)
                if no_96:
                    if no_96.suggested_answer_id.code == "b":
                        aut_c5 = 1
                        aut_answer_c.append(aut_c5)
                if no_97:
                    if no_97.suggested_answer_id.code == "b":
                        aut_c6 = 1
                        aut_answer_c.append(aut_c6)
                if no_98:
                    if no_98.suggested_answer_id.code == "b":
                        aut_c7 = 1
                        aut_answer_c.append(aut_c7)
                if no_99:
                    if no_99.suggested_answer_id.code == "b":
                        aut_c8 = 1
                        aut_answer_c.append(aut_c8)
                if no_100:
                    if no_100.suggested_answer_id.code == "b":
                        aut_c9 = 1
                        aut_answer_c.append(aut_c9)
                if no_171:
                    if no_171.suggested_answer_id.code == "b":
                        aut_c10 = 1
                        aut_answer_c.append(aut_c10)
                if no_172:
                    if no_172.suggested_answer_id.code == "b":
                        aut_c11 = 1
                        aut_answer_c.append(aut_c11)
                if no_173:
                    if no_173.suggested_answer_id.code == "b":
                        aut_c12 = 1
                        aut_answer_c.append(aut_c12)
                if no_174:
                    if no_174.suggested_answer_id.code == "b":
                        aut_c13 = 1
                        aut_answer_c.append(aut_c13)
                if no_175:
                    if no_175.suggested_answer_id.code == "b":
                        aut_c14 = 1
                        aut_answer_c.append(aut_c14)
                aut_c = len(aut_answer_c)
                
                aut_rs = aut_r + aut_c
                
                
                aut_category = ""
                if scoring_matrix:
                        line_data_aut = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == aut_rs)
                        percentile_aut = line_data_aut.autonomy
                        aut_category = self.epps_personality_category(percentile_aut)
                        epps_personality_aut =  self.env['survey.epps_personality'].search([('sequence','=',5)])
                        personality_ids.append((0,0,{
                                                    'factor':"aut",
                                                    'r1':aut_r1,
                                                     'r2':aut_r2,
                                                     'r3':aut_r3,
                                                     'r4':aut_r4,
                                                     'r5':aut_r5,
                                                     'r6':aut_r6,
                                                     'r7':aut_r7,
                                                     'r8':aut_r8,
                                                     'r9':aut_r9,
                                                     'r10':aut_r10,
                                                     'r11':aut_r11,
                                                     'r12':aut_r12,
                                                     'r13':aut_r13,
                                                     'r14':aut_r14,
                                                     'r':aut_r,
                                                     'c1':aut_c1,
                                                     'c2':aut_c2,
                                                     'c3':aut_c3,
                                                     'c4':aut_c4,
                                                     'c5':aut_c5,
                                                     'c6':aut_c6,
                                                     'c7':aut_c7,
                                                     'c8':aut_c8,
                                                     'c9':aut_c9,
                                                     'c10':aut_c10,
                                                     'c11':aut_c11,
                                                     'c12':aut_c12,
                                                     'c13':aut_c13,
                                                     'c14':aut_c14,
                                                     'c':aut_c,
                                                     'rs':aut_rs,
                                                     'percentile':percentile_aut,
                                                     'category':aut_category,
                                                     'description':epps_personality_aut.description if epps_personality_aut else False
                                                    
                                                     }))
                        
                        
                
                
                
                
                aff_answer_r = []
                aff_answer_c = []
                aff_r1 = 0
                aff_r2 = 0
                aff_r3 = 0
                aff_r4 = 0
                aff_r5 = 0
                aff_r6 = 0
                aff_r7 = 0
                aff_r8 = 0
                aff_r9 = 0
                aff_r10 = 0
                aff_r11 = 0
                aff_r12 = 0
                aff_r13 = 0
                aff_r14 = 0
                aff_r = 0
                
                aff_c1 = 0
                aff_c2 = 0
                aff_c3 = 0
                aff_c4 = 0
                aff_c5 = 0
                aff_c6 = 0
                aff_c7 = 0
                aff_c8 = 0
                aff_c9 = 0
                aff_c10 = 0
                aff_c11 = 0
                aff_c12 = 0
                aff_c13 = 0
                aff_c14 = 0
                aff_c = 0
                
                
                if no_76:
                    if no_76.suggested_answer_id.code == "a":
                        aff_r1 = 1
                        aff_answer_r.append(aff_r1)
                if no_81:
                    if no_81.suggested_answer_id.code == "a":
                        aff_r2 = 1
                        aff_answer_r.append(aff_r2)
                if no_86:
                    if no_86.suggested_answer_id.code == "a":
                        aff_r3 = 1
                        aff_answer_r.append(aff_r3)
                if no_91:
                    if no_91.suggested_answer_id.code == "a":
                        aff_r4 = 1
                        aff_answer_r.append(aff_r4)
                if no_96:
                    if no_96.suggested_answer_id.code == "a":
                        aff_r5 = 1
                        aff_answer_r.append(aff_r5)
                if no_106:
                    if no_106.suggested_answer_id.code == "a":
                        aff_r6 = 1
                        aff_answer_r.append(aff_r6)
                if no_111:
                    if no_111.suggested_answer_id.code == "a":
                        aff_r7 = 1
                        aff_answer_r.append(aff_r7)
                if no_116:
                    if no_116.suggested_answer_id.code == "a":
                        aff_r8 = 1
                        aff_answer_r.append(aff_r8)
                if no_121:
                    if no_121.suggested_answer_id.code == "a":
                        aff_r9 = 1
                        aff_answer_r.append(aff_r9)
                if no_126:
                    if no_126.suggested_answer_id.code == "a":
                        aff_r10 = 1
                        aff_answer_r.append(aff_r10)
                if no_131:
                    if no_131.suggested_answer_id.code == "a":
                        aff_r11 = 1
                        aff_answer_r.append(aff_r11)
                if no_136:
                    if no_136.suggested_answer_id.code == "a":
                        aff_r12 = 1
                        aff_answer_r.append(aff_r12)
                if no_141:
                    if no_141.suggested_answer_id.code == "a":
                        aff_r13 = 1
                        aff_answer_r.append(aff_r13)
                if no_146:
                    if no_146.suggested_answer_id.code == "a":
                        aff_r14 = 1
                        aff_answer_r.append(aff_r14)
              
                
                aff_r =len(aff_answer_r)
                
                if no_26:
                    if no_26.suggested_answer_id.code == "b":
                        aff_c1 = 1
                        aff_answer_c.append(aff_c1)
                if no_26:
                    if no_26.suggested_answer_id.code == "b":
                        aff_c2 = 1
                        aff_answer_c.append(aff_c2)
                if no_28:
                    if no_28.suggested_answer_id.code == "b":
                        aff_c3 = 1
                        aff_answer_c.append(aff_c3)
                if no_29:
                    if no_29.suggested_answer_id.code == "b":
                        aff_c4 = 1
                        aff_answer_c.append(aff_c4)
                if no_30:
                    if no_30.suggested_answer_id.code == "b":
                        aff_c5 = 1
                        aff_answer_c.append(aff_c5)
                if no_102:
                    if no_102.suggested_answer_id.code == "b":
                        aff_c6 = 1
                        aff_answer_c.append(aff_c6)
                if no_103:
                    if no_103.suggested_answer_id.code == "b":
                        aff_c7 = 1
                        aff_answer_c.append(aff_c7)
                if no_104:
                    if no_104.suggested_answer_id.code == "b":
                        aff_c8 = 1
                        aff_answer_c.append(aff_c8)
                if no_105:
                    if no_105.suggested_answer_id.code == "b":
                        aff_c9 = 1
                        aff_answer_c.append(aff_c9)
                if no_176:
                    if no_176.suggested_answer_id.code == "b":
                        aff_c10 = 1
                        aff_answer_c.append(aff_c10)
                if no_177:
                    if no_177.suggested_answer_id.code == "b":
                        aff_c11 = 1
                        aff_answer_c.append(aff_c11)
                if no_178:
                    if no_178.suggested_answer_id.code == "b":
                        aff_c12 = 1
                        aff_answer_c.append(aff_c12)
                if no_179:
                    if no_179.suggested_answer_id.code == "b":
                        aff_c13 = 1
                        aff_answer_c.append(aff_c13)
                if no_180:
                    if no_180.suggested_answer_id.code == "b":
                        aff_c14 = 1
                        aff_answer_c.append(aff_c14)
                aff_c = len(aff_answer_c)
                
                aff_rs = aff_r + aff_c
                
                
                aff_category = ""
                if scoring_matrix:
                        line_data_aff = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == aff_rs)
                        percentile_aff = line_data_aff.affiliation
                        aff_category = self.epps_personality_category(percentile_aff)
                        epps_personality_aff =  self.env['survey.epps_personality'].search([('sequence','=',6)])
                        personality_ids.append((0,0,{
                                                    'factor':"aff",
                                                    'r1':aff_r1,
                                                     'r2':aff_r2,
                                                     'r3':aff_r3,
                                                     'r4':aff_r4,
                                                     'r5':aff_r5,
                                                     'r6':aff_r6,
                                                     'r7':aff_r7,
                                                     'r8':aff_r8,
                                                     'r9':aff_r9,
                                                     'r10':aff_r10,
                                                     'r11':aff_r11,
                                                     'r12':aff_r12,
                                                     'r13':aff_r13,
                                                     'r14':aff_r14,
                                                     'r':aff_r,
                                                     'c1':aff_c1,
                                                     'c2':aff_c2,
                                                     'c3':aff_c3,
                                                     'c4':aff_c4,
                                                     'c5':aff_c5,
                                                     'c6':aff_c6,
                                                     'c7':aff_c7,
                                                     'c8':aff_c8,
                                                     'c9':aff_c9,
                                                     'c10':aff_c10,
                                                     'c11':aff_c11,
                                                     'c12':aff_c12,
                                                     'c13':aff_c13,
                                                     'c14':aff_c14,
                                                     'c':aff_c,
                                                     'rs':aff_rs,
                                                     'percentile':percentile_aff,
                                                     'category':aff_category,
                                                     'description':epps_personality_aff.description if epps_personality_aff else False
                                                    
                                                     }))
                
                
                
                int_answer_r = []
                int_answer_c = []
                int_r1 = 0
                int_r2 = 0
                int_r3 = 0
                int_r4 = 0
                int_r5 = 0
                int_r6 = 0
                int_r7 = 0
                int_r8 = 0
                int_r9 = 0
                int_r10 = 0
                int_r11 = 0
                int_r12 = 0
                int_r13 = 0
                int_r14 = 0
                int_r = 0
                
                int_c1 = 0
                int_c2 = 0
                int_c3 = 0
                int_c4 = 0
                int_c5 = 0
                int_c6 = 0
                int_c7 = 0
                int_c8 = 0
                int_c9 = 0
                int_c10 = 0
                int_c11 = 0
                int_c12 = 0
                int_c13 = 0
                int_c14 = 0
                int_c = 0
                
                
                if no_77:
                    if no_77.suggested_answer_id.code == "a":
                        int_r1 = 1
                        int_answer_r.append(int_r1)
                if no_82:
                    if no_82.suggested_answer_id.code == "a":
                        int_r2 = 1
                        int_answer_r.append(int_r2)
                if no_87:
                    if no_87.suggested_answer_id.code == "a":
                        int_r3 = 1
                        int_answer_r.append(int_r3)
                if no_92:
                    if no_92.suggested_answer_id.code == "a":
                        int_r4 = 1
                        int_answer_r.append(int_r4)
                if no_97:
                    if no_97.suggested_answer_id.code == "a":
                        int_r5 = 1
                        int_answer_r.append(int_r5)
                if no_102:
                    if no_102.suggested_answer_id.code == "a":
                        int_r6 = 1
                        int_answer_r.append(int_r6)
                if no_112:
                    if no_112.suggested_answer_id.code == "a":
                        aff_r7 = 1
                        int_answer_r.append(aff_r7)
                if no_117:
                    if no_117.suggested_answer_id.code == "a":
                        int_r8 = 1
                        int_answer_r.append(int_r8)
                if no_122:
                    if no_122.suggested_answer_id.code == "a":
                        int_r9 = 1
                        int_answer_r.append(int_r9)
                if no_127:
                    if no_127.suggested_answer_id.code == "a":
                        int_r10 = 1
                        int_answer_r.append(int_r10)
                if no_132:
                    if no_132.suggested_answer_id.code == "a":
                        int_r11 = 1
                        int_answer_r.append(int_r11)
                if no_137:
                    if no_137.suggested_answer_id.code == "a":
                        int_r12 = 1
                        int_answer_r.append(int_r12)
                if no_142:
                    if no_142.suggested_answer_id.code == "a":
                        int_r13 = 1
                        int_answer_r.append(int_r13)
                if no_147:
                    if no_147.suggested_answer_id.code == "a":
                        int_r14 = 1
                        int_answer_r.append(int_r14)
              
                
                int_r =len(int_answer_r)
                
                if no_31:
                    if no_31.suggested_answer_id.code == "b":
                        int_c1 = 1
                        int_answer_c.append(int_c1)
                if no_32:
                    if no_32.suggested_answer_id.code == "b":
                        int_c2 = 1
                        int_answer_c.append(int_c2)
                if no_33:
                    if no_33.suggested_answer_id.code == "b":
                        int_c3 = 1
                        int_answer_c.append(int_c3)
                if no_34:
                    if no_34.suggested_answer_id.code == "b":
                        int_c4 = 1
                        int_answer_c.append(int_c4)
                if no_35:
                    if no_35.suggested_answer_id.code == "b":
                        int_c5 = 1
                        int_answer_c.append(int_c5)
                if no_106:
                    if no_106.suggested_answer_id.code == "b":
                        int_c6 = 1
                        int_answer_c.append(int_c6)
                if no_108:
                    if no_108.suggested_answer_id.code == "b":
                        int_c7 = 1
                        int_answer_c.append(int_c7)
                if no_109:
                    if no_109.suggested_answer_id.code == "b":
                        int_c8 = 1
                        int_answer_c.append(int_c8)
                if no_110:
                    if no_110.suggested_answer_id.code == "b":
                        int_c9 = 1
                        int_answer_c.append(int_c9)
                if no_181:
                    if no_181.suggested_answer_id.code == "b":
                        int_c10 = 1
                        int_answer_c.append(int_c10)
                if no_182:
                    if no_182.suggested_answer_id.code == "b":
                        int_c11 = 1
                        int_answer_c.append(int_c11)
                if no_183:
                    if no_183.suggested_answer_id.code == "b":
                        int_c12 = 1
                        int_answer_c.append(int_c12)
                if no_184:
                    if no_184.suggested_answer_id.code == "b":
                        int_c13 = 1
                        int_answer_c.append(int_c13)
                if no_185:
                    if no_185.suggested_answer_id.code == "b":
                        int_c14 = 1
                        int_answer_c.append(int_c14)
                int_c = len(int_answer_c)
                
                int_rs = int_r + int_c
                
                
                int_category = ""
                if scoring_matrix:
                        line_data_int = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == int_rs)
                        percentile_int = line_data_int.intraception
                        int_category = self.epps_personality_category(percentile_int)
                        epps_personality_int =  self.env['survey.epps_personality'].search([('sequence','=',7)])
                        personality_ids.append((0,0,{
                                                    'factor':"int",
                                                    'r1':int_r1,
                                                     'r2':int_r2,
                                                     'r3':int_r3,
                                                     'r4':int_r4,
                                                     'r5':int_r5,
                                                     'r6':int_r6,
                                                     'r7':int_r7,
                                                     'r8':int_r8,
                                                     'r9':int_r9,
                                                     'r10':int_r10,
                                                     'r11':int_r11,
                                                     'r12':int_r12,
                                                     'r13':int_r13,
                                                     'r14':int_r14,
                                                     'r':int_r,
                                                     'c1':int_c1,
                                                     'c2':int_c2,
                                                     'c3':int_c3,
                                                     'c4':int_c4,
                                                     'c5':int_c5,
                                                     'c6':int_c6,
                                                     'c7':int_c7,
                                                     'c8':int_c8,
                                                     'c9':int_c9,
                                                     'c10':int_c10,
                                                     'c11':int_c11,
                                                     'c12':int_c12,
                                                     'c13':int_c13,
                                                     'c14':int_c14,
                                                     'c':int_c,
                                                     'rs':int_rs,
                                                     'percentile':percentile_int,
                                                     'category':int_category,
                                                     'description':epps_personality_int.description if epps_personality_int else False
                                                    
                                                     }))
                
                
                
                suc_answer_r = []
                suc_answer_c = []
                suc_r1 = 0
                suc_r2 = 0
                suc_r3 = 0
                suc_r4 = 0
                suc_r5 = 0
                suc_r6 = 0
                suc_r7 = 0
                suc_r8 = 0
                suc_r9 = 0
                suc_r10 = 0
                suc_r11 = 0
                suc_r12 = 0
                suc_r13 = 0
                suc_r14 = 0
                suc_r = 0
                
                suc_c1 = 0
                suc_c2 = 0
                suc_c3 = 0
                suc_c4 = 0
                suc_c5 = 0
                suc_c6 = 0
                suc_c7 = 0
                suc_c8 = 0
                suc_c9 = 0
                suc_c10 = 0
                suc_c11 = 0
                suc_c12 = 0
                suc_c13 = 0
                suc_c14 = 0
                suc_c = 0
                
                
                if no_78:
                    if no_78.suggested_answer_id.code == "a":
                        suc_r1 = 1
                        suc_answer_r.append(suc_r1)
                if no_83:
                    if no_83.suggested_answer_id.code == "a":
                        suc_r2 = 1
                        suc_answer_r.append(suc_r2)
                if no_88:
                    if no_88.suggested_answer_id.code == "a":
                        suc_r3 = 1
                        suc_answer_r.append(suc_r3)
                if no_93:
                    if no_93.suggested_answer_id.code == "a":
                        suc_r4 = 1
                        suc_answer_r.append(suc_r4)
                if no_98:
                    if no_98.suggested_answer_id.code == "a":
                        suc_r5 = 1
                        suc_answer_r.append(suc_r5)
                if no_103:
                    if no_103.suggested_answer_id.code == "a":
                        suc_r6 = 1
                        suc_answer_r.append(suc_r6)
                if no_108:
                    if no_108.suggested_answer_id.code == "a":
                        suc_r7 = 1
                        suc_answer_r.append(suc_r7)
                if no_118:
                    if no_118.suggested_answer_id.code == "a":
                        suc_r8 = 1
                        suc_answer_r.append(suc_r8)
                if no_123:
                    if no_123.suggested_answer_id.code == "a":
                        suc_r9 = 1
                        suc_answer_r.append(suc_r9)
                if no_128:
                    if no_128.suggested_answer_id.code == "a":
                        suc_r10 = 1
                        suc_answer_r.append(suc_r10)
                if no_133:
                    if no_133.suggested_answer_id.code == "a":
                        suc_r11 = 1
                        suc_answer_r.append(suc_r11)
                if no_138:
                    if no_138.suggested_answer_id.code == "a":
                        suc_r12 = 1
                        suc_answer_r.append(suc_r12)
                if no_143:
                    if no_143.suggested_answer_id.code == "a":
                        suc_r13 = 1
                        suc_answer_r.append(suc_r13)
                if no_148:
                    if no_148.suggested_answer_id.code == "a":
                        suc_r14 = 1
                        suc_answer_r.append(suc_r14)
              
                
                suc_r =len(suc_answer_r)
                
                if no_36:
                    if no_36.suggested_answer_id.code == "b":
                        suc_c1 = 1
                        suc_answer_c.append(suc_c1)
                if no_37:
                    if no_37.suggested_answer_id.code == "b":
                        suc_c2 = 1
                        suc_answer_c.append(suc_c2)
                if no_38:
                    if no_38.suggested_answer_id.code == "b":
                        suc_c3 = 1
                        suc_answer_c.append(suc_c3)
                if no_39:
                    if no_39.suggested_answer_id.code == "b":
                        suc_c4 = 1
                        suc_answer_c.append(suc_c4)
                if no_40:
                    if no_40.suggested_answer_id.code == "b":
                        suc_c5 = 1
                        suc_answer_c.append(suc_c5)
                if no_111:
                    if no_111.suggested_answer_id.code == "b":
                        suc_c6 = 1
                        suc_answer_c.append(suc_c6)
                if no_112:
                    if no_112.suggested_answer_id.code == "b":
                        suc_c7 = 1
                        suc_answer_c.append(suc_c7)
                if no_114:
                    if no_114.suggested_answer_id.code == "b":
                        suc_c8 = 1
                        suc_answer_c.append(suc_c8)
                if no_115:
                    if no_115.suggested_answer_id.code == "b":
                        suc_c9 = 1
                        suc_answer_c.append(suc_c9)
                if no_186:
                    if no_186.suggested_answer_id.code == "b":
                        suc_c10 = 1
                        suc_answer_c.append(suc_c10)
                if no_187:
                    if no_187.suggested_answer_id.code == "b":
                        suc_c11 = 1
                        suc_answer_c.append(suc_c11)
                if no_188:
                    if no_188.suggested_answer_id.code == "b":
                        suc_c12 = 1
                        suc_answer_c.append(suc_c12)
                if no_189:
                    if no_189.suggested_answer_id.code == "b":
                        suc_c13 = 1
                        suc_answer_c.append(suc_c13)
                if no_190:
                    if no_190.suggested_answer_id.code == "b":
                        suc_c14 = 1
                        suc_answer_c.append(suc_c14)
                suc_c = len(suc_answer_c)
                
                suc_rs = suc_r + suc_c
                
                
                suc_category = ""
                if scoring_matrix:
                        line_data_suc = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == suc_rs)
                        percentile_suc = line_data_suc.succorance
                        suc_category = self.epps_personality_category(percentile_suc)
                        epps_personality_suc =  self.env['survey.epps_personality'].search([('sequence','=',8)])
                        personality_ids.append((0,0,{
                                                    'factor':"suc",
                                                    'r1':suc_r1,
                                                     'r2':suc_r2,
                                                     'r3':suc_r3,
                                                     'r4':suc_r4,
                                                     'r5':suc_r5,
                                                     'r6':suc_r6,
                                                     'r7':suc_r7,
                                                     'r8':suc_r8,
                                                     'r9':suc_r9,
                                                     'r10':suc_r10,
                                                     'r11':suc_r11,
                                                     'r12':suc_r12,
                                                     'r13':suc_r13,
                                                     'r14':suc_r14,
                                                     'r':suc_r,
                                                     'c1':suc_c1,
                                                     'c2':suc_c2,
                                                     'c3':suc_c3,
                                                     'c4':suc_c4,
                                                     'c5':suc_c5,
                                                     'c6':suc_c6,
                                                     'c7':suc_c7,
                                                     'c8':suc_c8,
                                                     'c9':suc_c9,
                                                     'c10':suc_c10,
                                                     'c11':suc_c11,
                                                     'c12':suc_c12,
                                                     'c13':suc_c13,
                                                     'c14':suc_c14,
                                                     'c':suc_c,
                                                     'rs':suc_rs,
                                                     'percentile':percentile_suc,
                                                     'category':suc_category,
                                                     'description':epps_personality_suc.description if epps_personality_suc else False
                                                    
                                                     }))
                
                
                
                dom_answer_r = []
                dom_answer_c = []
                dom_r1 = 0
                dom_r2 = 0
                dom_r3 = 0
                dom_r4 = 0
                dom_r5 = 0
                dom_r6 = 0
                dom_r7 = 0
                dom_r8 = 0
                dom_r9 = 0
                dom_r10 = 0
                dom_r11 = 0
                dom_r12 = 0
                dom_r13 = 0
                dom_r14 = 0
                dom_r = 0
                
                dom_c1 = 0
                dom_c2 = 0
                dom_c3 = 0
                dom_c4 = 0
                dom_c5 = 0
                dom_c6 = 0
                dom_c7 = 0
                dom_c8 = 0
                dom_c9 = 0
                dom_c10 = 0
                dom_c11 = 0
                dom_c12 = 0
                dom_c13 = 0
                dom_c14 = 0
                dom_c = 0
                
                
                if no_79:
                    if no_79.suggested_answer_id.code == "a":
                        dom_r1 = 1
                        dom_answer_r.append(dom_r1)
                if no_84:
                    if no_84.suggested_answer_id.code == "a":
                        dom_r2 = 1
                        dom_answer_r.append(dom_r2)
                if no_89:
                    if no_89.suggested_answer_id.code == "a":
                        dom_r3 = 1
                        dom_answer_r.append(dom_r3)
                if no_94:
                    if no_94.suggested_answer_id.code == "a":
                        dom_r4 = 1
                        dom_answer_r.append(dom_r4)
                if no_99:
                    if no_99.suggested_answer_id.code == "a":
                        dom_r5 = 1
                        dom_answer_r.append(dom_r5)
                if no_104:
                    if no_104.suggested_answer_id.code == "a":
                        dom_r6 = 1
                        dom_answer_r.append(dom_r6)
                if no_109:
                    if no_109.suggested_answer_id.code == "a":
                        dom_r7 = 1
                        dom_answer_r.append(dom_r7)
                if no_114:
                    if no_114.suggested_answer_id.code == "a":
                        dom_r8 = 1
                        dom_answer_r.append(dom_r8)
                if no_124:
                    if no_124.suggested_answer_id.code == "a":
                        dom_r9 = 1
                        dom_answer_r.append(dom_r9)
                if no_129:
                    if no_129.suggested_answer_id.code == "a":
                        dom_r10 = 1
                        dom_answer_r.append(dom_r10)
                if no_134:
                    if no_134.suggested_answer_id.code == "a":
                        dom_r11 = 1
                        dom_answer_r.append(dom_r11)
                if no_139:
                    if no_139.suggested_answer_id.code == "a":
                        dom_r12 = 1
                        dom_answer_r.append(dom_r12)
                if no_144:
                    if no_144.suggested_answer_id.code == "a":
                        dom_r13 = 1
                        dom_answer_r.append(dom_r13)
                if no_149:
                    if no_149.suggested_answer_id.code == "a":
                        dom_r14 = 1
                        dom_answer_r.append(dom_r14)
              
                
                dom_r =len(dom_answer_r)
                
                if no_41:
                    if no_41.suggested_answer_id.code == "b":
                        dom_c1 = 1
                        dom_answer_c.append(dom_c1)
                if no_42:
                    if no_42.suggested_answer_id.code == "b":
                        dom_c2 = 1
                        dom_answer_c.append(dom_c2)
                if no_43:
                    if no_43.suggested_answer_id.code == "b":
                        dom_c3 = 1
                        dom_answer_c.append(dom_c3)
                if no_44:
                    if no_44.suggested_answer_id.code == "b":
                        dom_c4 = 1
                        dom_answer_c.append(dom_c4)
                if no_45:
                    if no_45.suggested_answer_id.code == "b":
                        dom_c5 = 1
                        dom_answer_c.append(dom_c5)
                if no_116:
                    if no_116.suggested_answer_id.code == "b":
                        dom_c6 = 1
                        dom_answer_c.append(dom_c6)
                if no_117:
                    if no_117.suggested_answer_id.code == "b":
                        dom_c7 = 1
                        dom_answer_c.append(dom_c7)
                if no_118:
                    if no_118.suggested_answer_id.code == "b":
                        dom_c8 = 1
                        dom_answer_c.append(dom_c8)
                if no_120:
                    if no_120.suggested_answer_id.code == "b":
                        dom_c9 = 1
                        dom_answer_c.append(dom_c9)
                if no_191:
                    if no_191.suggested_answer_id.code == "b":
                        dom_c10 = 1
                        dom_answer_c.append(dom_c10)
                if no_192:
                    if no_192.suggested_answer_id.code == "b":
                        dom_c11 = 1
                        dom_answer_c.append(dom_c11)
                if no_193:
                    if no_193.suggested_answer_id.code == "b":
                        dom_c12 = 1
                        dom_answer_c.append(dom_c12)
                if no_194:
                    if no_194.suggested_answer_id.code == "b":
                        dom_c13 = 1
                        dom_answer_c.append(dom_c13)
                if no_195:
                    if no_195.suggested_answer_id.code == "b":
                        dom_c14 = 1
                        dom_answer_c.append(dom_c14)
                dom_c = len(dom_answer_c)
                
                dom_rs = dom_r + dom_c
                
                
                dom_category = ""
                if scoring_matrix:
                        line_data_dom = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == dom_rs)
                        percentile_dom = line_data_dom.dominance
                        dom_category = self.epps_personality_category(percentile_dom)
                        epps_personality_dom =  self.env['survey.epps_personality'].search([('sequence','=',9)])
                        personality_ids.append((0,0,{
                                                    'factor':"dom",
                                                    'r1':dom_r1,
                                                     'r2':dom_r2,
                                                     'r3':dom_r3,
                                                     'r4':dom_r4,
                                                     'r5':dom_r5,
                                                     'r6':dom_r6,
                                                     'r7':dom_r7,
                                                     'r8':dom_r8,
                                                     'r9':dom_r9,
                                                     'r10':dom_r10,
                                                     'r11':dom_r11,
                                                     'r12':dom_r12,
                                                     'r13':dom_r13,
                                                     'r14':dom_r14,
                                                     'r':dom_r,
                                                     'c1':dom_c1,
                                                     'c2':dom_c2,
                                                     'c3':dom_c3,
                                                     'c4':dom_c4,
                                                     'c5':dom_c5,
                                                     'c6':dom_c6,
                                                     'c7':dom_c7,
                                                     'c8':dom_c8,
                                                     'c9':dom_c9,
                                                     'c10':dom_c10,
                                                     'c11':dom_c11,
                                                     'c12':dom_c12,
                                                     'c13':dom_c13,
                                                     'c14':dom_c14,
                                                     'c':dom_c,
                                                     'rs':dom_rs,
                                                     'percentile':percentile_dom,
                                                     'category':dom_category,
                                                     'description':epps_personality_dom.description if epps_personality_dom else False
                                                    
                                                     }))
                
                
                
                
                aba_answer_r = []
                aba_answer_c = []
                aba_r1 = 0
                aba_r2 = 0
                aba_r3 = 0
                aba_r4 = 0
                aba_r5 = 0
                aba_r6 = 0
                aba_r7 = 0
                aba_r8 = 0
                aba_r9 = 0
                aba_r10 = 0
                aba_r11 = 0
                aba_r12 = 0
                aba_r13 = 0
                aba_r14 = 0
                aba_r = 0
                
                aba_c1 = 0
                aba_c2 = 0
                aba_c3 = 0
                aba_c4 = 0
                aba_c5 = 0
                aba_c6 = 0
                aba_c7 = 0
                aba_c8 = 0
                aba_c9 = 0
                aba_c10 = 0
                aba_c11 = 0
                aba_c12 = 0
                aba_c13 = 0
                aba_c14 = 0
                aba_c = 0
                
                
                if no_80:
                    if no_80.suggested_answer_id.code == "a":
                        aba_r1 = 1
                        aba_answer_r.append(aba_r1)
                if no_85:
                    if no_85.suggested_answer_id.code == "a":
                        aba_r2 = 1
                        aba_answer_r.append(aba_r2)
                if no_90:
                    if no_90.suggested_answer_id.code == "a":
                        aba_r3 = 1
                        aba_answer_r.append(aba_r3)
                if no_95:
                    if no_95.suggested_answer_id.code == "a":
                        aba_r4 = 1
                        aba_answer_r.append(aba_r4)
                if no_100:
                    if no_100.suggested_answer_id.code == "a":
                        aba_r5 = 1
                        aba_answer_r.append(aba_r5)
                if no_105:
                    if no_105.suggested_answer_id.code == "a":
                        aba_r6 = 1
                        aba_answer_r.append(aba_r6)
                if no_110:
                    if no_110.suggested_answer_id.code == "a":
                        aba_r7 = 1
                        aba_answer_r.append(aba_r7)
                if no_115:
                    if no_115.suggested_answer_id.code == "a":
                        aba_r8 = 1
                        aba_answer_r.append(aba_r8)
                if no_125:
                    if no_125.suggested_answer_id.code == "a":
                        aba_r9 = 1
                        aba_answer_r.append(aba_r9)
                if no_130:
                    if no_130.suggested_answer_id.code == "a":
                        aba_r10 = 1
                        aba_answer_r.append(aba_r10)
                if no_135:
                    if no_135.suggested_answer_id.code == "a":
                        aba_r11 = 1
                        aba_answer_r.append(aba_r11)
                if no_140:
                    if no_140.suggested_answer_id.code == "a":
                        aba_r12 = 1
                        aba_answer_r.append(aba_r12)
                if no_145:
                    if no_145.suggested_answer_id.code == "a":
                        aba_r13 = 1
                        aba_answer_r.append(aba_r13)
                if no_150:
                    if no_150.suggested_answer_id.code == "a":
                        aba_r14 = 1
                        aba_answer_r.append(aba_r14)
              
                
                aba_r = len(aba_answer_r)
                
                if no_46:
                    if no_46.suggested_answer_id.code == "b":
                        aba_c1 = 1
                        aba_answer_c.append(aba_c1)
                if no_47:
                    if no_47.suggested_answer_id.code == "b":
                        aba_c2 = 1
                        aba_answer_c.append(aba_c2)
                if no_48:
                    if no_48.suggested_answer_id.code == "b":
                        aba_c3 = 1
                        aba_answer_c.append(aba_c3)
                if no_49:
                    if no_49.suggested_answer_id.code == "b":
                        aba_c4 = 1
                        aba_answer_c.append(aba_c4)
                if no_50:
                    if no_50.suggested_answer_id.code == "b":
                        aba_c5 = 1
                        aba_answer_c.append(aba_c5)
                if no_121:
                    if no_121.suggested_answer_id.code == "b":
                        aba_c6 = 1
                        aba_answer_c.append(aba_c6)
                if no_122:
                    if no_122.suggested_answer_id.code == "b":
                        aba_c7 = 1
                        aba_answer_c.append(aba_c7)
                if no_123:
                    if no_123.suggested_answer_id.code == "b":
                        aba_c8 = 1
                        aba_answer_c.append(aba_c8)
                if no_124:
                    if no_124.suggested_answer_id.code == "b":
                        aba_c9 = 1
                        aba_answer_c.append(aba_c9)
                if no_196:
                    if no_196.suggested_answer_id.code == "b":
                        aba_c10 = 1
                        aba_answer_c.append(aba_c10)
                if no_197:
                    if no_197.suggested_answer_id.code == "b":
                        aba_c11 = 1
                        aba_answer_c.append(aba_c11)
                if no_198:
                    if no_198.suggested_answer_id.code == "b":
                        aba_c12 = 1
                        aba_answer_c.append(aba_c12)
                if no_199:
                    if no_199.suggested_answer_id.code == "b":
                        aba_c13 = 1
                        aba_answer_c.append(aba_c13)
                if no_200:
                    if no_200.suggested_answer_id.code == "b":
                        aba_c14 = 1
                        aba_answer_c.append(aba_c14)
                aba_c = len(aba_answer_c)
                
                aba_rs = aba_r + aba_c
                
                
                aba_category = ""
                if scoring_matrix:
                        line_data_aba = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == aba_rs)
                        percentile_aba = line_data_aba.abasement
                        aba_category = self.epps_personality_category(percentile_aba)
                        epps_personality_aba =  self.env['survey.epps_personality'].search([('sequence','=',10)])
                        personality_ids.append((0,0,{
                                                    'factor':"aba",
                                                    'r1':aba_r1,
                                                     'r2':aba_r2,
                                                     'r3':aba_r3,
                                                     'r4':aba_r4,
                                                     'r5':aba_r5,
                                                     'r6':aba_r6,
                                                     'r7':aba_r7,
                                                     'r8':aba_r8,
                                                     'r9':aba_r9,
                                                     'r10':aba_r10,
                                                     'r11':aba_r11,
                                                     'r12':aba_r12,
                                                     'r13':aba_r13,
                                                     'r14':aba_r14,
                                                     'r':aba_r,
                                                     'c1':aba_c1,
                                                     'c2':aba_c2,
                                                     'c3':aba_c3,
                                                     'c4':aba_c4,
                                                     'c5':aba_c5,
                                                     'c6':aba_c6,
                                                     'c7':aba_c7,
                                                     'c8':aba_c8,
                                                     'c9':aba_c9,
                                                     'c10':aba_c10,
                                                     'c11':aba_c11,
                                                     'c12':aba_c12,
                                                     'c13':aba_c13,
                                                     'c14':aba_c14,
                                                     'c':aba_c,
                                                     'rs':aba_rs,
                                                     'percentile':percentile_aba,
                                                     'category':aba_category,
                                                     'description':epps_personality_aba.description if epps_personality_aba else False
                                                    
                                                     }))
                
                
                
                
                nur_answer_r = []
                nur_answer_c = []
                nur_r1 = 0
                nur_r2 = 0
                nur_r3 = 0
                nur_r4 = 0
                nur_r5 = 0
                nur_r6 = 0
                nur_r7 = 0
                nur_r8 = 0
                nur_r9 = 0
                nur_r10 = 0
                nur_r11 = 0
                nur_r12 = 0
                nur_r13 = 0
                nur_r14 = 0
                nur_r = 0
                
                nur_c1 = 0
                nur_c2 = 0
                nur_c3 = 0
                nur_c4 = 0
                nur_c5 = 0
                nur_c6 = 0
                nur_c7 = 0
                nur_c8 = 0
                nur_c9 = 0
                nur_c10 = 0
                nur_c11 = 0
                nur_c12 = 0
                nur_c13 = 0
                nur_c14 = 0
                nur_c = 0
                
                
                if no_151:
                    if no_151.suggested_answer_id.code == "a":
                        nur_r1 = 1
                        nur_answer_r.append(nur_r1)
                if no_156:
                    if no_156.suggested_answer_id.code == "a":
                        nur_r2 = 1
                        nur_answer_r.append(nur_r2)
                if no_161:
                    if no_161.suggested_answer_id.code == "a":
                        nur_r3 = 1
                        nur_answer_r.append(nur_r3)
                if no_166:
                    if no_166.suggested_answer_id.code == "a":
                        nur_r4 = 1
                        nur_answer_r.append(nur_r4)
                if no_171:
                    if no_171.suggested_answer_id.code == "a":
                        nur_r5 = 1
                        nur_answer_r.append(nur_r5)
                if no_176:
                    if no_176.suggested_answer_id.code == "a":
                        nur_r6 = 1
                        nur_answer_r.append(nur_r6)
                if no_181:
                    if no_181.suggested_answer_id.code == "a":
                        nur_r7 = 1
                        nur_answer_r.append(nur_r7)
                if no_186:
                    if no_186.suggested_answer_id.code == "a":
                        nur_r8 = 1
                        nur_answer_r.append(nur_r8)
                if no_191:
                    if no_191.suggested_answer_id.code == "a":
                        nur_r9 = 1
                        nur_answer_r.append(nur_r9)
                if no_196:
                    if no_196.suggested_answer_id.code == "a":
                        nur_r10 = 1
                        nur_answer_r.append(nur_r10)
                if no_206:
                    if no_206.suggested_answer_id.code == "a":
                        nur_r11 = 1
                        nur_answer_r.append(nur_r11)
                if no_211:
                    if no_211.suggested_answer_id.code == "a":
                        nur_r12 = 1
                        nur_answer_r.append(nur_r12)
                if no_216:
                    if no_216.suggested_answer_id.code == "a":
                        nur_r13 = 1
                        nur_answer_r.append(nur_r13)
                if no_221:
                    if no_221.suggested_answer_id.code == "a":
                        nur_r14 = 1
                        nur_answer_r.append(nur_r14)
              
                
                nur_r = len(nur_answer_r)
                
                if no_51:
                    if no_51.suggested_answer_id.code == "b":
                        nur_c1 = 1
                        nur_answer_c.append(nur_c1)
                if no_52:
                    if no_52.suggested_answer_id.code == "b":
                        nur_c2 = 1
                        nur_answer_c.append(nur_c2)
                if no_53:
                    if no_53.suggested_answer_id.code == "b":
                        nur_c3 = 1
                        nur_answer_c.append(nur_c3)
                if no_54:
                    if no_54.suggested_answer_id.code == "b":
                        nur_c4 = 1
                        nur_answer_c.append(nur_c4)
                if no_55:
                    if no_55.suggested_answer_id.code == "b":
                        nur_c5 = 1
                        nur_answer_c.append(aba_c5)
                if no_126:
                    if no_126.suggested_answer_id.code == "b":
                        nur_c6 = 1
                        nur_answer_c.append(nur_c6)
                if no_127:
                    if no_127.suggested_answer_id.code == "b":
                        nur_c7 = 1
                        nur_answer_c.append(nur_c7)
                if no_128:
                    if no_128.suggested_answer_id.code == "b":
                        nur_c8 = 1
                        nur_answer_c.append(nur_c8)
                if no_129:
                    if no_129.suggested_answer_id.code == "b":
                        nur_c9 = 1
                        nur_answer_c.append(nur_c9)
                if no_130:
                    if no_130.suggested_answer_id.code == "b":
                        nur_c10 = 1
                        nur_answer_c.append(nur_c10)
                if no_202:
                    if no_202.suggested_answer_id.code == "b":
                        nur_c11 = 1
                        nur_answer_c.append(nur_c11)
                if no_203:
                    if no_203.suggested_answer_id.code == "b":
                        nur_c12 = 1
                        nur_answer_c.append(nur_c12)
                if no_204:
                    if no_204.suggested_answer_id.code == "b":
                        nur_c13 = 1
                        nur_answer_c.append(nur_c13)
                if no_205:
                    if no_205.suggested_answer_id.code == "b":
                        nur_c14 = 1
                        nur_answer_c.append(nur_c14)
                nur_c = len(nur_answer_c)
                
                nur_rs = nur_r + nur_c
                
                
                nur_category = ""
                if scoring_matrix:
                        line_data_nur = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == nur_rs)
                        percentile_nur = line_data_nur.nurturance
                        nur_category = self.epps_personality_category(percentile_nur)
                        epps_personality_nur =  self.env['survey.epps_personality'].search([('sequence','=',11)])
                        personality_ids.append((0,0,{
                                                    'factor':"nur",
                                                    'r1':nur_r1,
                                                     'r2':nur_r2,
                                                     'r3':nur_r3,
                                                     'r4':nur_r4,
                                                     'r5':nur_r5,
                                                     'r6':nur_r6,
                                                     'r7':nur_r7,
                                                     'r8':nur_r8,
                                                     'r9':nur_r9,
                                                     'r10':nur_r10,
                                                     'r11':nur_r11,
                                                     'r12':nur_r12,
                                                     'r13':nur_r13,
                                                     'r14':nur_r14,
                                                     'r':nur_r,
                                                     'c1':nur_c1,
                                                     'c2':nur_c2,
                                                     'c3':nur_c3,
                                                     'c4':nur_c4,
                                                     'c5':nur_c5,
                                                     'c6':nur_c6,
                                                     'c7':nur_c7,
                                                     'c8':nur_c8,
                                                     'c9':nur_c9,
                                                     'c10':nur_c10,
                                                     'c11':nur_c11,
                                                     'c12':nur_c12,
                                                     'c13':nur_c13,
                                                     'c14':nur_c14,
                                                     'c':nur_c,
                                                     'rs':nur_rs,
                                                     'percentile':percentile_nur,
                                                     'category':nur_category,
                                                     'description':epps_personality_nur.description if epps_personality_nur else False
                                                    
                                                     }))
                
                
                
                
                chg_answer_r = []
                chg_answer_c = []
                chg_r1 = 0
                chg_r2 = 0
                chg_r3 = 0
                chg_r4 = 0
                chg_r5 = 0
                chg_r6 = 0
                chg_r7 = 0
                chg_r8 = 0
                chg_r9 = 0
                chg_r10 = 0
                chg_r11 = 0
                chg_r12 = 0
                chg_r13 = 0
                chg_r14 = 0
                chg_r = 0
                
                chg_c1 = 0
                chg_c2 = 0
                chg_c3 = 0
                chg_c4 = 0
                chg_c5 = 0
                chg_c6 = 0
                chg_c7 = 0
                chg_c8 = 0
                chg_c9 = 0
                chg_c10 = 0
                chg_c11 = 0
                chg_c12 = 0
                chg_c13 = 0
                chg_c14 = 0
                chg_c = 0
                
                
                if no_152:
                    if no_152.suggested_answer_id.code == "a":
                        chg_r1 = 1
                        chg_answer_r.append(chg_r1)
                if no_157:
                    if no_157.suggested_answer_id.code == "a":
                        chg_r2 = 1
                        chg_answer_r.append(chg_r2)
                if no_162:
                    if no_162.suggested_answer_id.code == "a":
                        chg_r3 = 1
                        chg_answer_r.append(chg_r3)
                if no_167:
                    if no_167.suggested_answer_id.code == "a":
                        chg_r4 = 1
                        chg_answer_r.append(chg_r4)
                if no_172:
                    if no_172.suggested_answer_id.code == "a":
                        chg_r5 = 1
                        chg_answer_r.append(chg_r5)
                if no_177:
                    if no_177.suggested_answer_id.code == "a":
                        chg_r6 = 1
                        chg_answer_r.append(chg_r6)
                if no_182:
                    if no_182.suggested_answer_id.code == "a":
                        chg_r7 = 1
                        chg_answer_r.append(chg_r7)
                if no_187:
                    if no_187.suggested_answer_id.code == "a":
                        chg_r8 = 1
                        chg_answer_r.append(chg_r8)
                if no_192:
                    if no_192.suggested_answer_id.code == "a":
                        chg_r9 = 1
                        chg_answer_r.append(chg_r9)
                if no_197:
                    if no_197.suggested_answer_id.code == "a":
                        chg_r10 = 1
                        chg_answer_r.append(chg_r10)
                if no_202:
                    if no_202.suggested_answer_id.code == "a":
                        chg_r11 = 1
                        chg_answer_r.append(chg_r11)
                if no_212:
                    if no_212.suggested_answer_id.code == "a":
                        chg_r12 = 1
                        chg_answer_r.append(chg_r12)
                if no_217:
                    if no_217.suggested_answer_id.code == "a":
                        chg_r13 = 1
                        chg_answer_r.append(chg_r13)
                if no_222:
                    if no_222.suggested_answer_id.code == "a":
                        chg_r14 = 1
                        chg_answer_r.append(chg_r14)
              
                
                chg_r = len(chg_answer_r)
                
                if no_56:
                    if no_56.suggested_answer_id.code == "b":
                        chg_c1 = 1
                        chg_answer_c.append(chg_c1)
                if no_57:
                    if no_57.suggested_answer_id.code == "b":
                        chg_c2 = 1
                        chg_answer_c.append(chg_c2)
                if no_58:
                    if no_58.suggested_answer_id.code == "b":
                        chg_c3 = 1
                        chg_answer_c.append(chg_c3)
                if no_59:
                    if no_59.suggested_answer_id.code == "b":
                        chg_c4 = 1
                        chg_answer_c.append(chg_c4)
                if no_60:
                    if no_60.suggested_answer_id.code == "b":
                        chg_c5 = 1
                        chg_answer_c.append(chg_c5)
                if no_131:
                    if no_131.suggested_answer_id.code == "b":
                        chg_c6 = 1
                        chg_answer_c.append(chg_c6)
                if no_132:
                    if no_132.suggested_answer_id.code == "b":
                        chg_c7 = 1
                        chg_answer_c.append(chg_c7)
                if no_133:
                    if no_133.suggested_answer_id.code == "b":
                        chg_c8 = 1
                        chg_answer_c.append(chg_c8)
                if no_134:
                    if no_134.suggested_answer_id.code == "b":
                        chg_c9 = 1
                        chg_answer_c.append(chg_c9)
                if no_135:
                    if no_135.suggested_answer_id.code == "b":
                        chg_c10 = 1
                        chg_answer_c.append(chg_c10)
                if no_206:
                    if no_206.suggested_answer_id.code == "b":
                        chg_c11 = 1
                        chg_answer_c.append(chg_c11)
                if no_208:
                    if no_208.suggested_answer_id.code == "b":
                        chg_c12 = 1
                        chg_answer_c.append(chg_c12)
                if no_209:
                    if no_209.suggested_answer_id.code == "b":
                        chg_c13 = 1
                        chg_answer_c.append(chg_c13)
                if no_210:
                    if no_210.suggested_answer_id.code == "b":
                        chg_c14 = 1
                        chg_answer_c.append(chg_c14)
                chg_c = len(chg_answer_c)
                
                chg_rs = chg_r + chg_c
                
                
                chg_category = ""
                if scoring_matrix:
                        line_data_chg = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == chg_rs)
                        percentile_chg = line_data_chg.change
                        chg_category = self.epps_personality_category(percentile_chg)
                        epps_personality_chg =  self.env['survey.epps_personality'].search([('sequence','=',12)])
                        personality_ids.append((0,0,{
                                                    'factor':"chg",
                                                    'r1':chg_r1,
                                                     'r2':chg_r2,
                                                     'r3':chg_r3,
                                                     'r4':chg_r4,
                                                     'r5':chg_r5,
                                                     'r6':chg_r6,
                                                     'r7':chg_r7,
                                                     'r8':chg_r8,
                                                     'r9':chg_r9,
                                                     'r10':chg_r10,
                                                     'r11':chg_r11,
                                                     'r12':chg_r12,
                                                     'r13':chg_r13,
                                                     'r14':chg_r14,
                                                     'r':chg_r,
                                                     'c1':chg_c1,
                                                     'c2':chg_c2,
                                                     'c3':chg_c3,
                                                     'c4':chg_c4,
                                                     'c5':chg_c5,
                                                     'c6':chg_c6,
                                                     'c7':chg_c7,
                                                     'c8':chg_c8,
                                                     'c9':chg_c9,
                                                     'c10':chg_c10,
                                                     'c11':chg_c11,
                                                     'c12':chg_c12,
                                                     'c13':chg_c13,
                                                     'c14':chg_c14,
                                                     'c':chg_c,
                                                     'rs':chg_rs,
                                                     'percentile':percentile_chg,
                                                     'category':chg_category,
                                                     'description':epps_personality_chg.description if epps_personality_chg else False
                                                    
                                                     }))
                
                
                
                end_answer_r = []
                end_answer_c = []
                end_r1 = 0
                end_r2 = 0
                end_r3 = 0
                end_r4 = 0
                end_r5 = 0
                end_r6 = 0
                end_r7 = 0
                end_r8 = 0
                end_r9 = 0
                end_r10 = 0
                end_r11 = 0
                end_r12 = 0
                end_r13 = 0
                end_r14 = 0
                end_r = 0
                
                end_c1 = 0
                end_c2 = 0
                end_c3 = 0
                end_c4 = 0
                end_c5 = 0
                end_c6 = 0
                end_c7 = 0
                end_c8 = 0
                end_c9 = 0
                end_c10 = 0
                end_c11 = 0
                end_c12 = 0
                end_c13 = 0
                end_c14 = 0
                end_c = 0
                
                
                if no_153:
                    if no_153.suggested_answer_id.code == "a":
                        end_r1 = 1
                        end_answer_r.append(end_r1)
                if no_158:
                    if no_158.suggested_answer_id.code == "a":
                        end_r2 = 1
                        end_answer_r.append(end_r2)
                if no_163:
                    if no_163.suggested_answer_id.code == "a":
                        end_r3 = 1
                        end_answer_r.append(end_r3)
                if no_168:
                    if no_168.suggested_answer_id.code == "a":
                        end_r4 = 1
                        end_answer_r.append(end_r4)
                if no_173:
                    if no_173.suggested_answer_id.code == "a":
                        end_r5 = 1
                        end_answer_r.append(end_r5)
                if no_178:
                    if no_178.suggested_answer_id.code == "a":
                        end_r6 = 1
                        end_answer_r.append(end_r6)
                if no_183:
                    if no_183.suggested_answer_id.code == "a":
                        end_r7 = 1
                        end_answer_r.append(end_r7)
                if no_188:
                    if no_188.suggested_answer_id.code == "a":
                        end_r8 = 1
                        end_answer_r.append(end_r8)
                if no_193:
                    if no_193.suggested_answer_id.code == "a":
                        end_r9 = 1
                        end_answer_r.append(end_r9)
                if no_198:
                    if no_198.suggested_answer_id.code == "a":
                        end_r10 = 1
                        end_answer_r.append(end_r10)
                if no_203:
                    if no_203.suggested_answer_id.code == "a":
                        end_r11 = 1
                        end_answer_r.append(end_r11)
                if no_208:
                    if no_208.suggested_answer_id.code == "a":
                        end_r12 = 1
                        end_answer_r.append(end_r12)
                if no_218:
                    if no_218.suggested_answer_id.code == "a":
                        end_r13 = 1
                        end_answer_r.append(end_r13)
                if no_223:
                    if no_223.suggested_answer_id.code == "a":
                        end_r14 = 1
                        end_answer_r.append(end_r14)
              
                
                end_r = len(end_answer_r)
                
                if no_61:
                    if no_61.suggested_answer_id.code == "b":
                        end_c1 = 1
                        end_answer_c.append(end_c1)
                if no_62:
                    if no_62.suggested_answer_id.code == "b":
                        end_c2 = 1
                        end_answer_c.append(end_c2)
                if no_63:
                    if no_63.suggested_answer_id.code == "b":
                        end_c3 = 1
                        end_answer_c.append(end_c3)
                if no_64:
                    if no_64.suggested_answer_id.code == "b":
                        end_c4 = 1
                        end_answer_c.append(end_c4)
                if no_65:
                    if no_65.suggested_answer_id.code == "b":
                        end_c5 = 1
                        end_answer_c.append(end_c5)
                if no_136:
                    if no_136.suggested_answer_id.code == "b":
                        end_c6 = 1
                        end_answer_c.append(end_c6)
                if no_137:
                    if no_137.suggested_answer_id.code == "b":
                        end_c7 = 1
                        end_answer_c.append(end_c7)
                if no_138:
                    if no_138.suggested_answer_id.code == "b":
                        end_c8 = 1
                        end_answer_c.append(end_c8)
                if no_139:
                    if no_139.suggested_answer_id.code == "b":
                        end_c9 = 1
                        end_answer_c.append(end_c9)
                if no_140:
                    if no_140.suggested_answer_id.code == "b":
                        end_c10 = 1
                        end_answer_c.append(end_c10)
                if no_211:
                    if no_211.suggested_answer_id.code == "b":
                        end_c11 = 1
                        end_answer_c.append(end_c11)
                if no_212:
                    if no_212.suggested_answer_id.code == "b":
                        end_c12 = 1
                        end_answer_c.append(end_c12)
                if no_214:
                    if no_214.suggested_answer_id.code == "b":
                        end_c13 = 1
                        end_answer_c.append(end_c13)
                if no_215:
                    if no_215.suggested_answer_id.code == "b":
                        end_c14 = 1
                        end_answer_c.append(end_c14)
                end_c = len(end_answer_c)
                
                end_rs = end_r + end_c
                
                
                end_category = ""
                if scoring_matrix:
                        line_data_end = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == end_rs)
                        percentile_end = line_data_end.endurance
                        end_category = self.epps_personality_category(percentile_end)
                        epps_personality_end =  self.env['survey.epps_personality'].search([('sequence','=',13)])
                        personality_ids.append((0,0,{
                                                    'factor':"end",
                                                    'r1':end_r1,
                                                     'r2':end_r2,
                                                     'r3':end_r3,
                                                     'r4':end_r4,
                                                     'r5':end_r5,
                                                     'r6':end_r6,
                                                     'r7':end_r7,
                                                     'r8':end_r8,
                                                     'r9':end_r9,
                                                     'r10':end_r10,
                                                     'r11':end_r11,
                                                     'r12':end_r12,
                                                     'r13':end_r13,
                                                     'r14':end_r14,
                                                     'r':end_r,
                                                     'c1':end_c1,
                                                     'c2':end_c2,
                                                     'c3':end_c3,
                                                     'c4':end_c4,
                                                     'c5':end_c5,
                                                     'c6':end_c6,
                                                     'c7':end_c7,
                                                     'c8':end_c8,
                                                     'c9':end_c9,
                                                     'c10':end_c10,
                                                     'c11':end_c11,
                                                     'c12':end_c12,
                                                     'c13':end_c13,
                                                     'c14':end_c14,
                                                     'c':end_c,
                                                     'rs':end_rs,
                                                     'percentile':percentile_end,
                                                     'category':end_category,
                                                     'description':epps_personality_end.description if epps_personality_end else False
                                                    
                                                     }))
                
                
                
                
                het_answer_r = []
                het_answer_c = []
                het_r1 = 0
                het_r2 = 0
                het_r3 = 0
                het_r4 = 0
                het_r5 = 0
                het_r6 = 0
                het_r7 = 0
                het_r8 = 0
                het_r9 = 0
                het_r10 = 0
                het_r11 = 0
                het_r12 = 0
                het_r13 = 0
                het_r14 = 0
                het_r = 0
                
                het_c1 = 0
                het_c2 = 0
                het_c3 = 0
                het_c4 = 0
                het_c5 = 0
                het_c6 = 0
                het_c7 = 0
                het_c8 = 0
                het_c9 = 0
                het_c10 = 0
                het_c11 = 0
                het_c12 = 0
                het_c13 = 0
                het_c14 = 0
                het_c = 0
                
                
                if no_154:
                    if no_154.suggested_answer_id.code == "a":
                        het_r1 = 1
                        het_answer_r.append(het_r1)
                if no_159:
                    if no_159.suggested_answer_id.code == "a":
                        het_r2 = 1
                        het_answer_r.append(het_r2)
                if no_164:
                    if no_164.suggested_answer_id.code == "a":
                        het_r3 = 1
                        het_answer_r.append(het_r3)
                if no_169:
                    if no_169.suggested_answer_id.code == "a":
                        het_r4 = 1
                        het_answer_r.append(het_r4)
                if no_174:
                    if no_174.suggested_answer_id.code == "a":
                        het_r5 = 1
                        het_answer_r.append(het_r5)
                if no_179:
                    if no_179.suggested_answer_id.code == "a":
                        het_r6 = 1
                        het_answer_r.append(het_r6)
                if no_184:
                    if no_184.suggested_answer_id.code == "a":
                        het_r7 = 1
                        het_answer_r.append(het_r7)
                if no_189:
                    if no_189.suggested_answer_id.code == "a":
                        het_r8 = 1
                        het_answer_r.append(end_r8)
                if no_194:
                    if no_194.suggested_answer_id.code == "a":
                        het_r9 = 1
                        het_answer_r.append(het_r9)
                if no_199:
                    if no_199.suggested_answer_id.code == "a":
                        het_r10 = 1
                        het_answer_r.append(het_r10)
                if no_204:
                    if no_204.suggested_answer_id.code == "a":
                        het_r11 = 1
                        het_answer_r.append(het_r11)
                if no_209:
                    if no_209.suggested_answer_id.code == "a":
                        het_r12 = 1
                        het_answer_r.append(het_r12)
                if no_214:
                    if no_214.suggested_answer_id.code == "a":
                        het_r13 = 1
                        het_answer_r.append(het_r13)
                if no_224:
                    if no_224.suggested_answer_id.code == "a":
                        het_r14 = 1
                        het_answer_r.append(het_r14)
              
                
                het_r = len(het_answer_r)
                
                if no_66:
                    if no_66.suggested_answer_id.code == "b":
                        het_c1 = 1
                        het_answer_c.append(het_c1)
                if no_67:
                    if no_67.suggested_answer_id.code == "b":
                        het_c2 = 1
                        het_answer_c.append(het_c2)
                if no_68:
                    if no_68.suggested_answer_id.code == "b":
                        het_c3 = 1
                        het_answer_c.append(het_c3)
                if no_69:
                    if no_69.suggested_answer_id.code == "b":
                        het_c4 = 1
                        het_answer_c.append(het_c4)
                if no_70:
                    if no_70.suggested_answer_id.code == "b":
                        het_c5 = 1
                        het_answer_c.append(het_c5)
                if no_141:
                    if no_141.suggested_answer_id.code == "b":
                        het_c6 = 1
                        het_answer_c.append(het_c6)
                if no_142:
                    if no_142.suggested_answer_id.code == "b":
                        het_c7 = 1
                        het_answer_c.append(het_c7)
                if no_143:
                    if no_143.suggested_answer_id.code == "b":
                        het_c8 = 1
                        het_answer_c.append(het_c8)
                if no_144:
                    if no_144.suggested_answer_id.code == "b":
                        het_c9 = 1
                        het_answer_c.append(het_c9)
                if no_145:
                    if no_145.suggested_answer_id.code == "b":
                        het_c10 = 1
                        het_answer_c.append(het_c10)
                if no_216:
                    if no_216.suggested_answer_id.code == "b":
                        het_c11 = 1
                        het_answer_c.append(het_c11)
                if no_217:
                    if no_217.suggested_answer_id.code == "b":
                        het_c12 = 1
                        het_answer_c.append(het_c12)
                if no_218:
                    if no_218.suggested_answer_id.code == "b":
                        het_c13 = 1
                        het_answer_c.append(het_c13)
                if no_220:
                    if no_220.suggested_answer_id.code == "b":
                        het_c14 = 1
                        het_answer_c.append(het_c14)
                het_c = len(het_answer_c)
                
                het_rs = het_r + het_c
                
                
                het_category = ""
                if scoring_matrix:
                        line_data_het = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == het_rs)
                        percentile_het = line_data_het.heterosextuality
                        het_category = self.epps_personality_category(percentile_het)
                        epps_personality_het =  self.env['survey.epps_personality'].search([('sequence','=',14)])
                        personality_ids.append((0,0,{
                                                    'factor':"het",
                                                    'r1':het_r1,
                                                     'r2':het_r2,
                                                     'r3':het_r3,
                                                     'r4':het_r4,
                                                     'r5':het_r5,
                                                     'r6':het_r6,
                                                     'r7':het_r7,
                                                     'r8':het_r8,
                                                     'r9':het_r9,
                                                     'r10':het_r10,
                                                     'r11':het_r11,
                                                     'r12':het_r12,
                                                     'r13':het_r13,
                                                     'r14':het_r14,
                                                     'r':het_r,
                                                     'c1':het_c1,
                                                     'c2':het_c2,
                                                     'c3':het_c3,
                                                     'c4':het_c4,
                                                     'c5':het_c5,
                                                     'c6':het_c6,
                                                     'c7':het_c7,
                                                     'c8':het_c8,
                                                     'c9':het_c9,
                                                     'c10':het_c10,
                                                     'c11':het_c11,
                                                     'c12':het_c12,
                                                     'c13':het_c13,
                                                     'c14':het_c14,
                                                     'c':het_c,
                                                     'rs':het_rs,
                                                     'percentile':percentile_het,
                                                     'category':het_category,
                                                     'description':epps_personality_het.description if epps_personality_het else False
                                                    
                                                     }))
                
                
                
                agg_answer_r = []
                agg_answer_c = []
                agg_r1 = 0
                agg_r2 = 0
                agg_r3 = 0
                agg_r4 = 0
                agg_r5 = 0
                agg_r6 = 0
                agg_r7 = 0
                agg_r8 = 0
                agg_r9 = 0
                agg_r10 = 0
                agg_r11 = 0
                agg_r12 = 0
                agg_r13 = 0
                agg_r14 = 0
                agg_r = 0
                
                agg_c1 = 0
                agg_c2 = 0
                agg_c3 = 0
                agg_c4 = 0
                agg_c5 = 0
                agg_c6 = 0
                agg_c7 = 0
                agg_c8 = 0
                agg_c9 = 0
                agg_c10 = 0
                agg_c11 = 0
                agg_c12 = 0
                agg_c13 = 0
                agg_c14 = 0
                agg_c = 0
                
                
                if no_155:
                    if no_155.suggested_answer_id.code == "a":
                        agg_r1 = 1
                        agg_answer_r.append(agg_r1)
                if no_160:
                    if no_160.suggested_answer_id.code == "a":
                        agg_r2 = 1
                        agg_answer_r.append(agg_r2)
                if no_165:
                    if no_165.suggested_answer_id.code == "a":
                        agg_r3 = 1
                        agg_answer_r.append(agg_r3)
                if no_170:
                    if no_170.suggested_answer_id.code == "a":
                        agg_r4 = 1
                        agg_answer_r.append(agg_r4)
                if no_175:
                    if no_175.suggested_answer_id.code == "a":
                        agg_r5 = 1
                        agg_answer_r.append(agg_r5)
                if no_180:
                    if no_180.suggested_answer_id.code == "a":
                        agg_r6 = 1
                        agg_answer_r.append(agg_r6)
                if no_185:
                    if no_185.suggested_answer_id.code == "a":
                        agg_r7 = 1
                        agg_answer_r.append(agg_r7)
                if no_190:
                    if no_190.suggested_answer_id.code == "a":
                        agg_r8 = 1
                        agg_answer_r.append(agg_r8)
                if no_195:
                    if no_195.suggested_answer_id.code == "a":
                        agg_r9 = 1
                        het_answer_r.append(agg_r9)
                if no_200:
                    if no_200.suggested_answer_id.code == "a":
                        agg_r10 = 1
                        agg_answer_r.append(agg_r10)
                if no_205:
                    if no_205.suggested_answer_id.code == "a":
                        agg_r11 = 1
                        agg_answer_r.append(agg_r11)
                if no_210:
                    if no_210.suggested_answer_id.code == "a":
                        agg_r12 = 1
                        agg_answer_r.append(agg_r12)
                if no_215:
                    if no_215.suggested_answer_id.code == "a":
                        agg_r13 = 1
                        agg_answer_r.append(agg_r13)
                if no_220:
                    if no_220.suggested_answer_id.code == "a":
                        agg_r14 = 1
                        agg_answer_r.append(agg_r14)
              
                
                agg_r = len(agg_answer_r)
                
                if no_71:
                    if no_71.suggested_answer_id.code == "b":
                        agg_c1 = 1
                        agg_answer_c.append(agg_c1)
                if no_72:
                    if no_72.suggested_answer_id.code == "b":
                        agg_c2 = 1
                        agg_answer_c.append(agg_c2)
                if no_73:
                    if no_73.suggested_answer_id.code == "b":
                        agg_c3 = 1
                        agg_answer_c.append(agg_c3)
                if no_74:
                    if no_74.suggested_answer_id.code == "b":
                        agg_c4 = 1
                        agg_answer_c.append(agg_c4)
                if no_75:
                    if no_75.suggested_answer_id.code == "b":
                        agg_c5 = 1
                        agg_answer_c.append(agg_c5)
                if no_146:
                    if no_146.suggested_answer_id.code == "b":
                        agg_c6 = 1
                        agg_answer_c.append(agg_c6)
                if no_147:
                    if no_147.suggested_answer_id.code == "b":
                        agg_c7 = 1
                        agg_answer_c.append(agg_c7)
                if no_148:
                    if no_148.suggested_answer_id.code == "b":
                        agg_c8 = 1
                        agg_answer_c.append(agg_c8)
                if no_149:
                    if no_149.suggested_answer_id.code == "b":
                        agg_c9 = 1
                        agg_answer_c.append(agg_c9)
                if no_150:
                    if no_150.suggested_answer_id.code == "b":
                        agg_c10 = 1
                        agg_answer_c.append(agg_c10)
                if no_221:
                    if no_221.suggested_answer_id.code == "b":
                        agg_c11 = 1
                        agg_answer_c.append(agg_c11)
                if no_222:
                    if no_222.suggested_answer_id.code == "b":
                        agg_c12 = 1
                        agg_answer_c.append(agg_c12)
                if no_223:
                    if no_223.suggested_answer_id.code == "b":
                        agg_c13 = 1
                        agg_answer_c.append(agg_c13)
                if no_224:
                    if no_224.suggested_answer_id.code == "b":
                        agg_c14 = 1
                        agg_answer_c.append(agg_c14)
                agg_c = len(agg_answer_c)
                
                agg_rs = agg_r + agg_c
                
                
                agg_category = ""
                if scoring_matrix:
                        line_data_agg = scoring_matrix.scoring_matrix_line_ids.filtered(lambda line:line.score == agg_rs)
                        percentile_agg = line_data_agg.aggression
                        agg_category = self.epps_personality_category(percentile_agg)
                        epps_personality_agg =  self.env['survey.epps_personality'].search([('sequence','=',15)])
                        personality_ids.append((0,0,{
                                                    'factor':"agg",
                                                    'r1':agg_r1,
                                                     'r2':agg_r2,
                                                     'r3':agg_r3,
                                                     'r4':agg_r4,
                                                     'r5':agg_r5,
                                                     'r6':agg_r6,
                                                     'r7':agg_r7,
                                                     'r8':agg_r8,
                                                     'r9':agg_r9,
                                                     'r10':agg_r10,
                                                     'r11':agg_r11,
                                                     'r12':agg_r12,
                                                     'r13':agg_r13,
                                                     'r14':agg_r14,
                                                     'r':agg_r,
                                                     'c1':agg_c1,
                                                     'c2':agg_c2,
                                                     'c3':agg_c3,
                                                     'c4':agg_c4,
                                                     'c5':agg_c5,
                                                     'c6':agg_c6,
                                                     'c7':agg_c7,
                                                     'c8':agg_c8,
                                                     'c9':agg_c9,
                                                     'c10':agg_c10,
                                                     'c11':agg_c11,
                                                     'c12':agg_c12,
                                                     'c13':agg_c13,
                                                     'c14':agg_c14,
                                                     'c':agg_c,
                                                     'rs':agg_rs,
                                                     'percentile':percentile_agg,
                                                     'category':agg_category,
                                                     'description':epps_personality_agg.description if epps_personality_agg else False
                                                    
                                                     }))
                        
                 
                record.epps_peronality_result_score_ids = personality_ids
                        
                
                                                  
                
                    
            
    


    def _get_mask_public_self(self):
        for record in self:
            if record.mask_public_self:
                line_ids = []
                for data in record.mask_public_self.personality.personality_ids:
                    line_ids.append((0,0,{'personality':data.personality}))
                if not record.mask_public_self_ids:
                    record.mask_public_self_ids = line_ids
                record.shadow_field_mask_public_self = True
            else:
                record.mask_public_self_ids = False
                record.shadow_field_mask_public_self = False

    def _get_mirror_perceived_self(self):
        for record in self:
            if record.mirror_perceived_self:
                line_ids = []
                for data in record.mirror_perceived_self.personality.personality_ids:
                    line_ids.append((0,0,{'personality':data.personality}))
                if not record.mirror_perceived_self_ids:
                    record.mirror_perceived_self_ids = line_ids
                record.personal_description = record.mirror_perceived_self.personality_description
                record.job_match = record.mirror_perceived_self.job_matches
                if not record.job_suggestion:
                    record.job_suggestion = record.mirror_perceived_self.job_suggestion
                record.shadow_field_mirror_perceived_self = True
            else:
                record.mirror_perceived_self_ids = False
                record.shadow_field_mirror_perceived_self = False

    def _get_core_private_self(self):
        for record in self:
            if record.core_private_self:
                line_ids = []
                for data in record.core_private_self.personality.personality_ids:
                    line_ids.append((0,0,{'personality':data.personality}))
                if not record.core_private_self_ids:
                    record.core_private_self_ids = line_ids
                record.shadow_field_core_private_self = True
            else:
                record.core_private_self_ids = False
                record.shadow_field_core_private_self = False





    def generate(self):
        for record in self:
            line_ids = []
            len_d_m = len([data.id for data in record.user_input_line_ids.filtered(lambda line:line.suggested_answer_id.value == 'M' and line.code == 'D' )])
            len_i_m = len([data.id for data in record.user_input_line_ids.filtered(lambda line:line.suggested_answer_id.value == 'M' and line.code == 'I' )])
            len_s_m = len([data.id for data in record.user_input_line_ids.filtered(lambda line:line.suggested_answer_id.value == 'M' and line.code == 'S' )])
            len_c_m = len([data.id for data in record.user_input_line_ids.filtered(lambda line:line.suggested_answer_id.value == 'M' and line.code == 'C' )])
            len_star_m = len([data.id for data in record.user_input_line_ids.filtered(lambda line:line.suggested_answer_id.value == 'M' and line.code == '*' )])
            total = len_d_m + len_i_m + len_s_m + len_c_m +len_star_m
            line_ids.append((0,0,{'line':1,'d_field':len_d_m,'i_field':len_i_m,'s_field':len_s_m,'c_field':len_c_m,'star_field':len_star_m,'total_field':total}))

            len_d_s = len([data.id for data in record.user_input_line_ids.filtered(lambda line: line.suggested_answer_id.value == 'L' and line.code == 'D')])
            len_i_s = len([data.id for data in record.user_input_line_ids.filtered(lambda line: line.suggested_answer_id.value == 'L' and line.code == 'I')])
            len_s_s = len([data.id for data in record.user_input_line_ids.filtered(lambda line: line.suggested_answer_id.value == 'L' and line.code == 'S')])
            len_c_s = len([data.id for data in record.user_input_line_ids.filtered(lambda line: line.suggested_answer_id.value == 'L' and line.code == 'C')])
            len_star_s = len([data.id for data in record.user_input_line_ids.filtered(lambda line: line.suggested_answer_id.value == 'L' and line.code == '*')])
            total_s = len_d_s+ len_i_s + len_s_s + len_c_s+ len_star_s
            line_ids.append((0, 0,{'line': 2, 'd_field': len_d_s, 'i_field': len_i_s, 's_field': len_s_s, 'c_field': len_c_s,'star_field': len_star_s, 'total_field': total_s}))
            line_ids.append((0, 0,{'line': 3, 'd_field': len_d_m - len_d_s, 'i_field': len_i_m - len_i_s, 's_field': len_s_m - len_s_s, 'c_field': len_c_m - len_c_s}))

            record.disc_result_ids = line_ids
            record.generate_score2()
            record.generate_score3()
            record.generate_final_score()
            record.is_hide_generate = True



    def generate_score2(self):
        for record in self:
            line_ids = []
            line_1 = record.disc_result_ids.filtered(lambda line:line.line==1)
            line_1_d_field = self.env['disc.scoring.matrix.line'].search([('score','=',line_1.d_field),('is_line_1','=',True)])
            line_1_i_field = self.env['disc.scoring.matrix.line'].search([('score','=',line_1.i_field),('is_line_1','=',True)])
            line_1_s_field = self.env['disc.scoring.matrix.line'].search([('score','=',line_1.s_field),('is_line_1','=',True)])
            line_1_c_field = self.env['disc.scoring.matrix.line'].search([('score','=',line_1.c_field),('is_line_1','=',True)])
            line_ids.append((0, 0,{'line': 1, 'd_field':line_1_d_field.d_field , 'i_field':line_1_i_field.i_field , 's_field':line_1_s_field.s_field , 'c_field': line_1_c_field.c_field}))
            line_2 = record.disc_result_ids.filtered(lambda line:line.line==2)
            line_2_d_field = self.env['disc.scoring.matrix.line'].search(
                [('score', '=', line_2.d_field), ('is_line_2', '=', True)])
            line_2_i_field = self.env['disc.scoring.matrix.line'].search(
                [('score', '=', line_2.i_field), ('is_line_2', '=', True)])
            line_2_s_field = self.env['disc.scoring.matrix.line'].search(
                [('score', '=', line_2.s_field), ('is_line_2', '=', True)])
            line_2_c_field = self.env['disc.scoring.matrix.line'].search(
                [('score', '=', line_2.c_field), ('is_line_2', '=', True)])
            line_ids.append((0, 0, {'line': 2, 'd_field': line_2_d_field.d_field, 'i_field': line_2_i_field.i_field,'s_field': line_2_s_field.s_field, 'c_field': line_2_c_field.c_field}))
            line_3 = record.disc_result_ids.filtered(lambda line: line.line == 3)
            line_3_d_field = self.env['disc.scoring.matrix.line'].search(
                [('score', '=', line_3.d_field), ('is_line_3', '=', True)])
            line_3_i_field = self.env['disc.scoring.matrix.line'].search(
                [('score', '=', line_3.i_field), ('is_line_3', '=', True)])
            line_3_s_field = self.env['disc.scoring.matrix.line'].search(
                [('score', '=', line_3.s_field), ('is_line_3', '=', True)])
            line_3_c_field = self.env['disc.scoring.matrix.line'].search(
                [('score', '=', line_3.c_field), ('is_line_3', '=', True)])
            line_ids.append((0, 0, {'line': 3, 'd_field': line_3_d_field.d_field, 'i_field': line_3_i_field.i_field,'s_field': line_3_s_field.s_field, 'c_field': line_3_c_field.c_field}))
            record.disc_result_score2_ids = line_ids


    def generate_score3(self):
        for record in self:
            score_ids = []
            score_ids_2 = []
            score_ids_3 = []
            line_ids = []
            c_1 = 0
            d_1 = 0
            c_d_1 = 0
            i_d_1 = 0
            i_d_c_1 = 0
            i_d_s_1 = 0
            i_s_d_1 = 0
            s_d_c_1 = 0
            d_c_1 = 0
            d_i_1 = 0
            d_i_s_1 = 0
            d_s_1 = 0
            c_i_s_1 = 0
            c_s_i_1 = 0
            i_s_c_i_c_s_1 = 0
            s_1 = 0
            c_s_1 = 0
            s_c_1 = 0
            d_i_c_1 = 0
            d_s_i_1 = 0
            d_s_c_1 = 0
            d_c_i_1 = 0
            d_c_s_1 =0
            i_1 = 0
            i_s_1 = 0
            i_c_1 = 0
            i_c_d_1 = 0
            i_c_s_1 = 0
            s_i_1 = 0
            s_d_1 = 0
            s_d_i_1 = 0
            s_i_d_1 = 0
            s_i_c_1 = 0
            s_c_d_1 = 0
            s_c_i_1 = 0
            c_i_1 = 0
            c_d_i_1 = 0
            c_d_s_1 = 0
            c_i_d_1 = 0
            c_s_d_1 = 0
            line_1 = record.disc_result_score2_ids.filtered(lambda line:line.line==1)
            D_1 = line_1.d_field
            I_1 = line_1.i_field
            S_1 = line_1.s_field
            C_1 = line_1.c_field

            if D_1 <= 0 and I_1 <= 0 and S_1 <= 0 and C_1 > 0:
                c_1 = 1
                score_ids.append(1)
            if D_1 > 0 and I_1 <= 0 and S_1 <= 0 and C_1 <= 0:
                d_1 = 1
                score_ids.append(2)
            if D_1 > 0 and I_1 <= 0 and S_1 <= 0 and C_1 > 0 and C_1 >= D_1:
                c_d_1 = 1
                score_ids.append(3)
            if D_1 > 0 and I_1 > 0 and S_1 <= 0 and C_1 <= 0 and I_1 >= D_1:
                i_d_1 = 1
                score_ids.append(4)
            if D_1 > 0 and I_1 > 0 and S_1 <= 0 and C_1 > 0 and I_1 >= D_1 >= C_1:
                i_d_c_1 = 1
                score_ids.append(5)
            if D_1 > 0 and I_1 > 0 and S_1 > 0 and C_1 <= 0 and I_1 >= D_1 >= S_1:
                i_d_s_1 = 1
                score_ids.append(6)
            if D_1 > 0 and I_1 > 0 and S_1 > 0 and C_1 <= 0 and I_1 >= S_1 >= D_1:
                i_s_d_1 = 1
                score_ids.append(7)
            if D_1 > 0 and I_1 <= 0 and S_1 > 0 and C_1 > 0 and S_1 >= D_1 and D_1 >= C_1:
                s_d_c_1 = 1
                score_ids.append(8)
            if D_1 > 0 and I_1 > 0 and S_1 <= 0 and C_1 <= 0 and D_1 >= I_1:
                d_c_1 = 1
                score_ids.append(9)
            if D_1 > 0 and I_1 > 0 and S_1 > 0 and C_1 <= 0 and D_1 >= I_1 >= S_1:
                d_i_1 = 1
                score_ids.append(10)
            if D_1 > 0 and I_1 <= 0 and S_1 > 0 and C_1 <= 0 and D_1 >=  S_1:
                d_i_s_1 = 1
                score_ids.append(11)
            if D_1 <= 0 and I_1 > 0 and S_1 > 0 and C_1 > 0 and C_1 >= I_1 >= S_1:
                d_s_1 = 1
                score_ids.append(12)
            if D_1 <= 0 and I_1 > 0 and S_1 > 0 and C_1 > 0 and C_1 >=  S_1 >= I_1:
                c_i_s_1 = 1
                score_ids.append(13)
            if D_1 <= 0 and I_1 > 0 and S_1 > 0 and C_1 > 0 and I_1 >=  S_1 and I_1 >= C_1:
                c_s_i_1 = 1
                score_ids.append(14)
            if D_1 <= 0 and I_1 <= 0 and S_1 > 0 and C_1 <= 0:
                i_s_c_i_c_s_1 = 1
                score_ids.append(15)
            if D_1 <= 0 and I_1 <= 0 and S_1 > 0 and C_1 > 0 and C_1 >= S_1:
                s_1 = 1
                score_ids.append(16)
            if D_1 <= 0 and I_1 <= 0 and S_1 > 0 and C_1 > 0 and S_1 >= C_1:
                c_s_1 = 1
                score_ids.append(17)
            if D_1 > 0 and I_1 <= 0 and S_1 <= 0 and C_1 > 0 and D_1 >= C_1:
                s_c_1 = 1
                score_ids.append(18)
            if D_1 > 0 and I_1 > 0 and S_1 <= 0 and C_1 > 0 and D_1 >= I_1 >= C_1:
                d_i_c_1 = 1
                score_ids.append(19)

            if D_1 > 0 and I_1 > 0 and S_1 > 0 and C_1 <= 0 and D_1 >= S_1 >= I_1:
                d_s_i_1 = 1
                score_ids.append(20)
            if D_1 > 0 and I_1 <= 0 and S_1 > 0 and C_1 > 0 and D_1 >= S_1 and S_1 >= C_1:
                d_s_c_1 = 1
                score_ids.append(21)
            if D_1 > 0 and I_1 >0 and S_1 <= 0 and C_1 > 0 and D_1 >= C_1 and C_1 >= I_1:
                d_c_i_1 = 1
                score_ids.append(22)
            if D_1 > 0 and I_1 <= 0 and S_1 > 0 and C_1 > 0 and D_1 >= C_1 and C_1 >= S_1:
                d_c_s_1 = 1
                score_ids.append(23)
            if D_1 <= 0 and I_1 > 0 and S_1 <= 0 and C_1 <= 0:
                i_1 = 1
                score_ids.append(24)
            if D_1 <= 0 and I_1 > 0 and S_1 > 0 and C_1 <= 0 and I_1>= S_1:
                i_s_1 = 1
                score_ids.append(25)

            if D_1 <= 0 and I_1 > 0 and S_1 <= 0 and C_1 > 0 and I_1 >= C_1:
                i_c_1 = 1
                score_ids.append(26)


            if D_1 > 0 and I_1 > 0 and S_1 <= 0 and C_1 > 0 and I_1 >= C_1 and C_1>=D_1:
                i_c_d_1 = 1
                score_ids.append(27)

            if D_1 <= 0 and I_1 > 0 and S_1 > 0 and C_1 > 0 and I_1 >= C_1 and C_1 >= S_1:
                i_c_s_1 = 1
                score_ids.append(28)

            if D_1 > 0 and I_1 <= 0 and S_1 > 0 and C_1 <= 0 and S_1 >= D_1:
                s_i_1 = 1
                score_ids.append(29)
            if D_1 <= 0 and I_1 > 0 and S_1 > 0 and C_1 <= 0 and S_1 >= I_1:
                s_d_1 = 1
                score_ids.append(30)
            if D_1 > 0 and I_1 > 0 and S_1 > 0 and C_1 <= 0 and S_1 >= D_1 and D_1 >= I_1:
                s_d_i_1 = 1
                score_ids.append(31)

            if D_1 > 0 and I_1 > 0 and S_1 > 0 and C_1 <= 0 and S_1 >= I_1 and I_1 >= D_1:
                s_i_d_1 = 1
                score_ids.append(32)
            if D_1 <= 0 and I_1 > 0 and S_1 > 0 and C_1 > 0 and S_1 >= I_1 and I_1 >= C_1:
                s_i_c_1 = 1
                score_ids.append(33)
            if D_1 > 0 and I_1 <= 0 and S_1 > 0 and C_1 > 0 and S_1 >= C_1 and C_1 >= D_1:
                s_c_d_1 = 1
                score_ids.append(34)
            if D_1 <= 0 and I_1 > 0 and S_1 > 0 and C_1 > 0 and S_1 >= C_1 and C_1 >= I_1:
                s_c_i_1 = 1
                score_ids.append(35)
            if D_1 <= 0 and I_1 > 0 and S_1 <= 0 and C_1 > 0 and C_1 >= I_1:
                c_i_1 = 1
                score_ids.append(36)
            if D_1 > 0 and I_1 > 0 and S_1 <= 0 and C_1 > 0 and C_1 >= D_1 and D_1 >= I_1:
                c_d_i_1 = 1
                score_ids.append(37)
            if D_1 > 0 and I_1 <= 0 and S_1 > 0 and C_1 > 0 and C_1 >= D_1 and D_1 >= S_1:
                c_d_s_1 = 1
                score_ids.append(38)
            if D_1 > 0 and I_1 > 0 and S_1 <= 0 and C_1 > 0 and C_1 >= I_1 and I_1 >= D_1:
                c_i_d_1 = 1
                score_ids.append(39)
            if D_1 > 0 and I_1 <= 0 and S_1 > 0 and C_1 > 0 and C_1 >= S_1 and S_1 >= D_1:
                c_s_d_1 = 1
                score_ids.append(40)
            line_ids.append((0,0,{'line':1,'c_fields':c_1,'d_fields':d_1,'c_d_fields':c_d_1,'i_d_fields':i_d_1,'i_d_c_fields':i_d_c_1,'i_d_s_fields':i_d_s_1,'i_s_d_fields':i_s_d_1,
                             's_d_c_fields':s_d_c_1,'d_i_fields':d_i_1,'d_i_s_fields':d_i_s_1,'d_s_fields':d_s_1,'c_i_s_fields':c_i_s_1,
                             'c_s_i_fields':c_s_i_1,
                             'i_s_c_i_c_s_fields':i_s_c_i_c_s_1,'s_fields':s_1,'c_s_fields':c_s_1,'s_c_fieds':s_c_1,'d_c_fields':d_c_1,'d_i_c_fields':d_i_c_1,
                             'd_s_i_fields':d_s_i_1,'d_s_c_fields':d_s_c_1,'d_c_i_fields':d_c_i_1,'d_c_s_fields':d_c_s_1,'i_fields':i_1,'i_s_fields':i_s_1,'i_c_fields':i_c_1,
                             'i_c_d_fields':i_c_d_1,'i_c_s_fields':i_c_s_1,'s_d_fields':s_d_1,'s_i_fields':s_i_1,'s_d_i_fields':s_d_i_1,'s_i_d_fields':s_i_d_1,'s_i_c_fields':s_i_c_1,
                             's_c_d_fields':s_c_d_1,'s_c_i_fields':s_c_i_1,'c_i_fields':c_i_1,'c_d_i_fields':c_d_i_1,'c_d_s_fields':c_d_s_1,'c_i_d_fields':c_i_d_1,'c_s_d_fields':c_s_d_1,
                             'match_score':min(score_ids) if score_ids else 0
                             }))



            c_2 = 0
            d_2 = 0
            c_d_2 = 0
            i_d_2 = 0
            i_d_c_2 = 0
            i_d_s_2 = 0
            i_s_d_2 = 0
            s_d_c_2 = 0
            d_c_2 = 0
            d_i_2 = 0
            d_i_s_2 = 0
            d_s_2 = 0
            c_i_s_2 = 0
            c_s_i_2 = 0
            i_s_c_i_c_s_2 = 0
            s_2 = 0
            c_s_2 = 0
            s_c_2 = 0
            d_i_c_2 = 0
            d_s_i_2 = 0
            d_s_c_2 = 0
            d_c_i_2 = 0
            d_c_s_2 =0
            i_2 = 0
            i_s_2 = 0
            i_c_2 = 0
            i_c_d_2 = 0
            i_c_s_2 = 0
            s_i_2 = 0
            s_d_2 = 0
            s_d_i_2 = 0
            s_i_d_2 = 0
            s_i_c_2 = 0
            s_c_d_2 = 0
            s_c_i_2 = 0
            c_i_2 = 0
            c_d_i_2 = 0
            c_d_s_2 = 0
            c_i_d_2 = 0
            c_s_d_2 = 0
            line_2 = record.disc_result_score2_ids.filtered(lambda line:line.line==2)
            D_2 = line_2.d_field
            I_2 = line_2.i_field
            S_2 = line_2.s_field
            C_2 = line_2.c_field

            if D_2 <= 0 and I_2 <= 0 and S_2 <=0 and C_2 > 0:
                c_2 = 1
                score_ids_2.append(1)
            if D_2 > 0 and I_2 <= 0 and S_2 <= 0 and C_2 <= 0:
                d_2 = 1
                score_ids_2.append(2)
            if D_2 > 0 and I_2 <= 0 and S_2 <= 0 and C_2 > 0 and C_2 >= D_2:
                c_d_2 = 1
                score_ids_2.append(3)
            if D_2 > 0 and I_2 > 0 and S_2 <= 0 and C_2 <= 0 and I_2 >= D_2:
                i_d_2 = 1
                score_ids_2.append(4)
            if D_2 > 0 and I_2 > 0 and S_2 <= 0 and C_2 > 0 and I_2 >= D_2>= C_2:
                i_d_c_2 = 1
                score_ids_2.append(5)
            if D_2 > 0 and I_2 > 0 and S_2 > 0 and C_2 <= 0 and I_2 >= D_2 >= S_2:
                i_d_s_2 = 1
                score_ids_2.append(6)
            if D_2 > 0 and I_2 > 0 and S_2 > 0 and C_2 <= 0 and I_2 >= S_2 >= D_2:
                i_s_d_2 = 1
                score_ids_2.append(7)
            if D_2 > 0 and I_2 <= 0 and S_2 > 0 and C_2 > 0 and S_2 >= D_2 and D_2 >= C_2:
                s_d_c_2 = 1
                score_ids_2.append(8)
            if D_2 > 0 and I_2 > 0 and S_2 <= 0 and C_2 <= 0 and D_2 >= I_2:
                d_c_2 = 1
                score_ids_2.append(9)
            if D_2 > 0 and I_2 > 0 and S_2 > 0 and C_2 <= 0 and D_2 >= I_2 >= S_2:
                d_i_2 = 1
                score_ids_2.append(10)
            if D_2 > 0 and I_2 <= 0 and S_2 > 0 and C_2 <= 0 and D_2 >=  S_2:
                d_i_s_2 = 1
                score_ids.append(11)
            if D_2 <= 0 and I_2 > 0 and S_2 > 0 and C_2 > 0 and C_2 >= I_2 >= S_2:
                d_s_2 = 1
                score_ids_2.append(12)
            if D_2 <= 0 and I_2 > 0 and S_2 > 0 and C_2 > 0 and C_2 >=  S_2 >= I_2:
                c_i_s_2 = 1
                score_ids_2.append(13)
            if D_2 <= 0 and I_2 > 0 and S_2 > 0 and C_2 > 0 and I_2 >=  S_2 and I_2 >= C_2:
                c_s_i_2 = 1
                score_ids_2.append(14)
            if D_2 <= 0 and I_2 <= 0 and S_2 > 0 and C_2 <= 0:
                i_s_c_i_c_s_2 = 1
                score_ids_2.append(15)
            if D_2 <= 0 and I_2 <= 0 and S_2 > 0 and C_2 > 0 and C_2 >= S_2:
                s_2 = 1
                score_ids_2.append(16)
            if D_2 <= 0 and I_2 <= 0 and S_2 > 0 and C_2 > 0 and S_2 >= C_2:
                c_s_2 = 1
                score_ids_2.append(17)
            if D_2 <= 0 and I_2 <= 0 and S_2 > 0 and C_2 > 0 and D_2 >= C_2:
                s_c_2 = 1
                score_ids_2.append(18)
            if D_2 > 0 and I_2 > 0 and S_2 <= 0 and C_2 > 0 and D_2 >= I_2 >= C_2:
                d_i_c_2 = 1
                score_ids_2.append(19)

            if D_2 > 0 and I_2 > 0 and S_2 > 0 and C_2 <= 0 and D_2 >= S_2 >= I_2:
                d_s_i_2 = 1
                score_ids_2.append(20)
            if D_2 > 0 and I_2 <= 0 and S_2 > 0 and C_2 > 0 and D_2 >= S_2 and S_2 >= C_2:
                d_s_c_2 = 1
                score_ids_2.append(21)
            if D_2 > 0 and I_2 >0 and S_2 <= 0 and C_2 > 0 and D_2 >= C_2 and C_2 >= I_2:
                d_c_i_2 = 1
                score_ids_2.append(22)
            if D_2 > 0 and I_2 <= 0 and S_2 > 0 and C_2 > 0 and D_2 >= C_2 and C_2 >= S_2:
                d_c_s_2 = 1
                score_ids_2.append(23)
            if D_2 <= 0 and I_2 > 0 and S_2 <= 0 and C_2 <= 0:
                i_2 = 1
                score_ids_2.append(24)
            if D_2 <= 0 and I_2 > 0 and S_2 > 0 and C_2 <= 0 and I_2 >= S_2:
                i_s_2 = 1
                score_ids_2.append(25)

            if D_2 <= 0 and I_2 > 0 and S_2 <= 0 and C_2 > 0 and I_2 >= C_2:
                i_c_2 = 1
                score_ids_2.append(26)


            if D_2 > 0 and I_2 > 0 and S_2 <= 0 and C_2 > 0 and I_2 >= C_2 and C_2 >= D_2:
                i_c_d_2 = 1
                score_ids_2.append(27)

            if D_2 <= 0 and I_2 > 0 and S_2 > 0 and C_2 > 0 and I_2 >= C_2 and C_2 >= S_2:
                i_c_s_2 = 1
                score_ids_2.append(28)
            if D_2 > 0 and I_2 <= 0 and S_2 > 0 and C_2 <= 0 and S_2 >= D_2:
                s_i_2 = 1
                score_ids_2.append(29)
            if D_2 <= 0 and I_2 > 0 and S_2 > 0 and C_2 <= 0 and S_2 >= I_2:
                s_d_2 = 1
                score_ids_2.append(30)
            if D_2 > 0 and I_2 > 0 and S_2 > 0 and C_2 <= 0 and S_2 >= D_2 and D_2 >= I_2:
                s_d_i_2 = 1
                score_ids_2.append(31)

            if D_2 > 0 and I_2 > 0 and S_2 > 0 and C_2 <= 0 and S_2 >= I_2 and I_2 >= D_2:
                s_i_d_2 = 1
                score_ids_2.append(32)
            if D_2 <= 0 and I_2 > 0 and S_2 > 0 and C_2 > 0 and S_2 >= I_2 and I_2 >= C_2:
                s_i_c_2 = 1
                score_ids_2.append(33)
            if D_2 > 0 and I_2 <= 0 and S_2 > 0 and C_2 > 0 and S_2 >= C_2 and C_2 >= D_2:
                s_c_d_2 = 1
                score_ids_2.append(34)
            if D_2 <= 0 and I_2 > 0 and S_2 > 0 and C_2 > 0 and S_2 >= C_2 and C_2 >= I_2:
                s_c_i_2 = 1
                score_ids_2.append(35)
            if D_2 <= 0 and I_2 > 0 and S_2 <= 0 and C_2 > 0 and C_2 >= I_2:
                c_i_2 = 1
                score_ids_2.append(36)
            if D_2 > 0 and I_2 > 0 and S_2 <= 0 and C_2 > 0 and C_2 >= D_2 and D_2 >= I_2:
                c_d_i_2 = 1
                score_ids_2.append(37)
            if D_2 > 0 and I_2 <= 0 and S_2 > 0 and C_2 > 0 and C_2 >= D_2 and D_2 >= S_2:
                c_d_s_2 = 1
                score_ids_2.append(38)
            if D_2 > 0 and I_2 > 0 and S_2 <= 0 and C_2 > 0 and C_2 >= I_2 and I_2 >= D_2:
                c_i_d_2 = 1
                score_ids_2.append(39)
            if D_2 > 0 and I_2 <= 0 and S_2 > 0 and C_2 > 0 and C_2 >= S_2 and S_2 >= D_2:
                c_s_d_2 = 1
                score_ids_2.append(40)
            line_ids.append((0,0,{'line':2,'c_fields':c_2,'d_fields':d_2,'c_d_fields':c_d_2,'i_d_fields':i_d_2,'i_d_c_fields':i_d_c_2,'i_d_s_fields':i_d_s_2,'i_s_d_fields':i_s_d_2,
                             's_d_c_fields':s_d_c_2,'d_i_fields':d_i_2,'d_i_s_fields':d_i_s_2,'d_s_fields':d_s_2,'c_i_s_fields':c_i_s_2,
                             'c_s_i_fields':c_s_i_2,
                             'i_s_c_i_c_s_fields':i_s_c_i_c_s_2,'s_fields':s_2,'c_s_fields':c_s_2,'s_c_fieds':s_c_2,'d_c_fields':d_c_2,'d_i_c_fields':d_i_c_2,
                             'd_s_i_fields':d_s_i_2,'d_s_c_fields':d_s_c_2,'d_c_i_fields':d_c_i_2,'d_c_s_fields':d_c_s_2,'i_fields':i_2,'i_s_fields':i_s_2,'i_c_fields':i_c_2,
                             'i_c_d_fields':i_c_d_2,'i_c_s_fields':i_c_s_2,'s_d_fields':s_d_2,'s_i_fields':s_i_2,'s_d_i_fields':s_d_i_2,'s_i_d_fields':s_i_d_2,'s_i_c_fields':s_i_c_2,
                             's_c_d_fields':s_c_d_2,'s_c_i_fields':s_c_i_2,'c_i_fields':c_i_2,'c_d_i_fields':c_d_i_2,'c_d_s_fields':c_d_s_2,'c_i_d_fields':c_i_d_2,'c_s_d_fields':c_s_d_2,
                             'match_score':min(score_ids_2) if score_ids_2 else 0
                             }))

            c_3 = 0
            d_3 = 0
            c_d_3 = 0
            i_d_3 = 0
            i_d_c_3 = 0
            i_d_s_3 = 0
            i_s_d_3 = 0
            s_d_c_3 = 0
            d_c_3 = 0
            d_i_3 = 0
            d_i_s_3 = 0
            d_s_3 = 0
            c_i_s_3 = 0
            c_s_i_3 = 0
            i_s_c_i_c_s_3 = 0
            s_3 = 0
            c_s_3 = 0
            s_c_3 = 0
            d_i_c_3 = 0
            d_s_i_3 = 0
            d_s_c_3 = 0
            d_c_i_3 = 0
            d_c_s_3 = 0
            i_3 = 0
            i_s_3 = 0
            i_c_3 = 0
            i_c_d_3 = 0
            i_c_s_3 = 0
            s_i_3 = 0
            s_d_3 = 0
            s_d_i_3 = 0
            s_i_d_3 = 0
            s_i_c_3 = 0
            s_c_d_3 = 0
            s_c_i_3 = 0
            c_i_3 = 0
            c_d_i_3 = 0
            c_d_s_3 = 0
            c_i_d_3 = 0
            c_s_d_3 = 0
            line_3 = record.disc_result_score2_ids.filtered(lambda line: line.line == 3)
            D_3 = line_3.d_field
            I_3 = line_3.i_field
            S_3 = line_3.s_field
            C_3 = line_3.c_field

            if D_3 <= 0 and I_3 <= 0 and S_3 <= 0 and C_3 > 0:
                c_3 = 1
                score_ids_3.append(1)
            if D_3 > 0 and I_3 <= 0 and S_3 <= 0 and C_3 <= 0:
                d_3 = 1
                score_ids_3.append(2)
            if D_3 > 0 and I_3 <= 0 and S_3 <= 0 and C_3 > 0 and C_3 >= D_3:
                c_d_3 = 1
                score_ids_3.append(3)
            if D_3 > 0 and I_3 > 0 and S_3 <= 0 and C_3 <= 0 and I_3 >= D_3:
                i_d_3 = 1
                score_ids_3.append(4)
            if D_3 > 0 and I_3 > 0 and S_3 <= 0 and C_3 > 0 and I_3 >= D_3 >= C_3:
                i_d_c_3 = 1
                score_ids_3.append(5)
            if D_3 > 0 and I_3 > 0 and S_3 > 0 and C_3 <= 0 and I_3 >= D_3 >= S_3:
                i_d_s_3 = 1
                score_ids_3.append(6)
            if D_3 > 0 and I_3 > 0 and S_3 > 0 and C_3 <= 0 and I_3 >= S_3 >= D_3:
                i_s_d_3 = 1
                score_ids_3.append(7)
            if D_3 > 0 and I_3 <= 0 and S_3 > 0 and C_3 > 0 and S_3 >= D_3 and D_3 >= C_3:
                s_d_c_3 = 1
                score_ids_3.append(8)
            if D_3 > 0 and I_3 > 0 and S_3 <= 0 and C_3 <= 0 and D_3 >= I_3:
                d_c_3 = 1
                score_ids_3.append(9)
            if D_3 > 0 and I_3 > 0 and S_3 > 0 and C_3 <= 0 and D_3 >= I_3 >= S_3:
                d_i_3 = 1
                score_ids_3.append(10)
            if D_3 > 0 and I_3 <= 0 and S_3 > 0 and C_3 <= 0 and D_3 >= S_3:
                d_i_s_3 = 1
                score_ids_3.append(11)
            if D_3 <= 0 and I_3 > 0 and S_3 > 0 and C_3 > 0 and C_3 >= I_3 >= S_3:
                d_s_3 = 1
                score_ids_3.append(12)
            if D_3 <= 0 and I_3 > 0 and S_3 > 0 and C_3 > 0 and C_3 >= S_3 >= I_3:
                c_i_s_3 = 1
                score_ids_3.append(13)
            if D_3 <= 0 and I_3 > 0 and S_3 > 0 and C_3 > 0 and I_3 >= S_3 and I_3 >= C_3:
                c_s_i_3 = 1
                score_ids_3.append(14)
            if D_3 <= 0 and I_3 <= 0 and S_3 > 0 and C_3 <= 0:
                i_s_c_i_c_s_3 = 1
                score_ids_3.append(15)
            if D_3 <= 0 and I_3 <= 0 and S_3 > 0 and C_3 > 0 and C_3 >= S_3:
                s_3 = 1
                score_ids_3.append(16)
            if D_3 <= 0 and I_3 <= 0 and S_3 > 0 and C_3 > 0 and S_3 >= C_3:
                c_s_3 = 1
                score_ids_3.append(17)
            if D_3 > 0 and I_3 <= 0 and S_3 <= 0 and C_3 > 0 and D_3 >= C_3:
                s_c_3 = 1
                score_ids_3.append(18)
            if D_3 > 0 and I_3 > 0 and S_3 <= 0 and C_3 > 0 and D_3 >= I_3 >= C_3:
                d_i_c_3 = 1
                score_ids_3.append(19)

            if D_3 > 0 and I_3 > 0 and S_3 > 0 and C_3 <= 0 and D_3 >= S_3 >= I_3:
                d_s_i_3 = 1
                score_ids_3.append(20)
            if D_3 > 0 and I_3 <= 0 and S_3 > 0 and C_3 > 0 and D_3 >= S_3 and S_3 >= C_3:
                d_s_c_3 = 1
                score_ids_3.append(21)
            if D_3 > 0 and I_3 > 0 and S_3 <= 0 and C_3 > 0 and D_3 >= C_3 and C_3 >= I_3:
                d_c_i_3 = 1
                score_ids_3.append(22)
            if D_3 > 0 and I_3 <= 0 and S_3 > 0 and C_3 > 0 and D_3 >= C_3 and C_3 >= S_3:
                d_c_s_3 = 1
                score_ids_3.append(23)
            if D_3 <= 0 and I_3 > 0 and S_3 <= 0 and C_3 <= 0 :
                i_3 = 1
                score_ids_3.append(24)
            if D_3 <= 0 and I_3 > 0 and S_3 > 0 and C_3 <= 0 and I_3 >= S_3:
                i_s_3 = 1
                score_ids_3.append(25)

            if D_3 <= 0 and I_3 > 0 and S_3 <= 0 and C_3 > 0 and I_3 >= C_3:
                i_c_3 = 1
                score_ids_3.append(26)

            if D_3 > 0 and I_3 > 0 and S_3 <= 0 and C_3 > 0 and I_3 >= C_3 and C_3 >= D_3:
                i_c_d_3 = 1
                score_ids_3.append(27)

            if D_3 <= 0 and I_3 > 0 and S_3 > 0 and C_3 > 0 and I_3 >= C_3 and C_3 >= S_3:
                i_c_s_3 = 1
                score_ids_3.append(28)
            if D_3 > 0 and I_3 <= 0 and S_3 > 0 and C_3 <= 0 and S_3 >= D_3:
                s_i_3 = 1
                score_ids_3.append(29)
            if D_3 <= 0 and I_3 > 0 and S_3 > 0 and C_3 <= 0 and S_3 >= I_3:
                s_d_3 = 1
                score_ids_3.append(30)
            if D_3 > 0 and I_3 > 0 and S_3 > 0 and C_3 <= 0 and S_3 >= D_3 and D_3 >= I_3:
                s_d_i_3 = 1
                score_ids_3.append(31)

            if D_3 > 0 and I_3 > 0 and S_3 > 0 and C_3 <= 0 and S_3 >= I_3 and I_3 >= D_3:
                s_i_d_3 = 1
                score_ids_3.append(32)
            if D_3 <= 0 and I_3 > 0 and S_3 > 0 and C_3 > 0 and S_3 >= I_3 and I_2 >= C_3:
                s_i_c_3 = 1
                score_ids_3.append(33)
            if D_3 > 0 and I_3 <= 0 and S_3 > 0 and C_3 > 0 and S_3 >= C_3 and C_3 >= D_3:
                s_c_d_3 = 1
                score_ids_3.append(34)
            if D_3 <= 0 and I_3 > 0 and S_3 > 0 and C_3 > 0 and S_3 >= C_3 and C_3 >= I_3:
                s_c_i_3 = 1
                score_ids_3.append(35)
            if D_3 <= 0 and I_3 > 0 and S_3 <= 0 and C_3 > 0 and C_3 >= I_3:
                c_i_3 = 1
                score_ids_3.append(36)
            if D_3 > 0 and I_3 > 0 and S_3 <= 0 and C_3 > 0 and C_3 >= D_3 and D_3 >= I_3:
                c_d_i_3 = 1
                score_ids_3.append(37)
            if D_3 > 0 and I_3 <= 0 and S_3 > 0 and C_3 > 0 and C_3 >= D_3 and D_3 >= S_3:
                c_d_s_3 = 1
                score_ids_3.append(38)
            if D_3 > 0 and I_3 > 0 and S_3 <= 0 and C_3 > 0 and C_3 >= I_3 and I_3 >= D_3:
                c_i_d_3 = 1
                score_ids_2.append(39)
            if D_3 > 0 and I_3 <= 0 and S_3 > 0 and C_3 > 0 and C_3 >= S_3 and S_3 >= D_3:
                c_s_d_3 = 1
                score_ids_3.append(40)
            line_ids.append((0, 0,
                             {'line': 3, 'c_fields': c_3, 'd_fields': d_3, 'c_d_fields': c_d_3, 'i_d_fields': i_d_3,
                              'i_d_c_fields': i_d_c_3, 'i_d_s_fields': i_d_s_3, 'i_s_d_fields': i_s_d_3,
                              's_d_c_fields': s_d_c_3, 'd_i_fields': d_i_3, 'd_i_s_fields': d_i_s_3,
                              'd_s_fields': d_s_3, 'c_i_s_fields': c_i_s_3,
                              'c_s_i_fields': c_s_i_3,
                              'i_s_c_i_c_s_fields': i_s_c_i_c_s_3, 's_fields': s_3, 'c_s_fields': c_s_3,
                              's_c_fieds': s_c_3, 'd_c_fields': d_c_3, 'd_i_c_fields': d_i_c_3,
                              'd_s_i_fields': d_s_i_3, 'd_s_c_fields': d_s_c_3, 'd_c_i_fields': d_c_i_3,
                              'd_c_s_fields': d_c_s_3, 'i_fields': i_3, 'i_s_fields': i_s_3, 'i_c_fields': i_c_3,
                              'i_c_d_fields': i_c_d_3, 'i_c_s_fields': i_c_s_3, 's_d_fields': s_d_3,
                              's_i_fields': s_i_3, 's_d_i_fields': s_d_i_3, 's_i_d_fields': s_i_d_3,
                              's_i_c_fields': s_i_c_3,
                              's_c_d_fields': s_c_d_3, 's_c_i_fields': s_c_i_3, 'c_i_fields': c_i_3,
                              'c_d_i_fields': c_d_i_3, 'c_d_s_fields': c_d_s_3, 'c_i_d_fields': c_i_d_3,
                              'c_s_d_fields': c_s_d_3,
                              'match_score': min(score_ids_3) if score_ids_3 else 0
                              }))


            record.disc_result_score3_ids = line_ids


    def generate_final_score(self):
        for record in self:
            if record.disc_result_score3_ids:
                line_1 = record.disc_result_score3_ids.filtered(lambda line:line.line == 1)
                line_2 = record.disc_result_score3_ids.filtered(lambda line:line.line == 2)
                line_3 = record.disc_result_score3_ids.filtered(lambda line:line.line == 3)
                mask_public_self = self.env['survey.disc.variables'].search([('sequence','=',line_1.match_score)])
                if mask_public_self:
                    record.mask_public_self = mask_public_self.id
                core_private_self = self.env['survey.disc.variables'].search([('sequence','=',line_2.match_score)])
                if core_private_self:
                    record.core_private_self = core_private_self.id
                mirror_perceived_self = self.env['survey.disc.variables'].search([('sequence', '=', line_3.match_score)])
                if mirror_perceived_self:
                    record.mirror_perceived_self = mirror_perceived_self.id




    @api.depends('survey_id')
    def _get_survey_type(self):
        for record in self:
            if record.survey_id:
                record.survey_type = str(record.survey_id.survey_type).upper()
            else:
                record.survey_type = False




    def save_lines(self, question, answer, comment=None):
        """ Save answers to questions, depending on question type

            If an answer already exists for question and user_input_id, it will be
            overwritten (or deleted for 'choice' questions) (in order to maintain data consistency).
        """
        old_answers = self.env['survey.user_input.line'].search([
            ('user_input_id', '=', self.id),
            ('question_id', '=', question.id)
        ])

        if question.question_type in ['char_box', 'text_box', 'numerical_box', 'date', 'datetime']:
            self._save_line_simple_answer(question, old_answers, answer)
            if question.save_as_email and answer:
                self.write({'email': answer})
            if question.save_as_nickname and answer:
                self.write({'nickname': answer})

        elif question.question_type in ['simple_choice', 'multiple_choice','epps']:
            self._save_line_choice(question, old_answers, answer, comment)
        elif question.question_type == 'matrix':
            self._save_line_matrix(question, old_answers, answer, comment)

        elif question.question_type == 'disc':
            self._save_line_disc(question, old_answers, answer, comment)
        elif question.question_type == 'video':
            self._save_video(question, old_answers, answer, comment)
        else:
            raise AttributeError(question.question_type + ": This type of question has no saving function")
        
        
    def _save_video(self,question, old_answers, answers, comment):
        print(question)
        print(old_answers)
        print(answers)
        print(comment)

    def _save_line_choice(self, question, old_answers, answers, comment):
        if not (isinstance(answers, list)):
            answers = [answers]
        vals_list = []

        if question.question_type == 'simple_choice':
            if not question.comment_count_as_answer or not question.comments_allowed or not comment:
                vals_list = [self._get_line_answer_values(question, answer, 'suggestion') for answer in answers]
        elif question.question_type == 'epps':
            vals_list = [self._get_line_answer_values(question, answer, 'suggestion') for answer in answers]
        elif question.question_type == 'multiple_choice':
            vals_list = [self._get_line_answer_values(question, answer, 'suggestion') for answer in answers]

        if comment:
            vals_list.append(self._get_line_comment_values(question, comment))

        old_answers.sudo().unlink()
        return self.env['survey.user_input.line'].create(vals_list)


    def _save_line_disc(self, question, old_answers, answers, comment):
        vals_list = []

        if answers:
            for row_key, row_answer in answers.items():
                for answer in row_answer:
                    vals = self._get_line_answer_values(question, answer, 'suggestion')
                    vals['matrix_row_id'] = int(row_key)
                    vals['disc_row_id'] = int(row_key)
                    vals_list.append(vals.copy())

        if comment:
            vals_list.append(self._get_line_comment_values(question, comment))

        old_answers.sudo().unlink()
        return self.env['survey.user_input.line'].create(vals_list)

class equip3SurveyEppsConsistencyResult(models.Model):
    _name = 'survey.consistency.result'
    survey_user_input =  fields.Many2one('survey.user_input')
    factor = fields.Char()
    c1 = fields.Integer()
    c2 = fields.Integer()
    c3 = fields.Integer()
    c4 = fields.Integer()
    c5 = fields.Integer()
    c6 = fields.Integer()
    c7 = fields.Integer()
    c8 = fields.Integer()
    c9 = fields.Integer()
    c10 = fields.Integer()
    c11 = fields.Integer()
    c12 = fields.Integer()
    c13 = fields.Integer()
    c14 = fields.Integer()
    c15 = fields.Integer()
    score = fields.Integer()
    percentile = fields.Integer()
    category = fields.Char()
    


class equip3SurveyInterviewSkillsResult(models.Model):
    _name = 'survey.interview.skill.result'
    survey_user_input =  fields.Many2one('survey.user_input')
    question = fields.Char()
    comment = fields.Char()
    score = fields.Selection([('0','Zero'),('1','Worst'),('2','Poor'),('3','Average'),('4','Good'),('5','Excellent')])
    
class equip3SurveyInterviewPersonalitysResult(models.Model):
    _name = 'survey.interview.personality.result'
    survey_user_input =  fields.Many2one('survey.user_input')
    question = fields.Char()
    comment = fields.Char()
    score = fields.Selection([('0','Zero'),('1','Worst'),('2','Poor'),('3','Average'),('4','Good'),('5','Excellent')])
    
    
class equip3SurveyEppsPersonalityResult(models.Model):
    _name = 'survey.personality.result'
    survey_user_input =  fields.Many2one('survey.user_input')
    factor = fields.Char()
    r1 = fields.Integer()
    r2 = fields.Integer()
    r3 = fields.Integer()
    r4 = fields.Integer()
    r5 = fields.Integer()
    r6 = fields.Integer()
    r7 = fields.Integer()
    r8 = fields.Integer()
    r9 = fields.Integer()
    r10 = fields.Integer()
    r11 = fields.Integer()
    r12 = fields.Integer()
    r13 = fields.Integer()
    r14 = fields.Integer()
    r = fields.Integer()
    
    c1 = fields.Integer()
    c2 = fields.Integer()
    c3 = fields.Integer()
    c4 = fields.Integer()
    c5 = fields.Integer()
    c6 = fields.Integer()
    c7 = fields.Integer()
    c8 = fields.Integer()
    c9 = fields.Integer()
    c10 = fields.Integer()
    c11 = fields.Integer()
    c12 = fields.Integer()
    c13 = fields.Integer()
    c14 = fields.Integer()
    c = fields.Integer()
    rs = fields.Integer()
    
    description = fields.Text()
    x_description_limited = fields.Text(compute="_compute_x_description_limited", stored=True)
    percentile = fields.Integer()
    category = fields.Char()
    
    
    
    @api.depends('description')
    def _compute_x_description_limited(self):
        for record in self:
            if record.description:
                if len(record.description) > 60:
                    record['x_description_limited'] = f"{record.description[:60]}..."
                else:
                    record['x_description_limited'] = record.description
            else:
                record['x_description_limited'] = False
    
    
    
class Equip3SurveyInheritSurveyUserInputLine(models.Model):
    _inherit = 'survey.user_input.line'
    _order = 'question_id'
    disc_row_id = fields.Many2one('survey.question.master.answer', string="Row answer")
    code = fields.Char("Code",compute='_get_code')
    is_skip = fields.Boolean(default=False)
    is_hide_code = fields.Boolean(deafult=False,compute='_get_survey_type')

    def _get_survey_type(self):
        for record in self:
            if record.user_input_id:
                if record.user_input_id.survey_type != 'DISC':
                    record.is_hide_code = True
                else:
                    record.is_hide_code =False
            else:
                record.is_hide_code = False






    @api.depends('user_input_id','suggested_answer_id','matrix_row_id')
    def _get_code(self):
        for record in self:
            if record.user_input_id.survey_type =='DISC' and record.suggested_answer_id and record.matrix_row_id:

                if str(record.suggested_answer_id.value).upper() == 'M' and not record.is_skip:
                    record.code = str(record.matrix_row_id.code_m)
                elif str(record.suggested_answer_id.value).upper() == 'L' and not record.is_skip:
                    record.code = str(record.matrix_row_id.code_l)
                elif str(record.suggested_answer_id.value).upper() == 'M' and  record.is_skip:
                    record.code = "*"
                elif str(record.suggested_answer_id.value).upper() == 'L' and  record.is_skip:
                    record.code = "*"
                else:
                    record.code = "*"
            else:
                record.code = "*"

class surveyInputPersonalityLine(models.Model):
    _name = 'survey.input.personality.line'
    mask_public_self = fields.Many2one('survey.user_input')
    core_private_self = fields.Many2one('survey.user_input')
    mirror_perceived_self = fields.Many2one('survey.user_input')
    personality = fields.Char()



