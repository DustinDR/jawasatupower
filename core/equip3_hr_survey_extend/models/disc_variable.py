
from odoo import models,fields


class surveyDiscVariables(models.Model):
    _name = 'survey.disc.variables'
    _rec_name = 'personality'
    sequence = fields.Integer()
    code = fields.Char()
    personality = fields.Many2one('disc.personality.root')
    personality_description = fields.Text()
    job_matches = fields.Text()
    job_suggestion = fields.Many2many('hr.job')
