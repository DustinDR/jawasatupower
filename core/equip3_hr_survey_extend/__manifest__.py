# -*- coding: utf-8 -*-
{
    'name': "Equip3 HR Survey Extend",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Hashmicro",
    'website': "https://www.hashmicro.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Human Resources/Survey',
    'version': '1.1.31',

    # any module necessary for this one to work correctly
    'depends': ['base', 'survey', 'hr', 'web'],
    "external_dependencies": {
        "python": ['plotly'],
    },

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'report/report.xml',
        'data/data.xml',
        'data/admin_question_data.xml',
        'data/interview_question_data.xml',
        'data/epps_scoring_matrix.xml',
        'data/disc_scoring_matrix_.xml',
        'data/data_epps.xml',
        'data/data_epps_personality.xml',
        'data/disc_personality_data.xml',
        'data/disc_variables_data.xml',
        'data/javascript_data.xml',
        'data/accounting_data.xml',
        'data/accounting_data indonesia.xml',
        'data/html_question_data.xml',
        'data/python_question_data.xml',
        'data/basic_programming_data.xml',
        'data/cognitive_question_data.xml',
        'data/seo_specialist_data.xml',
        'data/social_media_data.xml',
        'data/erp_question_data.xml',
        'data/digital_marketing_data.xml',
        'data/english_test_data.xml',
        'data/cognitive_v2_question_data.xml',
        'data/inventory_test_data.xml',
        'data/tax_question_data.xml',
        'data/erp_v2_question_data.xml',
        'data/sales_data.xml',
        'data/driver_test_data.xml',
        'data/customer_service_data.xml',
        'data/product_display_data.xml',
        'data/financial_administration_question_data.xml',
        'data/kasir_question_data.xml',
        'data/qa_question_data.xml',
        'data/php_question_data.xml',
        'data/administrasi_transaksi_data.xml',
        'data/product_owner_data.xml',
        'data/project_management_question_data.xml',
        'data/business_intelligence_data.xml',
        'data/ITIL_data.xml',
        'data/linux_system_administrator_data.xml',
        'data/uix_design_question_data.xml',
        'data/aws_question_data.xml',
        'data/business_analyst_question_data.xml',
        'data/data_science_question_data.xml',


        'views/templates.xml',
        'views/template/disc_question_templates.xml',
        'views/template/survey_templates.xml',
        'views/template/question_epps_template.xml',
        'views/template/video_templates.xml',
        'views/survey_questions_views.xml',
        'views/survey_survey.xml',
        'views/psychological_test.xml',
        'views/survey_user_input.xml',
        'views/survey_user_input_line.xml',
        'views/disc_personality.xml',
        'views/disc_variable.xml',
        'views/disc_matrix.xml',
        'views/survey_link_replace.xml',
        'views/survey_epps_scoring_matrix.xml',
        'views/survey_epps_personality.xml',
        'views/survey_personality_result.xml',
        'views/survey_interview.xml',
        'views/assets.xml',


    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
