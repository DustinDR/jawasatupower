# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Equip3 Inventory Access Right Setting',
    'author': 'Hashmicro / Prince',
    'version': '1.1.11',
    'summary': 'Manage your Inventory Access Right Setting.',
    'depends': ['base', 'stock','purchase', 'product_expiry','equip3_general_setting','dynamic_barcode_labels'],
    'category': 'Inventory/Inventory',
    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'views/res_config_settings.xml',
        'views/menu.xml',
        'views/access_rights_profile_view.xml',
        'views/res_user_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
