
from odoo import _, api, fields, models


class ResUsers(models.Model):
    _inherit = 'res.users'

    access_rights_profile_id = fields.Many2one('access.rights.profile', string="Access Rights Profile")

    def set_groups(self,values):
        if values.get('access_rights_profile_id'):
            group_obj = self.env['access.rights.profile'].browse(values.get('access_rights_profile_id'))
            values['groups_id'] = [(6,0,group_obj.group_ids.ids)]
        return values

    @api.model
    def create(self, values):
        values = self.set_groups(values)
        return super(ResUsers, self).create(values)

    def write(self, values):
        values = self.set_groups(values)
        return super(ResUsers, self).write(values)