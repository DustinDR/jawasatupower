odoo.define("app_web_superbar.RangeControlPanelModelExtension", function (require) {
    "use strict";

    const ActionModel = require("web/static/src/js/views/action_model.js");
    const ControlPanelModelExtension = require("web/static/src/js/control_panel/control_panel_model_extension.js");
    const session = require('web.session');
    const time = require('web.time');
    const core = require('web.core');
    const _t = core._t;

    class RangeControlPanelModelExtension extends ControlPanelModelExtension {
        getDomain()  {
            const ret = super.getDomain(...arguments);
            var appDomain = this.app_search();
            try {
                _.each(appDomain, function (domain) {
                    ret.push(domain);
                });
            } catch (e) {}
            return ret;
        }
        //返回范围数组
        app_search() {
            var self = this;
            var domain = [];
            // 注意，date和datetime型的处理是不同的，已处理完
            var $search_date = $('.app-search-range-date-container');
            if ($search_date.length) {
                var $sd = $search_date.find('input');
                var start_date = $sd.eq(0).val(),
                    end_date = $sd.eq(1).val(),
                    field = $(document).find('.app_select_field_date').val(),
                    field_type = 'datetime';
                var tz = session.user_context.tz,
                    start_utc,
                    end_utc;

                _.each(self.fields, function (value, key, list) {
                    if (value.name == field) {
                        field_type = value.type;
                        return false;
                    }
                });

                // odoo 14处理时区方式改变，不可用 moment，参考 https://www.cnblogs.com/goloving/p/10514914.html
                moment.locale(tz);
                let _dif = new Date().getTimezoneOffset();
                var l10n = _t.database.parameters;
                if (start_date) {
                    if (field_type === 'date') {
                        //日期类型，无须utc处理
                        start_date = moment(moment(start_date, time.strftime_to_moment_format(l10n.date_format))).format('YYYY-MM-DD');
                        domain.push([field, '>=', start_date]);
                    } else {
                        //日期时间，处理utc
                        start_date = moment(moment(start_date, time.strftime_to_moment_format(l10n.date_format))).format('YYYY-MM-DD 00:00:00');
                        start_utc = moment(start_date).add(_dif, "minutes").format('YYYY-MM-DD HH:mm:ss');
                        domain.push([field, '>=', start_utc]);
                    }
                }
                if (end_date) {
                    if (field_type === 'date') {
                        end_date = moment(moment(end_date, time.strftime_to_moment_format(l10n.date_format))).format('YYYY-MM-DD');
                        domain.push([field, '<=', end_date]);
                    } else {
                        //日期时间，处理utc
                        end_date = moment(moment(end_date, time.strftime_to_moment_format(l10n.date_format))).format('YYYY-MM-DD 00:00:00');
                        end_utc = moment(end_date).add(_dif, "minutes").format('YYYY-MM-DD HH:mm:ss');
                        domain.push([field, '<=', end_utc]);
                    }
                }
            }

            if ($(document).find('.app_select_field_number')) {
                var start_range = $(document).find('.app_start_number').val(),
                    end_range = $(document).find('.app_end_number').val(),
                    range_field = $(document).find('.app_select_field_number').val();

                if (start_range) {
                    domain.push([range_field, '>=', parseInt(start_range)]);
                }
                if (end_range) {
                    domain.push([range_field, '<=', parseInt(end_range)]);
                }
            }
            return domain;
        }
    }
    ActionModel.registry.add("RangeControlPanel", RangeControlPanelModelExtension, 10);

    return RangeControlPanelModelExtension;
});
