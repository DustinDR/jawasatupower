
from odoo import api, fields, models, _

class AccountTax(models.Model):
    _inherit = 'account.tax'

    tax_paid_account = fields.Many2one('account.account', string='Tax Paid Account', tracking=True)
    price_include_total = fields.Boolean(string="Included in Price Based on Total Amount", tracking=True)
    pay_separately = fields.Boolean(string="To Pay Separately", tracking=True)