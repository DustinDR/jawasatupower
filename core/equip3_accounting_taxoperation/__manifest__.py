
{
    'name': 'Accounting Tax Operation',
    'version': '1.1.2',
    'author': 'Hashmicro / Prince',
    'category' : 'Accounting',
    'depends': [
        'account',
        'analytic'
    ],
    'data': [
       'views/account_tax_views.xml',
       'views/account_move_views.xml',
    ],
    'demo': [],
    'auto_install': False,
    'installable': True,
    'application': True
}