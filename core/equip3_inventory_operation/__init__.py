# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import models
from . import wizard

from odoo import api, SUPERUSER_ID

def create_picking_type_dashboard_cards(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    env['stock.picking.type']._create_dashboard_cards()
