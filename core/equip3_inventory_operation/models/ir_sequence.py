
from odoo import SUPERUSER_ID, _, api, fields, models
from datetime import date
from odoo.exceptions import ValidationError


class IrSequence(models.Model):
    _inherit = "ir.sequence"

    @api.model
    def create(self, values):
        res = super(IrSequence, self).create(values)
        operation_type_ids = self.env['stock.picking.type'].search([
                    ('sequence_id', '=', res.id)
                ])
        if res.prefix:
            prefix = res.prefix.split('/')
            if operation_type_ids or ('IN' in prefix) or ('OUT' in prefix):
                res.prefix += '/%(y)s/%(month)s/%(day)s/'
        return res
