
from odoo import models, fields, api, _


class StockQuantPackage(models.Model):
    _inherit = 'stock.quant.package'

    move_location_id = fields.Many2one('stock.location', string='Move Location')
