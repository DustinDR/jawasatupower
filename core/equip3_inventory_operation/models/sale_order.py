
from odoo import models, fields, api, SUPERUSER_ID, _
from odoo.tools.float_utils import float_compare, float_round
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare, float_round, float_is_zero
from collections import defaultdict, namedtuple
from odoo.addons.stock.models.stock_rule import ProcurementException

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def _action_confirm(self):
        context = dict(self.env.context) or {}
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        product_service_operation = IrConfigParam.get_param('is_product_service_operation', False)
        context.update({'is_product_service_operation': product_service_operation})
        self = self.with_context(context)
        return super(SaleOrder, self)._action_confirm()

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    def _action_launch_stock_rule(self, previous_product_uom_qty=False):
        """
        Launch procurement group run method with required/custom fields genrated by a
        sale order line. procurement group will launch '_run_pull', '_run_buy' or '_run_manufacture'
        depending on the sale order line product rule.
        """
        context = dict(self.env.context) or {}
        if context.get('is_product_service_operation'):
            precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
            procurements = []
            for line in self:
                line = line.with_company(line.company_id)
                if line.state != 'sale' or not line.product_id.type in ('consu','product'):
                    if not line.product_id.is_product_service_operation: 
                        continue
                qty = line._get_qty_procurement(previous_product_uom_qty)
                if float_compare(qty, line.product_uom_qty, precision_digits=precision) >= 0:
                    continue

                group_id = line._get_procurement_group()
                if not group_id:
                    group_id = self.env['procurement.group'].create(line._prepare_procurement_group_vals())
                    line.order_id.procurement_group_id = group_id
                else:
                    # In case the procurement group is already created and the order was
                    # cancelled, we need to update certain values of the group.
                    updated_vals = {}
                    if group_id.partner_id != line.order_id.partner_shipping_id:
                        updated_vals.update({'partner_id': line.order_id.partner_shipping_id.id})
                    if group_id.move_type != line.order_id.picking_policy:
                        updated_vals.update({'move_type': line.order_id.picking_policy})
                    if updated_vals:
                        group_id.write(updated_vals)

                values = line._prepare_procurement_values(group_id=group_id)
                product_qty = line.product_uom_qty - qty
                line_uom = line.product_uom
                quant_uom = line.product_id.uom_id
                product_qty, procurement_uom = line_uom._adjust_uom_quantities(product_qty, quant_uom)
                procurements.append(self.env['procurement.group'].Procurement(
                    line.product_id, product_qty, procurement_uom,
                    line.order_id.partner_shipping_id.property_stock_customer,
                    line.name, line.order_id.name, line.order_id.company_id, values))
            if procurements:
                self.env['procurement.group'].run(procurements)
            return True
        else:
            return super(SaleOrderLine, self)._action_launch_stock_rule(previous_product_uom_qty=False)

class ProcurementGroup(models.Model):
    _inherit = 'procurement.group'

    @api.model
    def run(self, procurements, raise_user_error=True):
        """Fulfil `procurements` with the help of stock rules.

        Procurements are needs of products at a certain location. To fulfil
        these needs, we need to create some sort of documents (`stock.move`
        by default, but extensions of `_run_` methods allow to create every
        type of documents).

        :param procurements: the description of the procurement
        :type list: list of `~odoo.addons.stock.models.stock_rule.ProcurementGroup.Procurement`
        :param raise_user_error: will raise either an UserError or a ProcurementException
        :type raise_user_error: boolan, optional
        :raises UserError: if `raise_user_error` is True and a procurement isn't fulfillable
        :raises ProcurementException: if `raise_user_error` is False and a procurement isn't fulfillable
        """
        context = dict(self.env.context) or {}
        if context.get('is_product_service_operation'):
            def raise_exception(procurement_errors):
                if raise_user_error:
                    dummy, errors = zip(*procurement_errors)
                    raise UserError('\n'.join(errors))
                else:
                    raise ProcurementException(procurement_errors)

            actions_to_run = defaultdict(list)
            procurement_errors = []
            for procurement in procurements:
                procurement.values.setdefault('company_id', procurement.location_id.company_id)
                procurement.values.setdefault('priority', '0')
                procurement.values.setdefault('date_planned', fields.Datetime.now())
                if (
                    procurement.product_id.type not in ('consu', 'product') or
                    float_is_zero(procurement.product_qty, precision_rounding=procurement.product_uom.rounding)
                ):
                    if not procurement.product_id.is_product_service_operation:
                        continue
                rule = self._get_rule(procurement.product_id, procurement.location_id, procurement.values)
                if not rule:
                    error = _('No rule has been found to replenish "%s" in "%s".\nVerify the routes configuration on the product.') %\
                        (procurement.product_id.display_name, procurement.location_id.display_name)
                    procurement_errors.append((procurement, error))
                else:
                    action = 'pull' if rule.action == 'pull_push' else rule.action
                    actions_to_run[action].append((procurement, rule))

            if procurement_errors:
                raise_exception(procurement_errors)

            for action, procurements in actions_to_run.items():
                if hasattr(self.env['stock.rule'], '_run_%s' % action):
                    try:
                        getattr(self.env['stock.rule'], '_run_%s' % action)(procurements)
                    except ProcurementException as e:
                        procurement_errors += e.procurement_exceptions
                else:
                    _logger.error("The method _run_%s doesn't exist on the procurement rules" % action)

            if procurement_errors:
                raise_exception(procurement_errors)
            return True
        else:
            #return super(ProcurementGroup, self).run(procurements, raise_user_error=True)
            return super(ProcurementGroup, self).run(procurements, raise_user_error=raise_user_error)


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    def button_approve(self, force=False):
        context = dict(self.env.context) or {}
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        product_service_operation = IrConfigParam.get_param('is_product_service_operation', False)
        context.update({'is_product_service_operation': product_service_operation})
        self = self.with_context(context)
        return super(PurchaseOrder, self).button_approve(force=force)

    def _create_picking_service(self):
        StockPicking = self.env['stock.picking']
        for order in self.filtered(lambda po: po.state in ('purchase', 'done')):
            if any(product.type in ['service'] for product in order.order_line.product_id):
                order = order.with_company(order.company_id)
                pickings = order.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel'))
                if not pickings:
                    res = order._prepare_picking()
                    picking = StockPicking.with_user(SUPERUSER_ID).create(res)
                else:
                    picking = pickings[0]
                moves = order.order_line._create_stock_moves_service(picking)
                moves = moves.filtered(lambda x: x.state not in ('done', 'cancel'))._action_confirm()
                seq = 0
                for move in sorted(moves, key=lambda move: move.date):
                    seq += 5
                    move.sequence = seq
                moves._action_assign()
                picking.message_post_with_view('mail.message_origin_link',
                    values={'self': picking, 'origin': order},
                    subtype_id=self.env.ref('mail.mt_note').id)

    def _create_picking(self):
        res = super(PurchaseOrder, self)._create_picking()
        context = dict(self.env.context) or {}
        if context.get('is_product_service_operation'):
            self._create_picking_service()
        return res

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    def _create_stock_moves_service(self, picking):
        values = []
        for line in self.filtered(lambda l: not l.display_type):
            for val in line._prepare_stock_moves_services(picking):
                values.append(val)
            line.move_dest_ids.created_purchase_line_id = False

        return self.env['stock.move'].create(values)

    def _prepare_stock_moves_services(self, picking):
        """ Prepare the stock moves data for one order line. This function returns a list of
        dictionary ready to be used in stock.move's create()
        """
        context = dict(self.env.context) or {}
        if context.get('is_product_service_operation'):
            self.ensure_one()
            res = []
            if self.product_id.type in ['product', 'consu'] or not self.product_id.is_product_service_operation:
                return res

            qty = 0.0
            price_unit = self._get_stock_move_price_unit()
            outgoing_moves, incoming_moves = self._get_outgoing_incoming_moves()
            for move in outgoing_moves:
                qty -= move.product_uom._compute_quantity(move.product_uom_qty, self.product_uom, rounding_method='HALF-UP')
            for move in incoming_moves:
                qty += move.product_uom._compute_quantity(move.product_uom_qty, self.product_uom, rounding_method='HALF-UP')

            move_dests = self.move_dest_ids
            if not move_dests:
                move_dests = self.move_ids.move_dest_ids.filtered(lambda m: m.state != 'cancel' and not m.location_dest_id.usage == 'supplier')

            if not move_dests:
                qty_to_attach = 0
                qty_to_push = self.product_qty - qty
            else:
                move_dests_initial_demand = self.product_id.uom_id._compute_quantity(
                    sum(move_dests.filtered(lambda m: m.state != 'cancel' and not m.location_dest_id.usage == 'supplier').mapped('product_qty')),
                    self.product_uom, rounding_method='HALF-UP')
                qty_to_attach = move_dests_initial_demand - qty
                qty_to_push = self.product_qty - move_dests_initial_demand

            if float_compare(qty_to_attach, 0.0, precision_rounding=self.product_uom.rounding) > 0:
                product_uom_qty, product_uom = self.product_uom._adjust_uom_quantities(qty_to_attach, self.product_id.uom_id)
                res.append(self._prepare_stock_move_vals(picking, price_unit, product_uom_qty, product_uom))
            if float_compare(qty_to_push, 0.0, precision_rounding=self.product_uom.rounding) > 0:
                product_uom_qty, product_uom = self.product_uom._adjust_uom_quantities(qty_to_push, self.product_id.uom_id)
                extra_move_vals = self._prepare_stock_move_vals(picking, price_unit, product_uom_qty, product_uom)
                extra_move_vals['move_dest_ids'] = False  # don't attach
                res.append(extra_move_vals)
            return res