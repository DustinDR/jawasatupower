# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import datetime
from datetime import date, time
from re import findall as regex_findall
from re import split as regex_split
from operator import itemgetter
from string import digits
import string
from odoo import SUPERUSER_ID, _, api, fields, models
from odoo.exceptions import ValidationError, UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, format_datetime
from odoo.exceptions import Warning 
import xlwt
import base64
import tempfile
import os
from io import BytesIO
from xlrd import open_workbook, xldate_as_tuple
import tempfile
import binascii
import re
from collections import defaultdict
import pytz
from lxml import etree
from odoo.tools.float_utils import float_compare, float_is_zero, float_round
from odoo.tools import float_repr


class Picking(models.Model):
    _inherit = "stock.picking"

    warehouse_id = fields.Many2one('stock.warehouse', string="Warehouse")
    is_interwarehouse_transfer = fields.Boolean(string="Interwarehouse Transfer")

    is_transfer_in = fields.Boolean(string="Transit In", readonly=True)
    is_transfer_out = fields.Boolean(string="Transit Out", readonly=True)
    transfer_id = fields.Many2one('internal.transfer', string='Internal Transfer')
    partner_id = fields.Many2one(
        'res.partner', 'Contact',
        check_company=True, tracking=True,
        states={'done': [('readonly', True)], 'cancel': [('readonly', True)]})
    location_id = fields.Many2one(
        'stock.location', "Source Location",
        default=lambda self: self.env['stock.picking.type'].browse(self._context.get('default_picking_type_id')).default_location_src_id,
        check_company=True, readonly=True, required=True, tracking=True,
        states={'draft': [('readonly', False)]})
    date_done = fields.Datetime('Date of Transfer', copy=False, tracking=True, readonly=True, help="Date at which the transfer has been processed or cancelled.")
    origin = fields.Char(
        'Source Document', index=True, tracking=True,
        states={'done': [('readonly', True)], 'cancel': [('readonly', True)]},
        help="Reference of the document")
    company_id = fields.Many2one(
        'res.company', string='Company', tracking=True,
        readonly=True, store=True, index=True)
    is_show_location = fields.Boolean(string='Is Show Location', compute='_compute_show_location')
    branch_id = fields.Many2one('res.branch', string="Branch", tracking=True)
    filter_branch_ids = fields.Many2many('res.branch', string="Branches", compute='_compute_branch_ids', store=False)
    filter_source_location_ids = fields.Many2many('stock.location', compute='_compute_location')
    filter_dest_location_ids = fields.Many2many('stock.location', compute='_compute_location')
    filter_operation_picking_type_ids = fields.Many2many('stock.picking.type', compute='_compute_picking_type')
    journal_cancel = fields.Boolean(string="Journal Cancel", compute='_compute_journal_entry', store=True)
    cancel_reason = fields.Text(string="Cancel Reason")
    merge_info = fields.Html(string="Merge Info", readonly=True)
    is_return_orders = fields.Boolean(string="Return Order", compute="_compute_is_return_order", store=False)
    internal_transfer_line_id = fields.Many2one('internal.transfer.line')
    return_date_limit = fields.Datetime("Return before", copy=False, readonly=True)
    analytic_account_group_ids = fields.Many2many('account.analytic.tag', 'stock_analytic_rel', 'tag_id', 'picking_id', copy=True, string="Analytic Groups", default=lambda self: self.env.user.analytic_tag_ids.ids)
    origin_src_location = fields.Char('Origin Source Location', readonly='1')
    origin_dest_location = fields.Char('Origin Destination Location', readonly='1')
    check_second_picking_reserve = fields.Boolean(compute='second_picking_reserve')
    process_time = fields.Char(compute="_compute_process_time", string='Processed Time', store=True, help="The time it takes to complete a transfer from Draft until Done state.")
    process_time_hours = fields.Float(compute="_compute_process_time", string='Processed Time', store=True, help="The time it takes to complete a transfer from Draft until Done state.")
    transfer_log_activity_ids = fields.One2many('transfer.log.activity', 'reference', string='Transfer Log Activity Ids')
    delivery_package_level_ids = fields.One2many('stock.package_level', 'picking_id')
    show_entire_packs = fields.Boolean(related='picking_type_id.show_entire_packs')
    mr_id = fields.Many2one('material.request', string="Material Request")
    is_mbs_on_transfer_operations = fields.Boolean(string="Transfer Operations", compute="_compute_is_mbs_on_transfer_operations", store=False)
    is_source_package = fields.Boolean(string="Source Package", compute="_compute_source_package", store=False)

    def _compute_source_package(self):
        for record in self:
            record.is_source_package = False
            if all(move_line.package_id for move_line in record.move_line_ids_without_package):
                record.is_source_package = True
            else:
                if all(move_id.packaging_ids for move_id in record.move_ids_without_package):
                    record.is_source_package = True

    @api.depends('transfer_log_activity_ids')
    def _compute_process_time(self):
        for res in self:
            total_seconds = 0
            for log_line in res.transfer_log_activity_ids:
                time = str(log_line.process_time).split(':')
                if len(time) == 3:
                    total_seconds += (float(time[0]) * 60 * 60) + (float(time[1]) * 60) + (float(time[2]))
            seconds = total_seconds
            seconds_in_day = 60 * 60 * 24
            seconds_in_hour = 60 * 60
            seconds_in_minute = 60
            days = seconds // seconds_in_day
            hours = (seconds - (days * seconds_in_day)) // seconds_in_hour
            minutes = (seconds - (days * seconds_in_day) - (hours * seconds_in_hour)) // seconds_in_minute
            res.process_time = str(int(days)) + ' Days ' + str(int(hours)) + ' Hours ' + str(int(minutes)) + ' Minutes'
            res.process_time_hours = sum(res.transfer_log_activity_ids.mapped('process_time_hours'))

    def _get_process_time(self):
        time = fields.datetime.now() - self.transfer_log_activity_ids[-1].timestamp
        days, seconds = time.days, time.seconds
        hours = days * 24 + seconds // 3600
        minutes = (seconds % 3600) // 60
        seconds = seconds % 60
        second = str('0' + str(seconds)) if seconds < 10 else str(seconds)
        minute = str('0' + str(minutes)) if minutes < 10 else str(minutes)
        hour = str('0' + str(hours)) if hours < 10 else str(hours)
        return hour + ':' + minute + ':' + second

    @api.model
    def action_internal_transfer_menu(self):
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        internal_type = IrConfigParam.get_param('internal_type', "with_transit")
        if internal_type == 'with_transit':
            self.env.ref('equip3_inventory_operation.menu_internal_transfer_in').active = True
            self.env.ref('equip3_inventory_operation.menu_internal_transfer_out').active = True
            self.env.ref('equip3_inventory_operation.menu_internal_transfer_notes').active = False
        else:
            self.env.ref('equip3_inventory_operation.menu_internal_transfer_in').active = False
            self.env.ref('equip3_inventory_operation.menu_internal_transfer_out').active = False
            self.env.ref('equip3_inventory_operation.menu_internal_transfer_notes').active = True

    def _get_processed_hours(self):
        time = fields.datetime.now() - self.transfer_log_activity_ids[-1].timestamp
        hours = time.total_seconds() / 3600
        return hours

    def _get_processed_days(self, process_time):
        time = str(process_time).split(':')
        total_seconds = 0
        if len(time) == 3:
            total_seconds += (float(time[0]) * 60 * 60) + (float(time[1]) * 60) + (float(time[2]))
        return round(total_seconds / 86400.00, 2)

    def _compute_branch_ids(self):
        user = self.env.user
        for record in self:
            branch_ids = user.branch_ids.ids + user.branch_id.ids
            record.filter_branch_ids = [(6 ,0, branch_ids)]

    def transfer_log_action_assign(self):
        line_vals = []
        for rec in self:
            line_vals.append((0, 0, {'state': rec.state,
                                     'action' : "Create record",
                                     'timestamp': fields.datetime.now(),
                                     'reference': rec.id,
                                     'process_days': 00,
                                     'process_time_hours': 0,
                                     'process_time': '00:00:00',
                                     'user_id': self.env.user.id, 
                                     }))
            rec.transfer_log_activity_ids = line_vals

    def transfer_log_action(self):
        line_vals = []
        for rec in self:
            if rec.transfer_log_activity_ids:
                last_record_transfer_activity = rec.transfer_log_activity_ids[-1]

                before_state_value = dict(rec.fields_get(
                        allfields=['state'])['state']['selection'])[last_record_transfer_activity.state]
                after_state_value = dict(rec.fields_get(
                        allfields=['state'])['state']['selection'])[rec.state]
                action = before_state_value + ' To ' + after_state_value
                process_time = self._get_process_time()
                process_days = self._get_processed_days(process_time)
                line_vals.append((0, 0, {
                                        'state': rec.state,
                                        'action' : action,
                                        'timestamp': fields.datetime.now(),
                                        'reference': rec.id,
                                        'process_time': process_time,
                                        'process_time_hours': rec._get_processed_hours(),
                                        'process_days': process_days,
                                        'user_id':self.env.user.id,}))
                rec.transfer_log_activity_ids = line_vals
                if rec.state in ('waiting', 'confirmed', 'assigned'):
                    for move in rec.move_ids_without_package:
                        move.scheduled_date = move.date

    @api.onchange('warehouse_id')
    def _onchange_warehouse_id(self):
        if self.warehouse_id:
            self.picking_type_id = self.warehouse_id.int_type_id.id
            self.company_id = self.warehouse_id.company_id.id
            self.branch_id = self.warehouse_id.branch_id.id
        
    @api.onchange('picking_type_id', 'partner_id')
    def onchange_picking_type(self):
        result = super(Picking, self).onchange_picking_type()
        self._compute_branch_ids()
        if self.picking_type_id and self.is_interwarehouse_transfer and self.warehouse_id:
            self.location_id = False
            self.location_dest_id = False
            self._compute_location()
        if not self.warehouse_id:
            self.company_id = self.picking_type_id.company_id.id
        return result

    def second_picking_reserve(self):
        for record in self:
            # → Automatically reserve the quantity in the 2nd stock.picking operation (Virtual Location/Transit → Destination Location)
            if record.transfer_id and record.is_transfer_in:
                picking_id = self.env['stock.picking'].search([('transfer_id', '=', record.transfer_id.id), ('is_transfer_out', '=', True), ('origin','=',record.transfer_id.name), ('state','=','done')])
                if picking_id:
                    if record.state == 'draft':
                        for picking in picking_id:
                            record.action_confirm()
                            record.action_assign()
                            record.check_second_picking_reserve = True
                    else:
                        record.check_second_picking_reserve = False
                else:
                    record.check_second_picking_reserve = False
            else:
                record.check_second_picking_reserve = False

    def _reset_sequence(self):
        for rec in self:
            current_sequence = 3
            for line in rec.move_ids_without_package:
                line.sequence = current_sequence
                current_sequence += 1
            current_sequence = 1
            for line in rec.move_line_ids_without_package:
                line.move_line_sequence = current_sequence
                current_sequence += 1

    @api.model
    def default_get(self,fields):
        context = dict(self.env.context)
        res = super(Picking, self).default_get(fields)
        if context.get('picking_type_code') == 'outgoing':
            res['location_dest_id'] = self.env.ref('stock.stock_location_customers').id
        elif context.get('picking_type_code') == 'incoming':
            res['location_id'] = self.env.ref('stock.stock_location_suppliers').id

        return res

    @api.model
    def create(self, vals):
        picking_type_id = vals.get('picking_type_id')
        picking_type = self.env['stock.picking.type'].browse([picking_type_id])
        record_id = self.search([], limit=1, order='id desc')
        check_today = False
        if record_id and record_id.create_date.date() == date.today():
            check_today = True
        if not check_today:
            picking_type.sequence_id.number_next_actual = 1
        res = super(Picking, self).create(vals)
        res.transfer_log_action_assign()
        return res


    @api.onchange('partner_id')
    def onchange_partner_id(self):
        res = super(Picking, self).onchange_partner_id()
        self._compute_is_return_order()
        self._compute_is_mbs_on_transfer_operations()
        return res

    def _compute_is_mbs_on_transfer_operations(self):
        is_mbs_on_transfer_operations = self.env['ir.config_parameter'].sudo().get_param('is_mbs_on_transfer_operations', False)
        for record in self:
            record.is_mbs_on_transfer_operations = is_mbs_on_transfer_operations

    def _compute_is_return_order(self):
        is_return = self.env['ir.config_parameter'].sudo().get_param('is_return_orders')
        for record in self:
            if record.picking_type_code in ('incoming', 'outgoing') and is_return:
                record.is_return_orders = True
            else:
                record.is_return_orders = False

    def action_confirm(self):
        res = super(Picking , self).action_confirm()
        for record in self:
            record.transfer_log_action()
            for move_line in record.move_ids_without_package:
                move_line.initial_demand = move_line.product_uom_qty
                move_line.remaining = move_line.product_uom_qty
            if record.transfer_id and record.is_transfer_in:
                picking_id = self.env['stock.picking'].search([('transfer_id', '=', record.transfer_id.id), ('is_transfer_out', '=', True), ('state', '=', 'done')])
                if not picking_id:
                    raise Warning("You can only validate Operation IN if the Operation OUT is validated")
            transit_location = self.env.ref('equip3_inventory_masterdata.location_transit')
            if record.location_id.id == transit_location.id:
                operation_out = self.env['stock.picking'].search([('id','=',record.id -1)])
                if operation_out.state != 'done':
                    raise Warning("You can only validate Operation Return IN if the Operation Return OUT is validated")
        return res

    def action_serialize(self):
        filter_move_lot_line = self.move_ids_without_package.filtered(lambda r:r.product_id.tracking == 'lot' and r.product_id.is_in_autogenerate)
        if filter_move_lot_line:
            context = dict(self._context or {})
            context.update({
                'default_picking_id': self.id,
            })
            return {
                'name': 'Lot Serializer',
                'view_mode': 'form',
                'res_model': 'stock.lot.serialize',
                'view_type': 'form',
                'type': 'ir.actions.act_window',
                'context': context,
                'target': 'new',
            }
        else:
            for rec in self:
                for move in rec.move_ids_without_package:
                    if move.product_id.tracking == 'serial' and move.product_id.is_sn_autogenerate:
                        move._generate_serial_numbers()
                rec._reset_sequence()

    def action_assign(self):
        res = super(Picking , self).action_assign()
        if self.transfer_id and self.is_transfer_in:
            picking_id = self.env['stock.picking'].search([('transfer_id', '=', self.transfer_id.id), ('is_transfer_out', '=', True), ('state', '=', 'done')])
            if not picking_id:
                raise Warning("You can only validate Operation IN if the Operation OUT is validated")
        return res

    @api.onchange('location_id', 'location_dest_id')
    def onchange_location_id(self):
        if not self.is_interwarehouse_transfer:
            self._compute_picking_type()
            self._compute_location()
            self._compute_show_location()
            context = dict(self.env.context) or {}
            picking_type_ids = self.env['stock.picking.type'].search([])
            self.picking_type_id = False
            for record in self:
                if context.get('picking_type_code') == "outgoing":
                    picking_type_id = picking_type_ids.filtered(lambda r:r.code == 'outgoing' and r.default_location_src_id.id == record.location_id.id)
                elif context.get('picking_type_code') == "incoming":
                    picking_type_id = picking_type_ids.filtered(lambda r:r.code == 'incoming' and r.default_location_dest_id.id == record.location_dest_id.id)
                else:
                    picking_type_id = False
                if picking_type_id:
                    record.picking_type_id = picking_type_id[0].id

    def _compute_show_location(self):
        context = dict(self.env.context) or {}
        for record in self:
            if context.get('contact_display'):
                record.is_show_location = True
            else:
                record.is_show_location = False

    @api.depends('move_ids_without_package', 'move_ids_without_package.account_move_ids', 'move_ids_without_package.account_move_ids.state')
    def _compute_journal_entry(self):
        for record in self:
            account_move_ids = record.move_ids_without_package.mapped('account_move_ids')
            if account_move_ids and all(move.state == 'cancel' for move in account_move_ids):
                record.journal_cancel = True
            else:
                record.journal_cancel = False

    def _compute_picking_type(self):
        context = dict(self.env.context) or {}
        picking_type_ids = self.env['stock.picking.type'].search([])
        for record in self:
            if context.get('picking_type_code') == "outgoing":
                record.filter_operation_picking_type_ids = [(6, 0, picking_type_ids.filtered(lambda r:r.code == 'outgoing').ids)]
            elif context.get('picking_type_code') == "incoming":
                record.filter_operation_picking_type_ids = [(6, 0, picking_type_ids.filtered(lambda r:r.code == 'incoming').ids)]
            else:
                record.filter_operation_picking_type_ids = [(6, 0, picking_type_ids.ids)]

    @api.depends('branch_id')
    def _compute_location(self):
        context = dict(self.env.context) or {}
        stock_locations_ids = self.env['stock.location'].search([])
        for record in self:
            if record.is_interwarehouse_transfer:
                location_ids = []
                if record.warehouse_id:
                    location_obj = record.env['stock.location']
                    store_location_id = record.warehouse_id.view_location_id.id
                    addtional_ids = location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')])
                    for location in addtional_ids:
                        if location.location_id.id not in addtional_ids.ids:
                            location_ids.append(location.id)
                    child_location_ids = record.env['stock.location'].search([('id', 'child_of', location_ids), ('id', 'not in', location_ids)]).ids
                    final_location = child_location_ids + location_ids
                    record.filter_source_location_ids = [(6, 0, final_location)]
                    record.filter_dest_location_ids = [(6, 0, final_location)]
                else:
                    record.filter_source_location_ids = [(6, 0, [])]
                    record.filter_dest_location_ids = [(6, 0, [])]
            else:
                user = self.env.user
                if self.branch_id:
                    branch_ids = user.branch_ids.ids + user.branch_id.ids
                else:
                    branch_ids = self.env['res.branch'].search([]).ids
                    branch_ids.extend([False])
                if context.get('picking_type_code') == "outgoing":
                    record.filter_dest_location_ids = [(6, 0, stock_locations_ids.filtered(lambda r:r.usage != 'internal' and r.branch_id.id in branch_ids).ids)]
                    record.filter_source_location_ids = [(6, 0, stock_locations_ids.filtered(lambda r:r.usage == 'internal' and r.branch_id.id in branch_ids).ids)]
                elif context.get('picking_type_code') == "incoming":
                    record.filter_dest_location_ids = [(6, 0, stock_locations_ids.filtered(lambda r:r.usage == 'internal' and r.branch_id.id in branch_ids).ids)]
                    record.filter_source_location_ids = [(6, 0, stock_locations_ids.filtered(lambda r:r.usage != 'internal' and r.branch_id.id in branch_ids).ids)]
                else:
                    record.filter_dest_location_ids = [(6, 0, stock_locations_ids.ids)]
                    record.filter_source_location_ids = [(6, 0, stock_locations_ids.ids)]

    def button_validate(self):
        res = super(Picking, self).button_validate()
        for record in self:
            if record.transfer_id and record.is_transfer_in:
                picking_id = self.env['stock.picking'].search([('transfer_id', '=', record.transfer_id.id), ('is_transfer_out', '=', True), ('state', '=', 'done')])
                if not picking_id:
                    raise Warning("You can only validate Operation IN if the Operation OUT is validated")
            if record.state == 'done' and record.transfer_id:
                record.transfer_id.calculate_transfer_qty(record)
                if record.is_transfer_out or record.is_transfer_in:
                    for move in record.move_ids_without_package:
                        analytic_tag_ids = move.analytic_account_group_ids.mapped('analytic_distribution_ids')
                        for analytic_distribution_id in analytic_tag_ids:
                            vals = {
                                'name': move.product_id.name,
                                'account_id': analytic_distribution_id.account_id.id,
                                'tag_ids': [(6, 0, analytic_distribution_id.tag_id.ids)],
                                'partner_id': move.picking_id.partner_id.id,
                                'company_id': move.picking_id.company_id.id,
                                'amount': sum(move.stock_valuation_layer_ids.mapped('value')),
                                'unit_amount': move.quantity_done,
                                'product_id': move.product_id.id,
                                'product_uom_id': move.product_uom.id,
                                'general_account_id': move.product_id.categ_id.property_stock_valuation_account_id.id,
                            }
                            analytic_entry_id = self.env['account.analytic.line'].create(vals)
            # if record.transfer_id and record.transfer_id.is_transit and record.is_transfer_in and not record.backorder_id:
            #     for line in record.move_line_ids_without_package:
            #         transist_line = record.transfer_id.product_line_ids.filtered(lambda r: r.product_id.id == line.product_id.id)
            #         transist_line.write({'transfer_qty': line.qty_done})
            # if record.transfer_id and not record.transfer_id.is_transit and record.backorder_id:
            #     for line in record.move_line_ids_without_package:
            #         transist_line = record.transfer_id.product_line_ids.filtered(lambda r: r.product_id.id == line.product_id.id)
            #         for trns_line in transist_line:
            #             trns_line.transfer_qty += line.qty_done
            if record.transfer_id and 'Return' in record.origin:
                for line in record.move_line_ids_without_package:
                    transist_line = record.transfer_id.product_line_ids.filtered(lambda r: r.product_id.id == line.product_id.id)
                    transist_line.write({'return_qty': line.qty_done})
            
            return_date = fields.Datetime.now() + datetime.timedelta(days=int(self.env["ir.config_parameter"].sudo().get_param("return_policy_days")))
            record.write({ "return_date_limit": return_date })

            if record.transfer_id and record.is_transfer_out:
                picking_id = self.env['stock.picking'].search([('transfer_id', '=', record.transfer_id.id), ('is_transfer_in', '=', True)], limit=1)
                for move in picking_id.move_ids_without_package:
                    move.is_transfer_out = True
            for move in record.move_ids_without_package:
                for lot in move.lot_ids:
                    if lot.alert_date and lot.expiration_date:
                        lot.alert_date = lot.expiration_date - datetime.timedelta(days=lot.product_id.alert_time)
            ml_sequence = 1
            for line in record.move_line_ids_without_package:
                line.move_line_sequence = ml_sequence
                ml_sequence +=1
        return res

    def _get_next_sequence_and_serial(self, move):
        for record in self:
            if record.picking_type_code in ('incoming', 'outgoing'):
                for move_line in move:
                    if move_line.move_line_ids:
                        categories_id = move_line.product_id.categ_id
                        if move_line.product_id.tracking == 'serial':
                            if record.picking_type_code == 'incoming':
                                next_serial = move_line.move_line_ids[-1].lot_name
                            else:
                                next_serial = move_line.move_line_ids[-1].lot_id.name
                            if next_serial:
                                current_name = ''
                                if move_line.is_serial_number:
                                    if move_line.product_id.is_use_product_code == True:
                                        if move_line.product_id.sn_prefix:
                                            current_name = move_line.product_id.default_code + move_line.product_id.sn_prefix
                                        else:
                                            current_name = move_line.product_id.default_code
                                    else:
                                        current_name = move_line.product_id.sn_prefix
                                if next_serial and current_name:
                                    next_serial = next_serial.replace(current_name, '')
                                if next_serial and move_line.product_id.suffix:
                                    next_serial = next_serial.replace(move_line.product_id.suffix, '')
                                caught_initial_number = regex_findall("\d+", next_serial)
                                if caught_initial_number:
                                    initial_number = caught_initial_number[-1]
                                    padding = len(initial_number)
                                    # We split the serial number to get the prefix and suffix.
                                    splitted = regex_split(initial_number, next_serial)
                                    # initial_number could appear several times in the SN, e.g. BAV023B00001S00001
                                    prefix = initial_number.join(splitted[:-1])
                                    suffix = splitted[-1]
                                    initial_number = int(initial_number)

                                    lot_names = []
                                    for i in range(0, 2):
                                        lot_names.append('%s%s%s' % (
                                            prefix,
                                            str(initial_number + i).zfill(padding),
                                            suffix
                                        ))
                                    next_serial_number = lot_names[-1]
                                    move_line.product_id.current_sequence = next_serial_number
                        if move_line.product_id.tracking == 'lot':
                            if record.picking_type_code == 'incoming':
                                next_serial = move_line.move_line_ids[-1].lot_name
                            else:
                                next_serial = move_line.move_line_ids[-1].lot_id.name
                            if next_serial:
                                current_name = ''
                                if next_serial and move_line.product_id.default_code:
                                    next_serial = next_serial.replace(move_line.product_id.default_code, '')
                                if next_serial and move_line.product_id.in_prefix:
                                    next_serial = next_serial.replace(move_line.product_id.in_prefix, '')
                                if next_serial and move_line.product_id.in_suffix:
                                    next_serial = next_serial.replace(move_line.product_id.in_suffix, '')
                                caught_initial_number = regex_findall("\d+", next_serial)
                                if caught_initial_number:
                                    initial_number = caught_initial_number[-1]
                                    padding = len(initial_number)
                                    # We split the serial number to get the prefix and suffix.
                                    splitted = regex_split(initial_number, next_serial)
                                    # initial_number could appear several times in the SN, e.g. BAV023B00001S00001
                                    prefix = initial_number.join(splitted[:-1])
                                    suffix = splitted[-1]
                                    initial_number = int(initial_number)

                                    lot_names = []
                                    for i in range(0, 2):
                                        lot_names.append('%s%s%s' % (
                                            prefix,
                                            str(initial_number + i).zfill(padding),
                                            suffix
                                        ))
                                    next_serial_number = lot_names[-1]
                                    move_line.product_id.in_current_sequence = next_serial_number

    # def _put_in_pack(self, move_line_ids, create_package_level=True):
    #     if any(picking.picking_type_code == 'outgoing' for picking in self):
    #         package = False
    #         for pick in self:
    #             move_lines_to_pack = self.env['stock.move.line']
    #             package = self.env['stock.quant.package'].create({})

    #             precision_digits = self.env['decimal.precision'].precision_get('Product Unit of Measure')
    #             if float_is_zero(move_line_ids[0].qty_done, precision_digits=precision_digits):
    #                 for line in move_line_ids:
    #                     line.qty_done = line.product_uom_qty

    #             for ml in move_line_ids:
    #                 if float_compare(ml.qty_done, ml.product_uom_qty,
    #                                  precision_rounding=ml.product_uom_id.rounding) >= 0:
    #                     move_lines_to_pack |= ml
    #                 else:
    #                     quantity_left_todo = float_round(
    #                         ml.product_uom_qty - ml.qty_done,
    #                         precision_rounding=ml.product_uom_id.rounding,
    #                         rounding_method='UP')
    #                     done_to_keep = ml.qty_done
    #                     new_move_line = ml.copy(
    #                         default={'product_uom_qty': 0, 'qty_done': ml.qty_done})
    #                     vals = {'product_uom_qty': quantity_left_todo, 'qty_done': 0.0}
    #                     if pick.picking_type_id.code == 'incoming':
    #                         if ml.lot_id:
    #                             vals['lot_id'] = False
    #                         if ml.lot_name:
    #                             vals['lot_name'] = False
    #                     ml.write(vals)
    #                     new_move_line.write({'product_uom_qty': done_to_keep})
    #                     move_lines_to_pack |= new_move_line
    #             if create_package_level:
    #                 package_level = self.env['stock.package_level'].create({
    #                     'package_id': package.id,
    #                     'picking_id': pick.id,
    #                     'location_id': False,
    #                     'location_dest_id': move_line_ids.mapped('location_dest_id').id,
    #                     'move_line_ids': [(6, 0, move_lines_to_pack.ids)] if pick.picking_type_code != 'outgoing' else [],
    #                     'company_id': pick.company_id.id,
    #                 })
    #             move_lines_to_pack.write({
    #                 'result_package_id': package.id,
    #             })
    #         return package
    #     else:
    #         return super(Picking, self)._put_in_pack(move_line_ids=move_line_ids, create_package_level=create_package_level)

    def _check_entire_pack(self):
        """ This function check if entire packs are moved in the picking"""
        if any(picking.picking_type_code == 'outgoing' for picking in self):
            for picking in self:
                origin_packages = picking.move_line_ids.mapped("package_id")
                for pack in origin_packages:
                    package_level_ids = picking.package_level_ids.filtered(lambda pl: pl.package_id == pack)
                    move_lines_to_pack = picking.move_line_ids.filtered(lambda ml: ml.package_id == pack and not ml.result_package_id)
                    if package_level_ids:
                        package_level_ids.unlink()
                    for move_line in move_lines_to_pack:
                        package_qty = sum(pack.quant_ids.filtered(lambda r:r.product_id.id == move_line.product_id.id).mapped('quantity'))
                        occupancy = ""
                        if move_line.product_uom_qty == package_qty:
                            occupancy = "full"
                        elif move_line.product_uom_qty < package_qty:
                            occupancy = "partial"
                        package_level_id = self.env['stock.package_level'].create({
                            'picking_id': picking.id,
                            'package_id': pack.id,
                            'location_id': pack.location_id.id,
                            'product_id': move_line.product_id.id,
                            'occupancy': occupancy,
                            'location_dest_id': self._get_entire_pack_location_dest(move_line) or picking.location_dest_id.id,
                            'move_line_ids': [(6, 0, move_line.ids)] if picking.picking_type_code != 'outgoing' else [],
                            'company_id': picking.company_id.id,
                        })
        else:
            return super(Picking, self)._check_entire_pack()

    def do_unreserve(self):
        self.move_lines._do_unreserve()

    def button_action_cancel(self):
        context = dict(self.env.context) or {}
        context.update({'active_ids': self.ids})
        return {
            'name': 'Cancel Picking',
            'view_mode': 'form',
            'res_model': 'cancel.picking',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'context': context,
            'target': 'new',
        }

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(Picking, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if view_type == 'form' and self.env.context.get("date_done_string"):
            doc = etree.XML(result['arch'])
            date_done_reference = doc.xpath("//field[@name='date_done']")
            date_done_reference[0].set("string", self.env.context.get("date_done_string"))
            result['arch'] = etree.tostring(doc, encoding='unicode')
        return result

class Product(models.Model):
    _inherit = 'product.product'

    def _prepare_in_transit_svl_vals(self, quantity, unit_cost):
        """Prepare the values for a stock valuation layer created by a receipt.

        :param quantity: the quantity to value, expressed in `self.uom_id`
        :param unit_cost: the unit cost to value `quantity`
        :return: values to use in a call to create
        :rtype: dict
        """
        self.ensure_one()
        vals = {
            'product_id': self.id,
            'value': unit_cost * quantity,
            'unit_cost': unit_cost,
            'quantity': quantity,
        }
        if self.cost_method in ('average', 'fifo'):
            vals['remaining_qty'] = quantity
            vals['remaining_value'] = vals['value']
        return vals

    def _prepare_out_transit_svl_vals(self, quantity, company):
        """Prepare the values for a stock valuation layer created by a delivery.

        :param quantity: the quantity to value, expressed in `self.uom_id`
        :return: values to use in a call to create
        :rtype: dict
        """
        self.ensure_one()
        # Quantity is negative for out valuation layers.
        quantity = -1 * quantity
        vals = {
            'product_id': self.id,
            'value': quantity * self.standard_price,
            'unit_cost': self.standard_price,
            'quantity': quantity,
        }
        if self.cost_method in ('average', 'fifo'):
            fifo_vals = self._run_fifo(abs(quantity), company)
            vals['remaining_qty'] = fifo_vals.get('remaining_qty')
            # In case of AVCO, fix rounding issue of standard price when needed.
            if self.cost_method == 'average':
                currency = self.env.company.currency_id
                rounding_error = currency.round(self.standard_price * self.quantity_svl - self.value_svl)
                if rounding_error:
                    # If it is bigger than the (smallest number of the currency * quantity) / 2,
                    # then it isn't a rounding error but a stock valuation error, we shouldn't fix it under the hood ...
                    if abs(rounding_error) <= (abs(quantity) * currency.rounding) / 2:
                        vals['value'] += rounding_error
                        vals['rounding_adjustment'] = '\nRounding Adjustment: %s%s %s' % (
                            '+' if rounding_error > 0 else '',
                            float_repr(rounding_error, precision_digits=currency.decimal_places),
                            currency.symbol
                        )
            if self.cost_method == 'fifo':
                vals.update(fifo_vals)
        return vals

class StockMove(models.Model):
    _inherit = "stock.move"

    @api.model
    def _get_valued_types(self):
        res = super(StockMove, self)._get_valued_types()
        res.append('transit')
        return res

    def _is_transit(self):
        """Check if the move should be considered as entering the company so that the cost method
        will be able to apply the correct logic.

        :returns: True if the move is entering the company else False
        :rtype: bool
        """
        self.ensure_one()
        if self.picking_id and (self.picking_id.is_transfer_out or self.picking_id.is_transfer_in):
            return True
        return False

    def _create_transit_svl(self, forced_quantity=None):
        svl_id = False
        for move in self:
            if move.picking_id.is_transfer_out:
                svl_vals_list = []
                move = move.with_company(move.company_id)
                valued_move_lines = move.move_line_ids
                valued_quantity = 0
                for valued_move_line in valued_move_lines:
                    valued_quantity += valued_move_line.product_uom_id._compute_quantity(valued_move_line.qty_done, move.product_id.uom_id)
                if float_is_zero(forced_quantity or valued_quantity, precision_rounding=move.product_id.uom_id.rounding):
                    continue
                svl_vals = move.product_id._prepare_out_transit_svl_vals(forced_quantity or valued_quantity, move.company_id)
                svl_vals.update(move._prepare_common_svl_vals())
                if forced_quantity:
                    svl_vals['description'] = 'Correction of %s (modification of past move)' % move.picking_id.name or move.name
                svl_vals['description'] += svl_vals.pop('rounding_adjustment', '')
                svl_vals_list.append(svl_vals)
                svl_id = self.env['stock.valuation.layer'].sudo().create(svl_vals_list)
            elif move.picking_id.is_transfer_in:
                svl_vals_list = []
                move = move.with_company(move.company_id)
                valued_move_lines = move.move_line_ids
                valued_quantity = 0
                for valued_move_line in valued_move_lines:
                    valued_quantity += valued_move_line.product_uom_id._compute_quantity(valued_move_line.qty_done, move.product_id.uom_id)
                unit_cost = abs(move._get_price_unit())  # May be negative (i.e. decrease an out move).
                if move.product_id.cost_method == 'standard':
                    unit_cost = move.product_id.standard_price
                svl_vals = move.product_id._prepare_in_transit_svl_vals(forced_quantity or valued_quantity, unit_cost)
                svl_vals.update(move._prepare_common_svl_vals())
                if forced_quantity:
                    svl_vals['description'] = 'Correction of %s (modification of past move)' % move.picking_id.name or move.name
                svl_vals_list.append(svl_vals)
                svl_id = self.env['stock.valuation.layer'].sudo().create(svl_vals_list)
        return svl_id

    def _update_reserved_quantity(self, need, available_quantity, location_id, lot_id=None, package_id=None, owner_id=None, strict=True):
        self.ensure_one()
        if not package_id and self.picking_id.picking_type_code == 'outgoing' and self.picking_id.delivery_package_level_ids:
            package_level_ids = self.picking_id.delivery_package_level_ids.filtered(lambda r: self.product_id.id in r.package_id.quant_ids.mapped('product_id').ids)
            if package_level_ids:
                package_id = package_level_ids[0].package_id
        res = super(StockMove, self)._update_reserved_quantity(need=need, available_quantity=available_quantity, location_id=location_id, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=strict)
        self.picking_id._reset_sequence()
        return res

    mr_line_id = fields.Many2one('material.request.line', string="Material Request Line")

    @api.depends('product_id','product_description')
    def _compute_total_packages(self):
        for line in self:
            line.product_description = False
            if line.picking_code == 'incoming':
                if line.product_id.description_pickingin == False:
                    line.product_description = line.name
                else:
                    line.product_description = line.name + ' ' + (
                        line.product_id.description_pickingin)

            if line.picking_code == 'outgoing':
                if line.product_id.description_pickingout == False:
                    line.product_description = line.name
                else:
                    line.product_description = line.name + ' ' + (
                        line.product_id.description_pickingout)
            if line.picking_code == 'internal':
                if line.description_picking == False:
                    line.product_description = line.name
                else:
                    line.product_description = line.name + ' ' + (
                        line.description_picking)

    @api.model
    def default_get(self, fields):
        res = super(StockMove, self).default_get(fields)
        if self._context:
            context_keys = self._context.keys()
            next_sequence = 1
            if 'move_ids_without_package' in context_keys:
                if len(self._context.get('move_ids_without_package')) > 0:
                    next_sequence = len(self._context.get('move_ids_without_package')) + 1
            res.update({'move_line_sequence': next_sequence})
            if 'move_line_nosuggest_ids' in context_keys:
                if len(self._context.get('move_line_nosuggest_ids')) > 0:
                    next_sequence = len(self._context.get('move_line_nosuggest_ids')) + 1
            res.update({'move_line_sequence': next_sequence})
        return res

    move_line_sequence = fields.Char(string='No')
    picking_type_code = fields.Selection(related='picking_id.picking_type_code', store=True)
    serial_no = fields.Selection(related='product_id.tracking')
    export_file = fields.Binary("Upload File")
    export_name = fields.Char('Export Name', size=64)
    next_serial = fields.Char('First SN', compute='_compute_next_lot_and_serial', store=True)
    next_serial_not_autogenerate = fields.Char(string='New Serial Number Start From')
    next_lot_not_autogenerate = fields.Char(string='New Lot Number Start From')
    is_import = fields.Boolean(string="import file")
    is_record_confirm = fields.Boolean(string='Is Record Confirmed')
    product_type = fields.Selection(related='product_id.type')
    is_serial_number = fields.Boolean(string="Serial Number", related="product_id.is_sn_autogenerate")

    next_lot = fields.Char(string="New Lot Number Start From", compute='_compute_next_lot_and_serial')
    qty_per_lot = fields.Integer(string="Quantity Per Lot")
    is_autogenerate = fields.Boolean(related="product_id.is_in_autogenerate")
    picking_code = fields.Selection(related='picking_id.picking_type_code')
    is_product_service_operation = fields.Boolean(related='product_id.is_product_service_operation', string='Is Product Service Operation')
    serial = fields.Selection(related='product_id.tracking')
    initial_demand = fields.Float(string='Initial Demand')
    move_progress = fields.Float(string='Progress (%)')
    analytic_account_group_ids = fields.Many2many('account.analytic.tag', 'stock_move_analytic_rel', 'tag_id', 'move_id', copy=True, string="Analytic Groups", default=lambda self: self.env.user.analytic_tag_ids.ids)
    remaining = fields.Float(string="Remaining")
    fulfillment = fields.Float(string="Fulfillment (%)", compute="_compute_fulfillment", store=True)

    current_qty = fields.Float(compute="_compute_current_quantity", string="Current Quantity")
    scheduled_date = fields.Datetime(string="Scheduled Date")
    responsible = fields.Many2one('picking_id.user_id', string="Responsible", store=True)
    late_time = fields.Float(string="Late Time", compute="_compute_late_time", store=True)
    late_time_hours = fields.Float(string="Late Time Hours", compute="_compute_late_time", store=True)
    process_status = fields.Selection([('late' , 'Late'),
                                       ('on_time', 'On Time')], compute="_compute_process_status", string="Process Status", store=True)
    process_time = fields.Char(related="picking_id.process_time", store=True)
    process_time_hours = fields.Float(related="picking_id.process_time_hours", store=True)
    package_type = fields.Many2one('product.packaging', string="Package Type")
    qty_in_pack = fields.Integer(string="Quantity To Put in Package", default=0)
    packaging_ids = fields.Many2many('product.packaging', compute="_compute_package_ids", string='packaging')
    package_quant_ids = fields.Many2many('stock.quant', 'package_quant_receiving_note_rel', 'package_id', 'quant_id', string="Package Quant") 
    

    def _compute_package_ids(self):
        for rec in self:
            packaging_ids = self.env['product.packaging'].search([('product_id', 'in', rec.product_id.ids)])
            rec.packaging_ids = [(6, 0, packaging_ids.ids)]

    @api.depends('date', 'scheduled_date')
    def _compute_process_status(self):
        for record in self:
            if record.date and record.scheduled_date:
                if record.date > record.scheduled_date:
                    record.process_status = 'late'
                elif record.date <= record.scheduled_date:
                    record.process_status = 'on_time'

    @api.onchange('product_id', 'picking_type_id')
    def onchange_product(self):
        if self.picking_code == 'incoming':
            if self.product_id:
                self.package_type = self.product_id.def_packaging_id
        else:
            return super(StockMove,self).onchange_product()

    def action_put_in_package(self):
        for record in self:
            if record.quantity_done <= 0 and record.qty_in_pack <= 0:
                raise ValidationError("Please add 'Done' quantities to the picking to create a new pack.")
            if record.qty_in_pack <= 0:
                raise ValidationError("Quantity to Put in Package must have a value in it and not 0.")
            product_weight = record.product_id.weight
            product_total_weight = record.qty_in_pack * product_weight
            package_total_weight = record.qty_in_pack * product_weight
            final_package_id = False
            package_ids = self.env['stock.quant.package'].search([
                            ('packaging_id', '=', record.package_type.id),
                            '|',
                            ('location_id', '=', record.location_dest_id.id),
                            ('move_location_id', '=', record.location_dest_id.id)
                        ])
            Quant = self.env['stock.quant']
            empty_packages = []
            lot_name = ''
            total_weight = 0
            for package in package_ids:

                if package.weight < package.max_weight and total_weight < product_total_weight:
                    remaining_weight = package.max_weight - package.weight
                    if total_weight != 0:
                        product_total_weight -= total_weight
                    difference = abs(product_total_weight - remaining_weight)
                    if product_total_weight <= remaining_weight:
                        difference_weight = abs(remaining_weight - difference)
                    else:
                        difference_weight = abs(product_total_weight - difference)
                    empty_packages.append({'package': package, 'weight': difference_weight})
                    total_weight += difference_weight

            total_weight_packages = sum([package_line.get('weight') for package_line in empty_packages])
            remaining_package_weight = 0
            if total_weight_packages < package_total_weight:
                remaining_package_weight = package_total_weight - total_weight_packages
            import math
            if not empty_packages:
                packages = []
                counter = 0
                for rec in range(1, math.ceil(package_total_weight) + 1):
                    counter += 1
                    if counter == record.package_type.max_weight:
                        packages.append({
                            'packaging_id': record.package_type.id,
                            'location_id': record.location_dest_id.id,
                            'weight': counter,
                        })
                        counter = 0
                if counter > 0:
                    remaining_qty = package_total_weight - sum([package_line.get('weight') for package_line in packages])
                    packages.append({
                            'packaging_id': record.package_type.id,
                            'location_id': record.location_dest_id.id,
                            'weight': remaining_qty,
                        })
                for package_line_data in packages:
                    final_package_id = self.env['stock.quant.package'].create(package_line_data)
                    lot_names = self._generate_lot_numbers(1)
                    if lot_names:
                        lot_name = lot_names[0]
                    record.move_line_nosuggest_ids = [(0, 0, {
                                                    'location_dest_id': record.location_dest_id.id,
                                                    'result_package_id': final_package_id.id,
                                                    'qty_done': package_line_data.get('weight') * (1 / product_weight),
                                                    'product_uom_id': record.product_id.uom_id.id,
                                                    'product_id': record.product_id.id,
                                                    'lot_name' : lot_name,
                                                })]
                record.picking_id._get_next_sequence_and_serial(move=record)
            elif empty_packages:
                for empty_package_line in empty_packages:
                    lot_names = self._generate_lot_numbers(1)
                    if lot_names:
                        lot_name = lot_names[0]
                    record.move_line_nosuggest_ids = [(0, 0, {
                                                    'location_dest_id': record.location_dest_id.id,
                                                    'result_package_id': empty_package_line.get('package').id,
                                                    'qty_done': empty_package_line.get('weight') * (1 / product_weight),
                                                    'product_uom_id': record.product_id.uom_id.id,
                                                    'product_id': record.product_id.id,
                                                    'lot_name' : lot_name,
                                                })]
                record.picking_id._get_next_sequence_and_serial(move=record)
                if remaining_package_weight > 0:
                    packages = []
                    counter = 0
                    for rec in range(1, int(remaining_package_weight) + 1):
                        counter += 1
                        if counter == record.package_type.max_weight:
                            packages.append({
                                'packaging_id': record.package_type.id,
                                'location_id': record.location_dest_id.id,
                                'weight': counter,
                            })
                            counter = 0
                    if counter > 0:
                        remaining_qty = remaining_package_weight - sum([package_line.get('weight') for package_line in packages])
                        packages.append({
                                'packaging_id': record.package_type.id,
                                'location_id': record.location_dest_id.id,
                                'weight': remaining_qty,
                            })
                    for package_line_data in packages:
                        final_package_id = self.env['stock.quant.package'].create(package_line_data)
                        lot_names = self._generate_lot_numbers(1)
                        if lot_names:
                            lot_name = lot_names[0]
                        record.move_line_nosuggest_ids = [(0, 0, {
                                                        'location_dest_id': record.location_dest_id.id,
                                                        'result_package_id': final_package_id.id,
                                                        'qty_done': package_line_data.get('weight') * (1 / product_weight),
                                                        'product_uom_id': record.product_id.uom_id.id,
                                                        'product_id': record.product_id.id,
                                                        'lot_name' : lot_name,
                                                    })]
                record.picking_id._get_next_sequence_and_serial(move=record)
                    

            for line in record.move_line_nosuggest_ids:
                Quant.with_context({'move_id': record.id})._update_available_quantity(line.product_id, line.location_dest_id, line.qty_done, lot_id=line.lot_id, package_id=line.result_package_id, owner_id=line.owner_id)

    @api.depends('date', 'scheduled_date')
    def _compute_late_time(self):
        for record in self:
            record.late_time_hours = 0
            record.late_time = 0
            if record.date and record.scheduled_date:
                time = record.date - record.scheduled_date 
                hours = time.total_seconds() / 3600
                record.late_time_hours = hours
                final_hour = round(hours / 24, 2)
                record.late_time = final_hour

    @api.depends('product_id', 'location_id')
    def _compute_current_quantity(self):
        for record in self:
            stock_quant_ids = self.env['stock.quant'].search([('product_id', '=', record.product_id.id),
                                                              ('location_id', '=', record.location_id.id)])
            record.current_qty = sum(stock_quant_ids.mapped('available_quantity'))

    @api.depends('initial_demand', 'quantity_done', 'remaining', 'product_uom_qty')
    def _compute_fulfillment(self):
        for record in self:
            if record.initial_demand > 0:
                record.fulfillment = (record.quantity_done / record.initial_demand) * 100

    @api.onchange('product_uom_qty')
    def _onchange_remaining_qty(self):
        self.remaining = self.product_uom_qty

    def _prepare_move_split_vals(self, qty):
        res = super(StockMove, self)._prepare_move_split_vals(qty)
        res['initial_demand'] = self.initial_demand
        res['move_progress'] = self.move_progress
        return res

    def action_assign_lot_number(self):
        qty = []
        if self.is_autogenerate:
            next_lot = self.next_lot
        else:
            last_digit_check = self.next_lot_not_autogenerate and self.next_lot_not_autogenerate[-1] or ''
            if not last_digit_check.isdigit():
                raise ValidationError('New Lot Number Start From must contain digits behind it.')
            next_lot = self.next_lot_not_autogenerate
        demand = self.product_uom_qty
        quantity = self.qty_per_lot
        if quantity <= 0:
            raise ValidationError("Quantity per lot must be equal to or more than one!")
        total = int(demand / quantity)
        qty += [quantity for record in range(0, total)]
        qty_value = demand - sum(qty)
        if qty_value:
            qty += [int(qty_value)]
        lot_names = self._generate_lot_numbers(len(qty))
        move_lines_commands = []
        counter = 0
        for record in qty:
            move_lines_commands_data = self._generate_lot_move_line_commands(lot_names[counter], record)
            move_lines_commands.extend(move_lines_commands_data)
            counter += 1
        self.write({'move_line_ids': move_lines_commands})
        move_lines_commands = self.picking_id._get_next_sequence_and_serial(move=self)

    def _generate_lot_move_line_commands(self, lot_names, qty):
        self.ensure_one()
        move_lines = self.env['stock.move.line']
        if self.picking_type_id.show_reserved:
            move_lines = self.move_line_ids.filtered(lambda ml: not ml.lot_id and not ml.lot_name)
        else:
            move_lines = self.move_line_nosuggest_ids.filtered(lambda ml: not ml.lot_id and not ml.lot_name)

        location_dest = self.location_dest_id._get_putaway_strategy(self.product_id)
        move_line_vals = {
            'picking_id': self.picking_id.id,
            'location_dest_id': location_dest.id or self.location_dest_id.id,
            'location_id': self.location_id.id,
            'product_id': self.product_id.id,
            'product_uom_id': self.product_id.uom_id.id,
            'qty_done': qty,
        }

        move_lines_commands = []
        if move_lines:
            move_lines_commands.append((1, move_lines[0].id, {
                'lot_name': lot_names,
                'qty_done': qty,
            }))
            move_lines = move_lines[1:]
        else:
            move_line_cmd = dict(move_line_vals, lot_name=lot_names)
            move_lines_commands.append((0, 0, move_line_cmd))
        return move_lines_commands


    def _generate_lot_numbers(self, next_serial_count=False):
        self.ensure_one()
        if self.is_autogenerate:
            next_lot = self.next_lot
        else:
            last_digit_check = self.next_lot_not_autogenerate and self.next_lot_not_autogenerate[-1] or ''
            if not last_digit_check.isdigit():
                raise ValidationError('New Lot Number Start From must contain digits behind it.')
            next_lot = self.next_lot_not_autogenerate
        if self.product_id and self.product_id.in_suffix:
            next_lot = next_lot.replace(self.product_id.in_suffix, '')
        caught_initial_number = regex_findall("\d+", next_lot)
        if not caught_initial_number:
            raise UserError(_('The serial number must contain at least one digit.'))
        initial_number = str(caught_initial_number[-1])
        padding = len(initial_number)
        splitted = regex_split(initial_number, next_lot)
        prefix = initial_number.join(splitted[:-1])
        suffix = splitted[-1]
        if self.product_id and self.product_id.in_suffix:
            suffix = self.product_id.in_suffix
        initial_number = int(self.product_id.in_current_sequence)
        lot_names = []
        for i in range(0, next_serial_count):
            lot_names.append('%s%s%s' % (
                prefix,
                str(initial_number + i).zfill(padding),
                suffix
            ))
        return lot_names

    def action_show_details(self):
        self.ensure_one()
        action = super().action_show_details()
        action['context']['default_export_file'] = False
        action['context']['default_export_name'] = False
        action['context']['default_is_import'] = False
        return action

    def write(self, vals):
        res = super(StockMove, self).write(vals)
        for record in self:
            if record.state in ('waiting', 'confirmed', 'assigned') and not record.initial_demand and record.product_uom_qty > 0:
                record.initial_demand = record.product_uom_qty
            if record.move_line_ids:
                counter = 1
                for line in record.move_line_ids:
                    if line.qty_done != 0:
                        line.move_line_sequence = counter
                        counter += 1
        return res

    @api.depends('product_id', 'product_id.default_code',
                 'product_id.suffix', 'product_id.sn_prefix', 'product_id.is_sn_autogenerate', 'product_id.is_use_product_code',
                 'product_id.in_suffix', 'product_id.in_prefix', 'product_id.is_in_autogenerate', 'product_id.is_in_use_product_code')
    def _compute_next_lot_and_serial(self):
        for record in self:
            product_id = record.product_id
            next_number = product_id.product_tmpl_id._get_next_lot_and_serial()
            record.next_serial = next_number
            record.next_lot = next_number

    @api.onchange('export_file')
    def onchage_import_button_validate(self):
        if self.export_file:
            self.is_import = True
        else:
            self.is_import = False

    def import_file_data(self):
        self.action_import_template()
        self.export_file = False
        self.export_name = False
        self.is_import = False
        self.is_record_confirm = True

    def action_full_quantity_done(self):
        for record in self:
            if record.move_line_ids:
                for line in record.move_line_ids:
                    qty_done = round(record.product_uom_qty / len(record.move_line_ids), 2)
                    line.qty_done = qty_done
            else:
                record.quantity_done = record.product_uom_qty

    def action_partial_quantity_done(self):
        context = dict(self.env.context) or {}
        context.update({'default_product_id': self.product_id.id, 'default_move_id': self.id})
        view = self.env.ref('equip3_inventory_operation.view_partial_quantity_done_wizard')
        return {
            'name': _('Service Product Partial Done'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'partial.quantity.done',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'context':context,
        }

    def action_save(self):
        if self.move_line_nosuggest_ids:
            self.is_record_confirm = True
        else:
            self.is_record_confirm = False

    def action_unlink_data(self):
        if not self.is_record_confirm:
            self.move_line_nosuggest_ids.unlink()
        self.export_file = False
        self.export_name = False
        self.is_import = False
        if self.picking_id.picking_type_code == "incoming" \
            and self.package_quant_ids:
            packages = self.package_quant_ids.mapped('package_id')
            self.package_quant_ids.sudo().unlink()
            packages.write({'move_location_id': self.location_dest_id.id})

    def action_import_template(self):
        if self.export_name:
            import_name_extension = self.export_name.split('.')[1]
            if import_name_extension not in ['xls', 'xlsx']:
                raise ValidationError('File format must be in xlsx or xls.')
        context = dict(self.env.context) or {}
        if not self.export_file:
            context.update({'show_upload_file': False})
        else:
            workbook = open_workbook(file_contents=base64.decodestring(self.export_file))
            for sheet in workbook.sheets():
                line_list = []
                for count in range(1, sheet.nrows):
                    line_vals = {}
                    line = sheet.row_values(count)
                    if line[1] != '':
                        package_id = self.env['stock.quant.package'].search([('name', 'ilike', line[1])], limit=1)
                        if not package_id:
                            package_id = self.env['stock.quant.package'].create({'name': line[1]})
                        line_vals['result_package_id'] = package_id and package_id.id or False
                    is_float = False
                    if line[2] != '':
                        try:
                            is_float = float(line[2])
                        except ValueError:
                            pass
                        if not is_float:
                            raise ValidationError(_("Done Value Must be in digits!"))
                        line_vals['qty_done'] = line[2]
                    categories_id = self.product_id.categ_id

                    if self.is_serial_number and self.product_id.tracking == 'serial':
                        current_name = ''
                        if self.product_id.is_use_product_code == True:
                            if self.product_id.sn_prefix:
                                current_name = self.product_id.default_code + self.product_id.sn_prefix
                            else:
                                current_name = self.product_id.default_code
                        else:
                            current_name = self.product_id.sn_prefix
                        if current_name and line[0] and not line[0].startswith(current_name):
                            raise ValidationError(_("The Lot/Serial Number that you imported must match prefix of the product Serial Number Master Data."))
                        if line[0] and self.product_id.suffix and not line[0].endswith(self.product_id.suffix):
                            raise ValidationError(_("The Lot/Serial Number that you imported must match suffix of the product Serial Number Master Data."))

                    if self.is_autogenerate and self.product_id.tracking == 'lot':
                        current_name = ''
                        if self.product_id.is_in_use_product_code == True:
                            if self.product_id.in_prefix:
                                current_name = self.product_id.default_code + self.product_id.in_prefix
                            else:
                                current_name = self.product_id.default_code
                        else:
                            current_name = self.product_id.in_prefix
                        if current_name and line[0] and not line[0].startswith(current_name):
                            raise ValidationError(_("The Lot/Serial Number that you imported must match prefix of the product Serial Number Master Data."))
                        if line[0] and self.product_id.in_suffix and not line[0].endswith(self.product_id.in_suffix):
                            raise ValidationError(_("The Lot/Serial Number that you imported must match suffix of the product Serial Number Master Data."))

                    line_vals['lot_name'] = line[0]
                    line_vals['product_id'] = self.product_id.id
                    line_vals['product_uom_id'] = self.product_uom.id
                    if self.product_id.use_expiration_date:
                        line_vals['expiration_date'] = fields.Datetime.today() + datetime.timedelta(days=self.product_id.expiration_time)
                        line_vals['alert_date'] = fields.Datetime.today() + datetime.timedelta(days=self.product_id.expiration_time) - datetime.timedelta(days=self.product_id.alert_time)
                    if line[0] != '':
                        line_list.append((0, 0, line_vals))
            self.write({'move_line_nosuggest_ids': line_list}) 
            self.export_file = False
            context.update({'show_upload_file': True})
        return self.with_context(context).action_show_details()

    def _generate_serial_numbers(self, next_serial_count=False, is_sn_autogenerate=True):
        """ This method will generate `lot_name` from a string (field
        `next_serial`) and create a move line for each generated `lot_name`.
        """
        self.ensure_one()
        categories_id = self.product_id.categ_id
        if self.is_serial_number:
            next_serial = self.next_serial
        else:
            last_digit_check = self.next_serial_not_autogenerate and self.next_serial_not_autogenerate[-1] or ''
            if not last_digit_check.isdigit():
                raise ValidationError('New Serial Number Start From must contain digits behind it.')
            next_serial = self.next_serial_not_autogenerate
        if not next_serial_count:
            next_serial_count = self.next_serial_count
        if self.product_id.suffix:
            next_serial = next_serial.replace(self.product_id.suffix, '')
        # We look if the serial number contains at least one digit.
        caught_initial_number = regex_findall("\d+", next_serial)

        if not caught_initial_number:
            raise UserError(_('The serial number must contain at least one digit.'))
        # We base the serie on the last number find in the base serial number.
        initial_number = caught_initial_number[-1]
        padding = len(initial_number)
        # We split the serial number to get the prefix and suffix.
        splitted = regex_split(initial_number, next_serial)
        # initial_number could appear several times in the SN, e.g. BAV023B00001S00001
        prefix = initial_number.join(splitted[:-1])
        suffix = splitted[-1]
        if self.product_id.suffix:
            suffix = self.product_id.suffix
        initial_number = int(self.product_id.current_sequence)
        lot_names = []
        for i in range(0, next_serial_count):
            lot_names.append('%s%s%s' % (
                prefix,
                str(initial_number + i).zfill(padding),
                suffix
            ))
        move_lines_commands = self._generate_serial_move_line_commands(lot_names)
        self.write({'move_line_ids': move_lines_commands})
        move_lines_commands = self.picking_id._get_next_sequence_and_serial(move=self)
        return True

    def action_assign_serial_show_details(self):
        """ On `self.move_line_ids`, assign `lot_name` according to
        `self.next_serial` before returning `self.action_show_details`.
        """
        # self.ensure_one()
        self._generate_serial_numbers()
        return self.action_show_details()



    def unlink(self):
        approval = self.picking_id
        res = super(StockMove, self).unlink()
        approval._reset_sequence()
        return res

    @api.onchange('sequence')
    def set_sequence_line(self):
        for record in self:
            record.picking_id._reset_sequence()

    @api.model
    def create(self, vals):
        res = super(StockMove, self).create(vals)
        res.picking_id._reset_sequence()
        res._onchange_remaining_qty()
        return res 

class StockMoveLine(models.Model):
    _inherit = "stock.move.line"
    _order = 'move_line_sequence asc'

    @api.model
    def default_get(self, fields):
        res = super(StockMoveLine, self).default_get(fields)
        if self._context:
            context_keys = self._context.keys()
            next_sequence = 1
            if 'move_line_ids_without_package' in context_keys:
                if len(self._context.get('move_line_ids_without_package')) > 0:
                    next_sequence = len(self._context.get('move_line_ids_without_package')) + 1
            res.update({'move_line_sequence': next_sequence})
            if 'move_line_nosuggest_ids' in context_keys:
                if len(self._context.get('move_line_nosuggest_ids')) > 0:
                    next_sequence = len(self._context.get('move_line_nosuggest_ids')) + 1
            res.update({'move_line_sequence': next_sequence})
        return res

    sequence = fields.Char(string='Sequence')
    move_line_sequence = fields.Char(string='No')
    product_type = fields.Selection(related='product_id.type')
    quant_package_ids = fields.Many2many('stock.quant.package', compute='_compute_quant_package_ids', store=False)
    result_package_id = fields.Many2one(domain="[]")

    @api.depends('location_dest_id', 'product_id')
    def _compute_quant_package_ids(self):
        for record in self:
            if record.move_id.picking_code == 'incoming':
                package_ids = self.env['stock.quant.package'].search([('location_id', '=', record.location_dest_id.id), ('packaging_id', 'in', record.product_id.packaging_ids.ids)])
            else:
                package_ids = self.env['stock.quant.package'].search(['|', '|', ('location_id', '=', False), ('location_id', '=', record.location_dest_id.id), ('id', '=', record.package_id.id)])
            record.quant_package_ids = [(6, 0, package_ids.ids)]

    def unlink(self):
        approval = self.picking_id
        res = super(StockMoveLine, self).unlink()
        approval._reset_sequence()
        return res

    @api.onchange('sequence')
    def set_sequence_line(self):
        for record in self:
            record.picking_id._reset_sequence()

    @api.model
    def create(self, vals):
        res = super(StockMoveLine, self).create(vals)
        res.picking_id._reset_sequence()
        return res

class ReturnPicking(models.TransientModel):
    _inherit = 'stock.return.picking'

    return_reason = fields.Many2one("return.reason", string="Reason")
    return_reason_info = fields.Text("Product Return Details")
    is_return_possible = fields.Boolean("Is Return Possible", compute="_compute_is_return_possible")
    action = fields.Selection([('refund','Refund'),('repair','Repair'),('replace','Replace'),('return', "Return")], string='Action', default='refund')

    @api.depends('picking_id')
    def _compute_is_return_possible(self):
        for rec in self:
            return_date = self.env['stock.picking'].browse(self.env.context.get('active_id')).return_date_limit
            rec.is_return_possible = fields.Datetime.now() < return_date if return_date else False

    def create_returns(self):
        for wizard in self:
            new_picking_id, pick_type_id = wizard._create_returns()


        # Override the context to disable all the potential filters that could have been set previously
        ctx = dict(self.env.context)
        ctx.update({
            'default_partner_id': self.picking_id.partner_id.id,
            'search_default_picking_type_id': pick_type_id,
            'search_default_draft': False,
            'search_default_assigned': False,
            'search_default_confirmed': False,
            'search_default_ready': False,
            'search_default_planning_issues': False,
            'search_default_available': False,
        })
        current_picking_id = self.env['stock.picking'].search([('id','=',new_picking_id)])
        if current_picking_id.transfer_id:
            internal_type = self.env['ir.config_parameter'].sudo().get_param('internal_type') or False
            if internal_type == 'with_transit':
                picking_id_src = self.env['stock.picking'].search([('id','=',new_picking_id)])
                picking_id_dest = picking_id_src.copy({})
                move_lines = []
                for lines in picking_id_src.move_line_ids_without_package:
                    move_lines.append(lines.copy({}))
                transit_location = self.env.ref('equip3_inventory_masterdata.location_transit')
                stock_move_obj = self.env['stock.move']
                for picking in picking_id_src:
                    operation_type_src = self.env['stock.picking.type'].search([('default_location_dest_id','=', transit_location.id),('default_location_src_id','=', picking.location_id.id)], limit=1)
                    operation_type_dest = self.env['stock.picking.type'].search([('default_location_dest_id','=', picking.location_dest_id.id), ('default_location_src_id','=',transit_location.id)], limit=1)
                    vals_source = {
                        'location_id': picking.location_id.id,
                        'location_dest_id': transit_location.id,
                        'move_type': 'direct',
                        'partner_id': picking.partner_id.id,
                        'scheduled_date': picking.scheduled_date,
                        'picking_type_id': operation_type_src.id,
                        'origin': picking.origin,
                        'transfer_id': picking.transfer_id.id,
                        # 'branch_id': picking.branch_id.id,
                        'is_transfer_out': True,
                        'state':'draft',
                        'branch_id': picking.location_id.branch_id.id,
                    }

                    vals_dest = {
                        'location_id': transit_location.id,
                        'location_dest_id': picking.location_dest_id.id,
                        'move_type': 'direct',
                        'partner_id': picking.partner_id.id,
                        'scheduled_date': picking.scheduled_date,
                        'picking_type_id': operation_type_dest.id,
                        'origin': picking.origin,
                        'transfer_id': picking.transfer_id.id,
                        'is_transfer_in': True,
                        # 'branch_id': picking.branch_id.id,
                        'state':'draft',
                        'branch_id': picking.location_dest_id.branch_id.id,
                    }
                    src_picking = self.env['stock.picking'].create(vals_source)
                    dest_picking = self.env['stock.picking'].create(vals_dest)
                    counter = 1
                    for move in picking.move_ids_without_package:
                        src_move_data = {
                            'move_line_sequence': counter,
                            'picking_id': src_picking.id,
                            'name': move.product_id.name,
                            'product_id': move.product_id.id,
                            'product_uom_qty': move.product_uom_qty,
                            'product_uom': move.product_uom.id,
                            'location_id': picking.location_id.id,
                            'location_dest_id': transit_location.id,
                            'date': src_picking.scheduled_date,
                        }
                        dest_move_data = {
                            'move_line_sequence': counter,
                            'picking_id': dest_picking.id,
                            'name': move.product_id.name,
                            'product_id': move.product_id.id,
                            'product_uom_qty': move.product_uom_qty,
                            'product_uom': move.product_uom.id,
                            'location_id': transit_location.id,
                            'location_dest_id': picking.location_dest_id.id,
                            'date': src_picking.scheduled_date,
                        }
                        stock_move_obj.create(src_move_data)
                        stock_move_obj.create(dest_move_data)
                        counter += 1
                self.env['stock.picking'].search([('id','=',picking_id_dest.id)]).unlink()
                self.env['stock.picking'].search([('id','=',picking_id_src.id)]).unlink()
                return {
                    'name': _('Returned Picking'),
                    'view_mode': 'form,tree,calendar',
                    'res_model': 'stock.picking',
                    'res_id': src_picking.id,
                    'type': 'ir.actions.act_window',
                    'context': ctx,

                }
            else:
                    return {
                    'name': _('Returned Picking'),
                    'view_mode': 'form,tree,calendar',
                    'res_model': 'stock.picking',
                    'res_id': new_picking_id,
                    'type': 'ir.actions.act_window',
                    'context': ctx,
                    }
        else:
                    # print('Normal PIcking')
                    return {
                        'name': _('Returned Picking'),
                        'view_mode': 'form,tree,calendar',
                        'res_model': 'stock.picking',
                        'res_id': new_picking_id,
                        'type': 'ir.actions.act_window',
                        'context': ctx,
                    }

    def _prepare_move_default_values(self, return_line, new_picking):
        res = super(ReturnPicking, self)._prepare_move_default_values(return_line, new_picking)
        res.update({
            "action": return_line.action,
            "return_reason": return_line.return_reason.id if self.env.context.get("from_return_request_so_po") else self.return_reason.id
        })
        return res



class StockReturnPickingLine(models.TransientModel):
    _inherit = "stock.return.picking.line"

    return_reason = fields.Many2one("return.reason", string="Return Reason")
    action = fields.Selection([('refund','Refund'),('repair','Repair'),('replace','Replace'),('return', "Return")], string='Action', default='refund')


class TransferLogActivity(models.Model):
    _name = 'transfer.log.activity'
    _description = 'Transfer Log Activity'

    action = fields.Char(string="Action")
    reference = fields.Many2one('stock.picking', string='Reference', readonly=True)
    state = fields.Selection([('draft', 'Draft'),
                                ('waiting', 'Waiting Another Operation'),
                                ('confirmed', 'Waiting'),
                                ('assigned', 'Ready'),
                                ('done', 'Done'),
                                ('cancel', 'Cancelled'),],
                              string="Status", default="draft")
    timestamp = fields.Datetime(string="Timestamp")
    user_id = fields.Many2one('res.users', string="User")
    process_time = fields.Char(string="Processed")
    process_time_hours = fields.Float(string="Processed Time Hours")
    process_days = fields.Float(string='Processed Days')


