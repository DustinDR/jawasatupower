
from odoo import models, fields, api, _


class StockQuant(models.Model):
    _inherit = 'stock.quant'

    forecast_incoming = fields.Float(string="Forecast Incoming", compute="_compute_forecast_incoming", store=False)
    forecast_outgoing = fields.Float(string="Forecast Outgoing", compute="_compute_forecast_incoming")
    forecast_qty = fields.Float(string="Forecasted Quantity", compute="_compute_forecast_incoming")
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', compute="_compute_forecast_incoming")
    move_id = fields.Many2one('stock.move', string='Stock Move')

    def _compute_forecast_incoming(self):
        for record in self:
            record = record.sudo()
            stock_move_ids = self.env['stock.move'].search([('product_id', '=', record.product_id.id), 
                                                              ('picking_type_code', '=', 'incoming'),
                                                                '|',
                                                            ('location_id', '=', record.location_id.id),
                                                            ('location_dest_id', '=', record.location_id.id),
                                                            ])
            qty = stock_move_ids.mapped('quantity_done')
            record.forecast_incoming = sum(qty)

            stock_move_ids = self.env['stock.move'].search([('product_id', '=', record.product_id.id),
                                                              ('picking_type_code', '=', 'outgoing'),
                                                                '|',
                                                            ('location_id', '=', record.location_id.id),
                                                            ('location_dest_id', '=', record.location_id.id),
                                                            ])
            qty = stock_move_ids.mapped('quantity_done')
            record.forecast_outgoing = sum(qty)

            record.forecast_qty = record.quantity + (record.forecast_incoming - record.forecast_outgoing)
            record.warehouse_id = record.location_id.get_warehouse().id

    @api.model
    def _update_available_quantity(self, product_id, location_id, quantity, lot_id=None, package_id=None, owner_id=None, in_date=None):
        context = dict(self.env.context) or {}
        if context.get('move_id'):
            self = self.sudo()
            quants = self._gather(product_id, location_id, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=True)
            if lot_id and quantity > 0:
                quants = quants.filtered(lambda q: q.lot_id)

            incoming_dates = [d for d in quants.mapped('in_date') if d]
            incoming_dates = [fields.Datetime.from_string(incoming_date) for incoming_date in incoming_dates]
            if in_date:
                incoming_dates += [in_date]
            # If multiple incoming dates are available for a given lot_id/package_id/owner_id, we
            # consider only the oldest one as being relevant.
            if incoming_dates:
                in_date = fields.Datetime.to_string(min(incoming_dates))
            else:
                in_date = fields.Datetime.now()

            for quant in quants:
                try:
                    with self._cr.savepoint(flush=False):  # Avoid flush compute store of package
                        self._cr.execute("SELECT 1 FROM stock_quant WHERE id = %s FOR UPDATE NOWAIT", [quant.id], log_exceptions=False)
                        quant.write({
                            'quantity': quant.quantity + quantity,
                            'in_date': in_date,
                        })
                        break
                except OperationalError as e:
                    if e.pgcode == '55P03':  # could not obtain the lock
                        continue
                    else:
                        # Because savepoint doesn't flush, we need to invalidate the cache
                        # when there is a error raise from the write (other than lock-error)
                        self.clear_caches()
                        raise
            else:
                quant_id = self.create({
                    'product_id': product_id.id,
                    'location_id': location_id.id,
                    'quantity': quantity,
                    'lot_id': lot_id and lot_id.id,
                    'package_id': package_id and package_id.id,
                    'owner_id': owner_id and owner_id.id,
                    'in_date': in_date,
                    'move_id': context.get('move_id'),
                })
                if context.get('move_id'):
                    move_id = self.env['stock.move'].browse(context.get('move_id'))
                    move_id.package_quant_ids = [(4, quant_id.id)]
            return self._get_available_quantity(product_id, location_id, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=False, allow_negative=True), fields.Datetime.from_string(in_date)
        else:
            self = self.sudo()
            quants = self._gather(product_id, location_id, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=True)
            if lot_id and quantity > 0:
                quants = quants.filtered(lambda q: q.lot_id)

            incoming_dates = [d for d in quants.mapped('in_date') if d]
            incoming_dates = [fields.Datetime.from_string(incoming_date) for incoming_date in incoming_dates]
            if in_date:
                incoming_dates += [in_date]
            # If multiple incoming dates are available for a given lot_id/package_id/owner_id, we
            # consider only the oldest one as being relevant.
            if incoming_dates:
                in_date = fields.Datetime.to_string(min(incoming_dates))
            else:
                in_date = fields.Datetime.now()

            for quant in quants:
                if quant.move_id:
                    break
                try:
                    with self._cr.savepoint(flush=False):  # Avoid flush compute store of package
                        self._cr.execute("SELECT 1 FROM stock_quant WHERE id = %s FOR UPDATE NOWAIT", [quant.id], log_exceptions=False)
                        quant.write({
                            'quantity': quant.quantity + quantity,
                            'in_date': in_date,
                        })
                        break
                except OperationalError as e:
                    if e.pgcode == '55P03':  # could not obtain the lock
                        continue
                    else:
                        # Because savepoint doesn't flush, we need to invalidate the cache
                        # when there is a error raise from the write (other than lock-error)
                        self.clear_caches()
                        raise
            else:
                self.create({
                    'product_id': product_id.id,
                    'location_id': location_id.id,
                    'quantity': quantity,
                    'lot_id': lot_id and lot_id.id,
                    'package_id': package_id and package_id.id,
                    'owner_id': owner_id and owner_id.id,
                    'in_date': in_date,
                })
            return self._get_available_quantity(product_id, location_id, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=False, allow_negative=True), fields.Datetime.from_string(in_date)
