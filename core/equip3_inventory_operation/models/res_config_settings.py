
from odoo import api, fields, models, _


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    internal_type = fields.Selection([('with_transit', 'Use Transit Location before deliver to Destination Location'), ('direct_transit', 'Deliver directly to Destination Location')], string="Interwarehouse Transfer", default="with_transit")
    ex_period = fields.Integer(string="Interwarehouse Transfer Request Expiry", help="The Expiry Date field on the Internal Transfer Request form will automatically be filled with the date and time based on the day period you fill in here.", default=30)
    material_request = fields.Integer(string="Material Request Expiry", default=30)
    is_product_service_operation = fields.Boolean(string="Product Service Operation", help="When Active, All Service Type Products Automatically Will Create A Receiving Note After Confirming A Purchase Or Delivery Order After Confirming A Sale. You Also Can Set Wheter The Product Will Create Operation Or Not On Each Product Template", default=False)
    mr_expiry_days = fields.Selection([('before', 'Before'), ('after', 'After')], default="after")
    itr_expiry_days = fields.Selection([('before', 'Before'), ('after', 'After')], default="after")
    is_return_limit_policy = fields.Boolean("Return Limit Policy", config_parameter="is_return_limit_policy")
    return_policy_days = fields.Integer("Return Policy Days", config_parameter="return_policy_days")
    is_display_warehouse_address = fields.Boolean("Display Warehouse Address on Delivery Slip")


    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        res.update({
            'internal_type': IrConfigParam.get_param('internal_type', "with_transit"),
            'ex_period': int(IrConfigParam.get_param('ex_period', 30)),
            'material_request': int(IrConfigParam.get_param('material_request', 30)),
            'is_product_service_operation' : IrConfigParam.get_param('is_product_service_operation', False),
            'mr_expiry_days': IrConfigParam.get_param('mr_expiry_days', 'before'),
            'is_display_warehouse_address' : IrConfigParam.get_param('is_display_warehouse_address', False),
            'manufacturing': IrConfigParam.get_param('manufacturing', False),
        })
        return res

    def set_values(self):
        res = super(ResConfigSettings, self).set_values()
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        IrConfigParam.set_param('internal_type', self.internal_type)
        IrConfigParam.set_param('ex_period', self.ex_period)
        IrConfigParam.set_param('material_request', self.material_request)
        IrConfigParam.set_param('manufacturing', self.manufacturing)
        IrConfigParam.set_param('is_product_service_operation', self.is_product_service_operation)
        IrConfigParam.set_param('mr_expiry_days', self.mr_expiry_days)
        IrConfigParam.set_param('is_display_warehouse_address', self.is_display_warehouse_address)

        if self.internal_type == 'with_transit':
            self.env.ref('equip3_inventory_operation.menu_internal_transfer_in').active = True
            self.env.ref('equip3_inventory_operation.menu_internal_transfer_out').active = True
            self.env.ref('equip3_inventory_operation.menu_internal_transfer_notes').active = False
        else:
            self.env.ref('equip3_inventory_operation.menu_internal_transfer_in').active = False
            self.env.ref('equip3_inventory_operation.menu_internal_transfer_out').active = False
            self.env.ref('equip3_inventory_operation.menu_internal_transfer_notes').active = True

        IrConfigParam.set_param('is_return_orders', self.is_return_orders)
        if self.is_return_orders:
            self.env.ref('dev_rma.manu_dev_rma_rma').active = True
            self.env.ref('equip3_inventory_operation.menu_dev_rma_rma_main_so').active = True
        else:
            self.env.ref('dev_rma.manu_dev_rma_rma').active = False
            self.env.ref('equip3_inventory_operation.menu_dev_rma_rma_main_so').active = False

        product_template_ids = self.env['product.template'].search([('type', '=', 'service')])
        product_ids = self.env['product.product'].search([('type', '=', 'service')])
        if self.is_product_service_operation:
            for template in product_template_ids:
                template.write({'is_product_service_operation' : True})
            for product in product_ids:
                product.write({'is_product_service_operation' : True})
        else:
            for template in product_template_ids:
                template.write({'is_product_service_operation' : False})
            for product in product_ids:
                product.write({'is_product_service_operation' : False})
        return res

    @api.onchange("is_return_limit_policy")
    def _onchange_is_return_limit_policy(self):
        self.return_policy_days = self.is_return_limit_policy and self.return_policy_days or 0
