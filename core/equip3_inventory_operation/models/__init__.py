# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import stock_picking_type_dashboard
from . import stock_picking
from . import internal_transfer
from . import stock_picking_type
from . import stock_package_level
from . import stock_location
from . import ir_sequence
from . import res_config_settings
from . import dev_rma_rma
from . import merge_picking
from . import stock_assign_serial
from . import material_request
from . import sale_order
from . import return_reason
from . import ir_module
from . import stock_quant
from . import stock_quant_package
from . import repair_order