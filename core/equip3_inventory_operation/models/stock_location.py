from odoo import models, fields, api
from odoo.exceptions import UserError
from datetime import datetime

class StockLocation(models.Model):
    _inherit = 'stock.location'

    location_complete_name = fields.Char("Full Location Name", compute='_compute_location_name', store=True)

    @api.depends('name', 'location_id.name')
    def _compute_location_name(self):
        for record in self:
            name = record.name
            current = record
            while current.location_id:
                current = current.location_id
                name = '%s/%s' % (current.name, name)
            record.location_complete_name = name

    @api.model
    def create(self, vals):
        res = super(StockLocation, self).create(vals)
        # if len(res.location_complete_name.split('/')) >= 2:
        #     location_complete_name = res.location_complete_name.split('/')
        #     name = location_complete_name[1]
        #     location_id = self.search([('complete_name', '=', name)], limit=1)
        # else:
        #     location_id = res.location_id
        # warehouse_id = self.env['stock.warehouse'].search([('view_location_id', '=', location_id.id)], limit=1)
        # print('be_warehouse',warehouse_id)
        res.create_op_type()
        return res

    def create_op_type(self):
        # if self.usage == 'internal':
        #     print('loc',self.id)
        warehouse_id = self.env['stock.warehouse'].search([('code', '=', self.location_id.name)], limit=1)
        all_warehouse = self.env['stock.warehouse'].search([])
        # warehouse_id = False
        for ware in all_warehouse:
            code = ware.code + '/'
            print('code',code)
            if self.location_id and code in self.location_id.display_name:
                print('warename', ware.name)
                warehouse_id = ware
        #     print('names', ware.code)
        # print('warehouse_id',warehouse_id)
        # print(sss)
        if warehouse_id:
            source_location_id = self.env.ref('equip3_inventory_masterdata.location_transit').id
            sequence1_vals = {
                'name': warehouse_id.name + ' ' + self.name_get()[0][1] + ' Sequence Internal IN',
                'implementation': 'standard',
                'prefix': self.name_get()[0][1] + '/INT/IN',
                'padding': 3,
                'number_increment': 1,
                'number_next_actual': 1,
                'company_id': self.company_id.id
            }
            sequence1_id = self.env['ir.sequence'].create(sequence1_vals)
            operation1_vals = {
                'name': 'Internal Transfer IN ',
                'sequence_code': 'INT/IN',
                'code': 'internal',
                'default_location_src_id': source_location_id,
                'default_location_dest_id': self.id,
                'warehouse_id': warehouse_id and warehouse_id.id or False,
                'sequence_id': sequence1_id.id,
                'is_transit': True,
                'company_id': self.company_id.id
            }
            in_operation_id = self.env['stock.picking.type'].create(operation1_vals)
            sequence2_vals = {
                'name': warehouse_id.name + ' ' + self.name_get()[0][1] + ' Sequence Internal OUT',
                'implementation': 'standard',
                'prefix': self.name_get()[0][1] + '/INT/OUT',
                'padding': 3,
                'number_increment': 1,
                'number_next_actual': 1,
                'company_id': self.company_id.id,
            }
            sequence2_id = self.env['ir.sequence'].create(sequence2_vals)
            operation2_vals = {
                'name': 'Internal Transit OUT',
                'sequence_code': 'INT/OUT',
                'code': 'internal',
                'default_location_src_id': self.id,
                'default_location_dest_id': source_location_id,
                'warehouse_id': warehouse_id and warehouse_id.id or False,
                'sequence_id': sequence2_id.id,
                'is_transit': True,
                'company_id': self.company_id.id,
            }
            out_operation_id = self.env['stock.picking.type'].create(operation2_vals)
        # return res

