
from odoo import api, fields, models, _


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    is_product_service_operation = fields.Boolean(string="Product Service Operation")