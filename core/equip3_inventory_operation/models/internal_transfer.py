
import pytz
from pytz import timezone, UTC
from odoo import SUPERUSER_ID, _, api, fields, models
from datetime import datetime, date, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import ValidationError, UserError
from odoo import tools
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT


class InternalTransfer(models.Model):
    _name = "internal.transfer"
    _description = "Internal Transfer"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']

    name = fields.Char(string="Reference", readonly= True ,default='New',tracking=True)
    source_location_id = fields.Many2one('stock.location',string="Source Location", tracking=True)
    destination_location_id = fields.Many2one('stock.location',string="Destination Location", tracking=True)
    scheduled_date = fields.Datetime(string="Scheduled Date", required=True,tracking=True)
    description = fields.Text(string="Description",tracking=True)
    source_document = fields.Char(string="Source Document", readonly=True,tracking=True)
    state = fields.Selection(string='Status',selection=[
        ('draft','Draft'),
        ('to_approve', 'Waiting For Approval'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('confirm','Confirmed'),
        ('done','Done'),
        ('cancel','Cancelled')
    ],default='draft')
    state1 = fields.Selection(related='state')
    state2 = fields.Selection(related='state')
    state3 = fields.Selection(related='state')
    product_line_ids = fields.One2many('internal.transfer.line','product_line',string="Product Line")
    operation_type_in_id = fields.Many2one('stock.picking.type',string="Operation Type IN", readonly='1', tracking=True)
    operation_type_out_id = fields.Many2one('stock.picking.type',string="Operation Type Out", readonly='1',tracking=True)
    is_transit = fields.Boolean(string='Transit Operation')
    operation_count = fields.Integer(string='Operation Count', compute='_compute_operation_count')
    expiry_date = fields.Datetime(string="Expiry Date", help="Expiry Date is autofill based on the Expiry Period after this request document is created")
    is_source_loc = fields.Boolean(string="Source Location")
    is_destination_loc = fields.Boolean(string="Destination Location")
    internal_transfer_line_total = fields.Integer(string="Internal Transfer", store=True, compute='calculate_lines_total', tracking=True)
    operations = fields.Text()

    itr_approval_matrix_id = fields.Many2one('itr.approval.matrix', string="Approval Matrix", compute='_get_approval_matrix')
    is_itr_approval_matrix = fields.Boolean(string="ITR Request", compute='_get_approve_button_from_config')
    approved_matrix_ids = fields.One2many('itr.approval.matrix.line', 'transfer_id', compute="_compute_approving_matrix_lines_itr", store=True, string="Approved Matrix")
    approval_matrix_line_id = fields.Many2one('itr.approval.matrix.line', string='Internal Approval Matrix Line', compute='_get_approve_button', store=False)
    is_approve_button = fields.Boolean(string='Is Approve Button', compute='_get_approve_button', store=False)
    is_reset_to_draft = fields.Boolean(string='Is Reset to Draft', compute='_get_is_show_draft', store=False)
    is_product_lines = fields.Boolean(string='Is Product Lines', compute='_get_is_product_lines', store=False)

    source_warehouse_id = fields.Many2one('stock.warehouse', string="Source Warehouse")
    destination_warehouse_id = fields.Many2one('stock.warehouse', string="Destination Warehouse")
    company_id = fields.Many2one('res.company', related="source_warehouse_id.company_id",string="Company", readonly=True, tracking=True)
    branch_id = fields.Many2one('res.branch', related="source_warehouse_id.branch_id", string="Branch", readonly=True, tracking=True)
    filter_source_warehouse_id = fields.Many2many('stock.location', compute="_compute_source_warehouse", string="Source Warehouse")
    filter_destination_warehouse_id = fields.Many2many('stock.location', compute="_compute_destination_warehouse", string="Destination Warehouse")
    analytic_account_group_ids = fields.Many2many('account.analytic.tag', 'it_analytic_rel', 'tag_id', 'it_id', string="Analytic Groups", default=lambda self: self.env.user.analytic_tag_ids.ids)
    requested_by = fields.Many2one('res.users', 'Requested By', required='1', tracking=True, default=lambda self : self.env.user.id)
    get_warehouse = fields.Boolean(compute="_get_warehouses", string='Get Warehouse')

    def _get_warehouses(self):
        # for record in self:
        #     if not record.source_warehouse_id:
        #         record.write({'source_warehouse_id': record.source_location_id.warehouse_id.id})
        #     if not record.destination_warehouse_id:
        #         record.write({'destination_warehouse_id': record.destination_location_id.warehouse_id.id})
        self.get_warehouse = True


    def _get_street(self, partner):
        self.ensure_one()
        res = {}
        address = ''
        if partner.street:
            address = "%s" % (partner.street)
        if partner.street2:
            address += ", %s" % (partner.street2)
        # reload(sys)
        html_text = str(tools.plaintext2html(address, container_tag=True))
        data = html_text.split('p>')
        if data:
            return data[1][:-2]
        return False

    def _get_address_details(self, partner):
        self.ensure_one()
        res = {}
        address = ''
        if partner.city:
            address = "%s" % (partner.city)
        if partner.state_id.name:
            address += ", %s" % (partner.state_id.name)
        if partner.zip:
            address += ", %s" % (partner.zip)
        if partner.country_id.name:
            address += ", %s" % (partner.country_id.name)
        # reload(sys)
        html_text = str(tools.plaintext2html(address, container_tag=True))
        data = html_text.split('p>')
        if data:
            return data[1][:-2]
        return False



    @api.onchange('scheduled_date')
    def compute_expiry_date(self):
        for record in self:
            IrConfigParam = self.env['ir.config_parameter'].sudo()
            itr_expiry_days = IrConfigParam.get_param('mr_expiry_days', 'before')
            itr_ex_period = IrConfigParam.get_param('ex_period', 0)
            if record.scheduled_date:
                if itr_expiry_days == 'before':
                    record.expiry_date = record.scheduled_date - timedelta(days=int(itr_ex_period))
                else:
                    record.expiry_date = record.scheduled_date + timedelta(days=int(itr_ex_period))

    @api.depends('source_warehouse_id')
    def _compute_source_warehouse(self):
        for record in self:
            location_ids = []
            if record.source_warehouse_id:
                location_obj = record.env['stock.location']
                store_location_id = record.source_warehouse_id.view_location_id.id
                addtional_ids = location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')])
                for location in addtional_ids:
                    if location.location_id.id not in addtional_ids.ids:
                        location_ids.append(location.id)
                child_location_ids = record.env['stock.location'].search([('id', 'child_of', location_ids), ('id', 'not in', location_ids)]).ids
                final_location = child_location_ids + location_ids
                record.filter_source_warehouse_id = [(6, 0, final_location)]
            else:
                record.filter_source_warehouse_id = [(6, 0, [])]

    @api.depends('destination_warehouse_id')
    def _compute_destination_warehouse(self):
        for record in self:
            location_ids = []
            if record.destination_warehouse_id:
                location_obj = record.env['stock.location']
                store_location_id = record.destination_warehouse_id.view_location_id.id
                addtional_ids = location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')])
                for location in addtional_ids:
                    if location.location_id.id not in addtional_ids.ids:
                        location_ids.append(location.id)
                child_location_ids = record.env['stock.location'].search([('id', 'child_of', location_ids), ('id', 'not in', location_ids)]).ids
                final_location = child_location_ids + location_ids
                record.filter_destination_warehouse_id = [(6, 0, final_location)]
            else:
                record.filter_destination_warehouse_id = [(6, 0, [])]


    @api.depends('product_line_ids')
    def _get_is_product_lines(self):
        for record in self:
            record.is_product_lines = False
            if record.product_line_ids:
                record.is_product_lines = True

    def _get_is_show_draft(self):
        for record in self:
            not_approved_lines = record.approved_matrix_ids.filtered(lambda r: not r.approved_users)
            if record.is_itr_approval_matrix and \
                record.state == 'to_approve' and \
                len(not_approved_lines) == len(record.approved_matrix_ids):
                record.is_reset_to_draft = True
            else:
                record.is_reset_to_draft = False

    def _get_approve_button(self):
        for record in self:
            matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
            if len(matrix_line) == 0:
                record.is_approve_button = False
                record.approval_matrix_line_id = False
            elif len(matrix_line) > 0:
                matrix_line_id = matrix_line[0]
                if self.env.user.id in matrix_line_id.user_ids.ids and self.env.user.id != matrix_line_id.last_approved.id:
                    record.is_approve_button = True
                    record.approval_matrix_line_id = matrix_line_id.id
                else:
                    record.is_approve_button = False
                    record.approval_matrix_line_id = False
            else:
                record.is_approve_button = False
                record.approval_matrix_line_id = False

    @api.depends('itr_approval_matrix_id')
    def _compute_approving_matrix_lines_itr(self):
        data = [(5, 0, 0)]
        for record in self:
            counter = 1
            record.approved_matrix_ids = []
            for line in record.itr_approval_matrix_id.itr_approval_matrix_line_ids:
                data.append((0, 0, {
                    'sequence' : counter,
                    'user_ids' : [(6, 0, line.user_ids.ids)],
                    'minimum_approver' : line.minimum_approver,
                }))
                counter += 1
            record.approved_matrix_ids = data

    def _get_approve_button_from_config(self):
        is_internal_transfer_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_internal_transfer_approval_matrix', False)
        for record in self:
            record.is_itr_approval_matrix = is_internal_transfer_approval_matrix
            print('rita,', record.is_itr_approval_matrix)

    @api.depends('source_warehouse_id')
    def _get_approval_matrix(self):
        for record in self:
            # matrix_id = self.env['itr.approval.matrix'].search([('location_child_ids', 'in', record.source_location_id.ids)], limit=1)
            matrix_id = self.env['itr.approval.matrix'].search([('warehouse_id', '=', record.source_warehouse_id.id)], limit=1)
            record.itr_approval_matrix_id = matrix_id

    def itr_request_for_approving(self):
        for record in self:
            record.write({'state': 'to_approve'})

    def itr_approving(self):
        for record in self:
            user = self.env.user
            if record.is_approve_button and record.approval_matrix_line_id:
                approval_matrix_line_id = record.approval_matrix_line_id
                if user.id in approval_matrix_line_id.user_ids.ids and \
                    user.id not in approval_matrix_line_id.approved_users.ids:
                    name = approval_matrix_line_id.state_char or ''
                    utc_datetime = datetime.now()
                    local_timezone = pytz.timezone(self.env.user.tz)
                    local_datetime = utc_datetime.replace(tzinfo=pytz.utc)
                    local_datetime = local_datetime.astimezone(local_timezone).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                    if name != '':
                        name += "\n • %s: Approved - %s" % (self.env.user.name, local_datetime)
                    else:
                        name += "• %s: Approved - %s" % (self.env.user.name, local_datetime)

                    approval_matrix_line_id.write({ 
                        'last_approved': self.env.user.id, 'state_char': name, 
                        'approved_users': [(4, user.id)]})
                    if approval_matrix_line_id.minimum_approver == len(approval_matrix_line_id.approved_users.ids):
                        approval_matrix_line_id.write({'time_stamp': datetime.now(), 'approved': True})
                        # next_approval_matrix_line_id = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
                        # if next_approval_matrix_line_id and len(next_approval_matrix_line_id[0].approver) > 1:
                        #     pass
            if len(record.approved_matrix_ids) == len(record.approved_matrix_ids.filtered(lambda r:r.approved)):
                record.write({'state': 'approved'})

    def itr_reject(self):
        for record in self:
            return {
                    'type': 'ir.actions.act_window',
                    'name': 'Reject Internal Transfer Request',
                    'res_model': 'itr.matrix.reject',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'target': 'new',
                }

    def itr_reset_to_draft(self):
        for record in self:
            record.write({'state': 'draft'})
            record.approved_matrix_ids.write({
                'state_char': False,
                'approved_users': [(6, 0, [])],
                'approved': False,
                "feedback": False,
                'time_stamp': False,
                'last_approved': False,
                'approved': False
            })

    @api.onchange('source_location_id')
    def _compute_source_loc(self):
        self._get_approve_button_from_config()
        self._get_is_product_lines()
        for record in self:
            if len(record.product_line_ids) != 0:
                record.is_source_loc = True
            else:
                record.is_source_loc = False

    @api.onchange('source_warehouse_id', 'destination_warehouse_id')
    def _onchange_warehouse_id_for_location(self):
        for record in self:
            if record.source_warehouse_id:
                source_location_id = self.env['stock.location'].search([('warehouse_id', '=', record.source_warehouse_id.id), ('usage', '=', 'internal')], limit=1, order="id")
                record.source_location_id = source_location_id
                record.analytic_account_group_ids = record.source_warehouse_id.branch_id.analytic_tag_ids
            if record.destination_warehouse_id:
                destination_location_id = self.env['stock.location'].search([('warehouse_id', '=', record.destination_warehouse_id.id), ('usage', '=', 'internal')], limit=1, order="id")
                record.destination_location_id = destination_location_id
        
    @api.depends('product_line_ids')
    def calculate_lines_total(self):
        for record in self:
            record.internal_transfer_line_total = len(record.product_line_ids)

    @api.onchange('destination_location_id')
    def _compute_destination_loc(self):
        for record in self:
            if len(record.product_line_ids) != 0:
                record.is_destination_loc = True
            else:
                record.is_destination_loc = False

    def upd_source(self):
        for record in self:
            for product_line_id in record.product_line_ids:
                product_line_id.write({'source_location_id' : record.source_location_id.id})
            record.is_source_loc = False
                

    def upd_dest(self):
        for record in self:
            for product_line_id in record.product_line_ids:
                product_line_id.write({'destination_location_id' : record.destination_location_id.id})
            record.is_destination_loc = False
       
    def _compute_operation_count(self):
        for record in self:
            record.operation_count = self.env['stock.picking'].search_count([('transfer_id', '=', record.id)])

    def action_confirm(self):
        for transfer in self:
            if not transfer.product_line_ids:
                raise ValidationError(_('Please add product lines'))
            temp_list = []
            line_vals_list = []
            for line in transfer.product_line_ids:
                if line.scheduled_date.date() in temp_list:
                    filter_line = list(filter(lambda r:r.get('date') == line.scheduled_date.date(), line_vals_list))
                    if filter_line:
                        filter_line[0]['lines'].append(line)
                else:
                    temp_list.append(line.scheduled_date.date())
                    line_vals_list.append({
                        'date': line.scheduled_date.date(),
                        'lines': [line]
                    })
            for line_vals in line_vals_list:
                if transfer.is_transit:
                    stock_move_obj = self.env['stock.move']
                    transit_location = self.env.ref('equip3_inventory_masterdata.location_transit')
                    do_data = {
                        'location_id': transfer.source_location_id.id,
                        'location_dest_id': transit_location.id,
                        'origin_dest_location': transfer.destination_location_id.location_id.name + '/' + transfer.destination_location_id.name,
                        'move_type': 'direct',
                        'partner_id': transfer.create_uid.partner_id.id,
                        'scheduled_date': line_vals.get('date'),
                        'analytic_account_group_ids': [(6, 0, transfer.source_location_id.warehouse_id.branch_id.analytic_tag_ids.ids)],
                        'picking_type_id': transfer.operation_type_out_id.id,
                        'origin': transfer.name,
                        'transfer_id': transfer.id,
                        # 'branch_id': transfer.branch_id and transfer.branch_id.id or False,
                        'is_transfer_out': True,
                        'company_id': transfer.company_id.id,
                        'branch_id': transfer.source_location_id.warehouse_id.branch_id.id,
                    }
                    do_picking = self.env['stock.picking'].create(do_data)
                    receipt_data = {
                        'location_id': transit_location.id,
                        'location_dest_id': transfer.destination_location_id.id,
                        'origin_src_location': transfer.source_location_id.location_id.name + '/' + transfer.source_location_id.name,
                        'move_type': 'direct',
                        'partner_id': transfer.create_uid.partner_id.id,
                        'scheduled_date': line_vals.get('date'),
                        'picking_type_id': transfer.operation_type_in_id.id,
                        'analytic_account_group_ids': [(6, 0, transfer.destination_location_id.warehouse_id.branch_id.analytic_tag_ids.ids)],
                        'origin': transfer.name,
                        'transfer_id': transfer.id,
                        'is_transfer_in': True,
                        'company_id': transfer.company_id.id,
                        # 'branch_id': transfer.branch_id and transfer.branch_id.id or False,
                        'branch_id': transfer.destination_location_id.warehouse_id.branch_id.id,
                    }
                    receipt_picking = self.env['stock.picking'].create(receipt_data)
                    counter = 1
                    for line in line_vals.get('lines'):
                        receipt_move_data = {
                            'move_line_sequence': counter,
                            'picking_id': receipt_picking.id,
                            'name': line.product_id.name,
                            'product_id': line.product_id.id,
                            'product_uom_qty': line.qty,
                            'product_uom': line.uom.id,
                            'analytic_account_group_ids': [(6, 0, receipt_picking.analytic_account_group_ids.ids)],
                            'location_id': transit_location.id,
                            'location_dest_id': line.destination_location_id.id,
                            'origin_src_location': transfer.source_location_id.location_id.name + '/' + transfer.source_location_id.name,
                            'date': line_vals.get('date'),
                            'is_transit': True,
                            'origin': transfer.name,
                            # 'is_transfer_in': True,
                        }
                        receipt_move = stock_move_obj.create(receipt_move_data)
                        receipt_move._onchange_remaining_qty()
                        do_move_data = {
                            'move_line_sequence': counter,
                            'picking_id': do_picking.id,
                            'name': line.product_id.name,
                            'product_id': line.product_id.id,
                            'product_uom_qty': line.qty,
                            'product_uom': line.uom.id,
                            'analytic_account_group_ids': [(6, 0, do_picking.analytic_account_group_ids.ids)],
                            'location_id': line.source_location_id.id,
                            'location_dest_id': transit_location.id,
                            'origin_dest_location': transfer.destination_location_id.location_id.name + '/' + transfer.destination_location_id.name,
                            'date': line_vals.get('date'),
                            'is_transit': True,
                            'origin': transfer.name,
                            # 'is_transfer_out': True,
                        }
                        do_move = stock_move_obj.create(do_move_data)
                        do_move._onchange_remaining_qty()
                        counter += 1
                if not transfer.is_transit:
                    do_data = {
                        'location_id': transfer.source_location_id.id,
                        'location_dest_id': transfer.destination_location_id.id,
                        'move_type': 'direct',
                        'partner_id': transfer.create_uid.partner_id.id,
                        'scheduled_date': line_vals.get('date'),
                        'picking_type_id': transfer.operation_type_out_id.id,
                        'origin': transfer.name,
                        'company_id': transfer.company_id.id,
                        'analytic_account_group_ids': [(6, 0, transfer.analytic_account_group_ids.ids)],
                        'branch_id': transfer.branch_id and transfer.branch_id.id or False,
                        'transfer_id': transfer.id,
                    }
                    do_picking = self.env['stock.picking'].create(do_data)
                    counter = 1
                    for line in line_vals.get('lines'):
                        stock_move_obj = self.env['stock.move']
                        do_move_data = {
                            'move_line_sequence': counter,
                            'picking_id': do_picking.id,
                            'name': line.product_id.name,
                            'product_id': line.product_id.id,
                            'product_uom_qty': line.qty,
                            'product_uom': line.uom.id,
                            'analytic_account_group_ids': [(6, 0, transfer.analytic_account_group_ids.ids)],
                            'location_id': line.source_location_id.id,
                            'location_dest_id': line.destination_location_id.id,
                            'date': line_vals.get('date'),
                        }
                        counter += 1
                        do_move = stock_move_obj.create(do_move_data)
                        do_move._onchange_remaining_qty()
                transfer.write({'state': 'confirm'})

            stock_picking = self.env['stock.picking'].search([('transfer_id','=', self.id)])
            if stock_picking:
                for picking in stock_picking:
                    self.product_line_ids.write({'picking_id': [(4, picking.id)]})
                # self.product_line_ids.write({'picking_id': (0, 0, stock_picking.ids)})

    def action_done(self):
        for record in self:
            record.write({'state': 'done'})

    def print_report(self):
        for record in self:
            # print('zoropr')
            all_transfer_op = record.env['stock.picking'].search([('transfer_id', '=', record.id), ('state', '!=', 'cancel')])
            operations = ''
            for picking in all_transfer_op:
                print('name',picking.name)
                operations =  operations + '• ' + str(picking.name) + '\n' 
            self.operations = operations 
            return self.env.ref('equip3_inventory_operation.report_internal_transfer').report_action(self)


    def action_cancel(self):
        for record in self:
            record.write({'state': 'cancel'})
            all_transfer_op = record.env['stock.picking'].search([('transfer_id', '=', record.id), ('state', '!=', 'cancel')])
            for picking in all_transfer_op:
                print('name',picking.name)
                picking.action_cancel()





    def button_notes(self):
        view_id = self.env.ref('equip3_inventory_operation.view_tree_stock_picking')
        return {
            'name': 'Stock Picking',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'domain': [('transfer_id', '=', self.id), ('state', '!=', 'cancel')],
            'res_model': 'stock.picking',
            'target': 'current',
            'views': [(view_id.id, 'tree'),(False,'form')],
        }

    @api.model
    def default_get(self, fields):
        res = super(InternalTransfer, self).default_get(fields)
        IrConfigParam = self.env['ir.config_parameter'].sudo()
        internal_type = IrConfigParam.get_param('internal_type', False)
        if internal_type == 'with_transit':
            res['is_transit'] = True
        return res

    @api.model
    def _expire_date_cron(self):
        today_date = datetime.now()
        expire_records = self.search([
                            ('state', 'in', ('draft', 'to_approve')),
                            ('expiry_date', '<', today_date)
                        ])
        template_id = self.env.ref('equip3_inventory_operation.email_template_expired_internal_transfer_request')
        for record in expire_records:
            record.write({'state': 'cancel'})
            record.send_email_notification(template_id, 'equip3_inventory_operation.email_template_expired_internal_transfer_request', record.create_uid)

    @api.model
    def _expire_date_reminder_cron(self):
        today_date = date.today() + timedelta(days=1)
        expire_records = self.search([
                            ('state', 'in', ('draft', 'to_approve'))
                        ])
        user_reminder_template = self.env.ref('equip3_inventory_operation.email_template_expired_internal_transfer_reminder_user')
        approver_user_template = self.env.ref('equip3_inventory_operation.email_template_expired_internal_transfer_reminder_approved_user')
        for record in expire_records:
            if record.expiry_date and record.expiry_date.date() == today_date:
                record.send_email_notification(user_reminder_template, 'equip3_inventory_operation.email_template_expired_internal_transfer_reminder_user', record.create_uid)
                matrix_line = sorted(record.approved_matrix_ids.filtered(lambda r: not r.approved), key=lambda r:r.sequence)
                if record.is_itr_approval_matrix and matrix_line:
                    matrix_line = matrix_line[0]
                    approver_user = False
                    for user in matrix_line.user_ids:
                        if user.id in matrix_line.user_ids.ids and \
                            user.id not in matrix_line.approved_users.ids:
                            approver_user = user
                            break
                    record.send_email_notification(approver_user_template, 'equip3_inventory_operation.email_template_expired_internal_transfer_reminder_approved_user', approver_user)

    def send_email_notification(self, template_id, template_name, user_id):
        record = self
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        action_id = self.env.ref('equip3_inventory_operation.action_internal_transfer_request').id
        url = base_url + '/web#id='+ str(record.id) + '&action=' + str(action_id) +'&view_type=form&model=internal.transfer'
        ctx = {
                'email_from': self.env.user.company_id.email,
                'email_to': user_id.partner_id.email,
                'user_name': user_id.name,
                'url': url,
            }
        template_id.with_context(ctx).send_mail(record.id, True)
        template_id = self.env["ir.model.data"].xmlid_to_object(template_name)

        body_html = self.env['mail.render.mixin'].with_context(ctx)._render_template(
                    template_id.body_html, 'internal.transfer', record.ids, post_process=True)[record.id]
        message_id = (
            self.env["mail.message"]
            .sudo()
            .create(
                {
                    "subject": "Internal Transfer Expiry",
                    "body": body_html,
                    "model": "internal.transfer",
                    "res_id": record.id,
                    "message_type": "notification",
                    "partner_ids": [
                        (
                            6,
                            0,
                            user_id.partner_id.ids,
                        )
                    ],
                }
            )
        )
        notif_create_values = {
            "mail_message_id": message_id.id,
            "res_partner_id": user_id.partner_id.id,
            "notification_type": "inbox",
            "notification_status": "sent",
        }
        self.env["mail.notification"].sudo().create(notif_create_values)
    
    @api.model
    def create(self, vals):
        # vals['name'] = self.env['ir.sequence'].next_by_code('internal.transfer')
        seq1 = self.env['ir.sequence'].sudo().search([('name', '=', 'Internal Transfer')])
        material_req = self.env['internal.transfer'].search([])
        check_today = False
        for rec in material_req:
            if rec.create_date.date() == date.today():
                check_today = True
                break
        if check_today == True:
            seq = self.env['ir.sequence'].next_by_code('internal.transfer')
            vals['name'] = seq
            print('TOday')
        else:
            seq1.sudo().write({'number_next_actual': 1})
            seq = self.env['ir.sequence'].next_by_code('internal.transfer')
            vals['name'] = seq
            print('Next')
        res = super(InternalTransfer, self).create(vals)
        return res

    @api.onchange('source_location_id', 'is_transit')
    def onchange_source_loction_id(self):
        for record in self:
            transit_location_id = self.env.ref('equip3_inventory_masterdata.location_transit')
            if record.source_location_id and not record.is_transit:
                condition = [
                    ('default_location_src_id', '=', record.source_location_id.id), 
                    ('default_location_dest_id', '=', record.source_location_id.id), 
                ]
                picking_type_id = self.env['stock.picking.type'].search(condition, limit=1)
                record.operation_type_out_id = picking_type_id and picking_type_id.id or False
            elif record.source_location_id and record.is_transit:
                condition = [
                    ('code', '=', 'internal'),
                    ('is_transit', '=', True),
                    ('default_location_dest_id', '=', transit_location_id.id),
                    ('default_location_src_id', '=' , record.source_location_id.id)
                ]
                picking_type_id = self.env['stock.picking.type'].search(condition, limit=1)
                record.operation_type_out_id = picking_type_id and picking_type_id.id or False

    @api.onchange('destination_location_id', 'is_transit')
    def onchange_dest_loction_id(self):
        for record in self:
            transit_location_id = self.env.ref('equip3_inventory_masterdata.location_transit')
            if record.destination_location_id and not record.is_transit:
                condition = [
                    ('default_location_src_id', '=', record.destination_location_id.id), 
                    ('default_location_dest_id', '=', record.destination_location_id.id), 
                ]
                picking_type_id = self.env['stock.picking.type'].search(condition, limit=1)
                record.operation_type_in_id = picking_type_id and picking_type_id.id or False
            elif record.destination_location_id and record.is_transit:
                condition = [
                    ('code', '=', 'internal'),
                    ('is_transit', '=', record.is_transit),
                    ('default_location_dest_id','=', record.destination_location_id.id),
                    ('default_location_src_id','=', transit_location_id.id)
                ]
                picking_type_id = self.env['stock.picking.type'].search(condition, limit=1)
                record.operation_type_in_id = picking_type_id and picking_type_id.id or False

    def calculate_transfer_qty(self, stock_picking):
        for record in self:
            if stock_picking:
                internal_type = record.env['ir.config_parameter'].sudo().get_param('internal_type') or False
                transit_location = record.env.ref('equip3_inventory_masterdata.location_transit')
                if internal_type == 'with_transit':
                    for picking in stock_picking:
                        if picking.location_id.id == transit_location.id and \
                            picking.location_dest_id.id == record.destination_location_id.id:
                            for line in picking.move_ids_without_package:
                                product_lines = record.product_line_ids.filtered(lambda r:r.product_id.id == line.product_id.id)
                                total_qty = line.quantity_done
                                for itr_line in product_lines:
                                    if itr_line.transfer_qty != itr_line.qty:
                                        if itr_line.qty <= total_qty:
                                            itr_line.transfer_qty = itr_line.qty
                                            total_qty -= itr_line.transfer_qty
                                        elif itr_line.qty > total_qty:
                                            diff = itr_line.qty - total_qty
                                            itr_line.transfer_qty += itr_line.qty - diff
                                            total_qty -= itr_line.transfer_qty
                                        else:
                                            itr_line.transfer_qty = 0
                else:
                    for picking in stock_picking:
                        if picking.location_id.id == record.source_location_id.id and \
                            picking.location_dest_id.id == record.destination_location_id.id:
                            for line in picking.move_ids_without_package:
                                product_lines = record.product_line_ids.filtered(lambda r:r.product_id.id == line.product_id.id)
                                total_qty = line.quantity_done
                                for itr_line in product_lines:
                                    if itr_line.transfer_qty != itr_line.qty:
                                        if itr_line.qty <= total_qty:
                                            itr_line.transfer_qty = itr_line.qty
                                            total_qty -= itr_line.transfer_qty
                                        elif itr_line.qty > total_qty:
                                            diff = itr_line.qty - total_qty
                                            itr_line.transfer_qty += itr_line.qty - diff
                                            total_qty -= itr_line.transfer_qty
                                        else:
                                            itr_line.transfer_qty = 0

class InternalTransferLine(models.Model):
    _name = 'internal.transfer.line'
    _description = "Internal Transfer Line"

    qty_cancel = fields.Float(string="Quantity Cancel",tracking=True)
    product_line = fields.Many2one('internal.transfer',string="Internal Transfer")
    product_id = fields.Many2one('product.product',string="Product", required=True,tracking=True)
    qty = fields.Float(string="Quantity", default=1,tracking=True)
    uom = fields.Many2one('uom.uom',string="Unit of Measure", required=True,tracking=True)
    source_location_id = fields.Many2one('stock.location',string="Source Location",tracking=True)
    destination_location_id = fields.Many2one('stock.location',string="Destination Location",tracking=True)
    transfer_qty = fields.Float(string="Transferred Quantity", readonly=True, tracking=True)
    return_qty = fields.Float(string="Return Quantity", readonly=True, tracking=True, compute='compute_return_quantity')
    total_trf = fields.Float(string="Transferred Quantity", readonly=True, tracking=True, compute='calculate_total_trf_qty', store=True)
    scheduled_date = fields.Datetime(string="Scheduled Date",tracking=True)
    filter_source_loc = fields.Many2many('stock.location','prod','fsl_id','prod_fsl_id', compute='_compute_filter_loc', string="Filter Source location", store=False)
    filter_dest_loc = fields.Many2many('stock.location','prod_id','fdl_id','prod_id_fdl_id', compute='_compute_filter_loc',  string="Filter Destination Location", store=False)
    current_qty = fields.Float(string="Current Quantity", readonly=True, store=True, compute='calculate_current_qty',tracking=True)
    filter_available_product_ids = fields.Many2many('product.product', 'product_id', 'avl_product_rel','product_avl_product_id', string="Available Quantity", compute='avl_qty_calculation', store=False)
    analytic_account_group_ids = fields.Many2many('account.analytic.tag', string="Analytic Groups")

    status = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirmed'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')],
        related="product_line.state", readonly='1', string='Request State')
    description = fields.Text('Description', required='1')
    mr_line_id = fields.Many2one('material.request.line')
    source_document = fields.Char('Source Document')
    requested_by = fields.Many2one('res.partner', 'Requested By')
    company_id = fields.Many2one('res.company', 'Company')
    picking_id = fields.One2many('stock.picking', 'internal_transfer_line_id')
    itr_in_progress_qty = fields.Float('In Progress Quantity', compute='_compute_itr_in_progress_qty')
    itr_remaining_qty = fields.Float(string="Remaining Quantity", compute="_compute_remaining_qty_itr")

    @api.onchange('product_id')
    def get_description(self):
        for record in self:
            if record.product_id.description_picking:
                print('dp', record.product_id.description_picking)
                record.description = record.product_id.description_picking
            else:
                record.description = record.product_id.display_name
    @api.model
    def default_get(self, fields):
        res = super(InternalTransferLine, self).default_get(fields)
        if self._context:
            context_keys = self._context.keys()
            next_sequence = 1
            if 'product_line_ids' in context_keys:
                if len(self._context.get('product_line_ids')) > 0:
                    next_sequence = len(self._context.get('product_line_ids')) + 1
            res.update({'sequence': next_sequence})
        return res

    sequence = fields.Char(string="No.")

    def _compute_remaining_qty_itr(self):
        for record in self:
            record.itr_remaining_qty = record.qty - (record.transfer_qty + record.itr_in_progress_qty)

    def _compute_itr_in_progress_qty(self):
        for record in self:
            record.itr_in_progress_qty = 0
            transit_location = self.env.ref('equip3_inventory_masterdata.location_transit')
            stock_picking = record.env['stock.picking'].search([('transfer_id','=',record.product_line.id), ('state', 'not in', ('draft', 'done', 'cancel'))])
            for picking in stock_picking:
                if ((picking.location_id.id == transit_location.id and picking.origin == record.product_line.name and picking.location_dest_id == record.product_line.destination_location_id) or 
                    (picking.location_id.id == record.product_line.source_location_id.id and picking.location_dest_id.id == transit_location.id)):
                    for move in picking.move_ids_without_package:
                        product_lines = record.product_line.product_line_ids.filtered(lambda r:r.product_id.id == move.product_id.id)
                        total_qty = move.reserved_availability
                        line_data = []
                        for itr_line in product_lines:
                            if total_qty != 0:
                                if total_qty <= itr_line.qty:
                                    line_data.append({'line': itr_line, 'qty': total_qty})
                                    total_qty = 0
                                elif total_qty > itr_line.qty:
                                    diff = total_qty - itr_line.qty
                                    qty = total_qty - diff
                                    line_data.append({'line': itr_line, 'qty': qty})
                                    total_qty -= qty
                        if line_data:
                            filter_line = list(filter(lambda r:r.get('line').id == record.id, line_data))
                            if filter_line:
                                record.itr_in_progress_qty = filter_line[0].get('qty')
                if picking.location_id.id == record.product_line.source_location_id.id and picking.origin == record.product_line.name and picking.location_dest_id.id == record.product_line.destination_location_id.id:
                    for move in picking.move_ids_without_package:
                        if move.state == 'done':
                            record.itr_in_progress_qty += abs(record.qty - move.quantity_done)

    def compute_return_quantity(self):
        for record in self:
            stock_picking = record.env['stock.picking'].search([('transfer_id','=',record.product_line.id),('state','=','done')])
            if stock_picking:
                internal_type = record.env['ir.config_parameter'].sudo().get_param('internal_type') or False
                transit_location = record.env.ref('equip3_inventory_masterdata.location_transit')
                if internal_type == 'with_transit':
                    for picking in stock_picking:
                        for product_line in self.product_line:
                            if picking.origin != product_line.name:
                                # if picking.origin != self.product_line.name:
                                if picking.location_id == transit_location and picking.location_dest_id == record.source_location_id:
                                    for line in picking.move_line_ids_without_package:
                                        if line.product_id == record.product_id:
                                            record.return_qty += line.qty_done
                            else:
                                record.return_qty += 0
                else:
                    for picking in stock_picking:
                        if picking.origin and 'Return' in picking.origin:
                            if picking.location_id == record.destination_location_id and picking.location_dest_id == record.source_location_id:
                                for line in picking.move_line_ids_without_package:
                                    if line.product_id == record.product_id:
                                        record.return_qty += line.qty_done
                        else:
                            record.return_qty += 0
            else:
                record.return_qty = 0
            record.return_qty += 0


    @api.depends('source_location_id')
    def avl_qty_calculation(self):
        for record in self:
            product_ids = []
            stock_quant_ids = self.env['stock.quant'].search([('location_id', '=', record.source_location_id.id), ('available_quantity', '>', 0), ('product_id.type', '=', 'product')])
            for quant in stock_quant_ids:
                if quant.available_quantity > 0:
                    product_ids.append(quant.product_id.id)
            record.filter_available_product_ids = [(6, 0, product_ids)]

    def _compute_filter_loc(self):
        sou_loc_id = self.product_line.source_location_id.id
        des_loc_id = self.product_line.destination_location_id.id
        for record in self:
            filter_sou_location_ids = self.env['stock.location'].search(['|', ('id', 'child_of', sou_loc_id), ('id', '=', sou_loc_id)])
            filter_des_location_ids = self.env['stock.location'].search(['|', ('id', 'child_of', des_loc_id), ('id', '=', des_loc_id)])
            record.filter_source_loc = [(6, 0, filter_sou_location_ids.ids)]
            record.filter_dest_loc = [(6, 0, filter_des_location_ids.ids)]

    @api.onchange('product_id')
    def onchange_product_id(self):
        self.uom = False
        if self.product_id:
            self.uom = self.product_id.uom_id.id
        self._compute_filter_loc()

    @api.depends('transfer_qty', 'return_qty', 'qty_cancel')
    def calculate_total_trf_qty(self):
        for record in self:
            record.total_trf = record.transfer_qty - record.return_qty - record.qty_cancel

    @api.depends('product_id', 'source_location_id')
    def calculate_current_qty(self):
        for record in self:
            if record.product_id.type == 'product':
                stock_quant_id = self.env['stock.quant'].search([('location_id', '=', record.source_location_id.id), ('product_id', '=', record.product_id.id)])
                avl_qty = sum(stock_quant_id.mapped('available_quantity'))
                record.current_qty = avl_qty

class ReturnPicking(models.TransientModel):
    _inherit = 'stock.return.picking'

    def _create_returns(self):
        res = super(ReturnPicking, self)._create_returns()
        transfer_id = self.picking_id.transfer_id
        if transfer_id and transfer_id.is_transit and self.picking_id.is_transfer_in:
            for line in self.product_return_moves:
                transfer_lines = transfer_id.product_line_ids.filtered(lambda r: r.product_id.id == line.product_id.id)
                trf_qty = sum(transfer_lines.mapped('transfer_qty'))
                return_qty = trf_qty - line.quantity
                transfer_lines.write({'transfer_qty': return_qty})
        return res   

    @api.onchange('picking_id')
    def _onchange_picking_id(self):
        move_dest_exists = False
        product_return_moves = [(5,)]
        if self.picking_id and self.picking_id.state != 'done':
            raise UserError(_("You may only return Done pickings."))
        # In case we want to set specific default values (e.g. 'to_refund'), we must fetch the
        # default values for creation.
        line_fields = [f for f in self.env['stock.return.picking.line']._fields.keys()]
        product_return_moves_data_tmpl = self.env['stock.return.picking.line'].default_get(line_fields)
        for move in self.picking_id.move_lines:
            if move.state == 'cancel':
                continue
            if move.scrapped:
                continue
            if move.move_dest_ids:
                move_dest_exists = True
            product_return_moves_data = dict(product_return_moves_data_tmpl)
            product_return_moves_data.update(self._prepare_stock_return_picking_line_vals_from_move(move))
            product_return_moves.append((0, 0, product_return_moves_data))
        if self.picking_id and not product_return_moves:
            raise UserError(_("No products to return (only lines in Done state and not fully returned yet can be returned)."))
        if self.picking_id:
            self.product_return_moves = product_return_moves
            self.move_dest_exists = move_dest_exists
            self.parent_location_id = self.picking_id.picking_type_id.warehouse_id and self.picking_id.picking_type_id.warehouse_id.view_location_id.id or self.picking_id.location_id.location_id.id
            self.original_location_id = self.picking_id.location_id.id
            location_id = self.picking_id.location_id.id
            if self.picking_id.picking_type_id.return_picking_type_id.default_location_dest_id.return_location:
                location_id = self.picking_id.picking_type_id.return_picking_type_id.default_location_dest_id.id
            self.location_id = location_id
            if self.picking_id.transfer_id.is_transit and self.picking_id.is_transfer_in:
                location_id = self.picking_id.transfer_id.source_location_id
            self.location_id = location_id 

# class StockBackorderConfirmation(models.TransientModel):
#     _inherit = 'stock.backorder.confirmation'

    # def process(self):
    #     context = dict(self.env.context) or {}
    #     if context.get("active_model") not in ['sale.order', 'purchase.order']:
    #         for record in self.pick_ids:
    #             if record.is_transfer_in and record.transfer_id.is_transit:
    #                 origin = record.transfer_id.name + " Backorder"
    #                 transit_location = self.env.ref('equip3_inventory_masterdata.location_transit')
    #                 data = {
    #                     'location_id' : record.transfer_id.source_location_id.id,
    #                     'location_dest_id' : transit_location.id,
    #                     'move_type': 'direct',
    #                     'partner_id': record.transfer_id.create_uid.partner_id.id,
    #                     'scheduled_date': record.scheduled_date,
    #                     'picking_type_id': record.transfer_id.operation_type_out_id.id,
    #                     'origin': origin,
    #                     'branch_id' : record.branch_id,
    #                     'transfer_id': record.transfer_id.id,
    #                 }
    #                 picking_id = self.env['stock.picking'].create(data)
    #                 for line in record.move_ids_without_package:
    #                     stock_move_obj = self.env['stock.move']
    #                     do_move_data = {
    #                         'move_line_sequence': line.move_line_sequence,
    #                         'picking_id': picking_id.id,
    #                         'name': line.name,
    #                         'product_id': line.product_id.id,
    #                         'product_uom_qty': line.product_uom_qty - line.quantity_done,
    #                         'product_uom': line.product_uom.id,
    #                         'location_id': record.transfer_id.source_location_id.id,
    #                         'location_dest_id': transit_location.id,
    #                         'date': line.date,
    #                     }
    #                     stock_move_obj.create(do_move_data)
    #     return super(StockBackorderConfirmation, self).process()

class StockImmediateTransfer(models.TransientModel):
    _inherit = 'stock.immediate.transfer'

    def process(self):
        res = super(StockImmediateTransfer, self).process()
        context = dict(self.env.context) or {}
        if context.get("active_model") not in ['sale.order', 'purchase.order']:
            picking_ids = self.env['stock.picking'].browse(context.get('active_ids'))
            for picking_id in picking_ids:
                if picking_id.transfer_id and picking_id.state == 'done':
                    picking_id.transfer_id.calculate_transfer_qty(picking_id)
                #     for line in picking_id.move_line_ids_without_package:
                #         transist_line = picking_id.transfer_id.product_line_ids.filtered(lambda r: r.product_id.id == line.product_id.id)
                #         transist_line.write({'transfer_qty': line.qty_done})
                # if picking_id.transfer_id and not picking_id.transfer_id.is_transit and picking_id.backorder_id:
                #     for line in picking_id.move_line_ids_without_package:
                #         transist_line = picking_id.transfer_id.product_line_ids.filtered(lambda r: r.product_id.id == line.product_id.id)
                #         transist_line.write({'transfer_qty': line.qty_done})
                if picking_id.transfer_id and 'Return' in picking_id.origin:
                    for line in picking_id.move_line_ids_without_package:
                        transist_line = picking_id.transfer_id.product_line_ids.filtered(lambda r: r.product_id.id == line.product_id.id)
                        transist_line.write({'return_qty': line.qty_done})
        return res

class InternalApprovalMatrixLine(models.Model):
    _inherit = "itr.approval.matrix.line"

    transfer_id = fields.Many2one('internal.transfer', string="Approval Matrix")
