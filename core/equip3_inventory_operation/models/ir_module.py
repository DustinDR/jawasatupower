from odoo import models


class Module(models.Model):
    _inherit = "ir.module.module"

    def button_immediate_upgrade(self):
        res = super(Module, self).button_immediate_upgrade()

        if self.name in ["equip3_inventory_operation", "dev_rma"]:
            is_return_orders = self.env['ir.config_parameter'].get_param('is_return_orders', False)
            if is_return_orders:
                self.env.ref('dev_rma.manu_dev_rma_rma').active = True
                self.env.ref('equip3_inventory_operation.menu_dev_rma_rma_main_so').active = True
            else:
                self.env.ref('dev_rma.manu_dev_rma_rma').active = False
                self.env.ref('equip3_inventory_operation.menu_dev_rma_rma_main_so').active = False

        return res