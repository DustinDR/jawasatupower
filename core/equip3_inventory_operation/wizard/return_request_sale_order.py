
from odoo import models, fields, api, _


class SaleOrderRepair(models.TransientModel):
    _name = 'sale.order.repair'

    repair_type = fields.Selection([("customer_repair","Customer Repair"),("internal_repair","Internal Repair")], string='Repair Type', default='customer_repair')
    repair_line_ids = fields.One2many('sale.order.repair.line', 'repair_id', string="Repair Line", readonly=True)
    is_return_required = fields.Boolean('Return the repaired parts ')
    return_sale_order_id = fields.Many2one('dev.rma.rma')

    def action_confirm(self):
        for line in self.repair_line_ids:
            # seq = self.env['ir.sequence'].sudo().search([('name', '=', 'Customer Repair')])
            # seq1 = self.env['ir.sequence'].sudo().search([('name', '=', 'Internal Repair')])
            # internal_last_id = self.env['repair.order'].search([('repair_type', '=', 'internal_repair')], order='id desc', limit=1)
            # customer_last_id = self.env['repair.order'].search([('repair_type', '=', 'customer_repair')], order='id desc', limit=1)
            # string1 = internal_last_id.name
            # string2 = customer_last_id.name
            # iro_no = int(string1[-5:])
            # cro_no = int(string2[-5:])
            # context = self.env.context.get('default_repair_type')
            # name = ''
            # if self.repair_type == 'internal_repair':
            #     if cro_no > iro_no:
            #         seq1.write({'number_next_actual': cro_no})
            #     internal_seq = self.env['ir.sequence'].next_by_code('internal.repair')
            #     name = internal_seq
            # if self.repair_type == 'customer_repair':
            #     if iro_no > cro_no:
            #         seq.write({'number_next_actual': iro_no})
            #     customer_seq = self.env['ir.sequence'].next_by_code('customer.repair')
            #     name = customer_seq
            vals = {
                'product_id' : line.product_id.id,
                'product_uom' : line.product_id.uom_id.id,
                'product_qty' : line.quantity,
                'location_id' : line.location_id.id,
                'repair_type' : self.repair_type,
                'source_doc': self.return_sale_order_id.name,
            }
            repair_id = self.env['repair.order'].create(vals)
            if self.is_return_required:
                repair_id.address_id = self.return_sale_order_id.partner_id.id
            for move_line in self.return_sale_order_id.picking_id.move_line_ids_without_package:
                if move_line.product_id.id == line.product_id.id:
                    if move_line.lot_id:
                        repair_id.lot_id = move_line.lot_id.id
            print('lot', repair_id.lot_id)

class SaleOrderRepairLine(models.TransientModel):
    _name = 'sale.order.repair.line'

    repair_id = fields.Many2one('sale.order.repair', string="Repair")
    product_id = fields.Many2one('product.product', string="Product")
    quantity = fields.Float(string="Quantity")
    location_id = fields.Many2one('stock.location', string="Location")