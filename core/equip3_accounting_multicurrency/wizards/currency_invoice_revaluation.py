from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools.misc import format_date
import datetime


class AccountMove(models.Model):
    _inherit = 'account.move'

    currency_revaluation_id = fields.Many2one('account.move', string='Currency Revaluation', ondelete='cascade')
    children_ids = fields.One2many('account.move', 'currency_revaluation_id', string='Children')
    currency_revaluation_count = fields.Integer(string="currency revaluation count", default ='0') 


class CurrencyTaxRate(models.Model):
    _name = "currency.invoice.revaluation"
    _description = "Currency Invoice Revaluation"
    _order = "revaluation_date desc"

    revaluation_period_start = fields.Date(default=str(datetime.date.today() - datetime.timedelta(days=30)), required=True)
    revaluation_period_end = fields.Date(default=fields.Date.context_today, required=True)
    revaluation_date = fields.Date(default=fields.Date.context_today, required=True)    
    journal_id = fields.Many2one(
		string="Unrealized Exchange Journal",
        default=lambda self: self.env.company.unrealized_exchange_journal_id,
        comodel_name='account.journal', 
        required=True)
    income_unrealized_account = fields.Many2one(
        comodel_name="account.account",
        default=lambda self: self.env.company.income_unrealized_exchange_account_id,
        string="Unrealized Gain Account", 
        required=True)        
    expense_unrealized_account = fields.Many2one(
		comodel_name="account.account",
        default=lambda self: self.env.company.expense_unrealized_exchange_account_id,
        string="Unrealized Loss Account", 
        required=True)
 
    def validate(self):        
        if self._context.get('active_ids'):
            active_ids = self._context.get('active_ids')
            move_vals = self.env['account.move'].search([('id', 'in', active_ids)])
        else:           
            move_vals = self.env['account.move'].search(['&',('invoice_date', '>=', format_date(self.env, self.revaluation_period_start)),('invoice_date', '<=', format_date(self.env, self.revaluation_period_end))])
        moves_ids = move_vals.filtered(lambda line: line.journal_id.type in ['purchase', 'sale'] and line.state == 'posted' and line.payment_state in ['not_paid','partial'])
        AccountMove = self.env['account.move']
        move_id=[]
        for move in moves_ids:
            curr = self.env['res.currency.rate'].search(['&',('name', '<=', format_date(self.env, self.revaluation_date)),('currency_id', '=', move.currency_id.id)],order='name desc')
            if curr:
                rate = curr[0].mr_rate
                if (rate - move.current_rate) != 0:
                    move_line_id = AccountMove.create(self._prepare_move_line(move, rate, move.journal_id.type))
                    move.update({'current_rate' : curr[0].mr_rate, 'currency_revaluation_id' : move_line_id, 'currency_revaluation_count' : '1'})                    
                    move_line_id.post()                    
                    move_id.append(move_line_id.id)
                    
                    domain = [('account_internal_type', 'in', ('receivable', 'payable')), ('reconciled', '=', False)]                    
                    inv_to_reconcile = move.line_ids.filtered_domain(domain)
                    move_to_reconcile = move_line_id.line_ids.filtered_domain(domain)                    
                    for account in move_to_reconcile.account_id:
                        print(move_to_reconcile)
                        print(self.env['account.move.line'].search_read([('id','=',move_to_reconcile.id)]))
                        print(inv_to_reconcile)
                        print(self.env['account.move.line'].search_read([('id','=',inv_to_reconcile.id)]))                        
                        print(account)
                        print((move_to_reconcile + inv_to_reconcile)\
                            .filtered_domain([('account_id', '=', account.id), ('reconciled', '=', False)]))
                        print((move_to_reconcile + inv_to_reconcile)\
                            .filtered_domain([('account_id', '=', account.id), ('reconciled', '=', False)])\
                            .reconcile())
        # raise UserError(_("Error disini"))
        return {
                'name':  _('Currency Revaluation'),
                'type': 'ir.actions.act_window',
                'view_mode': 'tree,kanban,form',
                'res_model': 'account.move',
                'views_id': self.env.ref('account.view_move_tree').id,
                'domain': [('id', 'in', move_id)],
                'context' : {'default_move_type': 'entry'}
            }


    def _prepare_move_line(self, move_id, rate,move_type):
        all_move_vals = []        
        if move_id.journal_id.type == 'purchase':
            balance_current_rate = move_id.amount_residual / move_id.current_rate
            balance_rate = move_id.amount_residual / rate
            line_vals_list = []
            line_vals_list.append((0,0,
                                        {                                        
                                            'partner_id': move_id.partner_id.id,
                                            'name': move_id.name,
                                            'date_maturity': self.revaluation_date.strftime("%Y-%m-%d"),
                                            'debit': abs(balance_current_rate-balance_rate) if move_id.current_rate < rate else 0.0,
                                            'credit': abs(balance_rate-balance_current_rate) if move_id.current_rate > rate else 0.0,
                                            'account_id': move_id.partner_id.property_account_payable_id.id,

                                        }))
            line_vals_list.append((0,0,
                                       {
                                            'partner_id': move_id.partner_id.id,
                                            'name': move_id.name,
                                            'date_maturity': self.revaluation_date.strftime("%Y-%m-%d"),
                                            'debit': 0.0 if move_id.current_rate < rate else abs(balance_rate-balance_current_rate),
                                            'credit': 0.0 if move_id.current_rate > rate else abs(balance_current_rate-balance_rate),
                                            'account_id': self.income_unrealized_account.id if move_id.current_rate < rate else self.expense_unrealized_account.id,
                                        }))
            move_vals = {
                        'partner_id': move_id.partner_id.id,
                        'ref': move_id.name,
                        'date': self.revaluation_date.strftime("%Y-%m-%d"),
                        'journal_id': self.journal_id.id,
                        'line_ids': line_vals_list,
                        # 'line_ids': json.dumps(line_vals_list),
                        }
            all_move_vals.append(move_vals)

        elif move_id.journal_id.type == 'sale':
            balance_current_rate = move_id.amount_residual / move_id.current_rate
            balance_rate = move_id.amount_residual / rate
            line_vals_list = []
            line_vals_list.append((0,0,
                                        {
                                            'partner_id': move_id.partner_id.id,
                                            'name': move_id.name,
                                            'date_maturity': self.revaluation_date.strftime("%Y-%m-%d"),
                                            'debit': abs(balance_current_rate-balance_rate) if move_id.current_rate > rate else 0.0,
                                            'credit': abs(balance_rate-balance_current_rate) if move_id.current_rate < rate else 0.0,
                                            'account_id': move_id.partner_id.property_account_receivable_id.id,
                                            
                                        }))
            line_vals_list.append((0,0,
                                       {
                                            'partner_id': move_id.partner_id.id,
                                            'name': move_id.name,
                                            'date_maturity': self.revaluation_date.strftime("%Y-%m-%d"),
                                            'debit': 0.0 if move_id.current_rate > rate else abs(balance_rate-balance_current_rate),
                                            'credit': 0.0 if move_id.current_rate < rate else abs(balance_current_rate-balance_rate),
                                            'account_id': self.income_unrealized_account.id if move_id.current_rate > rate else self.expense_unrealized_account.id,
                                        }))
            move_vals = {
                        'partner_id': move_id.partner_id.id,
                        'ref': move_id.name,
                        'date': self.revaluation_date.strftime("%Y-%m-%d"),
                        'journal_id': self.journal_id.id,
                        'line_ids': line_vals_list,
                        # 'line_ids': json.dumps(line_vals_list),
                        }
            all_move_vals.append(move_vals)
        return all_move_vals

class AccountPaymentRegister(models.TransientModel):
    _inherit = 'account.payment.register'

    def _create_payments(self):
        self.ensure_one()
        batches = self._get_batches()
        edit_mode = self.can_edit_wizard and (len(batches[0]['lines']) == 1 or self.group_payment)

        to_reconcile = []
        if edit_mode:
            payment_vals = self._create_payment_vals_from_wizard()
            payment_vals_list = [payment_vals]
            to_reconcile.append(batches[0]['lines'])
        else:
            # Don't group payments: Create one batch per move.
            if not self.group_payment:
                new_batches = []
                for batch_result in batches:
                    for line in batch_result['lines']:
                        new_batches.append({
                            **batch_result,
                            'lines': line,
                        })
                batches = new_batches

            payment_vals_list = []
            for batch_result in batches:
                payment_vals_list.append(self._create_payment_vals_from_batch(batch_result))
                to_reconcile.append(batch_result['lines'])

        payments = self.env['account.payment'].create(payment_vals_list)

        # If payments are made using a currency different than the source one, ensure the balance match exactly in
        # order to fully paid the source journal items.
        # For example, suppose a new currency B having a rate 100:1 regarding the company currency A.
        # If you try to pay 12.15A using 0.12B, the computed balance will be 12.00A for the payment instead of 12.15A.
        if edit_mode:
            for payment, lines in zip(payments, to_reconcile):
                # Batches are made using the same currency so making 'lines.currency_id' is ok.
                if payment.currency_id != lines.currency_id:
                    liquidity_lines, counterpart_lines, writeoff_lines = payment._seek_for_lines()
                    source_balance = abs(sum(lines.mapped('amount_residual')))
                    payment_rate = liquidity_lines[0].amount_currency / liquidity_lines[0].balance
                    source_balance_converted = abs(source_balance) * payment_rate

                    # Translate the balance into the payment currency is order to be able to compare them.
                    # In case in both have the same value (12.15 * 0.01 ~= 0.12 in our example), it means the user
                    # attempt to fully paid the source lines and then, we need to manually fix them to get a perfect
                    # match.
                    payment_balance = abs(sum(counterpart_lines.mapped('balance')))
                    payment_amount_currency = abs(sum(counterpart_lines.mapped('amount_currency')))
                    if not payment.currency_id.is_zero(source_balance_converted - payment_amount_currency):
                        continue

                    delta_balance = source_balance - payment_balance

                    # Balance are already the same.
                    if self.company_currency_id.is_zero(delta_balance):
                        continue

                    # Fix the balance but make sure to peek the liquidity and counterpart lines first.
                    debit_lines = (liquidity_lines + counterpart_lines).filtered('debit')
                    credit_lines = (liquidity_lines + counterpart_lines).filtered('credit')

                    payment.move_id.write({'line_ids': [
                        (1, debit_lines[0].id, {'debit': debit_lines[0].debit + delta_balance}),
                        (1, credit_lines[0].id, {'credit': credit_lines[0].credit + delta_balance}),
                    ]})

        payments.action_post()

        domain = [('account_internal_type', 'in', ('receivable', 'payable')), ('reconciled', '=', False)]
        for payment, lines in zip(payments, to_reconcile):

            # When using the payment tokens, the payment could not be posted at this point (e.g. the transaction failed)
            # and then, we can't perform the reconciliation.
            if payment.state != 'posted':
                continue

            payment_lines = payment.line_ids.filtered_domain(domain)
            for account in payment_lines.account_id:
                print(account)
                print(payment_lines)
                print(self.env['account.move.line'].search_read([('id','=',payment_lines.id)]))
                print(lines)
                print(self.env['account.move.line'].search_read([('id','=',lines.id)]))
                print((payment_lines + lines)\
                    .filtered_domain([('account_id', '=', account.id), ('reconciled', '=', False)]))
                print((payment_lines + lines)\
                    .filtered_domain([('account_id', '=', account.id), ('reconciled', '=', False)])\
                    .reconcile())

        # raise UserError(_("Error disini"))
        return payments