from odoo import models, fields, api, _, tools

class AccountVoucher(models.Model):
    _inherit = "account.voucher"

    apply_manual_currency_exchange = fields.Boolean(string='Apply Manual Currency Exchange')
    manual_currency_exchange_inverse_rate = fields.Float(string="Inverse Rate")
    manual_currency_exchange_rate = fields.Float(string="Manual Currency Exchange Rate", digits=(12, 12), default=0.0)
    invisible_manual_currency = fields.Boolean(string="invisible manual currency", default=False)
    invisible_conversion = fields.Boolean(string="invisible conversion", default=False)

    @api.onchange('currency_id')
    def _oncange_currency_id(self):
        if self.currency_id == self.env.company.currency_id:
            self.invisible_manual_currency = True
            self.apply_manual_currency_exchange = False
        else:
            self.invisible_manual_currency = False
            self.apply_manual_currency_exchange = False


    @api.onchange('invisible_manual_currency','currency_id','apply_manual_currency_exchange')
    def _oncange_invisible_conversion(self):
        if self.invisible_manual_currency == False and self.env.company.is_inverse_rate == True:
            if self.apply_manual_currency_exchange:
                self.invisible_conversion = False
            else:
                self.invisible_conversion = True
        else:
            self.invisible_conversion = True

    @api.onchange('manual_currency_exchange_inverse_rate')
    def _oncange_rate_conversion(self):
        if self.manual_currency_exchange_inverse_rate:
            self.manual_currency_exchange_rate = 1 / self.manual_currency_exchange_inverse_rate

    @api.onchange('manual_currency_exchange_rate')
    def _oncange_rate(self):
        if self.manual_currency_exchange_rate:
            self.manual_currency_exchange_inverse_rate = 1 / self.manual_currency_exchange_rate

    def round(self, amount):
        self.ensure_one()
        return tools.float_round(amount, precision_rounding=self.currency_id.rounding)

    def _convert(self, amount):
        for voucher in self:
            if self.apply_manual_currency_exchange == False:
                res = voucher.currency_id._convert(amount, voucher.company_id.currency_id, voucher.company_id, voucher.account_date)
            else:
                res = amount / self.manual_currency_exchange_rate
            return self.round(res)

    def action_move_line_create(self):
        for voucher in self:
            local_context = dict(self._context)
            if voucher.move_id:
                continue
            company_currency = voucher.journal_id.company_id.currency_id.id
            current_currency = voucher.currency_id.id or company_currency
            ctx = local_context.copy()
            ctx['date'] = voucher.account_date
            ctx['check_move_validity'] = False
            # Create the account move record.
            move = self.env['account.move'].create(voucher.account_move_get())
            #print ('===s===',move.name)
            # Get the name of the account_move just created
            # Create the first line of the voucher
            move_line = self.env['account.move.line'].with_context(ctx).create(voucher.with_context(ctx).first_move_line_get(move.id, company_currency, current_currency))
            line_total = move_line.debit - move_line.credit
            first_move_id=move_line.id
            if voucher.voucher_type == 'sale':
                line_total = line_total - voucher._convert(voucher.tax_amount)
            elif voucher.voucher_type == 'purchase':
                line_total = line_total + voucher._convert(voucher.tax_amount)
            #print ('===ctx===',ctx)
            # Create one move line per voucher line where amount is not 0.0            
            line_total = voucher.with_context(ctx).voucher_move_line_create(line_total, move.id, company_currency, current_currency)            
            # Add tax correction to move line if any tax correction specified
            if voucher.tax_correction != 0.0:
                tax_move_line = self.env['account.move.line'].search([('move_id', '=', move.id), ('tax_line_id', '!=', False)], limit=1)
                if len(tax_move_line):
                    tax_move_line.write({'debit': tax_move_line.debit + voucher.tax_correction if tax_move_line.debit > 0 else 0,
                        'credit': tax_move_line.credit + voucher.tax_correction if tax_move_line.credit > 0 else 0})            
            total_line_debit = sum(move.line_ids.mapped('debit'))
            total_line_credit = sum(move.line_ids.mapped('credit'))
            for x in move.line_ids:
                if x.id == first_move_id:
                    if x.debit > 0:
                        x.debit = self.round(total_line_credit)
                    if x.credit > 0:
                        x.credit = self.round(total_line_debit)
            move._post()
            voucher.write({
                'name': move.name,
                'move_id': move.id,
                'state': 'posted',
            })
        return True