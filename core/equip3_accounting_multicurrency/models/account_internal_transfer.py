from odoo import models, fields, api, _
from datetime import date
from lxml import etree
from odoo.exceptions import UserError, ValidationError

class AccountInternalTransfer(models.Model):
    _inherit = "account.internal.transfer"

    # administration = fields.Boolean(string='Administration', default=False, tracking=True)
    # # show when administration = true – mandatory
    # administration_account = fields.Many2one('account.account', string="Administration Account", tracking=True)
    # administration_fee = fields.Monetary(string="Amount", currency_field='currency_id', tracking=True)
    # state = fields.Selection(selection=[
    #         ('draft', 'Draft'),
    #         ('in_progress', 'In Progress'),
    #         ('done', 'Completed'),
    #     ], string='State', default='draft', tracking=True)
    
    apply_manual_currency_exchange = fields.Boolean(string="Apply Manual Currency Exchange")
    manual_currency_exchange_rate = fields.Float(string="Manual Currency Exchange Rate", digits=(12,12))
    manual_currency_exchange_inverse_rate = fields.Float(string="Inverse Rate")

    type_curr = fields.Selection(selection=[
            ('bank_cash', 'Internal Transfer'),
            ('purchase_currency', 'Purchase Currency'),
        ], string='Type', default="bank_cash")

    # @api.depends('manual_currency_exchange_inverse_rate')
    # def _calculate_exchange_rate(self):
    #     for rec in self:
    #         if rec.manual_currency_exchange_inverse_rate > 0:
    #             rec.manual_currency_exchange_rate = 1/rec.manual_currency_exchange_inverse_rate

    @api.onchange('manual_currency_exchange_inverse_rate')
    def _oncange_rate_conversion(self):
        if self.manual_currency_exchange_inverse_rate:
            self.manual_currency_exchange_rate = 1 / self.manual_currency_exchange_inverse_rate

    @api.onchange('manual_currency_exchange_rate')
    def _oncange_rate(self):
        if self.manual_currency_exchange_rate:
            self.manual_currency_exchange_inverse_rate = 1 / self.manual_currency_exchange_rate

    @api.depends('transfer_amount', 'company_id', 'branch_id')
    def _get_approval_matrix(self):
        for record in self:
            matrix_id = False
            if record.type_curr == "bank_cash":
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.transfer_amount),
                        ('max_amount', '>=', record.transfer_amount),
                        ('approval_matrix_type', '=', 'inter_bank_cash_approval_matrix')
                    ], limit=1)
            elif record.type_curr == "purchase_currency":
                matrix_id = self.env['approval.matrix.accounting'].search([
                        ('company_id', '=', record.company_id.id),
                        ('branch_id', '=', record.branch_id.id),
                        ('min_amount', '<=', record.transfer_amount),
                        ('max_amount', '>=', record.transfer_amount),
                        ('approval_matrix_type', '=', 'purchase_currency_approval_matrix')
                    ], limit=1)
            record.approval_matrix_id = matrix_id
            record._compute_approving_matrix_lines()

    def _get_approve_button_from_config(self):
        for record in self:
            is_internal_approval_matrix = False
            if record.type_curr == 'bank_cash':
                is_internal_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_internal_transfer_approval_matrix', False)
            elif record.type_curr == 'purchase_currency':
                is_internal_approval_matrix = self.env['ir.config_parameter'].sudo().get_param('is_purchase_currency_approval_matrix', False)
            record.is_internal_approval_matrix = is_internal_approval_matrix
    
    @api.model
    def create(self, vals):
        if 'company_id' in vals:
            self = self.with_company(vals['company_id'])
        if vals.get('name', _('New')) == _('New'):
            seq_date = None
            if 'transfer_date' in vals:
                seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['transfer_date']))
            
            if self._context.get('default_type_curr') == 'bank_cash':
                vals['name'] = self.env['ir.sequence'].next_by_code('account.internal.transfer', sequence_date=seq_date) or _('New')
            elif self._context.get('default_type_curr') == 'purchase_currency':
                vals['name'] = self.env['ir.sequence'].next_by_code('purchase.currency', sequence_date=seq_date) or _('New')
        result = super(AccountInternalTransfer, self).create(vals)
        return result

    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(AccountInternalTransfer, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if view_type == 'form':
            doc = etree.XML(result['arch'])
            type_curr = self._context.get('default_type_curr')
            if type_curr == "purchase_currency":
                bank_from_journal_id = doc.xpath("//field[@name='bank_from_journal_id']")
                bank_from_journal_id[0].set("string", "Sold Currency")
                bank_to_journal_id = doc.xpath("//field[@name='bank_to_journal_id']")
                bank_to_journal_id[0].set("string", "Purchased Currency")
                result['arch'] = etree.tostring(doc, encoding='unicode')
        return result

    # @api.onchange('bank_to_journal_id')
    # def currency(self):
    #     for rec in self:
    #         if rec.bank_to_journal_id:
    #             rec.currency_id = rec.bank_to_journal_id.currency_id


    def warning_message(self,value):
        if value <= 0:
            raise UserError(_("Purchase amount should bigger than 0!"))

    def action_validate(self):
        for record in self:
            self.warning_message(abs(record.transfer_amount))
            ref = ''
            name = ''
            if record.type_curr == 'bank_cash':
                ref = 'Internal Transfer' + ' ' + (record.transfer_desc or '')
                name = 'Internal Transfer' +' ' + (record.name or '')
            elif record.type_curr == 'purchase_currency':
                ref = 'Purchase Currency' + ' ' + (record.transfer_desc or '')
                name = 'Purchase Currency' +' ' + (record.name or '')

            counterpart_transfer_amount = abs(record.transfer_amount)
            counterpart_administration_fee = abs(record.administration_fee)
            counterpart_amount = counterpart_transfer_amount + counterpart_administration_fee
            company_currency = record.company_id.currency_id

            # Manage currency.
            if record.currency_id == company_currency:
                # Single-currency.
                balance_transfer_amount = counterpart_transfer_amount
                balance_administration_fee = counterpart_administration_fee
                balance = counterpart_amount
                counterpart_transfer_amount = 0.0
                counterpart_administration_fee = 0.0
                counterpart_amount = 0.0
                currency_id = False
            else:
                # Multi-currencies.
                if record.apply_manual_currency_exchange == True:
                    balance_transfer_amount = counterpart_transfer_amount/record.manual_currency_exchange_rate
                    balance_administration_fee = counterpart_administration_fee/record.manual_currency_exchange_rate
                    balance = balance_transfer_amount + balance_administration_fee
                else:
                    balance_transfer_amount = record.currency_id._convert(counterpart_transfer_amount, company_currency, record.company_id, record.transfer_date)
                    balance_administration_fee = record.currency_id._convert(counterpart_administration_fee, company_currency, record.company_id, record.transfer_date)
                    balance = record.currency_id._convert(counterpart_amount, company_currency, record.company_id, record.transfer_date)

                currency_id = record.currency_id.id
            
            if record.transfer_in_transit == True and record.administration == True:                
                credit_vals = {
                        'name': name,
                        'amount_currency': -counterpart_amount,
                        'currency_id': currency_id,
                        'debit': 0.0,
                        'credit': abs(balance),
                        'date_maturity': record.transfer_date,
                        'account_id': record.bank_from_journal_id.payment_credit_account_id.id,
                    }

                debit_vals1 = {
                        'name': name,
                        'amount_currency': counterpart_transfer_amount,
                        'currency_id': currency_id,
                        'debit': abs(balance_transfer_amount),
                        'credit': 0.0,
                        'date_maturity': record.transfer_date,
                        'account_id': record.account_in_transit.id,
                    }

                debit_vals2 = {
                        'name': name,
                        'amount_currency': counterpart_administration_fee,
                        'currency_id': currency_id,
                        'debit': abs(balance_administration_fee),
                        'credit': 0.0,
                        'date_maturity': record.transfer_date,
                        'account_id': record.administration_account.id,
                    }
                vals = {
                    'ref': ref,
                    'date': record.transfer_date,
                    'journal_id': record.bank_from_journal_id.id,
                    'line_ids': [(0, 0, credit_vals),(0, 0, debit_vals1),(0, 0, debit_vals2)]
                }
                
                move_id = self.env['account.move'].create(vals)
                move_id.post()
                record.write({'state': 'in_progress'})

            elif record.transfer_in_transit == True and record.administration == False:
                credit_vals = {
                        'name': name,
                        'amount_currency': -counterpart_transfer_amount,
                        'currency_id': currency_id,
                        'debit': 0.0,
                        'credit': abs(balance_transfer_amount),
                        'date_maturity': record.transfer_date,
                        'account_id': record.bank_from_journal_id.payment_credit_account_id.id,
                    }

                debit_vals1 = {
                        'name': name,
                        'amount_currency': counterpart_transfer_amount,
                        'currency_id': currency_id,
                        'debit': abs(balance_transfer_amount),
                        'credit': 0.0,
                        'date_maturity': record.transfer_date,
                        'account_id': record.account_in_transit.id,
                    }
                vals = {
                    'ref': ref,
                    'date': record.transfer_date,
                    'journal_id': record.bank_from_journal_id.id,
                    'line_ids': [(0, 0, credit_vals),(0, 0, debit_vals1)]
                }

                move_id = self.env['account.move'].create(vals)
                move_id.post()
                record.write({'state': 'in_progress'})

            elif record.transfer_in_transit == False and record.administration == True:
                credit_vals = {
                        'name': name,
                        'amount_currency': -counterpart_amount,
                        'currency_id': currency_id,
                        'debit': 0.0,
                        'credit': abs(balance),
                        'date_maturity': record.transfer_date,
                        'account_id': record.bank_from_journal_id.payment_credit_account_id.id,
                    }

                debit_vals1 = {
                        'name': name,
                        'amount_currency': counterpart_transfer_amount,
                        'currency_id': currency_id,
                        'debit': abs(balance_transfer_amount),
                        'credit': 0.0,
                        'date_maturity': record.transfer_date,
                        'account_id': record.bank_to_journal_id.payment_debit_account_id.id,
                    }

                debit_vals2 = {
                        'name': name,
                        'amount_currency': counterpart_administration_fee,
                        'currency_id': currency_id,
                        'debit': abs(balance_administration_fee),
                        'credit': 0.0,
                        'date_maturity': record.transfer_date,
                        'account_id': record.administration_account.id,
                    }
                vals = {
                    'ref': ref,
                    'date': record.transfer_date,
                    'journal_id': record.bank_from_journal_id.id,
                    'line_ids': [(0, 0, credit_vals),(0, 0, debit_vals1),(0, 0, debit_vals2)]
                }

                move_id = self.env['account.move'].create(vals)
                move_id.post()
                record.write({'state': 'done'})

            else:
                credit_vals = {
                        'name': name,
                        'amount_currency': -counterpart_transfer_amount,
                        'currency_id': currency_id,
                        'debit': 0.0,
                        'credit': abs(balance_transfer_amount),
                        'date_maturity': record.transfer_date,
                        'account_id': record.bank_from_journal_id.payment_credit_account_id.id,
                    }
                debit_vals = {
                        'name': name,
                        'amount_currency': counterpart_transfer_amount,
                        'currency_id': currency_id,
                        'debit': abs(balance_transfer_amount),
                        'credit': 0.0,
                        'date_maturity': record.transfer_date,
                        'account_id': record.bank_to_journal_id.payment_debit_account_id.id,
                    }
                vals = {
                    'ref': ref,
                    'date': record.transfer_date,
                    'journal_id': record.bank_from_journal_id.id,
                    'line_ids': [(0, 0, debit_vals), (0, 0, credit_vals)]
                }

                move_id = self.env['account.move'].create(vals)
                move_id.post()
                record.write({'state': 'done'})

    def action_complete(self):
        for record in self:
            if record.type_curr == 'bank_cash':
                ref = 'Internal Transfer' + ' ' + (record.transfer_desc or '')
                name = 'Internal Transfer' +' ' + (record.name or '')
            elif record.type_curr == 'purchase_currency':
                ref = 'Purchase Currency' + ' ' + (record.transfer_desc or '')
                name = 'Purchase Currency' +' ' + (record.name or '')

            # Manage currency.
            counterpart_transfer_amount = abs(record.transfer_amount)
            company_currency = record.company_id.currency_id            
            
            if record.currency_id == company_currency:
                # Single-currency.
                balance_transfer_amount = counterpart_transfer_amount
                counterpart_transfer_amount = 0.0
                currency_id = False
            else:
                # Multi-currencies.
                if record.apply_manual_currency_exchange == True:
                    balance_transfer_amount = counterpart_transfer_amount/record.manual_currency_exchange_rate
                else:
                    balance_transfer_amount = record.currency_id._convert(counterpart_transfer_amount, company_currency, record.company_id, record.transfer_date)
                currency_id = record.currency_id.id

            credit_vals = {
                    'name': name,
                    'amount_currency': -counterpart_transfer_amount,
                    'currency_id': currency_id,
                    'debit': 0.0,
                    'credit': abs(balance_transfer_amount),
                    'date_maturity': date.today(),
                    'account_id': record.account_in_transit.id
                }

            debit_vals = {
                    'name': name,
                    'amount_currency': counterpart_transfer_amount,
                    'currency_id': currency_id,
                    'debit': abs(balance_transfer_amount),
                    'credit': 0.0,
                    'date_maturity': date.today(),
                    'account_id': record.bank_to_journal_id.payment_debit_account_id.id,
                }
            vals = {
                'ref': ref,
                'date': date.today(),
                'journal_id': record.bank_to_journal_id.id,
                'line_ids': [(0, 0, credit_vals),(0, 0, debit_vals)]
            }

            move_id = self.env['account.move'].create(vals)
            move_id.post()
            record.write({'state': 'done', 'has_reconciled_entries' : True})

            domain = [('account_id', '=', self.account_in_transit.id), ('reconciled', '=', False), ('name', 'like', '%' + name)]                    
            bank_to_reconcile = move_id.line_ids.filtered_domain(domain)
            move_to_reconcile = self.env['account.move.line'].search(domain)
            for account in move_to_reconcile.account_id:
                (move_to_reconcile + bank_to_reconcile)\
                    .filtered_domain([('account_id', '=', self.account_in_transit.id), ('reconciled', '=', False), ('name', 'like', '%' + name)])\
                    .reconcile()

    def _reconciled_lines(self):
        ids = []        
        if self.type_curr == 'bank_cash':
            name = 'Internal Transfer' +' ' + self.name
        elif self.type_curr == 'purchase_currency':
            name = 'Purchase Currency' +' ' + self.name

        domain = [('account_id', '=', self.account_in_transit.id), ('name', 'like', '%' + name), ('reconciled', '=', True)]
        move_to_reconcile = self.env['account.move.line'].search(domain)
        for aml in move_to_reconcile:
            ids.extend([r.debit_move_id.id for r in aml.matched_debit_ids] if aml.credit > 0 else [r.credit_move_id.id for r in aml.matched_credit_ids])
            ids.append(aml.id)
        return ids