import logging
import math
import re
import time
import traceback
from lxml import etree
from odoo.addons.base.models.ir_ui_view import (
transfer_field_to_modifiers, transfer_node_to_modifiers, transfer_modifiers_to_node,
)
from odoo import api, fields, models, tools, _

_logger = logging.getLogger(__name__)

def setup_modifiers(node, field=None, context=None, in_tree_view=False):
    modifiers = {}
    if field is not None:
        transfer_field_to_modifiers(field, modifiers)
    transfer_node_to_modifiers(
        node, modifiers, context=context)
    transfer_modifiers_to_node(modifiers, node)


class CurrencyTaxRate(models.Model):
    _name = "res.currency.tax"
    _description = "Currency Tax Rate"
    _order = "name desc"

    @api.depends('conversion', 'mr_rate')
    def _get_rate(self):
        for record in self:
            if record.conversion and record.company_id.is_inverse_rate:
                val_rate = float(1) / record.conversion
                record.rate = val_rate
                record.mr_rate = val_rate
            elif record.mr_rate and not record.company_id.is_inverse_rate:
                record.rate = record.mr_rate
                record.conversion = float(1) / record.mr_rate
            else:
                record.rate = 1.0

    name = fields.Date(
        string='Date', required=True, index=True,
        default=lambda self: fields.Date.today())
    rate = fields.Float(
        digits=(12, 12), default=1.0,
        help='The rate of the currency to the currency of rate 1',
        compute='_get_rate', store=True)
    conversion = fields.Float('Inverse Rate')
    mr_rate = fields.Float(
        digits=(12, 12), default=1.0,
        help='The rate of the currency to the currency of rate 1')
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True, required=True, ondelete="cascade")
    company_id = fields.Many2one('res.company', string='Company',
                                 default=lambda self: self.env.company)

    _sql_constraints = [
        ('unique_name_per_day', 'unique (name,currency_id,company_id)', 'Only one currency rate per day allowed!'),
        ('currency_rate_check', 'CHECK (rate>0)', 'The currency rate must be strictly positive.'),
    ]

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        result = super(CurrencyTaxRate, self).fields_view_get(
            view_id, view_type, toolbar=toolbar, submenu=submenu)
        company_id = self.env.context.get("company_id") or self.env.company.id
        company_obj = self.env['res.company'].browse(company_id)
        doc = etree.XML(result['arch'])
        if company_obj.is_taxes_rate and company_obj.is_inverse_rate:
            for node in doc.xpath("//field[@name='mr_rate']"):
                doc.remove(node)
        else:
            for node in doc.xpath("//field[@name='conversion']"):
                doc.remove(node)
            if view_type == 'tree':
                for node in doc.xpath("//field[@name='rate']"):
                    doc.remove(node)
        result['arch'] = etree.tostring(doc, encoding='unicode')
        return result

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        if operator in ['=', '!=']:
            try:
                date_format = '%Y-%m-%d'
                if self._context.get('lang'):
                    lang_id = self.env['res.lang']._search([('code', '=', self._context['lang'])], access_rights_uid=name_get_uid)
                    if lang_id:
                        date_format = self.browse(lang_id).date_format
                name = time.strftime('%Y-%m-%d', time.strptime(name, date_format))
            except ValueError:
                try:
                    args.append(('rate', operator, float(name)))
                except ValueError:
                    return []
                name = ''
                operator = 'ilike'
        return super(CurrencyTaxRate, self)._name_search(name, args=args, operator=operator, limit=limit, name_get_uid=name_get_uid)


class Currency(models.Model):
    _inherit = "res.currency"

    tax_rate_ids = fields.One2many(
        'res.currency.tax', 'currency_id',
        tracking=True, string='Rates')
    tax_rate = fields.Float(
        compute='_compute_current_tax_rate',
        tracking=True, string='Current Taxes Rate', digits=(12, 12),
        help='The rate of the currency to the currency of rate 1.')
    tax_conversion = fields.Float(
        compute='_compute_current_tax_conversion',
        tracking=True, string='Inverse Taxes Rate', digits=0)
    is_taxes_rate = fields.Boolean(
        compute='_compute_get_taxes_rate')

    def _compute_get_taxes_rate(self):
        company_id = self.env.context.get("company_id") or self.env.company.id
        company_obj = self.env['res.company'].browse(company_id)
        for data in self:
            data.is_taxes_rate = company_obj.is_taxes_rate

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(Currency, self).fields_view_get(
            view_id, view_type, toolbar=toolbar, submenu=submenu)
        company_id = self.env.context.get("company_id") or self.env.company.id
        company_obj = self.env['res.company'].browse(company_id)
        doc = etree.XML(result['arch'])
        if not company_obj.is_taxes_rate:
            if view_type == 'form':
                node = doc.xpath("//field[@name='tax_conversion']")[0]
                node.set('invisible', '1')
                setup_modifiers(node, result['fields']['tax_conversion'])

                node = doc.xpath("//field[@name='tax_rate']")[0]
                node.set('invisible', '1')
                setup_modifiers(node, result['fields']['tax_rate'])
        if not company_obj.is_inverse_rate:
            if view_type == 'form':
                node = doc.xpath("//field[@name='tax_conversion']")[0]
                node.set('invisible', '1')
                setup_modifiers(node, result['fields']['tax_conversion'])

        result['arch'] = etree.tostring(doc, encoding='unicode')
        return result

    def _get_tax_rate(self, company, date):
        self.env['res.currency.tax'].flush(['rate', 'currency_id', 'company_id', 'name'])
        query = """SELECT c.id,
                        COALESCE((SELECT r.rate FROM res_currency_tax r
                                  WHERE r.currency_id = c.id AND r.name <= %s
                                    AND (r.company_id IS NULL OR r.company_id = %s)
                               ORDER BY r.company_id, r.name DESC
                                  LIMIT 1), 1.0) AS rate
                   FROM res_currency c
                   WHERE c.id IN %s"""
        self._cr.execute(query, (date, company.id, tuple(self.ids)))
        currency_rates = dict(self._cr.fetchall())
        return currency_rates

    @api.depends('tax_rate_ids.rate')
    def _compute_current_tax_rate(self):
        date = self._context.get('date') or fields.Date.today()
        company = self.env['res.company'].browse(self._context.get('company_id')) or self.env.company
        # the subquery selects the last rate before 'date' for the given currency/company
        currency_rates = self._get_tax_rate(company, date)
        for currency in self:
            currency.tax_rate = currency_rates.get(currency.id) or 1.0

    def _get_tax_conversion(self, company, date):
        self.env['res.currency.tax'].flush(['conversion', 'currency_id', 'company_id', 'name'])
        query = """SELECT c.id,
                        COALESCE((SELECT r.conversion FROM res_currency_tax r
                                  WHERE r.currency_id = c.id AND r.name <= %s
                                    AND (r.company_id IS NULL OR r.company_id = %s)
                               ORDER BY r.company_id, r.name DESC
                                  LIMIT 1), 1.0) AS conversion
                   FROM res_currency c
                   WHERE c.id IN %s"""
        self._cr.execute(query, (date, company.id, tuple(self.ids)))
        currency_rates = dict(self._cr.fetchall())
        return currency_rates

    @api.depends('tax_rate_ids.rate')
    def _compute_current_tax_conversion(self):
        date = self._context.get('date') or fields.Date.today()
        company = self.env['res.company'].browse(self._context.get('company_id')) or self.env.company
        # the subquery selects the last rate before 'date' for the given currency/company
        currency_rates = self._get_tax_conversion(company, date)
        for currency in self:
            currency.tax_conversion = currency_rates.get(currency.id) or 1.0

    @api.model
    def _get_conversion_tax_rate(self, from_currency, to_currency, company, date):
        currency_rates = (from_currency + to_currency)._get_tax_rate(company, date)
        res = currency_rates.get(to_currency.id) / currency_rates.get(from_currency.id)
        return res

    def _tax_convert(self, from_amount, to_currency, company, date, round=True):
        """Returns the converted amount of ``from_amount``` from the currency
           ``self`` to the currency ``to_currency`` for the given ``date`` and
           company.

           :param company: The company from which we retrieve the convertion rate
           :param date: The nearest date from which we retriev the conversion rate.
           :param round: Round the result or not
        """
        self, to_currency = self or to_currency, to_currency or self
        assert self, "convert amount from unknown currency"
        assert to_currency, "convert amount to unknown currency"
        assert company, "convert amount from unknown company"
        assert date, "convert amount from unknown date"
        # apply conversion rate
        if self == to_currency:
            to_amount = from_amount
        else:
            to_amount = from_amount * self._get_conversion_tax_rate(self, to_currency, company, date)
        # apply rounding
        return to_currency.round(to_amount) if round else to_amount

