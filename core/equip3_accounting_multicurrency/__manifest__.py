{
    'name': 'Accounting Multicurrency',
    'version': '1.1.7',
    'category': 'Accounting',
    'author': 'Mochamad Rezki',
    'depends': [
        'equip3_accounting_accessright_setting',
        'equip3_accounting_operation',
        'sr_manual_currency_exchange_rate'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/currency_views.xml',
        'views/account_move_views.xml',
        'wizards/currency_invoice_revaluation_views.xml',
        'views/account_internal_transfer_views.xml',
        'data/ir_sequence_data.xml',
        'views/account_voucher_views.xml'        
    ],
    'demo': [],
    'auto_install': False,
    'installable': True,
    'application': True
}