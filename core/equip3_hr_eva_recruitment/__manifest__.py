# -*- coding: utf-8 -*-
{
   'name': 'Hashmicro eva recruitment',
   'version': '1.1.1',
   'author': 'Hashmicro / denada',
   'depends': ['web','hr_recruitment','equip3_hr_recruitment_extend','sh_backmate_theme_adv'],
   'data': [
      "views/views.xml",
   ],
   'qweb': [
      'static/src/xml/*.xml',
   ],
   # 'auto_install': True,
   'installable': True,
   # 'application': True
}
