# -*- coding: utf-8 -*-
{
    'name': 'Inventory OPS APP',
    'author': 'Hashmicro/Antsyz- Saravana',
    'version': '1.1',
    'category': 'Stock API',
    'description': '''Stock module will be installed automatically and allow the Ops mobile app to access the Equip System to do the following functionality.
    - Receiving
    - Delivery
    - Internal Transfer
    ''',
    'summary': 'Manage your stock operation activities.',
    'depends': ['equip3_inventory_operation'],
    'data': [
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
