# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.tools.float_utils import float_compare


class StockMove(models.Model):
    _inherit = 'stock.move'

    incoming_reserved = fields.Float(copy=False, string='Reserved')
    scanned_ids = fields.One2many('stock.move.scanned', 'move_id', string='Scanned Moves')


StockMove()


class StockMoveScanned(models.Model):
    _name = 'stock.move.scanned'
    _description = 'Scanned Data of Stock Move'

    move_id = fields.Many2one('stock.move', string='Stock Move')
    name = fields.Char()
    qty = fields.Float()

StockMoveScanned()