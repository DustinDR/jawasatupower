# -*- coding: utf-8 -*-
from . import dashboard
from . import stock_picking
from . import product
from . import stock_move
from . import internal_transfer