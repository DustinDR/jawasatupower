from odoo import api, fields, models, _


class HrJob(models.Model):
    _name = 'hr.skill.type'
    _description = 'Hr Skill Type'
    _inherit = ['hr.skill.type', 'mail.thread']

    company_id = fields.Many2one("res.company", string="Company", default=lambda self: self.env.company,
                                 track_visibility='always')
    branch_id = fields.Many2one("res.branch", string="Branch", domain="[('company_id', '=', company_id)]",
                                track_visibility='always')
