# -*- coding: utf-8 -*-

from odoo import fields, models, api


class ResConfigSettingsInherit(models.TransientModel):
    _inherit = 'res.config.settings'

    emp_seq = fields.Boolean(string="Employee ID Sequence Number", default=True)

    def set_values(self):
        super(ResConfigSettingsInherit, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param('equip3_hr_masterdata_employee.emp_seq',
                                                         self.emp_seq)

    @api.model
    def get_values(self):
        res = super(ResConfigSettingsInherit, self).get_values()
        res.update(emp_seq=self.env['ir.config_parameter'].sudo().get_param('equip3_hr_masterdata_employee.emp_seq',
                                                                            default=True))
        return res
