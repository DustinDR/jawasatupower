from odoo import _, api, fields, models
from lxml import etree

class JobExperience(models.Model):
    _name = 'employee.job.experience.level'
    _description = 'Job Experience Level'
    _inherit = ['mail.thread']

    name = fields.Char(string="Job Experience Level", tracking=True)
    description = fields.Text("Description", tracking=True)
    created_by = fields.Many2one('res.users', "Created By", default=lambda self: self.env.user)
    created_date = fields.Date("Created On", default=fields.Date.today())
    company_id = fields.Many2one("res.company", string="Company", default=lambda self: self.env.company,
                                 tracking=True)
    branch_id = fields.Many2one("res.branch", string="Branch", domain="[('company_id', '=', company_id)]",
                                tracking=True)
    classification_ids = fields.Many2many("employee.job.classification", string="Job Classification")

    _sql_constraints = [('name_unique', 'unique(name)', 'Job Experience Level must be unique.')]



    @api.model
    def fields_view_get(self, view_id=None, view_type=None,
                        toolbar=False, submenu=False):
        res = super(JobExperience, self).fields_view_get(
            view_id=view_id, view_type=view_type)
        if  self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_manager'):
            root = etree.fromstring(res['arch'])
            root.set('create', 'true')
            root.set('edit', 'true')
            root.set('delete', 'true')
            res['arch'] = etree.tostring(root)
        elif self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_officer') and not self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_manager'):
            root = etree.fromstring(res['arch'])
            root.set('create', 'true')
            root.set('edit', 'true')
            root.set('delete', 'false')
            res['arch'] = etree.tostring(root)
        else:
            root = etree.fromstring(res['arch'])
            root.set('create', 'false')
            root.set('edit', 'false')
            root.set('delete', 'false')
            res['arch'] = etree.tostring(root)
            
        return res