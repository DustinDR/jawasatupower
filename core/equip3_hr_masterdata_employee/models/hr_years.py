from odoo import _, api, fields, models
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
import calendar
from lxml import etree

class equip3HrYears(models.Model):
    _name = 'hr.years'
    _description = 'HR Years'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    @api.depends('status')
    def compute_state(self):
        for rec in self:
            rec.state = rec.status

    state = fields.Selection([('draft', 'Draft'), ('open', 'Open'), ('closed', 'Closed')], compute='compute_state')
    status = fields.Selection([('draft', 'Draft'), ('open', 'Open'), ('closed', 'Closed')])
    name = fields.Integer("HR Year", required=True, states={'closed': [('readonly', True)]})
    code = fields.Char("Code")
    start_date = fields.Date(required=True, string="Start Date", states={'closed': [('readonly', True)]})
    end_date = fields.Date(required=True, string="End date", states={'closed': [('readonly', True)]})
    company_id = fields.Many2one("res.company", string="Company", default=lambda self: self.env.company,
                                 track_visibility='always')
    branch_id = fields.Many2one("res.branch", string="Branch", domain="[('company_id', '=', company_id)]",
                                track_visibility='always', states={'closed': [('readonly', True)]})
    year_ids = fields.One2many('hr.years.line', 'year_id', states={'closed': [('readonly', True)]})
    is_hide_create_month = fields.Boolean(default=False)
    is_hide_open_period = fields.Boolean(default=True)
    is_hide_close_period = fields.Boolean(default=True)
    
    
    @api.model
    def fields_view_get(self, view_id=None, view_type=None,
                        toolbar=False, submenu=False):
        res = super(equip3HrYears, self).fields_view_get(
            view_id=view_id, view_type=view_type)

        if  self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_manager'):
            root = etree.fromstring(res['arch'])
            root.set('create', 'true')
            root.set('edit', 'true')
            root.set('delete', 'true')
            res['arch'] = etree.tostring(root)
        elif self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_officer') and not self.env.user.has_group('equip3_hr_employee_access_right_setting.group_hr_manager'):
            root = etree.fromstring(res['arch'])
            root.set('create', 'true')
            root.set('edit', 'true')
            root.set('delete', 'false')
            res['arch'] = etree.tostring(root)
        else:
            root = etree.fromstring(res['arch'])
            root.set('create', 'false')
            root.set('edit', 'false')
            root.set('delete', 'false')
            res['arch'] = etree.tostring(root)
            
        return res


    def unlink(self):
        for record in self:
            if record.status in ('closed', 'open'):
                raise ValidationError("Only Draft status can be deleted")
        data = super(equip3HrYears, self).unlink()
        return data

    @api.onchange('start_date', 'end_date')
    def _onchange_date(self):
        for record in self:
            if record.start_date and record.end_date:
                if record.start_date > record.end_date:
                    raise ValidationError("End Date must be greater than Start Date!")

    @api.constrains('name')
    def check_name(self):
        for record in self:
            if record.name:
                check_name = self.search([('name', '=', record.name), ('id', '!=', record.id)])
                if check_name:
                    raise ValidationError("HR Year must be unique!")

    def diff_month(self, d1, d2):
        return (d1.year - d2.year) * 12 + d1.month - d2.month

    def create_month(self):
        for record in self:
            start_date = datetime.strptime(str(record.start_date), "%Y-%m-%d")
            end_date = datetime.strptime(str(record.end_date), "%Y-%m-%d")
            diff = record.diff_month(end_date, start_date)
            last_date = calendar.monthrange(start_date.year, start_date.month)[1]
            zero_first = "0" if start_date.month < 10 else ""
            line = [(0, 0, {'start_period': datetime(start_date.year, 1, start_date.month),
                            'end_period': datetime.strptime(f"{start_date.year}-{start_date.month}-{last_date}",
                                                            "%Y-%m-%d"),
                            'period_name': f"{zero_first}{start_date.month}/{start_date.year}",
                            'code': f"{zero_first}{start_date.month}/{start_date.year}",
                            'status': "draft"
                            })]
            plus = 0
            for data in range(diff):
                plus += 1
                month = datetime(start_date.year, 1, start_date.month) + relativedelta(months=plus)
                last_date_loop = calendar.monthrange(month.year, month.month)[1]
                zero = "0" if month.month < 10 else ""
                line.append((0, 0, {'start_period': month,
                                    'end_period': datetime.strptime(f"{month.year}-{month.month}-{last_date_loop}",
                                                                    "%Y-%m-%d"),
                                    'period_name': f"{zero}{month.month}/{month.year}",
                                    'code': f"{zero}{month.month}/{month.year}",
                                    'status': "draft"
                                    }))

            record.year_ids = line
            record.status = 'draft'
            record.is_hide_create_month = True
            record.is_hide_open_period = False
            record.message_post(body=f"Periods Status:draft")

    def to_open(self):
        for record in self:
            if record.year_ids:
                data = []
                for line in record.year_ids:
                    data.append((1, line.id, {'status': 'open'}))
                record.year_ids = data
            record.status = "open"
            record.is_hide_open_period = True
            record.is_hide_close_period = False
            record.message_post(body=f"Periods Status:Draft -> Open")

    def to_close(self):
        for record in self:
            if record.year_ids:
                data = []
                for line in record.year_ids:
                    data.append((1, line.id, {'status': 'closed'}))
                record.year_ids = data
            record.status = "closed"
            record.is_hide_close_period = True
            record.message_post(body=f"Periods Status:Open -> Closed")


class equip3HrYearsLine(models.Model):
    _name = 'hr.years.line'
    year_id = fields.Many2one('hr.years')
    period_name = fields.Char('Period Name')
    code = fields.Char("Code")
    start_period = fields.Date("Start Of Period")
    end_period = fields.Date("End Of Period")
    status = fields.Selection([('draft', 'Draft'), ('open', 'Open'), ('closed', 'Closed')])
