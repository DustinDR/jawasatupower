from odoo import api, fields, models, _


class HrContractWage(models.Model):
    _inherit = 'hr.contract'
    _description = 'HR Contract'

    parent_id = fields.Many2one('hr.employee', 'Manager', related='employee_id.parent_id')

    @api.onchange('wage')
    def onchange_wage(self):
        for rec in self:
            if rec.wage and rec.employee_id.grade_id.minimum_sal and rec.employee_id.grade_id.maximum_sal:
                if rec.wage < rec.employee_id.grade_id.minimum_sal or rec.wage > rec.employee_id.grade_id.maximum_sal:
                    warning = {
                        'title': _('Wage does not match!'),
                        'message': _(
                            'The Wage does not match with Range Salary of Grade %s. \nAre you sure to continue with '
                            'the current wage ?') % rec.employee_id.grade_id.name,
                    }
                    return {'warning': warning}
