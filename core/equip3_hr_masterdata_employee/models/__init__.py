# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from . import res_config_settings
from . import work_location
from . import religion
from . import race
from . import hr_contract
from . import employee_grade
from . import job_experience
from . import job_classification
from . import hr_job
from . import hr_department
from . import hr_skills
from . import orientation_checklist
from . import orientation_checklist_line
from . import hr
from . import res_country
from . import bank_account
from . import hr_employee_family
from . import hr_years
from . import employee_marital_status
from . import hr_company_document
from . import hr_employee_address

