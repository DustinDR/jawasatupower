from odoo import api, fields, models, _


class HrDepartmentBranch(models.Model):
    _inherit = 'hr.department'
    _description = 'Hr Department'

    company_id = fields.Many2one('res.company', string='Company', index=True, default=lambda self: self.env.company,
                                 tracking=True)
    branch_id = fields.Many2one("res.branch", string="Branch", domain="[('company_id', '=', company_id)]",
                                tracking=True)
    description = fields.Text('Description')
